package com.jutsugames.modernwizzards.gis;

import com.google.android.maps.GeoPoint;

public abstract interface SpatialZone {
	boolean intersects(GeoPoint loc);
	GeoPoint getCenter();
	void moveCenter(double distance, int bearing);
}
