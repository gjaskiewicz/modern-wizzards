package com.jutsugames.modernwizzards.gis;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.util.MoveLocation;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.widget.Toast;

public class SphericalZone implements SpatialZone {
	GeoPoint center;
	//promien w metrach
	public int radius;
	
	public SphericalZone(GeoPoint centerLoc, double rad) {
		super();
		center = centerLoc;
		radius = (int)rad;
		
	}

	@Override
	public boolean intersects(GeoPoint loc) {
		if(center == null) return false;
		return Units.optimizedDistanceCheck(loc, this.center, radius);
	}

	@Override
	public GeoPoint getCenter() {
		return center;
	}

	@Override
	public void moveCenter(double distance, int bearing) {
			center = MoveLocation.moveGeoPoint(center, distance, bearing);
	}

	public void setCenter(GeoPoint newCenter)	{
		center = newCenter;
	}
}
