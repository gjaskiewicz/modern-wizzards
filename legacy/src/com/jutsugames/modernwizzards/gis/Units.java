package com.jutsugames.modernwizzards.gis;

import com.google.android.maps.GeoPoint;

public class Units {
	
	//ile metrow przypada na mikrostopien LAT (20 000 000m obwodu ziemi / 180 000 000 microstopni)
	private static double MICROLATITUDE_TO_METER = 0.11;
	//do przeliczenia
	private static double MICROLONGITUDE_TO_METER = 0;
	
	/**
	 * @return ile metrow przypada na mikrostopien
	 */
	public static double latitudeToMeters()	{
		return MICROLATITUDE_TO_METER;
	}
	
	/**
	 * ile metrow przypada na mikrostopien
	 * @param latitude dla jakiej jest obliczane
	 * @return ile metrow przypada na mikrostopien
	 */
	public static double longitudeToMeters(int latitude)	{
		if (MICROLONGITUDE_TO_METER==0)	{
			//przelicz
			MICROLONGITUDE_TO_METER = Math.cos(Math.toRadians(latitude/1E6))*40 / 360 ;
			System.out.println("PRZELICZONE MICROLONGITUDE_TO_METER = " + MICROLONGITUDE_TO_METER);
		}
		return MICROLONGITUDE_TO_METER;
	}
	
	/**
	 * zwraca true jesli dystans jest mniejszy niz podany, w metrach
	 * @param loc1 pkt1
	 * @param loc2 pkt1
	 * @param meterDistance sprawdzany dystans
	 * @return true jesli dystans jest mniejszy
	 */
	public static boolean optimizedDistanceCheck(GeoPoint loc1, GeoPoint loc2, int meterDistance)	{
		if(Math.abs(loc1.getLatitudeE6()-loc2.getLatitudeE6())*latitudeToMeters() > meterDistance )
			return false;
		else if(Math.abs(loc1.getLongitudeE6()-
				loc2.getLongitudeE6())*longitudeToMeters(loc1.getLatitudeE6()) > meterDistance )
			return false;
		else if(optimizedDistance(loc1,loc2)>meterDistance)
			return false;
		return true;
	}
	
	/**
	 * zwraca przyblizona odleglosc pomiedzy punktami, w metrach
	 * @param loc1
	 * @param loc2
	 * @return
	 */
	public static int optimizedDistance(GeoPoint loc1, GeoPoint loc2)	{
		int latDiff = loc1.getLatitudeE6() - loc2.getLatitudeE6();
		int lngDiff = loc1.getLongitudeE6() - loc2.getLongitudeE6();
		
		return (int)Math.sqrt(
				Math.pow(latDiff*latitudeToMeters(), 2) +
				Math.pow(lngDiff*longitudeToMeters(loc2.getLatitudeE6()), 2)
			   );
	}
	
	
	public static int toMicroDeg(double degs) {
		return (int)(degs * 1E6);
	}
	
	//XXX: to nieprawda - poprawic
	public static double metersToDegLat(double m) {
		return m / 171196.672;
	}

	public static double metersToDegLng(double m) {
		return m / 111196.672;
	}
	
	public static double degLatToMeters(double relLat) {
		return relLat * 111196.672;
	}

	public static double degLngToMeters(double lat, double relLng) {
		return relLng * 111196.672 * (1/ Math.cos(Math.toRadians(lat)));
	}
	
	/**
	 * zwraca dok�adny dystans pomi�dzy punktami z uwzgl. krzywizny ziemi
	 * @deprecated
	 */
	private static double distance(double lat1, double lng1, double lat2, double lng2) {
		double R = 6371; // Radius of the earth in km
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lng2 - lng1); 
		double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		        Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * 
		        Math.sin(dLon/2) * Math.sin(dLon/2); 
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		double d = R * c * 1000; // Distance in meters
		
		return d;
	}
	
	public static GeoPoint coordsToLocation(double lat, double lng) {
		int lngDta = (int)(lng * 1E6);
        int latDta = (int)(lat * 1E6);
        return new GeoPoint(latDta, lngDta);
	}
}
