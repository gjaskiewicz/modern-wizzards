package com.jutsugames.modernwizzards;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jutsugames.modernwizzards.controller.AlertBulider;
import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.Inventory;
import com.jutsugames.modernwizzards.gamelogic.ItemsAggregation;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.achivements.Achivement;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class AchivementsActivity extends MWBaseActivity {
	private static final String TAG = "AchivementsActivity";
	
	private static Creature chosenOne;
	ListView listView;
	ArrayAdapter<Achivement> listAdapter;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Context ctx = this;
        setContentView(R.layout.list_general);
        listView = (ListView) findViewById(R.id.list);
        
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<Achivement> allAchivements = Game.getGame().getAchivContainer().getAllAchivs();
        
        TextView title = (TextView) this.findViewById(R.id.listTitle);
        title.setText(R.string.scr_stats_achivements);
        
        listAdapter = new ArrayAdapter<Achivement>(this, R.layout.creature_item, allAchivements )
        		{

        	@Override
        	public View getView(int position, View convertView, ViewGroup parent) {
        		
        		View row;
        		 
        		if (null == convertView) {
        			row = inflater.inflate(R.layout.creature_item, null);
        		} else {
        			row = convertView;
        		}
        		
        		Achivement ii = (Achivement) getItem(position);
        		ImageView iv = (ImageView) row.findViewById(R.id.creature_item_image);
        		if(!ii.isAchived())	{
        			iv.setImageResource(R.drawable.placeholder);
        			//iv.setVisibility(View.GONE);
        		}
        		else
        			iv.setImageResource(ii.getInfo(ctx).getImageID());
        		//iv.setImageDrawable(getResources().getDrawable(ii.getInfo(ctx).getImageID()));
        		
        		TextView tv = (TextView) row.findViewById(R.id.creature_item_description);        		
        		tv.setText(ii.getInfo(ctx).getName());
        		
        		return row;
        	}    
        };
        
        listView.setAdapter(listAdapter);
        
        
        //final Intent i = new Intent(this, CreatureDetailViewActivity.class);
        
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				Achivement ii = Game.getGame().getAchivContainer().getAllAchivs().get(arg2);
				AlertBulider ab = new AlertBulider() {
					@Override
					public void onYesClicked() {
					}
					
					@Override
					public void onNoClicked() {
					}
				};
				ab.message= ii.getInfo(ctx).getDescription();
				ab.infoOnly=true;
				AlertActivity.showNewAlert(AchivementsActivity.this, ab);
			}
         });          
         
    }
    
}
