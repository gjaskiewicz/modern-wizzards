package com.jutsugames.modernwizzards;

import java.util.Locale;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jutsugames.modernwizzards.Game.GameState;
import com.jutsugames.modernwizzards.MWMap2Activity;
import com.jutsugames.modernwizzards.controller.AlertBulider;
import com.jutsugames.modernwizzards.controller.CodesController;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class OptionsActivity extends MWBaseActivity {

	private static final int ACTIVITY_RESULT_QR_DRDROID = 0;
	ImageView imgView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		$log.d("INFO", "OA onCreate fun");
		//this.setLocale();
		this.getLocale();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.options);
		final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		Button backButton = (Button) findViewById(R.id.options_ButtonBack);
		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		CheckBox useGps = (CheckBox) findViewById(R.id.options_usegps);
		useGps.setVisibility(View.GONE);
		useGps.setChecked(Options.USE_GPS);
		useGps.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				Options.USE_GPS = isChecked;
				if(!Options.USE_GPS) Game.mainActivity.stopGPS();
				saveOptions();
			}
		});

		CheckBox generateObj = (CheckBox) findViewById(R.id.options_generate);
		generateObj.setChecked(Options.GENERATE_TEST_DATA);
		generateObj
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						Options.GENERATE_TEST_DATA = isChecked;
						saveOptions();
					}
				});

		CheckBox cbxVibrate = (CheckBox) findViewById(R.id.options_cbxVibrate);
		cbxVibrate.setChecked(Options.USE_VIBRATIONS);
		cbxVibrate
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						Options.USE_VIBRATIONS = isChecked;
						saveOptions();
					}
				});

		CheckBox cbxSounds = (CheckBox) findViewById(R.id.options_cbxSounds);
		cbxSounds.setChecked(Options.PLAY_SOUNDS);
		cbxSounds
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						Options.PLAY_SOUNDS = isChecked;
						saveOptions();
					}
				});
		
		
		CheckBox cbxSaves = (CheckBox) findViewById(R.id.options_cbxSaveAndLoad);
		cbxSaves.setChecked(Options.SAVE_AND_LOAD);
		cbxSaves.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				Options.SAVE_AND_LOAD = isChecked;
				saveOptions();
			}
		});
		
		
		CheckBox cbxDevMode = (CheckBox) findViewById(R.id.options_cbxdevelopmentsMode);
		cbxDevMode.setChecked(Options.DEVELOPMENT_MODE);
		cbxDevMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				Options.DEVELOPMENT_MODE = isChecked;
				saveOptions();
			}
		});
		
		Button deleteAllSavesButton = (Button) findViewById(R.id.options_deleteAllSaves);
		deleteAllSavesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		    	AlertBulider ab = new AlertBulider() {
					@Override
					public void onYesClicked() {
						Game.deleteAllSaves();
						Options.dontSaveThisTime = true;
					}
					@Override
					public void onNoClicked() {	}
				};
				ab.message= OptionsActivity.this.getString(R.string.opt_deletesaves_confirm);
				AlertActivity.showNewAlert(OptionsActivity.this, ab);
			}	
		});
		
		Button loadSaveButton = (Button) findViewById(R.id.options_loadSaveFromFile);
		loadSaveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Options.LOAD_SAVE_FROM_FILE = true;
				saveOptions();
			}	
		});
		
		Button mapv2Button = (Button) findViewById(R.id.options_mapv2);
		mapv2Button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Context context = getApplicationContext();
				CharSequence text = "mapv2 start!";
				int duration = Toast.LENGTH_SHORT;

				Toast toast = Toast.makeText(context, text, duration);
				toast.show();
				
	        	if(Game.getGame().getGameState()==GameState.STATE_INIT)	{
	        		MWToast.showToast(R.string.game_gps_loc_wait, Toast.LENGTH_SHORT);
	        		return;
	        	}
	        	else {				
        		Intent i = new Intent(getBaseContext(), MWMap2Activity.class);
    			//przekaz pozycje
        		startActivity(i);	
	        	}
			}
			
		});
		
		//qr code example
		Button qrcodeButton = (Button) findViewById(R.id.options_qr_scan);
		qrcodeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Context context = getApplicationContext();
				Intent qrDroid = new Intent( "la.droid.qr.scan" );
				//qrDroid.putExtra("la.droid.qr.image", "http://qrdroid.com/market/qr.png");
				qrDroid.putExtra( "la.droid.qr.complete" , true);
				try {
					CharSequence text = "qrDroid scan starting";
					int duration = Toast.LENGTH_SHORT;

					Toast toast = Toast.makeText(context, text, duration);
					toast.show();						
					startActivityForResult(qrDroid, 0);
					} 
				catch (ActivityNotFoundException activity) 
					{
					CharSequence text = "qrDroid not found";
					int duration = Toast.LENGTH_SHORT;

					Toast toast = Toast.makeText(context, text, duration);
					toast.show();										
					}
				}
			
		});
		
		//qr decode example
		Button qrdecodeButton = (Button) findViewById(R.id.options_qr_decode);
		qrdecodeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Context context = getApplicationContext();
				Intent qrDroid = new Intent( "la.droid.qr.decode" );
				//na razie na stale wpisane
				qrDroid.putExtra("la.droid.qr.image", "http://qrdroid.com/market/qr.png");
				qrDroid.putExtra( "la.droid.qr.complete" , true);
				try {
					CharSequence text = "qrDroid decode starting";
					int duration = Toast.LENGTH_SHORT;

					Toast toast = Toast.makeText(context, text, duration);
					toast.show();						
					startActivityForResult(qrDroid, 0);
					} 
				catch (ActivityNotFoundException activity) 
					{
					CharSequence text = "qrDroid not found";
					int duration = Toast.LENGTH_SHORT;

					Toast toast = Toast.makeText(context, text, duration);
					toast.show();										
					}
				}
			
		});		
		
		//qr encode example
		Button qrencodeButton = (Button) findViewById(R.id.options_qr_encode);
		qrencodeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Context context = getApplicationContext();
				Intent qrDroid = new Intent( "la.droid.qr.encode" );
				//na razie na stale wpisane
				qrDroid.putExtra("la.droid.qr.code", "Witaj  !");
				//qrDroid.putExtra( "la.droid.qr.size" , 150);
				qrDroid.putExtra( "la.droid.qr.image" , true);
				try {
					CharSequence text = "qrDroid encode starting";
					int duration = Toast.LENGTH_SHORT;

					Toast toast = Toast.makeText(context, text, duration);
					toast.show();						
					startActivityForResult(qrDroid, 0);
					} 
				catch (ActivityNotFoundException activity) 
					{
					CharSequence text = "qrDroid not found";
					int duration = Toast.LENGTH_SHORT;

					Toast toast = Toast.makeText(context, text, duration);
					toast.show();										
					}
				}
			
		});				

		final EditText codeEnter = (EditText) findViewById(R.id.codeEnter);
		codeEnter.setOnEditorActionListener(new OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if( event!=null )	{
					CodesController.parseCode(v.getText().toString());
					codeEnter.setText("");
				}
				return false;
			}
		});
		
		
		Spinner spinner = (Spinner) findViewById(R.id.spinner1);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.language_sel,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		if(Options.LANGUAGE_ID==100)	{
			String loc= Locale.getDefault().getLanguage();
			if(loc.equals("pl")) Options.LANGUAGE_ID=1;
			else Options.LANGUAGE_ID=0;
		}
		spinner.setSelection(Options.LANGUAGE_ID);
		
		this.getLocale();
	}
	
	@Override
	/**
	 * Reads data scanned by user and returned by QR Droid
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if( ACTIVITY_RESULT_QR_DRDROID==requestCode && null!=data && data.getExtras()!=null ) {
			//Read result from QR Droid (it's stored in la.droid.qr.result)
			String result = data.getExtras().getString("la.droid.qr.result");
			//Just set result to EditText to be able to view it

			int duration = Toast.LENGTH_SHORT;
			Context context = getApplicationContext();
			Toast toast = Toast.makeText(context, result, duration);
			toast.show();	
			
			imgView.setImageURI( Uri.parse(result) );
		}
	}	

	private void saveOptions() {
		/*
		 * CheckBox useGps = (CheckBox) findViewById(R.id.options_usegps);
		 * Options.USE_GPS=useGps.isChecked(); CheckBox generateObj = (CheckBox)
		 * findViewById(R.id.options_generate);
		 * Options.GENERATE_TEST_DATA=generateObj.isChecked(); CheckBox
		 * cbxVibrate = (CheckBox) findViewById(R.id.options_cbxVibrate);
		 * Options.USE_VIBRATIONS=cbxVibrate.isChecked();
		 * Options.SAVE_AND_LOAD=(
		 * (CheckBox)findViewById(R.id.options_cbxSaveAndLoad)).isChecked();
		 */
		this.saveLanguage();
		Options.saveOptions(this);
	}

	private void saveLanguage() {
		Spinner spinner = (Spinner) findViewById(R.id.spinner1);
		String selected = spinner.getSelectedItem().toString();

		if (selected.equals("Polski")) {
			Options.LANGUAGE_ID = 1;
			$log.d("INFO", "OA saveLanguageId 1");
		}
		else	{
			Options.LANGUAGE_ID = 0;
			$log.d("INFO", "OA saveLanguageId 0");
		}
	}

	// pobiera locale
	private Locale getLocale() {
		// print locale
		Locale l = this.getBaseContext().getResources().getConfiguration().locale;
		$log.d("INFO", "OA getLocale fun: " + l);
		return l;
	}

	// ustawia jezyk - jest w mainActivity
	/*private void setLocale() {
		String loc = "en";
		if (Options.LANGUAGE_ID == 0) {
			loc = "en";
		} else if (Options.LANGUAGE_ID == 1) {
			loc = "pl";
		}

		if (loc == "en" || loc == "pl") {
			Locale locale = new Locale(loc);
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
			$log.d("INFO", "OA setLocale: " + loc);
		}

	}*/

	public void onNothingSelected(AdapterView<?> parent) {
		// Another interface callback
		$log.d("INFO", "onNothingSelected");
	}

	public void startWithMock(View view) {
		System.out.println("START GAME WITH MOCK!");
		// Options.GENERATE_OBJECTS = true;
		// Game.getGame().testGenerateForMockLocation();
	}
	
	@Override
	public void finish() {
		saveOptions();
		super.finish();
	}
}
