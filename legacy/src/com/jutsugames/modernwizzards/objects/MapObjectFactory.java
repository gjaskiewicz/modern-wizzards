package com.jutsugames.modernwizzards.objects;

import java.util.Date;
import java.util.Random;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.gis.Units;

public class MapObjectFactory {	
	
	public static long getGeoDateHash(GeoPoint point)	{
		Date data = new Date();
		//data.getHours();
		long geohash = LocationSlab.geoPointToSlabHash(point) * 10000;
		
		int day = data.getDay()*100;
		//zapis seeda = xxxxyyyyddhh;
		//System.out.println("the day is: "+data.getDay());
		long seed = geohash + day;
		return seed;
	}	
	
}
