package com.jutsugames.modernwizzards.objects;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.MainLoop;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.objects.map.ManaZone;

/*
 * Reprezentuje wycinek �wiata
 */
public class LocationSlab {
	//microstopnie geograficzne
	//10000 microstopni geog ~1,1 kilometra
	public static int SLAB_SIZE_LAT = 5000;
	//proporcja km / stopnie dlugosci geog sie zmienia z szerokoscia geograficzna 
	public static int SLAB_SIZE_LNG = 8000;
	
	long slabHash;
	//zakres dlugosci i szerokosci
	int latSpan;
	int lngSpan;
	//lewa dolna wspolrzedna wycinka w microstopniach
	GeoPoint location;
	
	public boolean generatedCreatures = false;
	public boolean generatedManazones = false;
	public boolean generatedPlants = false;
	
	/** wygenerowane stworki w tym segmencie*/
	public transient LinkedList<Creature> creatures = new LinkedList<Creature>();
	/** wygenerowane itemy w tym segmencie*/
	public transient LinkedList<CollectableItem> items = new LinkedList<CollectableItem>();
	/** wygenerowane manazony w tym segmencie*/
	public transient LinkedList<ManaZone> manaZones = new LinkedList<ManaZone>();
	
	/*
	 * wsp�rzedne w mikrostopniach
	 */
	private LocationSlab(int latID, int lngID, long hash) {
		slabHash = hash;
		location = new GeoPoint(latID, lngID);
		latSpan = SLAB_SIZE_LAT;
		lngSpan = SLAB_SIZE_LNG;
	}
	
	public static LocationSlab newSlabForLocation(GeoPoint g) {
		
		int locLatID = latitudeToHash(g.getLatitudeE6())*SLAB_SIZE_LAT;
		int locLngID = longitudeToHash(g.getLongitudeE6())*SLAB_SIZE_LNG;
		long slabHash = geoPointToSlabHash(g); 
		return new LocationSlab(locLatID, locLngID, slabHash);
	}
	
	public static long geoPointToSlabHash(GeoPoint g)	{
		long lat = (long)latitudeToHash(g.getLatitudeE6())*100000;
		long lng = (long)longitudeToHash(g.getLongitudeE6());
		return lat + lng;
		//XXXXXYYYYY
	}
	
	public static int latitudeToHash(int lat)	{
		return (int)Math.floor(lat/SLAB_SIZE_LAT);
	}
	
	public static int longitudeToHash(int lng)	{
		return (int)Math.floor(lng/SLAB_SIZE_LNG);
	}
	
	public long getHash()	{
		return slabHash;
	}

	public void regenerate() {
		//if(!Options.GENERATE_TEST_DATA) return;
		
		//System.out.println("GENERATING OBJECTS FOR NEW SLAB!");
		if(!generatedCreatures && GameContent.getInstance().knownCreatures.size()>0)	{
			creatures.addAll(CreaturesFactory.createCreatures(this));
			generatedCreatures=true;
		}
		if(!generatedPlants && GameContent.getInstance().knownPlants.size()>0)	{
			items.addAll(PlantFactory.createObjects(this));
			generatedPlants=true;
		}
		if(!generatedManazones && GameContent.getInstance().generateManaZones)	{
			manaZones.addAll(ManaZonesFactory.createZones(this));
			generatedManazones=true;
		}
	}

	public LinkedList<PhysicalObject> getAllGeneratedObjects() {
		LinkedList<PhysicalObject>  allObjects = new LinkedList<PhysicalObject>();
			allObjects.addAll(this.creatures);
			allObjects.addAll(this.items);
			allObjects.addAll(this.manaZones);
			return allObjects;
	}
	
	/**
	 * usuwa obiekcik z mapy, mniejsza o to co to jest.
	 * @param po
	 * @return
	 */
	public boolean remove(PhysicalObject po)	{
		po.display(false);
		MainLoop.animations.remove(po);
		return 
		creatures.remove(po) ||
		manaZones.remove(po) ||
		items.remove(po);
	}
	
	/*public static List<LocationSlab> upperBoundSlabAreaCover(double latMin, double latMax, double lngMin, double lngMax) {
		List<LocationSlab> result = new LinkedList<LocationSlab>();
		
		long locLatIDMin = (long)Math.ceil(latMin / latSpan);
		long locLngIDMin = (long)Math.ceil(lngMin / lngSpan);
		
		long locLatIDMax = (long)Math.ceil(latMax / latSpan);
		long locLngIDMax = (long)Math.ceil(lngMax / lngSpan);
		
		for (long i=locLatIDMin;i<=locLatIDMax;i++)
			for (long j=locLngIDMin;j<=locLngIDMax;j++) {
				result.add(new LocationSlab(i, j));
			}
		
		return result;
	}
	
	public long getRandomSeed() {
		Date toDay = new Date();
		int year = toDay.getYear();
		int mnth = toDay.getMonth();
		int day = toDay.getDay();
		
		long sdDate = Long.parseLong(""+year+mnth+day);
		long sdLoc = 
				(2083 * latID) % 4638551 +
				(4001 * lngID) % 4638551;
		
		return (25183 * sdDate + 17021 * sdLoc) % 8769499;
	}
	
	public boolean hasPosition(double lat, double lng) {
		double myLat = getLat();
		double myLng = getLng();
		
		if (myLat <= lat && lat < myLat + latSpan) 
			if (myLng <= lng && lng < myLng + lngSpan) {
				return true;
			}
		
		return false;
	}*/
	
}
