package com.jutsugames.modernwizzards.objects;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.AllSpells;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.creatures.*;
import com.jutsugames.modernwizzards.gis.SphericalZone;
import com.jutsugames.modernwizzards.gis.Units;
import com.jutsugames.modernwizzards.map.WorldContainer;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class CreaturesFactory extends MapObjectFactory {
	private static int CREATURES_SEED = 7;
	
	public static List<Creature> createCreatures(LocationSlab ls) {
		
		Random r = new Random(getGeoDateHash(ls.location)+CREATURES_SEED);	
		List<Creature> result = new LinkedList<Creature>();
		
		int zonesLimit = 15;
		for (int i = 0; i <= zonesLimit; i++)	{
			
			int creatureSeed = r.nextInt(Integer.MAX_VALUE);
			Random creatureRandSet = new Random(creatureSeed);
		
				Creature creature = getCreature(creatureRandSet);
				if(creature == null) continue;
				creature.isGenerated = true;
				creature.homeLocation = ls;
				creature.generationHash = creatureSeed;
				creature.zone.radius = (int) (creatureRandSet.nextDouble() * 25 + 30);
				
				GeoPoint gp = new GeoPoint(ls.location.getLatitudeE6() + (int)(creatureRandSet.nextDouble() * ls.latSpan),
					(ls.location.getLongitudeE6() + (int)(creatureRandSet.nextDouble() * ls.lngSpan)));
				creature.setLocation(gp);
				
				if(Game.getGame().worldContainer.creaturesDefeated.contains(creatureSeed)) {
					//nie dodawaj - juz bylo
				}
				else	{
					//dodaj
					result.add(creature);
				}
					
		}
		return result;
	}

	public static Creature getCreature(Random r)	{
		//tylko aby sie wygenerowały;
		GameContent.getInstance().getAllCreaturesTypes();
		
		if(Creature.creaturesPopularitySum==0) return null;
		int seed = r.nextInt(Creature.creaturesPopularitySum);
		for(Creature cp : GameContent.getInstance().getAllCreaturesTypes())	{
			if(seed < cp.popularity + cp.popularityOffset)	{
				try {
					if(!GameContent.getInstance().knownCreatures.contains(cp.nameID))	{
						//stworek jeszcze nie znany, nie dodajemy. 
						return null;
					}
					return cp.getClass().newInstance();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		$log.e("CreaturesFactory", "Invalid seed while generate");
		return null;
	}
	
}
