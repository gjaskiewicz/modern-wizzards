package com.jutsugames.modernwizzards.objects;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.gis.SphericalZone;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.objects.map.ManaZone;

public class ManaZonesFactory extends MapObjectFactory {

	public static List<ManaZone> createZones(LocationSlab ls) {
		Random r = new Random(getGeoDateHash(ls.location)+77);
		
		List<ManaZone> result = new LinkedList<ManaZone>();
		
		//ma by od 4 do 3 zon�w i basta. 
		int zonesLimit = r.nextInt(1) + 3;
		for (int i = 0; i <= zonesLimit; i++)	{
			//double rand = r.nextDouble();
			//if (rand < 0.1 ) {
				GeoPoint gp = new GeoPoint(ls.location.getLatitudeE6() + (int)(r.nextDouble() * ls.latSpan),
						(ls.location.getLongitudeE6() + (int)(r.nextDouble() * ls.lngSpan)));
				ManaZone mz = new ManaZone(gp,
						(int)(70 + (r.nextDouble()-0.5)*40));
				mz.isGenerated=true;
				mz.homeLocation=ls;
				result.add(mz);
			//}		
		}		
		return result;
		
	}

}
