package com.jutsugames.modernwizzards.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.creatures.*;
import com.jutsugames.modernwizzards.gamelogic.mixtures.*;
import com.jutsugames.modernwizzards.gamelogic.spells.*;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class ObjectInfoFactory {
	
	private HashMap<String, ObjectInfo> storedRes = new HashMap<String, ObjectInfo>();
	private Context ctx;
	private static ObjectInfoFactory instance = new ObjectInfoFactory();

	public void initializeContext(Context ctx_) {
		ctx = ctx_;
	}
	
	private ObjectInfoFactory () { 
		storedRes = new HashMap<String, ObjectInfo>(); 
	}
	
	public static ObjectInfoFactory get() { 
		if(instance == null) {
			instance = new ObjectInfoFactory();
		}
		return instance; 
	}

	public ObjectInfo createById(String id, int[] ids, Class cls, Context ctx) {
		return new ObjectInfo(
			id,
			ids[0],
			ctx.getResources().getString(ids[1]),
			ctx.getResources().getString(ids[2]),
			cls
		);
	}
	
	/**
	 * Automatycznie zaczytuje resources dla podanej nazwy -> chodzi przede wszystkim o rosliny, 
	 * stwory i mikstury/czary
	 * TODO: odizolowac obrazki od nazwy i opisu
	 * @param nameID
	 * @return zmapowany ObjectInfo
	 */
	public ObjectInfo getInfoStandarized(String nameID)	{
		
		//albo wyciagamy z zebranych info
		if(storedRes.containsKey(nameID))	{
			//$log.d("OBJ INFO", "resource \"" + nameID + "\" exists!");
			return storedRes.get(nameID);
		}
		//albo generujemy i dodajemy
		$log.d("OBJ INFO", "generating info for : " + nameID );
		
		int picture = 0;
		int dispName = 0;
		int description = 0;
		try { picture = ctx.getResources().getIdentifier(nameID, "drawable", ctx.getPackageName() );
		}
		catch (Exception e)	{ 
			picture = R.drawable.unknown;

		}
		
		try { dispName = ctx.getResources().getIdentifier(nameID, "string", ctx.getPackageName() );
		}
		catch (Exception e)	{ //R.string.undefined;
		}
		
		try { description = ctx.getResources().getIdentifier(nameID+"_desc", "string", ctx.getPackageName() );
		}
		catch (Exception e)	{  //R.string.undefined;
		}
		
		String name, descr;
		
		if(picture == 0) {
			picture = R.drawable.unknown;
			/*if(nameID.contains("potion"))	{
				picture = ctx.getResources().getIdentifier("potion_uni", "drawable", ctx.getPackageName());
			}*/
		}
		if(dispName == 0 )	{
			name = "!"+nameID+"!";
		}
		else
			name = ctx.getResources().getString(dispName);
		if(description == 0 )
			descr = "!"+nameID+"_desc!";
		else
			descr = ctx.getResources().getString(description);
		
		ObjectInfo res = new ObjectInfo(
				nameID,
				picture,
				name,
				descr,
				null
			);
		
		storedRes.put(nameID, res);
		
		return res;
	}
	
}
