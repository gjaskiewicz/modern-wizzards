package com.jutsugames.modernwizzards.objects.map;

import java.util.Random;

import android.R.bool;
import android.R.string;
import android.content.Context;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.Game.GameState;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.MainLoop;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gis.SpatialZone;
import com.jutsugames.modernwizzards.gis.SphericalZone;
import com.jutsugames.modernwizzards.util.ChangeListener.EventTypes;
import com.jutsugames.modernwizzards.util.MWMath;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class ManaZone extends PhysicalObject {
	private static final String TAG = "ManaZone";	
	
	public static final String manaZoneID = "ManaZoneID";
	
	private static final int MANAZONE_COLOR = 0x809999FF;
	private boolean firstEntered = false;
	
	private boolean isPlayerInside = false;
	private int alpha = 180;
	
	public ManaZone(GeoPoint gp, int radius) {
		setLocation(gp);
		zone = new SphericalZone(gp, radius);
	}

	@Override
	public boolean shouldInteract(GeoPoint gp) {
		boolean intersects = zone.intersects(gp);
		if(intersects && !isPlayerInside)	{
			//gracz wlaz�
			isPlayerInside = true;
			//animuj!
			dispCircle.setFillColor(getColor());
			
			MWToast.showToast(R.string.player_mana_charging, Toast.LENGTH_LONG);
			
		}
		else if (!intersects && isPlayerInside) {
			//gracz wylaz�
			isPlayerInside = false;
			Game.getGame().getAchivContainer().getAchivMedit().stop();
			//nie animuj!
		}
		return intersects;
	}

	@Override
	public void interact() {
		Player p = Game.getGame().playerEgo;
		if(!firstEntered) {
			firstEntered = true;
			notifyListeners(manaZoneID, EventTypes.VISITED, null);
		}
	
		if(Game.getGame().getGameState() == GameState.STATE_PLAYERDEAD)	{
			p.resurect();
		}
		if(!Player.getInst().isUnderEffectOfSpell("spell_oob"))	{
			p.addManaPoints(Player.getInst().manaRegenFactor, false);
			Game.getGame().getAchivContainer().getAchivMedit().checkMedit();
		}
	}



	public int getColor() {
		if(!isPlayerInside) {
			return MANAZONE_COLOR;
		}
		else {
			alpha+=7;
			int color = (int)((MWMath.sin(alpha)+1)*68+88) * 0x01000000 + 0x009999FF;
			//$log.d("MANA ZONE", "alpha:" + (int)((MWMath.sin(alpha)+1)*64+64) + " color: " +color);
			return color;
		}

	}

}
