package com.jutsugames.modernwizzards.objects.map;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gis.SphericalZone;
import com.jutsugames.modernwizzards.util.ChangeListener;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class TriggerZone extends PhysicalObject {
	
	private static final String TAG = "TriggerZone";
	private static final int TRIGGER_COLOR = 0xD0D0D0D0;
	//private int radius;
	private String zoneID;
	private String label;
	
	private transient OverlayItem vis;
	private transient Timer timer = new Timer();
	
	public TriggerZone(String ID, GeoPoint gp, int rad, String label_) {
		//radius = rad;
		zoneID = ID;
		if(label_ == null )
			label = Game.getGlobalTopmostActivity().getString(R.string.trigger);
		else
			label = label_;
		setLocation(gp);
		zone = new SphericalZone(gp, rad);
		
		
	}
	
	@Override
	public boolean shouldInteract(GeoPoint gp) {
		return zone.intersects(gp);
	}

	@Override
	public void interact() {
		notifyListeners(zoneID, ChangeListener.EventTypes.VISITED,null);
		if(timer != null) {
			timer.cancel();
			timer.purge();
		}
		timer = new Timer();
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				$log.d(TAG, "Exited emited from zone " + zoneID);
				notifyListeners(zoneID, ChangeListener.EventTypes.EXITED,null);
			}
		},3000);
		//MWToast.makeToast(Game.getGame().mainActivity, "Trigger interacts!", Toast.LENGTH_SHORT);
	}

	@Override
	public int getColor() {
		return TRIGGER_COLOR;
	}
}
