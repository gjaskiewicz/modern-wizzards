package com.jutsugames.modernwizzards.objects.map;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;
import com.jutsugames.modernwizzards.*;
import com.jutsugames.modernwizzards.Game.GameState;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gis.SpatialZone;
import com.jutsugames.modernwizzards.gis.SphericalZone;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.util.ChangeListener.EventTypes;
import com.jutsugames.modernwizzards.util.VibratorUtil;

public class CollectableItem extends PhysicalObject {
	private static final String TAG = "CollectableItem";
	private InventoryItem item;
	
	public boolean playerWasUninterested = false;
	private boolean lastCheckVisited = false;
	
	/** do alert�w */
	public static boolean runningDialog;
	public static CollectableItem runningDialogItem;
	
	public CollectableItem() {}
	
	public CollectableItem(SphericalZone zone, InventoryItem item) {
		super();
		this.zone = zone;
		this.item = item;
		if(zone!=null)
			setLocation(zone.getCenter());
	}

	@Override
	public boolean shouldInteract(GeoPoint gp) {
		if(zone.intersects(gp))	{
			//ci�gle jest w zonie i nie chcia�.
			if(lastCheckVisited == true && playerWasUninterested) {
				return false;
			}
			else {
				lastCheckVisited = true;
				return true;
			}
		}
		else {
			lastCheckVisited = false;
			return false;
		}
		//return zone.intersects(gp);
	}

	@Override
	public void interact() {
		//System.out.println("Pickable");
		if (runningDialog || Game.getGame().getGameState() != GameState.STATE_WALKING) {
			return;
		}
		if(Creature.runningDialog)	{
			return;
		}
		
		runningDialog = true;
		$log.d("PLANT", "Plant Interacts: " + nameID );
		
		long[] vPatern = {0, 300};
		VibratorUtil.vibrate(vPatern, -1);
		
		runningDialogItem=this;
		Game.mainActivity.startActivity(new Intent(Game.mainActivity, AlertItemActivity.class)
			.putExtra(AlertActivity.ALERT_TYPE, AlertActivity.ALERT_TYPE_ITEM)
		);
		Game.getGlobalTopmostActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
	}
	
	public InventoryItem getItem() {
		return item;
	}
	
	public ObjectInfo getInfo() {
		return item.getInfo();
	}	
	
	public void collected() {
		notifyListeners(item.getNameID(), EventTypes.COLLECTED, null);
	}
	
	@Override
	public int getColor() {
		return item.getColor();
	}

}
