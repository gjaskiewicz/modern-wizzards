package com.jutsugames.modernwizzards.objects;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ObjectInfo {
	private final String id;
	private final int imageID;
	private final String name;
	private final String description;
	private final Class gameObjectClass;
	
	public ObjectInfo(String id, int imageID, String name, String description, Class gameObjectClass) {
		super();
		this.id = id;
		this.imageID = imageID;
		this.name = name;
		this.description = description;
		this.gameObjectClass = gameObjectClass;
	}

	public int getImageID() {
		return imageID;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getID() {
		return id;
	}
	
	public Class getObjectClass() {
		return gameObjectClass;
	}
	
	/**
	 * u�ywa� z rozwag�, nie bedzie zawsze dzialalo. 
	 * @return
	 */
	public Object newInstance() {
		try {
			Constructor ctor = gameObjectClass.getConstructor();
			return ctor.newInstance();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
