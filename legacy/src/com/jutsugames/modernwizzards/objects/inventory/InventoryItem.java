package com.jutsugames.modernwizzards.objects.inventory;

import android.content.Context;
import com.jutsugames.modernwizzards.objects.ObjectInfo;

/*
 * Inventory item to na razie mikstury i skladniki
 * Kieeedys moze jeszcze jakies amulety (permanentny spell), inne artefakty
 */
public interface InventoryItem {		
	ObjectInfo getInfo();
	int getColor();
	void use();
	String getNameID();
	
	//TODO: 
	
}
