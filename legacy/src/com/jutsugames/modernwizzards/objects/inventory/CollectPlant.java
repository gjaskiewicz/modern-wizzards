package com.jutsugames.modernwizzards.objects.inventory;

import android.content.Context;

import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;


public class CollectPlant implements InventoryItem {

	private String nameID;
	
	/**
	 * skala popularności w odniesieniu do innych skladnikow
	 */
	public int popularity;
	/**
	 * przesuniecie popularnosci (potrzebne do generacji wg popularności)
	 */
	public int popularityOffset;
	/**
	 * suma popularnosci wszystkich roslinek, do wyważonej generacji
	 */
	public static int plantsPopularitySum=0;
	public int poisonousPower;
	public double visibility;

	public CollectPlant(String name) {
		this.nameID = name;
	}
	
	public CollectPlant(CollectPlant cp) {
		this.nameID = cp.nameID;
		this.popularity = cp.popularity;
		this.visibility = cp.visibility;
	}

	@Override
	public int getColor() {
		return 0x8800FF00 - 0x1100*popularity;
		//return 0x8800FF00;
	}
	
	public String getNameID() {
		return nameID;
	}

	@Override
	public ObjectInfo getInfo() {
		return ObjectInfoFactory.get().getInfoStandarized(nameID);
	}

	@Override
	public void use() {
		// TODO Auto-generated method stub
		
	}

}
