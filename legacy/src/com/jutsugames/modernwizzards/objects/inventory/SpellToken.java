package com.jutsugames.modernwizzards.objects.inventory;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.SpellFromPotion;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

/**
 * Czy to jest innymi s�owy mikstura??
 */
public class SpellToken implements InventoryItem, Castable {
	private static final String TAG = "SpellToken";
	private final Castable spell;
	private boolean blocked = false;
	
	public SpellToken(Castable spell) {
		super();
		this.spell = spell;
	}
	
	public int getColor() {
		if(spell instanceof SpellFromPotion)	{
			return ((SpellFromPotion)spell).getColor();
		}
		else return 0xFF888888;
	}

	public Castable getSpell() {
		return spell;
	}
	
	public void setBlocked(boolean isBlocked) {
		blocked = isBlocked;
	}
	
	public boolean isBlocked() {
		return blocked;
	}

	@Override
	public int getRequiredMana() {
		return spell.getRequiredMana();
	}

	@Override
	public boolean castThis() {
		if(!blocked) {
			Game.getGame().getInventory().removeMixture(this);
			$log.d(TAG, "Casting spell");
			spell.castThis();
			return true;
		} else {
			$log.d(TAG, "Casting spell blocked");
			MWToast.makeToast(	Game.getGlobalTopmostActivity(), 
								R.string.usageBlocked, 
								Toast.LENGTH_LONG);
			return false;
		}
	}

	@Override
	public void takeEffect() {
		spell.takeEffect();
	}

	@Override
	public ObjectInfo getInfo() {
		return spell.getInfo();
	}

	@Override
	public UsageType getUsageType() {
		return spell.getUsageType();
	}

	@Override
	public EffectTargetType getEffectTargetType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void use() {
		castThis();
		
	}

	@Override
	public String getNameID() {
		return spell.getInfo().getName();
	}

}
