package com.jutsugames.modernwizzards.objects;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.AllSpells;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gis.SphericalZone;
import com.jutsugames.modernwizzards.gis.Units;
import com.jutsugames.modernwizzards.map.WorldContainer;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;

public class PlantFactory extends MapObjectFactory {
	private static int PLANTS_SEED = 1;
	
	public static List<CollectableItem> createObjects(LocationSlab ls) {
		
		Random r = new Random(getGeoDateHash(ls.location)+PLANTS_SEED);
		List<CollectableItem> result = new LinkedList<CollectableItem>();
		
		int zonesLimit = r.nextInt(6) + 17;
		for (int i = 0; i <= zonesLimit; i++)	{
			
			int plantSeed = r.nextInt(Integer.MAX_VALUE);
			Random plantRandSet = new Random(plantSeed);

				int latitude = (ls.location.getLatitudeE6() + (int)((plantRandSet.nextDouble()) * ls.latSpan));
				int longitude = (ls.location.getLongitudeE6() + (int)((plantRandSet.nextDouble()) * ls.lngSpan));
				GeoPoint gp = new GeoPoint(latitude, longitude);
				CollectPlant cp = getObject(plantRandSet);
				if(cp==null) continue;
				CollectableItem cIt = new CollectableItem(
						new SphericalZone(gp, 30+(plantRandSet.nextInt(15))),
						cp
					);
				cIt.isGenerated=true; 
				cIt.homeLocation=ls;
				cIt.generationHash=plantSeed;
				if(Game.getGame().worldContainer.gatheredPlants.contains(plantSeed)) {
					//nie dodawaj - juz bylo
				}
				else	{
					//dodaj
					result.add(cIt);
				}
	
		}		

		return result;
	}
	
	static CollectPlant getObject(Random r)	{
		//tylko aby sie wygenerowały;
		GameContent.getInstance().getAllPlantsTypes();
		
		if(CollectPlant.plantsPopularitySum==0) return null;
		double rand = r.nextInt(CollectPlant.plantsPopularitySum);
		for(CollectPlant cp : GameContent.getInstance().getAllPlantsTypes())	{
			if(rand < (cp.popularity + cp.popularityOffset))	{
				if(!GameContent.getInstance().knownPlants.contains(cp.getNameID()))
					//nieznana rozlinka
					return null;
				return new CollectPlant(cp);
			}
		}
		return null;
	}
	
}
