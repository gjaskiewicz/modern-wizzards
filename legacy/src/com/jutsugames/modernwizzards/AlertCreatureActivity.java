/**
 * 
 */
package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;

/**
 * @author bartek
 *
 */
public class AlertCreatureActivity extends AlertActivity {

    public void onCreate(Bundle savedInstanceState) {
    	
    	objectDisplayed = Creature.runningDialogCreature;
    	
    	super.onCreate(savedInstanceState);
        $log.d(TAG, "AlertActivity starts");
        
        setForCreature();
		
    }  
	
	public void setForCreature() {
		buttonYes.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	$log.d(TAG, "AlertActivity FIGHT clicked");
	        	//objectDisplayed.runningDialog.dismiss(); 
	        	startFight();
	        }}
		);	
		//dodaj listenera do buttona no
		buttonNo.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	$log.d(TAG, "AlertActivity RUN clicked");
	        	Creature.runningDialog = false;
	        	Game.getGame().playerRunsFromCreature((Creature) objectDisplayed);
	        	finish();
	        }}
		);	
	}

	@Override
	public void onBackPressed() {
		$log.d(TAG, "AlertCreatureActivity BACK clicked");
			Creature.runningDialog = false;
			Game.getGame().playerRunsFromCreature((Creature) objectDisplayed);
		super.onBackPressed();
	}

    protected void startFight() {
     	Creature.runningDialog = false;
        Game.getGame().playerFightsCreature((Creature) objectDisplayed);
        finish();
    }
	
}
