package com.jutsugames.modernwizzards;

import java.util.LinkedList;
import java.util.List;

import com.jutsugames.modernwizzards.controller.AlertBulider;
import com.jutsugames.modernwizzards.controller.InventoryItemsAdapter;
import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.SpellFactory;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import com.jutsugames.modernwizzards.views.FramedView;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;
import android.widget.GridView;
//import android.support.v4.app.NavUtils;

public class AlchemyActivity extends MWBaseActivity {
	private InventoryItemsAdapter invAdapter;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alchemy);
        final GridView inventoryItems = (GridView)findViewById(R.id.ingrediantsView);
        
        Menubutton backButton = (Menubutton) findViewById(R.id.alchemy_backButton);
        backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
        
        Menubutton cleanButton = (Menubutton) findViewById(R.id.alchemy_clearButton);
        cleanButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				resetView();
			}
		});
        
		invAdapter = new InventoryItemsAdapter(this);
		inventoryItems.setAdapter(invAdapter);
		inventoryItems.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				if (!invAdapter.canUse(position)) return;
				
				LinearLayout mixingPot = (LinearLayout)findViewById(R.id.mixView);
				InventoryItem item = invAdapter.getAtPosition(position);
				
				if (mixingPot.getChildCount() < 3) {
					View v = getViewForItem(item);
					mixingPot.addView(v);
				}
				else return;
				
				invAdapter.setUsed(position); 
				GridView sv = (GridView)findViewById(R.id.ingrediantsView);
				sv.invalidateViews();
				
				List<String> ing = new LinkedList<String>();
				List<CollectPlant> items = invAdapter.getSelectedItems();
				for (CollectPlant usedItem : items) {
					ObjectInfo iInfo = usedItem.getInfo();
					String itm = iInfo.getID();
					ing.add(itm);
				}
				
				List<String> pSpells = AllSpells.get().possiblePotions(ing);
				
				LinearLayout vSpel = (LinearLayout)findViewById(R.id.resultView);
				vSpel.removeAllViews();
				for (String spell : pSpells) {
					View v = getViewForResult(spell);
					vSpel.addView(v);
				}
				
				vSpel.invalidate();
				
			}
		});
        
    }
    
	private void resetView() {
		final LinearLayout mixingPot = (LinearLayout)findViewById(R.id.mixView);
		//final GridView sv = (GridView)findViewById(R.id.ingrediantsView);
		final LinearLayout vSpel = (LinearLayout)findViewById(R.id.resultView);
		
		invAdapter.clearAllItems();
		invAdapter.notifyDataSetChanged();
		
		mixingPot.removeAllViews();
		mixingPot.invalidate();
		
		vSpel.removeAllViews();
		vSpel.invalidate();
	}
    
	public View getViewForResult(final String potionName) {
		View picView = FramedView.getFramedView(null);
		
		ObjectInfo sInfo = ObjectInfoFactory.get().getInfoStandarized(potionName);
		
		//Drawable visPic = getResources().getDrawable(sInfo.getImageID());
		
		ImageView image = (ImageView) picView.findViewById(R.id.inventory_item_image);
		//ImageView picView = new ImageView(this);
		image.setImageResource(sInfo.getImageID());
		
		//picView.setScaleType(ScaleType.FIT_CENTER);
		
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100, 100);
		layoutParams.setMargins(5, 5, 5, 5);
		picView.setLayoutParams(layoutParams);
		
		picView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				askCreatePotion(potionName);
			}
		});
		
		return picView;
	}
	
	public void askCreatePotion(final String name) {
		final ObjectInfo hInfo = ObjectInfoFactory.get().getInfoStandarized(name);
			//SpellFactory.getSpellByID(name, Game.getGame().playerEgo).getInfo();
			//ObjectInfoFactory.get().getInfo(name);
		
    	AlertBulider ab = new AlertBulider() {
			@Override
			public void onYesClicked() {
				makePotion(name);
			}
			@Override
			public void onNoClicked() {	}
		};
		ab.message= this.getString(R.string.potion_create);
		ab.objName= hInfo.getName();
		AlertActivity.showNewAlert(this, ab);
		
		
		/*AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		helpBuilder.setTitle(this.getString(R.string.potion_create));
		helpBuilder.setMessage(hInfo.getName());
		
		LinearLayout layunder = new LinearLayout(this);
		layunder.setOrientation(LinearLayout.HORIZONTAL);
		Drawable visPic = getResources().getDrawable(hInfo.getImageID());
		ImageView picView = new ImageView(this);
		picView.setImageDrawable(visPic);
		//layunder.addView(picView);		
		helpBuilder.setView(layunder);
		
		TextView txV = new TextView(this);
		if (hInfo.getImageID() != R.drawable.unknown) {
			txV.setText(hInfo.getDescription());
		} else {
			String id=this.getString(R.string.text_view_id);
			txV.setText(id+name);
		}
		layunder.addView(txV);
		String yes=this.getString(R.string.yes_button);
		helpBuilder.setPositiveButton(yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				makePotion(name);
			}
		});
		String no=this.getString(R.string.no_button);		
		helpBuilder.setNegativeButton(no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		
		AlertDialog runningDialog = helpBuilder.create();
		runningDialog.show();*/
	}
	
	private void makePotion(String name)	{
		Spell addSpell = SpellFactory.getSpellByID(name, Game.getGame().playerEgo); //(Castable)hInfo.newInstance();
		//System.out.println("CREATED MIXTURE WITH SPELL: "+addSpell.getClass());
		//Experience za stworzenie mikstury
		Player.getInst().addExperience(200);
		Game.getGame().getInventory().addMixture(new SpellToken(addSpell));
		Game.getGame().getAchivContainer().getAchivMixtures().addMixtureMade(name);
		
		List<String> usedPlants = new LinkedList<String>(); 
		List<CollectPlant> items = invAdapter.getSelectedItems();
		for (CollectPlant item : items) {
			Game.getGame().getInventory().removePlant(item);
			usedPlants.add(item.getNameID());
		}
		addSpell.emitEventCreatedByAlchemy(usedPlants);
		Game.saveGame();
		resetView();
	}
	
	public View getViewForItem(InventoryItem item) {
		ObjectInfo oInfo = item.getInfo();
		
		//Drawable visPic = getResources().getDrawable();
		ImageView picView = new ImageView(this);
		//picView.setImageDrawable(visPic);
		picView.setImageResource(oInfo.getImageID());
		
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100, 100);
		picView.setLayoutParams(layoutParams);

		picView.setScaleType(ScaleType.FIT_CENTER);
		picView.setPadding(5, 5, 5, 5);
				
		return picView;
	}
	
    @Override
    public void finish() {
      setResult(RESULT_OK, new Intent());
      super.finish();
    } 

    
}
