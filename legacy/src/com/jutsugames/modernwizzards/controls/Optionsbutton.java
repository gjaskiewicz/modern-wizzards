package com.jutsugames.modernwizzards.controls;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.jutsugames.modernwizzards.R;

public class Optionsbutton extends AnimateAndRunButton {
	public Optionsbutton(Context context, AttributeSet attrs) {
		super(context, attrs, R.anim.optionsclick);
	}
}
