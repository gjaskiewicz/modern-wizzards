package com.jutsugames.modernwizzards.controls;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;

import com.jutsugames.modernwizzards.ModernWizzardsActivity;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.util.SoundsUtil;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class Menubutton extends AnimateAndRunButton {
	private static String TAG = "Menubutton";
	private static int border;
	
	//private Animation notifyAnim;
	
	private OnClickListener setListener;
	
	private boolean notificationOn;
	public boolean isDown;
	//private Bitmap notification;
	private Rect bitmapBcgSrcRect, bitmapBcgDstRect, 
		bitmapNormalSrcRect, bitmapNormalDstRect;
	
	private static int bitmapNormalID = R.drawable.button_normal;
	private static Bitmap bitmapBcg,bitmapNormal,bitmapDown;
	private static Typeface typeface;
	private int extraImg = 0;
	private Bitmap bitmapExtra;
	private Rect extraRect;
	
	Paint paintTxt, paintNotify; 
	BitmapFactory.Options myOptions;


	
	public Menubutton(Context context, AttributeSet attrs) {
		super(context, attrs, R.anim.menuclick);
		notificationOn = false;
		bitmapNormalID = R.drawable.button_normal;
		//attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "src");
		paintTxt = new Paint();
		paintTxt.setColor(Color.BLACK);
		paintNotify = new Paint();
		paintNotify.setColor(Color.RED);
		if(!isInEditMode()) {
			if(typeface==null)	
				typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/balham.otf");
			paintTxt.setTypeface(typeface);
		}
		paintTxt.setAntiAlias(true);
		paintTxt.setTextAlign(Align.CENTER);
		
		border=(int)(ModernWizzardsActivity.tenDPinPixels*4/10);

		myOptions = new BitmapFactory.Options();
		myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//important
		
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction()==MotionEvent.ACTION_DOWN)	{
			Animation anim = new ScaleAnimation(1.0f, 0.9f, 1.0f, 0.9f,
					Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
					0.5f);
			anim.setInterpolator(new LinearInterpolator());
			anim.setDuration(700);
			anim.setRepeatCount(10000);
			anim.setRepeatMode(Animation.REVERSE);
			this.startAnimation(anim);
		}
		else if(event.getAction()==MotionEvent.ACTION_UP)	{
			this.getAnimation().setRepeatCount(0);
		}
		return super.onTouchEvent(event);
	}
	
	public void makeBitmaps()	{
		if(getWidth()==0 || getHeight()==0) return;
		//notification = BitmapFactory.decodeResource(getResources(), R.drawable.exclamation_mark_icon, myOptions);
		//notification = Bitmap.createScaledBitmap(notification, getHeight() -(border*2), getHeight()-(border*2), true);	
		//if(bitmapBcg!=null) bitmapBcg.recycle();
		if(bitmapBcg==null) bitmapBcg = BitmapFactory.decodeResource(getResources(), R.drawable.button_bcg,myOptions);
		//bitmapBcg = Bitmap.createScaledBitmap(bitmapBcg, getWidth(), getHeight(), true);
		//if(bitmapNormal!=null) bitmapNormal.recycle();
		if(isInEditMode())
			bitmapNormalID = R.drawable.button_normal;
		if(bitmapNormal==null) bitmapNormal = BitmapFactory.decodeResource(getResources(), bitmapNormalID ,myOptions);
		//bitmapNormal = Bitmap.createScaledBitmap(bitmapNormal, getWidth()-(border*2), getHeight()-(border*2), true);
		//if(bitmapDown!=null) bitmapDown.recycle();
		if(bitmapDown==null) bitmapDown = BitmapFactory.decodeResource(getResources(), R.drawable.button_down,myOptions);
		//bitmapDown = Bitmap.createScaledBitmap(bitmapDown, getWidth()-(border*2), getHeight()-(border*2), true);	
		if(extraImg!=0)	{
			bitmapExtra = BitmapFactory.decodeResource(getResources(), extraImg, myOptions);
			extraRect = new Rect(0,0,bitmapExtra.getWidth(), bitmapExtra.getHeight());
		}
		
	}
	
	public void setNotification(boolean isNotificationOn) {
		notificationOn = isNotificationOn;
		//if(isNotificationOn)
		//	animateForever();
		invalidate();
	}
	
	public void refreshText() {
		//textOverlay = getTextOverlay();
		invalidate();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		//super.onDraw(canvas);
		//canvas.drawBitmap(textOverlay, 0, 0, null);
		//$log.d(TAG,"Menubutton onDraw");
		
		if(notificationOn) 
			canvas.drawRect(0, 0, getWidth(), getHeight(), paintNotify);
		else if(bitmapBcg!=null) //canvas.drawBitmap(bitmapBcg, 0, 0, paintTxt);
			canvas.drawBitmap(bitmapBcg, bitmapBcgSrcRect, bitmapBcgDstRect, null);

		if(isDown) 
			canvas.drawBitmap(bitmapDown, bitmapNormalSrcRect, bitmapNormalDstRect, paintTxt);
		else if(bitmapNormal!=null) 
			canvas.drawBitmap(bitmapNormal, bitmapNormalSrcRect, bitmapNormalDstRect, paintTxt);
		
		if(bitmapExtra!=null)	{
			canvas.drawBitmap(bitmapExtra, extraRect, bitmapNormalDstRect, paintTxt);
		}
		
		canvas.drawText(text, (getMeasuredWidth() == 0 ? 350 : getMeasuredWidth())/2, (float)(getHeight()*8/10), paintTxt);
		
		//if(notificationOn) {canvas.drawBitmap(notification, getMeasuredWidth()-65.0f, 8.0f, paint);
	}

	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		//$log.d(TAG,"Size changed");
		paintTxt.setTextSize((float)(getHeight()*8/10));
		makeBitmaps();
		refreshText();
		bitmapBcgSrcRect = new Rect(0, 0, bitmapBcg.getWidth(), bitmapBcg.getHeight());
		bitmapBcgDstRect = new Rect(0, 0, w, h);
		if(bitmapNormal!=null)
			bitmapNormalSrcRect = new Rect(0, 0, bitmapNormal.getWidth(), bitmapNormal.getHeight());
		bitmapNormalDstRect = new Rect(border, border, w-border, h-border);
	}
	
	//@Override
	public void animatee()	{
		//Animation anim = AnimationUtils.loadAnimation(this.getContext(), R.anim.menuclick);
		Animation anim = new ScaleAnimation(1.0f, 0.9f, 1.0f, 0.9f,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setDuration(100);
		anim.setRepeatCount(1);
		anim.setRepeatMode(Animation.REVERSE);
		this.startAnimation(anim);
	}

	public void animateForever()	{
		this.animatee();
		this.getAnimation().setDuration(1000);
		this.getAnimation().setRepeatCount(Animation.INFINITE);
	}
	
	@Override
	public void setOnClickListener(OnClickListener innerListener) {
		setListener = innerListener;
		OnClickListener actualListener = new OnClickListener()	{
			@Override
			public void onClick(View v) {
				$log.d(TAG, "CLICKED: " + text);
				
				animatee();
				SoundsUtil.playSound(R.raw.button, getContext());
				setListener.onClick(null);
			}
		};
		super.setOnClickListener(actualListener);
	}
	
	public void setImage(int imageID) {
		extraImg = imageID;
		makeBitmaps();
	}
}
