package com.jutsugames.modernwizzards.controls;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import com.jutsugames.modernwizzards.R;

public class AnimateAndRunButton extends ImageView {
	
	//ImageView copy;
	protected String text;
	/*private OnClickListener action;
	private WhenAnimate actionType;
	
	public static enum WhenAnimate {
		IMMIDIATE,
		AFTERANIM
	}*/	
	
	/*public void setSpecialOnClicListener(OnClickListener actionToPerform, WhenAnimate actionType1)	{
		action = actionToPerform;
		actionType = actionType1;
	}*/

	public void setText(int newText) {
		setText(getContext().getResources().getString(newText));
		//text = replaceSpecialTags(getContext(), newText);
	}
	
	public void setText(String newText) {
		text = replaceSpecialTags(getContext(), newText);
	}
	
	public String getText() {
		return text;
	}
	
	/**
	 * If text matches some special tags then those tags will be replaced with resources from string values
	 */
	private String replaceSpecialTags(Context context, String text) {
		$log.d("AnimateAndRunButton", "In replaceSpecialTags");
		if (text == null)
			return "";
		
		if (text.equals("back")) {
			text = context.getString(R.string.back);
			$log.d("AnimateAndRunButton", "In replaceSpecialTags, back found");
		}
		else if (text.equals("use"))
			text = context.getString(R.string.use);
		return text;
	}

	public AnimateAndRunButton(final Context context, AttributeSet attrs, final int animationId) {
		super(context, attrs);
		//copy = new ImageView(context, attrs);
		if(attrs!=null)	{
			text = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "text");
			int textid = attrs.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "text", R.string.undefined);
			if(text!= null && text.substring(0,1).equals("@"))	{
				//text = "haha";
				//text.substring(8);
				//textid = getContext().getResources().getIdentifier(text.substring(0), "string", getContext().getPackageName() );
				text = getContext().getResources().getString(textid);
			}
		}
		text = replaceSpecialTags(context,text);
		//$log.d("AnimateAndRunButton", "In constructor, text = " + text);
		
		/*
		this.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Animation anim = AnimationUtils.loadAnimation(context, animationId);
				arg0.startAnimation(anim);
				$log.d("AnimateAndRunButton", "In onclick, text = " + text + " " );
				anim.setAnimationListener(new Animation.AnimationListener() {
					@Override
					public void onAnimationStart(Animation animation) {
						if(action!=null && actionType == WhenAnimate.IMMIDIATE)
							action.onClick(null);
					}
					@Override
					public void onAnimationRepeat(Animation animation) {
					}
					@Override
					public void onAnimationEnd(Animation animation) {
						copy.performClick();
						if(action!=null && actionType == WhenAnimate.AFTERANIM)
							action.onClick(null);
					}
				});
			}
		});*/
	}
}
