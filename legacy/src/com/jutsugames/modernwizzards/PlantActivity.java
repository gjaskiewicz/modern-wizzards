package com.jutsugames.modernwizzards;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.Inventory;
import com.jutsugames.modernwizzards.gamelogic.ItemsAggregation;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class PlantActivity extends MWBaseActivity {
	private static final String TAG = "PlantActivity";
	
	ListView plantView;
	ArrayAdapter<CollectPlant> plantAdapter;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        $log.d("PlantActivity", "PlantActivity starts");
        final Context ctx = this;
        setContentView(R.layout.plant_dialog);
        plantView = (ListView) findViewById(R.id.plantList);
        
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        //zaladuj liste plants do listView za pomoca adaptera
        List<CollectPlant> allPlants = GameContent.getInstance().getAllPlantsTypes();
        plantAdapter = new ArrayAdapter<CollectPlant>(this, R.layout.plant_item, allPlants )
        		{
        	private boolean adapterRegistered = false;
        	
        	@Override
        	public View getView(int position, View convertView, ViewGroup parent) {
        		
        		View row;
        		 
        		if (null == convertView) {
        			row = inflater.inflate(R.layout.plant_item, null);
        		} else {
        			row = convertView;
        		}
        		
        		CollectPlant ii = (CollectPlant) getItem(position);
        		
        		ImageView iv = (ImageView) row.findViewById(R.id.plant_item_image);
        		iv.setImageDrawable(getResources().getDrawable(ii.getInfo().getImageID()));
        		
        		TextView tv = (TextView) row.findViewById(R.id.plant_item_description);        		
        		tv.setText(ii.getInfo().getName());
        		
        		return row;
        	}    
        };
        
        plantView.setAdapter(plantAdapter);
        

        final Intent i = new Intent(this, PlantDetailViewActivity.class);
        
        plantView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				InventoryItem ii = (InventoryItem) GameContent.getInstance().getAllPlantsTypes().get(arg2);
				//i.putExtra("info_name", ii.getInfo().getID());
				PlantDetailViewActivity.toDisplay = ii;
				startActivityForResult(i, 0);
			}
         });          
    }
    
}
