package com.jutsugames.modernwizzards;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class MWBaseActivity extends Activity {
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
	        Game.setGlobalTopmostActivity(this);
	        super.onCreate(savedInstanceState);
	        ModernWizzardsActivity.logMem();
	    }

	    @Override
	    public void onResume() {
	    	Game.setGlobalTopmostActivity(this);
	    	$log.d("ACTIVITY", "SCREEN RESUMES: " + this.getClass().getName());
	    	super.onResume();
	    }
	    
	    @Override
	    public void onPause() {
	        //if (Game.getGlobalTopmostActivity() == this)
	        //	Game.setGlobalTopmostActivity(null);
	    	$log.d("ACTIVITY", "SCREEN PAUSES: " + this.getClass().getName());
	    	super.onPause();
	    	ModernWizzardsActivity.logMem();
	        
	    }
	    
	    @Override
	    public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    	Game.setGlobalTopmostActivity(this);
	    	super.onActivityResult(requestCode, resultCode, data);
	    }
}
