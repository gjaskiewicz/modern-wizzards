package com.jutsugames.modernwizzards.quests.display;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.HashMap;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.AllSpells;
import com.jutsugames.modernwizzards.CreatureDetailViewActivity;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.InventoryActivity;
import com.jutsugames.modernwizzards.InventoryDetailViewActivity;
import com.jutsugames.modernwizzards.MWBaseActivity;
import com.jutsugames.modernwizzards.MWMap2Activity;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.PlantActivity;
import com.jutsugames.modernwizzards.PlantDetailViewActivity;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.SpellsActivity;
import com.jutsugames.modernwizzards.Game.GameState;
import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.creatures.Warewolf;
import com.jutsugames.modernwizzards.gamelogic.mixtures.HealingPotion;
import com.jutsugames.modernwizzards.gamelogic.mixtures.ManaPotion;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import com.jutsugames.modernwizzards.quests.logic.Quest;
import com.jutsugames.modernwizzards.quests.logic.QuestsEngine;
import com.jutsugames.modernwizzards.util.ChangeListener.EventTypes;

public class MessageViewActivity extends MWBaseActivity {
	private static final String TAG = "MessageViewActivity";
	public final static String EXTRA_LAT = "com.jutsugames.modernwizzards.LATITUDE";//przekazywane MapActivity
	public final static String EXTRA_LON = "com.jutsugames.modernwizzards.LONGITUDE";//przekazywane MapActivity
	public static final String MESSAGE_TYPE = "message_type";
	public static final String MESSAGE_TYPE_NARRATION = "message_type_narration";
	public static final String MESSAGE_CANCLOSE = "message_canclose";
	
	private String result = null;
	private String questClassName;
	
	private WebView results;
	private String messageText;
	private String messageSender;
	private String messageTitle;
	private String messageType;
	private String messageSenderImg;
	private boolean canClose = true;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        $log.d(TAG, "onCreate");
        
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
          finish();
        }
        messageTitle = extras.getString("title");
        messageText = extras.getString("message");
        messageSender = extras.getString("from");
        messageSenderImg = extras.getString("fromimg");
        boolean isReadOnly = extras.getBoolean("readOnly");
        questClassName = extras.getString("questClassName");
        messageType = extras.getString(MESSAGE_TYPE);
        canClose = extras.getBoolean(MESSAGE_CANCLOSE);
        
    	setContentView(R.layout.quests_dialog_readonly);
    	//titleViewId = R.id.quest_title_readonly;
    	int messageViewId = R.id.quest_full_message_ro,
    		buttonBackId = R.id.ButtonBack_ro;
    	
    	$log.d(TAG, "setting button handlers");
        if(!isReadOnly) {
        	setContentView(R.layout.quests_dialog);
        	messageViewId = R.id.quest_full_message;
        	buttonBackId = R.id.ButtonBack;
        	
        	Menubutton button_yes = (Menubutton) findViewById(R.id.ButtonYes);
        	button_yes.setText(R.string.accept);
            button_yes.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                	result = "Accept";
                    finish();
                }});
            Menubutton button_no = (Menubutton) findViewById(R.id.ButtonNo);
            button_no.setVisibility(View.GONE);
            button_no.setText(R.string.reject);
            button_no.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                	result = "Reject";
                    finish();
                }});
        }
        
        ImageButton button_back = (ImageButton) findViewById(buttonBackId);
        if(!canClose)	{
        	button_back.setVisibility(View.GONE);
        }
        button_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }});
        
        //((TextView)findViewById(titleViewId)).setText(messageTitle);
        results = (WebView) findViewById(messageViewId);
        results.setWebViewClient(new MyWebViewClient());
        reloadViewWithMessage(messageText);      
        results.setBackgroundColor(0);
        
        if(MESSAGE_TYPE_NARRATION.equals(messageType))	{
        	setForNarration();
        }
        
        if(questClassName!=null)	{
        	Quest quest = QuestsEngine.getInstance().getQuestByClassName(questClassName);
            if(quest!=null) quest.openMessage();
        }
    }
    
    private void setForNarration() {
        String text = "<html><head><style> " +
        		"a { font-size : 15pt } " +
        		"p { font-size : 15pt }" +
        		"</style></head><body>"
                + "<p align=\"center\"><i>"                
                + messageText
                + "</i></p> "
                + "</body></html>";
        
        results.loadDataWithBaseURL(null, text, "text/html", "utf-8", null);
      //konieczne, zeby polskie znaki sie dobrze zaladowaly   
       findViewById(R.id.msg1).setBackgroundDrawable(null);     
	}

	void reloadViewWithMessage(String message) {
    	this.messageText = message;
        String text = "<html><head><style> " +
        		"a { font-size : 15pt } " +
        		"p { font-size : 15pt }" +
        		"</style></head><body>"
        		+ "<table><tr><td width=\"100%\">"
        		+"<p align=\"left\"><b>" + messageSender + "</b><br/>"
        	    +"<span style=\"font-size: 17pt\">" + messageTitle + "</span></p>"
        	    + "</td><td>"
        	    + messageSenderImg 
        		+ "</td</tr></table>"
        		//+ "<img src=\"file:///android_asset/item_garlic.jpg\" width=\"100%\"  >"
                + "<p align=\"justify\">"                
       /*         + "��柳��� <a href='plant/item_daisyflower' target='abc'>Daisy flower</a>"
                + " <a href='plant/item_fernflower' target='abc'>Fern flower</a>"
                + " <a href='plant/potion_health' target='abc'>Healing potion</a>"
                + " <a href='plant/potion_mana' target='abc'>Mana potion</a>"
                + " <a href='map/map1' target='abc'>Map</a> "
                + " <a href='map/" + (Options.MOCK_LAT+0.002) + "/" + Options.MOCK_LNG + "' target='abc'>Map</a> "*/
                + message
                + "</p> "
                + "</body></html>";
        
        results.loadDataWithBaseURL(null, text, "text/html", "utf-8", null);
        //konieczne, zeby polskie znaki sie dobrze zaladowaly        
    }
    
    @Override
    public void finish() {
      Intent data = new Intent(Game.Actions.QUEST_DECISION);
      data.putExtra("returnValue", result);
      data.putExtra("questClassName", questClassName);
      sendBroadcast(data);
      super.finish();
    }

    @Override
    public void onBackPressed() {
    	if(canClose)
    		super.onBackPressed();
    }
    
    //zmienia zachowanie klikniecia w link w tekscie
    class MyWebViewClient extends WebViewClient {
    	private final String PLANT_TAG="plant";//tag w linku oznaczajacy zeby wyswietlic rosline
    	private final String POTION_TAG="potion";//tag w linku oznaczajacy zeby wyswietlic miksture
    	private final String CREATURE_TAG="creature";//tag w linku oznaczajacy zeby wyswietlic miksture
    	private final String MAP_TAG="map";//tag w linku oznaczajacy przeskok do mapy
    	private final String INVENTORY_TAG="inventory";
    	private final String PLANTIARY_TAG="plantiary";
    	private final String MESSAGE_TAG="message";
    	private final String SPELLS_TAG="spells";
    	private final String OPENWEB_TAG="openweb";
    	
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        	url.replaceAll("\"�", "");
        	
        	$log.d(TAG, "url clicked " + url);
			String delims = "[/]+";
			String[] tokens = url.split(delims, 2);
			
			//odczytaj 1-czesc i 2-czesc zakodowana w urlu
			//Jarek: Jak juz mamy tokent to po kiego je jeszcze jakos dziwnie interpretowac?? 
			

			//odpowiednia akcja w zaleznosci od typu linka
			if( PLANT_TAG.equals(tokens[0]) ) {
				showPlant(tokens[1]);		
			} else if( POTION_TAG.equals(tokens[0]) ) {
				showPotion(tokens[1]);
			} else if( CREATURE_TAG.equals(tokens[0]) ) {
				showCreature(tokens[1]);
			} else if( MAP_TAG.equals(tokens[0]) ) {
				//wyciete: Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2]);
				showMap(0,0);//na razie tylko wyswietla mape bez konkretnego punktu
			} else if ( INVENTORY_TAG.equals(tokens[0])) {
				showInventory();
			} else if ( MESSAGE_TAG.equals(tokens[0]) ) {
				sendMessage(tokens[1]);
			} else if ( SPELLS_TAG.equals(tokens[0]) ) {
				showSpells(tokens[1]);
			} else if ( PLANTIARY_TAG.equals(tokens[0]) ) {
				showPlantiary();
			} else if ( OPENWEB_TAG.equals(tokens[0])) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(tokens[1])));
			}

			return true; //klikniecie nie ma zadnego skutku, jest przeladowane
        }
        
        private void showPlantiary() {
        	startActivity(new Intent(getBaseContext(), PlantActivity.class));
		}

		private void showSpells(String string) {
			//Game.getGame().mainActivity.gotoSpells(null);
			startActivity(new Intent(getBaseContext(), SpellsActivity.class));
		}

		//wyswietla informacje o roslinie, po nazwie
        public void showPlant(String plantName) {
            final Intent i = new Intent(getBaseContext(), PlantDetailViewActivity.class);
            CollectPlant a = new CollectPlant(plantName);
			PlantDetailViewActivity.toDisplay = (InventoryItem) a;
			startActivityForResult(i, 0);            
        	return;
        }        
        
        //wyswietla informacje o stworze, po nazwie
        public void showCreature(String creatureName) {
            final Intent i = new Intent(getBaseContext(), CreatureDetailViewActivity.class);
            
            Creature a = null;
            for(Creature c : GameContent.getInstance().getAllCreaturesTypes())	{
            	if(c.nameID.equals(creatureName))	{
            		a = c;
            		break;
            	}
            }
            
			CreatureDetailViewActivity.toDisplay = a;
			startActivityForResult(i, 0);            
        	return;
        }         
        
        //wyswietla informacje o miksturze, po umownej nazwie, ktora jest mapowana na klase mikstury
        public void showPotion(String potionName) {
            final Intent i = new Intent(getBaseContext(), InventoryDetailViewActivity.class);
            SpellToken a;
            a = new SpellToken(new HealingPotion());
            //bo kompilator nie pozwala jezeli jest ryzyko, ze nie bedzie zainicjalizowana
			
			//jezeli nazwa mikstury byla poprawna	            
			if(potionName.equals("potion_health") || potionName.equals("potion_mana"))
			{
			if(potionName.equals("potion_health"))
			{
				 a = new SpellToken(new HealingPotion());		
			} else if(potionName.equals("potion_mana"))
			{
				 a = new SpellToken(new ManaPotion());
			}
	
			InventoryDetailViewActivity.toDisplay = (InventoryItem) a;
			startActivityForResult(i, 0);  
			}
			
        	return;
        }            
        
        //wyswietla mape
        public void showMap(double lat, double lon) {
        	//przekopiowane z goToMap z klasy ModernWizzardsActivity i dostosowane
        	//Jarek: Nu nu nu - nie ladnie jest kopiowac kod
        	if(Game.getGame().getGameState()==GameState.STATE_INIT)	{
        		MWToast.showToast(R.string.game_gps_loc_wait, Toast.LENGTH_SHORT);
        		return;
        	}
        	else {
        		$log.d(TAG, "start MWMapActivity");
        		Intent i = new Intent(getBaseContext(), MWMap2Activity.class);
    			//przekaz pozycje
        		//TODO: Tymczasowo jest centrize, ale nie sprawdzi sie przy kilku questach naraz.
        		i.putExtra("centrize", true);
    			//i.putExtra(EXTRA_LAT,lat);
    			//i.putExtra(EXTRA_LON,lon);
        		startActivity(i);
        	}
        	return;
        }
        
        public void showInventory() {
        	//InventoryActivity.fightMode = false;
    		startActivity(new Intent(getBaseContext(), InventoryActivity.class));
        }
        
        public void sendMessage(String message) {
        	HashMap<String, Object> extraParams = new HashMap<String, Object>();
        	extraParams.put("message", message);
		 	Intent i = new Intent("com.jutsugames.modernwizzards.EVENT").
		 				putExtra("objID", TAG).
		 				putExtra("eventType", EventTypes.MESSAGE.toString()).
		 				putExtra("extraParams", extraParams);
			Game.mainActivity.sendBroadcast(i);
			finish();
        }
    }
}
