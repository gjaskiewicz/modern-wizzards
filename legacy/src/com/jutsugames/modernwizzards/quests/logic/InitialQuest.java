package com.jutsugames.modernwizzards.quests.logic;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import android.content.Intent;
import android.os.CountDownTimer;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.mixtures.InitialPotion;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import com.jutsugames.modernwizzards.util.GameStateToSave;
import com.jutsugames.modernwizzards.util.SavesStorage;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

public class InitialQuest extends Quest {
	private static final String TAG = "InitialQuest";

	
	@Override
	public void initateQuest() {
		title_ = ctx_.getResources().getString(R.string.quests_initial_message_title);
		from_ = ctx_.getResources().getString(R.string.quests_initial_message_from);
		fromimg_ = ctx_.getResources().getString(R.string.quest_profile_bf);
		
		setBasicMessage(R.string.quests_initial_message_content);
		
		setFullMessageText(getMessageWithNarrations());
		
		setAccepted();
		Game.getGame().showNewMessageAlert(this);
	}
	
	@Override
	public void initiateQuestBackground() {
		SpellToken initialPotion = new SpellToken(new InitialPotion());
		
		if (Game.getGame().playerEgo.inv.getMixturesCountByName(initialPotion.getNameID()) == 0)
			Game.getGame().playerEgo.inv.addMixture(initialPotion);
	}
	
	@Override
	public void onMessageOpen() {
		super.onMessageOpen();
		if(progressState_ == ProgressState.INPROGRESS) {
			Game.turnOnOffNotificationForButton(R.id.btnInventory, true);
		}
	}
	
	@Override
	public Set<PostProcessingRequestType> handleEvent(String objID, EventTypes et,
			Map<String, Object> extraParams) {
		super.handleEvent(objID, et, extraParams);
		$log.d(TAG,"Handling event");
		if(acceptanceState_ == AcceptanceState.ACCEPTED && 
				progressState_ == ProgressState.INPROGRESS && 
				"potion_initial".equals(objID) &&
				et == EventTypes.USED) {
			//komentuje bo to narracja, nie wiadomosc. 
			//completedSteps_.add(new Step(1,new Date(),"narration_initial_potion_effect")); 
			//Drinking initial potion is an only step in this quest
			
			showNarrationMessage(R.string.narration_initial_potion_effect);
			
			setFullMessageText(getMessageWithNarrations());
			
			Player.getInst().addExperience(100);
			setCompleted();
			Game.saveGame();
		}
		return null;
	}

}
