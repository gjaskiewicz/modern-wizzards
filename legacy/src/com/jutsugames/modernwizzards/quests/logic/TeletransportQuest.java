package com.jutsugames.modernwizzards.quests.logic;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.content.Intent;
import android.os.CountDownTimer;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.creatures.Bat;
import com.jutsugames.modernwizzards.gamelogic.creatures.Bunny;
import com.jutsugames.modernwizzards.gamelogic.mixtures.ManaPotion;
import com.jutsugames.modernwizzards.gamelogic.mixtures.UnknownPotion;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.logic.Quest.AcceptanceState;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.quests.logic.Quest.Step;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

public class TeletransportQuest extends Quest {
	private transient static final String TAG = "TeletransportQuest";
	private transient final String pickUpZoneID = "PickUpZone";
	private transient final String returnZoneID = "ReturnZone";
	private transient TriggerZone pickUpZone;
	private transient TriggerZone returnZone;
	private transient final int amountOfBats = 8;
	private transient Random rnd;
	private transient List<Bat> bats = new LinkedList<Bat>();

	
	@Override
	public void initateQuest() {
		title_ = ctx_.getResources().getString(R.string.quest_teletransport_title);
		from_ = ctx_.getResources().getString(R.string.quest_teletransport_from);
		fromimg_ = ctx_.getResources().getString(R.string.quest_profile_william);
		
		setBasicMessage(R.string.quest_teletransport_initial);
		
		setFullMessageText(getMessageWithNarrations());

		setAccepted();
		
		Game.getGame().showNewMessageAlert(this);
		
		Game.saveGame();
	}
	
	@Override
	public void initiateQuestBackground() {
		rnd = new Random();
		
		GeoPoint gp_player = Game.getGame().playerEgo.getGeoPoint();
		
		//Delete potions in case of restoring
		cleanQuestRemains();
		
		pickUpZone = new TriggerZone(pickUpZoneID, 
				new GeoPoint(
					gp_player.getLatitudeE6(), 
					gp_player.getLongitudeE6() ), 
				30, ctx_.getResources().getString(R.string.target));
		pickUpZone.moveObject(200.0 + rnd.nextInt(100), rnd.nextInt(360));
		
		Game.getGame().worldContainer.addQuestObject(pickUpZone, true);
		pickUpZone.display(true);
	}
	
	@Override
	public boolean tryToRestoreQuest() {
		switch(completedSteps_.size()) {
			case 0:
				initiateQuestBackground();
				return true;
			case 1:
				handlePackageReceived();
				return true;
			default:
				return false;
				
		}
	}
	
	@Override
	public void cleanQuestRemains() {
		deleteQuestPotions();
	}
	
	@Override
	public Set<PostProcessingRequestType> handleEvent(String objID, EventTypes et,
			Map<String, Object> extraParams) {
		super.handleEvent(objID, et, extraParams);
		
		if(acceptanceState_ != AcceptanceState.ACCEPTED || 
				progressState_ != ProgressState.INPROGRESS) return null;
		
		if(		et == EventTypes.VISITED &&
				pickUpZoneID.equals(objID) ) { 
			
			Game.getGame().worldContainer.removeQuestObject(pickUpZone);
			pickUpZone.display(false);
			
			completedSteps_.add(new Step(1,new Date(),"quest_teletransport_narration_package_received"));
			showNewInQuestMessage(R.string.quest_teletransport_narration_package_received);
			setFullMessageText(getMessageWithNarrations());
			
			handlePackageReceived();
			
			Game.saveGame();
			Set<PostProcessingRequestType> result = getResultSet(PostProcessingRequestType.REFRESHMAP);
			result.add(PostProcessingRequestType.REFRESHINVENTORY);
			return result;

		} else if(	et == EventTypes.VISITED &&
					returnZoneID.equals(objID)) { //Player killed bat and is in return zone
			
			//time
			completedSteps_.add(new Step(2 ,new Date(), "quest_teletransport_narration_end"));
			showNewInQuestMessage(R.string.quest_teletransport_narration_end, 0,0);
			setFullMessageText(getMessageWithNarrations());
			
			for(Bat bat : bats)
				Game.getGame().worldContainer.removeQuestObject(bat);
			
			Game.getGame().worldContainer.removeQuestObject(returnZone);
 			
			deleteQuestPotions();
			
			GameContent.getInstance().knownCreatures.add("creature_bat");
			//TODO: poni�sze wstawione a� sie pojawi� questy poboczne
			GameContent.getInstance().knownCreatures.add("creature_wraith");
			GameContent.getInstance().knownCreatures.add("creature_warewolf");
			Player.getInst().addExperience(700);
			setCompleted();
			Game.saveGame();
			
			return getResultSet(PostProcessingRequestType.REFRESHMAP);
		}
		return null;
	}
	
	@Override
	public void onMessageOpen() {
		super.onMessageOpen();
		if(progressState_ == ProgressState.INPROGRESS && 
				completedSteps_.size() == 0) {
			$log.d(TAG, "Message opened sending broadcast");
			Game.turnOnOffNotificationForButton(R.id.btnMap, true);
		}
	}
	

	
	private void handlePackageReceived() {
		deleteQuestPotions(); //Erase old potions in case of restoring quest
		createQuestPotions();
		
		rnd = new Random();
		GeoPoint gp_player = Game.getGame().playerEgo.getGeoPoint();
		
		returnZone = new TriggerZone(returnZoneID, 
				new GeoPoint(
					gp_player.getLatitudeE6(), 
					gp_player.getLongitudeE6() ), 
				30, ctx_.getResources().getString(R.string.target));
		int distanceToReturnZone = 300 + rnd.nextInt(100);
		int bearingToReturnZone = rnd.nextInt(360);
		returnZone.moveObject(distanceToReturnZone, bearingToReturnZone);
			
		//middle bat
		Bat middlebat = new Bat();
		middlebat.setLocation(new GeoPoint(	gp_player.getLatitudeE6(),
										gp_player.getLongitudeE6()) );
		middlebat.moveObject(distanceToReturnZone/2, bearingToReturnZone);
		middlebat.display(true);
		Game.getGame().worldContainer.addQuestObject(middlebat, false);
		bats.add(middlebat);
		
		for(int i=1; i < amountOfBats; ++i) {
			Bat bat = new Bat();
			//bat.attackWithoutWarning = true;
			bat.setLocation(new GeoPoint( 
					middlebat.getGeoPoint().getLatitudeE6(),
					middlebat.getGeoPoint().getLongitudeE6()) );
			double distanceToNewBat = distanceToReturnZone * (0.8+rnd.nextDouble()/5) /2 ;
			int bearingToNewBat = (i * (360/(amountOfBats-1))) + rnd.nextInt(20);
			//$log.d(TAG, "New bat distance: " + distanceToNewBat + " bearing: " + bearingToNewBat);
				bat.moveObject(distanceToNewBat, bearingToNewBat);
			bat.display(true);
			//if(i==0)
			//	bat.secondsNotToAttack = 10;
			Game.getGame().worldContainer.addQuestObject(bat, false);
			bats.add(bat);
		}
		//Return zone is added at the end to be checked after bats in collection of otherObjects
		Game.getGame().worldContainer.addQuestObject(returnZone, true);
		returnZone.display(true);
	}
	
	private void deleteQuestPotions() {
		List<SpellToken> spellTokensToDelete = new LinkedList<SpellToken>();
		for(SpellToken spellToken : Player.getInst().inv.getMixtures() ) {
			if(spellToken.isBlocked() && "potion_unknown".equals(spellToken.getSpell().getInfo().getID()))
				spellTokensToDelete.add(spellToken);
		}
		for(SpellToken spellToken : spellTokensToDelete)
			Player.getInst().inv.removeMixture(spellToken);
	}

	private void createQuestPotions() {
		for(int i =0; i < 1; i++) {
			SpellToken st = new SpellToken(new UnknownPotion());
			st.setBlocked(true);
			Game.getGame().playerEgo.inv.addMixture(st);
		}
	}
}
