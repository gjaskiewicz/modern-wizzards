package com.jutsugames.modernwizzards.quests.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.content.Intent;
import android.os.CountDownTimer;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.AllSpells;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.ModernWizzardsActivity;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.spells.Telekinesis;
import com.jutsugames.modernwizzards.gis.SphericalZone;
import com.jutsugames.modernwizzards.objects.PlantFactory;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.logic.Quest.AcceptanceState;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.quests.logic.Quest.Step;
import com.jutsugames.modernwizzards.util.MoveLocation;
import com.jutsugames.modernwizzards.util.ChangeListener.EventTypes;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class CollectPlantsQuest extends Quest {
	private static final String COLLECT_PLANT_QUEST_ZONE = "CollectPlantQuestZone";
	private static final String TAG = "CollectPlantsQuest";
	private transient Map<String, List<CollectableItem>> plantsZones;
	private transient List<TriggerZone> triggeredZonesAbovePlants;
	private transient final String[] items_types_to_collect =  {"item_daisyflower", "item_4clover", "item_swallowweed", "item_garlic" };
	private transient final String targetZoneID = "CollectPlantsQuestTargetZone";
	private transient TriggerZone targetZone;
	private transient Random rnd;
	private boolean firstNarrationShown = false;
	private List<String> collectedPlants = new ArrayList<String>();
	
	
	@Override
	public void initateQuest() {
		title_ = ctx_.getResources().getString(R.string.collectplants_quest_title);
		from_ = ctx_.getResources().getString(R.string.collectplants_quest_from);
		fromimg_ = ctx_.getResources().getString(R.string.quest_profile_william);
		
		setBasicMessage(R.string.collectplants_quest_content);
		
		setFullMessageText(getMessageWithNarrations());

		setAccepted();

		Game.getGame().showNewMessageAlert(this);
	}
	
	@Override
	public void initiateQuestBackground() {
		rnd = new Random();
		
		plantsZones = new HashMap<String, List<CollectableItem>>();
		triggeredZonesAbovePlants = new LinkedList<TriggerZone>();
	
		GeoPoint gp_player = Game.getGame().playerEgo.getGeoPoint();
		
		int angleD=0;
		for(String plantType : items_types_to_collect) 
		{
			if(collectedPlants.contains(plantType)) continue; //Skip already collected items. Meaning during restore.
			
			LinkedList<CollectableItem> tmpPlantsColl = new LinkedList<CollectableItem>();
			angleD++;
			GeoPoint gp = MoveLocation.moveGeoPoint(gp_player, 100.0+rnd.nextInt(100), 90*angleD+rnd.nextInt(90));
			CollectableItem plant = new CollectableItem(
					new SphericalZone(gp, 30+(rnd.nextInt(15) ) ), new CollectPlant(plantType));
	
			
			Game.getGame().worldContainer.addQuestObject(plant, true);
			plant.display(false);
			if(!firstNarrationShown) {
				TriggerZone tz = new TriggerZone(	COLLECT_PLANT_QUEST_ZONE, 
													gp, 
													60, 
													ctx_.getResources().getString(R.string.target));
				tz.display(true);
				
				triggeredZonesAbovePlants.add(tz);
				Game.getGame().worldContainer.addQuestObject(tz, true);
			} else {
				plant.display(true);
			}
			
			tmpPlantsColl.add(plant);
			
			plantsZones.put(plantType, tmpPlantsColl);
		}

	}
	
	@Override
	public boolean tryToRestoreQuest() {
		if (progressState_ != ProgressState.INPROGRESS)
			return false;

		if(completedSteps_.size() < items_types_to_collect.length) {
			initiateQuestBackground();
			if(firstNarrationShown) makePlantsVisible();
			return true;
		} else if (completedSteps_.size() == items_types_to_collect.length) { //All plants collected setup destination point
			initiateQuestBackground();
			setupDestinationPoint();
			return true;
		}
		return false;
	}

	@Override
	public Set<PostProcessingRequestType> handleEvent(String objID, EventTypes et,
			Map<String, Object> extraParams) {
		super.handleEvent(objID, et, extraParams);
		
		if(acceptanceState_ != AcceptanceState.ACCEPTED || 
				progressState_ != ProgressState.INPROGRESS) return null;
		
		if(		COLLECT_PLANT_QUEST_ZONE.equals(objID) && 
				et == EventTypes.VISITED && 
				firstNarrationShown  == false) {
			firstNarrationShown = true;
			
			clearTriggeredZonesAbovePlants();
			
			makePlantsVisible();
			
			showNarrationMessage(R.string.collectplants_narration_close_to_plant);
			
			Game.saveGame();
			
			return getResultSet(PostProcessingRequestType.REFRESHMAP);
		
		} else if(	et == EventTypes.COLLECTED && 
					completedSteps_.size() < items_types_to_collect.length) {
			
			$log.d(TAG, "Plant collected " + objID);
			collectedPlants.add(objID);
			
			for( CollectableItem plant : plantsZones.get(objID) ) {
				plant.display(false);
				Game.getGame().worldContainer.removeQuestObject(plant);
			}
				
			plantsZones.remove(objID);
			int plantsZonesSize =  plantsZones.size();
			
			Step step = new Step(items_types_to_collect.length - plantsZonesSize,new Date(),null);
			completedSteps_.add(step);
			
			if(completedSteps_.size() == items_types_to_collect.length/2) { //Player collected half of requested plants
				"collectplants_narration_use_telekinesis".equals(step.narrationTextName);
				showNarrationMessage(R.string.collectplants_narration_use_telekinesis);
				setFullMessageText(getMessageWithNarrations());
				Game.getGame().playerEgo.addKnownSpell("spell_telekinesis");
				
				Game.turnOnOffNotificationForButton(R.id.btnSpells, true);
			}	
			
			if(completedSteps_.size() == items_types_to_collect.length) { //Last plant collected
				
				clearTriggeredZonesAbovePlants();

				//dodajemy wiadomosc gdzie ma sie udac
				completedSteps_.add(new Step(items_types_to_collect.length ,new Date(), "collectplants_narration_all_collected"));
				showNewInQuestMessage(R.string.collectplants_narration_all_collected);
				setupDestinationPoint();
			}
			
			Game.saveGame();	 

		} else 	if(	et == EventTypes.VISITED &&
					//completedSteps_.size() == items_types_to_collect.length+1 &&
					Game.getGame().playerEgo.inv.getPlants().size() >= items_types_to_collect.length &&
					targetZoneID.equals(objID)) {
			completedSteps_.add(new Step(items_types_to_collect.length+1 ,new Date(), "collectplants_narration_end"));
			
			//TODO: zast�pczo dodane wszystkie ro�liny.
			LinkedList<String> allPlantsTypes = new LinkedList<String>();
			for(CollectPlant ci : GameContent.getInstance().getAllPlantsTypes())	{
				allPlantsTypes.add(ci.getNameID());
			}
			GameContent.getInstance().knownPlants.addAll(allPlantsTypes);
			
			removeQuestPlants();
			
			targetZone.display(false);
			Game.getGame().worldContainer.removeQuestObject(targetZone);
			
				
			showNewInQuestMessage(R.string.collectplants_narration_end, 0, 0);
				
			setFullMessageText(getMessageWithNarrations());
			
			Player.getInst().addExperience(300);
			setCompleted();
			Game.saveGame();
		}
		return null;
		
	}

	private void removeQuestPlants() {
		for(String plantName : items_types_to_collect) {
			CollectPlant plant = Game.getGame().playerEgo.inv.getPlantByName(plantName);
			if(plant != null) 
				Game.getGame().playerEgo.inv.removePlant(plant);
			else
				$log.e(TAG, "Can't find plant [" + plantName + "] in the player's inventory");
		}
	}

	private void setupDestinationPoint() {
		
		targetZone = new TriggerZone(targetZoneID, Player.getInst().getGeoPoint(), 
				40,
				ctx_.getResources().getString(R.string.target) );
		targetZone.moveObject(120.0 + rnd.nextInt(80), rnd.nextInt(360));
		targetZone.display(true);
		Game.getGame().worldContainer.addQuestObject(targetZone, true);
	}

	private void clearTriggeredZonesAbovePlants() {
		for( TriggerZone tz : triggeredZonesAbovePlants) {
			Game.getGame().worldContainer.removeQuestObject(tz);
			tz.display(false);
		}
		triggeredZonesAbovePlants.clear();
	}

	private void makePlantsVisible() {
		for( List<CollectableItem> pl : plantsZones.values()) {
			for( CollectableItem plant : pl ) {
				plant.display(true);
			}
		}
	}
	
	@Override
	public void onMessageOpen() {
		super.onMessageOpen();
		if(progressState_ == ProgressState.INPROGRESS && 
				completedSteps_.size() < items_types_to_collect.length/2) {
			Game.turnOnOffNotificationForButton(R.id.btnMap, true);
		}
	}
}
