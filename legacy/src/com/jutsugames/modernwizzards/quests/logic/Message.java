package com.jutsugames.modernwizzards.quests.logic;

import java.util.Date;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.ModernWizzardsActivity;
import com.jutsugames.modernwizzards.R;

import android.content.Context;
import android.content.Intent;

public class Message {
	protected static Context ctx_;
	public static void initializeContext(Context ctx) {
		ctx_ = ctx;
	}
	
	
	public static enum MessageState {
		UNREAD,
		READ,
		REPLIED,
		DELETED
	};
	protected String title_;
	protected String from_;
	protected String fromimg_;
	protected String message_;

	public MessageState state;
	public Date received;
	
	//dla ci�g�w wiadomo�ci / w�tk�w.
	public Message previusMessage;
	
	public Message()	{
		//Game.getGame().showNewMessageAlert(this);
	}
	
	public void setTitle(String title) {
		title_ = title;
	}
	
	public String getTitle() {
		return title_;
	}
	
	public void setFrom(String from) {
		from_ = from;
	}
	
	public String getFrom() {
		return from_;
	}
	
	public String getFromImg()	{
		return fromimg_;
	}
	
	public void setFullMessageText(String message) {
		message_ = message;
	}
	
	public String getFullMessageText() {
		return message_;
	}
	
	public void openMessage() {
		if(state==MessageState.UNREAD)	{
			state = MessageState.READ;
			onMessageOpen();
			
			Intent i = new Intent(Game.Actions.MESSAGE_OPENED).putExtra("className", this.getClass().getSimpleName());
			ctx_.sendBroadcast(i);
		}
	}
	
	public void onMessageOpen() {
	}
	
	public boolean isAlreadyRead() {
		return state == MessageState.READ;
	}

}
