package com.jutsugames.modernwizzards.quests.logic;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.content.Intent;
import android.os.CountDownTimer;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.creatures.Bunny;
import com.jutsugames.modernwizzards.gamelogic.creatures.Gnome;
import com.jutsugames.modernwizzards.gamelogic.mixtures.InitialPotion;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.quests.logic.Quest.Step;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

public class SealsQuest extends Quest {
	private static final String TAG = "SealsQuest";
	private transient final String puzzlingGnomeZoneID = "puzzlingGnomeZone";
	private transient TriggerZone puzzlingGnomeZone;
	private transient Gnome puzzlingGnome;
	private transient Random rnd;
	
	@Override
	public void initateQuest() {
		title_ = ctx_.getResources().getString(R.string.quest_seals_title);
		from_ = ctx_.getResources().getString(R.string.quest_seals_from);
		fromimg_ = ctx_.getResources().getString(R.string.quest_profile_anette);
		
		setBasicMessage(R.string.quest_seals_initial);
		
		setFullMessageText(getMessageWithNarrations());

		setAccepted();
		
		Game.getGame().showNewMessageAlert(this);
	}
	
	@Override
	public void initiateQuestBackground() {
		rnd = new Random();
		
		GeoPoint gp_player = Game.getGame().playerEgo.getGeoPoint();
		
		puzzlingGnomeZone = new TriggerZone(puzzlingGnomeZoneID, 
				new GeoPoint(
					gp_player.getLatitudeE6(), 
					gp_player.getLongitudeE6() ), 
				50, ctx_.getResources().getString(R.string.target));
		puzzlingGnomeZone.moveObject(150.0 + rnd.nextInt(100), rnd.nextInt(360));
		
		Game.getGame().worldContainer.addQuestObject(puzzlingGnomeZone, true);
		puzzlingGnomeZone.display(true);
		puzzlingGnome = new Gnome();
	}
	
	@Override
	public Set<PostProcessingRequestType> handleEvent(String objID, EventTypes et,
			Map<String, Object> extraParams) {
		super.handleEvent(objID, et, extraParams);
		if(acceptanceState_ != AcceptanceState.ACCEPTED || 
				progressState_ != ProgressState.INPROGRESS) return null;
		
		if(		et == EventTypes.VISITED &&
				puzzlingGnomeZoneID.equals(objID) ) { //Player reached triggered zone.
			
			Game.getGame().worldContainer.removeQuestObject(puzzlingGnomeZone);
			puzzlingGnomeZone.display(false);
			
			completedSteps_.add(new Step(1 ,new Date(),null));
			
			showNarrationMessage(R.string.quest_seals_narration_close_gnome);
		 	
		} else if(	et == EventTypes.MESSAGE &&
					extraParams.containsKey("message") ) {
			
			if("fight".equals((String)extraParams.get("message")) && completedSteps_.size()==1) {
				gnomeAttacks();
			} else if ("talk".equals((String)extraParams.get("message")) && completedSteps_.size()==1) {
				
				String updateMessage = Game.mainActivity.getResources().getString(R.string.quest_seals_gnome_before_first_question);
				updateMessage += getQuestion(1);
				
				showNarrationMessage(updateMessage, false);
				completedSteps_.add(new Step(2 ,new Date(),null));
		
			} else if (completedSteps_.size()==2) { //Answer to first question
				String answer = (String)extraParams.get("message");
				if("good_answer".equals(answer)) { 
					String updateMessage = Game.mainActivity.getResources().getString(R.string.quest_seals_gnome_first_question_correct);
					updateMessage += getQuestion(2);
					showNarrationMessage(updateMessage, false);
					completedSteps_.add(new Step(3 ,new Date(),null));
				} else { //Wrong answer
					gnomeAttacks();
				}
			} else if (completedSteps_.size()==3) { //Answer to second question
				String answer = (String)extraParams.get("message");
				if("good_answer".equals(answer)) { 
					String updateMessage = Game.mainActivity.getResources().getString(R.string.quest_seals_gnome_second_question_correct);
					updateMessage += getQuestion(3);
					showNarrationMessage(updateMessage, false);
					completedSteps_.add(new Step(4 ,new Date(),null));
				} else { //Wrong answer
					gnomeAttacks();
				}
			} else if (completedSteps_.size()==4) { //Answer to third question
				String answer = (String)extraParams.get("message");
				if("good_answer".equals(answer)) { 
					String updateMessage = Game.mainActivity.getResources().getString(R.string.quest_seals_gnome_all_questions_correct);
					showNarrationMessage(updateMessage, true);
					finishQuest();
				} else { //Wrong answer
					gnomeAttacks();
				}
			}
			
		} else if(	et == EventTypes.KILLED &&
					puzzlingGnome.nameID.equals(objID))  { //Player killed gnome
			finishQuest();
		}
		return null;
	}

	
	@Override
	public void onMessageOpen() {
		super.onMessageOpen();
		if(progressState_ == ProgressState.INPROGRESS && 
				completedSteps_.size() == 0) {
			$log.d(TAG, "Message opened sending broadcast");
			Game.turnOnOffNotificationForButton(R.id.btnMap, true);
		}
	}
	
	public void gnomeAttacks() {
		puzzlingGnome.attackWithoutWarning = true;
		puzzlingGnome.containsItem = "item_keypart";
		puzzlingGnome.setLocation(puzzlingGnomeZone.getGeoPoint());
		//puzzlingGnome.moveObject(rnd.nextInt(100), rnd.nextInt(360));
		puzzlingGnome.display(true);
		Game.getGame().worldContainer.addQuestObject(puzzlingGnome, true);
	}
	
	private void finishQuest() {
		if(progressState_ == ProgressState.COMPLITED)
			return;
		
		new CollectableItem(null, new CollectPlant("item_keypart")).interact();
		
		//for(PhysicalObject item : Game.getGame().worldContainer.otherObjects)
		//	item.display(false);
		//TODO: to jest kiepski pomys�, mo�esz miec obiekty z quest�w pobocznych tutaj.
		//Game.getGame().worldContainer.otherObjects.clear(); //Hope to have only plants here during this quest
		
		completedSteps_.add(new Step(completedSteps_.size()+1 ,new Date(),null));
		
		GameContent.getInstance().knownCreatures.add("creature_gnome");
		
		Player.getInst().addExperience(700);
		setCompleted();
		Game.saveGame();
	}
	
	private String getQuestion(int id) {
		int resurcesQuestionID = 0, resurcesQuestionAnswersID = 0;
		switch (id) {
		case 1:
			resurcesQuestionID = R.string.quest_seals_gnome_question_1;
			resurcesQuestionAnswersID = R.array.quest_seals_gnome_question_1_answers;
			break;
		case 2:
			resurcesQuestionID = R.string.quest_seals_gnome_question_2;
			resurcesQuestionAnswersID = R.array.quest_seals_gnome_question_2_answers;
			break;
		case 3:
			resurcesQuestionID = R.string.quest_seals_gnome_question_3;
			resurcesQuestionAnswersID = R.array.quest_seals_gnome_question_3_answers;
			break;
		default:
			return null;
		}
		
		String question = Game.mainActivity.getResources().getString(resurcesQuestionID);
		
		//Get answers and shuffle them
		String[] answersArray = Game.mainActivity.getResources().getStringArray(resurcesQuestionAnswersID);
		List<String> answersList = Arrays.asList(answersArray);
		Collections.shuffle(answersList);
		for(String answer : answersList) {
			question += answer;
		}
		
		return question;
		
	}
}
