package com.jutsugames.modernwizzards.quests.logic;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.content.Intent;
import android.os.CountDownTimer;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.creatures.Bunny;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.quests.logic.Quest.Step;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

public class ThanksMessage extends Quest {
	private static final String TAG = "ThanksMessage";

	
	@Override
	public void initateQuest() {
		title_ = Game.getGlobalTopmostActivity().getResources().getString(R.string.thanks_message_title);
		from_ = Game.getGlobalTopmostActivity().getResources().getString(R.string.thanks_message_from);
		fromimg_ = ctx_.getResources().getString(R.string.quest_profile_bf);
		
		setBasicMessage(R.string.thanks_message_content);
		
		setFullMessageText(getMessageWithNarrations());

		setAccepted();
	}
	
	@Override
	public void initiateQuestBackground() {
		setCompleted();
		Game.saveGame();
	}
	
	@Override
	public void onMessageOpen() {
		initiateQuestBackground();
	}
	
}
