package com.jutsugames.modernwizzards.quests.logic;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.content.Intent;
import android.os.CountDownTimer;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.creatures.Bunny;
import com.jutsugames.modernwizzards.gamelogic.creatures.Gnome;
import com.jutsugames.modernwizzards.gamelogic.creatures.Warewolf;
import com.jutsugames.modernwizzards.gamelogic.creatures.Wraith;
import com.jutsugames.modernwizzards.gamelogic.mixtures.InitialPotion;
import com.jutsugames.modernwizzards.gamelogic.mixtures.InvisibilityPotion;
import com.jutsugames.modernwizzards.gamelogic.mixtures.ManaPotion;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.logic.Quest.AcceptanceState;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.quests.logic.Quest.Step;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

public class LittleRedRidingHoodQuest extends Quest {
	private static final String TAG = "LittleRedRidingHoodQuest";
	private transient final String grandMaZoneID = "grandMaZone";
	private transient TriggerZone grandMaZone;
	private transient int grandMaZoneRadius = 50;
	private Warewolf warewolf;
	private transient Random rnd;
	private transient boolean messageNoAntidotumDisplayed = false;

	@Override
	public void initateQuest() {
		title_ = ctx_.getResources().getString(R.string.quest_lrrh_title);
		from_ = ctx_.getResources().getString(R.string.quest_lrrh_from);
		fromimg_ = ctx_.getResources()
				.getString(R.string.quest_profile_unknown);

		setBasicMessage(R.string.quest_lrrh_content);

		setFullMessageText(getMessageWithNarrations());

		Game.getGame().showNewMessageAlert(this);
	}

	@Override
	public void initiateQuestBackground() {
		rnd = new Random();

		GeoPoint gp_player = Game.getGame().playerEgo.getGeoPoint();
		GameContent.getInstance().knownPotions.add("potion_antidote");
		
		grandMaZone = new TriggerZone(grandMaZoneID, new GeoPoint(
				gp_player.getLatitudeE6(), gp_player.getLongitudeE6()),
				grandMaZoneRadius, ctx_.getResources().getString(
						R.string.quest_lrrh_grandMa_zone_name));
		grandMaZone.moveObject(250.0 + rnd.nextInt(100), rnd.nextInt(360));

		Game.getGame().worldContainer.addQuestObject(grandMaZone, true);
		grandMaZone.display(true);

		if (warewolf == null)
			warewolf = new Warewolf();
		warewolf.zone.radius = grandMaZoneRadius + 20;
		warewolf.setLocation(grandMaZone.getGeoPoint());
		Game.getGame().worldContainer.addQuestObject(warewolf, false);
		warewolf.display(false);
	}

	@Override
	public void setAccepted() {
		$log.d(TAG, "Set assepted");
		super.setAccepted();
		progressState_ = ProgressState.INPROGRESS;
		initiateQuestBackground();
	}

	@Override
	public boolean tryToRestoreQuest() {
		if (progressState_ != ProgressState.INPROGRESS)
			return false;

		switch (completedSteps_.size()) {
		case 0:
			initiateQuestBackground();
			return true;
		case 1:

			return true;
		default:
			return false;

		}
	}

	@Override
	public Set<PostProcessingRequestType> handleEvent(String objID,
			EventTypes et, Map<String, Object> extraParams) {
		super.handleEvent(objID, et, extraParams);
		if (acceptanceState_ != AcceptanceState.ACCEPTED
				|| progressState_ != ProgressState.INPROGRESS)
			return null;

		if (et == EventTypes.VISITED && grandMaZoneID.equals(objID)
				&& warewolf.getHealthPoints() <= 0) { // Player reached
														// triggered zone.
			if (Player.getInst().inv.getMixturesCountByName("potion_antidote") > 0) {
				finishQuest();
			} else {
				showNarrationOncePerVisit(R.string.quest_lrrh_without_antidote,
						objID);
			}
		}
		return null;
	}

	@Override
	public void onMessageOpen() {
	}

	private void finishQuest() {
		if (progressState_ == ProgressState.COMPLITED)
			return;
		// for(PhysicalObject item : Game.getGame().worldContainer.otherObjects)
		// item.display(false);
		// TODO: to jest kiepski pomys�, mo�esz miec obiekty z quest�w
		// pobocznych tutaj.
		// Game.getGame().worldContainer.otherObjects.clear(); //Hope to have
		// only plants here during this quest
		Game.getGame().worldContainer.removeQuestObject(grandMaZone);
		grandMaZone.display(false);
		warewolf = null;

		Player.getInst().inv.removeMixture(Player.getInst().inv
				.getMixtureByName("potion_antidote"));

		completedSteps_.add(new Step(completedSteps_.size() + 1, new Date(),
				"quest_lrrh_end"));
		showNewInQuestMessage(R.string.quest_lrrh_end);

		setFullMessageText(getMessageWithNarrations());
		GameContent.getInstance().knownPotions.add("potion_mana");
		GameContent.getInstance().knownCreatures.add("creature_warewolf");
		Player.getInst().addExperience(300);
		setCompleted();

		Game.saveGame();
	}

}
