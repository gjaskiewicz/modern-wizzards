package com.jutsugames.modernwizzards.quests.logic;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.os.CountDownTimer;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.spells.OutOfBodyExperience;
import com.jutsugames.modernwizzards.objects.map.ManaZone;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.logic.Quest.AcceptanceState;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class ManaSourcesQuest extends Quest {
	private static final String TAG = "ManaSourcesQuest";
	private transient List<ManaZone> manaZones;
	
	@Override
	public void initateQuest() {
		title_ = ctx_.getResources().getString(R.string.quest_mana_sources_title);
		from_ = ctx_.getResources().getString(R.string.quest_mana_sources_from);
		fromimg_ = ctx_.getResources().getString(R.string.quest_profile_anette);
		
		setBasicMessage(R.string.quest_mana_sources_initial);
		
		setFullMessageText(getMessageWithNarrations());

		setAccepted();
		
		Game.getGame().showNewMessageAlert(this);
	}
	
	@Override
	public void initiateQuestBackground() {
		Random rnd = new Random();
		int startingBearing = rnd.nextInt(90);
		
		GeoPoint gp_player = Game.getGame().playerEgo.getGeoPoint();
		
		manaZones = new LinkedList<ManaZone>();
		
		for(int i = 0; i < 3; ++i) 
		{
			ManaZone mz = new ManaZone(new GeoPoint(
											gp_player.getLatitudeE6(), 
											gp_player.getLongitudeE6()), 
										40);
			mz.moveObject(150.0, 120*i + startingBearing);
			mz.display(true);
			Game.getGame().worldContainer.addQuestObject(mz, false);
			manaZones.add(mz);
		}
	}
	
	@Override
	public Set<PostProcessingRequestType> handleEvent(String objID, EventTypes et,
			Map<String, Object> extraParams) {
		super.handleEvent(objID, et, extraParams);
		
		if(acceptanceState_ != AcceptanceState.ACCEPTED || 
				progressState_ != ProgressState.INPROGRESS) return null;
		
		if( 	ManaZone.manaZoneID.equals(objID) && //Entered first area with normal walking
				et == EventTypes.VISITED && 
				completedSteps_.size() == 0 ) {
			
			completedSteps_.add(new Step(1,new Date(),null));
			
			if( Game.getGame().playerEgo.isManaFull() ) {
				finishQuest();
				return getResultSet(PostProcessingRequestType.REFRESHMAP);
			}
			
		} else 	if(	Game.getGame().playerEgo.nameID.equals(objID) && //Entered second area with oob
					et == EventTypes.MANA_FULL && 
					completedSteps_.size() == 1 ) {
		
			finishQuest();
			return getResultSet(PostProcessingRequestType.REFRESHMAP);
		}
		return null;
	}
	
	@Override
	public void onMessageOpen() {
		super.onMessageOpen();
		if(progressState_ == ProgressState.INPROGRESS && 
				completedSteps_.size() < 1) { //Player started to play and didn't visited any triggered area.
			Game.turnOnOffNotificationForButton(R.id.btnMap, true);
		}
	}
	
	private void finishQuest() {
		if(isCompleted())
			return;
		
		for( PhysicalObject mz : manaZones) {
			Game.getGame().worldContainer.removeQuestObject(mz);
			mz.display(false);
		}
		
		manaZones.clear();
		
		GameContent.getInstance().generateManaZones = true;
		
		completedSteps_.add(new Step(2,new Date(),"quest_mana_sources_narration_mana_full"));
		
		showNewInQuestMessage(R.string.quest_mana_sources_narration_mana_full);
		
		setFullMessageText(getMessageWithNarrations());
		
		Player.getInst().addExperience(400);
		setCompleted();
		Game.saveGame();
	}

}