package com.jutsugames.modernwizzards.quests.logic;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.content.Intent;
import android.os.CountDownTimer;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gamelogic.creatures.Bunny;
import com.jutsugames.modernwizzards.gamelogic.creatures.Gnome;
import com.jutsugames.modernwizzards.gamelogic.creatures.Golem;
import com.jutsugames.modernwizzards.gamelogic.mixtures.InitialPotion;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.quests.logic.Quest.Step;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

public class SpecialMissionQuest extends Quest {
	private static final String TAG = "SpecialMissionQuest";
	private final String golemZoneID = "golemZone", goalZoneID = "goalZone";
	private transient TriggerZone golemZone, goalZone;
	private transient Golem golem;
	private transient Random rnd;

	
	@Override
	public void initateQuest() {
		title_ = ctx_.getResources().getString(R.string.quest_special_mission_title);
		from_ = ctx_.getResources().getString(R.string.quest_special_mission_from);
		fromimg_ = ctx_.getResources().getString(R.string.quest_profile_anette);
		
		setBasicMessage(R.string.quest_special_mission_initial);
		
		setFullMessageText(getMessageWithNarrations());
		
		setAccepted();
		
		Game.getGame().showNewMessageAlert(this);
	}
	
	@Override
	public void initiateQuestBackground() { 
		rnd = new Random();
		
		GeoPoint gp_player = Game.getGame().playerEgo.getGeoPoint();
		
		golemZone = new TriggerZone(golemZoneID, 
				new GeoPoint(
					gp_player.getLatitudeE6(), 
					gp_player.getLongitudeE6() ), 
				70, ctx_.getResources().getString(R.string.target));
		golemZone.moveObject(150.0 + rnd.nextInt(100), rnd.nextInt(360));
		
		Game.getGame().worldContainer.addQuestObject(golemZone, true);
		golemZone.display(true);
	}
	
	@Override
	public Set<PostProcessingRequestType> handleEvent(String objID, EventTypes et,
			Map<String, Object> extraParams) {
		super.handleEvent(objID, et, extraParams);
		if(acceptanceState_ != AcceptanceState.ACCEPTED || 
				progressState_ != ProgressState.INPROGRESS) return null;
		
		if(	
				et == EventTypes.VISITED &&
				golemZoneID.equals(objID)
			) 
		{ //Player reached triggered zone. Add golem
			Game.getGame().worldContainer.removeQuestObject(golemZone);
			golemZone.display(false);
			
			completedSteps_.add(new Step(1 ,new Date(),null));
			
			showNarrationMessage(R.string.quest_special_mission_narration_in_first_zone);
		 	
			golem = new Golem();
			golem.setLocation(golemZone.getGeoPoint());
			golem.display(true);
			Game.getGame().worldContainer.addQuestObject(golem, true);
			
			goalZone = new TriggerZone(goalZoneID, 
					golemZone.getGeoPoint(), 
					30, ctx_.getResources().getString(R.string.target));
			
			Game.getGame().worldContainer.addQuestObject(goalZone, true);
			goalZone.display(true);
		
		} else if(			
				et == EventTypes.VISITED &&
				goalZoneID.equals(objID) && 
					(golem.getHealthPoints()<=0 ||
					Game.getGame().playerEgo.isUnderEffectOfSpell("potion_invisibility")
					)
				)	{
			
			completedSteps_.add(new Step(2 ,new Date(),null));
			showNarrationMessage(R.string.quest_special_mission_narration_final);
			
			Game.getGame().worldContainer.removeQuestObject(golem);
			Game.getGame().worldContainer.removeQuestObject(goalZone);

			//for(PhysicalObject item : Game.getGame().worldContainer.otherObjects)
			//	item.display(false);
			//Game.getGame().worldContainer.otherObjects.clear();
			setCompleted();
			Game.saveGame();
			
			
		}
		return null;
	}
	
	@Override
	public void onMessageOpen() {
		super.onMessageOpen();
		if(progressState_ == ProgressState.INPROGRESS && 
				completedSteps_.size() == 0) {
			$log.d(TAG, "Message opened sending broadcast");
			Game.turnOnOffNotificationForButton(R.id.btnMap, true);
		}
	}
}
