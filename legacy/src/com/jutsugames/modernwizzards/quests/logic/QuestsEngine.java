package com.jutsugames.modernwizzards.quests.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.R.bool;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.util.Log;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.ModernWizzardsActivity;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.quests.display.QuestsMessageListActivity;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.util.ChangeListener;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;
import com.jutsugames.modernwizzards.util.RestoreFromSave;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

/**
 * 
 * @author Jarek
 * 
 * Class designed for managing all quests in game.
 * Class represents some kind of quests storage. 
 */

public class QuestsEngine implements ChangeListener, RestoreFromSave {
	private transient static final String TAG = "QuestsEngine";
	
	private ArrayList<Quest> quests_ = new ArrayList<Quest>();

    private static QuestsEngine INSTANCE = null;

    private transient IntentFilter eventFilterForQuestsDecisions;
	private transient BroadcastReceiver eventsReceiverForQuestsDecisions;
	private transient boolean isReceiverRegistered = false;
    
    public static Map<Class<?>, Class<?>> nextQuestsCollection = new HashMap<Class<?>, Class<?>>();
    public static Map<Class<?>, Set<Class<?>>> extraQuestsCollection = new HashMap<Class<?>, Set<Class<?>>>();
    
	private int timeToNextQuest = 20000; //[ms]
	protected final int DEV_MODE_NEXT_QUEST_IN = 4000; //In ms

    static {
    	nextQuestsCollection.put(InitialQuest.class, 		GoToPointQuest.class);
    	nextQuestsCollection.put(GoToPointQuest.class, 		CollectPlantsQuest.class);
    	nextQuestsCollection.put(CollectPlantsQuest.class, 	ManaSourcesQuest.class);
    	nextQuestsCollection.put(ManaSourcesQuest.class, 	FleeingRabbitQuest.class);
    	nextQuestsCollection.put(FleeingRabbitQuest.class,  AlchemyQuest.class);
    	nextQuestsCollection.put(AlchemyQuest.class, 		TeletransportQuest.class);
    	nextQuestsCollection.put(TeletransportQuest.class,  SealsQuest.class);
    	nextQuestsCollection.put(SealsQuest.class,  		SpecialMissionQuest.class);
    	nextQuestsCollection.put(SpecialMissionQuest.class, FinalMessage.class);
    	nextQuestsCollection.put(FinalMessage.class, 		ThanksMessage.class);
    	nextQuestsCollection.put(ThanksMessage.class, 		null);
    	
    	Set<Class<?>> extraQuestsAfterTeletransport = new HashSet<Class<?>>();
    	extraQuestsAfterTeletransport.add(HauntedHouseQuest.class);
    	extraQuestsAfterTeletransport.add(LittleRedRidingHoodQuest.class);
    	extraQuestsCollection.put(TeletransportQuest.class, extraQuestsAfterTeletransport);
    }
    
    public static QuestsEngine getInstance() {
    	if(INSTANCE == null)
    		INSTANCE = new QuestsEngine();
        return INSTANCE;
    }
    
	public static synchronized void reloadQuestsEngine(QuestsEngine qe) {
		INSTANCE = qe;
	}
	
	private QuestsEngine() {
		eventFilterForQuestsDecisions = new IntentFilter(Game.Actions.QUEST_DECISION);
		eventsReceiverForQuestsDecisions = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.hasExtra("returnValue") && intent.hasExtra("questClassName")) {
	    			Quest quest;
					quest = getQuestByClassName(intent.getExtras().getString("questClassName"));
					if(quest == null) return;
					String decision = intent.getExtras().getString("returnValue");
	    			if("Accept".equals(decision))
	    				quest.setAccepted();
	    			else if ("Reject".equals(decision))
	    				quest.setRejected();
	    		}
			}
        };
        if(Game.mainActivity != null) {
        	Game.mainActivity.registerReceiver(eventsReceiverForQuestsDecisions, eventFilterForQuestsDecisions);
        	isReceiverRegistered = true;
        }
	}
	
	
	public Quest getQuestByID(int id) {
		if(id>=quests_.size()) return null; 
		return quests_.get(id);
	}
	
	public Quest getQuestByClass(Class<?> cls) {
		for(Quest q : quests_) {
			if(q.getClass().isAssignableFrom(cls)) {
				return q;
			}
		}
		return null;	
	}
	
	public Quest getQuestByClassName(String className) {
		if(className == null) return null;
		try {
			return getQuestByClass(Class.forName(className));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public int getCountOfUnopenedQuestMessages() {
		int counter = 0;
		for(Quest quest : quests_) {
			if(!quest.isAlreadyRead()) ++counter;
		}
		return counter;
	}
	
	public void removeQuestByClass(Class<?> cls) {
		quests_.remove(getQuestByClass(cls));
		Game.questsCollectionChanged();
	}
	
	private boolean isQuestAlreadyInEngine(Class<?> cls) {
		for(Quest quest : quests_) {
			if(quest.getClass() == cls)
				return true;
		}
		return false;
	}
	
	//TODO: Przygotować odpowiednio timer 
	public void initiateQuest(final Class<?> questClass) {
		if(questClass == null) {
			$log.e(TAG, "Tring to initiate null quest class");
			return;
		}
		
		synchronized (INSTANCE) {
			if (Game.getGame().locationIsNotSet) { //Set listener to event location set
		        //Set receiver for notification updates of menu buttons
				$log.d(TAG, "Location is not set. Setting broadcast receiver");
				IntentFilter eventFilter = new IntentFilter("com.jutsugames.modernwizzards.LOCATION_SET");
				BroadcastReceiver eventsReceiver =  new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						Game.mainActivity.unregisterReceiver(this);
						startQuest(questClass);
					}
		        };
		        Game.mainActivity.registerReceiver(eventsReceiver, eventFilter);
			} else { //Start timer
				$log.d(TAG, "Location is set. Starting timer");
				
				new AsyncTask<Void, Void, Void>() {

					@Override
					protected Void doInBackground(Void... params) {
						int waitTime = (Options.DEVELOPMENT_MODE ? DEV_MODE_NEXT_QUEST_IN : timeToNextQuest);
						try {
							Thread.sleep(waitTime);
							$log.d(TAG, "Timer is up");
							startQuest(questClass);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						return null;
					}
					
				}.execute();
			}
		}
	}
	
	public void startQuest(Class<?> questClass) {
		try {
			if(isQuestAlreadyInEngine(questClass)) {	
				$log.e(TAG, "Quest " + questClass.getSimpleName() + " already in Quests Engine");
				return;
			}
			Quest nextQuest = (Quest) questClass.newInstance();
			nextQuest.initateQuest();
			addQuest(nextQuest);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void onRestoreFromSave() {
		$log.d(TAG, "onRestoreFromSave " + this.getClass().getSimpleName());
		if(!isReceiverRegistered) {
        	Game.mainActivity.registerReceiver(eventsReceiverForQuestsDecisions, eventFilterForQuestsDecisions);
        	isReceiverRegistered = true;
		}
		Set<Quest> questsToReinitiate = new HashSet<Quest>();
		for(Quest quest : quests_) {
			Set<Class<?>> questsClassesToCheck = new HashSet<Class<?>>(); //Collection of quest classes which should be added after initiated just after given quest 
			if(nextQuestsCollection.containsKey(quest.getClass())) {
				Class<?> nextQuestClass = nextQuestsCollection.get(quest.getClass());
				if(nextQuestClass == null) {
					$log.w(TAG, "No next quest");
					continue;
				}
				
				questsClassesToCheck.add(nextQuestClass);
			}
			
			if(extraQuestsCollection.containsKey(quest.getClass())) {
				for(Class<?> cls : extraQuestsCollection.get(quest.getClass())) {
					questsClassesToCheck.add(cls);
				}
			}
			
			for(Class<?> questClass : questsClassesToCheck) {
				Quest questToCheck = getQuestByClass(questClass);
				if(questToCheck == null && quest.isCompleted() ) {
					$log.d(TAG, "Next quest for quest " + quest.getClass().getSimpleName() + " is null");
					initiateQuest(questClass);
					continue;
				}
				else if(questToCheck != null && 
						!questToCheck.isCompleted() && 
						!questToCheck.isRejected()) {
					
					$log.d(TAG, "Next quest for quest " + quest.getClass().getSimpleName() + " is not null but is not completed");
					questsToReinitiate.add(questToCheck);
					continue;
				}
			}
		}
		
		for(Quest quest : questsToReinitiate) {
			if(!quest.tryToRestoreQuest()) {
				quests_.remove(quest);
				initiateQuest(quest.getClass());
			}
		}
	}

	@Override
	public synchronized Set<PostProcessingRequestType> handleEvent(String objID, EventTypes et,
			Map<String, Object> extraParams ) {
		HashSet<PostProcessingRequestType> result = new HashSet<PostProcessingRequestType>();
		for (Quest q : quests_ )
			if(q.getProgressState() == ProgressState.INPROGRESS) {
				$log.d(TAG, "Quest " + q.title_ + " received event form object " + objID);
				Set<PostProcessingRequestType> tmpRes = q.handleEvent(objID, et, extraParams);
				if(tmpRes != null) result.addAll(tmpRes);
			}
		
		if(et == EventTypes.QUEST_FINISHED) {
			try {
				Class<?> finishedQuestClass = Class.forName(objID);
				Class<?> nextQuestClass = nextQuestsCollection.get(finishedQuestClass);
				initiateQuest(nextQuestClass);
				if(extraQuestsCollection.containsKey(finishedQuestClass))
					for(Class<?> cls : extraQuestsCollection.get(finishedQuestClass)) {
						initiateQuest(cls);
					}
			} catch (ClassNotFoundException e) {
				$log.e(TAG, "Problems with finding class for " + objID);
				e.printStackTrace();
			}
		}
		$log.d(TAG, objID + " has emited event " + et.toString() );
		return result;
	}
	
	public List<Quest> getQuestsColl() {
		return quests_;
	}
	
	public void addQuest(Quest quest) {
		quests_.add(0, quest);
		Game.questsCollectionChanged();
		Activity activity = Game.getGlobalTopmostActivity();
		if( activity == null) return;
		if( !(activity instanceof QuestsMessageListActivity)) {
			Game.turnOnOffNotificationForButton(R.id.btnQuests, true);
		}
		
	}


	
}
