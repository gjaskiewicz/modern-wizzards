package com.jutsugames.modernwizzards.quests.logic;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.content.Intent;
import android.os.CountDownTimer;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.creatures.Bunny;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.quests.logic.Quest.Step;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

public class AlchemyQuest extends Quest {
	private static final String TAG = "AlchemyQuest";
	
	@Override
	public void initateQuest() {
		title_ = ctx_.getResources().getString(R.string.quest_alchemy_initial_title);
		from_ = ctx_.getResources().getString(R.string.quest_alchemy_initial_from);
		fromimg_ = ctx_.getResources().getString(R.string.quest_profile_anette);
		
		setBasicMessage(R.string.quest_alchemy_initial);
		
		setFullMessageText(getMessageWithNarrations());

		setAccepted();

		Game.getGame().showNewMessageAlert(this);
	}
	
	@Override
	public void initiateQuestBackground() { 
		GameContent.getInstance().knownPotions.add("potion_health");
		GameContent.getInstance().isAlchemyActive=true;
	}
	
	@Override
	public Set<PostProcessingRequestType> handleEvent(String objID, EventTypes et,
			Map<String, Object> extraParams) {
		super.handleEvent(objID, et, extraParams);
		if(acceptanceState_ == AcceptanceState.ACCEPTED && 
				progressState_ == ProgressState.INPROGRESS && 
				et == EventTypes.CREATED_BY_ALCHEMY &&
				"potion_health".equals(objID)
				) { //Player reached triggered zone. Add rabbit exact location
			completedSteps_.add(new Step(1 ,new Date(),null));
			
			List<String> usedPlants = (List<String>) extraParams.get("usedPlants");
			String usedPlantsString = new String();
			for(String plant : usedPlants) { usedPlantsString += plant + " "; }
			$log.d(TAG, "Receved event CREATED_BY_ALCHEMY ("+ objID +") .From plants: " + usedPlantsString );
			
			Player.getInst().addExperience(600);
			
			//TODO: Te powinny byc dodane po questach pobocznych.
			GameContent.getInstance().knownPotions.add("potion_luckjelly");
			GameContent.getInstance().knownPotions.add("potion_stimulation");
			GameContent.getInstance().knownPotions.add("potion_perception");
			
			setCompleted();
			Game.saveGame();
		}
		return null;
	}
	
	@Override
	public void onMessageOpen() {
		super.onMessageOpen();
		if(progressState_ == ProgressState.INPROGRESS && 
				completedSteps_.size() == 0) {
			$log.d(TAG, "Message opened sending broadcast");
			Game.turnOnOffNotificationForButton(R.id.btnInventory, true);
		}
	}
}
