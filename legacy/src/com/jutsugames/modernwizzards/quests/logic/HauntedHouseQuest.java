package com.jutsugames.modernwizzards.quests.logic;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.content.Intent;
import android.os.CountDownTimer;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.creatures.Bunny;
import com.jutsugames.modernwizzards.gamelogic.creatures.Gnome;
import com.jutsugames.modernwizzards.gamelogic.creatures.Wraith;
import com.jutsugames.modernwizzards.gamelogic.mixtures.InitialPotion;
import com.jutsugames.modernwizzards.gamelogic.mixtures.InvisibilityPotion;
import com.jutsugames.modernwizzards.gamelogic.mixtures.ManaPotion;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.logic.Quest.AcceptanceState;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.quests.logic.Quest.Step;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

public class HauntedHouseQuest extends Quest {
	private static final String TAG = "HauntedHouseQuest";
	private transient final String hauntedHouseZoneID = "hauntedHouseZone";
	private transient TriggerZone hauntedHouseZone;
	private transient int hauntedHouseZoneRadius = 70;
	private transient Wraith wraith;
	private transient Random rnd;
	
	@Override
	public void initateQuest() {
		title_ = ctx_.getResources().getString(R.string.quest_haunted_house_title);
		from_ = ctx_.getResources().getString(R.string.quest_haunted_house_from);
		fromimg_ = ctx_.getResources().getString(R.string.quest_profile_unknown);
		
		setBasicMessage(R.string.quest_haunted_house_content);
		
		setFullMessageText(getMessageWithNarrations());
		
		Game.getGame().showNewMessageAlert(this);
	}
	
	@Override
	public void initiateQuestBackground() {
		rnd = new Random();
		
		GeoPoint gp_player = Game.getGame().playerEgo.getGeoPoint();
		
		hauntedHouseZone = new TriggerZone(hauntedHouseZoneID, 
				new GeoPoint(
					gp_player.getLatitudeE6(), 
					gp_player.getLongitudeE6() ), 
					hauntedHouseZoneRadius, 
					ctx_.getResources().getString(R.string.target));
		hauntedHouseZone.moveObject(250.0 + rnd.nextInt(100), rnd.nextInt(360));
		
		Game.getGame().worldContainer.addQuestObject(hauntedHouseZone, true);
		hauntedHouseZone.display(true);
	}
	
	@Override
	public void setAccepted() {
		$log.d(TAG,"Set assepted");
		super.setAccepted();
		progressState_ = ProgressState.INPROGRESS;
		initiateQuestBackground();
	}
	
	@Override
	public Set<PostProcessingRequestType> handleEvent(String objID, EventTypes et,
			Map<String, Object> extraParams) {
		super.handleEvent(objID, et, extraParams);
		if(acceptanceState_ != AcceptanceState.ACCEPTED || 
				progressState_ != ProgressState.INPROGRESS) return null;
		
		if(		et == EventTypes.VISITED &&
				hauntedHouseZoneID.equals(objID) ) { //Player reached triggered zone.
			wraith = new Wraith();
			
			
			Game.getGame().worldContainer.removeQuestObject(hauntedHouseZone);
			hauntedHouseZone.display(false);
			
			completedSteps_.add(new Step(1 ,new Date(),null));
			wraithAttacks();
			
		} else if(	et == EventTypes.KILLED &&
					wraith.nameID.equals(objID))  { //Player killed wraith
			finishQuest();
		}
		return null;
	}

	@Override
	public void onMessageOpen() {}
	
	public void wraithAttacks() {
		wraith.zone.radius = hauntedHouseZoneRadius;
		wraith.attackWithoutWarning = true;
		wraith.setLocation(hauntedHouseZone.getGeoPoint());
		wraith.display(true);
		Game.getGame().worldContainer.addQuestObject(wraith, true);
	}
	
	private void finishQuest() {
		if(progressState_ == ProgressState.COMPLITED)
			return;
		//for(PhysicalObject item : Game.getGame().worldContainer.otherObjects)
		//	item.display(false);
		//TODO: to jest kiepski pomys�, mo�esz miec obiekty z quest�w pobocznych tutaj.
		//Game.getGame().worldContainer.otherObjects.clear(); //Hope to have only plants here during this quest
		
		completedSteps_.add(new Step(completedSteps_.size()+1 ,new Date(), "quest_haunted_house_end"));
		showNewInQuestMessage(R.string.quest_haunted_house_end);
		
		setFullMessageText(getMessageWithNarrations());
		Game.getGame().playerEgo.inv.addMixture(new SpellToken(new InvisibilityPotion()));
		GameContent.getInstance().knownCreatures.add("creature_wraith");
		GameContent.getInstance().knownPotions.add("potion_invisibility");
		Player.getInst().addExperience(300);
		setCompleted();
		
		Game.saveGame();
	}
	
}
