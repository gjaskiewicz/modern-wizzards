package com.jutsugames.modernwizzards.quests.logic;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.util.Log;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.quests.display.MessageViewActivity;
import com.jutsugames.modernwizzards.util.ChangeListener;
import com.jutsugames.modernwizzards.util.RestoreFromSave;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class Quest extends Message implements ChangeListener {
	private static final String TAG = "Quest";

	protected transient Class<?> questClass_;
	protected transient Map<String,Set<Integer>> narrationsAlreadyShownDuringVisit;
	
	/** lista wszystkich wiadomosci kt�re w quescie/w�tku wiadomo�ci sie pojawiaj� */
	public LinkedList<Message> messages;
	
	protected ProgressState progressState_;
	protected TreeSet<Step> completedSteps_;
	protected Integer stepsToGoal_;
	private String basicMessage = null;

	
	public static enum AcceptanceState {
		NONE,
		ACCEPTED,
		REJECTED
	};
	protected AcceptanceState acceptanceState_;
	
	public static enum ProgressState {
		NONE,
		INPROGRESS,
		COMPLITED,
		FAILED
	};
	
	protected class Step implements Comparable<Step> {
		public Step(Integer id, Date date, String narrURI) {
			stepID = id;
			completeDate = date;
			narrationTextName = narrURI;
		};
		public Integer stepID;
		public Date completeDate;
		public String narrationTextName;
		
		@Override
		public int compareTo(Step another) {
			return completeDate.compareTo(another.completeDate);
		}
	};
	
	public Quest() {
		this("","");
	}
	
	public Quest(String title, String from) {
		title_ = title;
		from_ = from;
		state = MessageState.UNREAD;
		//visible_ = true;
		acceptanceState_ = AcceptanceState.NONE;
		progressState_ = ProgressState.NONE;
		completedSteps_ = new TreeSet<Step>();
		stepsToGoal_ = 100;
		questClass_ = this.getClass();
		
		narrationsAlreadyShownDuringVisit = new HashMap<String,Set<Integer>>();

		
	}
	
	//Initiate only message. Rest of stuff should be initialized in initiateQuestBackground method
	public void initateQuest() {}
	
	//Initiate map objects, set state "in progress", etc.
	public void initiateQuestBackground() {}
	
	@Override
	public void onMessageOpen() {
		initiateQuestBackground();
		progressState_ = ProgressState.INPROGRESS;
	}
	
	/**
	 * Some visual parts of quests (i.e. triggered zones on map) couldn't be restored during normal deserialization. 
	 * So this method will be called after entire deserialization to prepare visual parts of quest.  
	 */
/*	@Override
	public void onRestoreFromSave() {
		$log.d(TAG, "onRestoreFromSave " + this.getClass().getSimpleName());
		Class<?> nextQuestClass = QuestsEngine.nextQuestsCollection.get(this.getClass());
		if(nextQuestClass == null) {
			$log.w(TAG, "No next quest");
			return;
		}
		Quest nextQuest = QuestsEngine.getInstance().getQuestByClass(nextQuestClass);
		if(nextQuest == null && isCompleted() ) {
			$log.d(TAG, "Next quest for quest " + this.getClass().getSimpleName() + " is null");
			initiateNextQuest();
		}
		else if(nextQuest != null && !nextQuest.isCompleted() ) {
			$log.d(TAG, "Next quest for quest " + this.getClass().getSimpleName() + " is not null but is not completed");
			QuestsEngine.getInstance().removeQuestByClass(nextQuestClass);
			initiateNextQuest();
		}
	}*/
	

	
	public AcceptanceState getAcceptanceState() {
		return acceptanceState_;
	}
	
	public ProgressState getProgressState() {
		return progressState_;
	}
	
	public void setAccepted() {
		$log.d(TAG,"Set accepted");
		acceptanceState_ = AcceptanceState.ACCEPTED;
	}
	
	public boolean isCompleted() {
		return progressState_ == ProgressState.COMPLITED;
	}
	
	public boolean isRejected() {
		return acceptanceState_ == AcceptanceState.REJECTED;
	}

	public void setRejected() {
		acceptanceState_ = AcceptanceState.REJECTED;
		Game.saveGame();
	}
	
	public void setCompleted() {
		progressState_ = ProgressState.COMPLITED;
		Intent i = new Intent("com.jutsugames.modernwizzards.EVENT").
				putExtra("objID",this.getClass().getName()).
				putExtra("eventType", EventTypes.QUEST_FINISHED.toString());
		Game.mainActivity.sendBroadcast(i);
	}
	
	public Set<PostProcessingRequestType> handleEvent(String objID, EventTypes et,
			Map<String, Object> extraParams ) {
		if(et.equals(EventTypes.EXITED))
			narrationsAlreadyShownDuringVisit.get(objID).clear();
		return null;
	}
	
	@Override
	public String getTitle() {
		String extraState = new String();
		switch(progressState_) {
			case INPROGRESS:
				extraState = ctx_.getString(R.string.inprogress);
				break;
			case COMPLITED:
				extraState = ctx_.getString(R.string.completed);
				break;
			case FAILED:
				extraState = ctx_.getString(R.string.failed);
				break;
		}
		//zakomentowalem bo na szzegolach brzydko wyglada
		//return title_ + (extraState.length() == 0 ? "" : " (" + extraState + ")");
		return title_;
	}
	
	public void setBasicMessage(int messageId) {
		setBasicMessage(ctx_.getString(messageId));
	}
	
	public void setBasicMessage(String message) {
		basicMessage = message;
	}
	
	public String getMessageWithNarrations() {
		String fullMessage = "";
		Format formatter;
		formatter = new SimpleDateFormat("dd/MM/yy HH:mm");
		
		if (basicMessage != null)
			fullMessage += basicMessage;
		
		
		if(completedSteps_.size() > 0)
			for (Step s : completedSteps_)
				if (s.narrationTextName != null) {
					$log.d(TAG, "Looking for narration text");
					int id = ctx_.getResources().getIdentifier(s.narrationTextName, "string", "com.jutsugames.modernwizzards");
					if (id != 0) {
						$log.d(TAG, "Resource id found");
						fullMessage += "</br></br>" +  formatter.format(s.completeDate) + "</br>" + ctx_.getString(id);
					}
				}
				
		return fullMessage;
	}
	
	/**
	 * Pokazuje NARRACJE , czyli go�y tekst.
	 * @param narrationID id z resurs�w
	 */
	public void showNarrationMessage(int narrationID) {
		String message = Game.mainActivity.getResources().getString(narrationID);
		showNarrationMessage(message, true);
	}
	
	/**
	 * Pokazuje narracj� raz na wizyt� w zonie
	 * @param narrationID
	 */
	public void showNarrationOncePerVisit(Integer narrationID, String zoneID) {
		if(!narrationsAlreadyShownDuringVisit.containsKey(zoneID)) {
			narrationsAlreadyShownDuringVisit.put(zoneID, new HashSet<Integer>());
		}
		
		if(!narrationsAlreadyShownDuringVisit.get(zoneID).contains(narrationID)) {
			showNarrationMessage(narrationID);
			narrationsAlreadyShownDuringVisit.get(zoneID).add(narrationID);
		}
	}
	
	/**
	 * Pokazuje NARRACJE , czyli go�y tekst.
	 * @param message tekst do wyswietlenia.
	 * @param canClose TODO
	 */
	public void showNarrationMessage(String message, boolean canClose) {
		Intent i = new Intent(Game.mainActivity, MessageViewActivity.class);
		i.putExtra(MessageViewActivity.MESSAGE_TYPE, MessageViewActivity.MESSAGE_TYPE_NARRATION);
		//i.putExtra("title", getTitle());
		i.putExtra("message", message);
		i.putExtra("readOnly",true);
		i.putExtra(MessageViewActivity.MESSAGE_CANCLOSE, canClose);
		Game.mainActivity.startActivity(i);
	}
	
	public void showNewInQuestMessage(int narrationID) {
		showNewInQuestMessage(narrationID, 0, 0);
	}
	
	public void showNewInQuestMessage(int narrationID, int titleID, int fromID) {
		String message = Game.mainActivity.getResources().getString(narrationID);
		
		String title, from;
		if(fromID != 0)
			from = Game.mainActivity.getResources().getString(fromID);
		else 
			from = getFrom();
		
		if(titleID != 0)
			title = Game.mainActivity.getResources().getString(titleID);
		else 
			title = getTitle();
		
		Intent i = new Intent(Game.mainActivity, MessageViewActivity.class);
		//i.putExtra(MessageViewActivity.MESSAGE_TYPE, MessageViewActivity.MESSAGE_);
		i.putExtra("title", title);
		i.putExtra("from", from);
		i.putExtra("fromimg", fromimg_);
		i.putExtra("message", message);
		i.putExtra("readOnly",true);
		i.putExtra(MessageViewActivity.MESSAGE_CANCLOSE, true);
		Game.mainActivity.startActivity(i);
	}
	
	/**
	 * Tries to restore quests's saved state.
	 * @return When success then return true
	 */
	public boolean tryToRestoreQuest() {
		return false;
	}
	
	public void cleanQuestRemains() {}
	
	protected Set<PostProcessingRequestType> getResultSet(PostProcessingRequestType req) {
		HashSet<PostProcessingRequestType> result = new HashSet<PostProcessingRequestType>();
		if (req != null) result.add(req);
		return result;
	}
	
	protected Set<PostProcessingRequestType> getResultSet() {
		return getResultSet(null);
	}
	
	
}
