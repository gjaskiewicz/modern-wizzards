package com.jutsugames.modernwizzards.quests.logic;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.content.Intent;
import android.os.CountDownTimer;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.creatures.Bunny;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.quests.logic.Quest.Step;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

public class FinalMessage extends Quest {
	private static final String TAG = "FinalMessage";
	
	@Override
	public void initateQuest() {	
		title_ = Game.mainActivity.getResources().getString(R.string.quest_special_mission_security_alert_title);
		from_ = Game.mainActivity.getResources().getString(R.string.quest_special_mission_security_alert_from);
		fromimg_ = ctx_.getResources().getString(R.string.quest_profile_anthony);
		
		setBasicMessage(R.string.quest_special_mission_security_alert);
		
		setFullMessageText(getMessageWithNarrations());

		setAccepted();
		
		
		Game.getGame().showNewMessageAlert(this);
	}
	
	@Override
	public void initiateQuestBackground() {
		setCompleted();
		Game.saveGame();
		
	}
	
	@Override
	public void onMessageOpen() {
		Game.getGame().getAchivContainer().getAchivGameFinished().achive();
		initiateQuestBackground();
	}
	
}
