package com.jutsugames.modernwizzards.quests.logic;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.os.CountDownTimer;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.spells.OutOfBodyExperience;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class GoToPointQuest extends Quest {
	private static final String TAG = "GoToPointQuest";
	private transient List<TriggerZone> triggerZones;
	
	@Override
	public void initateQuest() {
		title_ = ctx_.getResources().getString(R.string.gotopoint_quest_title);
		from_ = ctx_.getResources().getString(R.string.gotopoint_quest_from);
		fromimg_ = ctx_.getResources().getString(R.string.quest_profile_william);
		
		setBasicMessage(R.string.gotopoint_quest_content);
		
		setFullMessageText(getMessageWithNarrations());

		setAccepted();
		
		Game.getGame().showNewMessageAlert(this);
	}
	
	@Override
	public void initiateQuestBackground() {
		Random rnd = new Random();
		int startingBearing = rnd.nextInt(90);
		
		GeoPoint gp_player = Game.getGame().playerEgo.getGeoPoint();
		
		triggerZones = new LinkedList<TriggerZone>();
		for(int i = 0; i < 4; ++i) 
		{
			TriggerZone tz = new TriggerZone("GoToPointQuestZone", 
					new GeoPoint(
							gp_player.getLatitudeE6(), 
							gp_player.getLongitudeE6()), 
					40, 
					ctx_.getResources().getString(R.string.target));
			tz.moveObject(120.0, 90*i + startingBearing);
			tz.display(true);
			Game.getGame().worldContainer.addQuestObject(tz, true);
			triggerZones.add(tz);
		}

	}
	
	@Override
	public Set<PostProcessingRequestType> handleEvent(String objID, EventTypes et,
			Map<String, Object> extraParams) {
		super.handleEvent(objID, et, extraParams);
		if(acceptanceState_ == AcceptanceState.ACCEPTED && 			//Entered first area with normal walking
				progressState_ == ProgressState.INPROGRESS && 
				"GoToPointQuestZone".equals(objID) &&
				et == EventTypes.VISITED && 
				completedSteps_.size() == 0 &&
				!Game.getGame().playerEgo.isUnderEffectOfSpell("spell_oob") 
				) {
			
			completedSteps_.add(new Step(1,new Date(),"gotopoint_quest_narration_1"));
			
			
			for( TriggerZone tz : triggerZones) {
				Game.getGame().worldContainer.removeQuestObject(tz);
				tz.display(false);
			}
			triggerZones.clear();

			showNewInQuestMessage(R.string.gotopoint_quest_narration_1);
			
			setFullMessageText(getMessageWithNarrations());
			
			//Preparing next part of quest. Using oob.
			TriggerZone tz = new TriggerZone("GoToPointWithOOBQuestZone", 
					Player.getInst().getGeoPoint(), 
					40, 
					ctx_.getResources().getString(R.string.target) );
			tz.moveObject(200.0, 180);
			tz.display(true);
			
			Game.getGame().worldContainer.addQuestObject(tz, true);
			Game.getGame().playerEgo.addKnownSpell("spell_oob");
			triggerZones.add(tz);
			
			Game.turnOnOffNotificationForButton(R.id.btnSpells, true);
		
			return getResultSet(PostProcessingRequestType.REFRESHMAP);
			
		} else 	if(acceptanceState_ == AcceptanceState.ACCEPTED &&  //Entered second area with oob
				progressState_ == ProgressState.INPROGRESS && 
				"GoToPointWithOOBQuestZone".equals(objID) &&
				et == EventTypes.VISITED && 
				completedSteps_.size() == 1 &&
				Game.getGame().playerEgo.isUnderEffectOfSpell("spell_oob") 
				) {
			completedSteps_.add(new Step(2,new Date(),"gotopoint_quest_narration_2"));
			
			Game.getGame().playerEgo.endOutOfBody(false);
			
			for( TriggerZone tz : triggerZones) {
				Game.getGame().worldContainer.removeQuestObject(tz);
				tz.display(false);
			}
			triggerZones.clear();
			
			showNewInQuestMessage(R.string.gotopoint_quest_narration_2, 0, 0);
			
			setFullMessageText(getMessageWithNarrations());
			
			HashSet<PostProcessingRequestType> result = new HashSet<PostProcessingRequestType>();
			result.add(PostProcessingRequestType.REFRESHMAP);
			return result;
			
		} else 	if(acceptanceState_ == AcceptanceState.ACCEPTED &&   //Entered second area with normal walking (player should do this with oob)
				progressState_ == ProgressState.INPROGRESS && 
				"GoToPointWithOOBQuestZone".equals(objID) &&
				et == EventTypes.VISITED && 
				completedSteps_.size() == 1 &&
				!Game.getGame().playerEgo.isUnderEffectOfSpell("spell_oob") 
				) {
			//toasty w questach wywalaja teraz wyjatki, niewiedziec czemu.
			MWToast.showToast(R.string.gotopoint_quest_have_to_use_oob, Toast.LENGTH_SHORT);
		} else if (acceptanceState_ == AcceptanceState.ACCEPTED &&  //Entered second area with oob
				progressState_ == ProgressState.INPROGRESS && 
				"spell_oob".equals(objID) &&
				et == EventTypes.BACK_IN_BODY && 
				completedSteps_.size() == 2) {
			setCompleted();
			
			Player.getInst().addExperience(200);
			Game.saveGame();
		}
		$log.d(TAG, "nie wiadomo co robic!!");
		return null;
	}
	
	@Override
	public void onMessageOpen() {
		super.onMessageOpen();
		if(progressState_ == ProgressState.INPROGRESS && 
				completedSteps_.size() < 1) { //Player started to play and didn't visited any triggered area.
			Game.turnOnOffNotificationForButton(R.id.btnMap, true);
		}
	}

}