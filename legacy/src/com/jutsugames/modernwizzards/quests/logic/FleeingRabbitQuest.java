package com.jutsugames.modernwizzards.quests.logic;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.content.Intent;
import android.os.CountDownTimer;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.creatures.Bunny;
import com.jutsugames.modernwizzards.gamelogic.mixtures.InvisibilityPotion;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.logic.Quest.AcceptanceState;
import com.jutsugames.modernwizzards.quests.logic.Quest.ProgressState;
import com.jutsugames.modernwizzards.quests.logic.Quest.Step;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

public class FleeingRabbitQuest extends Quest {
	private static final String TAG = "FleeingRabbitQuest";
	private transient final String fleeingRabbitCloseZoneID = "FleeingRabbitCloseZone";
	private transient TriggerZone fleeingRabbitCloseZone;
	private transient Bunny fleeingRabbit;
	private transient Random rnd;
	
	
	@Override
	public void initateQuest() {
		title_ = ctx_.getResources().getString(R.string.fleeing_rabbit_quest_title);
		from_ = ctx_.getResources().getString(R.string.fleeing_rabbit_quest_from);
		fromimg_ = ctx_.getResources().getString(R.string.quest_profile_william);
		
		setBasicMessage(R.string.fleeing_rabbit_quest_content);
		
		setFullMessageText(getMessageWithNarrations());

		setAccepted();
	
		Game.getGame().showNewMessageAlert(this);
	}
	
	@Override
	public void initiateQuestBackground() { 
		rnd = new Random();
		
		GeoPoint gp_player = Game.getGame().playerEgo.getGeoPoint();
		
		fleeingRabbitCloseZone = new TriggerZone(fleeingRabbitCloseZoneID, 
				new GeoPoint(
					gp_player.getLatitudeE6(), 
					gp_player.getLongitudeE6() ), 
				90, ctx_.getResources().getString(R.string.target));
		fleeingRabbitCloseZone.moveObject(150.0 + rnd.nextInt(100), rnd.nextInt(360));
		
		Game.getGame().worldContainer.addQuestObject(fleeingRabbitCloseZone, false);
		fleeingRabbitCloseZone.display(true);
	}
	
	@Override
	public Set<PostProcessingRequestType> handleEvent(String objID, EventTypes et,
			Map<String, Object> extraParams) {
		super.handleEvent(objID, et, extraParams);
		
		if(acceptanceState_ != AcceptanceState.ACCEPTED || 
				progressState_ != ProgressState.INPROGRESS) return null;
		
		if(		et == EventTypes.VISITED &&
				fleeingRabbitCloseZoneID.equals(objID) ) { //Player reached triggered zone. Add rabbit exact location
			
			Game.getGame().worldContainer.removeQuestObject(fleeingRabbitCloseZone);
			fleeingRabbitCloseZone.display(false);
			
			fleeingRabbit = new Bunny();
			fleeingRabbit.attackWithoutWarning = true;
			fleeingRabbit.setLocation(fleeingRabbitCloseZone.getGeoPoint());
			fleeingRabbit.moveObject(rnd.nextInt(70), rnd.nextInt(360));
			fleeingRabbit.display(true);
			Game.getGame().worldContainer.addQuestObject(fleeingRabbit, false);
			
			showNarrationMessage(R.string.fleeing_rabbit_narration_close_to_rabbit);
			
		} else if(	et == EventTypes.KILLED &&
					fleeingRabbit.nameID.equals(objID)) { //Player killed rabbit
			
			showNarrationMessage(R.string.fleeing_rabbit_narration_rabbit_killed);
			
			//TODO: poni�sze trzeba pokaza showNewInQuestMessage jak si� narracje przeczyta.
			completedSteps_.add(new Step(1 ,new Date(), "fleeing_rabbit_quest_end"));
			setFullMessageText(getMessageWithNarrations());
			Game.turnOnOffNotificationForButton(R.id.btnQuests, true);
			
			//Game.getGame().playerEgo.hasCreaturesOnMap = true;
			//for(PhysicalObject item : Game.getGame().worldContainer.otherObjects)
			//	item.display(false);
			//TODO: potrzebne?
 			//Game.getGame().worldContainer.otherObjects.clear(); //Hope to have only plants here during this quest
			//Game.getGame().worldContainer.clearGeneratedSlabs();
			
			
			//w��czenie generacji kr�lik�w i kot�w
			GameContent.getInstance().knownCreatures.add("creature_bunny");
			GameContent.getInstance().knownCreatures.add("creature_black_cat");
			GameContent.getInstance().knownCreatures.add("creature_strumpf");
			
			Player.getInst().addExperience(500);
			setCompleted();
			Game.saveGame();
		}
		return null;
	}
	
	@Override
	public void onMessageOpen() {
		super.onMessageOpen();
		if(progressState_ == ProgressState.INPROGRESS && 
				completedSteps_.size() == 0) {
			$log.d(TAG, "Message opened sending broadcast");
			Game.turnOnOffNotificationForButton(R.id.btnMap, true);
		}
	}
}
