package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.LinkedList;
import java.util.List;

import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.ItemsAggregation;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.SpellFactory;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class SpellsActivity extends MWBaseActivity {
	private static final String TAG = "SpellsActivity";

	private ListView spellsView;
	private LayoutInflater inflater;
	Menubutton checkGlobal;
	Menubutton checkBattle;
	
	/** jak nieprawda to pokaz bitewne*/
	public boolean showGlobal = true;
	
	private ArrayAdapter<String> spellsAdapter;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        $log.d(TAG, "SpellsActivity starts");
        
        //showGlobal=false;
        
        setContentView(R.layout.spells);
        
        spellsView = (ListView) findViewById(R.id.spellsList);
        
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        	
        Menubutton backButton = (Menubutton) findViewById(R.id.spells_ButtonBack);
        backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		checkGlobal = (Menubutton) findViewById(R.id.check_global);
		checkBattle = (Menubutton) findViewById(R.id.check_battle);
		checkGlobal.isDown = showGlobal;
		checkGlobal.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showGlobal=!showGlobal;
				checkGlobal.isDown=showGlobal;
				checkBattle.isDown=!showGlobal;
				checkGlobal.animatee();
				refreshListView();
			}
		});
		checkBattle.isDown = !showGlobal;
		checkBattle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showGlobal=!showGlobal;
				checkGlobal.isDown=showGlobal;
				checkBattle.isDown=!showGlobal;
				checkBattle.animatee();
				refreshListView();
			}
		});
        
		/*
		 * Initialize spells adapter
		 */
		spellsAdapter = new ArrayAdapter<String>(this, R.layout.spell_item,
				getSpells()) {

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View row;

				if (null == convertView) {
					row = inflater.inflate(R.layout.spell_item, null);
				} else {
					row = convertView;
				}

				final String spellID = getItem(position);

				Castable spell = SpellFactory.getSpellByID(
						spellID, Game.getGame().playerEgo);
				
				ObjectInfo objectInfo = spell.getInfo();

				ImageView iv = (ImageView) row
						.findViewById(R.id.spell_item_image);
				iv.setImageResource(objectInfo.getImageID());

				TextView tv = (TextView) row
						.findViewById(R.id.spell_item_description);
				tv.setText(objectInfo.getName() + "\n"
						+ objectInfo.getDescription());

				 
				TextView manaCost = (TextView) row.findViewById(R.id.spell_item_cost);
				manaCost.setText(""+spell.getRequiredMana());

				View castButton = row.findViewById(R.id.spell_cast_button);
				if(showGlobal)	{
					castButton.setOnClickListener(new OnClickListener() {
	
						@Override
						public void onClick(View v) {
							if(!showGlobal) return;
							Castable spell = SpellFactory.getSpellByID(
									spellID, Game.getGame().playerEgo);
							spell.castThis(); // TODO: by� mo�e jaka� logika jest tu
												// jeszcze potrzebna
						}
					});
				}
				return row;
			}
		};
		spellsView.setAdapter(spellsAdapter);

	}
    
    private LinkedList<String> getSpells()	{
    	System.out.println("sprawdzam czary: " + showGlobal);
    	if(showGlobal)	{
    		System.out.println("sprawdzam czary: " + showGlobal);
    		return (LinkedList<String>) Game.getGame().playerEgo.getKnownSpells().clone();
    	} else {
    		System.out.println("sprawdzam czary: " + showGlobal);
    		return (LinkedList<String>) Game.getGame().playerEgo.getKnownBattleSpells().clone();
    	}
    }
    
	@Override
	public void onResume() {
		Game.turnOnOffNotificationForButton(R.id.btnSpells, false);
		super.onResume();
	}

	public void refreshListView() {
		spellsAdapter.clear();
		for(String spl : getSpells())	{
			spellsAdapter.add(spl);
		}	
		spellsAdapter.notifyDataSetChanged();
		spellsView.invalidate();
	}
}
