package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.ItemsAggregation;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;

public class InventoryActivity extends MWBaseActivity {
	private static final int REQUEST_CODE = 0;
	private static final int REQUEST_CODE_ALCHEMY = 1;
	private static final String TAG = "InventoryActivity";
	public static final String PARAM_FIGHT_MODE = "PARAM_FIGHT_MODE";

	public static Boolean SHOW_ITEMS = true;
	public static Boolean SHOW_MIXTURES = true;

	public boolean fightMode = false;

	private static InventoryItem chosenOne;
	ListView inventoryView;
	ArrayAdapter<InventoryItem> invAdapter;

	private Menubutton alchemyButton, checkMixtures, checkItems;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		$log.d("InventoryActivity", "InventoryActivity starts");
		final Context ctx = this;
		setContentView(R.layout.inventory_dialog);
		inventoryView = (ListView) findViewById(R.id.inventoryList);

		final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (getIntent().getExtras()!=null)	{
			if(getIntent().getExtras().getBoolean(PARAM_FIGHT_MODE))	{
				fightMode=true;
				SHOW_ITEMS = false;
				SHOW_MIXTURES = true;
			}
		}
		else {
			SHOW_ITEMS = true;
			SHOW_MIXTURES = true;
		}

		Menubutton backButton = (Menubutton) findViewById(R.id.inventory_ButtonBack);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		alchemyButton = (Menubutton) findViewById(R.id.inventory_toAlchemy);
		if (!GameContent.getInstance().isAlchemyActive || fightMode) {
			alchemyButton.isDown = true;
		}
		alchemyButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (GameContent.getInstance().isAlchemyActive && !fightMode) {
					alchemyButton.animatee();
					startActivityForResult(new Intent(ctx,
							AlchemyActivity.class), REQUEST_CODE_ALCHEMY);
				} else {
					MWToast.makeToast(Game.getGlobalTopmostActivity(),
							R.string.alchemy_not_available, Toast.LENGTH_SHORT)
							.show();
				}
			}
		});

		checkMixtures = (Menubutton) findViewById(R.id.check_mixtures);
		checkMixtures.isDown = SHOW_MIXTURES;
		checkMixtures.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (fightMode)
					return;
				checkMixtures.animatee();
				SHOW_MIXTURES = !SHOW_MIXTURES;
				checkMixtures.isDown = SHOW_MIXTURES;
				if (!SHOW_ITEMS && !SHOW_MIXTURES)
					checkItems.isDown = SHOW_ITEMS = true;
				refreshListView();
			}
		});

		checkItems = (Menubutton) findViewById(R.id.check_items);
		checkItems.isDown = SHOW_ITEMS;
		checkItems.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (fightMode)
					return;
				checkItems.animatee();
				SHOW_ITEMS = !SHOW_ITEMS;
				checkItems.isDown = SHOW_ITEMS;
				if (!SHOW_ITEMS && !SHOW_MIXTURES) {
					checkMixtures.isDown = SHOW_MIXTURES = true;
				}
				refreshListView();
			}
		});

		//TODO: Jarek: zmieni� wyst�pienia registerAdapter na model jak w li�cie quest�w
		// Asynchoroniczne powiadomienia do adapter�w zamiast dodawanie ich do kolekcji.
		invAdapter = new ArrayAdapter<InventoryItem>(this,
				R.layout.inventory_item,
				Game.getGame().playerEgo.inv.getDisplayList()) {
			private boolean adapterRegistered = false;

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				
				if (!adapterRegistered) {
					Game.getGame().playerEgo.inv.registerAdapter(this);
					adapterRegistered = true;
				}

				View row;

				if (null == convertView) {
					row = inflater.inflate(R.layout.inventory_item, null);
				} else {
					row = convertView;
				}

				InventoryItem ii = (InventoryItem) getItem(position);

				ImageView iv = (ImageView) row
						.findViewById(R.id.inventory_item_image);
				iv.setImageResource(ii.getInfo().getImageID());

				TextView tv = (TextView) row
						.findViewById(R.id.inventory_item_description);
				tv.setText(ii.getInfo().getName());

				if ((ii instanceof ItemsAggregation)
						&& ((ItemsAggregation) ii).count > 1) {
					row.findViewById(R.id.inventory_item_count_tag)
							.setVisibility(View.VISIBLE);
					((TextView) row.findViewById(R.id.inventory_item_count_no))
							.setText("" + ((ItemsAggregation) ii).count);
				} else
					row.findViewById(R.id.inventory_item_count_tag)
							.setVisibility(View.GONE);

				/*if (ii instanceof SpellToken) {
					row.findViewById(R.id.inventory_item_bcg)
							.setBackgroundColor(((SpellToken) ii).getColor());
					if (ii.getInfo().getImageID() == R.drawable.unknown) {
						iv.setImageDrawable(getResources().getDrawable(
								R.drawable.potion_uni));
					}
				}*/

				return row;
			}
		};
		inventoryView.setAdapter(invAdapter);

		final Intent i = new Intent(this, InventoryDetailViewActivity.class);

		inventoryView
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						InventoryItem ii = Game.getGame().playerEgo.inv
								.getDisplayList().get(arg2);
						
						if(fightMode && ii instanceof SpellToken)	{
							//w fight mode odrazu wypijac
							((SpellToken)ii).castThis();
							refreshListView();
						}
						else	{
							chosenOne = ii;
							// i.putExtra("info_name", ii.getInfo().getID());
							InventoryDetailViewActivity.toDisplay = ii;
							startActivityForResult(i, REQUEST_CODE);
						}

					}
				});
	}
	
	@Override
	public void onResume() {
		Game.turnOnOffNotificationForButton(R.id.btnInventory, false);
		super.onResume();
	}

	public void refreshListView() {
		Game.getGame().getInventory().notifyAdapters();
		invAdapter.clear();
		// cholernie potrzebne. Samo notifyDataSetChanged tylko przemalowuje
		// istniejaca obiekty.
		for (InventoryItem iv : Game.getGame().playerEgo.inv.getDisplayList()) {
			invAdapter.add(iv);
		}
		invAdapter.notifyDataSetChanged();
		inventoryView.invalidateViews();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
			if (data.hasExtra("returnValue") && data.hasExtra("info_name")) {
				$log.d(TAG,
						"Incoming result from activity "
								+ data.getStringExtra("returnValue") + " "
								+ data.getStringExtra("info_name"));
				if (data.getStringExtra("returnValue") != null) {
					if ("useThis".equals(data.getStringExtra("returnValue"))
							&& InventoryActivity.chosenOne != null) {
						if (InventoryActivity.chosenOne instanceof SpellToken) {
							$log.d(TAG, "using mixture "
									+ InventoryActivity.chosenOne.getInfo()
											.getName());
							((SpellToken) InventoryActivity.chosenOne)
									.castThis();
							// inv.removeMixture((SpellToken) ii);
						}
					}
				}
			}

		}
		refreshListView();
	}

}
