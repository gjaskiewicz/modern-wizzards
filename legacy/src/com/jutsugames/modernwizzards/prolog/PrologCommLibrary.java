package com.jutsugames.modernwizzards.prolog;
import alice.tuprolog.Library;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Var;


public class PrologCommLibrary extends Library {
	
	public boolean is_dead_1 (Term who) {
		return false;
	}
	
	public boolean obj_info_1 (Term who) {
		return true;
	}
	
	public boolean time_hour_1 (Term varTerm) {
		return true;
	}
	
	public boolean call_method_1 (Term who, Term method, Term args) {
		return true;
	}

	public boolean know_1 (Term spellname) {
		//System.out.println("knows?: "+spellname.isAtom()+" "+spellname+" "+spellname.getClass());
		
		if (spellname.isAtom()) {
			String sName = "";
			
			if (spellname instanceof Struct) {
				sName = ""+spellname;
			} else if (spellname instanceof Var) {
				Var v = (Var)spellname;
				sName = ""+v.getTerm();
			}
			
			if ("nothing".equals(sName)) {
				return true;
			}

		}
		
		return false;
	}
	
	public boolean activespell_1 (Term spellname) {
		System.out.println("activespell?: "+spellname.isAtom()+" "+spellname+" "+spellname.getClass());
		
		if (spellname.isAtom()) {
			String sName = "";
			
			if (spellname instanceof Struct) {
				sName = ""+spellname;
			} else if (spellname instanceof Var) {
				Var v = (Var)spellname;
				sName = ""+v.getTerm();
			}
			
			if ("absolute_wisdom".equals(sName)) {
				System.out.println("OK");
				return true;
			}

		}
		
		return false;
	}
	
}
