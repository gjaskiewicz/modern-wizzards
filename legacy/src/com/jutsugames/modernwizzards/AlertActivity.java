package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Arrays;
import java.util.LinkedList;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.controller.AlertBulider;
import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;
import com.jutsugames.modernwizzards.objects.PlantFactory;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.quests.display.QuestsMessageListActivity;
import com.jutsugames.modernwizzards.quests.logic.Message;
import com.jutsugames.modernwizzards.quests.logic.Quest;
import com.jutsugames.modernwizzards.quests.logic.QuestsEngine;

public class AlertActivity extends MWBaseActivity {
	
	public static LinkedList<AlertBulider> alertContent;
	
	protected static final String TAG = "AlertActivity";
	public static final String ALERT_TYPE = "alert_type";
	public static final String ALERT_TYPE_BUILT = "built";
	public static final String ALERT_TYPE_MESSAGE = "message";
	public static final String ALERT_TYPE_CREATURE = "creature";
	public static final String ALERT_TYPE_ITEM = "item";
	
	//do wyswietlania wiadomosci
	public Quest linkedQuest;
	//do wysiwetlania powiadomien o stworkach lub itemach
	protected PhysicalObject objectDisplayed;
	
	protected Menubutton buttonYes;
	protected Menubutton buttonNo;
	private ImageView picView;
	private TextView objectName;
	private TextView actionQuestion;

	private AlertBulider ab;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        $log.d(TAG, "AlertActivity starts");
        
        this.setContentView(R.layout.alert_dialog);        
        
        buttonYes = (Menubutton) findViewById(R.id.ButtonYes);
        buttonNo = (Menubutton) findViewById(R.id.ButtonNo);
        picView = (ImageView) findViewById(R.id.alertImage);
        objectName = (TextView) findViewById(R.id.alertCreatureName);	
        actionQuestion = (TextView) findViewById(R.id.alertActionQuestion);
        actionQuestion.setVisibility(View.GONE);
        
        Bundle ext = getIntent().getExtras();
        if(ext.getString(ALERT_TYPE).equals(ALERT_TYPE_MESSAGE)) {
        	setUpForMessage();
        } else if(ext.getString(ALERT_TYPE).equals(ALERT_TYPE_BUILT)) {
        	setUpForBuilt(ext.getInt("builtAlertIdx"));
        }
        else {
	        ObjectInfo oi = objectDisplayed.getInfo();        
			//Drawable visPic = getResources().getDrawable(oi.getImageID());
			//picView.setImageDrawable(visPic);
			picView.setImageResource(oi.getImageID());
			
			objectName.setText(oi.getName());
	    }
        
        linkedQuest = QuestsEngine.getInstance().getQuestByClassName(ext.getString("questClassName"));
    }  
    
	private void setUpForBuilt(int builderIdx) {
		ab = alertContent.get(builderIdx);
		TextView t1 = (TextView) findViewById(R.id.alertTitle);	
		t1.setText(ab.message);
		if(ab.objName == null) 	{
			objectName.setVisibility(View.GONE);
		}
		else {
			objectName.setText(ab.objName);
		}
		picView.setVisibility(View.GONE);
		actionQuestion.setVisibility(View.GONE);
		
		//String tx = getResources().getString(ab.yesButtonTx);
		buttonYes.setText(ab.yesButtonTx);
		//tx = getResources().getString(ab.noButtonTx);
		buttonNo.setText(ab.noButtonTx);
		
		if(ab.infoOnly)	{
			buttonNo.setVisibility(View.GONE);
		}
		
		buttonYes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ab.onYesClicked();
				finish();
			}
		});
		buttonNo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ab.onNoClicked();
				finish();
			}
		});
	}

	private void setUpForMessage() {
		TextView t1 = (TextView) findViewById(R.id.alertTitle);	
		t1.setText(R.string.alert_message_title);
		objectName.setVisibility(View.GONE);
		picView.setVisibility(View.GONE);
		actionQuestion.setVisibility(View.GONE);
		
		buttonYes.setText(R.string.alert_message_show);
		buttonNo.setText(R.string.alert_message_later);
		buttonYes.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	$log.d(TAG, "AlertActivity FIGHT clicked");
	        	//objectDisplayed.runningDialog.dismiss(); 
	        	QuestsMessageListActivity.displayMessageThread(
	        			Game.getGame().getGlobalTopmostActivity(), 
	        			linkedQuest);
	        	finish();
	        }}
		);	
		//dodaj listenera do buttona no
		buttonNo.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	finish();
	        }}
		);	
	}

	@Override
	public void onBackPressed() {
		$log.d(TAG, "AlertActivity BACK clicked");
		if(ab!=null && ab.infoOnly)	{
			ab.onYesClicked();
		}
		super.onBackPressed();
	}
    
	public static void showNewAlert(Activity ctx, AlertBulider content)	{
		Intent i = new Intent(ctx, AlertActivity.class);
		if(alertContent==null)	
			alertContent = new LinkedList<AlertBulider>();
		alertContent.add(content);
		int idx = alertContent.lastIndexOf(content);
		i.putExtra("builtAlertIdx", idx);
		i.putExtra(ALERT_TYPE, ALERT_TYPE_BUILT);
		ctx.startActivity(i);  
	}
}
