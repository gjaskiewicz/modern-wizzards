package com.jutsugames.modernwizzards;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.Inventory;
import com.jutsugames.modernwizzards.gamelogic.ItemsAggregation;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class RecepturesActivity extends MWBaseActivity {
	private static final String TAG = "RecepturesActivity";
	
	ListView potionsView;
	ArrayAdapter<String> potionsAdapter;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        $log.d("CreatureActivity", "CreatureActivity starts");
        final Context ctx = this;
        setContentView(R.layout.creature_dialog);
        potionsView = (ListView) findViewById(R.id.creatureList);
        
        ((TextView)findViewById(R.id.listTitle)).setText(R.string.list_title_receptures);
        
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        //zaladuj liste creature do listView za pomoca adaptera
        final LinkedList<String> allPotions = new LinkedList<String>();
        for(String potion : GameContent.getInstance().knownPotions) 
        	allPotions.add(potion);
        //(String[])(allPotions.toArray());
        potionsAdapter = new ArrayAdapter<String>(this, R.layout.creature_item, allPotions )
        		{
        	
        	@Override
        	public View getView(int position, View convertView, ViewGroup parent) {
        		
        		View row;
        		 
        		if (null == convertView) {
        			row = inflater.inflate(R.layout.creature_item, null);
        		} else {
        			row = convertView;
        		}
        		
        		ObjectInfo oi = ObjectInfoFactory.get().getInfoStandarized(getItem(position));
        		
        		//Creature ii = (Creature) getItem(position);
        		ImageView iv = (ImageView) row.findViewById(R.id.creature_item_image);
        		iv.setImageResource(oi.getImageID());
        		
        		TextView tv = (TextView) row.findViewById(R.id.creature_item_description);        		
        		tv.setText(oi.getName());
        		
        		return row;
        	}    
        };
        
        potionsView.setAdapter(potionsAdapter);
        final Intent i = new Intent(this, ReceptureDetailViewActivity.class);
		
        
        potionsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				String receptureName = allPotions.get(arg2);
				//Creature ii = GameContent.getInstance().getAllCreaturesTypes().get(arg2);
				i.putExtra(ReceptureDetailViewActivity.POTION_NAME, receptureName);
				//CreatureDetailViewActivity.toDisplay = ii;
				startActivity(i);
			}
         });          
    }
    
}
