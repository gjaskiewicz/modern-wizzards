package com.jutsugames.modernwizzards.util;

import java.util.Map;
import java.util.Set;

import com.jutsugames.modernwizzards.objects.ObjectInfo;

/**
 * @author Jarek
 *
 */
public interface ChangeListener {
	public enum EventTypes {
		USED,
		CASTED,
		KILLED,
		COLLECTED,
		VISITED,
		EXITED,
		CREATED_BY_ALCHEMY,
		MANA_FULL,
		BACK_IN_BODY,
		MESSAGE,
		QUEST_FINISHED,
	};
	
	public enum PostProcessingRequestType {
		REFRESHMAP,
		REFRESHINVENTORY
	}
	
	/**
	 * 
	 * @param objID
	 * @param et
	 * @param extraParams
	 * @return what to post process in UI thread. I.e refresh map view. 
	 */
	public Set<PostProcessingRequestType> handleEvent(String objID, 
							EventTypes et, 
							Map<String, Object> extraParams );
}
