package com.jutsugames.modernwizzards.util;

import java.util.HashMap;
import java.util.Map;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class SoundsUtil {
	
	static SoundPool sp;
	private static float volume;
	private static Map<Integer, Integer> loadedSounds;
	
	public static void playSound(int soundResID, Context ctx)	{
		if(!Options.PLAY_SOUNDS) return;
		if(ctx == null) ctx = Game.mainActivity;
		if(sp==null) {
			sp = new SoundPool(10, AudioManager.STREAM_MUSIC, 0 );
			AudioManager audioManager = (AudioManager) ctx.getSystemService(ctx.AUDIO_SERVICE);
	        volume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
	        if(Game.mainActivity!=null)
	        	Game.mainActivity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
	        loadedSounds = new HashMap<Integer, Integer>();
		}
		if(!loadedSounds.containsKey(soundResID))	{
			int soundID = sp.load(ctx, soundResID, 1);
			loadedSounds.put(soundResID, soundID);
			sp.play(soundID, volume, volume, 1, 0 , 1f);
		}
		else {
			int streamid = sp.play(loadedSounds.get(soundResID), 1, 1, 1, 0 , 1f);
			$log.d("SOUND", "Playing sound stream id: " + streamid);
		}	
	
		
	}
}
