package com.jutsugames.modernwizzards.util;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;

import android.content.Context;
import android.os.Vibrator;

public class VibratorUtil {
	static Vibrator v;
	
	public static void vibrate(long[] pattern, int repeat)	{
		if(!Options.USE_VIBRATIONS) return;
		if(v==null)	{
			v = (Vibrator) Game.getGame().getGlobalTopmostActivity().getSystemService(Context.VIBRATOR_SERVICE);
		}
		v.vibrate(pattern, repeat);
	}
}
