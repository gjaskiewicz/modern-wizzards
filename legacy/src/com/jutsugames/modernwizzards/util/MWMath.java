package com.jutsugames.modernwizzards.util;

public class MWMath {
	private static double sinValues[]; 
	
	public static double sin(int deg)	{
		deg = deg%360;
		if(deg<0) deg+=360;
		if(sinValues == null)	{
			sinValues = new double[360];
			for(int i = 0 ; i < 360 ; i++)	{
				sinValues[i] = Math.sin(i*Math.PI/180);
				//System.out.println("deg: " + i + " sin: " + sinValues[i]);
			}
		}
		return sinValues[deg];
	}
}
