package com.jutsugames.modernwizzards.util;

public interface RestoreFromSave {
	public void onRestoreFromSave();
}
