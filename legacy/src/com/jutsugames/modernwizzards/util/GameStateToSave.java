package com.jutsugames.modernwizzards.util;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.ModernWizzardsActivity;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.quests.logic.Quest;
import com.jutsugames.modernwizzards.quests.logic.QuestsEngine;

public class GameStateToSave {
	public Game game;
	public QuestsEngine questsEngine;
	public GameContent gameContent;
	
	public GameStateToSave() {
	}
	
	public void dumpGameState() {
		game = Game.getGame();
		questsEngine = QuestsEngine.getInstance();
		gameContent = GameContent.getInstance();
		
	}
	
	public void restoreGameState(ModernWizzardsActivity context) {
		game.setRunningContext(context);
		Game.reloadGame(game);
		Quest.initializeContext(context);
		QuestsEngine.reloadQuestsEngine(questsEngine);
		GameContent.reloadGameContent(gameContent);
		Game.getGame().isInitialized = true;
		for(RestoreFromSave rfs : Game.objectsToRestore)
			rfs.onRestoreFromSave();
		Game.objectsToRestore.clear();
		QuestsEngine.getInstance().onRestoreFromSave();
		//Game.getGame().playerEgo.nameID = playerName;
		//Game.getGame().playerEgo = player;
	}
	
}
