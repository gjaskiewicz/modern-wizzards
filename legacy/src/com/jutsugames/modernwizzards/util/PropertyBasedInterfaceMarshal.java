package com.jutsugames.modernwizzards.util;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.jutsugames.modernwizzards.Game;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class PropertyBasedInterfaceMarshal implements JsonDeserializer<Object>,
		JsonSerializer<Object> {
	
	private static final String TAG = "PropertyBasedInterfaceMarshal";

	private static final String CLASS_META_KEY = "CLASS_META_KEY";
	
	@Override
	public JsonElement serialize(Object object, Type type,
			JsonSerializationContext jsonSerializationContext) {
			$log.d(TAG, "Serializing class " + object.getClass().getName());
	       JsonElement jsonEle = jsonSerializationContext.serialize(object, object.getClass());
	       jsonEle.getAsJsonObject().addProperty(CLASS_META_KEY, object.getClass().getCanonicalName());
	       return jsonEle;
	}

	@Override
	public Object deserialize(JsonElement jsonElement, Type type,
			JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
		 	JsonObject jsonObj = jsonElement.getAsJsonObject();
	        String className = jsonObj.get(CLASS_META_KEY).getAsString();
	        $log.d(TAG, "Deserialize class " + className);
	        try {
	            Class<?> clz = Class.forName(className);
	            Object obj = jsonDeserializationContext.deserialize(jsonElement, clz);
	            if(RestoreFromSave.class.isAssignableFrom(clz)) {
	            	//$log.d(TAG, "Adding " + className + " to restore");
	            	Game.objectsToRestore.add((RestoreFromSave)obj);
	            }
	            return obj;
	        } catch (ClassNotFoundException e) {
	        	$log.e(TAG, "deserialization failed: \n" + e);
	            throw new JsonParseException(e);
	        } catch (Exception e) {
	        	$log.e(TAG, "deserialization failed: \n" + e);
	            throw new JsonParseException(e);
	        } 
	        
	}

}
