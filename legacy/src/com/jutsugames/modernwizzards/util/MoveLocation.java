package com.jutsugames.modernwizzards.util;

import android.location.Location;

import com.google.android.maps.GeoPoint;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class MoveLocation {
	private static final String TAG = "MoveGeoPoint";
	/**
	 * Method moves passed GeoPoint with certain distance with certain direction. 
	 * @param gp 
	 * @param distance
	 * @param bearing
	 */
	public static void moveLocation(Location lt, double distance, int bearing) {
		double dist = distance/6371000.0;
		double brng = Math.toRadians(bearing);
		double lat1 = Math.toRadians(lt.getLatitude());
		double lon1 = Math.toRadians(lt.getLongitude());

		double lat2 = Math.asin( Math.sin(lat1)*Math.cos(dist) + Math.cos(lat1)*Math.sin(dist)*Math.cos(brng) );
		double a = Math.atan2(Math.sin(brng)*Math.sin(dist)*Math.cos(lat1), Math.cos(dist)-Math.sin(lat1)*Math.sin(lat2));

		double lon2 = lon1 + a;

		lon2 = (lon2+ 3*Math.PI) % (2*Math.PI) - Math.PI;
		
		lt.setLatitude(Math.toDegrees(lat2));
		lt.setLongitude(Math.toDegrees(lon2));
	}
	
	public static GeoPoint moveGeoPoint(GeoPoint gp, double distance, int bearing) {
		double dist = distance/6371000.0;
		double brng = Math.toRadians(bearing);
		double lat1 = Math.toRadians(gp.getLatitudeE6()/1E6);
		double lon1 = Math.toRadians(gp.getLongitudeE6()/1E6);

		double lat2 = Math.asin( Math.sin(lat1)*Math.cos(dist) + Math.cos(lat1)*Math.sin(dist)*Math.cos(brng) );
		double a = Math.atan2(Math.sin(brng)*Math.sin(dist)*Math.cos(lat1), Math.cos(dist)-Math.sin(lat1)*Math.sin(lat2));

		double lon2 = lon1 + a;

		lon2 = (lon2+ 3*Math.PI) % (2*Math.PI) - Math.PI;
		
		return new GeoPoint((int)(Math.toDegrees(lat2)*1E6), (int)(Math.toDegrees(lon2)*1E6));
	}
}
