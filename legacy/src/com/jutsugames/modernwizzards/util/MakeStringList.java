package com.jutsugames.modernwizzards.util;
import java.util.LinkedList;
import java.util.List;


public class MakeStringList {

	public static String makeList(List<String> str) {
		StringBuilder sbb = new StringBuilder();
		
		sbb.append("[");
		boolean first = true;
		for (String s : str) {
			if (!first) 
				sbb.append(",");
			
			sbb.append(s);
			first = false;
		}
		sbb.append("]");
		
		String sk = sbb.toString();
		int lIdx = sk.lastIndexOf(',');
		char[] c = sk.toCharArray();
		
		if (lIdx > 0) {
			c[lIdx] = '|';
		}
		
		return new String(c);
	}
	
	/*
	public static void main(String[] args) {
		List<String> r = new LinkedList<String>();
		r.add("datura");
		r.add("fallopia");
		r.add("absinthum");
		
		String s = makeList(r);
		
		System.out.println(s);
	}
	*/
}
