package com.jutsugames.modernwizzards.util;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.security.spec.EncodedKeySpec;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.ItemsAggregation;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gis.SpatialZone;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.quests.logic.Quest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
//import android.provider.MediaStore.Files;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class SavesStorage extends SQLiteOpenHelper {
	private static final String TAG = "SavesStorage";
	private static final String SAVES_FOLDER = "ModernWizardsSaves";
	private static final String DATABASE_NAME = "saves.db";
	private static final String EXTERNAL_SAVE = "wizards.svg";
	private static final String KEY = "03A24B75139C060708090A0B0C0D0E0F";
	private static final Long EXTERNAL_SAVE_ID = -1L;
	
	
	private static final int DATABASE_VERSION = 1;
	private static final String TABLE_NAME = "SAVE";
	
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_DATE = "date";
	public static final String COLUMN_PLAYERNAME = "player_name";
	public static final String COLUMN_GAMESTATE = "game_state";
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private Gson gson;
	 
	public class SaveInfo {
		public long id;
		public String title;
		public Date date;
		public String playerName;
		
		public SaveInfo() {}
		
		public SaveInfo(long id, String title, String date, String playerName) {
			this.id = id;
			this.title = title;
			try {
				this.date = (Date)dateFormat.parse(date); 
			} catch (ParseException  e) {
				this.date = null;
			}
			this.playerName = playerName;
		}
		
		public SaveInfo(Cursor cursor) {
			id = cursor.getLong(0);
			title = cursor.getString(1);
			try {
				date = (Date)dateFormat.parse(cursor.getString(2)); 
			} catch (ParseException  e) {
				date = null;
			}
			playerName = cursor.getString(3);
		}

	}
	
	public class Save {
		
		public Save(long id, String save) {
			super();
			this.id = id;
			this.save = save;
		}
		public long id;
		public String save;
		
		
	}
	
	public SavesStorage(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		
		gson = new GsonBuilder().
				registerTypeAdapter(Castable.class, new PropertyBasedInterfaceMarshal()).
				registerTypeAdapter(Spell.class, new PropertyBasedInterfaceMarshal()).
				registerTypeAdapter(Quest.class, new PropertyBasedInterfaceMarshal()).
				registerTypeAdapter(SpatialZone.class, new PropertyBasedInterfaceMarshal()).
				registerTypeAdapter(InventoryItem.class, new PropertyBasedInterfaceMarshal()).
				registerTypeAdapter(PhysicalObject.class, new PropertyBasedInterfaceMarshal()).
				//registerTypeAdapter(QuestsEngine.class, new PropertyBasedInterfaceMarshal()).
				//registerTypeAdapter(GameContent.class, new PropertyBasedInterfaceMarshal()).
				create();
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + TABLE_NAME + " ( " + 
					COLUMN_ID + " integer primary key autoincrement, " +
					COLUMN_TITLE + " text not null, " +
					COLUMN_DATE + " text not null, " +
					COLUMN_PLAYERNAME + " text not null, " +
					COLUMN_GAMESTATE + " text not null );");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		
		onCreate(db);
	}
	
	public void deleteAllSaves() {
		SQLiteDatabase db = getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_NAME);
		db.close();
	}
	
	private void deleteLastSaves(SQLiteDatabase db, long lastAddedSaveID) {
		db.execSQL("DELETE FROM " + TABLE_NAME + " WHERE _id<=" + (lastAddedSaveID-20));
	}
	
	public Save storeSave(GameStateToSave gameState) throws Exception {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		Date today = Calendar.getInstance().getTime();
		String save = gson.toJson(gameState);
		values.put(COLUMN_DATE, dateFormat.format(today));
		values.put(COLUMN_PLAYERNAME, gameState.game.playerEgo.nameID);
		values.put(COLUMN_TITLE, "test save");
		values.put(COLUMN_GAMESTATE, save);
		long result = db.insert(TABLE_NAME, null, values);
		deleteLastSaves(db,result);
		db.close();
		if(result == -1)
			throw new Exception("Problems with saveing game state");
		return new Save(result, save);
	}
	
	public List<SaveInfo> getAllSavesInfo() {
		    List<SaveInfo> saves = new ArrayList<SaveInfo>();
		    SQLiteDatabase db = getReadableDatabase();
		    String[] columns = { COLUMN_ID, COLUMN_TITLE, COLUMN_DATE, COLUMN_PLAYERNAME };
		    
		    Cursor cursor = db.query(TABLE_NAME,
		    		columns, null, null, null, null, null);

		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      SaveInfo saveInfo = new SaveInfo(cursor);
		      saves.add(saveInfo);
		      cursor.moveToNext();
		    }
		    // Make sure to close the cursor
		    cursor.close();
		    return saves;
		  }
	
	public GameStateToSave getGameStateWithID(long id) {
		if(id == EXTERNAL_SAVE_ID) {
			return getGameStateFromExternalFile();
		} else {
			SQLiteDatabase db = getReadableDatabase();
			Cursor cursor = db.query(TABLE_NAME,
		    		new String[] {COLUMN_GAMESTATE}, "_id = ?", new String[] {String.valueOf(id) }, null, null, null);
			cursor.moveToFirst();
			
			String jsonGameState = cursor.getString(0);
			cursor.close();
			return stringToGameState(jsonGameState);
		}
	}
	
	private GameStateToSave stringToGameState(String jsonGameState) {
		//Hold json save for error handling 
		ACRAPostSender.gamestate=new String(jsonGameState.getBytes());
		
		return gson.fromJson(jsonGameState, GameStateToSave.class);
	}

	/**
	 * Calculates list of ids which represents possible order of restoring game states.
	 * Positive values means saves from SQLite internal database. -1 (EXTERNAL_SAVE_ID) means file "save.txt" from phone storage.
	 * 
	 * @return List of saves ids to restore. 
	 */
	public List<Long> getOrderdIdsOfSaves() {
		List<Long> ids = new ArrayList<Long>();
		
		//Add id of external file 
		File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + SAVES_FOLDER +
        		File.separator + EXTERNAL_SAVE);
		if(file.exists() && Options.LOAD_SAVE_FROM_FILE) {
			ids.add(EXTERNAL_SAVE_ID);
		}
		
		
		//Add ids from internal SQLite database
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query(TABLE_NAME,
	    		new String[] {COLUMN_ID, COLUMN_DATE}, null, null, null, null, COLUMN_DATE + " DESC");
		cursor.moveToFirst();
		
		while(!cursor.isAfterLast()) {
			ids.add(cursor.getLong(0));
			cursor.moveToNext();
		}
		
		return ids;
	}
	
	public GameStateToSave getGameStateFromExternalFile() {
		File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + 
        		File.separator + SAVES_FOLDER +  File.separator + EXTERNAL_SAVE);
		if(file.exists()) {
			DataInputStream in = null;
			
			try {
				 byte[] buffer = new byte[(int) file.length()];
		         in = new DataInputStream(new FileInputStream(file));
		         in.readFully(buffer);
		         String result = new String(buffer);
		         if(!result.startsWith("{\"game\":")) {
		        	 result = EncryptDecryptHelper.decrypt(result, KEY);
		         }
				return stringToGameState(result);
			} catch (FileNotFoundException e) {
				$log.e(TAG, "Can't parse seve from external file (FileNotFoundException)");
			} catch (IOException e) {
				$log.e(TAG, "Can't parse seve from external file (IOException)");
			}
			finally {
	            try {
	                in.close();
	            } catch (IOException e) { /* ignore it */ }
	        }
		}
		return null;
	}
	
	public void backUpSavesToExternalStorage() throws Exception {
		 File dbFile =
                 new File(Environment.getDataDirectory() + "/data/com.jutsugames.modernwizzards/databases/"+DATABASE_NAME);
		 
		$log.d(TAG,"ExternalStorageState: " + Environment.getExternalStorageState());
        File exportDir = new File(Environment.getExternalStorageDirectory() + File.separator + SAVES_FOLDER, "");
        if (!exportDir.exists()) {
           if(! exportDir.mkdirs() ) throw new Exception("Can't create directory");
        }
        $log.d(TAG, "Export dir: " + exportDir.getAbsolutePath() + " " + dbFile.getName());
        File file = new File(exportDir.getAbsolutePath(), dbFile.getName());

        file.createNewFile();
        this.copyFile(dbFile, file);
	}
	
	public void saveGameStateToExternalStorage(String gameState, boolean encrypt) {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + 
        		File.separator + SAVES_FOLDER + File.separator + EXTERNAL_SAVE);
        mediaStorageDir.delete();
        
		FileOutputStream fos;
		try {
	        mediaStorageDir.createNewFile();
			fos = new FileOutputStream(mediaStorageDir);
			if(encrypt) {
				gameState = EncryptDecryptHelper.encrypt(gameState, KEY);
			}
			fos.write(gameState.getBytes());
			fos.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void copyFile(File src, File dst) throws IOException {
        FileChannel inChannel = new FileInputStream(src).getChannel();
        FileChannel outChannel = new FileOutputStream(dst).getChannel();
        try {
           inChannel.transferTo(0, inChannel.size(), outChannel);
        } finally {
           if (inChannel != null)
              inChannel.close();
           if (outChannel != null)
              outChannel.close();
        }
     }

}
