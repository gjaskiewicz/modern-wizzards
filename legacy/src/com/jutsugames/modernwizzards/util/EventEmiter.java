package com.jutsugames.modernwizzards.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.util.Log;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.quests.logic.QuestsEngine;
import com.jutsugames.modernwizzards.util.ChangeListener.EventTypes;
import com.jutsugames.modernwizzards.util.ChangeListener.PostProcessingRequestType;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

/**
 * 
 * @author Jarek
 * 
 * Class designed for being implementation of observer programming pattern.
 * It will be mainly used by QuestsEngine (added by default). 
 */

public class EventEmiter {
	private static final String TAG = "EventEmiter";
	private static transient BroadcastReceiver eventsReceiver; //Will receive notification updates of menu buttons
	private static transient IntentFilter eventFilter;
	private static transient Activity activity;
	
	protected transient static List<ChangeListener> listeners = new ArrayList<ChangeListener>();
	
	/**
	 * Methods for Observer/Listener programming pattern
	 */
	public EventEmiter() {
		if(eventFilter == null) {
			eventFilter = new IntentFilter("com.jutsugames.modernwizzards.EVENT");
			eventsReceiver =  new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					String objID = intent.getStringExtra("objID");
					EventTypes et = EventTypes.valueOf(intent.getStringExtra("eventType"));
					Map<String, Object> extraParams = (Map<String, Object>)intent.getSerializableExtra("extraParams");
					$log.d(TAG, "Event to EventEmiter: objID: " + objID );
					notifyListeners(objID, et, extraParams);
					
				}
	        };
	        
		}
		//listeners.add(QuestsEngine.getInstance());
	}
	
	public static void setActivity(Activity activity) {
		if(EventEmiter.activity == activity) return;
		if(EventEmiter.activity != null) {
			try {
				EventEmiter.activity.unregisterReceiver(eventsReceiver);
			} catch (IllegalArgumentException e) {}
		}
		EventEmiter.activity = activity;
		EventEmiter.activity.registerReceiver(eventsReceiver, eventFilter);
	}
	
	
	
	protected void notifyListeners(String objID, ChangeListener.EventTypes et, Map<String,Object> extraParams ) {
		 	//Intent i = new Intent("com.jutsugames.modernwizzards.EVENT").putExtra("objID", objID);
			//Game.mainActivity.sendBroadcast(i);

			new ProcessEvent().execute(objID, et, extraParams);
			
/*		    for (Iterator<ChangeListener> iterator = listeners.iterator(); iterator.hasNext();) {
		    	ChangeListener cl = (ChangeListener) iterator.next();
		    	cl.handleEvent(objID, et , extraParams);
		    }*/
		  }
	
	public static void addChangeListener(ChangeListener newListener) {
		    listeners.add(newListener);
		  }
	
	private  class ProcessEvent extends AsyncTask<Object, Void, Set<PostProcessingRequestType> > {

		@Override
		protected Set<PostProcessingRequestType> doInBackground(Object... params) {
			try {
				String objID = (String) params[0];
				EventTypes et = (EventTypes) params[1];
				@SuppressWarnings("unchecked")
				Map<String,Object> extraParams = (Map<String,Object>) params[2];
				Set<PostProcessingRequestType> result = new HashSet<PostProcessingRequestType>();
				for(ChangeListener cl : listeners) {
					Set<PostProcessingRequestType> tmpRes = cl.handleEvent(objID, et , extraParams);
					if(tmpRes != null) result.addAll(tmpRes);
				}
				
				result.add(PostProcessingRequestType.REFRESHMAP);
				return result;
			}
			catch (Exception e) {
				$log.e(TAG, e.getClass().getName(),e);
				return null;
			}
		}
		
		@Override
		public void onPostExecute(Set<PostProcessingRequestType> result) {
			if(result != null) {
				$log.i(TAG, "Event OK!");
				if(result.contains(PostProcessingRequestType.REFRESHMAP)) Game.getGame().invalidateMapView();
				if(result.contains(PostProcessingRequestType.REFRESHINVENTORY)) Game.getGame().playerEgo.inv.notifyAdapters();
			}
			else 
				$log.i(TAG, "Event FAILED!");
		}
		
	}
}
