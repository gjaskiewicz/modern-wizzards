/**
 * 
 */
package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;

/**
 * @author bartek
 *
 */
public class AlertItemActivity extends AlertActivity {

    public void onCreate(Bundle savedInstanceState) {
    	
    	objectDisplayed = CollectableItem.runningDialogItem;
    	
    	super.onCreate(savedInstanceState);
        $log.d(TAG, "AlertActivity starts");
        
        setForItem();
		
    }  
	
	public void setForItem() {
		TextView t1 = (TextView) findViewById(R.id.alertTitle);	
		t1.setText(R.string.alert_item_title);
		
		buttonYes.setText(R.string.alert_item_take);
		buttonYes.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	$log.d(TAG, "AlertActivity TAKE clicked");
	        	CollectableItem.runningDialog = false;
	        	Game.getGame().collectItem((CollectableItem)objectDisplayed);
	            finish();
	            //onDestroy();
	        }}
		);
		//dodaj listenera do buttona no
		buttonNo.setText(R.string.alert_item_leave);
		buttonNo.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	$log.d(TAG, "AlertActivity LEAVE clicked");
	        	CollectableItem.runningDialog = false;
	        	((CollectableItem)objectDisplayed).playerWasUninterested = true;
	        	finish();
	        	//onDestroy();
	        }}
		);	
	}

	@Override
	public void onBackPressed() {
		$log.d(TAG, "AlertItemActivity BACK clicked");
			CollectableItem.runningDialog = false;
	    	((CollectableItem)objectDisplayed).playerWasUninterested = true;
		super.onBackPressed();
	}


}
