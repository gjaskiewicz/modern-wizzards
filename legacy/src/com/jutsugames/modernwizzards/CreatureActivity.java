package com.jutsugames.modernwizzards;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.Inventory;
import com.jutsugames.modernwizzards.gamelogic.ItemsAggregation;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class CreatureActivity extends MWBaseActivity {
	private static final String TAG = "CreatureActivity";
	
	private static Creature chosenOne;
	ListView creatureView;
	ArrayAdapter<Creature> creatureAdapter;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        $log.d("CreatureActivity", "CreatureActivity starts");
        final Context ctx = this;
        setContentView(R.layout.creature_dialog);
        creatureView = (ListView) findViewById(R.id.creatureList);
        
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        //zaladuj liste creature do listView za pomoca adaptera
        List<Creature> allCreatures = GameContent.getInstance().getAllCreaturesTypes();
        creatureAdapter = new ArrayAdapter<Creature>(this, R.layout.creature_item, allCreatures )
        		{
        	private boolean adapterRegistered = false;
        	
        	@Override
        	public View getView(int position, View convertView, ViewGroup parent) {
        		
        		View row;
        		 
        		if (null == convertView) {
        			row = inflater.inflate(R.layout.creature_item, null);
        		} else {
        			row = convertView;
        		}
        		
        		Creature ii = (Creature) getItem(position);
        		ImageView iv = (ImageView) row.findViewById(R.id.creature_item_image);
        		iv.setImageDrawable(getResources().getDrawable(ii.getInfo(ctx).getImageID()));
        		
        		TextView tv = (TextView) row.findViewById(R.id.creature_item_description);        		
        		tv.setText(ii.getInfo(ctx).getName());
        		
        		return row;
        	}    
        };
        
        creatureView.setAdapter(creatureAdapter);
        

        final Intent i = new Intent(this, CreatureDetailViewActivity.class);
        
        creatureView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Creature ii = GameContent.getInstance().getAllCreaturesTypes().get(arg2);
				chosenOne = ii;
				//i.putExtra("info_name", ii.getInfo().getID());
				CreatureDetailViewActivity.toDisplay = ii;
				startActivityForResult(i, 0);
			}
         });          
    }
    
}
