package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;
import android.app.Activity;
import android.content.SharedPreferences;

public class Options {
	public static transient boolean dontSaveThisTime = false;
	
	public static boolean TERMS_OF_USE_ACCEPTED = false;
	public static boolean GENERATE_TEST_DATA = false;
	//jesli false, uzyta bedzie domyslna lokalizacja (budojo)
	public static boolean USE_GPS = true; 
	//jesli nie ma GPS, zostanie uzyta ta lokalizacja:
	//public static double MOCK_LAT = 52.1448;
	//public static double MOCK_LNG = 21.0213;
	//festiwal w krakowie:
	public static double MOCK_LAT = 50.064427;
	public static double MOCK_LNG = 19.930632;
	public static int LANGUAGE_ID = 100;
	
	public static boolean PLAY_SOUNDS = true; 
	public static boolean USE_VIBRATIONS = true; 
	public static boolean LOCK_VIEW_TO_LOCATION = true;
	public static boolean SAVE_AND_LOAD = true;
	public static boolean DEVELOPMENT_MODE = false; //Faster loading new quests
	public static boolean LOAD_SAVE_FROM_FILE = false; //Reset with every start of application even when file doesn't exists
	public static int MAP_VISUALIZATION=0;
	
	public static final String PREFS_NAME="ModernWizardsPref";
	
	public Options()	{
		
	}
	
	public static void loadOptions(Activity ack)	{
		 SharedPreferences settings = ack.getSharedPreferences(PREFS_NAME, 0);
		 TERMS_OF_USE_ACCEPTED = settings.getBoolean("TERMS_OF_USE_ACCEPTED", false);
		 GENERATE_TEST_DATA = settings.getBoolean("GENERATE_OBJECTS", false);
		 USE_GPS = settings.getBoolean("USE_GPS", true);
		 //USE_GPS = true;
		 PLAY_SOUNDS = settings.getBoolean("PLAY_SOUNDS", true);
		 USE_VIBRATIONS = settings.getBoolean("USE_VIBRATIONS", true);
		 LOCK_VIEW_TO_LOCATION = settings.getBoolean("LOCK_VIEW_TO_LOCATION", false);
		 SAVE_AND_LOAD = settings.getBoolean("SAVE_AND_LOAD", true);
		 //SAVE_AND_LOAD = false;
		 LANGUAGE_ID = settings.getInt("LANGUAGE_ID", 100);
		 DEVELOPMENT_MODE = settings.getBoolean("DEVELOPMENT_MODE", false);
		 LOAD_SAVE_FROM_FILE = settings.getBoolean("LOAD_SAVE_FROM_FILE", false);

		 MAP_VISUALIZATION = settings.getInt("MAP_VISUALIZATION", 0);
	     $log.d("INFO", "O get languageId load Options: " + Options.LANGUAGE_ID);		 
	}
	
    public static void saveOptions(Activity ack)	{
        SharedPreferences settings = ack.getSharedPreferences(Options.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("TERMS_OF_USE_ACCEPTED", TERMS_OF_USE_ACCEPTED);
        editor.putBoolean("GENERATE_OBJECTS", GENERATE_TEST_DATA);
        editor.putBoolean("USE_GPS", USE_GPS);
        editor.putBoolean("PLAY_SOUNDS", PLAY_SOUNDS);
        editor.putBoolean("USE_VIBRATIONS", USE_VIBRATIONS);
        editor.putBoolean("LOCK_VIEW_TO_LOCATION", LOCK_VIEW_TO_LOCATION);
        editor.putBoolean("SAVE_AND_LOAD", SAVE_AND_LOAD);
        editor.putBoolean("DEVELOPMENT_MODE", DEVELOPMENT_MODE);
        editor.putBoolean("LOAD_SAVE_FROM_FILE", LOAD_SAVE_FROM_FILE);
        editor.putInt("MAP_VISUALIZATION", MAP_VISUALIZATION);
        editor.putInt("LANGUAGE_ID", LANGUAGE_ID);        
        $log.d("INFO", "O get languageId before save: " + LANGUAGE_ID);
        
        // Commit the edits!
        editor.commit();
        
        int b = settings.getInt("LANGUAGE_ID", 1);
        $log.d("INFO", "O get languageId after save: " + b);
        $log.d("INFO", "Options.LANGUAGE_ID: " + b);
    }
}
