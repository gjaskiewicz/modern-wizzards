package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.io.File;

import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.creatures.Warewolf;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.Game.GameState;
import com.jutsugames.modernwizzards.PlantActivity;
import com.jutsugames.modernwizzards.CreatureActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class StatsActivity extends MWBaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	final Context ctx = this;
    	
        $log.d("INFO", "StatsActivity onCreate");      
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stats);
        refresh();
        
        Menubutton backButton = (Menubutton) findViewById(R.id.button_menu);
        backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
        
        //uruchamianie PlantActivity
        Menubutton plantButton = (Menubutton) findViewById(R.id.button_plants);
        plantButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ctx, PlantActivity.class));
			}
		});
        
        //uruchamianie CreatureActivity
        Menubutton creatureButton = (Menubutton) findViewById(R.id.button_bestiary);
        creatureButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ctx, CreatureActivity.class));
			}
		});   
        
        Menubutton achivementsButton = (Menubutton) findViewById(R.id.button_achivements);
        achivementsButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ctx, AchivementsActivity.class));
			}
		});  
        
        Menubutton recepturesButton = (Menubutton) findViewById(R.id.button_receptures);
        recepturesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ctx, RecepturesActivity.class));
				//TODO: Zrobic!!
				//MWToast.makeToast(getBaseContext(), "To Be Implemented...", Toast.LENGTH_SHORT).show();
				//(new CollectableItem(null, new CollectPlant("item_fernflower"))).interact();
			}
		});
        
        
		/* Stary stary kod do ustawiania zdjec profilowych
		 * Button btnChange = (Button)statisticsView.findViewById(R.id.btnChangePicture);
		
		btnChange.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				changePicture();
			}
		});
		
		File file = new File( AVATAR_PATH );
		if (file.exists()) {
			BitmapFactory.Options options = new BitmapFactory.Options();
		    options.inSampleSize = 4;

		    Bitmap bitmap = BitmapFactory.decodeFile( AVATAR_PATH, options );
		    ImageView _image = new ImageView(ctx);
		    _image.setImageBitmap(bitmap);
		    
		    FrameLayout pFrame = (FrameLayout)statisticsView.findViewById(R.id.photoFrame);
		    pFrame.addView(_image);
		} else {
			System.out.println("Picture not found: "+AVATAR_PATH);
		}
		*/
        
    }
    
	public void refresh()	{
		findViewById(R.id.barHP).invalidate();
		findViewById(R.id.barMana).invalidate();
		
		TextView spellsList = (TextView) findViewById(R.id.txUnderEffectOfSpells);
		String spells = "";
		for(Spell spl : Game.getGame().playerEgo.underEffectOfSpell)	{
			spells += "-" + spl.getInfo().getName() + "\n";
		}
		if(Game.getGame().playerEgo.underEffectOfSpell.size()==0)	
			spells = getResources().getString(R.string.scr_stats_spells_none);
		spellsList.setText(getResources().getString(R.string.scr_stats_spells_header)+spells);
		
		if(Game.getGame().getGameState() == GameState.STATE_PLAYERDEAD)	{
			//findViewById(R.id.ManaAndHealth).setVisibility(View.GONE);
			spellsList.setText(getResources().getString(R.string.scr_stats_resurect));
		}
		else {
			//findViewById(R.id.ManaAndHealth).setVisibility(View.VISIBLE);
		}
		
	}    
    
	
	/*
		public void changePicture() {
		File file = new File( AVATAR_PATH );
	    Uri outputFileUri = Uri.fromFile( file );

	    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE );
	    intent.putExtra( MediaStore.EXTRA_OUTPUT, outputFileUri );

	    ctx.startActivityForResult( intent, 0 );
	}
	 */
}
