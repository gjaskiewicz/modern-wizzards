package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.HashMap;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.AllSpells;
import com.jutsugames.modernwizzards.CreatureDetailViewActivity;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.InventoryActivity;
import com.jutsugames.modernwizzards.InventoryDetailViewActivity;
import com.jutsugames.modernwizzards.MWBaseActivity;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.PlantDetailViewActivity;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.SpellsActivity;
import com.jutsugames.modernwizzards.Game.GameState;
import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.creatures.Warewolf;
import com.jutsugames.modernwizzards.gamelogic.mixtures.HealingPotion;
import com.jutsugames.modernwizzards.gamelogic.mixtures.ManaPotion;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import com.jutsugames.modernwizzards.quests.logic.Quest;
import com.jutsugames.modernwizzards.quests.logic.QuestsEngine;
import com.jutsugames.modernwizzards.util.ChangeListener.EventTypes;

public class DisclaimerActivity extends MWBaseActivity {
	private static final String TAG = "DisclaimerActivity";
	public static final int TERMS_ACCEPTED = 7777;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.disclaimer);
		$log.d(TAG, "onCreate");

		Menubutton buttonYes = (Menubutton) findViewById(R.id.btnAccept);
		buttonYes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				termsAccepted();
			}
		});
		Menubutton buttonNo = (Menubutton) findViewById(R.id.btnReject);
		buttonNo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				termsRejected();
			}
		});

		WebView results = (WebView) findViewById(R.id.disclaimerText);

		results.loadDataWithBaseURL(null,
				getResources().getString(R.string.disclaimer_text),
				"text/html", "utf-8", null);
		results.setBackgroundColor(0);

	}

	public void termsRejected() {
		finish();
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public void termsAccepted() {
		Options.TERMS_OF_USE_ACCEPTED = true;
		Options.saveOptions(this);
		//ModernWizzardsActivity.instance.termsAccepted();
		setResult(TERMS_ACCEPTED);
		finish();
	}

	@Override
	public void onBackPressed() {
		termsRejected();
	}
}
