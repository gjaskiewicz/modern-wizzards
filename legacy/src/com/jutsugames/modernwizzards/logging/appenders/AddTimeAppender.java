package com.jutsugames.modernwizzards.logging.appenders;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.jutsugames.modernwizzards.logging.LogAppender;
import com.jutsugames.modernwizzards.logging.LogLevel;

public class AddTimeAppender implements LogAppender {

	LogAppender base;
	public AddTimeAppender (LogAppender base) {
		this.base = base;
	}
	
	@Override
	public void appendLog(LogLevel lev, String tag, String message, Throwable t) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(new Date());
		base.appendLog(lev, tag, "["+date+"] "+message,t);
	}

	@Override
	public void flush() {
		base.flush();
	}
}
