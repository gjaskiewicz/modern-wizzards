package com.jutsugames.modernwizzards.logging.appenders;

import android.util.Log;

import com.jutsugames.modernwizzards.logging.LogAppender;
import com.jutsugames.modernwizzards.logging.LogLevel;

public class LogCatAppender implements LogAppender {

	@Override
	public void appendLog(LogLevel lev, String tag, String msg, Throwable t) {
		switch (lev) {
		case DEBUG:
			if (t != null) {
				Log.d(tag, msg, t);
			} else {
				Log.d(tag, msg);
			}
			break;
		case INFO:
			if (t != null) {
				Log.i(tag, msg, t);
			} else {
				Log.i(tag, msg);
			}
			break;
		case ERROR:
			if (t != null) {
				Log.e(tag, msg, t);
			} else {
				Log.e(tag, msg);
			}
			break;
		case VERBOSE:
			if (t != null) {
				Log.v(tag, msg, t);
			} else {
				Log.v(tag, msg);
			}
			break;
		case STATS:
			if (t != null) {
				Log.i(tag, msg, t);
			} else {
				Log.i(tag, msg);
			}
			break;
		case WARN:
			if (t != null) {
				Log.w(tag, msg, t);
			} else {
				Log.w(tag, msg);
			}
			break;
		case WTF:
			if (t != null) {
				Log.e(tag, msg, t);
			} else {
				Log.e(tag, msg);
			}
			break;
		default:
			Log.d("Log-Internal", ""+lev+" logging level not supported by logcat (yet)");
		}
	}
	
	@Override
	public void flush() { }
}
