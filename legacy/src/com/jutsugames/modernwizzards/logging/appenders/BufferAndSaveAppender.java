package com.jutsugames.modernwizzards.logging.appenders;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.jutsugames.modernwizzards.logging.LogAppender;
import com.jutsugames.modernwizzards.logging.LogLevel;

public class BufferAndSaveAppender implements LogAppender {
	
	int bufferThreshold;
	List<String> buffer;
	String filename;
	
	public BufferAndSaveAppender(String filename) {
		this(filename, 100);
	}
	
	public BufferAndSaveAppender(String filename, int bufferThreshold) {
		this.filename = filename;
		this.bufferThreshold = bufferThreshold;
		buffer = new ArrayList<String>(bufferThreshold);
	}
	
	@Override
	public void appendLog(LogLevel lev, String tag, String message, Throwable t) {
		String msg = 
				lev + ":" +
				tag + ":" + 
				message + 
				(t != null ? " Stacktrace: "+t.toString() : "") +
				"\n";
		
		if (buffer.size() >= bufferThreshold) {
			this.flush();
		}
		
		buffer.add(msg);
	} 
	
	@Override
	public void flush() {
		File tFile = new File(filename);
		try {
			if (!tFile.exists()) {
				tFile.createNewFile();
			}
			FileOutputStream fos = new FileOutputStream(tFile);
			for (String cmsg : buffer) {
				fos.write(cmsg.getBytes());
			}
			fos.close();
		} catch (IOException e) {
			Log.e("Log-internal", "IO-Error in file appender", e);
		}
	}
}
