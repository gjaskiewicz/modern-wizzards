package com.jutsugames.modernwizzards.logging.appenders;

import com.jutsugames.modernwizzards.logging.LogAppender;
import com.jutsugames.modernwizzards.logging.LogLevel;

public abstract class LogFilter implements LogAppender {

	LogAppender base;
	
	public LogFilter(LogAppender base) {
		this.base = base;
	}
	
	@Override
	public void appendLog(LogLevel lev, String tag, String message, Throwable t) {
		if (conditionOk(lev,tag,message,t)) {
			base.appendLog(lev, tag, message, t);
		}
	}
	
	@Override
	public void flush() {
		base.flush();
	}

	abstract boolean conditionOk(LogLevel lev, String tag, String message, Throwable t);
}
