package com.jutsugames.modernwizzards.logging.appenders;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.jutsugames.modernwizzards.logging.LogAppender;
import com.jutsugames.modernwizzards.logging.LogLevel;

public class ByLevelFilter extends LogFilter {

	Set<LogLevel> levels;
	
	public ByLevelFilter(LogAppender base, LogLevel ... acceptedLevels) {
		super(base);
		levels = new HashSet<LogLevel>();
		levels.addAll(Arrays.asList(acceptedLevels));
	}

	@Override
	boolean conditionOk(LogLevel lev, String tag, String message, Throwable t) {
		return levels.contains(lev);
	}	
}
