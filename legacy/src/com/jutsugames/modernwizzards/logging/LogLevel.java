package com.jutsugames.modernwizzards.logging;

public enum LogLevel {
	WTF, //What a terrible failure
	ERROR,
	WARN,
	INFO,
	DEBUG,
	VERBOSE,
	STATS
}
