package com.jutsugames.modernwizzards.logging;

import java.util.LinkedList;
import java.util.List;

import android.os.Environment;
import android.util.Log;

import com.jutsugames.modernwizzards.logging.appenders.AddTimeAppender;
import com.jutsugames.modernwizzards.logging.appenders.BufferAndSaveAppender;
import com.jutsugames.modernwizzards.logging.appenders.ByLevelFilter;
import com.jutsugames.modernwizzards.logging.appenders.LogCatAppender;

//W klasach loggera Jutsu unikac stosowania loggera Jutsu ze wzgledu na ryzyko zapetlenia 
public class JutsuLogger {
	
	public static JutsuLogger $log = new JutsuLogger();

	//config
	static {
		$log.addAppender(new LogCatAppender());
		/*$log.addAppender(
			new ByLevelFilter(new BufferAndSaveAppender(Environment.getDataDirectory() + "/syslog.log"),
			LogLevel.WTF, LogLevel.ERROR, LogLevel.WARN, LogLevel.INFO, LogLevel.DEBUG, LogLevel.VERBOSE)
		);
		$log.addAppender(
			new ByLevelFilter(
				new AddTimeAppender(
					new BufferAndSaveAppender(Environment.getDataDirectory() + "/stats.log")
				), LogLevel.STATS
			)
		);*/
	}
	
	private List<LogAppender> appenders = new LinkedList<LogAppender>();
	
	public void addAppender(LogAppender appender) {
		appenders.add(appender);
	}
	
	public void deleteAppender(LogAppender appender) {
		if(!appenders.remove(appender)) {
			$log.w("Log-Internal", "Appender not on the list - cannot remove");
		}
	}
	
	public void flush() {
		for (LogAppender appender : appenders) {
			appender.flush();
		}
	}
	
	public void log(LogLevel lev, String tag, String message, Throwable t) {
		
		for (LogAppender appender : appenders) {
			appender.appendLog(lev, tag, message, t);
		}
	}
	
	public void logf(LogLevel lev, String tag, String format, Object ... args) {
		String message = String.format(format, args);
		log(lev, tag, message, null);
	}
	
	//nie robic tego konstruktora publicznego (u�ywa� $log)
	JutsuLogger() { }
	
	//GJ: metody dla wygody
	public void d(String tag, String message, Throwable t) {
		log(LogLevel.DEBUG, tag, message, t);
	}
	
	public void w(String tag, String message, Throwable t) {
		log(LogLevel.WARN, tag, message, t);
	}
	
	public void e(String tag, String message, Throwable t) {
		log(LogLevel.ERROR, tag, message, t);
	}
	
	public void i(String tag, String message, Throwable t) {
		log(LogLevel.INFO, tag, message, t);
	}
	
	public void wtf(String tag, String message, Throwable t) {
		log(LogLevel.WTF, tag, message, t);
	}
	
	public void d(String tag, String message) {
		d(tag, message, null);
	}
	
	public void w(String tag, String message) {
		w(tag, message, null);
	}
	
	public void e(String tag, String message) {
		e(tag, message, null);
	}
	
	public void i(String tag, String message) {
		i(tag, message, null);
	}
	
	public void wtf(String tag, String message) {
		wtf(tag, message, null);
	}
	
	public void dff(String tag, String format, Object ... args) {
		logf(LogLevel.DEBUG, tag, format, args);
	}
	
	public void wff(String tag, String format, Object ... args) {
		logf(LogLevel.WARN, tag, format, args);
	}
	
	public void eff(String tag, String format, Object ... args) {
		logf(LogLevel.ERROR, tag, format, args);
	}
	
	public void iff(String tag, String format, Object ... args) {
		logf(LogLevel.INFO, tag, format, args);
	}
	
	public void wtff(String tag, String format, Object ... args) {
		logf(LogLevel.WTF, tag, format, args);
	}
}
