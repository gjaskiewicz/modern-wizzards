package com.jutsugames.modernwizzards.logging;

public interface LogAppender {
	void appendLog(LogLevel lev, String tag, String message, Throwable t);
	void flush();
}
