package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.LinkedList;

import com.jutsugames.modernwizzards.Game.GameState;
import com.jutsugames.modernwizzards.controller.AlertBulider;
import com.jutsugames.modernwizzards.controls.AnimateAndRunButton;
import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Fight;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.SpellFactory;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.views.CreatureBehaviorView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FightActivity extends MWBaseActivity {

	private static final String TAG = "FightActivity";
	
	private Fight fight; 
	private ImageView creatureVis;
	
	private HorizontalScrollView spellsScroll;
	private LinearLayout spellsLayout;
	
	//toinvalidate
	private View viewBarMyHP;
	private View viewBarMyMana;
	private View viewBarCrHP;
	private TextView hpTx;
	private CreatureBehaviorView creatureBehaviour;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        $log.d("INFO", "FightActivity onCreate");
        setContentView(R.layout.fight_screen);
        fight = Game.getGame().fight;
        
        if(!Player.getInst().firstFightDone)	{
        	fight.pauseFight=true;
        	AlertBulider ab = new AlertBulider() {
    			@Override
    			public void onYesClicked() {
    				fight.instructionConfirmed();
    			}
    			@Override
    			public void onNoClicked() {	}
    		};
    		ab.message= this.getString(R.string.fight_instructions);
    		ab.yesButtonTx = R.string.game_global_ok;
    		ab.infoOnly = true;
    		AlertActivity.showNewAlert(this, ab);
        }
        
        
		creatureVis = (ImageView) findViewById(R.id.CreatureImage);
		//Drawable visPic = getResources().getDrawable(fight.creature.getInfo(this).getImageID());
		//creatureVis.setImageDrawable(visPic);
		creatureVis.setImageResource(fight.creature.getInfo(this).getImageID());
		
		creatureBehaviour = (CreatureBehaviorView) findViewById(R.id.behaviour);
		viewBarMyHP = findViewById(R.id.barhpimg);
		viewBarMyMana = findViewById(R.id.barmanaimg);
		viewBarCrHP =  findViewById(R.id.CreatureHealth);
		hpTx = (TextView) findViewById(R.id.creatureHpTx);
		fight.controller = this;
		
		final Menubutton runButton = (Menubutton) findViewById(R.id.buttonRun);
		runButton.setOnClickListener(  new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showRunFromFightDialog();
				runButton.animatee();
			}
		});
		
		assignSpellsToButtonsNew();
		
		final Menubutton buttonInventory = (Menubutton) findViewById(R.id.buttonInventory);
		buttonInventory.setOnClickListener( new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showInventory();
				buttonInventory.animatee();
			}
		});
		
		updateBars();
		
		spellsLayout = (LinearLayout) findViewById(R.id.spellLayout);
		spellsScroll = (HorizontalScrollView) findViewById(R.id.spellScroll);
		spellsScroll.postDelayed(new Runnable() {
			@Override
			public void run() {
				//TODO: wyskrolowa� to g�wno dobrze. http://stackoverflow.com/questions/7002282/android-calculate-scrollto-position-in-horizontalscrollview
				spellsScroll.scrollTo(spellsLayout.getWidth()/2, 0);
			}
		}, 200);
	}
	
	private void assignSpellToButton(final String spell, final Menubutton view)	{
		view.setImage(SpellFactory.getSpellByID(spell, Game.getGame().playerEgo).getInfo().getImageID());
		view.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String spl = spell;
				castSpell(spl);
				view.animatee();
			}
		});
	}
	

	private void assignSpellsToButtonsNew() {
		Menubutton example = (Menubutton) findViewById(R.id.spell1);
		LayoutParams layoutParams = example.getLayoutParams();
		
		LinearLayout sl = (LinearLayout) findViewById(R.id.spellLayout);
		sl.removeAllViews();
		
		for(String spl: Game.getGame().playerEgo.getKnownBattleSpells())	{
			Menubutton button = new Menubutton(getApplicationContext(), null);
			button.setLayoutParams(layoutParams);
			assignSpellToButton(spl, button);
			sl.addView(button);
			if(!Player.getInst().firstFightDone)	{
				button.setNotification(true);
			}
		}
	}
	
	private void showInventory()	{
		//InventoryActivity.fightMode = true;
		startActivity(new Intent(this, InventoryActivity.class).putExtra(InventoryActivity.PARAM_FIGHT_MODE, true));
	}
	
	
	private void castSpell(String spell) {
		$log.d("VIEW", "Fighting: casting seleted: "+ spell);
		fight.playerCastsSpell(
				SpellFactory.getSpellByID(spell, Game.getGame().playerEgo));
	}
	
	public void creatureCastsSpell(Spell spell) {
		ObjectInfo info = spell.getInfo();
		String text = this.getString(R.string.creature_cast_fc);
		MWToast.makeToast( this, text +" " + info.getName() + "!", Toast.LENGTH_SHORT).show();
	}
	
	public void updateBars()	{
		//player
		int max = Game.getGame().playerEgo.maxHealthPoints;
		
		max = fight.creature.maxHealthPoints;
		
		int hp = fight.creature.getHP();
		hpTx.setText(hp+"/"+max);
	}
	
	public void showRunFromFightDialog()	{
    	AlertBulider ab = new AlertBulider() {
			@Override
			public void onYesClicked() {
				fight.playerRunsFromTheFight();
			}
			@Override
			public void onNoClicked() {	}
		};
		ab.message= this.getString(R.string.escape_text_fc);
		ab.yesButtonTx = R.string.run_button;
		ab.noButtonTx = R.string.stay_button;
		AlertActivity.showNewAlert(this, ab);
	
	}

	public void showWinAlert() {
    	AlertBulider ab = new AlertBulider() {
			@Override
			public void onYesClicked() {
				fight.winAlertConfirmed();
			}
			@Override
			public void onNoClicked() {	}
		};
		ab.message= this.getString(R.string.fight_winalert_desc);
		ab.yesButtonTx = R.string.fight_winalert_ok;
		ab.infoOnly = true;
		AlertActivity.showNewAlert(this, ab);

	}

	public void showLooseAlert() {
    	AlertBulider ab = new AlertBulider() {
			@Override
			public void onYesClicked() {
				fight.fightEnds();
			}
			@Override
			public void onNoClicked() {	}
		};
		ab.message= this.getString(R.string.fight_lostalert_desc);
		ab.yesButtonTx = R.string.fight_lostalert_ok;
		ab.infoOnly = true;
		AlertActivity.showNewAlert(this, ab);
		
	}

	public void animateCreatureHit()	{
		Animation anim = AnimationUtils.loadAnimation(this, R.anim.creaturehit);
		creatureVis.startAnimation(anim);
	}
	
	public void refresh()	{
		updateBars();
		
		viewBarCrHP.invalidate();
		viewBarMyHP.invalidate();
		viewBarMyMana.invalidate();
		creatureBehaviour.invalidate();
		
	}

	public void addSpellToDisplay(Spell spell) {
		creatureBehaviour.addSpellToDisplay(spell);
	}
	
    @Override
    public void onBackPressed() {
    	$log.d(TAG, "AlertActivity BACK clicked");
    	showRunFromFightDialog();
    }
}
