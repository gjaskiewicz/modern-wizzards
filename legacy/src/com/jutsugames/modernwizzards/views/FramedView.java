package com.jutsugames.modernwizzards.views;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class FramedView {
	
	public static View	getFramedView(InventoryItem ii)	{
		
		LayoutInflater inflater = Game.mainActivity.getLayoutInflater();
		View layout = inflater.inflate(R.layout.framed_item, null);
		//image = layout.
		layout.findViewById(R.id.inventory_item_count_tag).setVisibility(View.GONE);
		
		/*if(ii instanceof SpellToken) {
			layout.findViewById(R.id.inventory_item_bcg).setBackgroundColor(((SpellToken)ii).getColor());
			if(ii.getInfo().getImageID()==R.drawable.unknown) {
				ImageView iv = (ImageView) layout.findViewById(R.id.inventory_item_image);
				iv.setImageDrawable(Game.mainActivity.getResources().getDrawable(R.drawable.potion_uni));
			}
		}*/
		
		return layout;
		
	}
}
