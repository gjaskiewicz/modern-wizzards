package com.jutsugames.modernwizzards.views;

import java.util.LinkedList;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.R.drawable;
import com.jutsugames.modernwizzards.gamelogic.Spell;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class CreatureBehaviorView extends View {

	private Bitmap bcg;
	private Bitmap target;
	private Paint painter;
	private LinkedList<Spell> spells;
	
	public CreatureBehaviorView(Context context, AttributeSet attrs) {
		super(context, attrs);
		painter = new Paint();
		//painter.se
		spells = new LinkedList<Spell>();
	}

	public void loadTiles() {		
		BitmapFactory.Options myOptions = new BitmapFactory.Options();
		myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// important
		bcg = BitmapFactory.decodeResource(getResources(), R.drawable.mw_fight_behaviour_bcg2, myOptions);
		bcg = Bitmap.createScaledBitmap(bcg, getWidth(), getHeight(), true);
		target = BitmapFactory.decodeResource(getResources(), R.drawable.mw_fight_behaviour_target, myOptions);
		target = Bitmap.createScaledBitmap(target, getHeight()*2/3, getHeight()*2/3, true);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		loadTiles();
	}

	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawBitmap(bcg, 0, 0, null);
		
		for(Spell s : spells){
			s.paintEffect(canvas, this);
		}
		
		float pos;
		if (this.isInEditMode())
			pos = 0;
		else	{
			pos = (float)Game.getGame().fight.creature.getBehaviourPosition();
			painter.setAlpha(Game.getGame().fight.creature.fightBehaviour.getBehaviourOpacity());
		}
		
		float position = (getWidth()-getHeight()*2/3-14)*(pos+100)/200;
		canvas.drawBitmap(target, position+7, getHeight()/6, painter);
	}
	
	public int behavPosToX(int pos)	{
		return 7+(getWidth()-14)*(pos+100)/200;
	}
	
	public int spellWidthToX(int w)	{
		return w*(getWidth()-14)/200;
	}

	public void addSpellToDisplay(Spell spell) {
		spells.add(spell);
	}

}
