package com.jutsugames.modernwizzards.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.R.drawable;
import com.jutsugames.modernwizzards.gamelogic.Player;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class HorizontalBarExp extends HorizontalBar {

	public HorizontalBarExp(Context context, AttributeSet attrs) {
		super(context, attrs);
		barResourceID = R.drawable.mw_fight_bar_exp;
		$log.d("VIEW", "widok HorizontalBarExp tworzy sie1");
	}

	@Override
	protected double getFillPercentage() {
		if(isInEditMode()) return 0.7;
		int baseLevel = Player.getInst().getExpForLevel(Player.getInst().expLevel);
		double pos = Game.getGame().playerEgo.experience - baseLevel;
		double posMax = Player.getInst().getExpForLevel(Player.getInst().expLevel+1) - baseLevel;
		if (pos <= 0)
			pos = 1;
		return (pos / posMax);
	}
	
	@Override
	protected String getText() {
		if(isInEditMode()) return "Exp: 70/100";
		return new String("Level: " + Game.getGame().playerEgo.expLevel +
				" Exp: "+ Game.getGame().playerEgo.experience +"/" + 
				(int)(Player.getInst().getExpForLevel(Player.getInst().expLevel+1)));
	}
}
