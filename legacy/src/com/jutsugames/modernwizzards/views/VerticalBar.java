package com.jutsugames.modernwizzards.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.R.drawable;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class VerticalBar extends View {

	protected Bitmap bcg, bar;
	private Rect destRect;
	
	public VerticalBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		$log.d("VIEW", "widok VerticalBar tworzy sie1");
	}

	protected void makeBitmaps() {
		destRect = new Rect(-getHeight()+7, 7, -7, getWidth()-7);
		BitmapFactory.Options myOptions = new BitmapFactory.Options();
		myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// important
		bcg = BitmapFactory.decodeResource(getResources(), R.drawable.mw_fight_bar_bcg, myOptions);
		bcg = Bitmap.createScaledBitmap(bcg, getHeight(), getWidth(), true);
		bar = BitmapFactory.decodeResource(getResources(), R.drawable.mw_fight_bar_hp, myOptions);
		bar = Bitmap.createScaledBitmap(bar,  getHeight()-14, getWidth()-14, true);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		makeBitmaps();
	}

	protected double getFillPercentage() {
		if(isInEditMode()) return 1;
		double pos = Game.getGame().fight.creature.getHealthPoints();
		double posMax = Game.getGame().fight.creature.maxHealthPoints;
		if (pos <= 0)
			pos = 1;
		return (pos / posMax);
	}

	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		destRect.right = (int)(-getHeight()+7+ getFillPercentage()*(getHeight()-14));
		canvas.rotate(-90);
		canvas.drawBitmap(bcg, -getHeight(), 0, null);
		canvas.drawBitmap(bar, null, destRect, null);
		
		// $log.d("VIEW", "creature health onDraw pos = " +toPosition +
		// " size = "+ size);
	}
}
