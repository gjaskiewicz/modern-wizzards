package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.net.URI;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.provider.Settings.Secure;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game.GameState;
import com.jutsugames.modernwizzards.controller.AlertBulider;
import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.controls.Optionsbutton;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.MainLoop;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;
import com.jutsugames.modernwizzards.quests.display.QuestsMessageListActivity;
import com.jutsugames.modernwizzards.quests.logic.AlchemyQuest;
import com.jutsugames.modernwizzards.quests.logic.FleeingRabbitQuest;
import com.jutsugames.modernwizzards.quests.logic.InitialQuest;
import com.jutsugames.modernwizzards.quests.logic.Message;
import com.jutsugames.modernwizzards.quests.logic.Quest;
import com.jutsugames.modernwizzards.quests.logic.QuestsEngine;
import com.jutsugames.modernwizzards.util.*;

public class ModernWizzardsActivity extends MWBaseActivity {
	
	private static final String TAG = "ModernWizzardsActivity";
	public static ModernWizzardsActivity instance;
	
	private LayoutInflater inflater;
	public View mainMenuView;

	public static float tenDPinPixels = 20;
	
	private ImageButton logoButton;
	private Animation logoAnimation;
	private boolean isEngineRunning = false;

	private Map<Integer,Integer> notificationsOnButtons; 
	
	private BroadcastReceiver eventsReceiver; //Will receive notification updates of menu buttons
	private IntentFilter eventFilter, eventFilterMessageOpened, eventFilterQuestsCollectionChanged;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	instance = this;
        super.onCreate(savedInstanceState);
        Options.loadOptions(this);
        tenDPinPixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
        
        //TODO: wersja angielska
        setLocale();
    	ObjectInfoFactory.get().initializeContext(this);
        
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        inflater = (LayoutInflater) ModernWizzardsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	mainMenuView = inflater.inflate(R.layout.mainmenu, null, false);
    	setContentView(mainMenuView); 
        
        if(!Options.TERMS_OF_USE_ACCEPTED)	{
        	startActivityForResult(new Intent(this, DisclaimerActivity.class), DisclaimerActivity.TERMS_ACCEPTED);
        }
        else	{
        	termsAccepted();
        }
        
		String plain = "Zaphod's just zis guy, ya knöw?";
	    String encrypted = EncryptDecryptHelper.encrypt(plain, "000102030405060708090A0B0C0D0E0F");
	    System.out.println(encrypted);
	    String decrypted = EncryptDecryptHelper.decrypt(encrypted, "000102030405060708090A0B0C0D0E0F");
	    if (decrypted != null && decrypted.equals(plain)) {
	        $log.d(TAG, "Hey! " + decrypted);
	    } else {
	    	$log.d(TAG, "Bummer!");
	    }
    }
    
    public void termsAccepted()	{
        
        if(Options.SAVE_AND_LOAD) {
	        SavesStorage savesStorage = new SavesStorage(this);
	        for(Long id : savesStorage.getOrderdIdsOfSaves()) {
	        	try {
	        		GameStateToSave gs = savesStorage.getGameStateWithID(id);
	        		if(gs != null ) {
	        			$log.d(TAG, "Restored GameState: player name(gs3): " + gs.game.playerEgo.nameID + " for id:" + id);
	        			gs.restoreGameState(this);
	        			break;
	        		}
	        	} catch (Exception e) {
					$log.e(TAG, "Problem with restoring save id " + id + ". ");
					e.printStackTrace();
				}
	        }
			savesStorage.close();
	    }
        Options.LOAD_SAVE_FROM_FILE = false;
        Options.saveOptions(this);
        
		Game game = Game.getGame(); 
        game.setRunningContext(this);
        Game.setGlobalTopmostActivity(this);
        
        EventEmiter.addChangeListener(QuestsEngine.getInstance());
        EventEmiter.setActivity(this);
        
        setUpButtons();
        
        logoButton = ((ImageButton)findViewById(R.id.mwLogotype));
        /**
         * TODO: przerzucić te serwisy do OPCJI, jeśli w ogole to potrzebne jest.
         * Initialize logo button.
         * It is used to start/stop game engine
         */
        /*logoButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isEngineRunning) {
					stopService(new Intent(getBaseContext(), EngineService.class));
					stopService(new Intent(getBaseContext(), QuestsService.class));
					stopAnimationOnLogo();
				}
				else {
					startService(new Intent(getBaseContext(), EngineService.class));
					startService(new Intent(getBaseContext(), QuestsService.class));
					startAnimationOnLogo();
				}
			}
		});*/
        logoButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(Game.getGame().locationIsNotSet) {
					MWToast.showToast(R.string.game_gps_loc_uncalculated, Toast.LENGTH_SHORT);
				}
				else {
					MWToast.showToast(R.string.game_gps_loc_calculated, Toast.LENGTH_SHORT);
				}
			}
		});
        logoAnimation = AnimationUtils.loadAnimation(this, R.anim.logoicon);
        //startAnimationOnLogo();
        
        ImageButton help = ((ImageButton)findViewById(R.id.mwhelp));
        //help.setVisibility(View.GONE);
        help.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.help_uri))));
			}
		});
  
        //Set receiver for notification updates of menu buttons
        eventFilter = new IntentFilter(Game.Actions.MENU_BUTTON_UPDATE);
        eventFilterMessageOpened = new IntentFilter(Game.Actions.MESSAGE_OPENED);
        eventFilterQuestsCollectionChanged = new IntentFilter(Game.Actions.QUESTS_COLLECTION_CHANGED);
        
		eventsReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if(intent.getAction().equals(Game.Actions.MENU_BUTTON_UPDATE)) {
					Integer buttonID = intent.getIntExtra("buttinID",0);
					boolean onoff = intent.getBooleanExtra("onoff", false);
					setNotificationFor(buttonID, onoff);
				} else if (intent.getAction().equals(Game.Actions.MESSAGE_OPENED)) {
					setNotificationFor(R.id.btnQuests, false);
					refreshCounterOfUnopenedMessages();
				} else if (intent.getAction().equals(Game.Actions.QUESTS_COLLECTION_CHANGED)) {
					refreshCounterOfUnopenedMessages();
				}
			}
        };
        registerReceiver(eventsReceiver, eventFilter);
        registerReceiver(eventsReceiver, eventFilterMessageOpened);
        registerReceiver(eventsReceiver, eventFilterQuestsCollectionChanged);
        
        //TODO: remove this code for final version
        if(!Options.USE_GPS) {
	        game.playerEgo.setLocation(new GeoPoint((int)(Options.MOCK_LAT*1E6) ,(int)(Options.MOCK_LNG*1E6)));
			game.locationChanged(game.playerEgo.getLocation());
	    }
        else {
        	startGPS();
        }
        
        Message.initializeContext(this); //If there is any problem if context could sometimes change? 
        if(!Game.getGame().isInitialized) {
        	if(Options.GENERATE_TEST_DATA) {
        		game.initTestData();
        		GameContent.getInstance().setAllAllowed();
        	}
        	Quest firstQuests = new InitialQuest();
        	//Quest firstQuests = new FleeingRabbitQuest();
        	firstQuests.initateQuest();
        	QuestsEngine.getInstance().addQuest(firstQuests);
        	Game.getGame().isInitialized = true;
        }
        
        refreshCounterOfUnopenedMessages();
		
        new RequestTask().execute("http://jutsu.pl/wizards/runlog.php?telid="+Secure.getString(getBaseContext().getContentResolver(),
                Secure.ANDROID_ID));
        
		MainLoop.startGameLoop();
		
		logMem();
    }
    

	private void setUpButtons() {
		((LinearLayout)findViewById(R.id.mainmenu_span_buttons)).setLayoutParams(
				((FrameLayout)findViewById(R.id.mainmenu_screen)).getLayoutParams()); 	
		
		try {
			((TextView)findViewById(R.id.mwVersion)).setText(
					getPackageManager().getPackageInfo(getPackageName(), 0).versionName
				);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		
		((Menubutton)findViewById(R.id.btnMap)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gotoToMap(null);
			}
		});
		((Menubutton)findViewById(R.id.btnSpells)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gotoSpells(null);
			}
		});
		((Menubutton)findViewById(R.id.btnQuests)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gotoQuests(null);
			}
		});
		((Menubutton)findViewById(R.id.btnInventory)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gotoInventory(null);
			}
		});
		((Menubutton)findViewById(R.id.btnStatistics)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gotoStatistics(null);
			}
		});
		((Menubutton)findViewById(R.id.btnIntro)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gotoWatchIntro(null);
			}
		});
		((ImageButton)findViewById(R.id.mwClose)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				quitGame(null);
			}
		});
		((Optionsbutton)findViewById(R.id.btnOptions)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gotoOptions(null);
			}
		});
		
	}

	@Override
	public void onBackPressed() {
		//super.onBackPressed();
		quitGame(null);
	}
	
	/**
	 * Uruchamia animację logotypu. Migający logotyp oznacza że lokalizacja jest w trakcie określania!
	 */
	public void startAnimationOnLogo()	{
		if(logoAnimation == null) {
			$log.d(TAG, "logo animation not started");
			return;
		}
		logoAnimation.reset();
		logoButton.startAnimation(logoAnimation);
		//isEngineRunning = true;
	}
	
	public void stopAnimationOnLogo() {
		logoButton.clearAnimation();
		//isEngineRunning = false;
	}
    
	/**
	 * jeśli chcesz się przekonać jaki rozkład ma funkcja Random, uzyj tego.
	 */
    private void testRandom() {
    	int[] res = new int[100];
    	Random rnd = new Random(123);
    	for(int i = 1 ; i < 1000000; i++)	{
			res[rnd.nextInt(100)]++;
		}
    	for(int i = 0 ; i < 100; i++)	{
			Log.d("test", "for i:" + i + " : " + res[i]);
		}
	}

	public void setNotificationFor(Integer id, boolean notificationOn) {
    	if(notificationsOnButtons == null)
    		notificationsOnButtons = new HashMap<Integer, Integer>();
    	
    	Integer counter = notificationsOnButtons.get(id);
    	if( counter == null  )
    		if(notificationOn)
    			counter = new Integer(1);
    		else
    			counter = new Integer(0);
    	else
    		if(notificationOn)
    			counter = 1;
    			//counter++;
    		else
    			counter = 0;
    			//counter = (counter <= 1 ? 0 : --counter);
    	notificationsOnButtons.put(id,counter);
    	repaintButtons();
    }
    
    private void repaintButtons() {
    	for (Map.Entry<Integer, Integer> entry : notificationsOnButtons.entrySet()) {
    	    int key = entry.getKey();
    	    int counter = entry.getValue();
    	    
    	    //$log.d(TAG,"Key: " + key + " Counter: " + counter);
    	    
    	    View button = mainMenuView.findViewById(key);
    	    if(button instanceof Menubutton)
    	    	((Menubutton)button).setNotification( (counter >= 1 ? true : false) );
    	}
    	mainMenuView.invalidate();
    }
    
    private void refreshCounterOfUnopenedMessages() {
    	//pobrać widok przycisku 
    	Menubutton questsButton = ((Menubutton)findViewById(R.id.btnQuests));
    	//pobrać ilość nie otworzonych wiadomości
    	int unopenedMessages = QuestsEngine.getInstance().getCountOfUnopenedQuestMessages();
    	//pobrać tekst dla przycisku Quests
    	String questsMenuButtonText = getString(R.string.mainmenu_quests);
    	//połączyć go z ilością nie odczytanych wiadomości
    	if(unopenedMessages>0)	{
    		questsMenuButtonText = questsMenuButtonText + "[" + unopenedMessages + "]";
    	}	
    	//ustawić nowy napis
    	questsButton.setText(questsMenuButtonText);
    }
    
    public void gotoToMap(View view) {
    	$log.d(TAG, "onClick MAP" );
    	if(Game.getGame().getGameState()==GameState.STATE_INIT || Player.getInst().getGeoPoint()==null)	{
    		MWToast.showToast( R.string.game_gps_loc_wait, Toast.LENGTH_SHORT);
    		return;
    	}
    	else {
    		startActivity(new Intent(this, MWMap2Activity.class));
    	}
    }

	public void gotoInventory(View view) {
		//InventoryActivity.fightMode = false;
		startActivity(new Intent(this, InventoryActivity.class));
    }
	
	public void gotoOptions(View view) {	
		startActivity(new Intent(this, OptionsActivity.class));
    }
    
    public void gotoQuests(View view) {
    	Intent myIntent = new Intent(this, QuestsMessageListActivity.class);
    	startActivity(myIntent);
    	overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
     
    public void gotoSpells(View view) {
    	startActivity(new Intent(this, SpellsActivity.class));
    }
    
    public void gotoStatistics(View view) {
		//setContentView(statisticsView);
    	startActivity(new Intent(this, StatsActivity.class));
    	overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
    
    public void gotoWatchIntro(View view) {
    	String videoid = getResources().getString(R.string.intro_youtube_id);
    	startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://m.youtube.com/watch?v=" + videoid)));
    	overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
    
    public void quitGame(View view) {
    	$log.d(TAG, "game quits"); 
    	AlertBulider ab = new AlertBulider() {
			@Override
			public void onYesClicked() {
				//Game.getGame().setGameState(GameState.STATE_ERROR);
				stopGPS();
				if(!Options.dontSaveThisTime) //zapewnia ze gra sie nie zapisze po kliknieciu "delete all saves"
					Game.saveGame();
		    	new Quest("", "").showNarrationMessage(R.string.thanks_message_content);
				
				finish();
		    	android.os.Process.killProcess(android.os.Process.myPid());
			}
			@Override
			public void onNoClicked() {
				
			}
		};
		ab.message= getResources().getString(R.string.game_reallyquit);
		AlertActivity.showNewAlert(this, ab);
    }    
    
    @Override
    public void onPause()
    {
    	$log.d(TAG, "Pause MainActivity");
    	//$log.flush();
        super.onPause();
    }
     
    @Override
    public void onResume()
    {
        super.onResume();
    }
    
	@Override
	protected void onStart() {
		super.onStart();
		//wywalam gps na start, bo wylacza go przy przejsiu do innej activity. 
	}
	
	public void startGPS()	{
		LocationManager mLocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
		if(Options.USE_GPS  ) {
			if(mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))	{
				mLocationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0,  Game.getGame().mLocationListener);
				mLocationManager.requestLocationUpdates( LocationManager.NETWORK_PROVIDER, 1000, 10,  Game.getGame().mLocationListener);
				
				startAnimationOnLogo();
			}
			else {
				//TODO: wrzuci na twarz komunikat, że masz włączy GPS w telefonie!
				$log.e(TAG, "GPS PROVIDER NOT ENABLED!");
				MWToast.showToast(R.string.game_gps_not_enabled, Toast.LENGTH_LONG);
			}
		}
	}
	
	public void stopGPS()	{
		LocationManager mLocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
		mLocationManager.removeUpdates(Game.getGame().mLocationListener);
	}
    
    @Override
    public void onDestroy() {
    	unregisterReceiver(eventsReceiver);
    	$log.flush();
        super.onDestroy();
    }   
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if(resultCode == DisclaimerActivity.TERMS_ACCEPTED)	{
    		termsAccepted();
    	}
    }
    
    /**odczytuje z dysku preferowany jezyk aplikacji*/
    private void loadLanguage()
    {
        $log.d("INFO", "MDA loadLanguage fun");      	
    }  
    
    //ustawia jezyk
    private void setLocale(){
    	
    	String loc= Locale.getDefault().getLanguage();
    	if(Options.LANGUAGE_ID==0)
    	{
    		loc="en";
    	}
    	else if (Options.LANGUAGE_ID==1)
    	{
    		loc="pl";
    	}
    	
    	
    	if(loc=="en" || loc=="pl"){	
    	 Locale locale = new Locale(loc);
         Locale.setDefault(locale);
         Configuration config = new Configuration();
         config.locale = locale;
         getBaseContext().getResources()
                 .updateConfiguration(
                         config,
                         getBaseContext().getResources()
                                 .getDisplayMetrics());     
         $log.d("INFO", "OA setLocale: " + loc);    	    	         
         }
    	
    }
    
    @Override
    public void finish() {
    	$log.d(TAG, "MW Finish?"); 
    	super.finish();
    }
    
    public static void logMem()	{
    	Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
    	Debug.getMemoryInfo(memoryInfo);

    	String memMessage = String.format(
        "Memory: Pss=%.2f MB, Private=%.2f MB, Shared=%.2f MB",
        memoryInfo.getTotalPss() / 1024.0,
        memoryInfo.getTotalPrivateDirty() / 1024.0,
        memoryInfo.getTotalSharedDirty() / 1024.0);
    	$log.d("MW MEMORY", "MW " + memMessage);
    }
}