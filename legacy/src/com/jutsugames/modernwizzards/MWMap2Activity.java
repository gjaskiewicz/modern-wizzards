package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

//google maps v2
import android.support.v4.app.FragmentActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.android.gms.maps.model.UrlTileProvider;

import com.jutsugames.modernwizzards.controller.MapController;
import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.objects.map.TriggerZone;
import com.jutsugames.modernwizzards.quests.display.MessageViewActivity;
import com.jutsugames.modernwizzards.util.MoveLocation;

public class MWMap2Activity extends FragmentActivity implements
		SensorEventListener, OnCameraChangeListener {
	private static final String TAG = "MWMapActivity2";

	// te 4 dotycz� kompasu
	public static Float azimut = 0.0f;
	Sensor accelerometer;
	Sensor magnetometer;
	private SensorManager mSensorManager;

	public static GoogleMap googleMap;

	private static OnMapClickListener currentTapListener;

	protected static final double BUTTONS_MOVE_DIST = 25;

	private ImageView buttonLock;
	private View filtersPanel;

	private Bitmap compassMarker;
	GroundOverlay compass;

	// ktory triger jest pokazywany
	int triggerNo = 1;

	private UrlTileProvider tileProvider;

	private TileOverlay tileOverlay;

	private Menubutton buttonTerrain;

	private View manaBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		$log.d(TAG, "onCreate!!");
		setContentView(R.layout.main_map2);
		Game.setGlobalTopmostActivity(this);

		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		accelerometer = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		magnetometer = mSensorManager
				.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

		setupScreen();
		
		// compass setup();
		BitmapFactory.Options myOptions = new BitmapFactory.Options();
		myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// important
		compassMarker = BitmapFactory.decodeResource(
				this.getResources(), R.drawable.map_cursor,
				myOptions);
		compassMarker = Bitmap.createScaledBitmap(compassMarker, 31, 51, true);
		

		tileProvider = new UrlTileProvider(256, 256) {
			@Override
			public synchronized URL getTileUrl(int x, int y, int zoom) {
				// The moon tile coordinate system is reversed. This is not
				// normal.
				String WATERCOLOR = "http://c.tile.stamen.com/watercolor/%d/%d/%d.jpg";
				String s = String.format(Locale.US, WATERCOLOR, zoom, x, y);
				URL url = null;
				try {
					url = new URL(s);
				} catch (MalformedURLException e) {
					//throw new AssertionError(e);
				}
				return url;
			}
		};

		googleMap=null;
		setUpMap();
	}
	
	private void setUpMap()	{
		if(googleMap==null)	{
			SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.mapv2);
			googleMap = fm.getMap();
		}
		if(googleMap==null)	{
			return; //to tragedia..
		}
		try	{
			googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
				convLocationToLatLng(Game.getGame().playerEgo.getLocation()), 16));
			googleMap.setOnCameraChangeListener(this);
			//TODO if no multitouch...
			googleMap.getUiSettings().setZoomControlsEnabled(false);
			
			MapController.getInst().addAll(this);
			
			googleMap.setOnMapClickListener(currentTapListener);
			
			setMapVisualization(Options.MAP_VISUALIZATION);
			
			if(getIntent().getExtras() != null && getIntent().getExtras().getBoolean("centrize"))	{
				centerAtNextTrigger(null);
			}
			
			showPlayerSight(convLocationToLatLng(Game.getGame().playerEgo.getLocation()));
			
			drawCompass();
		}
		catch(Exception e)	{
			$log.d(TAG, "CAMERA NOT INITIALIZED YET!");
		}
	}
	
	private void movePlayerLocation(int direction) {
		MoveLocation.moveLocation(Player.getInst().getLocation(),
				BUTTONS_MOVE_DIST, direction);
		Game.getGame().locationChanged(Player.getInst().getLocation());
		//Game.getGame().playerEgo.repaint();

		// playerMarker.setPosition(
		// locationToLatLng(Game.getGame().playerEgo.getLocation()));


		drawCompass();
	}

	private void setMapVisualization(int i)	{
		Options.saveOptions(this);
		removeTileOverlay();
		if(i==0)	{
			tileOverlay = googleMap.addTileOverlay(new TileOverlayOptions().tileProvider(
				tileProvider).zIndex(-100));
			googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
			buttonTerrain.setText(R.string.mapviewtype_feelings);
		}
		else if(i==1)	{
			tileOverlay = googleMap.addTileOverlay(new TileOverlayOptions().tileProvider(
				tileProvider).zIndex(-100));
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			buttonTerrain.setText(getResources().getString(R.string.mapviewtype_feelings)+"+");
		}
		else if(i==2)	{
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			buttonTerrain.setText(R.string.mapviewtype_normal);
		}
		else if(i==3)	{
			googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			buttonTerrain.setText(R.string.mapviewtype_satelite);
		}
		else if(i==4)	{
			googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			buttonTerrain.setText(getResources().getString(R.string.mapviewtype_satelite)+"+");
		}
		else if(i==5)	{
			googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
			buttonTerrain.setText(R.string.mapviewtype_terrain);
		}
		else if(i==6)	{
			googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
			buttonTerrain.setText(R.string.mapviewtype_none);
		}
	}
	
	private void removeTileOverlay()	{
		if(tileOverlay!=null)
			tileOverlay.remove();
	}
	
	private void setupScreen()	{
		manaBar = findViewById(R.id.barMana);
		
		Button bN = (Button) findViewById(R.id.movN2);
		Button bE = (Button) findViewById(R.id.movE2);
		Button bW = (Button) findViewById(R.id.movW2);
		Button bS = (Button) findViewById(R.id.movS2);

		if (bN == null)
			$log.d(TAG, "bN is null");
		bN.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				movePlayerLocation(0);
			}
		});
		bS.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				movePlayerLocation(180);
			}
		});
		bW.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				movePlayerLocation(270);
			}
		});
		bE.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				movePlayerLocation(90);
			}
		});

		ImageView buttonCentrize = (ImageView) findViewById(R.id.button_centrize2);
		buttonCentrize.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				centerAtNextTrigger(null);
			}
		});
		
		buttonLock = (ImageView) findViewById(R.id.button_lock2);
		setPosLocker();
		buttonLock.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Options.LOCK_VIEW_TO_LOCATION = !Options.LOCK_VIEW_TO_LOCATION;
				Options.saveOptions(Game.mainActivity);
				setPosLocker();
			}
		});

		filtersPanel = findViewById(R.id.filters2);
		filtersPanel.setVisibility(View.GONE);
		Menubutton buttonFilters = (Menubutton) findViewById(R.id.button_filters2);
		buttonFilters.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (filtersPanel.getVisibility() == View.VISIBLE) {
					filtersPanel.setVisibility(View.GONE);
				} else {
					filtersPanel.setVisibility(View.VISIBLE);
				}
			}
		});

		Menubutton buttonMenu = (Menubutton) findViewById(R.id.button_mainmenu2);
		buttonMenu.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		buttonTerrain = (Menubutton) findViewById(R.id.button_terrain2);
		buttonTerrain.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// nie dziala zeby przycisk sie zmienil
				Options.MAP_VISUALIZATION=(Options.MAP_VISUALIZATION+1)%7;
				setMapVisualization(Options.MAP_VISUALIZATION);
			}
		});

		final View ctrl = findViewById(R.id.controls2);
		Menubutton buttonControls = (Menubutton) findViewById(R.id.button_controls2);
		buttonControls.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				if (ctrl.getVisibility() == View.GONE) {
					ctrl.setVisibility(View.VISIBLE);
				} else {
					ctrl.setVisibility(View.GONE);
				}
			}
		});
		if(Options.USE_GPS)	{
			buttonControls.setVisibility(View.GONE);
			ctrl.setVisibility(View.GONE);
		}
		else ctrl.setVisibility(View.VISIBLE);
		
		View buttonClosePanel = findViewById(R.id.button_closepanel2);
		buttonClosePanel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				filtersPanel.setVisibility(View.GONE);
			}
		});
	}
	
	private void setPosLocker() {
		if (!Options.LOCK_VIEW_TO_LOCATION)
			buttonLock.setImageResource(R.drawable.mw_map_lock_off);
		else
			buttonLock.setImageResource(R.drawable.mw_map_lock_on);
	}

	public void onResume() {
		Game.setGlobalTopmostActivity(this);
		
		if(googleMap==null)	{
			setUpMap();
		}
		
		mSensorManager.registerListener(this, accelerometer,
				SensorManager.SENSOR_DELAY_UI);
		mSensorManager.registerListener(this, magnetometer,
				SensorManager.SENSOR_DELAY_UI);

		Game.turnOnOffNotificationForButton(R.id.btnMap, false);
		
		if(googleMap!=null)	{
			googleMap.setOnMapClickListener(currentTapListener);
		}
		
		super.onResume();
	}

	public void onPause() {
		mSensorManager.unregisterListener(this);
		super.onPause();
	}
	

	float[] mGravity;
	float[] mGeomagnetic;

	//private Marker playerMarker;
	private Circle playerSight;

	private Bitmap rotatedCompassBitmap;

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
			mGravity = event.values;
		if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
			mGeomagnetic = event.values;
		if (mGravity != null && mGeomagnetic != null) {
			float R[] = new float[9];
			float I[] = new float[9];
			boolean success = SensorManager.getRotationMatrix(R, I, mGravity,
					mGeomagnetic);
			if (success) {
				float orientation[] = new float[3];
				SensorManager.getOrientation(R, orientation);
				azimut = orientation[0]; // orientation contains: azimut, pitch
											// and roll
				// drawCompass();
			}
		}

	}

	// wyswietla ikone gracza w pozycji playerPos
	private void showPlayerSight(LatLng playerPos) {

		CircleOptions circleOptions = new CircleOptions()
				.center(playerPos)
				.radius(Player.getInst().getSightRange())
				.strokeColor(0x400000FF)
				.strokeWidth(5)
				.fillColor(0x00000000);
		playerSight = googleMap.addCircle(circleOptions);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	public void drawCompass() {
		if (compass != null)
			compass.remove();

		if(rotatedCompassBitmap!=null)	{
			rotatedCompassBitmap.recycle();
		}
		rotatedCompassBitmap = Bitmap.createBitmap(80, 80, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(rotatedCompassBitmap);

		canvas.rotate(MWMap2Activity.azimut * 360 / (2 * 3.14159f), 40, 40);
		canvas.drawBitmap(compassMarker, -15 + 40, -35 + 40, null);

		if(googleMap!=null)	{
			compass = googleMap.addGroundOverlay(new GroundOverlayOptions().image(
					BitmapDescriptorFactory.fromBitmap(rotatedCompassBitmap)).position(
					convLocationToLatLng(Game.getGame().playerEgo.getLocation()),
					(float) (5000000 / Math.pow(2,
							googleMap.getCameraPosition().zoom))) // width w metrach
					);
		}
		playerSight.setCenter(convLocationToLatLng(Game.getGame().playerEgo
				.getLocation()));
	}

	@Override
	public void onCameraChange(final CameraPosition position) {
		drawCompass();
	}

	public static void refreshMapView() {
		if(Game.getGlobalTopmostActivity() instanceof MWMap2Activity)	{
			MWMap2Activity map = (MWMap2Activity) Game.getGlobalTopmostActivity();
			if(map.manaBar!=null)
				map.manaBar.invalidate();
			map.drawCompass();
			
			if(Options.LOCK_VIEW_TO_LOCATION)	{
				if(googleMap!=null)
					googleMap.animateCamera(
							CameraUpdateFactory.newCameraPosition(
						            new CameraPosition.Builder().target(
						            		Player.getInst().getLatLng())
				                    .zoom(16f)
				                    .bearing(azimut * 360 / (2 * 3.14159f))
				                    .tilt(40)
				                    .build()
				            ));		
			}
		}
	}

	public void centerAtNextTrigger(View v) {
		TriggerZone trigger = null;

		// TODO: po ostatnim trzeba kliknac 2 razy :|? wyjasnic czemu i poprawic
		Iterator<TriggerZone> i = Game.getGame().worldContainer
				.getTriggerZones().iterator();
		int iter = 0;
		while (iter < triggerNo) {
			if (i.hasNext()) {
				trigger = i.next();
				iter++;
			} else {
				iter = 0;
				break;
			}
		}
		triggerNo = iter + 1;

		if (trigger == null) {
			MWToast.showToast(R.string.game_map_notargets, Toast.LENGTH_SHORT);
		} else {
			Options.LOCK_VIEW_TO_LOCATION = false;
			this.setPosLocker();
			//mapView.getController().animateTo(trigger.getGeoPoint());
			googleMap.animateCamera(CameraUpdateFactory.newLatLng(trigger.getLatLng()));
		}
	}	
	
	public static GeoPoint convLatLngToGeopoint(LatLng point) {
		return new GeoPoint((int)(point.latitude*1E6), (int)(point.longitude*1E6));
	}

	public static LatLng convLocationToLatLng(Location l) {
		double lat = l.getLatitude();
		double lon = l.getLongitude();
		return new LatLng(lat, lon);
	}

	@Override
	public void onBackPressed() {
		currentTapListener = null;
		if(googleMap!=null)	
			googleMap.clear();
		super.onBackPressed();
	}

	public static void addTapListener(OnMapClickListener tapListener) {
		currentTapListener = tapListener;
	}
	
	@Override
	protected void onResumeFragments() {
		// TODO Auto-generated method stub
		super.onResumeFragments();
	}

	public static void removeTapListener() {
		currentTapListener = null;
		googleMap.setOnMapClickListener(null);
	}
	
}
