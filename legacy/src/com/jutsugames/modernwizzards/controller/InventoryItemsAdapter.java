package com.jutsugames.modernwizzards.controller;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Matrix.ScaleToFit;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class InventoryItemsAdapter extends BaseAdapter {

	private Context ctx;
	private Set<Integer> usedItems = new HashSet<Integer>();
	List<CollectPlant> plants = new LinkedList<CollectPlant>();

	public InventoryItemsAdapter(Context ctx) {
		this.ctx = ctx;
		plants = Game.getGame().getInventory().getPlants();
	}

	@Override
	public int getCount() {
		return plants.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View arg1, ViewGroup arg2) {
		//List<InventoryItem> inv = Game.getGame().getInventory();
		
		InventoryItem iItem = plants.get(position);
		ObjectInfo oInfo = iItem.getInfo();
		
		//Drawable visPic = ctx.getResources().getDrawable();
		ImageView picView = new ImageView(ctx);
		picView.setAdjustViewBounds(true);
		picView.setImageResource(oInfo.getImageID());

		//picView.setScaleType(ScaleType.)
		picView.setPadding(5, 5, 5, 5);
		
		if (!canUse(position)) {
			picView.setAlpha(64);
		}
		
		return picView;
	}
	
	public boolean canUse(int position) {
		return !usedItems.contains(position);
	}

	public void setUsed(int position) {
		usedItems.add(position);
	}

	public void clearAllItems() {
		plants = Game.getGame().getInventory().getPlants();
		usedItems.clear();
	}

	public List<CollectPlant> getSelectedItems() {
		List<CollectPlant> result = new LinkedList<CollectPlant>();
		
		for (Integer position : usedItems) {
			CollectPlant item = plants.get(position);
			result.add(item);
		}
		
		return result;
	}

	public InventoryItem getAtPosition(int position) {
		return plants.get(position);
	}
}
