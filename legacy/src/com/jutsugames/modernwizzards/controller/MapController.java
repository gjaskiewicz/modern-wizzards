package com.jutsugames.modernwizzards.controller;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.HashSet;
import java.util.LinkedList;

import android.graphics.Color;
import android.location.Location;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.MWMap2Activity;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;

public class MapController {
	private HashSet<PhysicalObject> objDisplayed = new HashSet<PhysicalObject>();
	private static MapController inst;
	
	private MapController()	{
	}
	
	public Circle displayObj(PhysicalObject po)	{
		objDisplayed.add(po);
		if(MWMap2Activity.googleMap!=null)	{
			//$log.d("MapController", "Adding circle...");
			po.dispCircle= MWMap2Activity.googleMap.addCircle(
					new CircleOptions()
						.center(MWMap2Activity.convLocationToLatLng(po.getLocation()))
						.radius(po.zone.radius)
						.strokeColor(Color.TRANSPARENT)
						.fillColor(po.getColor())
						.zIndex(10)
				);
			return po.dispCircle;
		}
		else return null;
	}
	
	public void dontDisplayObj(PhysicalObject po) {
		objDisplayed.remove(po);
	}
	
	/*public LatLng geopointToLatLng(GeoPoint l) {
		double lat = l.getLatitudeE6()/1E6;
		double lon = l.getLongitudeE6()/1000000;
		return new LatLng(lat, lon);
	}*/
	
	
	public static MapController getInst()	{
		if(inst==null) 
			inst = new MapController();
		return inst;
	}

	public void addAll(MWMap2Activity mwMap2Activity) {
		for(PhysicalObject po : objDisplayed)	{
			displayObj(po);
		}
	}
}
