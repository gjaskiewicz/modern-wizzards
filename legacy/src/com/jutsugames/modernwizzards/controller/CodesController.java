package com.jutsugames.modernwizzards.controller;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.mixtures.HealingPotion;
import com.jutsugames.modernwizzards.gamelogic.mixtures.ManaPotion;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import com.jutsugames.modernwizzards.util.SavesStorage;

public class CodesController {
	
	public static void parseCode(String code)	{
		//MWToast.makeToast(Game.mainActivity, "Parsing code: " + code, MWToast.LENGTH_LONG).show();
		
		if(code.equals("nogps"))	{
			Options.USE_GPS=false;
			Game.mainActivity.stopGPS();
			MWToast.makeToast(Game.mainActivity, "GPS: " + Options.USE_GPS , MWToast.LENGTH_LONG).show();
			Options.saveOptions(Game.mainActivity);
		}
		else if(code.equals("usegps"))	{
			Options.USE_GPS=true;
			MWToast.makeToast(Game.mainActivity, "GPS: " + Options.USE_GPS , MWToast.LENGTH_LONG).show();
			Options.saveOptions(Game.mainActivity);
		}
		else if(code.equals("resurect"))	{
			Player.getInst().resurect();
			Player.getInst().addManaPoints(1000, false);
			Player.getInst().addHealthPoints(1000, false);
		}
		//load [filename] 
		else if(code.contains("load"))	{
			String filename = code.subSequence(code.indexOf(" "), 0).toString();
			if(!filename.equals(""))	{
				//SavesStorage.getSaveFromFile(filename);
				return;
			}
			else	{
				return;
			}
		}
		else if(code.equals("giveall"))	{
			Player playerEgo = Player.getInst();
			
			playerEgo.inv.addPlant(new CollectPlant("item_daisyflower"));
			playerEgo.inv.addPlant(new CollectPlant("item_daisyflower"));
			playerEgo.inv.addPlant(new CollectPlant("item_fernflower"));
			playerEgo.inv.addPlant(new CollectPlant("item_fernflower"));
			playerEgo.inv.addPlant(new CollectPlant("item_4clover"));
			playerEgo.inv.addPlant(new CollectPlant("item_4clover"));
			playerEgo.inv.addPlant(new CollectPlant("item_rozkovnik"));
			playerEgo.inv.addPlant(new CollectPlant("item_rozkovnik"));
			playerEgo.inv.addPlant(new CollectPlant("item_garlic"));
			playerEgo.inv.addPlant(new CollectPlant("item_garlic"));
			playerEgo.inv.addPlant(new CollectPlant("item_swallowweed"));
			playerEgo.inv.addPlant(new CollectPlant("item_swallowweed"));
			playerEgo.inv.addPlant(new CollectPlant("item_cornerstone"));
			playerEgo.inv.addPlant(new CollectPlant("item_smurfblood"));
			playerEgo.inv.addPlant(new CollectPlant("item_cateye"));
			playerEgo.inv.addPlant(new CollectPlant("item_bunnyleg"));
			playerEgo.inv.addPlant(new CollectPlant("item_ectoplasma"));
			playerEgo.inv.addPlant(new CollectPlant("item_mandragora"));
			playerEgo.inv.addPlant(new CollectPlant("item_szczeciogon"));
			
			playerEgo.inv.addMixture(new SpellToken(new HealingPotion()));
			playerEgo.inv.addMixture(new SpellToken(new ManaPotion()));
			
			GameContent.getInstance().setAllAllowed();
			
			MWToast.makeToast(Game.mainActivity, "All given!" , MWToast.LENGTH_LONG).show();
		}
		else	{
			MWToast.makeToast(Game.mainActivity, "WRONG CODE", MWToast.LENGTH_LONG).show();
		}
		
	}
}
