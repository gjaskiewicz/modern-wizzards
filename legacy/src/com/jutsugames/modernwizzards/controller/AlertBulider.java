package com.jutsugames.modernwizzards.controller;

import com.jutsugames.modernwizzards.R;

public abstract class AlertBulider {
	public abstract void onYesClicked();
	public abstract void onNoClicked();
	public String message;
	public String objName;
	/** YES button only */
	public boolean infoOnly = false;
	public int yesButtonTx = R.string.game_global_yes;
	public int noButtonTx = R.string.game_global_no;
}
