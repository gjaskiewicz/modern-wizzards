package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import com.jutsugames.modernwizzards.util.MakeStringList;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MWToast extends Toast {

	private static int lastToastOffset=1;
	private static Toast lastOne;
	
	public MWToast(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public static boolean showToast(int textID, int duration)	{
		try {
			Toast t = makeToast(Game.getGlobalTopmostActivity(), textID, duration);
			if(t!=null) t.show();
		}
		catch(Exception e) 	{
			//no trudno...
			$log.w("MWToast", "Exception while showing toast : " + Game.getGlobalTopmostActivity().getResources().getString(textID) + "\n"+ e);
			return false;
		}
		return true;
	}

	/**
	 * przedawnione, bo potrafi rzucac wyjatki. U�yj showToast zamiat tego.
	 * @param context juz ci niepotrzebny
	 * @param text co chcesz zobaczyc 
	 * @param duration uzyj Toast.LENGTH_SHORT chyba ze masz inny pomysl
	 * @return
	 */
	@Deprecated
	public static Toast makeToast(Context context, int textID, int duration)	{
		String text = context.getResources().getString(textID);
		return makeToast(context, text, duration);
	}
	
	/**
	 * przedawnione, bo potrafi rzucac wyjatki. U�yj showToast zamiat tego.
	 * @param context juz ci niepotrzebny
	 * @param text co chcesz zobaczyc 
	 * @param duration
	 * @return
	 */
	@Deprecated
	public static Toast makeToast(Context context, CharSequence text, int duration)	{
		//schowa starego toasta jak ju� chce sie kolejny pojawic;
		
		Toast t = new Toast(context);
		t.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 20+lastToastOffset*20);
		if(lastToastOffset++>3) lastToastOffset=1;
		LayoutInflater inflater = Game.mainActivity.getLayoutInflater();
		View layout = inflater.inflate(R.layout.custom_toast,
		                               (ViewGroup) Game.mainActivity.findViewById(R.id.toast_layout_root));
		
		TextView textV = (TextView) layout.findViewById(R.id.text);
		textV.setText(text);
		
		$log.d("MWToast", "NEW TOAST: " + text);
		
		t.setDuration(duration);
		t.setView(layout);
		
		if(lastOne!=null) lastOne.cancel();
		lastOne = t;
		
		return t;
	}
	
}
