package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;
import com.jutsugames.modernwizzards.objects.PlantFactory;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;

public class InventoryDetailViewActivity extends Activity {
	
	private static final String TAG = "InventoryDetailViewActivity";
	private String result = null;
	//String info_name;
	//u�y� do podawania argumentu
	public static InventoryItem toDisplay;
	private InventoryItem itemDisplayed;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        $log.d(TAG, "InventoryDetailViewActivity starts");
        itemDisplayed=toDisplay;
        
        //info_name = extras.getString("info_name");
        ObjectInfo oi = itemDisplayed.getInfo();
        
        int nameViewId, descViewId, imageViewId;	
		nameViewId = R.id.inventory_detail_name;
		descViewId = R.id.inventory_detail_desc;
		imageViewId = R.id.inventory_detail_image;
		
		setContentView(R.layout.inventory_item_details);
        ((TextView)findViewById(nameViewId)).setText(oi.getName());
        TextView results = (TextView) findViewById(descViewId);
        results.setMovementMethod(new ScrollingMovementMethod());
        results.setText(oi.getDescription());
        ImageView image = (ImageView) findViewById(imageViewId);
        image.setImageResource(oi.getImageID());
        //image.setImageDrawable(getResources().getDrawable(oi.getImageID()));
		
	    Menubutton button_back = (Menubutton) findViewById(R.id.inventory_detail_back_button);
	    button_back.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	            finish();
	        }});
	    
	    final Intent alchemyIntent = new Intent(this,AlchemyActivity.class);
	    
	    Menubutton use_button = (Menubutton) findViewById(R.id.inventory_detail_use_button);
	    //TODO: to bardzo �le. 
	    if(itemDisplayed instanceof CollectPlant ) { //Object is a plant so we need to change button text to alchemy and onClick start the alchemy activity.
	    	use_button.setText(R.string.scr_alch_title);
	    	use_button.refreshText();
		    use_button.setOnClickListener(new OnClickListener() {
		        @Override
		        public void onClick(View v) {
		        	startActivity(alchemyIntent);
		            //finish();
		        }});
	    } else {
		    use_button.setOnClickListener(new OnClickListener() {
		        @Override
		        public void onClick(View v) {
		        	result = "useThis";
		            finish();
		        }});
	    }
        

    }
    
    @Override
    public void finish() {
      Intent data = new Intent();
      data.putExtra("returnValue", result);
      data.putExtra("info_name", itemDisplayed.getInfo().getID());
      
      // Activity finished ok, return the data
      setResult(RESULT_OK, data);
      super.finish();
    } 

    
}
