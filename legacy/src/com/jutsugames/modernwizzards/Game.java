package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Fight;
import com.jutsugames.modernwizzards.gamelogic.GameContent;
import com.jutsugames.modernwizzards.gamelogic.Inventory;
import com.jutsugames.modernwizzards.gamelogic.MainLoop;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.achivements.AchivContainer;
import com.jutsugames.modernwizzards.gamelogic.creatures.Bunny;
import com.jutsugames.modernwizzards.gamelogic.mixtures.HealingPotion;
import com.jutsugames.modernwizzards.gamelogic.mixtures.ManaPotion;
import com.jutsugames.modernwizzards.gis.Units;
import com.jutsugames.modernwizzards.map.WorldContainer;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.objects.inventory.SpellToken;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.objects.map.ManaZone;
import com.jutsugames.modernwizzards.quests.logic.Message;
import com.jutsugames.modernwizzards.quests.logic.Quest;
import com.jutsugames.modernwizzards.util.GameStateToSave;
import com.jutsugames.modernwizzards.util.RestoreFromSave;
import com.jutsugames.modernwizzards.util.SavesStorage;
import com.jutsugames.modernwizzards.util.SavesStorage.Save;
import com.jutsugames.modernwizzards.util.SoundsUtil;
import com.jutsugames.modernwizzards.util.VibratorUtil;

public class Game implements Serializable {
	private static final long serialVersionUID = -7120112300303679322L;
	private static final String TAG = "Game";
	
	public static List<RestoreFromSave> objectsToRestore = new LinkedList<RestoreFromSave>();
	
	/** oznacza czy gra juz kiedys byla uruchomiona? */
	public boolean isInitialized = false;

	private static transient Activity globalTopmostActivity;
	
	public transient LocationListener mLocationListener;
	
	public final class Actions {
		public static final String MENU_BUTTON_UPDATE = "com.jutsugames.modernwizzards.MENU_BUTTON_UPDATE";
		public static final String MESSAGE_OPENED = "com.jutsugames.modernwizzards.MESSAGE_OPENED";
		public static final String QUEST_DECISION = "com.jutsugames.modernwizzards.QUEST_DECISION";
		public static final String QUESTS_COLLECTION_CHANGED = "com.jutsugames.modernwizzards.QUESTS_COLLECTION_CHANGED";
		
		
		private Actions() {}
	}

	public enum GameState { 
		STATE_ERROR
			{{ c = -1; }},
		STATE_INIT 			//przed startem gry / PRZED OKRE�LENIEM LOKALIZACJI!
			{{ c = 0; }}, 		
		STATE_INTRO 		//poczatkowe akcje wprowadzajace /raczej zbedne
			{{ c = 1; }}, 		
		STATE_WALKING 		//normalna gra
			{{ c = 2; }},		
		STATE_FIGHTING 		//stan walki
			{{ c = 3; }},		
		STATE_PLAYERDEAD 	//gracz umar� - powr�t do SafeZone.
			{{c = 4; }};	
		
		int c;
	}
	//TODO: jeszcze jakies stany? Saving ? 
	
	private transient static Game instance;
	
	//transient public Thread gameLoopRunner;
	public transient CountDownTimer gameLoopRunner;
	
	/** czy lokalizacja zosta�a ustalona od ostatniego uruchomienia. Cholernie wa�ne przy generacji */
	public transient boolean  locationIsNotSet = true;

	//transient private ModernWizzardsActivity runningContext;
	
	public transient Fight fight;
	
	public Date lastGameSaveDate;
	private AchivContainer achivContainer;
	public WorldContainer worldContainer;
	public  Player playerEgo;
	private GameState gameState = GameState.STATE_INIT;
	public static transient ModernWizzardsActivity mainActivity;
	private static SavesStorage savesStorage;
	
	public static synchronized Game getGame() {
		if(instance == null)
			instance = new Game();
		return instance;
	}
	
	/**
	 * Gra wczytana z sejwa. DRegeneruje tez mana i zdrowko. 
	 * @param game
	 */
	public static synchronized void reloadGame(Game game) {
		instance = game;
		$log.i(TAG, "GAME RELOADED");
		
		//Regenerate only when player was not dead
		if(game.getGameState() == GameState.STATE_PLAYERDEAD) return;
		
		long secondsInSleep;
		if(instance.lastGameSaveDate!=null)	{
			secondsInSleep = 
				(Calendar.getInstance().getTime().getTime()-instance.lastGameSaveDate.getTime())/1000;
			$log.i(TAG, "SECONDS SINCE SAVE: " + secondsInSleep);
		}
		else	{
			secondsInSleep = 60 * 60 * 12; //12 godzin
			$log.i(TAG, "UNKNOWN SECONDS SINCE SAVE! ");
		}
		
		//liniowo, 100 pkt w 3000 sek = prawie godzina
		if(Game.getGame().gameState==GameState.STATE_WALKING)	{
			Game.getGame().playerEgo.addHealthPoints((int) (secondsInSleep/60) , false);
		}
		//full regen w 4 godziny
		Player.getInst().addManaPoints(
				secondsInSleep / (60*60*4/Player.getInst().manaRegenFactor) * Player.getInst().maxManaPoints
				, false);
		
		Game.getGame().getAchivContainer().getAchivDays().nextRun();
	}
	
	private Game () {
		worldContainer = new WorldContainer();
		achivContainer = new AchivContainer();
		playerEgo = new Player();
		gameState = GameState.STATE_INIT;
		
		mLocationListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location location) {
				if(location == null) return;
				$log.d(TAG, "new loc from: " + location.getProvider() + " acc: " + location.getAccuracy());
				if(location.getProvider() == LocationManager.NETWORK_PROVIDER)	{
					if(location.hasAccuracy() && location.getAccuracy() > 100) {
						Game.this.locationChanged(location);
					}
					else return;
				}
				else Game.this.locationChanged(location);
				
			}

			@Override
			public void onStatusChanged(String s, int i, Bundle bundle) {
			}

			@Override
			public void onProviderEnabled(String s) {
			}

			@Override
			public void onProviderDisabled(String s) {
			}
		};
		
		$log.d(TAG,"Game constructor out");
	}	

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		$log.d(TAG, "GAMESTATE changes to: " + gameState);
		this.gameState = gameState;
	}

	public void locationChanged(Location l) {
		//location initialized
		//$log.i(TAG, "new location, accuracy: " + l.getAccuracy() + "m");
		//MWMap2Activity.lastLocation = l;
		if(l == null) return;
		playerEgo.setLocation(l);
		
		getAchivContainer().getAchivDistanceTravel().addMetersTraveled(l);
		getAchivContainer().getAchivDistanceVariety().addMetersTraveled(l);
		
		if (locationIsNotSet) {
			locationIsNotSet = false;
			mainActivity.stopAnimationOnLogo();
			mainActivity.mainMenuView.invalidate();
			MWToast.showToast(R.string.game_gps_loc_set, Toast.LENGTH_SHORT);
			initiateGameState();
			Intent i = new Intent("com.jutsugames.modernwizzards.LOCATION_SET");
			Game.mainActivity.sendBroadcast(i);
		}
	}
	
	/**
	 * Odsiweza widok mapy, jak trzeba to i odwoluje sie do generacji element�w.
	 */
	public void refreshMap()	{
		
		worldContainer.generateObjects(playerEgo.getPlayerLocation());
		LinkedList<PhysicalObject> po = worldContainer.getAllGeneratedObjs();
		for(PhysicalObject obj : po) {
			if(Units.optimizedDistanceCheck(obj.getGeoPoint(), playerEgo.getPlayerLocation(), playerEgo.getSightRange())) {
				if(gameState!=GameState.STATE_PLAYERDEAD || obj instanceof ManaZone)
					obj.display(true);
				else
					obj.display(false);
			}
			
			else obj.display(false);
		}
	}
	
	public void invalidateMapView() {
		MWMap2Activity.refreshMapView();
	}
	
	private void initiateGameState() {
    	$log.d(TAG, "initiateGameState");
    	worldContainer.generateObjects(playerEgo.getGeoPoint());
    	
    	if(gameState != GameState.STATE_PLAYERDEAD) { 
    		gameState = GameState.STATE_WALKING;
		
    		//wy�wietlenie poczatkowych obiekt�w na mapie
			for(PhysicalObject obj : worldContainer.getAllGeneratedObjs()) {
				if(Units.optimizedDistanceCheck(obj.getGeoPoint(), playerEgo.getGeoPoint(), playerEgo.getSightRange()))
					obj.display(true);
			}
    	}
		
	}

	public synchronized void checkInteractions() {
		if(locationIsNotSet)	return;
		//w walce nie sprawdzamy
		//if(gameState == GameState.STATE_FIGHTING) return;
		
		GeoPoint location = playerEgo.getPlayerLocation();

		for(PhysicalObject obj : worldContainer.getAllGeneratedObjs()) {
			//$log.d(TAG, "Checking " + worldContainer.creatures.size() + "creature objects");
			if (obj.shouldInteract(location)) {
				obj.interact();
			}
		}		
		
		@SuppressWarnings("unchecked")
		LinkedList<PhysicalObject> otherObjectsClone = (LinkedList<PhysicalObject>) worldContainer.getAllQuestObjets();
		for(PhysicalObject obj : otherObjectsClone) {
			if (obj.shouldInteract(location)) {
				obj.interact();
			}
		}
	}

	public void setRunningContext(ModernWizzardsActivity applicationContext) {
		mainActivity = applicationContext;
	}

	/**
	 * U�yj Game.getGame().getGlobalTopmostActivity();
	 * @deprecated
	 */
	public Context getRunningContext() {
		return getGlobalTopmostActivity();
	}

	public void collectItem(CollectableItem collectableItem) {
		InventoryItem invItem = collectableItem.getItem();
		if(invItem instanceof CollectPlant)	{
			playerEgo.inv.addPlant((CollectPlant)invItem);
			int pop = ((CollectPlant) invItem).popularity;
			if(pop == 0) pop=20;
			int exp = 200 / pop;
			Game.getGame().playerEgo.addExperience(exp);
			Game.getGame().getAchivContainer().getAchivPlants().addCollectedPlant(((CollectPlant)invItem).getNameID());
		}
		else if (invItem instanceof SpellToken) {
			playerEgo.inv.addMixture((SpellToken)invItem);
		}
		else
			$log.e(TAG,"unknown type of collectable item");
		
		
		removeMapItem(collectableItem);
		collectableItem.collected();
		
		Game.saveGame(); 
	}

	public void removeMapItem(CollectableItem collectableItem) {
		$log.d(TAG, "removeMapItem " + collectableItem.getItem().getNameID());
		collectableItem.display(false);
		if(collectableItem.isGenerated)	{
			if(worldContainer.saveDate != Calendar.getInstance().getTime().getDate())	{
				worldContainer.creaturesDefeated.clear();
				worldContainer.gatheredPlants.clear();
				worldContainer.saveDate = Calendar.getInstance().getTime().getDate();
			}
			worldContainer.gatheredPlants.add(collectableItem.generationHash);	
			
			collectableItem.homeLocation.items.remove(collectableItem);
		}
		getGame().invalidateMapView();
	}

	public Inventory getInventory() {		
		return playerEgo.inv;
	}
	

	public void playerFightsCreature(Creature creature)	{
		gameState = Game.GameState.STATE_FIGHTING;
		fight = new Fight(Game.getGame().playerEgo, creature);
		//Game.getGame().mainActivity.gotoFighting(null, fight);
	}
	
	public void playerRunsFromCreature(Creature creature) {
		//TODO: zabrac health, mana, honor, experience?
		//Game.getGame().playerEgo.takeHealth(20);
		playerEgo.takeMana(20);
		creature.secondsNotToAttack=30;
		// sprawia, �e creature przez chwile nie atakuje znowu.
		
	}

	//TEST - remove later
		void initTestData() {
			//playerEgo.healthPoints = 80;
			//playerEgo.manaPoints = 60;

			//TEST OBJECTS::
			//TriggerZone tz = new TriggerZone( new GeoPoint((int)(playerEgo.getLatitude()*1E6), (int)(playerEgo.getLongitude()*1E6)), 30);
			//tz.moveObject(400.0, 90);
			//tz.createVis(gameVisual);
			//worldContainer.otherObjects.add(tz);
			

		}

		public static Activity getGlobalTopmostActivity() {
			if(globalTopmostActivity==null)
					$log.e(TAG, "NULL globalTopmostActivity!!!");
			return globalTopmostActivity;
		}
		
		public static void setGlobalTopmostActivity(Activity acv) {
			if(globalTopmostActivity!=null)
				$log.d("SCREEN", "SCREEN CHANGES FROM ACTIVITY: "+ globalTopmostActivity.getClass().getName());
			else 
				$log.w("SCREEN", "EMPTY globalTopmostActivity");
			if(acv != null)
				$log.d("SCREEN", "SCREEN CHANGES TO ACTIVITY: " + acv.getClass().getName());
			//$log.d("SCREEN", "SCREEN CHANGES TO ACTIVITY: ");
			globalTopmostActivity = acv;
		}
		
		public static void turnOnOffNotificationForButton(int id, boolean onOff) {
			Intent i = new Intent(Game.Actions.MENU_BUTTON_UPDATE).putExtra("buttinID", id).putExtra("onoff", onOff);
			globalTopmostActivity.sendBroadcast(i);
		}
		
		public static void questsCollectionChanged() {
			Intent i = new Intent(Game.Actions.QUESTS_COLLECTION_CHANGED);
			globalTopmostActivity.sendBroadcast(i);
		}
		
		public static void saveGame() {
			//TODO: needs synchronization with nextQuestTimer (save have to finish before timer will be triggered)  
			
			if(!Options.SAVE_AND_LOAD) return;
			try {
				instance.lastGameSaveDate=Calendar.getInstance().getTime();
				
				GameStateToSave gs = new GameStateToSave();
				if(savesStorage==null)
					savesStorage = new SavesStorage(Game.getGlobalTopmostActivity());
				gs.dumpGameState();
			
				Save save = savesStorage.storeSave(gs);
				savesStorage.close();
				savesStorage.backUpSavesToExternalStorage();
				savesStorage.saveGameStateToExternalStorage(save.save, true);
				$log.d(TAG, "Dump success: id: " + save.id);
			} catch (Exception e) {
				$log.e(TAG, "Dump failed " + e.getMessage() );
			}
		}
		
		public static void deleteAllSaves() {
			SavesStorage savesStorage = new SavesStorage(Game.getGlobalTopmostActivity());
			savesStorage.deleteAllSaves();
		}
		
	public void showNewMessageAlert(Quest quest) {
		if(Game.getGame().getGameState()==GameState.STATE_FIGHTING) return;
		long[] vPatern = {0, 100,100,100,100,100};
		VibratorUtil.vibrate(vPatern, -1);
		SoundsUtil.playSound(R.raw.message, mainActivity);
		Game.getGlobalTopmostActivity().startActivity(
				new Intent(Game.getGlobalTopmostActivity(), AlertActivity.class)
				.putExtra(AlertActivity.ALERT_TYPE, AlertActivity.ALERT_TYPE_MESSAGE)
				.putExtra("questClassName", quest.getClass().getName())
				);
	}    	
		
	public AchivContainer getAchivContainer()	{
		if(achivContainer== null ) achivContainer = new AchivContainer();
		return achivContainer;
	}
}
