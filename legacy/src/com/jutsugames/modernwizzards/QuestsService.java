package com.jutsugames.modernwizzards;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class QuestsService extends Service {
	private static final String TAG = "QuestsService";
	
	private BroadcastReceiver eventsReceiver;
	private IntentFilter eventFilter;
	
	@Override
	public void onCreate() {
		super.onCreate();
		$log.d(TAG, "QuestsService created");
		eventFilter = new IntentFilter("com.jutsugames.modernwizzards.EVENT");
		eventsReceiver =  new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String objID = intent.getStringExtra("objID");
				$log.d(TAG, "Event to service: objID: " + objID );
			}
        };
        
        registerReceiver(eventsReceiver, eventFilter);
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//start();
		$log.d(TAG, "QuestsService started");
		return super.onStartCommand(intent, flags, startId);
	}
	
	@Override
	public void onDestroy() {
		$log.d(TAG, "QuestsService stopped");
		unregisterReceiver(eventsReceiver);
		super.onDestroy();
	}

}
