package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;
import com.jutsugames.modernwizzards.objects.PlantFactory;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;

public class CreatureDetailViewActivity extends Activity {
	
	private static final String TAG = "CreatureDetailViewActivity";
	private String result = null;
	//String info_name;
	//u�y� do podawania argumentu
	public static Creature toDisplay;
	private Creature itemDisplayed;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        $log.d(TAG, "CreatureDetailViewActivity starts");
        itemDisplayed=toDisplay;
        
        //info_name = extras.getString("info_name");
        ObjectInfo oi = itemDisplayed.getInfo(this);
        
        int nameViewId, descViewId, imageViewId;	
		nameViewId = R.id.creature_detail_name;
		descViewId = R.id.creature_detail_desc;
		imageViewId = R.id.creature_detail_image;
		
		setContentView(R.layout.creature_item_details);
        ((TextView)findViewById(nameViewId)).setText(oi.getName());
        TextView results = (TextView) findViewById(descViewId);
        results.setMovementMethod(new ScrollingMovementMethod());
        results.setText(oi.getDescription());
        ImageView image = (ImageView) findViewById(imageViewId);
        image.setImageResource(oi.getImageID());
        //image.setImageDrawable(getResources().getDrawable(oi.getImageID()));

    }
    
    @Override
    public void finish() {
      Intent data = new Intent();
      data.putExtra("returnValue", result);
      data.putExtra("info_name", itemDisplayed.getInfo(this).getID());
      
      // Activity finished ok, return the data
      setResult(RESULT_OK, data);
      super.finish();
    } 

    
}
