package com.jutsugames.modernwizzards;

import java.util.HashMap;

import android.app.Application;
import org.acra.*;
import org.acra.annotation.*;

import com.jutsugames.modernwizzards.util.ACRAPostSender;

@ReportsCrashes(formKey = "", formUri = "http://www.yourselectedbackend.com/reportpath")
public class MWApplication extends Application {
  @Override
  public void onCreate() {
    // The following line triggers the initialization of ACRA

    ACRA.init(this);
    super.onCreate();
    HashMap<String,String> ACRAData = new HashMap<String,String>();
	//ACRAData.put("LastSave", "unset yet");
    ACRA.getErrorReporter().setReportSender(new ACRAPostSender(ACRAData));
  }
}