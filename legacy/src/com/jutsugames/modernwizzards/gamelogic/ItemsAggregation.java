package com.jutsugames.modernwizzards.gamelogic;

import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;

public class ItemsAggregation implements InventoryItem {
	public int count = 1;
	public InventoryItem item;
	public ItemsAggregation(InventoryItem cp) {
		item = cp;
		count = 1;
	}
	@Override
	public ObjectInfo getInfo() {
		return item.getInfo();
	}
	
	@Override
	public int getColor() {
		return item.getColor();
	}
	
	@Override
	public void use() {
		item.use();
		
	}
	
	public String getNameID() {
		return item.getNameID();
	}
}
