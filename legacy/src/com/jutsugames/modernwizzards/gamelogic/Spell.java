package com.jutsugames.modernwizzards.gamelogic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.*;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;
import com.jutsugames.modernwizzards.util.EventEmiter;
import com.jutsugames.modernwizzards.util.RestoreFromSave;
import com.jutsugames.modernwizzards.util.ChangeListener.EventTypes;
import com.jutsugames.modernwizzards.views.CreatureBehaviorView;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public abstract class Spell extends EventEmiter implements Castable, RestoreFromSave {
	private static final String TAG = "SPELL";
	
	//technical string name - z tego wynika nazwa i opis i rysunek. 
	public String nameID;
	//ile mana zabierze z puli
	public int requiredMana;
	//jesli 1 - zawsze trafia, jesli 0 - nigdy nie trafia
	public float chanceToSuccess = 1;
	//ilosc iteracji przez ile daje efekt
	public int turnDuration;
	//tylko dla bitewnych - szerokosc dzialania czaru
	public int behaviourRange = 100;
	
	public UsageType usageType;
	public EffectTargetType targetType;
	
	protected boolean playerIsOwner = false;
	protected boolean playerIsTarget = false;
	protected transient Creature owner;
	protected transient Creature target;
	
	protected transient Paint painter;
	private int displayTime = 30;
	
	public Spell()	{}
	
	public Spell(Creature owner)	{
		this.owner = owner;
		if(owner == Player.getInst())
			playerIsOwner = true;
		
		painter = new Paint();
		painter.setColor(0x88FF0000);
	}
	
	@Override
	public void onRestoreFromSave() {
		if(playerIsOwner)
			owner = Player.getInst();
		if(playerIsTarget)
			target = Player.getInst();
	} 
	
	@Override
	public ObjectInfo getInfo() {
		return ObjectInfoFactory.get().getInfoStandarized(nameID);
	}

	@Override
	public int getRequiredMana() {
		return requiredMana;
	}

	@Override
	public boolean castThis() {
		$log.d(TAG, "CASTING :" + this.nameID);
		Context ctx = Game.getGlobalTopmostActivity();
		//jesli nie ma wystarczajaco mana
		if(!owner.takeMana(getRequiredMana()))	{
			$log.d("SPELL", "CASTING :" + this.nameID + " FAILED! NO MANA!");
			if(owner==Game.getGame().playerEgo)	{
				MWToast.showToast(R.string.spells_nomana, Toast.LENGTH_SHORT);
			}
			return false; //TODO: sprawdzenie gdzie indziej?
		}
		
		//pow�tpiewam czy to ma sens
		/*if(	Math.random() > this.chanceToSuccess )	{
			$log.d(TAG, "CASTING :" + this.nameID + " FAILED TO CAST!");
			//czar sie nie udal, "chybil"
			if(owner==Game.getGame().playerEgo)	{				
				MWToast.makeToast( ctx, "Failed to cast!", Toast.LENGTH_SHORT).show();
			} 
			else	{				
				MWToast.makeToast( ctx, 
						owner.getInfo(ctx).getName() + " failed to attack!", Toast.LENGTH_SHORT).show();
			}
			return false;
		}*/
		//else czar sie udal
		
		//dodaje do listy
		if(this.getEffectTargetType()==EffectTargetType.SELF)	{
			$log.d(TAG, "Adding self to effect target. Name: " + owner.nameID  );
			owner.underEffectOfSpell.add(this);
		}
		else if(this.getEffectTargetType()==EffectTargetType.ANOTHER)	{
			if(target==Game.getGame().fight.creature)
				Game.getGame().fight.controller.addSpellToDisplay(this);
			if(checkIfSpellHits())	{
				target.underEffectOfSpell.add(this);
			}
		}
		else {
			//AMBIENT//nic do roboty, tylko "take effect" 
		}
		$log.d(TAG, "CASTING " + this.nameID + " SUCCESS");
		return true;
	}
	
	public void setTarget(Creature creature)	{
		target = creature;
		if(target == Game.getGame().playerEgo)
			playerIsTarget = true;
		else
			playerIsTarget = false;
	}

	@Override
	public UsageType getUsageType() {
		return usageType;
	}
	
	public void takeEffect()	{
		turnDuration--;
		if(turnDuration==0)	{
			$log.d("SPELL", "this spell should be removed");
		}
	}
	
	@Override
	public EffectTargetType getEffectTargetType() {
		return targetType;
	}
	
	private boolean checkIfSpellHits()	{
		if(Math.abs(target.getBehaviourPosition()) < this.behaviourRange ) 	{
			//MWToast.makeToast( Game.getGlobalTopmostActivity(), "Spell HITS!", Toast.LENGTH_SHORT).show();
			if(target.isUnderEffectOfSpell("spell_astralshield"))	{
				MWToast.showToast(R.string.spells_repeled, Toast.LENGTH_SHORT);
				//TODO: odbija w przeciwnika?
				return false;
			}
			return true;
		}
		else {
			MWToast.showToast(R.string.spells_miss, Toast.LENGTH_SHORT);
			return false;
		}
	}

	/**
	 * efekt czaru w walce. Do przeciazania.
	 * @param canvas
	 * @param cbv
	 */
	public void paintEffect(Canvas canvas, CreatureBehaviorView cbv) {
		if(displayTime--<=0) return;
		painter.setAlpha(displayTime*7);
		canvas.drawRect(cbv.behavPosToX(0)-cbv.spellWidthToX(behaviourRange), 
				10,
				cbv.behavPosToX(0)+cbv.spellWidthToX(behaviourRange),
				cbv.getHeight()-10, painter);	
	}
	
	public void emitEventCreatedByAlchemy(List<String> usedPlants) {
		Map<String,Object> extraParams = new HashMap<String, Object>();
		extraParams.put("usedPlants", usedPlants);
		notifyListeners(nameID, EventTypes.CREATED_BY_ALCHEMY, extraParams);
	}
	
}
