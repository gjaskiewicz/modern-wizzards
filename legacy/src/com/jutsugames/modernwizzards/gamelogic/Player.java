package com.jutsugames.modernwizzards.gamelogic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.Game.GameState;
import com.jutsugames.modernwizzards.gamelogic.Castable.UsageType;
import com.jutsugames.modernwizzards.gamelogic.spells.OutOfBodyExperience;
import com.jutsugames.modernwizzards.gis.SphericalZone;
import com.jutsugames.modernwizzards.quests.display.MessageViewActivity;
import com.jutsugames.modernwizzards.quests.logic.Message;
import com.jutsugames.modernwizzards.quests.logic.QuestsEngine;
import com.jutsugames.modernwizzards.util.ChangeListener.EventTypes;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class Player extends Creature {
	private static final String TAG = "Player";
	
	private int PLAYER_COLOR = 0xFF000000;
	
	/**  pole widzenia mniejwiecej w metrach*/
	private int sightRange = 300;
	
	private LinkedList<String> knownGlobalSpells = new LinkedList<String>();
	
	public double chanceToSpott = 1.0;	
	private transient OverlayItem vis;
	
	/** pozycja w Out Of Body*/
	public GeoPoint oobLocation;
	//public transient SightRange sightIndicator;
	
	/** ilosc pkt doswiadczenia */
	public int experience;
	/** obecny level */
	public int expLevel;
	/** factor do regeneracji mana */
	public double manaRegenFactor=2;
	
	/** czy juz zginal kiedys */
	public boolean killedOnce = false;
	/** czy ukonczyl pierwsza walke */
	public boolean firstFightDone = false;
	
	//Some skills/options activators
	/*public boolean hasPlantsOnMap = false;
	public boolean hasCreaturesOnMap = false;
	public boolean hasAlchemyActive = false;
	public boolean hasManaZonesOnMap = false;
	*/
	public Player() {
		super();
		healthPoints = 100;
		manaPoints = 100;
		maxHealthPoints = 100;
		maxManaPoints = 100;
		experience = 0;
		nameID = "player";
		
		zone=new SphericalZone(null, 0);
		
		addKnownFightSpell("spell_physical");
	}
	
	public int getSightRange() {
		if(isUnderEffectOfSpell("potion_perception")) return 550;
		return sightRange;
	}	
	
	public void setSightRange(int range) {
		sightRange = range;
	}	
	
	/**
	 * Dodaje nowy czar do listy umianych. Zapewnia nieduplikowanie.
	 * @param spellID id czaru. 
	 */
	public void addKnownFightSpell(String spellID) {
		for( String spl : this.knownFightSpells)	{
			if(spl.equals(spellID))
				return;
		}
		knownFightSpells.add(spellID);
	}	
	
	public void addKnownSpell(String spellID) {
		if(knownGlobalSpells.contains(spellID)) return;
		knownGlobalSpells.add(spellID);
	}
	
	public LinkedList<String> getKnownSpells() {
		return knownGlobalSpells;
	}
	
/*	public List<String> getKnownGlobalSpells()	{
		List<String> result = new LinkedList<String>();
		for(String spell : knownSpells )	{
			Spell spl = SpellFactory.getSpellByID(spell, this);
			if(spl.getUsageType()==UsageType.MAP || spl.getUsageType()==UsageType.ANYTIME)
				result.add(spell);
		}
		return result;
	}*/
	
/*	public void updateDisplay()	{
		ShapeMarker marker = MWMapActivity.getGameVisual();
		deleteVis(marker);
		createVis(marker);
	}*/

	/** 
	 * Przeciazenie dla Player - wywolywane w chwili smierci wszelakiej. 
	 * (non-Javadoc)
	 * @see com.jutsugames.modernwizzards.gamelogic.Creature#die()
	 */
	@Override
	public void die() {
		notifyListeners(nameID, EventTypes.KILLED, null);
		Game.getGame().setGameState(GameState.STATE_PLAYERDEAD);
		underEffectOfSpell = new LinkedList<Spell>();
		manaPoints=0;
		if(!killedOnce) {
			killedOnce = true;
			
			Intent i = new Intent(Game.mainActivity, MessageViewActivity.class);
			//i.putExtra("title", Game.mainActivity.getResources().getString(R.string.resuraction));
			i.putExtra("message", Game.mainActivity.getResources().getString(R.string.first_player_death));
			i.putExtra(MessageViewActivity.MESSAGE_TYPE, MessageViewActivity.MESSAGE_TYPE_NARRATION);
			i.putExtra(MessageViewActivity.MESSAGE_CANCLOSE, true);
			i.putExtra("readOnly",true);
			Game.mainActivity.startActivity(i);
		}
		
		Game.saveGame();
	}	
	
	public GeoPoint getPlayerLocation() {
		if(isUnderEffectOfSpell("spell_oob"))	{
			//Log.d("PLAYER", "returning oob location");
			return oobLocation;
		}
		return getGeoPoint();
	}

	/**
	 * Wskrzesza gracza. 
	 */
	public void resurect() {
		healthPoints = 10;
		manaPoints = 10;
		Game.getGame().setGameState(GameState.STATE_WALKING);
		MWToast.makeToast(Game.getGlobalTopmostActivity(), R.string.player_resurected, Toast.LENGTH_LONG).show();
	}
	
	public void endOutOfBody(boolean immediate) {
		for( Spell spell : underEffectOfSpell) {
			if("spell_oob".equals(spell.nameID))
				((OutOfBodyExperience)spell).forceReturnToBody(immediate); //setting return to body
		}
	}

	/** dodaje exp i troszczy sie o levelowanie
	 * @param howMuch ile expa
	 */
	public void addExperience(int howMuch)	{
		if(getExpForLevel(expLevel+1) < experience+howMuch)	{
			levelUp();
		}
		experience+=howMuch;
	}
	
	private void levelUp() {
		expLevel++;
		$log.i(TAG, "PLAYER LEVEL UP TO: " + expLevel);
		MWToast.showToast(R.string.player_levelup, Toast.LENGTH_SHORT);
		//wi�ksze limity mana i hp
		double factor = (double)manaPoints/(double)maxManaPoints;
		maxManaPoints+=10;
		manaPoints=factor*(double)maxManaPoints;
		factor = (double)healthPoints/(double)maxHealthPoints;
		maxHealthPoints+=10;
		healthPoints=(int) (factor*(double)maxHealthPoints);
		//wi�ksze pole widzenia
		sightRange=300+expLevel*30;
		//szybciej regenetuje 
		manaRegenFactor+=0.5;
		//odblokowac kolejne czary...
		  if(expLevel==2)	{
			addKnownFightSpell("spell_fireball");
		} if(expLevel==3)	{
			addKnownFightSpell("spell_astralshield");
		} if(expLevel==4)	{
			addKnownFightSpell("spell_electrokinesis");
		}
		//i tak dalej...
	}

	
	public int getExpForLevel(int level)	{
		int sum=0;
		for(int i = 1; i<=level ; i++)	{
			sum+=i;
		}
		return sum*1000;
	}
	
	private class SightRange extends PhysicalObject	{
		
		public SightRange()	{
			setLocation(Game.getGame().playerEgo.getGeoPoint());
			zone = new SphericalZone(
					Game.getGame().playerEgo.getGeoPoint(), 
					Game.getGame().playerEgo.getSightRange());
		}
		
		
		@Override
		public void display(boolean display) {
			super.display(display);
		}
	}

	public static Player getInst() {
		return Game.getGame().playerEgo;
	}
	
	@Override
	public void display(boolean display) {
		super.display(display);
	}
}
