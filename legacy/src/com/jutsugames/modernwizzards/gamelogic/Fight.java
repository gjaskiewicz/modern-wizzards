package com.jutsugames.modernwizzards.gamelogic;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;

import com.jutsugames.modernwizzards.FightActivity;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.StatsActivity;
import com.jutsugames.modernwizzards.Game.GameState;
import com.jutsugames.modernwizzards.gamelogic.Castable.EffectTargetType;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class Fight {
	//nr iteracji
	private long fightIteration = 1;

	public Creature creature;
	public FightActivity controller;
	
	/**oznacza czy walka jeszcze trwa czy juz sie skonczyla (ale stan gry jeszcze zostaje)*/
	private boolean finished = false;
	public boolean pauseFight = false;
	
	public Fight(Player playerEgo, Creature fightingWith) {
		$log.d("FIGHT", "FIGHT STARTS");
		this.creature = fightingWith;
		playerEgo.endOutOfBody(true);
		Activity topmost = Game.mainActivity;
		topmost.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
		topmost.startActivity(new Intent(topmost, FightActivity.class));
		//controller = (FightActivity)Game.getGame().getGlobalTopmostActivity();
	}
	
	public long getFightIteration() {
		return fightIteration;
	}
	
	public void iterateFight()	{
		//$log.d("FIGHT", "fight iter!: "+ fightIteration);
		if(pauseFight) return;
		if(finished) return;
		else {
			fightIteration++;
	    	creature.updateBehaviourPosition(fightIteration);
	    	creature.thinkFight(this);
	    	
	    	takeEffects();
	    	checkIfFightEnds();
	    	
	    	//controller.fightingView.postInvalidate(); 
	    	if(controller!=null)
	    		controller.refresh();
		}
	}
	
	public void playerCastsSpell(Spell spell)	{
		spell.setTarget(creature);
		if(!spell.castThis())
			return;
	}
	
	public void creatureCastsSpell(Spell spell)	{
		controller.creatureCastsSpell(spell);
	    spell.setTarget(Game.getGame().playerEgo);
		spell.castThis();
	}

	private void takeEffects() {
		//for(Castable spell : Game.getGame().playerEgo.underEffectOfSpell)
		//	spell.takeEffect();
		for(Castable spell : creature.underEffectOfSpell)
			spell.takeEffect();
		
		creature.removeFinishedSpells();
		//Game.getGame().playerEgo.removeFinishedSpells();
	}
	
	public void fightEnds() {
		$log.d("FIGHT", "FIGHT FINISH");
		controller.finish();
		//Game.getGame().gameLoop.fight = null;
	}

	private boolean checkIfFightEnds() {
		if(Game.getGame().playerEgo.getHP()==0)	{
			playerLost();
			return true;
		}
		if(creature.getHP()==0)	{
			playerWon();
			creature.die();
			return true;
		}
		return false;
	}

	public void playerRunsFromTheFight()	{
		$log.d("FIGHT", "PLAYER RUNS FROM THE FIGHT!");
		Game.getGame().playerEgo.takeHealth(20);
		Game.getGame().playerEgo.takeMana(20);
		creature.secondsNotToAttack=30;
		Game.getGame().setGameState(Game.GameState.STATE_WALKING);
		fightEnds();	
	}
	
	private void playerWon() {
		$log.d("FIGHT", "PLAYER WINS");
		Player.getInst().firstFightDone = true;
		finished=true;
		//Game.getGame().gameState = Game.GameState.STATE_WALKING;
		controller.showWinAlert();
	}

	private void playerLost() {
		$log.d("FIGHT", "PLAYER HAS LOST AND DIED");
		finished=true;
		controller.showLooseAlert();
		Game.getGame().playerEgo.die();
	}

	public void winAlertConfirmed() {
		Player.getInst().firstFightDone = true;
		fightEnds();
		//dodaj experience
		Game.getGame().playerEgo.addExperience(creature.getExperienceForDefeat());
		//zmieniamy stan
		Game.getGame().setGameState(GameState.STATE_WALKING);
		creature.giveContainingItem();
	}

	public void instructionConfirmed() {
		pauseFight=false;
	}
	
}
