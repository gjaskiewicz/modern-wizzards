package com.jutsugames.modernwizzards.gamelogic;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.jutsugames.modernwizzards.AllSpells;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.gamelogic.creatures.*;
import com.jutsugames.modernwizzards.gamelogic.mixtures.*;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;

/**
 * Przechowuje info o tym co gracz wie, umie, jakie funkcje w grze ma dost�pne.
 * Bardzo do serializacji. 
 * @author bartek
 */
public class GameContent {
	private static GameContent instance;
	
	public boolean isAlchemyActive = false;
	public HashSet<String> knownCreatures = new HashSet<String>();
	public HashSet<String> knownPlants = new HashSet<String>();
	public HashSet<String> knownPotions = new HashSet<String>();
	public boolean generateManaZones = false;
	//TODO: potrzeby transient? chyba nie
	private transient LinkedList<Creature> allCreatures;
	private transient LinkedList<SpellFromPotion> allPotions;
	private transient LinkedList<CollectPlant> allPlants;
	
	public List<CollectPlant> getAllPlantsTypes() {
		if(allPlants == null) {
			allPlants = new LinkedList<CollectPlant>();
			//plantNameOnPlant = new HashMap<String, CollectPlant>();
		}
		else
			return allPlants;
		
		CollectPlant tempPlant = new CollectPlant("item_daisyflower");
		tempPlant.popularity = 10;
		allPlants.add(tempPlant);
		
		tempPlant = new CollectPlant("item_fernflower");
		tempPlant.popularity = 3;
		allPlants.add(tempPlant);
		
		tempPlant = new CollectPlant("item_rozkovnik");
		tempPlant.popularity = 7;
		allPlants.add(tempPlant);
		
		tempPlant = new CollectPlant("item_szczeciogon");
		tempPlant.popularity = 5;
		allPlants.add(tempPlant);
		
		tempPlant = new CollectPlant("item_cornerstone");
		tempPlant.popularity = 5;
		allPlants.add(tempPlant);
		
		tempPlant = new CollectPlant("item_4clover");
		tempPlant.popularity = 8;
		allPlants.add(tempPlant);
		
		tempPlant = new CollectPlant("item_mandragora");
		tempPlant.popularity = 4;
		allPlants.add(tempPlant);
		
		tempPlant = new CollectPlant("item_garlic");
		tempPlant.popularity = 9;
		allPlants.add(tempPlant);
		
		tempPlant = new CollectPlant("item_swallowweed");
		tempPlant.popularity = 8;
		allPlants.add(tempPlant);
			
		for(CollectPlant cp : allPlants)	{
			cp.popularityOffset = CollectPlant.plantsPopularitySum;
			CollectPlant.plantsPopularitySum += cp.popularity;
		}
		return allPlants;
		
	}
	
	/**
	 * Odblokowuje wszystko
	 */
	public void setAllAllowed()	{
		isAlchemyActive=true;
		generateManaZones=true;
		for(Creature c : getAllCreaturesTypes())	{
			knownCreatures.add(c.nameID);
		}
		
		//knownCreatures.add("creature_warewolf");
		
		for(CollectPlant c : getAllPlantsTypes())	{
			knownPlants.add(c.getNameID());
		}
		Game.getGame().playerEgo.setSightRange(500);
		Game.getGame().playerEgo.addKnownFightSpell("spell_fireball");
		Game.getGame().playerEgo.addKnownFightSpell("spell_astralshield");
		Game.getGame().playerEgo.addKnownFightSpell("spell_electrokinesis");
		Game.getGame().playerEgo.addKnownSpell("spell_oob");
		Game.getGame().playerEgo.addKnownSpell("spell_telekinesis");
		
		knownPotions.add("potion_health");
		knownPotions.add("potion_mana");
		knownPotions.add("potion_luckjelly");
		knownPotions.add("potion_invisibility");
		knownPotions.add("potion_antidote");
		knownPotions.add("potion_stimulation");
		knownPotions.add("potion_perception");
	}
	
	/**
	 * Zwraca list� wszystkich TYP�W stwork�w istniejacych w grze, jako referencj� np do generacji mapy
	 * oraz do Bestiariusza. NIE MODYFIKOWA� WYNIK�W!
	 * @return jak w opisie
	 */
	public List<Creature> getAllCreaturesTypes() {
		if(allCreatures == null) {
			allCreatures = new LinkedList<Creature>();
		}
		else
			return allCreatures;
		
		addCreature(new Bunny());
		addCreature(new Bat());
		addCreature(new BlackCat());
		addCreature(new Gnome());
		addCreature(new Strumpf());
		addCreature(new Wraith());
		addCreature(new Golem());
		addCreature(new Warewolf());
		
		return allCreatures;
	}
	
	/**
	 * Zwraca list� wszystkich TYP�W MIKSTUR istniejacych w grze, jako referencj� np do receptur
	 * NIE MODYFIKOWA� WYNIK�W!
	 * @return jak w opisie
	 */
	public List<SpellFromPotion> getAllPotionsTypes() {
		if(allPotions == null) {
			allPotions = new LinkedList<SpellFromPotion>();
			allPotions.add(new HealingPotion());
			allPotions.add(new ManaPotion());
			allPotions.add(new AntidotePotion());
			allPotions.add(new InvisibilityPotion());
			allPotions.add(new PerceptionPotion());
			allPotions.add(new StimulationPotion());
			allPotions.add(new LuckPotion());
			return allPotions;
		}
		else
			return allPotions;
	}	
	
	private void addCreature(Creature add)	{
		add.popularityOffset=Creature.creaturesPopularitySum;
		Creature.creaturesPopularitySum+=add.popularity;
		allCreatures.add(add);
	}

	public static GameContent getInstance() {
		if(instance == null)
			instance = new GameContent();
		return instance;
	}
	
	public static synchronized void reloadGameContent(GameContent gc) {
		instance = gc;
	}
	
	public LinkedList<SpellFromPotion> getPotionsForIngredients(String ing1, String ing2, String ing3)	{
		LinkedList<SpellFromPotion> res = new LinkedList<SpellFromPotion>();
		
		for(SpellFromPotion sp : getAllPotionsTypes())	{
			if(sp.canMakeFromIng(ing1, ing2, ing3))
				res.add(sp);
		}
			
		return res;
	}
}
