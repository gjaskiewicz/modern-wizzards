package com.jutsugames.modernwizzards.gamelogic;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import com.jutsugames.modernwizzards.gamelogic.mixtures.*;
import com.jutsugames.modernwizzards.gamelogic.spells.*;

public class SpellFactory {
	private static final String TAG = "SpellFactory";
	
	public static Spell getSpellByID(String spellID, Creature owner)	{
		$log.d(TAG, "getSpellByID " +  spellID);
		//battle spells:
		if(spellID.equals("spell_physical"))	
			return new PhysicalAttack(owner);
		if(spellID.equals("spell_battlespeed"))	
			return new Speed(owner);
		if(spellID.equals("spell_astralshield"))	
			return new AstralShield(owner);
		else if(spellID.equals("spell_fireball"))	
			return new Fireball(owner);
		else if(spellID.equals("spell_electrokinesis"))	
			return new Electrokinesis(owner);
		
		//env spells:
		else if(spellID.equals("spell_oob"))	
			return new OutOfBodyExperience(owner);
		else if(spellID.equals("spell_telekinesis"))	
			return new Telekinesis(owner);	
		
		//TODO: to tutaj czy osobna fabryka? a moze po prostu Class.forName ?
		//potions
		else if(spellID.equals("potion_health"))	
			return new HealingPotion();	
		else if(spellID.equals("potion_mana"))	
			return new ManaPotion();	
		else if(spellID.equals("potion_antidote"))	
			return new AntidotePotion();	
		else if(spellID.equals("potion_initial"))	
			return new InitialPotion();	
		else if(spellID.equals("potion_stimulation"))	
			return new StimulationPotion();	
		else if(spellID.equals("potion_perception"))	
			return new PerceptionPotion();	
		else if(spellID.equals("potion_invisibility"))	
			return new InvisibilityPotion();
		else if(spellID.equals("potion_luckjelly"))	
			return new LuckPotion();
		
		$log.e("SPELL FACTORY", "UNKNOWN SPELL ID!!: " + spellID );
		return null;
	}
}
