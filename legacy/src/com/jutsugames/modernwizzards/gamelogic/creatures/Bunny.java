package com.jutsugames.modernwizzards.gamelogic.creatures;

import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.FightBehaviour;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.spells.PhysicalAttack;
import com.jutsugames.modernwizzards.gamelogic.spells.Speed;

public class Bunny extends Creature {
	
	public static class BunnyBehaviour extends FightBehaviour {
		public int JUMP_ITER;
		public int JUMP_DIST;
		public int WAIT_ITER;
		public int dir;
		public int jumping = JUMP_ITER;
		public int wait;

		public BunnyBehaviour(int jUMP_ITER, int jUMP_DIST, int wAIT_ITER,
				int dir, int wait) {
			JUMP_ITER = jUMP_ITER;
			JUMP_DIST = jUMP_DIST;
			WAIT_ITER = wAIT_ITER;
			this.dir = dir;
			this.wait = wait;
		}

		int getJumpIter(Bunny bunny) {
			if(bunny.isUnderEffectOfSpell("spell_battlespeed"))	
				return 30;
			else	return JUMP_ITER;
		}

		int getJumpDist(Bunny bunny)	{
			if(bunny.isUnderEffectOfSpell("spell_battlespeed"))	
				return 30;
			else	return JUMP_DIST;
		}

		int getWaitIter(Bunny bunny)	{
			if(bunny.isUnderEffectOfSpell("spell_battlespeed"))	
				return 0;
			else	return WAIT_ITER;
		}

		public void updateBehaviourPosition(Bunny bunny, long iteration) {
			//behaviourPosition = getBehaviourPosition();
			if(jumping > 0) {
				jumping--;
				behaviourPosition += dir * getJumpDist(bunny);
				if(behaviourPosition>100)	{
					dir = -1;
					behaviourPosition = 100;
				}
				if(behaviourPosition<-100)	{
					dir = 1;
					behaviourPosition = -100;
				}
			}
			else if(wait>0)	{
				wait--;
			}
			else if(jumping==0) {
				wait = getWaitIter(bunny);
				jumping = -1;
			}
			else if(wait==0) {
				jumping = getJumpIter(bunny);
				wait = -1;
			}
			
			setBehaviourPosition(behaviourPosition);
		}
	}

	//private BunnyBehaviour 

	public Bunny()	{
		nameID = "creature_bunny";
		
		healthPoints = 40;
		maxHealthPoints = 40;
		manaPoints = 40;
		maxManaPoints = 40;
		popularity = 7;
		aggresivness = 0.2;
		
		fightBehaviour  = new BunnyBehaviour(3, 11, 7, 1, 0);
		
		containsItem = "item_bunnyleg";
		
		this.knownFightSpells.add("spell_physical");
		this.knownFightSpells.add("spell_battlespeed");
	}
	
	@Override
	public Spell castSpell(String knownSpell)	{
		Spell toCast = null;
		if(knownSpell.equals("spell_physical"))	{
			PhysicalAttack pa = new PhysicalAttack(this);
			pa.hitpointsToTake = 8;
			pa.displayNameID = "spell_physical_bite";
			//pa.infects = true;
			//pa.infection = new Infection(this);
			toCast=pa;
		}
		else if(knownSpell.equals("spell_battlespeed"))	{
			toCast = new Speed(this);
		}
		return toCast;
	}
	
	@Override
	public void updateBehaviourPosition(long iteration) {
		((BunnyBehaviour)fightBehaviour).updateBehaviourPosition(this, iteration);
	}
}
