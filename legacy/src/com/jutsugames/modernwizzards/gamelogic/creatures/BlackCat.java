package com.jutsugames.modernwizzards.gamelogic.creatures;

import android.content.Context;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.*;
import com.jutsugames.modernwizzards.gamelogic.spells.*;

public class BlackCat extends Creature {
	
	public BlackCat()	{
		nameID = "creature_black_cat";
		
		healthPoints = 60;
		maxHealthPoints = 60;
		manaPoints = 70;
		maxManaPoints = 70;
		popularity = 6;
		aggresivness = 0.3;
		
		containsItem = "item_cateye";
		
		this.knownFightSpells.add("spell_physical");
		this.knownFightSpells.add("spell_physical2");
		this.knownFightSpells.add("spell_badluck");
		
		//this.knownSpells.add(new Thunderbolt());
		//this.knownSpells.add(new IcySpike());
	}

	@Override
	public Spell castSpell(String knownSpell)	{
		Spell toCast = null;
		if(knownSpell.equals("spell_physical"))	{
			PhysicalAttack pa = new PhysicalAttack(this);
			pa.hitpointsToTake = 10;
			pa.displayNameID = "spell_physical_bite";			
			toCast=pa;
		}
		if(knownSpell.equals("spell_physical2"))	{
			PhysicalAttack pa = new PhysicalAttack(this);
			pa.hitpointsToTake = 8;
			pa.displayNameID = "spell_physical_scratch";			
			toCast=pa;
		}
		else if(knownSpell.equals("spell_badluck"))	{
			toCast = new BadLuck(this);
		}
		return toCast;
	}
	
}
