package com.jutsugames.modernwizzards.gamelogic.creatures;

import android.content.Context;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.spells.BadLuck;
import com.jutsugames.modernwizzards.gamelogic.spells.Drain;
import com.jutsugames.modernwizzards.gamelogic.spells.PhysicalAttack;

public class Gnome extends Creature {
	
	public Gnome()	{
		nameID = "creature_gnome";
		
		manaPoints = 100;
		healthPoints = 100;
		maxManaPoints = 100;
		maxHealthPoints = 100;
		aggresivness = 0.6;
		
		popularity = 4;
		
		this.knownFightSpells.add("spell_physical");
	}
	
	@Override
	public Spell castSpell(String knownSpell)	{
		Spell toCast = null;
		if(knownSpell.equals("spell_physical"))	{
			PhysicalAttack pa = new PhysicalAttack(this);
			pa.hitpointsToTake = 15;
			pa.displayNameID = "spell_physical_spear";			
			toCast=pa;
		}
		return toCast;
	}
	
}
