package com.jutsugames.modernwizzards.gamelogic.creatures;

import android.content.Context;
import android.util.Log;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.spells.Infection;
import com.jutsugames.modernwizzards.gamelogic.spells.PhysicalAttack;
import com.jutsugames.modernwizzards.gamelogic.spells.Speed;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class Warewolf extends Creature {
	
	private int movesNormalFor = 0;
	private int movesFastFor = 0;
	private int ownIter = 0;
	
	public Warewolf()	{
		nameID = "creature_warewolf";
		
		manaPoints = 200;
		maxManaPoints = 200;
		healthPoints = 180;
		maxHealthPoints = 180;
		popularity = 3;
		aggresivness = 1.0;
		knownFightSpells.add("spell_physical");
		knownFightSpells.add("spell_physical2");
	}

	@Override
	public Spell castSpell(String knownSpell)	{
		Spell toCast = null;
		if(knownSpell.equals("spell_physical"))	{
			PhysicalAttack pa = new PhysicalAttack(this);
			pa.hitpointsToTake = 20;
			pa.displayNameID = "spell_physical_bite";					
			pa.infects = true;
			pa.infection = new Infection(this);
			toCast=pa;
		}
		if(knownSpell.equals("spell_physical2"))	{
			PhysicalAttack pa = new PhysicalAttack(this);
			pa.hitpointsToTake = 15;
			pa.displayNameID = "spell_physical_scratch";			
			toCast=pa;
		}
		return toCast;
	}

	@Override
	public void updateBehaviourPosition(long iteration) {
		ownIter++;
		if(movesFastFor>0)	{ 
			ownIter++;
			movesFastFor--;
		}
		else if(movesFastFor==0)	{
			movesFastFor--;
			movesNormalFor=(int) (Math.random()*100);
		}
		else if(movesNormalFor>0)	{ 
			movesNormalFor--;
		}
		else if(movesNormalFor==0)	{
			movesNormalFor--;
			movesFastFor=(int) (Math.random()*100);
		}
		
		fightBehaviour.setBehaviourPosition(Math.sin(((double)ownIter)/18) * 100);
		//behaviourPosition = (Math.sin(((double)iteration)/13)+Math.sin(((double)iteration)/7)*0.7)*50;
		//$log.d("CREATURE", "iter: "+iteration + " position " + behaviourPosition);
		
	}
}
