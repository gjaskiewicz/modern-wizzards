package com.jutsugames.modernwizzards.gamelogic.creatures;

import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.FightBehaviour;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.spells.PhysicalAttack;
import com.jutsugames.modernwizzards.gamelogic.spells.Speed;

public class Golem extends Creature {
	
	public Golem()	{
		nameID = "creature_golem";
		
		healthPoints = 250;
		maxHealthPoints = 250;
		manaPoints = 300;
		maxManaPoints = 300;
		popularity = 0;
		aggresivness = 1.0;
		
		containsItem = null;
		
		fightBehaviour = new GolemBehaviour();
		
		this.knownFightSpells.add("spell_physical");
	}
	
	@Override
	public Spell castSpell(String knownSpell)	{
		Spell toCast = null;
		if(knownSpell.equals("spell_physical"))	{
			PhysicalAttack pa = new PhysicalAttack(this);
			pa.hitpointsToTake = 25;
			toCast=pa;
		}
		return toCast;
	}
	
	class GolemBehaviour extends FightBehaviour {
		
		public void updateBehaviourPosition(long iteration) {
			behaviourPosition = Math.sin(((double)iteration)/27)*50;
		}
	}
}
