package com.jutsugames.modernwizzards.gamelogic.creatures;

import android.content.Context;
import android.util.Log;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.spells.*;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class Bat extends Creature {
	
	public Bat()	{
		nameID = "creature_bat";
		
		manaPoints = 50;
		maxManaPoints = 50;
		healthPoints = 30;
		maxHealthPoints = 30;
		popularity = 7;
		aggresivness = 0.5;
		
		knownFightSpells.add("spell_physical");
	}

	@Override
	public Spell castSpell(String knownSpell)	{
		Spell toCast = null;
		if(knownSpell.equals("spell_physical"))	{
			PhysicalAttack pa = new PhysicalAttack(this);
			pa.displayNameID = "spell_physical_bite";				
			pa.hitpointsToTake = 7;
			pa.infects = true;
			pa.infection = new Drain(this);
			toCast=pa;
		}
		return toCast;
	}
	
	public void updateBehaviourPosition(long iteration) {
		
		fightBehaviour.setBehaviourPosition((Math.sin(((double)iteration)/13)+Math.sin(((double)iteration)/7)*0.7)*50);
		//$log.d("CREATURE", "iter: "+iteration + " position " + behaviourPosition);
		
	}
}
