package com.jutsugames.modernwizzards.gamelogic.creatures;

import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.FightBehaviour;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.spells.PhysicalAttack;
import com.jutsugames.modernwizzards.gamelogic.spells.Speed;

public class Strumpf extends Creature {
	
	public Strumpf()	{
		nameID = "creature_strumpf";
		
		healthPoints = 30;
		maxHealthPoints = 30;
		manaPoints = 40;
		maxManaPoints = 40;
		popularity = 5;
		aggresivness = 0.1;
		
		containsItem = "item_smurfblood";
		
		fightBehaviour = new StrumpfBehaviour();
		
		this.knownFightSpells.add("spell_physical");
	}
	
	@Override
	public Spell castSpell(String knownSpell)	{
		Spell toCast = null;
		if(knownSpell.equals("spell_physical"))	{
			PhysicalAttack pa = new PhysicalAttack(this);
			pa.hitpointsToTake = 8;
			//pa.infects = true;
			//pa.infection = new Infection(this);
			toCast=pa;
		}
		return toCast;
	}
	
	class StrumpfBehaviour extends FightBehaviour {
		private int invisibleFor=1;
		private int movesFor=0;
		private int movesFrom=0;
		private int movesTo=0;
		private int moveDelta=4;
		
		public void updateBehaviourPosition(long iteration) {
			if(invisibleFor>0)	{
				invisibleFor--;
				behaviourOpacity=255-invisibleFor*77;
				if(behaviourOpacity<0) behaviourOpacity=0;
			}
			else if(invisibleFor==0)	{
				invisibleFor--;
			}
			else if(movesFor>0){
				int sign = -1;
				if(movesTo<movesFrom) sign=1;
				behaviourPosition = movesTo+(movesFor*moveDelta*sign); 
				movesFor--;
				behaviourOpacity=movesFor*77;
				if(behaviourOpacity>255) behaviourOpacity=255;
			}
			else if(movesFor==0){
				movesFor--;
				invisibleFor = (int) (Math.random()*50+25);
				do {
					movesFrom = (int) (Math.random()*200-100);
					movesTo = (int) (Math.random()*200-100);
					movesFor=Math.abs((movesFrom-movesTo)/moveDelta);
				}
				while(movesFor<25);
				behaviourPosition=movesFrom;
			}
		}
	}
}
