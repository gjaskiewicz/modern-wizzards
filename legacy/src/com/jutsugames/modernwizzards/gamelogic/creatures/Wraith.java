package com.jutsugames.modernwizzards.gamelogic.creatures;

import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.FightBehaviour;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.spells.PhysicalAttack;
import com.jutsugames.modernwizzards.gamelogic.spells.Speed;

public class Wraith extends Creature {
	
	public Wraith()	{
		nameID = "creature_wraith";
		
		healthPoints = 130;
		maxHealthPoints = 130;
		manaPoints = 200;
		maxManaPoints = 200;
		popularity = 4;
		aggresivness = 0.9;
		
		containsItem = "item_ectoplasma";
		
		fightBehaviour = new WraithBehaviour();
		
		this.knownFightSpells.add("spell_physical");
	}
	
	@Override
	public Spell castSpell(String knownSpell)	{
		Spell toCast = null;
		if(knownSpell.equals("spell_physical"))	{
			PhysicalAttack pa = new PhysicalAttack(this);
			pa.hitpointsToTake = 10;
			//pa.infects = true;
			//pa.infection = new Infection(this);
			toCast=pa;
		}
		return toCast;
	}
	
	class WraithBehaviour extends FightBehaviour {
		private int invisibleFor=0;
		private int movesFor=100;
		
		public void updateBehaviourPosition(long iteration) {
			behaviourPosition = (Math.sin(((double)iteration)/17)+Math.sin(((double)iteration)/10)*0.2)*80;
			if(invisibleFor>0)	{
				invisibleFor--;
				behaviourOpacity=255-invisibleFor*25;
				if(behaviourOpacity<0) behaviourOpacity=0;
			}
			else if(invisibleFor==0)	{
				invisibleFor--;
				movesFor=(int) (Math.random()*100+25);
			}
			else if(movesFor>0){
				behaviourOpacity=movesFor*25;
				if(behaviourOpacity>255) behaviourOpacity=255;
				movesFor--;
			}
			else if(movesFor==0){
				movesFor--;
				invisibleFor = (int) (Math.random()*100+25);
			}
		}
	}
}
