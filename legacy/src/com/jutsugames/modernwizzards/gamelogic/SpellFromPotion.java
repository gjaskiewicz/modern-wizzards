package com.jutsugames.modernwizzards.gamelogic;

import java.util.LinkedList;

import com.jutsugames.modernwizzards.Game;

public class SpellFromPotion extends Spell {
	//Castable containingSpell;
	protected int mixtureColor;
	private LinkedList<String> ingredients = new LinkedList<String>();
	
	public SpellFromPotion(Creature owner) {
		super(owner);
		usageType = UsageType.ANYTIME;
		targetType = EffectTargetType.SELF;
		requiredMana = 0;
	}
	
	public SpellFromPotion()	{
		this(Player.getInst());
	}
	
	public int getColor() {
		return mixtureColor;
	}
	
	public void useThis()	{
		//rzuc czar, odejmij z inventory 
	}

	public boolean canMakeFromIng(String ing1, String ing2, String ing3) {
		if(ingredients.contains(ing1) && 
				ingredients.contains(ing2) &&
				ingredients.contains(ing3)) 
			return true;
		return false;
	}
}
