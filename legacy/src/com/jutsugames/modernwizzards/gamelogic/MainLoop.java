package com.jutsugames.modernwizzards.gamelogic;

import java.util.LinkedList;

import alice.util.jedit.InputHandler.next_char;
import android.os.CountDownTimer;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.jutsugames.modernwizzards.*;
import com.google.android.maps.MapView;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Game.GameState;
import com.jutsugames.modernwizzards.ModernWizzardsActivity;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.gamelogic.Castable.UsageType;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class MainLoop {
	private static final String TAG = "MainLoop";
	// public Game game;
	public static int loopIteration = 0;

	public transient static LinkedList<PhysicalObject> animations = new LinkedList<PhysicalObject>();
	
	public static int ITERATIONS_PER_SECOND = 25;

	public static void startGameLoop() {
    	$log.d(TAG, "Starting game loop"); 
		Game.getGame().gameLoopRunner = new CountDownTimer(25 * 3600 * 24 * 30,
				1000 / ITERATIONS_PER_SECOND) {
			@Override
			public void onTick(long millisUntilFinished) {
				try {
					iterateGameState();
				}
				catch (Exception e)	{
					$log.e(TAG, "Error in Game Loop:" + e); 
					e.printStackTrace();
				}
			}

			@Override
			public void onFinish() {
				$log.d("MAIN LOOP CDT", "CDT STOPS!");
				startGameLoop();
			}
		};
		Game.getGame().gameLoopRunner.start();
	}

	/**
	 * Iteruje stan gry. Wywoływane ITERATIONS_PER_SECONDx na sekunde.
	 */
	public static void iterateGameState() {
		loopIteration++;
    	//$log.d(TAG, "Iterating game loop"); 
		
		
		if(Game.getGame().locationIsNotSet && loopIteration % (ITERATIONS_PER_SECOND * 10) == 0) 	{
			MWToast.showToast(R.string.game_gps_loc_wait, Toast.LENGTH_LONG);
		}
		
		if (Game.getGame().getGameState() == GameState.STATE_WALKING
				|| Game.getGame().getGameState() == GameState.STATE_PLAYERDEAD) {
			
			//animacje
			for(PhysicalObject po : animations)	{
				po.display(false);
				po.display(true);
			}
			if(animations.size()>0)	MWMap2Activity.refreshMapView();
			
			//standardowe akcje, interakcje itp
			if (loopIteration % ITERATIONS_PER_SECOND == 0) {
				Game.getGame().checkInteractions();
				Game.getGame().refreshMap();
				//Game.getGame().playerEgo.repaint();
				MWMap2Activity.refreshMapView();
				if (Game.getGlobalTopmostActivity() instanceof StatsActivity) {
					((StatsActivity) Game.getGlobalTopmostActivity()).refresh();
				}
			}

			
			
		}

		if (Game.getGame().getGameState() == GameState.STATE_WALKING
				|| Game.getGame().getGameState() == GameState.STATE_FIGHTING) {

			for (Spell spl : Game.getGame().playerEgo.underEffectOfSpell) {
				spl.takeEffect();
			}
			
			addManaLogarytmic();

			if (loopIteration % (ITERATIONS_PER_SECOND * 30) == 0)
				// $log.d("MAIN LOOP", "25*30th loop");
				Game.getGame().playerEgo.addHealthPoints(1, false);

			Game.getGame().playerEgo.removeFinishedSpells();
		}

		if (Game.getGame().getGameState() == GameState.STATE_FIGHTING) {
			Game.getGame().fight.iterateFight();
		}

		if (Game.getGame().getGameState() == GameState.STATE_PLAYERDEAD) {
			// tylko checkInteraction w poszukiwaniu Mana Zone.
		}
		//$log.d(TAG, "Iterating game loop END"); 
	}
	
	/**
	 * dodaje graczowi mana w logarytmiczny sposob, co iteracje. 
	 * Czym wiecej mana, tym wolniej. 
	 */
	private static void addManaLogarytmic()	{
		if(Player.getInst().isUnderEffectOfSpell("spell_oob")) return;
		double div = Game.getGame().playerEgo.getMana()*Game.getGame().playerEgo.getMana();
		if (div<=1) div=1;
		double pointsToAdd = Game.getGame().playerEgo.manaRegenFactor/div;
		Player.getInst().addManaPoints(pointsToAdd, false);
		//if (loopIteration % (div) == 0) {
			// $log.d("MAIN LOOP", "25th loop");
			//Game.getGame().playerEgo.addManaPoints(1, false);
		//}
	}
}
