package com.jutsugames.modernwizzards.gamelogic;

import android.graphics.Color;
import android.location.Location;
import android.view.Display;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWMap2Activity;
import com.jutsugames.modernwizzards.controller.MapController;
import com.jutsugames.modernwizzards.gis.SphericalZone;
import com.jutsugames.modernwizzards.gis.Units;
import com.jutsugames.modernwizzards.objects.LocationSlab;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;
import com.jutsugames.modernwizzards.util.EventEmiter;
import com.jutsugames.modernwizzards.util.MoveLocation;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;


/**
 * Klasa bazowa dla wszystkiego co mo�e znalezc sie na mapie. 
 * @author bartek
 */
public abstract class PhysicalObject extends EventEmiter {
	public String nameID;
	private static final String TAG = "PhysicalObject";
	
	/** czy wygenerowany czy questowy*/
	public boolean isGenerated;
	public LocationSlab homeLocation;
	public int generationHash;
	
	private transient Location location;
	private GeoPoint locationGP;

	//obszar wystepowania
	public transient SphericalZone zone;
	private transient boolean displayed;
	public transient Circle dispCircle ;
	protected transient OverlayItem vis;
	
	//TODO: usu� mnie, bylem potrzebny bo Questy dzialaj� na innym w�tku
	private transient PhysicalObject toDisplay;
	
	public PhysicalObject()	{
		super();
		displayed = false;
	}

	public boolean shouldInteract(GeoPoint gp) {
		return false;
	}


	public void interact() {
	}
	
	/**
	 * Pokazuje lub ukrywa wizualizajce obiektu ma mapie googla
	 * @param display czy ma byc widoczny czy nie.
	 */
	public void display(boolean display)	{
		if(display && !displayed)	{
			//createVis(MWMapActivity.getGameVisual());
			displayed = true;
			toDisplay = this;
			//$log.d(TAG, "Display circle...");
			Game.getGame().mainActivity.runOnUiThread(
					new Runnable() {
						@Override
						public void run() {
							MapController.getInst().displayObj(toDisplay);
						}
					}
			);
		}
		if(!display && displayed)	{
			//MainLoop.animations.remove(this);
			//deleteVis(MWMapActivity.getGameVisual());
			displayed = false;
			MapController.getInst().dontDisplayObj(this);
			if(dispCircle!=null)	{
				//$log.d(TAG, "Destroy circle...");
				Game.getGame().mainActivity.runOnUiThread(
						new Runnable() {
							@Override
							public void run() {
								dispCircle.remove();
							}
						}
				);
			}
		}
	}
	
	public Location getLocation()	{
		if(location==null)	{
			setLocation(locationGP);
		}
		return location;
	}
	
	public GeoPoint getGeoPoint() {
		return locationGP;
	}
	
	public void setLocation(GeoPoint gp) {
		locationGP = gp;
		location = new Location("Object location");
		location.setLatitude(gp.getLatitudeE6()/1E6);
		location.setLongitude(gp.getLongitudeE6()/1E6);
		
		if(zone!=null) zone.setCenter(locationGP);
		
		if(dispCircle!=null)
			dispCircle.setCenter(MWMap2Activity.convLocationToLatLng(getLocation()));
	}
	
	public void setLocation(Location lc) {
		location = lc;
		locationGP = new GeoPoint((int)(location.getLatitude()*1E6), (int)(location.getLongitude()*1E6));
		if(zone!=null) zone.setCenter(locationGP);
	}
	
	public void moveObject(double distance, int bearing) {
		MoveLocation.moveLocation(location, distance, bearing);
		locationGP = MoveLocation.moveGeoPoint(locationGP, distance, bearing);
		zone.moveCenter(distance, bearing);
	}

	public ObjectInfo getInfo() {
		return ObjectInfoFactory.get().getInfoStandarized(nameID);
	}
	
	public void moveTowards(GeoPoint destination, double metersPerIter)	{
		GeoPoint currLoc = this.getGeoPoint();
		int latVectL = (int)(1.5/Units.latitudeToMeters());
		int lngVectL = (int)(1.5/Units.longitudeToMeters(Game.getGame().playerEgo.getGeoPoint().getLatitudeE6()));
		int latDist = destination.getLatitudeE6() - currLoc.getLatitudeE6();
		int lngDist = destination.getLongitudeE6() - currLoc.getLongitudeE6();
		double metDist = Units.optimizedDistance(destination, currLoc);
		if(metDist==0) metDist=1;
		int latVect = (int)(latVectL * ((int)(latDist*Units.latitudeToMeters()) / metDist));
		int lngVect = (int)(lngVectL * ((int)(lngDist*Units.longitudeToMeters(destination.getLatitudeE6())) / metDist));
		setLocation(new GeoPoint(currLoc.getLatitudeE6()+latVect, currLoc.getLongitudeE6()+lngVect));
	}
	
	public int getColor()	{
		return Color.TRANSPARENT;
	}
	
	public LatLng getLatLng()	{
		return MWMap2Activity.convLocationToLatLng(getLocation());
	}
	
}
