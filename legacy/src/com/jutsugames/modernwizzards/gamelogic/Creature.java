package com.jutsugames.modernwizzards.gamelogic;

import java.util.Calendar;
import java.util.LinkedList;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.AlertActivity;
import com.jutsugames.modernwizzards.AlertCreatureActivity;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.Game.GameState;
import com.jutsugames.modernwizzards.gis.SphericalZone;
import com.jutsugames.modernwizzards.util.VibratorUtil;
import com.jutsugames.modernwizzards.util.ChangeListener.EventTypes;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

abstract public class Creature extends PhysicalObject {
	private static final String TAG = "Creature";	
	public static enum CreatureState {
		ALIVE,
		DEAD
	}
	
	/**zajmuje sie polozeniem i zachowaniem stworka w walce*/
	public FightBehaviour fightBehaviour;

	protected int healthPoints = 0;
	public int maxHealthPoints =0;
	public double manaPoints = 0;
	public int maxManaPoints =0;
	
	/**jesli 1 - zawsze widoczny, jesli 0 - nigdy nie pojawia sie na mapie.*/
	public int visibility =1;
	/**jesli 1 - zawsze atakuje, jesli 0 - nigdy nie atakuje*/
	public double aggresivness =1;
	
	/** skala popularno�ci w odniesieniu do innych stworkow */
	public int popularity;
	/** przesuniecie popularnosci (potrzebne do generacji wg popularno�ci)*/
	public int popularityOffset;
	/** suma popularnosci wszystkich stworkow, do wywa�onej generacji */
	public static int creaturesPopularitySum=0;
	
	public LinkedList<String> knownFightSpells = new LinkedList<String>();
	public LinkedList<InventoryItem> inventory = new LinkedList<InventoryItem>();
	public LinkedList<Spell> underEffectOfSpell = new LinkedList<Spell>();
	public Inventory inv = new Inventory();

	public static boolean runningDialog;
	public static Creature runningDialogCreature;
	
	public int secondsNotToAttack = 0;
	
	public boolean attackWithoutWarning = false;
	
	/** id itemu ktore zaiwera, lub null */
	public String containsItem;
	/** standardowy parametr szcze�cia */
	private double baseLuckFactor = 1;
	//obrazek
	//opis
	//lista czar�w
	//co pozostawia po sobie, je�li pozostawia (przedmiot Item)
	
	public Creature()	{
		super();
		zone = new SphericalZone(null, 50);
		fightBehaviour = new FightBehaviour();
		healthPoints = maxHealthPoints;
		manaPoints = maxManaPoints;
	}
	
	public boolean isUnderEffectOfSpell(String spellName)	{
		for( Spell spl : this.underEffectOfSpell)	{
			if(spl.nameID.compareTo(spellName) == 0)
				return true;
		}
		return false;
	}
	
	public void interact() {			
		if (Game.getGame().getGameState()!=GameState.STATE_WALKING)	{
			$log.d("CREATURE", "Creature: no interaction, player is not 'walking'");
			return;
		}
		if ( secondsNotToAttack > 0)	{
			return;
		}
		
		if ( Player.getInst().isUnderEffectOfSpell("potion_invisibility"))
			return; //a przynajmniej nie atakuj
		
		if (runningDialog) {
			$log.d("CREATURE", "Creature: sorry, runningDialog");
			return;
		}
		runningDialog = true;

		$log.d("CREATURE", "Creature Interacts: " + nameID );
		
		long[] vPatern = {0, 300,100,300};
		VibratorUtil.vibrate(vPatern, -1);
		
		runningDialogCreature=this;
		Game.getGlobalTopmostActivity().startActivity(
				new Intent(Game.getGlobalTopmostActivity(), AlertCreatureActivity.class)
				.putExtra(AlertActivity.ALERT_TYPE, AlertActivity.ALERT_TYPE_CREATURE)
				);
		Game.getGlobalTopmostActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
	}
	
	public boolean shouldInteract(GeoPoint gp)	{
		if ( secondsNotToAttack > 0)	{
			secondsNotToAttack--;
		}
		return zone.intersects(gp);
	}

	public String getDisplayName() {
		return ObjectInfoFactory.get().getInfoStandarized(nameID).getName();
	}

	public ObjectInfo getInfo(Context ctx) {
		return ObjectInfoFactory.get().getInfoStandarized(nameID);
	}
	
	/**
	 * zabiera wskazane mana
	 * @param howMuch ile zabrac
	 * @return true jesli mozna
	 * false jesli sie nie da (jest mniej
	 */
	public boolean takeMana(int howMuch) {
			if(manaPoints-howMuch<0)	{
				return false;
			}
			else	{
				manaPoints -=howMuch;
				return true;
			}	
	}
	
	public void removeFinishedSpells()	{
		LinkedList<Spell> toRemove = new LinkedList<Spell>();
		for(Spell spell : underEffectOfSpell)	
			if(spell.turnDuration<=0)	{
				toRemove.add(spell);
			}
		underEffectOfSpell.removeAll(toRemove);
	}
	
	public void takeHealth(double hitpointsToTake) {
		if(healthPoints-hitpointsToTake<=0)	{
			healthPoints = 0;
			//die?
		}
		else	{
			healthPoints -=hitpointsToTake;
			if(this == Game.getGame().playerEgo)	{
				long[] vPatern = {0, (long) (hitpointsToTake*20.0)};
				VibratorUtil.vibrate(vPatern, -1);
			}
		}
		if(Game.getGame().fight!=null && Game.getGame().fight.creature == this)	{
			Game.getGame().fight.controller.animateCreatureHit();
		}
	}
	
	public int getHP() {
		return healthPoints;
	}
	
	public double getMana() {
		return manaPoints;
	}
	
	public boolean isManaFull() {
		return manaPoints == maxManaPoints;
	}
	
	public void addManaPoints(double howMuch, boolean force)	{
		if(force) manaPoints+=howMuch;
		else if(manaPoints==maxHealthPoints) return;
		else if(manaPoints + howMuch < maxManaPoints)	{
			manaPoints += howMuch;
		}
		else {
			manaPoints = maxManaPoints;
			notifyListeners(nameID, EventTypes.MANA_FULL, null);
			MWToast.makeToast(Game.getGlobalTopmostActivity(), R.string.player_mana_full, Toast.LENGTH_LONG).show();
		}
	}
	
	public void addHealthPoints(int howMuch, boolean force) {
		if(force) healthPoints +=howMuch;
		else if(healthPoints + howMuch < maxHealthPoints)	{
			healthPoints += howMuch;
		}
		else healthPoints = maxHealthPoints;
	}

	/**
	 * smierc stworka. smierc gracza przeciazona w Player. 
	 */
	public void die() {
		if(isGenerated)	{
			homeLocation.creatures.remove(this);
			if(Game.getGame().worldContainer.saveDate != Calendar.getInstance().getTime().getDate())	{
				Game.getGame().worldContainer.creaturesDefeated.clear();
				Game.getGame().worldContainer.gatheredPlants.clear();
				Game.getGame().worldContainer.saveDate = Calendar.getInstance().getTime().getDate();
			}
			Game.getGame().worldContainer.creaturesDefeated.add(this.generationHash);
			
			Game.saveGame();
		}
		else	{
			//TODO: tutaj tez usuwac? na wszelki wypadek
			Game.getGame().worldContainer.removeQuestObject(this);
		}
		notifyListeners(nameID, EventTypes.KILLED, null);
		Game.getGame().getAchivContainer().getAchivHunter().addCreatureDefeated(nameID);
		//deleteVis(MWMapActivity.getGameVisual());
		display(false);
		Game.getGame().invalidateMapView();
	}

	public void updateBehaviourPosition(long iteration) {
		fightBehaviour.updateBehaviourPosition(iteration);
	}

	public void thinkFight(Fight fight) {
		// creature thinks... mo�e ucieka, moze wykorzystuje mikstur�, moze sprawdza efekty, strategie?
		//rzuca 1 na 10 sekund jesli moze. 
		if(fight.getFightIteration() % (25*3) == 0)	{
			int whichSpell = (int)(Math.random()*knownFightSpells.size());
			//whichSpell = 0;
			fight.creatureCastsSpell(
					this.castSpell(knownFightSpells.get(whichSpell))
					);
					//SpellFactory.getSpellByID(knownSpells.get(whichSpell), this));
		}
	}
	
	public LinkedList<String> getKnownBattleSpells()	{
		/*LinkedList<String> result = new LinkedList<String>();
		for(String spell : knownFightSpells )	{
			Spell spl = SpellFactory.getSpellByID(spell, this);
			if(spl.getUsageType()==UsageType.BATTLE || spl.getUsageType()==UsageType.ANYTIME)
				result.add(spell);
		}*/
		return knownFightSpells;
	}
	

	
	public int getHealthPoints() {
		return healthPoints;
	}
	
	public double getBehaviourPosition() {
		return fightBehaviour.getBehaviourPosition();
	}

	/**
	 * Fabryka czar�w - powinno dzia�a� z knownSpells
	 * @param spellID
	 * @return Spell gotowy do rzucenia.
	 */
	public Spell castSpell(String spellID)	{
		return SpellFactory.getSpellByID(spellID, this);
	}

	/**
	 * Uruchamiane po walce uruchamia komunikat z itemem do pobrania (jesli creature go zawiera)
	 */
	public void giveContainingItem() {
		if(containsItem!=null)	{
			new CollectableItem(null, new CollectPlant(containsItem)).interact();
		}
	}

	public int getColor()	{
		return 0xA8FF8800 - 
				0x00000100 * (int)(aggresivness*136);
	}
	
	public int getExperienceForDefeat()	{
		int pop = this.popularity;
		if(pop == 0) pop=2;
		return 1500 / pop;
	}
	
	/** zwraca "rzut ko�cia" w przedziale <0-1>, uwzgl�dniaj�c szcz�cie
	 * zawsze czym wi�cej tym bardziej "na korzysc!" rzucaj�cego!
	 * @return
	 */
	public double getDiceRoll()	{
		double roll = Math.random();
		double positive = baseLuckFactor;
		double negative = 1;
		if(isUnderEffectOfSpell("spell_badluck"))	{
			negative+=1;
		}
		if(isUnderEffectOfSpell("spell_luckjelly"))	{
			positive+=1;
		}
		for(InventoryItem ii : inventory)	{
			if(ii instanceof CollectPlant) {
				if(((CollectPlant)ii).getNameID().equals("item_4clover") ||
						((CollectPlant)ii).getNameID().equals("item_bunnyleg"))	{
					positive+=0.2;
					break;
				}
			}
		}
		Math.pow(roll, negative/positive);
		return roll;
	}
	
}
