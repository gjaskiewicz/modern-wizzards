package com.jutsugames.modernwizzards.gamelogic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;

import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.util.ChangeListener;

public interface Castable {
	public static enum UsageType {
		MAP,
		BATTLE,
		ANYTIME
	}
	
	public static enum EffectTargetType {
		SELF,
		ANOTHER,
		AMBIENT
	}
	
	//TODO: duration in turns;
	
	int getRequiredMana();
	/*
	 * zabiera mane na za�, dodaje do listy czar�w 
	 */
	boolean castThis();
	void takeEffect();
	
	ObjectInfo getInfo();
	UsageType getUsageType();
	EffectTargetType getEffectTargetType();

}
