package com.jutsugames.modernwizzards.gamelogic;

public class FightBehaviour	{
	protected double behaviourPosition;
	public int behaviourOpacity = 255;
	
	public double getBehaviourPosition() {
		return behaviourPosition;
	}

	public void setBehaviourPosition(double behaviourPosition) {
		this.behaviourPosition = behaviourPosition;
	}
	
	public void updateBehaviourPosition(long iteration) {
		setBehaviourPosition(Math.sin(((double)iteration)/18)*100);
	}
	
	/**
	 * Widocznosc stworka w widoku walki
	 * @return wodocznosc od 0 do 255
	 */
	public int getBehaviourOpacity() {
		return behaviourOpacity;
	}
}
