package com.jutsugames.modernwizzards.gamelogic.mixtures;

import java.util.LinkedList;

import android.content.Context;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.gamelogic.MainLoop;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.SpellFromPotion;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;

public class LuckPotion extends SpellFromPotion {

	public LuckPotion()	{
		super();
		nameID="potion_luckjelly";
		mixtureColor = 0x40FFFFFF;
		this.turnDuration = MainLoop.ITERATIONS_PER_SECOND * 60 * 15; //trwa 15 minuty
	}
	
	@Override
	public boolean castThis() {
		// zapewnienie ze gracz bedzie tylko pod jedn� mikstur�
		if (owner.isUnderEffectOfSpell(nameID))
			return false;
		return super.castThis();
	}

	@Override
	public void takeEffect() {
		super.takeEffect();
	}

}
