package com.jutsugames.modernwizzards.gamelogic.mixtures;

import android.content.Context;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.SpellFromPotion;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;

public class ManaPotion extends SpellFromPotion {
	
	public ManaPotion()	{
		super();
		nameID = "potion_mana";
		mixtureColor = 0xFF0000FF;
	}
	
	@Override
	public boolean castThis() {
		Player player = Game.getGame().playerEgo;
		player.addManaPoints(100, false);
		return true;
	}

	@Override
	public void takeEffect() {
	}
	
}
