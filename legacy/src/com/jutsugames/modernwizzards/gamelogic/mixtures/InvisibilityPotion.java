package com.jutsugames.modernwizzards.gamelogic.mixtures;

import java.util.LinkedList;

import android.content.Context;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.SpellFromPotion;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;

public class InvisibilityPotion extends SpellFromPotion {

	public InvisibilityPotion()	{
		super();
		nameID="potion_invisibility";
		mixtureColor = 0x40FFFFFF;
		this.turnDuration = 30 * 60 * 3; //trwa 3 minuty
	}
	
	@Override
	public boolean castThis() {
		return super.castThis();		
	}

	@Override
	public void takeEffect() {
		super.takeEffect();
	}

}
