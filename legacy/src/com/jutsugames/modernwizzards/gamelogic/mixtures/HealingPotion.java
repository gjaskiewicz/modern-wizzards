package com.jutsugames.modernwizzards.gamelogic.mixtures;

import android.content.Context;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.SpellFromPotion;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;

public class HealingPotion extends SpellFromPotion {
	
	public HealingPotion()	{
		super();
		nameID = "potion_health";
		mixtureColor = 0xFFFF0000;
	}
	
	@Override
	public boolean castThis() {
		Player player = Game.getGame().playerEgo;
		player.addHealthPoints(50, false);
		return true;
	}

	@Override
	public void takeEffect() {
	}
	
}
