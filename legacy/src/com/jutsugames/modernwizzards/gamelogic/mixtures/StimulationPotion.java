package com.jutsugames.modernwizzards.gamelogic.mixtures;

import java.util.LinkedList;

import android.content.Context;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.gamelogic.MainLoop;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.SpellFromPotion;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;

public class StimulationPotion extends SpellFromPotion {

	public StimulationPotion()	{
		super();
		nameID="potion_stimulation";
		mixtureColor = 0xFFFF00FF;
		this.turnDuration = 30 * 60; //dziala minute
	}
	
	@Override
	public boolean castThis() {
		super.castThis();
		Game.getGame().playerEgo.addHealthPoints(50, true);
		Game.getGame().playerEgo.addManaPoints(50, true);
		return true;
	}

	@Override
	public void takeEffect() {
		super.takeEffect();
		if(turnDuration % MainLoop.ITERATIONS_PER_SECOND == 0)	{
			if(Game.getGame().playerEgo.getHealthPoints()>
				Game.getGame().playerEgo.maxHealthPoints)	{
					Game.getGame().playerEgo.takeHealth(1);
			}
			if(Game.getGame().playerEgo.getMana()>
				Game.getGame().playerEgo.maxManaPoints)	{
					Game.getGame().playerEgo.takeMana(1);
			}
		}
	}

}
