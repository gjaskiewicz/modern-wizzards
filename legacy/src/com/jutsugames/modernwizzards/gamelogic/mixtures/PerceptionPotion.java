package com.jutsugames.modernwizzards.gamelogic.mixtures;

import java.util.LinkedList;

import android.content.Context;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.SpellFromPotion;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;

public class PerceptionPotion extends SpellFromPotion {

	public PerceptionPotion()	{
		super();
		nameID="potion_perception";
		mixtureColor = 0xFF00FF00;
		turnDuration = 30 * 60 * 5; //trwa 5 minut.
	}
	
	@Override
	public boolean castThis() {
		return super.castThis();
	}

	@Override
	public void takeEffect() {
		super.takeEffect();
	}

}
