package com.jutsugames.modernwizzards.gamelogic.mixtures;

import com.jutsugames.modernwizzards.gamelogic.SpellFromPotion;
import com.jutsugames.modernwizzards.util.ChangeListener.EventTypes;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class InitialPotion extends SpellFromPotion {

	public InitialPotion()	{
		super();
		nameID="potion_initial";
	}
	
	@Override
	public int getRequiredMana() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean castThis() {
		/*LayoutInflater inflater2 = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater2.inflate(R.layout.custom_dialog3,
                (ViewGroup) findViewById(R.id.custom_linear_layout3));	
		*/
		$log.d("InitialPotion", "using of potion");
		notifyListeners(getInfo().getID(), EventTypes.USED, null);
		return true;
	}

	@Override
	public void takeEffect() {
		// TODO Auto-generated method stub
	}

}
