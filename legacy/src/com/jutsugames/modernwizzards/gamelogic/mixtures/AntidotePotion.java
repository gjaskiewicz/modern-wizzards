package com.jutsugames.modernwizzards.gamelogic.mixtures;

import java.util.LinkedList;

import android.content.Context;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.SpellFromPotion;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;

public class AntidotePotion extends SpellFromPotion {

	public AntidotePotion()	{
		super();
		nameID="potion_antidote";
		mixtureColor = 0xFF00FF00;
	}
	
	@Override
	public boolean castThis() {
		LinkedList<Spell> toRemove = new LinkedList<Spell>();
		for(Spell spl : Game.getGame().playerEgo.underEffectOfSpell){
			if(spl.nameID.equals("spell_infection"))
				toRemove.add(spl);
		}
		Game.getGame().playerEgo.underEffectOfSpell.removeAll(toRemove);
		return true;
	}

	@Override
	public void takeEffect() {
	}

}
