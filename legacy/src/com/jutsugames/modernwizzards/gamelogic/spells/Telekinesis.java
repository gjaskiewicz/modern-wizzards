package com.jutsugames.modernwizzards.gamelogic.spells;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWMap2Activity;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.MainLoop;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gis.Units;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;

public class Telekinesis extends Spell {
	
	private static final String TAG = "Telekinesis";
	
	private CollectableItem pulledObject;
	transient OnMapClickListener clickListener;
	
	public Telekinesis(Creature owner)	{
		super(owner);
		nameID = "spell_telekinesis";
		requiredMana = 20;
		usageType = UsageType.MAP;
		targetType = EffectTargetType.SELF;
		turnDuration = 1000;
	}

	@Override
	public void onRestoreFromSave() {
		super.onRestoreFromSave();
		turnDuration=0;
		for( Spell spl : Player.getInst().underEffectOfSpell)	{
			if(spl.nameID==this.nameID)
				Player.getInst().underEffectOfSpell.remove(spl);
		}
	}
	
	@Override
	public boolean castThis() {
		MWToast.showToast(R.string.spell_telekinesis_using, Toast.LENGTH_SHORT);
		
		clickListener = new TapListener(this);
		MWMap2Activity.addTapListener(clickListener);
		Game.getGlobalTopmostActivity().startActivity(new Intent(Game.getGlobalTopmostActivity(), MWMap2Activity.class));
		return true;
	}

	@Override
	public void takeEffect() {
		turnDuration--;
		pulledObject.moveTowards(Game.getGame().playerEgo.getPlayerLocation(), 5);
		if(pulledObject.zone.radius>15 && turnDuration%5==0) 
			pulledObject.zone.radius--;
		if(pulledObject.shouldInteract(Game.getGame().playerEgo.getPlayerLocation()) && 
				turnDuration>30) {
			//to zakoncz
			turnDuration = 30;
		}
		if(turnDuration==0)	{
			//MainLoop.animations.remove(pulledObject);
			pulledObject.display(false);
		}
	}

	public void pullObject(CollectableItem obj) {
		pulledObject = obj;
		//MainLoop.animations.add(pulledObject);
		super.castThis();
		Game.getGame().getAchivContainer().getAchivJuggle().checkJuggling();
		MWMap2Activity.removeTapListener();
	}
	

	
	private class TapListener implements OnMapClickListener	{
		
		Telekinesis spellToExecute;
		
		TapListener(Telekinesis caller)	{
			spellToExecute = caller;
		}

		@Override
		public void onMapClick(LatLng llpoint) {
			GeoPoint point = MWMap2Activity.convLatLngToGeopoint(llpoint);
			$log.d(TAG, "onTap from Telekinesis");
			if(!Units.optimizedDistanceCheck(point, Game.getGame().playerEgo.getGeoPoint(), 250))	{
				MWToast.showToast(R.string.spell_telekinesis_object_too_far, Toast.LENGTH_SHORT);
				return;
			}
				
			for(PhysicalObject obj : Game.getGame().worldContainer.getAllObjects()) {
				if( obj instanceof CollectableItem ) {
					if (obj.shouldInteract(point)) {
						//obj.interact();
						MWToast.showToast(R.string.spell_telekinesis_pulling, Toast.LENGTH_SHORT);
						
						spellToExecute.pullObject((CollectableItem)obj);
					}
				}
			}
			return;
		}
		 
	}


	
}
