package com.jutsugames.modernwizzards.gamelogic.spells;

import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.Castable.EffectTargetType;
import com.jutsugames.modernwizzards.gamelogic.Castable.UsageType;

	public class AstralShield extends Spell	{
		public AstralShield(Creature owner)	{
			super(owner);
			nameID = "spell_astralshield";
			requiredMana = 25;
			turnDuration = 25*7;
			usageType = UsageType.BATTLE;
			targetType = EffectTargetType.SELF;
		}

		@Override
		public void takeEffect() {
			super.takeEffect();
			//zdejmowanie zlych czarow 
			if(target==null) return;
			for( Spell spl : target.underEffectOfSpell)	{
				if( spl.nameID.compareTo("spell_physical") == 0 ||
					spl.nameID.compareTo("spell_drain") == 0	||
					spl.nameID.compareTo("spell_fireball") == 0
						)
					spl.turnDuration=0;
			}
		}
		
	}