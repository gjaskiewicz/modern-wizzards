package com.jutsugames.modernwizzards.gamelogic.spells;

import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWMap2Activity;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.Castable.EffectTargetType;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gis.SphericalZone;
import com.jutsugames.modernwizzards.gis.Units;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.map.CollectableItem;
import com.jutsugames.modernwizzards.util.ChangeListener.EventTypes;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class OutOfBodyExperience extends Spell {
	
	private static final String TAG = "OutOfBodyExperience";
	
	public transient GeoPoint destination;
	public transient OOBpointer oobPointer;
	
	transient int latVectL;
	transient int lngVectL;
	
	public enum OOBState {
		STATE_FLYING		
		{{ c = 1; }},
		STATE_RETURNING			
		{{ c = 0; }};
		int c;
	}
	
	private OOBState state;
	
	private boolean canStillWalk;
	private boolean hasDestinationChanged;
	private boolean fastReturn;
	//boolean backhome = false;
	
	public OutOfBodyExperience(Creature owner)	{
		super(owner);
		nameID = "spell_oob";
		requiredMana = 10;
		usageType = UsageType.MAP;
		targetType = EffectTargetType.SELF;
		turnDuration = 25*60*5; //trwa 30 sekund, potem wraca gracza
		destination = null;
		canStillWalk = true;
		hasDestinationChanged = false;
		fastReturn = false;
		oobPointer = new OOBpointer();
		state = OOBState.STATE_FLYING;
	}

	@Override
	public boolean castThis() {
		if(Player.getInst().isUnderEffectOfSpell(this.nameID))	{
			return false;
		}
		
		MWMap2Activity.addTapListener(new TapListener(this));
		MWToast.showToast( R.string.click_where_wish_go, Toast.LENGTH_SHORT);
		
		$log.d(TAG, "Starting activity from class " + Game.getGlobalTopmostActivity().getClass().getName() );
		Game.getGlobalTopmostActivity().startActivity(new Intent(Game.getGlobalTopmostActivity(), MWMap2Activity.class));
				
		//Intent i = new Intent(Game.getGlobalTopmostActivity(), MWMapActivity.class);
		//i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		
		return true;
	}
	
	public void forceReturnToBody(boolean immediate) {
		turnDuration = 1;
		fastReturn = immediate;
	}

	@Override
	public void onRestoreFromSave() {
		super.onRestoreFromSave();
		$log.e(TAG, "Restoring OOBE!!!" );
		turnDuration=0;
		/*synchronized(Player.getInst().underEffectOfSpell) {
			for( Spell spl : Player.getInst().underEffectOfSpell)	{
				if(spl.nameID.equals(this.nameID))
					Player.getInst().underEffectOfSpell.remove(spl);
			}
		}*/
	}
	
	@Override
	public void takeEffect() {
		
		if(turnDuration > 1) //If turnDuration==1 we set returning to body and only after full return we set turnDuration to 0
			turnDuration--;
		
		if(turnDuration == 0)
			return;
		
		//if(turnDuration <= 0) ;//upewnienie si�, �e nie przeliczy czaru ju� nie aktuwnego
		
		//czy wracac do ciala?
		if(turnDuration>=0 && (turnDuration%25==0 || turnDuration == 1) && canStillWalk)	{
			if(!owner.takeMana(5) || turnDuration == 1)	{
				canStillWalk = false;
				latVectL*=2;
				lngVectL*=2;
				//MWMapActivity.getMapView().getOverlays().remove(clickListener);
				MWMap2Activity.removeTapListener();
				destination = Game.getGame().playerEgo.getGeoPoint();
				if(fastReturn) 
					Game.getGame().playerEgo.oobLocation = Game.getGame().playerEgo.getGeoPoint();
				if(turnDuration != 1)
					MWToast.makeToast(Game.getGlobalTopmostActivity(), R.string.oob_out_of_mana, Toast.LENGTH_SHORT).show();
				
				//TODO: zamknac czar;
			}
		}
		
		//plynny przejazd do miejsca docelowego
		GeoPoint currLoc = Game.getGame().playerEgo.getPlayerLocation();
		
		//olane ograniczenie na dystans
		//if(Units.optimizedDistanceCheck(Game.getGame().playerEgo.getLocation(), currLoc, 500))	{
		
		if(!canStillWalk) destination = Game.getGame().playerEgo.getGeoPoint(); //gwarantuje, �e powr�t do cia�a si� uda nawet jak cia�o "ucieka" 
		int latDist = destination.getLatitudeE6() - currLoc.getLatitudeE6();
		int lngDist = destination.getLongitudeE6() - currLoc.getLongitudeE6();
		double metDist = Units.optimizedDistance(destination, currLoc);
		if(metDist==0) metDist=1;
		int latVect = (int)(latVectL * ((int)(latDist*Units.latitudeToMeters()) / metDist));
		int lngVect = (int)(lngVectL * ((int)(lngDist*Units.longitudeToMeters(destination.getLatitudeE6())) / metDist));
		//ruch o pol metra
		Game.getGame().playerEgo.oobLocation = 
			new GeoPoint(currLoc.getLatitudeE6()+latVect, currLoc.getLongitudeE6()+lngVect);
		if(turnDuration%3==0)
			oobPointer.setLocation(Game.getGame().playerEgo.oobLocation);
		
		//}
		
		checkIfCloseToBody();
		
		//MWMap2Activity.refreshMapView();
	}

	private void checkIfCloseToBody() {
		//dusza gracza jest blisko jego cia�a :)
		if( Units.optimizedDistanceCheck(Game.getGame().playerEgo.getGeoPoint(), Game.getGame().playerEgo.oobLocation, 15) ) 
		{
			/**
			 * graczowi sko�czy�a si� mana
			 * lub stwierdzi�, �e mu si� znudzi�o latanie po mie�ci w samej duszy i wr�ci� do cia�a.
			 */
			if(!canStillWalk || (canStillWalk && hasDestinationChanged && 
					Units.optimizedDistanceCheck(Game.getGame().playerEgo.getGeoPoint(), destination, 15))) 
			{
				turnDuration=0;
				Game.getGame().playerEgo.oobLocation = null;
				oobPointer.display(false);
				if(!fastReturn)
					MWToast.makeToast(Game.getGlobalTopmostActivity(), R.string.oob_finished, Toast.LENGTH_SHORT).show();
				notifyListeners(nameID, EventTypes.BACK_IN_BODY, null);
				/*if(Game.getGlobalTopmostActivity() instanceof MWMapActivity) {
					Game.getGlobalTopmostActivity().finish();
				}*/
			}
		}
	}

	private class TapListener implements OnMapClickListener	{

		OutOfBodyExperience caller;
		boolean clicked = false;
		
		public TapListener(OutOfBodyExperience oobe)	{
			caller = oobe;
		}
		
		@Override
		public void onMapClick(LatLng point) {
			if(!clicked)	{
				clicked=true;
				caller.supercast();
				
				destination = Game.getGame().playerEgo.getGeoPoint();
				Game.getGame().playerEgo.oobLocation = Game.getGame().playerEgo.getGeoPoint();
				
				latVectL = (int)(1.5/Units.latitudeToMeters());
				lngVectL = (int)(1.5/Units.longitudeToMeters(Game.getGame().playerEgo.getGeoPoint().getLatitudeE6()));
				
			}
			
			caller.destination = MWMap2Activity.convLatLngToGeopoint(point);//jesli nie za daleko:
			caller.hasDestinationChanged = true;
			oobPointer.display(true);
			 $log.d(TAG, "OOB DESTINATION CHANGED!");
		}
		
	}
	
	public class OOBpointer extends PhysicalObject	{
		OOBpointer()	{
			zone = new SphericalZone(Player.getInst().getGeoPoint(), 15);
			setLocation(Player.getInst().getGeoPoint());
		}
		@Override
		public int getColor() {
			// TODO Auto-generated method stub
			return 0xBBFFA0FF;
		}
	}

	public void supercast() {
		super.castThis();
	}
	
}
