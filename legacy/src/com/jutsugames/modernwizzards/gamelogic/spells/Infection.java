package com.jutsugames.modernwizzards.gamelogic.spells;

import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.Castable.EffectTargetType;
import com.jutsugames.modernwizzards.gamelogic.Castable.UsageType;

public class Infection extends Spell {

	private int takeHealthEvery = 25; // turns

	public Infection(Creature owner) {
		super(owner);
		nameID = "spell_infection";
		requiredMana = 0;
		turnDuration = 25*60*60;
		usageType = UsageType.ANYTIME;
		targetType = EffectTargetType.ANOTHER;
		behaviourRange = 100;
	}

	@Override
	public boolean castThis() {
		// zapewnienie ze gracz bedzie tylko pod jedn� infekcj�
		if (target.isUnderEffectOfSpell(nameID))
			return false;
		return super.castThis();
	}

	@Override
	public void takeEffect() {
		super.takeEffect();
		// zdejmuje HP a� po 5 pkt, a po godzinie reszte.
		if ((turnDuration % takeHealthEvery == 0 && target.getHealthPoints() > 5)
				|| (turnDuration < 3600 && target.getHealthPoints() >= 0)) {
			target.takeHealth(1);
		}
		if(target == Player.getInst() && Player.getInst().getHealthPoints()<=0)	{
			//jak umar� to zako�cz. 
			Player.getInst().die();
			turnDuration=0;
		}
	}

}