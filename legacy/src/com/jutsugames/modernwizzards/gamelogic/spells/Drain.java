package com.jutsugames.modernwizzards.gamelogic.spells;

import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.Castable.EffectTargetType;
import com.jutsugames.modernwizzards.gamelogic.Castable.UsageType;

public class Drain extends Spell {

	public int manaToTake = 10;
	
	public Drain(Creature owner) {
		super(owner);
		nameID = "spell_drain";
		requiredMana = 0;
		turnDuration = 1;
		usageType = UsageType.ANYTIME;
		targetType = EffectTargetType.ANOTHER;
		behaviourRange = 100;
	}

	@Override
	public void takeEffect() {
		super.takeEffect();
		target.takeMana(manaToTake);
		owner.addManaPoints(manaToTake/2, false);
	}

}