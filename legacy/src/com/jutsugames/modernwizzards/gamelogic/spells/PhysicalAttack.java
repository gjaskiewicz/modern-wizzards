package com.jutsugames.modernwizzards.gamelogic.spells;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.Castable.EffectTargetType;
import com.jutsugames.modernwizzards.gamelogic.Castable.UsageType;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class PhysicalAttack extends Spell implements Castable {

	public String displayNameID;
	
	public double hitpointsToTake;
	public double chanceForCritical = 0.8;
	public double criticalFactor=2;
	public boolean infects;
	/**spell zawieraj�cy si� przy rzucaniu*/ 
	public Spell infection;
	
	public PhysicalAttack(Creature owner)	{
		super(owner);
		nameID = "spell_physical";
		requiredMana = 7;
		turnDuration = 1;
		hitpointsToTake = 12;
		usageType = UsageType.BATTLE;
		targetType = EffectTargetType.ANOTHER;
		behaviourRange = 15;
		
		painter.setColor(0x88888888);
	}

	@Override
	public boolean castThis() {
		boolean success = super.castThis();
		if (success && infects && infection!=null)	{
			infection.setTarget(target);
			infection.castThis();
		}
		return success;
	}

	@Override
	public void takeEffect() {
		super.takeEffect();
		$log.d("SPELL", "Physical takes hitpoints");
		double factor = 1;
		if(owner.getDiceRoll()>chanceForCritical) {
			factor = criticalFactor;
			MWToast.showToast(R.string.spells_critical, Toast.LENGTH_SHORT);
		}
		target.takeHealth(factor*hitpointsToTake);
	}

	@Override
	public ObjectInfo getInfo() {
		if(displayNameID==null)
			displayNameID=nameID;
		return ObjectInfoFactory.get().getInfoStandarized(displayNameID);
	}
}
