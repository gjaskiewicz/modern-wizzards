package com.jutsugames.modernwizzards.gamelogic.spells;

import android.content.Context;
import android.util.Log;

import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.Castable.EffectTargetType;
import com.jutsugames.modernwizzards.gamelogic.Castable.UsageType;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class Electrokinesis extends Spell implements Castable {

	public int hitpointsToTake;
	public boolean infects;
	/**spell zawieraj�cy si� przy rzucaniu*/ 
	public Spell infection;
	
	public Electrokinesis(Creature owner)	{
		super(owner);
		nameID = "spell_electrokinesis";
		requiredMana = 25;
		turnDuration = 1;
		hitpointsToTake = 15;
		usageType = UsageType.BATTLE;
		targetType = EffectTargetType.ANOTHER;
		behaviourRange = 80;
		
		painter.setColor(0x683a75c4);
	}

	@Override
	public boolean castThis() {
		boolean success = super.castThis();
		if (success && infects && infection!=null)	{
			infection.setTarget(target);
			infection.castThis();
		}
		return success;
	}

	@Override
	public void takeEffect() {
		super.takeEffect();
		$log.d("SPELL", "Eletrokinesis takes hitpoints");
		target.takeHealth(hitpointsToTake);
	}

}
