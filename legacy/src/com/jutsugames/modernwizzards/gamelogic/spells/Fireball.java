package com.jutsugames.modernwizzards.gamelogic.spells;

import android.content.Context;

import com.jutsugames.modernwizzards.gamelogic.Castable;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.Castable.EffectTargetType;
import com.jutsugames.modernwizzards.objects.ObjectInfo;

public class Fireball extends Spell implements Castable {

	public Fireball(Creature owner)	{
		super(owner);
		nameID = "spell_fireball";
		requiredMana = 30;
		turnDuration = 1;
		usageType = UsageType.BATTLE;
		targetType = EffectTargetType.ANOTHER;
		behaviourRange = 25;
	}

	@Override
	public boolean castThis() {
		return super.castThis(); //koniecznie!
	}

	@Override
	public void takeEffect() {
		super.takeEffect();		
		target.takeHealth(30);
	}

}
