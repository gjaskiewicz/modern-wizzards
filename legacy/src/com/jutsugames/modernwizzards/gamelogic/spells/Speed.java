package com.jutsugames.modernwizzards.gamelogic.spells;

import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.Castable.EffectTargetType;
import com.jutsugames.modernwizzards.gamelogic.Castable.UsageType;

	public class Speed extends Spell	{
		public Speed(Creature owner)	{
			super(owner);
			nameID = "spell_battlespeed";
			requiredMana = 10;
			turnDuration = 50;
			usageType = UsageType.BATTLE;
			targetType = EffectTargetType.SELF;
		}
		
	}