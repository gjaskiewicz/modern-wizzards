package com.jutsugames.modernwizzards.gamelogic.spells;

import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.MainLoop;
import com.jutsugames.modernwizzards.gamelogic.Spell;
import com.jutsugames.modernwizzards.gamelogic.Castable.EffectTargetType;
import com.jutsugames.modernwizzards.gamelogic.Castable.UsageType;

public class BadLuck extends Spell {
	public BadLuck(Creature owner) {
		super(owner);
		nameID = "spell_badluck";
		requiredMana = 0;
		turnDuration = MainLoop.ITERATIONS_PER_SECOND * 60 * 15; // dziala 15
																	// minut!
		usageType = UsageType.BATTLE;
		targetType = EffectTargetType.ANOTHER;
	}

	@Override
	public boolean castThis() {
		// zapewnienie ze gracz bedzie tylko pod jedn� infekcj�
		if (target.isUnderEffectOfSpell(nameID))
			return false;
		return super.castThis();
	}
}