package com.jutsugames.modernwizzards.gamelogic.achivements;

import java.util.Calendar;

public class AchivDays extends Achivement {
	public int daysInARow = 1;
	public int lastDay = 0;
	
	public AchivDays() {
		nameid = "achiv_days";
	}
	
	public void nextRun()	{
		//Calendar.getInstance().getTime().getgetDate();
		if(lastDay+1==Calendar.getInstance().get(Calendar.DAY_OF_YEAR))	{
			daysInARow++;
			if(daysInARow==7)	{
				achive();
			}
		}
		else	{
			lastDay=Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
			daysInARow=1;
		}
	}
}
