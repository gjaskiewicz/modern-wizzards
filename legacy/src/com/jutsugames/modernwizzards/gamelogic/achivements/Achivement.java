package com.jutsugames.modernwizzards.gamelogic.achivements;

import android.content.Context;

import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.MWToast;
import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;

public class Achivement {
	boolean achived = false;
	String nameid;
	
	public boolean isAchived()	{
		return achived;
	}
	
	public ObjectInfo getInfo(Context ctx) {
		return ObjectInfoFactory.get().getInfoStandarized(nameid);
	}
	
	public void achive()	{
		if(achived) return;
		achived = true;
		MWToast.showToast( R.string.achivment_unlocked, MWToast.LENGTH_LONG);
	}
}
