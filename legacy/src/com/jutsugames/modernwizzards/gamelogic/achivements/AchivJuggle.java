package com.jutsugames.modernwizzards.gamelogic.achivements;

import java.util.Calendar;

import com.jutsugames.modernwizzards.gamelogic.Player;
import com.jutsugames.modernwizzards.gamelogic.Spell;

public class AchivJuggle extends Achivement {
	
	public AchivJuggle() {
		nameid = "achiv_juggle";
	}
	
	public void checkJuggling()	{
		if(achived) return;
		int i = 0;
		for(Spell sp : Player.getInst().underEffectOfSpell)	{
			if(sp.nameID.equals("spell_telekinesis"))
				i++;
		}
		if(i==3)	{
			achive();
		}
	}
}
