package com.jutsugames.modernwizzards.gamelogic.achivements;

import java.util.Calendar;

public class AchivMedit extends Achivement {
	public transient int totalSecInZone=0;
	
	public AchivMedit() {
		nameid = "achiv_medit";
	}
	
	public void stop()	{
		totalSecInZone=0;
	}
	
	public void checkMedit()	{
		if(isAchived()) return;
		totalSecInZone++;
		if(totalSecInZone==60*15)	{
			achive();
		}
	}
}
