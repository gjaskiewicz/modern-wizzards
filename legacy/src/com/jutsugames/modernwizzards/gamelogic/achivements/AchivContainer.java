package com.jutsugames.modernwizzards.gamelogic.achivements;

import java.util.LinkedList;

public class AchivContainer {
	private AchivHunter achivHunter = new AchivHunter();
	private AchivPlants achivPlants = new AchivPlants();
	private AchivMixtures achivMixtures = new AchivMixtures();
	
	private AchivDistanceTravel achivDistanceTravel;
	private AchivDistanceVariety achivDistanceVariety;
	
	private AchivDays achivDays;
	private AchivGameFinished achivGameFinished;
	
	private AchivMedit achivMedit;
	private AchivJuggle achivJuggle;
	
	private transient LinkedList<Achivement> allAchives;
	
	public LinkedList<Achivement> getAllAchivs() {
		if(allAchives!=null) return allAchives;
		allAchives = new LinkedList<Achivement>();
		allAchives.add(achivHunter);
		allAchives.add(achivPlants);
		allAchives.add(achivMixtures);
		allAchives.add(getAchivDistanceTravel());
		allAchives.add(getAchivDistanceVariety());
		allAchives.add(getAchivDays());
		allAchives.add(getAchivMedit());
		allAchives.add(getAchivJuggle());
		allAchives.add(getAchivGameFinished());
		return allAchives;
	}
	
	public AchivHunter getAchivHunter()	{
		if(achivHunter==null) achivHunter = new AchivHunter();
		return achivHunter;
	}

	public AchivPlants getAchivPlants() {
		if(achivPlants==null) achivPlants = new AchivPlants();
		return achivPlants;
	}
	
	public AchivMixtures getAchivMixtures() {
		if(achivMixtures==null) achivMixtures = new AchivMixtures();
		return achivMixtures;
	}

	public AchivDistanceTravel getAchivDistanceTravel()	{
		if(achivDistanceTravel==null) achivDistanceTravel = new AchivDistanceTravel();
		return achivDistanceTravel;
	}
	
	public AchivDistanceVariety getAchivDistanceVariety()	{
		if(achivDistanceVariety==null) achivDistanceVariety = new AchivDistanceVariety();
		return achivDistanceVariety;
	}
	
	public AchivGameFinished getAchivGameFinished()	{
		if(achivGameFinished==null) achivGameFinished = new AchivGameFinished();
		return achivGameFinished;
	}
	
	public AchivDays getAchivDays()	{
		if(achivDays==null) achivDays = new AchivDays();
		return achivDays;
	}
	
	public AchivMedit getAchivMedit()	{
		if(achivMedit==null) achivMedit = new AchivMedit();
		return achivMedit;
	}
	
	public AchivJuggle getAchivJuggle()	{
		if(achivJuggle==null) achivJuggle = new AchivJuggle();
		return achivJuggle;
	}
}
