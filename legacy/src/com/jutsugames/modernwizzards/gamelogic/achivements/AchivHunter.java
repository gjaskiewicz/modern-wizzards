package com.jutsugames.modernwizzards.gamelogic.achivements;

import java.util.HashSet;

public class AchivHunter extends Achivement {
	public HashSet<String> defetedCreatures = new HashSet<String>();
	
	public AchivHunter() {
		nameid="achiv_hunter";
	}
	
	public void addCreatureDefeated(String creature)	{
		defetedCreatures.add(creature);
		if(defetedCreatures.size()>=8 && !achived)	{
			achive();
		}
	}
}
