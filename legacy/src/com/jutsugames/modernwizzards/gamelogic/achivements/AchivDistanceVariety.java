package com.jutsugames.modernwizzards.gamelogic.achivements;

import java.util.HashSet;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.gis.Units;

import android.location.Location;

public class AchivDistanceVariety extends Achivement {
	private GeoPoint lefttop;
	private GeoPoint rightdown;
	
	public AchivDistanceVariety() {
		nameid="achiv_distvariety";
	}
	
	public void addMetersTraveled(Location l) {
		GeoPoint locationGP = new GeoPoint((int)(l.getLatitude()*1E6), (int)(l.getLongitude()*1E6));
		boolean changed = false;
		
		if(lefttop==null) lefttop=locationGP;
		if(rightdown==null) rightdown=locationGP;
		
		if(lefttop.getLatitudeE6()>locationGP.getLatitudeE6())	{
			changed = true;
			lefttop = new GeoPoint(locationGP.getLatitudeE6(), lefttop.getLongitudeE6());
		}
		if(lefttop.getLongitudeE6()>locationGP.getLongitudeE6()){
			changed = true;
			lefttop = new GeoPoint(lefttop.getLatitudeE6(), locationGP.getLongitudeE6());
		}	
		if(rightdown.getLatitudeE6()<locationGP.getLatitudeE6()){
			changed = true;
			rightdown = new GeoPoint(locationGP.getLatitudeE6(), rightdown.getLongitudeE6());
		}
		if(rightdown.getLongitudeE6()<locationGP.getLongitudeE6())	{
			changed = true;
			rightdown = new GeoPoint(rightdown.getLatitudeE6(), locationGP.getLongitudeE6());		
		}
		
		if(changed)	{
			int meterDelta = Units.optimizedDistance(lefttop, rightdown);
			
			if(meterDelta > 500000) {
				achive();
			}
		}
	}
}
