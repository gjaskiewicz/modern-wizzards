package com.jutsugames.modernwizzards.gamelogic.achivements;

import java.util.HashSet;

public class AchivPlants extends Achivement {
	public HashSet<String> collectedPlants = new HashSet<String>();
	
	public AchivPlants() {
		nameid = "achiv_plants";
	}
	
	public void addCollectedPlant(String plant)	{
		collectedPlants.add(plant);
		if(collectedPlants.size()>=9 && !achived)	{
			achive();
		}
	}
}
