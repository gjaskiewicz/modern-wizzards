package com.jutsugames.modernwizzards.gamelogic.achivements;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gis.Units;

import android.location.Location;

public class AchivDistanceTravel extends Achivement {
	public int metersTraveled = 0;
	private transient GeoPoint prevLoc;
	
	public AchivDistanceTravel() {
		nameid="achiv_disttraveled";
	}
	
	public void addMetersTraveled(Location l)	{
		GeoPoint locationGP = new GeoPoint((int)(l.getLatitude()*1E6), (int)(l.getLongitude()*1E6));
		
		if(prevLoc==null) prevLoc=locationGP;
		
		int meterDelta = Units.optimizedDistance(prevLoc, locationGP);
		
		if(meterDelta>1000) meterDelta=0;
		prevLoc = locationGP;
		metersTraveled+=meterDelta;
		if(metersTraveled>20000)	{
			achive();
		}
	}
}
