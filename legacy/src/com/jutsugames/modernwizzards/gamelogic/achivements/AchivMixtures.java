package com.jutsugames.modernwizzards.gamelogic.achivements;

import java.util.HashSet;

public class AchivMixtures extends Achivement {
	public HashSet<String> mixturesmade = new HashSet<String>();
	
	public AchivMixtures() {
		nameid = "achiv_mixtures";
	}
	
	public void addMixtureMade(String what)	{
		mixturesmade.add(what);
		if(mixturesmade.size()>=6 && !achived)	{
			achive();
		}
	}
}
