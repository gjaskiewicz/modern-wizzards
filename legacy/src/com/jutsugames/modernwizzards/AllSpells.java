package com.jutsugames.modernwizzards;

import java.io.InputStream;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.creatures.*;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.prolog.PrologCommLibrary;
import com.jutsugames.modernwizzards.util.MakeStringList;

import alice.tuprolog.*;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class AllSpells {

	private static AllSpells _inst = new AllSpells();
	private static final boolean PERFORM_SELF_TEST = false;
	static {
		if (PERFORM_SELF_TEST) {
			_inst.selfTest();
		}
	}
	public static AllSpells get() { 
		return _inst;
	}
	
	//private static List<CollectPlant> allPlants;
	
	private String fullRead(InputStream ins) {
		java.util.Scanner s = new java.util.Scanner(ins).useDelimiter("\\A");
	    String readValue = s.hasNext() ? s.next() : null;
	    if (readValue == null) {
	    	$log.d("PROLOG", "Fullread error");
	    	throw new Error("Full read error while parsing spellbook");
	    }
	    
	    return readValue;
	}
	
	private Prolog getEngine()	{
		if(engine==null)	{
			engine = new Prolog(); {
			String is = fullRead(getClass()
				.getResourceAsStream("/com/jutsugames/modernwizzards/spellbook/spellbook.pl"));
			String hb = fullRead(getClass()
				.getResourceAsStream("/com/jutsugames/modernwizzards/spellbook/herbal_knowledge.pl"));
			String cn = fullRead(getClass()
				.getResourceAsStream("/com/jutsugames/modernwizzards/spellbook/creatures_knowledge.pl"));
			
			try {
				Theory spellbook = new Theory(is);
				Theory herbarium = new Theory(hb);
				Theory beastarius = new Theory(cn);
				
				spellbook.append(herbarium);
				spellbook.append(beastarius);
				
				engine.setTheory(spellbook);
				engine.loadLibrary(new PrologCommLibrary());
			} catch (InvalidTheoryException e) {
				e.printStackTrace();
				throw new Error("Invalid spellbook");
			} catch (InvalidLibraryException e) {
				e.printStackTrace();
				throw new Error("No library");
			}
		}
		}
		ModernWizzardsActivity.logMem();	
		return	engine;
	}
	
	private Prolog engine;
	
	
	void selfTest() {
		try {
			$log.d("PROLOG", "Prolog self test");
			SolveInfo solutionInfo = getEngine().solve("make([item_garlic,item_swallowweed|item_rozkovnik], X).");
			while(getEngine().hasOpenAlternatives()) {
				Term solution = solutionInfo.getSolution();
				System.out.println(solution);
				$log.d("PROLOG", "Self-test: " + solution);
				
				solutionInfo = getEngine().solveNext();
			}
			$log.d("PROLOG", "Self-test: end");
			getEngine().solveEnd();
			
		} catch (MalformedGoalException e) {
			e.printStackTrace();
			$log.d("PROLOG", "ERR: " + e.getMessage());
		} catch (NoSolutionException e) {
			e.printStackTrace();
			$log.d("PROLOG", "ERR: " + e.getMessage());
		} catch (NoMoreSolutionException e) {
			e.printStackTrace();
			$log.d("PROLOG", "ERR: " + e.getMessage());
		}
	}
	 
	public LinkedList<String> possiblePotions(List<String> str) {
		String prologList = MakeStringList.makeList(str);
		LinkedList<String> possibleSpells = new LinkedList<String>();
		
		try {
			Set<String> spellSet = new HashSet<String>();
			String pQuery = "make("+prologList+", X).";
			$log.d("PROLOG", "query: " + pQuery);
			//System.out.println(pQuery);
			SolveInfo solutionInfo = getEngine().solve(pQuery);
			while(getEngine().hasOpenAlternatives()) {
				Term solution = solutionInfo.getSolution();
				//System.out.println(solution);
				Struct pStruct = (Struct)solution;
				
				Var t1 = (Var)pStruct.getArg(1);
				String tVal = ""+t1.getTerm();
				if (!("nothing".equals(tVal) || "test".equals(tVal))) {
					spellSet.add(tVal);
					$log.d("PROLOG", "query result: " + tVal);
				}
				
				solutionInfo = getEngine().solveNext();
			}
			getEngine().solveEnd();
			
			possibleSpells.addAll(spellSet);

		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return possibleSpells;
	}
	

	/**
	 * Zwraca list� wszystkich TYP�W ro�lin istniejacych w grze, jako referencj� np do generacji mapy
	 * oraz do Zielnika. NIE MODYFIKOWA� WYNIK�W!
	 * @return jak w opisie
	 */
	/*public List<CollectPlant> getAllPlantsTypes() {
		if(allPlants == null) {
			allPlants = new LinkedList<CollectPlant>();
			//plantNameOnPlant = new HashMap<String, CollectPlant>();
		}
		else
			return allPlants;
		
		try {
			String pQuery = "plant_popularity(X,Y).";
			
			$log.d("PROLOG", "query: " + pQuery);
			SolveInfo solutionInfo = engine.solve(pQuery);
			boolean keepGoin = true;
			while(keepGoin) {
				CollectPlant tempPlant;
				
				String plantName = ""+solutionInfo.getVarValue("X");
				tempPlant = new CollectPlant(plantName);
				//Var t2 = (Var)pStruct.getArg(1);
				String varY = ""+solutionInfo.getTerm("Y");
				tempPlant.popularity = Integer.parseInt(varY);
				//$log.d("PROLOG", "loaded plant popularity: " + tempPlant.popularity);
				//plantNameOnPlant.put(plantName, tempPlant);
				allPlants.add(tempPlant);
				
				$log.d("PROLOG", "loaded plant: " + plantName);
				$log.d("PROLOG", "loaded plant popularity: " + tempPlant.popularity);
				//$log.d("PROLOG", "loaded plant v1: ." + solutionInfo.getTerm("Y")+".");
				
				keepGoin = engine.hasOpenAlternatives();
				if(keepGoin) solutionInfo = engine.solveNext();
			}
			engine.solveEnd();
			
			for(CollectPlant cp : allPlants)	{
				cp.popularityOffset = CollectPlant.plantsPopularitySum;
				CollectPlant.plantsPopularitySum += cp.popularity;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return allPlants;
	}
	*/
	
	/* kasuje bo niepotrzebne - mo�na odrazu tworzy� CollectPlant(IDname)
	public CollectPlant getPlantByName(String name ) {
		if(plantNameOnPlant == null) getPlants();
		
		if (plantNameOnPlant.containsKey(name))
			return plantNameOnPlant.get(name);
		else
			return null;
	}*/
	
}
