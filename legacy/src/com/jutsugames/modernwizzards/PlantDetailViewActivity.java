package com.jutsugames.modernwizzards;

import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jutsugames.modernwizzards.R;
import com.jutsugames.modernwizzards.controls.Menubutton;
import com.jutsugames.modernwizzards.objects.ObjectInfo;
import com.jutsugames.modernwizzards.objects.ObjectInfoFactory;
import com.jutsugames.modernwizzards.objects.PlantFactory;
import com.jutsugames.modernwizzards.objects.inventory.CollectPlant;
import com.jutsugames.modernwizzards.objects.inventory.InventoryItem;

public class PlantDetailViewActivity extends Activity {
	
	private static final String TAG = "PlantDetailViewActivity";
	private String result = null;
	//String info_name;
	//u�y� do podawania argumentu
	public static InventoryItem toDisplay;
	private InventoryItem itemDisplayed;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        $log.d(TAG, "PlantDetailViewActivity starts");
        itemDisplayed=toDisplay;
        
        //info_name = extras.getString("info_name");
        ObjectInfo oi = itemDisplayed.getInfo();
        
        int nameViewId, descViewId, imageViewId;	
		nameViewId = R.id.plant_detail_name;
		descViewId = R.id.plant_detail_desc;
		imageViewId = R.id.plant_detail_image;
		
		setContentView(R.layout.plant_item_details);
        ((TextView)findViewById(nameViewId)).setText(oi.getName());
        TextView results = (TextView) findViewById(descViewId);
        results.setMovementMethod(new ScrollingMovementMethod());
        results.setText(oi.getDescription());
        ImageView image = (ImageView) findViewById(imageViewId);
        image.setImageResource(oi.getImageID());
        //image.setImageDrawable(getResources().getDrawable(oi.getImageID()));

    }
    
    @Override
    public void finish() {
      Intent data = new Intent();
      data.putExtra("returnValue", result);
      data.putExtra("info_name", itemDisplayed.getInfo().getID());
      
      // Activity finished ok, return the data
      setResult(RESULT_OK, data);
      super.finish();
    } 

    
}
