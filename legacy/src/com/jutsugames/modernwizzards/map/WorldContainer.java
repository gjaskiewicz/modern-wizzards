package com.jutsugames.modernwizzards.map;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.android.maps.GeoPoint;
import com.jutsugames.modernwizzards.Game;
import com.jutsugames.modernwizzards.Options;
import com.jutsugames.modernwizzards.gamelogic.Creature;
import com.jutsugames.modernwizzards.gamelogic.MainLoop;
import com.jutsugames.modernwizzards.gamelogic.PhysicalObject;
import com.jutsugames.modernwizzards.gis.Units;
import com.jutsugames.modernwizzards.objects.CreaturesFactory;
import com.jutsugames.modernwizzards.objects.LocationSlab;
import com.jutsugames.modernwizzards.objects.ManaZonesFactory;
import com.jutsugames.modernwizzards.objects.PlantFactory;
import com.jutsugames.modernwizzards.objects.map.*;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

//obiekt przechowuje i zarz�ca obiektami na mapie - jak trzeba, to je generuje itp. 
public class WorldContainer {
	
	//private transient List<Long> generatedSlabs;
	
	private static final String TAG = "WorldContainer";
	private transient Map<Long, LocationSlab> locationSlabs;
	private transient Map<Long, LocationSlab> newSlabs;
	
	/** dzien ROKU, u�yj Calendar.getInstance().getTime().getDate()*/
	public int saveDate = 1; 
	public LinkedList<Integer> gatheredPlants = new LinkedList<Integer>();
	public LinkedList<Integer> creaturesDefeated = new LinkedList<Integer>();

	/** obiekty questowe, nie zwiazane ze Slabami! */
	//public transient List<PhysicalObject> otherObjects = Collections.synchronizedList( new LinkedList<PhysicalObject>());
	private transient LinkedList<PhysicalObject> otherObjects = new LinkedList<PhysicalObject>();

	private transient LinkedList<PhysicalObject> allObjects;
	//private transient int lastGenerationTurn=-1;
	
	public WorldContainer()	{
		//generatedSlabs= new LinkedList<Long>();
		locationSlabs = new HashMap<Long, LocationSlab>();
	}
	
	/**
	 * u�ywac do dodawania obiektow questowych na mape.
	 * @param obj
	 * @param clearObjects czy czyscic mape z obiekt�w dooko�a.
	 */
	public void addQuestObject(PhysicalObject obj, boolean clearObjects)	{
		if(clearObjects)	{
			for(PhysicalObject po : getAllGeneratedObjs())	{
				if(Units.optimizedDistanceCheck(po.getGeoPoint(), 
						obj.getGeoPoint(), po.zone.radius+obj.zone.radius))	{
					po.homeLocation.remove(po);
				}
			}
		}
		synchronized (otherObjects) {
			otherObjects.add(obj);
		}
		obj.display(true);
	}
	
	/**
	 * u�ywac do uzuwania obiektow questowych z mapy.
	 * @param obj
	 */
	public void removeQuestObject(PhysicalObject obj)	{
		synchronized (otherObjects) {
			otherObjects.remove(obj);
		}
		MainLoop.animations.remove(obj);
		obj.display(false);
	}
	
	/**
	 * Wszystkie generowane obiekty ORAZ questowe obiekty. U�yj do czar�w, interakcji itp.
	 * @return
	 */
	public LinkedList<PhysicalObject> getAllObjects()	{
		LinkedList<PhysicalObject> allObj = getAllGeneratedObjs();
		allObj.addAll(Game.getGame().worldContainer.otherObjects);
		return allObj;
	}
	
	/**
	 * zwraca liste wszystkich generowanych obiekt�w na mapie
	 * @return
	 */
	public LinkedList<PhysicalObject> getAllGeneratedObjs(){
		//TODO: mo�naby zmniejszy� ilo�c generowania tych list, ale cos sie walilo z wyswietlaniem
		//if(lastGenerationTurn!=MainLoop.loopIteration) {
			allObjects = new LinkedList<PhysicalObject>();
			for(LocationSlab ls : locationSlabs.values()) {
				allObjects.addAll(ls.creatures);
				allObjects.addAll(ls.items);
				allObjects.addAll(ls.manaZones);
			}
			//lastGenerationTurn=MainLoop.loopIteration;
		//}
		return allObjects;
	}
	
	/**
	 * zapelnia mape w slabie danego punktu i przyleglych lokalizacjach.
	 * @param g - dla jakiego punktu generowa�. 
	 */
	public void generateObjects(GeoPoint g)	{
		//dla wszystkich sasiednich "kwadrat�w"
		newSlabs = new HashMap<Long, LocationSlab>();
		
		generateObjectsForSlab(new GeoPoint(g.getLatitudeE6()+LocationSlab.SLAB_SIZE_LAT, g.getLongitudeE6()+LocationSlab.SLAB_SIZE_LNG ));
		generateObjectsForSlab(new GeoPoint(g.getLatitudeE6()+LocationSlab.SLAB_SIZE_LAT, g.getLongitudeE6()));
		generateObjectsForSlab(new GeoPoint(g.getLatitudeE6()+LocationSlab.SLAB_SIZE_LAT, g.getLongitudeE6()-LocationSlab.SLAB_SIZE_LNG ));
		generateObjectsForSlab(new GeoPoint(g.getLatitudeE6(), g.getLongitudeE6()+LocationSlab.SLAB_SIZE_LNG ));
		generateObjectsForSlab(g);
		generateObjectsForSlab(new GeoPoint(g.getLatitudeE6(), g.getLongitudeE6()-LocationSlab.SLAB_SIZE_LNG ));
		generateObjectsForSlab(new GeoPoint(g.getLatitudeE6()-LocationSlab.SLAB_SIZE_LAT, g.getLongitudeE6()+LocationSlab.SLAB_SIZE_LNG ));
		generateObjectsForSlab(new GeoPoint(g.getLatitudeE6()-LocationSlab.SLAB_SIZE_LAT, g.getLongitudeE6() ));
		generateObjectsForSlab(new GeoPoint(g.getLatitudeE6()-LocationSlab.SLAB_SIZE_LAT, g.getLongitudeE6()-LocationSlab.SLAB_SIZE_LNG ));

		//zapewnia ze dalsze obiekty napewno znikn� z mapy. 
		for(LocationSlab ls : locationSlabs.values()) {
			if(!newSlabs.containsKey(ls.getHash()))	{
				//$log.d(TAG,"Chowam obiektYYYYY bez slabu");
				for(PhysicalObject po : ls.getAllGeneratedObjects())	{
					po.display(false);
					//$log.d(TAG,"Chowam obiekt bez slabu");
				}
			}
		}
		locationSlabs = newSlabs;
	}
	
	/**
	 * generuje mape dla konkretnego Slaba (segmentu mapy)
	 * @param g
	 * @return
	 */
	private boolean generateObjectsForSlab(GeoPoint g)	{
		long slabHash = LocationSlab.geoPointToSlabHash(g);
		if(isMapGeneratedForLocation(g))	{
			//na wypadek "aktualizacji"
			LocationSlab oldOne = locationSlabs.get(LocationSlab.geoPointToSlabHash(g));
			oldOne.regenerate(); 
			newSlabs.put(slabHash, oldOne);
			return false; //nie ma potrzeby, juz wygenerowane
		}
		else	{
			LocationSlab ls = LocationSlab.newSlabForLocation(g);
			ls.regenerate();
			newSlabs.put(slabHash, ls);
			//locationSlabs.put(slabHash, ls);
			return true;
		}
	}
	
	/**
	 * sprawdza czy generowano juz mape dla tego segmentu
	 * @param g
	 * @return
	 */
	private boolean isMapGeneratedForLocation(GeoPoint g)	{
		long hash=LocationSlab.geoPointToSlabHash(g);
		return locationSlabs.containsKey(hash);
		//return generatedSlabs.contains(hash);
	}
	
	public LinkedList<TriggerZone>	getTriggerZones()	{
		LinkedList<TriggerZone> res = new LinkedList<TriggerZone>();
		for(PhysicalObject o : otherObjects)	{
			if(o instanceof TriggerZone)	
				res.add((TriggerZone)o);
		}
		return res;
	}

	/** uwaga - KLON */
	public LinkedList<PhysicalObject> getAllQuestObjets() {
		return (LinkedList<PhysicalObject>)(otherObjects.clone());
	}
}
