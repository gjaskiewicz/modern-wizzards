package com.jutsugames.modernwizzards;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import static com.jutsugames.modernwizzards.logging.JutsuLogger.$log;

public class EngineService extends Service {
	private static final String TAG = "EngineService";
	
	@Override
	public void onCreate() {
		super.onCreate();
		$log.d(TAG, "EngineService created");
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//start();
		$log.d(TAG, "EngineService started");
		return super.onStartCommand(intent, flags, startId);
	}
	
	@Override
	public void onDestroy() {
		$log.d(TAG, "EngineService stopped");
		super.onDestroy();
	}

}
