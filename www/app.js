var map;
var user;
var playerEgo;

var debug_log = function(msg) {
  console.log(msg);
}

var initMap = function() {
  var mapElement = document.getElementById('map');
  map = new google.maps.Map(mapElement, {
    center: {lat: -34.397, lng: 150.644},
    zoom: 8
  });
};

var init = function() {
  if (!navigator.geolocation) {
    debug_log('no geo localization');
  } else {
    navigator.geolocation.getCurrentPosition(updatePosition);
  }

  var config = {
    apiKey: "AIzaSyCS-JrZ57QgXwBh1rw_0mkOaqP4ujqmsGY",
    authDomain: "modern-wizzards-web-dev.firebaseapp.com",
    databaseURL: "https://modern-wizzards-web-dev.firebaseio.com",
    projectId: "modern-wizzards-web-dev",
    storageBucket: "modern-wizzards-web-dev.appspot.com",
    messagingSenderId: "207677770229"
  };
  firebase.initializeApp(config);

  var provider = new firebase.auth.GoogleAuthProvider();
  provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
  firebase.auth().signInWithPopup(provider).then(function(result) {
    var token = result.credential.accessToken;
    user = result.user;
    // ...
    debug_log("Logged in " + user + " " + token);

    $("#user").attr("src", user.photoURL);

    var ref = firebase.database().ref('users/' + user.uid)
    ref.once("value", function(snapshot) {
      if (!snapshot.exists()) {
        ref.set({
          setLocation: false,
          inventory: [],
        });
      } else {
        playerEgo = snapshot.val();
        debug_log('Loaded user from DB');
      }
    });

    // $('#test-text').addClass('animated bounce');
  }).catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;
    var email = error.email;
    var credential = error.credential;
    // ...
    debug_log("Not logged in " + error);
  });
};

var updatePosition = function(position) {
    debug_log("Latitude: " + position.coords.latitude + " Longitude: " + position.coords.longitude); 
}

var openScreen = function(name) {
  $('.screen').hide();
  $(`#${name}_screen`).show();
  closeNav();
}

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
