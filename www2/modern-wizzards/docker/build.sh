#!/bin/sh

set -e
MINOR=`git rev-list --count --first-parent HEAD`
MAJOR="v.0.1"
VERSION="${MAJOR}.${MINOR}"

echo "BUILDING version ${VERSION} ... "

rm -r build
mkdir build
cp -r ../build .
chmod -R 777 build
docker build . -t gcr.io/modern-wizzards-web/dev-server:${VERSION}

echo "Pushing version ${VERSION} to GCR ..."
# gcloud docker -- push gcr.io/modern-wizzards-web/dev-server:${VERSION}

