#!/bin/bash

VER=`git rev-list --count HEAD`
echo "export default ${VER}" > src/version.js
npm run build
firebase deploy --project=modern-wizzards-web-dev
