import Color from "./Color.js";

describe('Colors', () => {
  it('should convert to hex no alpha', () => {
    expect(Color.toHexRGB(0xfa0000)).toEqual("#fa0000");
    expect(Color.toHexRGB(0x00fb00)).toEqual("#00fb00");
    expect(Color.toHexRGB(0x0000fc)).toEqual("#0000fc");
  });

  it('should convert to hex with alpha', () => {
    expect(Color.toHexRGB(0xccfa0000)).toEqual("#fa0000");
    expect(Color.toHexRGB(0xff00fb00)).toEqual("#00fb00");
    expect(Color.toHexRGB(0xff0000fc)).toEqual("#0000fc");
  });

  it('should get opacity', () => {
    expect(Color.opacity(0xfffa0000)).toEqual(1);
    expect(Color.opacity(0x8000fb00)).toEqual(0.5019607843137255);
    expect(Color.opacity(0x000000fc)).toEqual(0);
  });
});
