export default class TestData {
  static cities = {
    warsaw: [52.233333, 21.016667],
    radom: [51.4, 21.166667],
    auckland: [-36.840556, 174.74],
    sanFransisco: [37.783333, -122.416667],
    tokyo: [35.683333, 139.683333],
  }

  static getFullEgo() {
    return {
      setLocation: true,
      lat: TestData.cities.radom[0],
      lng: TestData.cities.radom[1],
      health: 100,
      mana: 100,
      maxHealth: 100,
      maxMana: 100,
      inventory: [ 
        {
          name: 'item_daisyflower',
          count: 2
        },
        {
          name: 'item_fernflower',
          count: 1
        },
        {
          name: 'potion_health'
        },
        {
          name: 'potion_mana'
        },
      ],
      sightRange: 300,
      underEffectOfSpell: [],
      /** ilosc pkt doswiadczenia */
      experience: 0,
      /** obecny level */
      expLevel: 1,
      /** factor do regeneracji mana */
      manaRegenFactor: 2,
      /** czy juz zginal kiedys */
      killedOnce: false,
      /** czy ukonczyl pierwsza walke */
      firstFightDone: false,

      knownFightSpells: {"spell_physical": true},
      knownGlobalSpells: {"spell_oob": true},
      knowledge: { // a.k.a GameContent
        isAlchemyActive: false,
        knownCreatures: { },
        knownPlants: {'item_daisyflower': true},
        knownPotions: {'potion_health': true},
        generateManaZones: false,
      }
    };
  }
}
