import MapController from '../MapController.js';

export default class FakeMapController {

  constructor() {
    this.handler = null;
  }

  init() {
    if(MapController.inst != null) {
      throw "MapController was already initialized";
    }
    MapController.inst = this;
  }

  addClickHandler(handler) {
    this.handler = handler;
    return () => { this.handler = null; };
  }
  
  displayObj() { }

  dontDisplayObj() { }

  update() { }
};
