import React, { Component } from 'react';
import logo from './logo.svg';
import * as firebase from 'firebase';
import $log from './utils/log.js';
import GameController from './main.js';
import PlayerShortStats from './ui/PlayerShortStats.js';
import InventoryScreen from './ui/InventoryScreen.js';
import ItemDetailsScreen from './ui/ItemDetailsScreen.js';
import AlchemyScreen from './ui/AlchemyScreen.js';
import SpellsScreen from './ui/SpellsScreen.js';
import StatsScreen from './ui/StatsScreen.js';
import DebugScreen from './ui/DebugScreen.js';
import FightScreen from './ui/FightScreen.js';
import LoginScreen from './ui/LoginScreen.js';
import LoadingScreen from './ui/LoadingScreen.js';
import QuestsMessageListScreen from './ui/QuestsMessageListScreen.js';
import QuestMessageScreen from './ui/QuestMessageScreen.js';
import AlertItemActivity from './ui/AlertItemActivity.js';
import AlertCreatureActivity from './ui/AlertCreatureActivity.js';
import CreatureShortStats from './ui/CreatureShortStats.js';
import GenericAlert from './ui/GenericAlert.js';
import MWToast from './ui/MWToast.js';
import Game from './Game.js';
import GameSaver from './utils/GameSaver.js';
import EffectsContainer from './Effects.js';
import MainLoop from './gamelogic/MainLoop.js';
import GeoPoint from './gis/GeoPoint.js';
import SimpleMap from './ui/SimpleMap.js';
import MapController from './MapController.js';
import CodesController from './misc/CodesController.js';
import SpellFactory from './gamelogic/SpellFactory.js';
import Inventory from './gamelogic/Inventory.js';
import Player from './gamelogic/creatures/Player.js'
import ScreenOrientation from './utils/ScreenOrientation.js';
import CookieButton from './ui/CookieButton.js';
import R from './R.js';
import Config from './Config.js';
import EventSytem from './gamelogic/events/EventSystem.js';
import EventTypes from './gamelogic/events/EventTypes.js';

import './App.css';
import './Fonts.css';
import './sidenav.css';

var gameController = new GameController();
window.gameController = gameController;

const TAG = "App";

class SideMenu extends Component {
  constructor(props) {
    super(props);
  }

  closeNav() {
    gameController.toggleMenu(false);
  }

  toggleScreen(screen) {
    gameController.showScreen(screen);
    gameController.toggleMenu(false);
  }

  render() {
    const initialStyle = {
      width: (this.props.open ? '250px' : '0px')
    };

    let devBtn = <p />;
    if (this.props.isDev) {
      devBtn = (<a href="#" onClick={() => this.toggleScreen('debug')}>Debug</a>);
    }

    return (
      <div id="mySidenav" className="sidenav" style={initialStyle}>
        <a href="javascript:void(0)" className="closebtn" onClick={this.closeNav}>&times;</a>
        <a href="#" onClick={() => this.toggleScreen('map')}>{R.strings.mainmenu_map}</a>
        <a href="#" onClick={() => this.toggleScreen('quests')}>{R.strings.mainmenu_quests}</a>
        <a href="#" onClick={() => this.toggleScreen('spells')}>{R.strings.mainmenu_spells}</a>
        <a href="#" onClick={() => this.toggleScreen('inventory')}>{R.strings.mainmenu_inventory}</a>
        <a href="#" onClick={() => this.toggleScreen('stats')}>{R.strings.mainmenu_stat}</a>
        {devBtn}
        <a href="#" onClick={() => this.props.onLogout()}>Logout</a>
    </div>
    );
  }
}

class NavBar extends Component {
  constructor(props) {
    super(props);
  }

  openNav() {
    gameController.toggleMenu(true);
  }

  render() {
    return (
      <div className="navbar" onClick={this.openNav}>
        <span>&#9776;</span>
        <span className="menuText">Menu</span>
      </div>)
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    gameController.dispatch = (s) => {
      this.setState(s);
    };
    this.state = gameController.defaultState();
    this.mainLoop = new MainLoop(gameController);
  }

  updateDimensions() {
    this.setState(s => ({...s, ui: {...s.ui, width: window.innerWidth, height: window.innerHeight} }));
  }

  componentWillMount() {
    this.updateDimensions();
  }

  componentDidMount() {
    MWToast.init('mwtoast');
    window.addEventListener("resize", () => this.updateDimensions());

    var config = Config.STAGING;
    firebase.initializeApp(config);

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        gameController.signIn(user);
        gameController.showOverlay('loading');
        downloadUserState(user)
          .then(user => this.mainLoop.startGameLoop())
          .then(() => this.startTracking())
          .then(() => gameController.showOverlay(null))
          .then(() => this.showDisclaimerIfNecessary());
      }
    });

    firebase.auth().getRedirectResult().then((result) => {
    });

    window.addEventListener("hashchange", e => this.hashChanged(e));
  }

  componentWillUnmount() {
    // FIXME: updateDimensions was bound
    window.removeEventListener("resize", this.updateDimensions);
  }

  showDisclaimerIfNecessary() {
    return gameController.applyToState(
      s => {
        if (s.options.TERMS_OF_USE_ACCEPTED) {
          return s;
        } else {
          return Game.getGame().showDiscalimer(s);
        }
      }
    );
  }

  startTracking() {
    if (this.state.playerEgo.isDev) {
      window.isDev = true;
    }
    if (this.state.options.LOCK_ORIENTATION) {
      ScreenOrientation.lock(window.screen);
    }
    if (!navigator.geolocation) {
      console.log('no geo localization');
    } else {
      this.setupGeoWatch();
    }
    let gameState = Game.State.STATE_WALKING;
    if (this.state.gameState == Game.State.STATE_FIGHTING) {
      // TODO: gracz uciekl z walki
    } else if (this.state.gameState == Game.State.STATE_PLAYERDEAD) {
      gameState = Game.State.STATE_PLAYERDEAD;
    }

    return gameController.applyToState(
      s => ({...s, gameState})
    );
  }

  setupGeoWatch() {
    if (this.geoWatchId) {
      $log.e(TAG, "No GEO tracker already set");
      return;
    }
    const options = {
      enableHighAccuracy: !this.state.options.DISABLE_PRECISION_LOCATION,
    };
    this.geoWatchId = navigator.geolocation.watchPosition(
      updatePosition,
      onLocationError,
      options);
  }

  stopTracking() {
    if (!this.geoWatchId && navigator.geolocation) {
      $log.e(TAG, "No GEO tracker but navigation avialable");
      return;
    }
    if (!navigator.geolocation) {
      navigator.geolocation.clearWatch(this.geoWatchId);
    }
  }

  isScreenShown(screen) {
    return (this.state.ui.activeScreen == screen) ? { } : {display: 'none'};
  }

  isOverlayShown(overlay) {
    const extra = (this.state.ui.activeOverlay == overlay) ? 'overlayShown' : 'overlayHidden';
    return `overlayScreen ${extra}`;
  }

  hashChanged(e) {
    gameController.applyToState(s => {
      const hsh = window.location.hash;
      if (hsh.startsWith('#screen=')) {
        const screenParam = hsh.replace('#screen=', '');
        const [screen, param] = screenParam.split('/');
        if (screen == 'inventory') {
          return {
            ...s,
            ui: { ...s.ui, activeScreen:screen, inventoryListFilter:param }
          };
        }
        if (screen == 'spells') {
          return {
            ...s,
            ui: { ...s.ui, activeScreen:screen, spellListFilter:param }
          };
        }
        return gameController.showScreenState(s, screen);
      } else if (hsh.startsWith('#message/')) {  
        const message = hsh.replace('#message/', '');
        return EventSytem.INSTANCE.notifyListenersState(
          s, null, EventTypes.MESSAGE, {message})
      } else {
        if (s.playerEgo.isDev) {
          return CodesController.parseCode(s, hsh);
        } else {
          return s;
        }
      }
    });
  }

  logout() {
    firebase.auth().signOut().then(() => {
      gameController.showScreen('login');
      gameController.toggleMenu(false);
      gameController.applyToState(s => gameController.defaultState());
      this.mainLoop.stopGameLoop();
    });
  }

  render() {
    const overlayStyle = { };
    const mainStyle = { };
    if (!this.state.ui.activeOverlay) {
      overlayStyle.display = 'none';
    } else {
      mainStyle.filter = 'blur(4px)';
    }
    const isShowingNarration = 
        this.state.ui.activeScreen == 'questMessage' &&
        this.state.ui.questMessage.isNarration;
    let topBar = <div />;
    if (!this.state.loginData.isLogged || isShowingNarration) {
      topBar = <div />;
    } else if (this.state.gameState != Game.State.STATE_FIGHTING) {
      topBar = (
        <div className="topContainer">
          <SideMenu 
              open={this.state.ui.menuOpen} 
              closeFunc={this.closeNav}
              isDev={this.state.playerEgo.isDev}
              onLogout={() => this.logout()}
          />
          <div className="leftTopContainer">
            <NavBar openFunc={this.openNav}/>
          </div>
          <div className="rightTopContainer">
            <PlayerShortStats 
              loginData={this.state.loginData}
              playerEgo={this.state.playerEgo} 
              isExpanded={this.state.ui.width > 375}
            />
          </div>
        </div>
      );
    } else {
      topBar = (
        <div className="topContainer">
          <div className="leftTopContainer">
            <CreatureShortStats 
                creature={this.state.ui.fightCreature} 
                isExpanded={this.state.ui.width > 480}/>
          </div>
          <div className="rightTopContainer">
            <PlayerShortStats 
                loginData={this.state.loginData}
                playerEgo={this.state.playerEgo} 
                isExpanded={this.state.ui.width > 420} />
          </div>
        </div>
      )
    }

    let onUseInv = null;
    if (this.state.ui.inventoryDetailsItem) {
      const itemID = this.state.ui.inventoryDetailsItem.getID();
      if (itemID.startsWith('potion_')) {
        onUseInv = () => {
          gameController.applyToState(s => {
            s = gameController.showScreenState(s, 'map');
            const spell = SpellFactory.getSpellByID(itemID, Player.INSTANCE);
            const cast = spell.castThis(s);
            s = cast.state;
            s = {
              ...s,
              playerEgo: {
                ...s.playerEgo,
                inventory: Inventory.get().removeItem(s.playerEgo.inventory, itemID),
              }
            };
            GameSaver.saveGame(s);
            return s;
          });
        };
      }
    }
//          
    return (
      <div>
        <div className="mainContainer" style={mainStyle}>
          {topBar}
          <div className="screen" style={this.isScreenShown('login')}>
            <LoginScreen />
          </div>
          <div className="screen" style={this.isScreenShown('map')}>
            <SimpleMap state={this.state}/>
          </div>
          <div className="screen" style={this.isScreenShown('quests')}>
            <QuestsMessageListScreen state={this.state}/>
          </div>
          <div className="screen" style={this.isScreenShown('questMessage')}>
            <QuestMessageScreen quest={this.state.ui.questMessage} state={this.state}/>
          </div>
          <div className="screen" style={this.isScreenShown('inventory')}>
            <InventoryScreen inventory={this.state.playerEgo.inventory}
                gameState={this.state.gameState}
                filterBy={this.state.ui.inventoryListFilter}
                isAlchemyActive={this.state.playerEgo.knowledge.isAlchemyActive}
            />
          </div>
          <div className="screen" style={this.isScreenShown('stats_details')}>
            <ItemDetailsScreen item={this.state.ui.statsDetailsItem}
                backTo="stats"
            />
          </div>
          <div className="screen" style={this.isScreenShown('inventory_details')}>
            <ItemDetailsScreen item={this.state.ui.inventoryDetailsItem}
                backTo="inventory"
                onUse={onUseInv}
            />
          </div>
          <div className="screen" style={this.isScreenShown('spell_details')}>
            <ItemDetailsScreen item={this.state.ui.spellDetailsItem}
                backTo="spells"
                onUse={() => Game.getGame().useItem()}
            />
          </div>
          <div className="screen" style={this.isScreenShown('alchemy')}>
            <AlchemyScreen playerEgo={this.state.playerEgo} />
          </div>
          <div className="screen" style={this.isScreenShown('spells')}>
            <SpellsScreen playerEgo={this.state.playerEgo} filterBy={this.state.ui.spellListFilter} />
          </div>
          <div className="screen" style={this.isScreenShown('stats')}>
            <StatsScreen state={this.state}/>
          </div>
          <div className="screen" style={this.isScreenShown('debug')}>
            <DebugScreen state={this.state} />
          </div>
          <div className="screen" style={this.isScreenShown('fight')}>
            <FightScreen creature={this.state.ui.fightCreature}
                playerEgo={this.state.playerEgo}
                fight={this.state.ui.fight}
            />
          </div>
        </div>
        <div id="mwtoast"></div>
        <div id="overlayContainer">
          <div className="overlay" style={overlayStyle}>
          </div>
          <div className={this.isOverlayShown('loading')}>
             <LoadingScreen />
          </div>
          <div className={this.isOverlayShown('alert_item')}>
             <AlertItemActivity collectItem={this.state.ui.alertItem} />
          </div>
          <div className={this.isOverlayShown('alert_creature')}>
             <AlertCreatureActivity creature={this.state.ui.fightCreature} />
          </div>
          <div className={this.isOverlayShown('alert_generic')}>
            <GenericAlert params={this.state.ui.genericOverlayParams} />
          </div>
          <EffectsContainer />
        </div>
        <CookieButton />
      </div>
    );
  }
}

var ref;
var downloadUserState = function(user) {
  return new Promise((resolve, reject) => {
    ref = firebase.database().ref('users/' + user.uid);
    gameController.ref = ref;
    ref.once("value", function(snapshot) {
      if (!snapshot.exists()) {
        $log.i(TAG, "Setting default player - 1st run");
        var state = gameController.defaultState();
        state.gameState = Game.State.STATE_WALKING;
        Game.saveGame(state);
        MWToast.toast(`Welcome ${user.displayName}`);
      } else {
        $log.i(TAG, "Loaded snapshot: ");
        $log.i(TAG, snapshot.val());
        gameController.applyToState(
          s => {
            let newState = gameController.merge(s, snapshot.val());
            newState.playerEgo.setLocation = false;
            return Game.reloadGame(newState, gameController);
          }
        );
        MWToast.toast(`Welcome back ${user.displayName}`);
      }
      resolve(user);
    });
  });
}

var updatePosition = function(position) {
  const gp = new GeoPoint(position.coords.latitude, position.coords.longitude);
  gameController.applyToState(
    s => Game.getGame().locationChanged(s, gp)
  );
}

var onLocationError = function(error) {
  MWToast.toast('Could not determine geolocation', {type: "ERROR"});
}

export default App;
