
import React, { Component } from 'react';
import res_profile_william from "./assets/quest/profile_william.jpg";
import res_logo_bf_s from "./assets/quest/logo_bf_s.png";
import res_ilustration_5 from "./assets/quest/ilustration_5.png";
import res_narration_initial_potion_effect from "./assets/quest/narration_initial_potion_effect.jpg";
import res_profile_anette from "./assets/quest/profile_anette.jpg";
import res_ilustration_12 from "./assets/quest/ilustration_12.png";
import res_collectplants_narration_use_telekinesis from "./assets/quest/collectplants_narration_use_telekinesis.jpg";
import res_profile_anthony from "./assets/quest/profile_anthony.jpg";
import res_ilustration_13_v2 from "./assets/quest/ilustration_13_v2.png";
import res_ilustration_7 from "./assets/quest/ilustration_7.png";
import res_collectplants_narration_close_to_plant from "./assets/quest/collectplants_narration_close_to_plant.jpg";
import res_fleeing_rabbit_narration_close_to_rabbit from "./assets/quest/fleeing_rabbit_narration_close_to_rabbit.jpg";
import './QR.css';

export default class QR {
  static drawables = {
    ilustration_5: res_ilustration_5,
    ilustration_7: res_ilustration_7,
    ilustration_12: res_ilustration_12,
    ilustration_13_v2: res_ilustration_13_v2,
    collectplants_narration_use_telekinesis: res_collectplants_narration_use_telekinesis,
    profile_william: res_profile_william,
    logo_bf_s: res_logo_bf_s,
    narration_initial_potion_effect: res_narration_initial_potion_effect,
    collectplants_narration_close_to_plant: res_collectplants_narration_close_to_plant,
    fleeing_rabbit_narration_close_to_rabbit: res_fleeing_rabbit_narration_close_to_rabbit,
    profile_anette: res_profile_anette,
    profile_anthony: res_profile_anthony,
  }

  static strings = {
    welcome_message: (<div>
      Modern Wizards is a mobile Role-Playing Game to be played by GPS, 
      in Your neighborhood. Discover the parallel world of Magic, 
      find out the origin of fairy tales and superstitions, 
      learn to use powerful spells. Become the Modern Wizard!
    </div>),

    quest_profile_bf: (<img src={res_logo_bf_s} className="qrMsgImg" />),
    quest_profile_william: (<img src={res_profile_william} className="qrMsgImg" />), 
    quest_profile_anette: (<img src={res_profile_anette} className="qrMsgImg" />), 
    quest_profile_anthony: (<img src={res_profile_anthony} className="qrMsgImg" />),         
    quest_profile_unknown: (<span />),

    // Initial quest
    quests_initial_message_title: 'Welcome to Modern Wizards!',
    quests_initial_message_from: 'Bayery Farmaceutics',
    quests_initial_message_content: (
    <div>
      Dear Tester,
      <br/>
      <br/>On behalf of the Bayery Farmaceutics ltd. we would like to inform that your application for the Modern Wizards project was approved. In the forthcoming tests you and other volunteers are going to complete tasks, while being under the influence of one of our new medications. These tasks are going to take place outdoors and can be fulfilled in your free time, without disturbing your daily routine. However, you are asked to make notes conscientiously and report every observation you make.
      <br/>In the received <a href="#screen=inventory/potions">equipment</a> you will find a vail, effect of which we want to analyse. Drink it as soon as you are ready to join in the tests.*
      <br/>
      <br/>We wish You good luck,
      <br/>Bayery Farmaceutics
      <br/>
      <br/>*Caution! To let the medicine work properly you must be outdoors.
    </div>),
    narration_initial_potion_effect: (
    <div>
      <img src={res_narration_initial_potion_effect} width="100%" />
      The bitter taste of the drug makes you sick at first. But after the last sip, the nausea is gone and it turns into a warm sensation. Pulsating waves fills your body up from the solar plexus to the fingertips, toes and the crown. It's accompanied by a pleasant tingle that seems to grow stronger with every second.
      <br/>
      <br/>Suddenly your body is pierced with a spasm of pain. Your legs bend and your body crawls up, but you still stand straight, looking down on yourself. The surrounding world changed miraculously. The colors got more intense, some unusual plants grew up in the cracks and leaks of walls and pavements and the people became transparent, as if they were non-physical. Somewhere You can see shadows moving without objects that could cast them 
      <br/>
      <br/>After a moment you get sucked back into your body. You get up, open your eyes and conclude that everything's alright. But is it...?
    </div>),

    // Go to point quest
    gotopoint_quest_title: 'Bilocation Test',
    gotopoint_quest_from: 'dr W. Alpine, BF',
    gotopoint_quest_content: (
    <div>
      Welcome,
      <br/>
      <br/>My name is William Alpine and I am a member of the Bayery Farmaceutics Research Section. I will supervise your actions during the tests, and it is me who you will be getting tasks and instructions from.
      <br/>I will go straight to the point. I marked <a href="#screen=map">four places</a> in your direct neighborhood. Go to any of them now please, as it will help us make the last calibrations of the communicator.
      <br/>
      <br/>I will get in touch with you as soon as you are there.
      <br/>
      <br/>Best regards,
      <br/>dr W. Alpine
    </div>),
    gotopoint_quest_narration_1: (
      <div>
        Perfect. The signal is strong, we are following you.
        <br/>
        <br/>Now we're going to do something much more interesting than walking in the neighbourhood. We assigned you <a href="#screen=map">another place</a> to reach. But this time it's not your body that has to get there, but your soul.
        <br/>Previous investigation show that our medication activates skill called by us as <a href="#screen=spells/global">bilocation</a>. Focus, take a deep breath and... leave your body.
        <br/>
        <br/>Good luck,
        <br/>dr W. Alpine
      </div>),
    gotopoint_quest_narration_2: (
      <div>
        Congratulations, you did it! Over time the exteriorisation will become quite natural for you. It happens to be exhausting, but use it whenever you have any problem with reaching the target.
        <br/>
        <br/>Now take a rest and grow in strength. Soon you will get a message concerning the next task.
        <br/>
        <br/>Best regards,
        <br/>dr W. Alpine
      </div>),
    gotopoint_quest_have_to_use_oob: 'Use "Bilocation", to get here.',  

    // Collect plant quest
    collectplants_quest_title: 'Superception Test',
    collectplants_quest_from: 'dr W. Alpine, BF ', 
    collectplants_quest_content: (
    <div>
      Hello,
      <br/>
      <br/>as I've said, we have new instructions for you – we want to investigate the borders of superception created in your body by our medicine. Look around in the <a href="#screen=map">assigned places</a> in search of the plants with special capacities. I am sending you <a href="#screen=stats">dataset</a>, that should make the identification easier.
      <br/>Collect some samples and wait for further instructions.
      <br/>
      <br/>Best regards,
      <br/>dr W. Alpine
    </div>),
    collectplants_narration_close_to_plant: (
    <div>
      <img src={res_collectplants_narration_close_to_plant} width="100%" />
      As you're getting closer to the designated place, you're feeling it's presence stronger and stronger. You notice that you've been feeling it all this time, but now you know where the source of this feeling is.
      <br/>When the plant is within your sight, you see that the bright aureola and deep colors of the magical flora contrast curiously with pale surroundings. The plant itself looks somehow blurry and transparent.
    </div>),
    collectplants_narration_use_telekinesis: (
    <div>
      <img src={res_collectplants_narration_use_telekinesis} width="100%" />
      This time, when you're reaching for the plant, it somehow jumps into your hands. Maybe it would be possible to do the same thing on a longer distance...?
      <br/>
      <br/><a href="#screen=spells/global">[you've learnt Telekinesis]</a>
    </div>),
    collectplants_narration_all_collected: (
    <div>
      It seems like you've gathered all the samples. Bring them to <a href="#screen=map">this place</a> and give to the BF representative that You will meet there.
      <br/>
      <br/>Regards,
      <br/>dr W. Alpine
    </div>),
    collectplants_narration_end: (
    <div>
      Great, I've just got the information that all the ingredients were provided. Thank you in the behalf of BF crew.
      <br/>
      <br/>Regards,
      <br/>dr W. Alpine
    </div>),

    // Mana source quest
    quest_mana_sources_title: 'Energize Test',
    quest_mana_sources_from: 'Annette Renault, BF',
    quest_mana_sources_initial: (
    <div>
      Hi,
      <br/>
      <br/>I'm Anne Renault form the Paranormal Medicine Department. My boss asked me to delegate you a little, unofficial mission, going a bit beyond the scope of the standard tests procedure.
      <br/>You see, every time you use magical power, it costs you energy that you can quickly recover in so called Mana Sources. They are located in, for example, the intersections of water veins. I marked one of those places on your <a href="#screen=map">map</a>. Now visit the Source and stay there until your organism adjusts to the Mana flows and you fully recover.
      <br/>
      <br/>Cheers,
      <br/>Anne
    </div>),
    quest_mana_sources_narration_mana_full: (
    <div>
      You should be able to find nearby Mana Sources on your own, now that you know them.
      <br/>Use them frequently, otherwise full regeneration will take few hours.
      <br/>
      <br/>Cheers,
      Anne
    </div>),

    // Fleeing rabbit
    fleeing_rabbit_quest_title: 'Red Alert',
    fleeing_rabbit_quest_from: 'dr W. Alpine, BF ',
    fleeing_rabbit_quest_content: (
    <div>
      Some unexpected complications appeared. One of the experimental objects escaped from the laboratory. We know that you developed telekinetic skills that can be very useful in this situation, and moreover you are the closest agent.
      <a href="#screen=map">Here is the place</a>, where the animal was seen last time. Try to find and neutralize it.
      <br/>
      <br/>We count on you!
    </div>),
    fleeing_rabbit_narration_close_to_rabbit: (
    <div>
      <img src={res_fleeing_rabbit_narration_close_to_rabbit} width="100%" />
      Whilst getting closer to the check point, you're beginning to feel slight anxiety. A shiver goes down your spine, and soon you get goosebumps on your shoulders. 
      <br/>
      <br/>The creature has to be very, very close...
    </div>),
    fleeing_rabbit_narration_rabbit_killed: (
    <div>
      <img src={res_ilustration_5} width="100%" /> 
      <br/>When the mutated rabbit falls down dead, you notice that it's surrounded by the same luminous aureola as around the plants you've found before. Is it possible that it also has some special powers?
      <br/>Even though the area seems to be finally safe, you can't help the weird sensation that something unusual is still nearby. Something that you haven't sensed before.
      <br/>It seems that the creature you've just killed wasn't the only one of it's kind...
    </div>),
    fleeing_rabbit_quest_end: (
    <div>
      Good job! We were afraid that the beast will scamper off and sow fear like a chupacabra or something! You will get some time off – at least that's what we can do now to pay you back..
      <br/>
      <br/>I have to remind you that all the events related to the tests are confidential. This incident upset our principals considerably.
      <br/>
      <br/>Regards,
      <br/>dr. W. Alpine
    </div>),

    // Alchemy quest
    quest_alchemy_initial_title: 'Alchemy Test',
    quest_alchemy_initial_from: 'Annette Renault, BF',
    quest_alchemy_initial: (
    <div>
      Hello,
      <br/>
      <br/>You got in the neck a little, didn't you? So I have a piece of advice for you:
      <br/>Take any three parts of astrally active Allium, Chamomile or Fern Flower, pulverize them, mix and then brew it like a tea.
      <br/>The brew should help to heal the wounds made by this poor creature.
      <br/>
      <br/>Cheers,
      <br/>Anne
    </div>),

    // Teletransportion quest
    quest_teletransport_title: 'Teletransportation',
    quest_teletransport_from: 'dr W. Alpine, BF',
    quest_teletransport_initial: (
    <div>
      Hello,
      <br/>
      <br/>we've been recently experiencing some problems with the transport of a medication between branches of the company and we'd like to use your skills in this area. The messengers' reports are not clear, they mention "flying abomination" and "overwhelming helplessness".  Brace yourself.
      <br/>One of the packages was abandoned today – I am sending you it's <a href="#screen=map">aproximate coordinates</a>. Find it and deliver to our laboratory.
      <br/>
      <br/>Take care,
      <br/>dr W. Alpine
    </div>),
    quest_teletransport_narration_package_received: (
    <div>
      We have noticed that you took over the package. Now go to <a href="#screen=map">destination point</a>, where you will give the package to the representative of the Chemistry Department.
    </div>),
    quest_teletransport_narration_end: (
    <div>
      I will hand over your remarks to the people in charge, I'm not sure however if they are going take it seriously. It's not that I don't believe you, but you have to admit it sounds a bit... unbelievable.
      <br/>
      <br/>Regards,
      <br/>dr W. Alpine
    </div>),

    // Special mission - seals
    quest_seals_title: 'Special Task',
    quest_seals_from: 'Anette Ranault, BF',
    quest_seals_initial: (
    <div>
      Hello again,
      <br/>
      <br/>The next task I have for you comes from Mr Dee himself, so listen carefully... 
      <br/>Not very far from your position we have detected an object of significant astral singature.   
      <br/>Try to get this artifact and wait for further instructions.
      <br/>
      <br/>Ah and I need to tell you: that object moves sometimes. 
      <br/>Good luck,
      <br/>Anne
    </div>),
    
    quest_seals_narration_close_gnome: (
    <div>
      Looking around you can't see anything unusual. Suddenly you notice a movement near the ground. A little, very ugly man comes out from a manhole. He has a pointed hat and huge feet. In his just as huge hand he's holding some kind of a spear.  He doesn't pay attention to you.
      <br/><img src={res_ilustration_7} width="100%" /> 
      <br/>You notice however that he has a tangled wire amulet on his neck, sparkling with a magical light. In the very same moment the gnome turns his face to you and asks surprised with a squeaky voice: 
      <br/>
      <br/>
      <i>-Yu cen see mee?.</i>
      <br/>
      <br/>
      <a href='#message/fight'>FIGHT</a>
      <br/>
      <br/>
      <a href='#message/talk'>TALK</a>
      <br/>
    </div>),
    
    quest_seals_gnome_before_first_question: (
    <div>
      <i>-Interesting, indeed. You're big, you are and though only little children can see me. And even that rarely happens, it does.</i>
      <br/>
      <br/>The gnome notices that you're looking at the amulet on his neck. And he's looking at what you're holding in your hand.
      <br/>
      <br/>-Oh, you like my shiny-shiny stuff, huh? And I myself like this sparkling gizmo you're holding there. We can bet it, can't we, huh? We can play cha-rades. Do you like charades? Do you? If you guess right, you'll get the spangle, if not, I'm taking yours. Ahem-ahem. So tell me:
    </div>),
    
    quest_seals_gnome_question_1: (
    <div>
      <br />  
      <br />  Brothers and sisters have I none,
      <br />  but that boy's father is my father's son. 
      <br />  Who is he?
      <br />  
    </div>),

    quest_seals_gnome_question_1_answers: [
      (<a href='#message/good_answer_1'>Your son</a>),
      (<a href='#message/wrong_answer_1'>Your nephew</a>),
      (<a href='#message/wrong_answer_1'>Yourself</a>),
    ],

    quest_seals_gnome_question_2: (
    <div>
      <br />  Giant and lights,
      <br />  Only seen at nights
      <br />  What is it?
      <br />  
    </div>),

    quest_seals_gnome_question_2_answers: [
      (<a href='#message/good_answer_2'>A star</a>),
      (<a href='#message/wrong_answer_2'>The moon</a>),
      (<a href='#message/wrong_answer_2'>An explosion</a>),
    ],
    
    quest_seals_gnome_question_3: (
    <div>
      <br />  I cannot be weighed, touched, or held,
      <br />  I have no color and cannot be smelled
      <br />  I am large enough to sink a ship,
      <br />  If you put me in a cup you'll have less to sip
      <br />  What am I?<br />  
    </div>),

    quest_seals_gnome_question_3_answers: [
      (<a href='#message/good_answer_3'>a hole</a>),
      (<a href='#message/wrong_answer_3'>air</a>),
      (<a href='#message/wrong_answer_3'>an imp</a>),
    ],
  
    quest_seals_gnome_first_question_correct: (
    <div>
      When You answered correctly, the dwarf scratched his head and said:
      <br />  
      <br />  <i>-Oouu, good you are. You won't be so smart with this one:</i>
      <br />  
    </div>),

    quest_seals_gnome_second_question_correct: (
    <div>
      <i>-Uuuooouuggghhh. Arlright, you got this one, but try this!:</i>
      <br />  
    </div>),

    quest_seals_gnome_all_questions_correct: (
    <div>
      <br />  <i>-AAARRGGHH! Bastard!</i>
      <br />  
      <br />  The gnome, apparently irritated, starts to hop. He takes off the amulet and throws it down to your feet.
      <br />  
      <br />  <i>-Damn you! Go away and don't come back or I'll kick your ass!</i>
      <br />  
    </div>),

    // Special mission - main
    quest_special_mission_title: 'Special Task, cont.',
    quest_special_mission_from: 'Anette Ranault, BF',
    quest_special_mission_initial: (
    <div>
      Did you get the Artifact back? Splendid! 
      <br/>We have determined that the artifact corresponds to a certain place, in which we assume it might show its properties. While you get there, be very careful – I have no idea what can happen to you there..
      <br/>
      <br/>I'm keeping my fingers crossed for you,
      <br/>Anne
    </div>),
    
    quest_special_mission_narration_in_first_zone: (
    <div>
      When you're getting closer to the place indicated by Anne you notice a huge stone. The rune rock starts to tremble and shine with a bluish light as You approach it. You touch the rock's surface and then, a hidden door opens by itself. 
      <br /><img src={res_ilustration_12} width="100%"  /> 
      <br/>You take the spiral stairs and reach a dusty basement, you look around and, when your eyes get used to the darkness, you notice something – in the middle of the room there is a small urn surrounded by a ring composed by bizarre symbols. Behind it you see a huge human figure, negligently made of clay.
    </div>),
    
    quest_special_mission_narration_final: (
    <div>
      <br />  A few blinky sparks jump on the urn's surface and make you drop it on the floor. It crashes into thousands of pieces. For a moment you expect it to be completely empty, but then…
      <br />  A luminous energy gathers around the pot's pieces. Though you can't feel any wind, they start to twirl in the air. The ashes soar spinning and finally they accumulate in the middle and create a human figure. When the man's face is formed, he smiles to you and say with a strong, yet hoarse voice:
      <br />  
      <img src={res_ilustration_13_v2} width="100%"  /> 
      <br />  <i>-Welcome, my friend. My name is John Dee and I have to thank you for releasing me. I owe you.
      <br />  You've been blessed with a great power. Be careful, as you might not be aware of how powerful forces you are dealing with. For now though, goodbye, I have a work to do.</i>
      <br />  
      <br />  The shadow dissolves in the air and turns into a wind that runs around you. Then it flows away and disappears from your sight just as quickly as it appeared.
    </div>),

    // Final message
    quest_special_mission_security_alert_from: 'Anthony Coldstone, BF',
    quest_special_mission_security_alert_title: 'Violation of safety regulations',
    quest_special_mission_security_alert: (
    <div>
      Tester,
      <br/>
      <br/>we detected that our former lab assistant, Anette Renault, has contacted you recently. You are asked to discontinue every kind of correspondence with that person and report immediately all conversations that took place until now.
      <br/>
      <br/>Anthony Coldstone,
      <br/>BF  Security
    </div>),

    // Thanks message
    thanks_message_from: 'Bayery Development',
    thanks_message_title: 'Post Scriptum',
    thanks_message_content: (
    <div>
      Thank you for participating in the tests!
      <br />  
      <br /> Please help us to improve Modern Wizards by filling 
             out <a href="https://docs.google.com/forms/d/1AyowvV2PYYEMM9QzCe9Qht6tWxlfLxU3uBEkntiDD5A" target="_blank">survey</a>.
      <br />
      <br /> Visit our <a href="http://www.facebook.com/ModernWizardsPL" target="_blank">Facebook profile</a> for news!
      <br />
      <br />  Best Regards,
      <br />  &nbsp; Bayery Development
    </div>),

    // Sidequest: Haunted house
    quest_haunted_house_from: 'der Wundermann',
    quest_haunted_house_title: 'Ghost Busting',
    quest_haunted_house_content: (
    <div>
      Good morning,
      <br />  
      <br />  I heard through the grape vine that you have a wide range of unusual skills at your disposal, that could be useful for me.
      <br />  See, I've recently bought a landmark property and bizarre things happen over there – the door shuts behind me, items move around on their own and sometimes you can hear steps in the empty halls. I thought: if it's not a ghost, I don't know what it is!
      <br />  I would be grateful if you could take a look on my problem in the nearest future. I assure you that this little effort will be very profitable to you.
      <br />  
      <br />  With best regards,
      <br />  der Wundermann
    </div>),
    quest_haunted_house_end: (
    <div>
      Indeed, you're an incredible expert in the area of supernatural powers! I used to do something like this myself, but it was such a long time ago...
      <br />  According to our agreement, I have something for you. Here comes the recipe for an invisibility potion – all you have to do is to bring the portion of the spiritual ectoplasm to boil and add some fern flower pollen and four leafs of one clover. Et voila, as you can say!
      <br />  
      <br />  With best regards,
      <br />  der Wundermann
    </div>),

    // Sidequest: Little Red Riding Hood
    quest_lrrh_from: 'Ms. Warwitch',
    quest_lrrh_title: 'Please help me!',
    quest_lrrh_grandMa_zone_name: 'Ms. Warwitch',
    quest_lrrh_content: (
    <div>
      <br />  Hello young man,
      <br />  
      <br />  we haven't met yet, but you were recommended by a good friend of mine. I am writing to you because, since this damn wolf have snapped me, I am suffering from a terrible infection. And, even though I am an apprentice of mystic arts, just like you, I am too weak to brew myself a medication.
      <br />  Please, pick up an Allium, Swallow Weed and Fern Flower, then prepare the Antidote and bring it to my cottage. In return, I will give you a recipe for another useful mixture.
      <br />  
      <br />  See you my dear,
      <br />  Ms. Warwitch
    </div>),
    quest_lrrh_without_antidote: (
    <div>
      Oh, it is so nice to see you. Do You have my medicine?
    </div>),
    quest_lrrh_end: (
    <div>
      Oh, thank you my child!
      <br />  Without this Antidote worms and flies would have a nice feast over my corpse. What would I do without you, my dear?
      <br />  As a reward for your effort I will tell you in secret a recipe for the Mana Potion: catch one Strumph and mix its essence with swallow weed and four leafs of one clover. Boil the mixture for thirteen minutes  and next let it cool for a few moments. And that's it!
    </div>),
  }
}
