import QuestsEngine from './QuestsEngine.js';
import AllQuests from './AllQuests.js';
import Quest from './Quest.js';
import GameController from '../main.js';

import TestData from '../test/TestData.js';

describe('QuestsEngine', () => {
  window.gameController = new GameController();

  it('should go to next quest', () => {
    let state = {
      playerEgo: TestData.getFullEgo(),
      quests: {
        InitialQuest: Quest.makeDefault(),
      },
      ui: { },
    };
    state = AllQuests.InitialQuest.setCompleted(state);
    expect(state.quests.InitialQuest.progressState).toBe(Quest.ProgressState.COMPLITED);
    expect(state.quests.GoToPointQuest.progressState).toBe(Quest.ProgressState.WAITING);
    expect(state.quests.GoToPointQuest.waitUntil).toBeGreaterThan(0);
  });

  it('should activate waiting quests', () => {
    let state = {
      playerEgo: TestData.getFullEgo(),
      quests: {
        InitialQuest: {
          ...Quest.makeDefault(),
          progressState: Quest.ProgressState.WAITING,
          waitUntil: 1,
        },
      },
      ui: { },
    };
    state = QuestsEngine.INSTANCE.unlockWaitingQuests(state);
    expect(state.quests.InitialQuest.progressState).toBe(Quest.ProgressState.INPROGRESS);
  });

});
