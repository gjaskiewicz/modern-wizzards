import InitialQuest from './storyline/InitialQuest.js';
import GoToPointQuest from './storyline/GoToPointQuest_2.js';
import CollectPlantsQuest from './storyline/CollectPlantsQuest.js';
import ManaSourcesQuest from './storyline/ManaSourcesQuest_2.js';
import FleeingRabbitQuest from './storyline/FleeingRabbitQuest.js';
import AlchemyQuest from './storyline/AlchemyQuest.js';
import TeletransportQuest from './storyline/TeletransportQuest.js';
import SealsQuest from './storyline/SealsQuest_2.js';
import SpecialMissionQuest from './storyline/SpecialMissionQuest_2.js';
import FinalMessage from './storyline/FinalMessage.js';
import ThanksMessage from './storyline/ThanksMessage.js';
// sidequests
import HauntedHouseQuest from './sidequests/HauntedHouseQuest.js';
import LittleRedRidingHoodQuest from './sidequests/LittleRedRidingHoodQuest.js';

const assignNames = (col) => {
  for (let key of Object.keys(col)) {
    col[key].questClass_ = key;
  }
  return col;
};

export default assignNames({
  InitialQuest: new InitialQuest(),
  GoToPointQuest: new GoToPointQuest(),
  CollectPlantsQuest: new CollectPlantsQuest(),
  ManaSourcesQuest: new ManaSourcesQuest(),
  FleeingRabbitQuest: new FleeingRabbitQuest(),
  AlchemyQuest: new AlchemyQuest(),
  TeletransportQuest: new TeletransportQuest(),
  SealsQuest: new SealsQuest(),
  SpecialMissionQuest: new SpecialMissionQuest(),
  FinalMessage: new FinalMessage(),
  ThanksMessage: new ThanksMessage(),
  // sidequests
  HauntedHouseQuest: new HauntedHouseQuest(),
  LittleRedRidingHoodQuest: new LittleRedRidingHoodQuest(),
});
