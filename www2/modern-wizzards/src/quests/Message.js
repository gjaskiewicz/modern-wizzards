export default class Message {
  
  static State = {
    UNREAD: 'UNREAD',
    READ: 'READ',
    REPLIED: 'REPLIED',
    DELETED: 'DELETED'
  };
  
  constructor() {
    this.title_ = "";
    this.from_ = "";
    this.fromimg_ = "";
    this.message_ = "";
    this.state = Message.State.UNREAD;
    this.received = null;
    //dla ci�g�w wiadomo�ci / w�tk�w.
    this.previusMessage = null;
  }
  
  setTitle(title) {
    this.title_ = title;
  }
  
  getTitle() {
    return this.title_;
  }
  
  setFrom(from) {
    this.from_ = from;
  }
  
  getFrom() {
    return this.from_;
  }
  
  getFromImg()  {
    return this.fromimg_;
  }
  
  setFullMessageText(message) {
    this.message_ = message;
  }
  
  getFullMessageText() {
    return this.message_;
  }
  
  openMessage(state) {
    let quest = state.quests[this.questClass_];  // constructor.name
    if(quest.state == Message.State.UNREAD)  {
      const quests = {...state.quests};
      quests[this.questClass_] = {         // constructor.name
        ...quest,
        state: Message.State.READ,
      };

      state = {
        ...state,
        quests
      }

      return this.onMessageOpen(state);     
      // TODO: migrate
      //Intent i = new Intent(Game.Actions.MESSAGE_OPENED).putExtra("className", this.getClass().getSimpleName());
      //ctx_.sendBroadcast(i);
    }
    return state;
  }
  
  onMessageOpen(s) { return s; }
  
  isAlreadyRead(state) {
    const quest = state.quests[this.questClass_]; //  || this.constructor.name
    return quest.state == Message.State.READ;
  }
}
