import PlayerEgo from "../../PlayerEgo.js";
import Inventory from "../../gamelogic/Inventory.js";
import RNG from "../../utils/RNG.js";
import GeoPoint from "../../gis/GeoPoint.js";
import Game from "../../Game.js";
import Quest from "../Quest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Warewolf from "../../gamelogic/creatures/Warewolf.js";
import GameSaver from "../../utils/GameSaver.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";
import TriggerZone from "../../gamelogic/world/TriggerZone.js";
import $log from "../../utils/log.js";

const TAG = "LittleRedRidingHoodQuest";
const grandMaZoneID = "grandMaZone";
const grandMaZoneRadius = 50;

export default class LittleRedRidingHoodQuest extends Quest {

  constructor() {
    super();
    this.timeToNextQuest = 100000; // [ms]
    this.grandMaZone = null;
    this.warewolf = null;
    this.rnd = new RNG();
    this.messageNoAntidotumDisplayed = false;

    this.title_ = QR.strings.quest_lrrh_title;
    this.from_ = QR.strings.quest_lrrh_from;
    this.fromimg_ = R.strings.quest_profile_unknown;

    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
  }

  initateQuest(s) {
    return s;
  }

  restoreQuest(state) {
    let quest = {...state.quests['LittleRedRidingHoodQuest']};

    this.setBasicMessage(QR.strings.quest_lrrh_content);
    this.setFullMessageText(this.getMessageWithNarrations(state));

    if (!quest.completedSteps) {
      if (this.isAlreadyRead(state) && 
          this.getAcceptanceState(state) == Quest.AcceptanceState.ACCEPTED &&
          this.getProgressState(state) == Quest.ProgressState.INPROGRESS) {
        return this.initiateQuestBackground(state);
      }
    }

    return state;
  }

  initiateQuestBackground(s) {
    const gp_player = PlayerEgo.getGeoPoint(s.playerEgo);
    s = {
      ...s,
      playerEgo: {
        ...s.playerEgo,
        knowledge: {
          ...s.playerEgo.knowledge,
          knownPotions: {
            ...s.playerEgo.knowledge.knownPotions,
            'potion_antidote': true,
          },
        },
      }
    };
    
    this.grandMaZone = new TriggerZone(grandMaZoneID,
        new GeoPoint(gp_player.getLatitudeE6(), gp_player.getLongitudeE6()),
        grandMaZoneRadius,
        R.strings.quest_lrrh_grandMa_zone_name);
    this.grandMaZone.moveObject(250.0 + this.rnd.nextInt(100), this.rnd.nextInt(360));

    Game.getGame().worldContainer.addQuestObject(this.grandMaZone, true);
    this.grandMaZone.display(true);

    if (this.warewolf == null) {
      this.warewolf = new Warewolf();
      if (s.playerEgo.isDev) {
        this.warewolf.health = 1;
      }
    }
    this.warewolf.zone.radius = grandMaZoneRadius + 20;
    this.warewolf.setLocation(this.grandMaZone.getGeoPoint());
    Game.getGame().worldContainer.addQuestObject(this.warewolf, false);
    this.warewolf.display(false);

    return s;
  }

  setAccepted(state) {
    $log.d(TAG, "Set assepted");
    state = super.setAccepted(state);
    state = this.applyToQuest(state, q => ({
      ...q,
      progressState: Quest.ProgressState.INPROGRESS,
    }));
    return this.initiateQuestBackground(state);
  }

  handleEvent(state, objID, eventType, extraParams) {
    state = super.handleEvent(state, objID, eventType, extraParams);

    if (this.getAcceptanceState(state) != Quest.AcceptanceState.ACCEPTED
        || this.getProgressState(state) != Quest.ProgressState.INPROGRESS)
      return state;

    if (eventType == EventTypes.VISITED 
        && grandMaZoneID == objID
        && this.warewolf.getHealth(state) <= 0) { 
      // Player reached triggered zone.
      if (Inventory.get().hasItem(state.playerEgo.inventory, "potion_antidote")) {
        state = this.finishQuest(state);
      } else {
        state = this.showNarrationOncePerVisit(state, QR.strings.quest_lrrh_without_antidote, objID);
      }
    }
    return state;
  }

  onMessageOpen(s) { return s; }

  finishQuest(state) {
    if (this.getProgressState(state) == Quest.ProgressState.COMPLITED) {
      return state;
    }

    Game.getGame().worldContainer.removeQuestObject(this.grandMaZone);
    this.grandMaZone.display(false);
    this.warewolf = null;

    const inventory = state.playerEgo.inventory.filter(item => {
      return !item.blocked || item.name != 'potion_antidote'
    });
    state = {
      ...state,
      playerEgo: {
        ...state.playerEgo,
        inventory
      }
    };

    state = this.pushStep(state, {
      id: 1,
      time: (new Date().getTime()),
      narrationTextName: "quest_lrrh_end",
    });

    state = this.showNewInQuestMessage(state, QR.strings.quest_lrrh_end);

    this.setFullMessageText(this.getMessageWithNarrations(state));
    state = {
      ...state,
      playerEgo: {
        ...(PlayerEgo.addExperience(state.playerEgo, 300)),
        knowledge: {
          ...state.playerEgo.knowledge,
          knownPotions: {
            ...state.playerEgo.knowledge.knownPotions,
            'potion_mana': true,
          },
          knownCreatures: {
            ...state.playerEgo.knowledge.knownCreatures,
            'creature_warewolf': true,
          },
        },
      }
    };
    state = this.setCompleted(state);
    GameSaver.saveGame(state);
    return state;
  }
}
