import Quest from "../Quest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import PlayerEgo from "../../PlayerEgo.js";
import GeoPoint from "../../gis/GeoPoint.js";
import RNG from "../../utils/RNG.js";
import TriggerZone from "../../gamelogic/world/TriggerZone.js";
import Game from "../../Game.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";
import Wraith from "../../gamelogic/creatures/Wraith.js";
import $log from "../../utils/log.js";

const TAG = "HauntedHouseQuest";
const hauntedHouseZoneID = "hauntedHouseZone";
const hauntedHouseZoneRadius = 70;

export default class HauntedHouseQuest extends Quest {
  
  constructor() {
    super();
    this.timeToNextQuest = 80000; // [ms]
    this.title_ = QR.strings.quest_haunted_house_title;
    this.from_ = QR.strings.quest_haunted_house_from;
    this.fromimg_ = QR.strings.quest_profile_unknown;

    this.hauntedHouseZone = null;
    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
    EventSystem.INSTANCE.registerListener(EventTypes.KILLED, this);
  }

  restoreQuest(s) {
    let quest = {...s.quests['HauntedHouseQuest']};

    this.setBasicMessage(QR.strings.quest_haunted_house_content);
    this.setFullMessageText(this.getMessageWithNarrations(s));

    if (this.isAlreadyRead(s) && 
        this.getAcceptanceState(s) == Quest.AcceptanceState.ACCEPTED &&
        this.getProgressState(s) == Quest.ProgressState.INPROGRESS) {
      return this.initiateQuestBackground(s);
    }
    return s;
  }
  
  initiateQuestBackground(state) {
    const rnd = new RNG();
    const gp_player = PlayerEgo.getGeoPoint(state.playerEgo);
    
    this.hauntedHouseZone = new TriggerZone(hauntedHouseZoneID, 
        new GeoPoint(
          gp_player.getLatitudeE6(), 
          gp_player.getLongitudeE6() ), 
          hauntedHouseZoneRadius, 
          R.strings.target);
    this.hauntedHouseZone.moveObject(250.0 + rnd.nextInt(100), rnd.nextInt(360));
    
    Game.getGame().worldContainer.addQuestObject(this.hauntedHouseZone, true);
    this.hauntedHouseZone.display(true);

    return state;
  }
  
  setAccepted(state) {
    $log.d(TAG, "Set accepted");
    state = super.setAccepted(state);
    state = this.applyToQuest(state, q => ({
      ...q,
      progressState: Quest.ProgressState.INPROGRESS,
    }));
    return this.initiateQuestBackground(state);
  }
  
  handleEvent(state, objID, eventType, extraParams) {
    state = super.handleEvent(state, objID, eventType, extraParams);

    if (this.getAcceptanceState(state) != Quest.AcceptanceState.ACCEPTED || 
        this.getProgressState(state) != Quest.ProgressState.INPROGRESS) {
      return state;
    }
    
    if (eventType == EventTypes.VISITED && hauntedHouseZoneID == objID) { 
      //Player reached triggered zone.
      this.wraith = new Wraith();
      
      Game.getGame().worldContainer.removeQuestObject(this.hauntedHouseZone);
      this.hauntedHouseZone.display(false);
      
      state = this.pushStep(state, {
        id: 1,
        time: (new Date().getTime()),
        narrationTextName: null,
      });
      state = this.wraithAttacks(state);
      this.hauntedHouseZone = null;
      
    } else if(eventType == EventTypes.KILLED &&
              this.wraith &&
              this.wraith.nameID == objID) { 
      //Player killed wraith
      state = this.finishQuest(state);
    }
    return state;
  }

  onMessageOpen(state) {
    return state;
  }
  
  wraithAttacks(state) {
    this.wraith.zone.radius = hauntedHouseZoneRadius;
    this.wraith.attackWithoutWarning = true;
    this.wraith.setLocation(this.hauntedHouseZone.getGeoPoint());
    this.wraith.display(true);
    Game.getGame().worldContainer.addQuestObject(this.wraith, true);
    return state;
  }
  
  finishQuest(state) {
    const quest = {...state.quests['HauntedHouseQuest']};
    if (this.getProgressState(state) == Quest.ProgressState.COMPLITED) {
      return state;
    }
    //for(PhysicalObject item : Game.getGame().worldContainer.otherObjects)
    //  item.display(false);
    //TODO: to jest kiepski pomys�, mo�esz miec obiekty z quest�w pobocznych tutaj.
    //Game.getGame().worldContainer.otherObjects.clear(); //Hope to have only plants here during this quest
    
    state = this.pushStep(state, {
      id: (quest.completedSteps.length + 1),
      time: (new Date().getTime()),
      narrationTextName: "quest_haunted_house_end",
    });
      
    state = this.showNewInQuestMessage(state, QR.strings.quest_haunted_house_end);
    this.setFullMessageText(this.getMessageWithNarrations(state));

    state = this.setCompleted(state);
    state = {
      ...state,
      playerEgo: PlayerEgo.addExperience(state.playerEgo, 300),
    };
    state = {
      ...state,
      playerEgo: {
        ...state.playerEgo,
        inventory: [...state.playerEgo.inventory, {name: "potion_invisibility"}],
        knowledge: {
          ...state.playerEgo.knowledge,
          knownCreatures: {
            ...state.playerEgo.knowledge.knownCreatures,
            'creature_wraith': true,
          },
        },
      }
    };
    Game.saveGame(state);
    return state;
  }
  
}
