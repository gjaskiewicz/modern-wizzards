import React from 'react';
import Message from './Message.js';
import $log from '../utils/log.js';
import Game from '../Game.js';
import EventSystem from "../gamelogic/events/EventSystem.js";
import EventTypes from "../gamelogic/events/EventTypes.js";
import R from '../R.js';
import QR from '../QR.js';

const TAG = "Quest";

class Step { // implements Comparable<Step> {
  constructor(id, /* Date */ date, /* String */ narrURI) {
    this.stepID = id;
    this.completeDate = date;
    this.narrationTextName = narrURI;
  };
};

export default class Quest extends Message { // implements ChangeListener {

  static AcceptanceState = {
    NONE: 'NONE',
    ACCEPTED: 'ACCEPTED',
    REJECTED: 'REJECTED'
  };

  static ProgressState = {
    NONE: 'NONE',
    WAITING: 'WAITING',
    INPROGRESS: 'INPROGRESS',
    COMPLITED: 'COMPLITED',
    FAILED: 'FAILED'
  };
  
  constructor(title = "", from = "") {
    super();
    this.title_ = title;
    this.from_ = from;
    this.state = Message.State.UNREAD;
    this.questClass_ = this.constructor.name;
    this.narrationsAlreadyShownDuringVisit = { }; // Map<String,Set<Integer>>
    
    // lista wszystkich wiadomosci kt�re w quescie/w�tku wiadomo�ci sie pojawiaj� 
    this.messages = [ ];
    this.completedSteps_ = [ ]; // TreeSet<Step>
    this.stepsToGoal_ = 100;
    this.basicMessage = null;

    this.progressState_ = Quest.ProgressState.NONE;
    this.acceptanceState_ = Quest.AcceptanceState.NONE;

    EventSystem.INSTANCE.registerListener(EventTypes.EXITED, this);
  }

  static makeDefault() {
    return {
      state: Message.State.UNREAD,
      progressState: Quest.ProgressState.NONE,
      acceptanceState: Quest.AcceptanceState.NONE,
      stepsToGoal: 100,
    }
  }
  
  getClassName() { 
    return this.questClass_; 
  }
  
  // Initiate only message. Rest of stuff should be initialized in initiateQuestBackground method
  initiateQuest(state) { return state; }

  restoreQuest(state) { return state; }
  
  //Initiate map objects, set state "in progress", etc.
  initiateQuestBackground(state) { return state; }
  
  applyToQuest(state, transformQuest) {
    const quest = state.quests[this.questClass_];
    const quests = {...state.quests};    
    quests[this.questClass_] = transformQuest(quest);
    return {
      ...state,
      quests,
    };
  }
  
  onMessageOpen(state) {
    state = this.initiateQuestBackground(state);
    return this.applyToQuest(state, q => ({
      ...q,
      progressState: Quest.ProgressState.INPROGRESS,
    }));
  }
  
  /**
   * Some visual parts of quests (i.e. triggered zones on map) couldn't be restored during normal deserialization. 
   * So this method will be called after entire deserialization to prepare visual parts of quest.  
   */
/*  @Override
  public void onRestoreFromSave() {
    $log.d(TAG, "onRestoreFromSave " + this.getClass().getSimpleName());
    Class<?> nextQuestClass = QuestsEngine.nextQuestsCollection.get(this.getClass());
    if(nextQuestClass == null) {
      $log.w(TAG, "No next quest");
      return;
    }
    Quest nextQuest = QuestsEngine.getInstance().getQuestByClass(nextQuestClass);
    if(nextQuest == null && isCompleted() ) {
      $log.d(TAG, "Next quest for quest " + this.getClass().getSimpleName() + " is null");
      initiateNextQuest();
    }
    else if(nextQuest != null && !nextQuest.isCompleted() ) {
      $log.d(TAG, "Next quest for quest " + this.getClass().getSimpleName() + " is not null but is not completed");
      QuestsEngine.getInstance().removeQuestByClass(nextQuestClass);
      initiateNextQuest();
    }
  }*/
  

  
  getAcceptanceState(state) {
    const quest = state.quests[this.questClass_];
    return (quest && quest.acceptanceState) || Quest.AcceptanceState.NONE;
  }
  
  getProgressState(state) {
    const quest = state.quests[this.questClass_];
    return (quest && quest.progressState) || Quest.ProgressState.NONE;
  }
  
  setAccepted(state) {
    $log.d(TAG,"Set accepted");
    return this.applyToQuest(state, q => ({
      ...q,
      acceptanceState: Quest.AcceptanceState.ACCEPTED
    }));
  }
  
  isCompleted() {
    return this.progressState_ == Quest.ProgressState.COMPLITED;
  }
  
  isRejected() {
    return this.acceptanceState_ == Quest.AcceptanceState.REJECTED;
  }

  setRejected(state) {
    state = this.applyToQuest(state, q => ({
      ...q,
      acceptanceState: Quest.AcceptanceState.REJECTED
    }));
    Game.saveGame(state);
    return state;
  }
  
  setCompleted(state) {
    state = this.applyToQuest(state, q => ({
      ...q,
      progressState: Quest.ProgressState.COMPLITED,
    }));
    state = EventSystem.INSTANCE.notifyListenersState(
      state,
      this.questClass_, 
      EventTypes.QUEST_FINISHED);
    return state;
  }
  
  handleEvent(state, objID, eventType, extraParams) {
    if (eventType == EventTypes.EXITED) {
      this.narrationsAlreadyShownDuringVisit[objID] = { };
    }
    return state;
  }
  
  getTitle() {
    let extraState = '';
    switch(this.progressState_) {
      case Quest.ProgressState.INPROGRESS:
        extraState = R.strings.inprogress;
        break;
      case Quest.ProgressState.COMPLITED:
        extraState = R.strings.completed;
        break;
      case Quest.ProgressState.FAILED:
        extraState = R.strings.failed;
        break;
    }
    //zakomentowalem bo na szzegolach brzydko wyglada
    //return title_ + (extraState.length() == 0 ? "" : " (" + extraState + ")");
    return this.title_;
  }
  
  setBasicMessage(message) {
    this.basicMessage = message;
    /*
    if (typeof message === 'string') {
      
    } else {
      // TODO: migrate
      throw "Migrate";
      // setBasicMessage(ctx_.getString(messageId));
    }
    */
  }
  
  getMessageWithNarrations(state) {
    const quest = state.quests[this.questClass_];
    let fullMessage = [ ];
    if (this.basicMessage) {
      fullMessage.push(this.basicMessage);
    }
    const completedSteps = quest.completedSteps || [ ];
    if (completedSteps && completedSteps.length) {
      for (let s of completedSteps) {
        if (s.narrationTextName) {
          $log.d(TAG, "Looking for narration text");
          let narration = QR.strings[s.narrationTextName];
          if (narration) {
            const time = new Date(s.completeDate).toLocaleString();
            $log.d(TAG, "Resource id found");
            fullMessage.push(<br />);
            fullMessage.push(<span>{time}</span>);
            fullMessage.push(<br />);
            fullMessage.push(narration);
          }
        }
      }
    }
        
    return fullMessage;
  }
  
  /**
   * Pokazuje NARRACJE , czyli go�y tekst.
   * @param narrationID id z resurs�w
   */
  /*
  showNarrationMessage(narrationID) {
    const message = R.strings[narrationID];
    //Game.mainActivity.getResources().getString(narrationID);
    //showNarrationMessage(this.message, true);
  }
  */
  
  /**
   * Pokazuje narracj� raz na wizyt� w zonie
   * @param narrationID
   */
  showNarrationOncePerVisit(state, narrationID, zoneID) {
    if(!this.narrationsAlreadyShownDuringVisit[zoneID]) {
      this.narrationsAlreadyShownDuringVisit[zoneID] = { };
    }
    
    if(!this.narrationsAlreadyShownDuringVisit[zoneID][narrationID]) {
      state = this.showNarrationMessage(state, narrationID);
      this.narrationsAlreadyShownDuringVisit[zoneID][narrationID] = true;
    }

    return state;
  }
  
  /*
   * Pokazuje NARRACJE , czyli go�y tekst.
   * @param message tekst do wyswietlenia.
   * @param canClose TODO
   */
  showNarrationMessage(state, message, canClose = true) {
    const questMessage = {
      isNarration: true,
      canClose,
      getFromImg: () => <span />,
      getFullMessageText: () => message,
    };
    return {
      ...state,
      ui: {...state.ui, activeScreen: 'questMessage', questMessage }
    }
  }
  
  /*
  showNewInQuestMessage(state, narration) {
    this.showNewInQuestMessage(state, narration, null, null);
  }
  */
  
  showNewInQuestMessage(state, message, titleParam, fromParam) {
    const title = titleParam || this.getTitle();
    const from = fromParam || this.getFrom();
    
    const q = {
      isNarration: true,
      getFromImg: () => <span />,
      getFullMessageText: () => message,
    };
    return {
      ...state,
      ui: {...state.ui, activeScreen: 'questMessage', questMessage:q }
    };
  }
  
  /*
   * Tries to restore quests's saved state.
   * @return When success then return true
   */
  // TODO: remove
  tryToRestoreQuest() {
    return null;
  }
  
  cleanQuestRemains() {}

  pushStep(state, step) {
    step = {
      ...step,
      completeDate: (new Date().getTime()),
    };
    return this.applyToQuest(state, q => ({
      ...q,
      completedSteps: (q.completedSteps ? [...q.completedSteps, step] : [ step ]),
    }));
  }
  
  /* ??? - not needed?
  getResultSet(PostProcessingRequestType req) {
    HashSet<PostProcessingRequestType> result = new HashSet<PostProcessingRequestType>();
    if (req != null) result.add(req);
    return result;
  }
  
  protected Set<PostProcessingRequestType> getResultSet() {
    return getResultSet(null);
  }
  */
}
