import Quest from "../Quest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Game from "../../Game.js";
import GameSaver from "../../utils/GameSaver.js";
import GameContent from "../../gamelogic/GameContent.js";
import RNG from "../../utils/RNG.js";
import Golem from "../../gamelogic/creatures/Golem.js";
import GeoPoint from "../../gis/GeoPoint.js";
import PlayerEgo from "../../PlayerEgo.js";
import TriggerZone from "../../gamelogic/world/TriggerZone.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";

const TAG = "SpecialMissionQuest";
const golemZoneID = "golemZone"; 
const goalZoneID = "goalZone";

export default class SpecialMissionQuest extends Quest {
  
  constructor() {
    super();
    this.title_ = QR.strings.quest_special_mission_title;
    this.from_ = QR.strings.quest_special_mission_from;
    this.fromimg_ = QR.strings.quest_profile_anette;

    this.golemZone = null;
    this.golem = null;
    this.goalZone = null;
    
    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
  }

  initiateQuest(s) {
    s = this.setAccepted(s);
    return s;
  }

  restoreQuest(s) {
    let quest = {...s.quests['SpecialMissionQuest']};

    this.setBasicMessage(QR.strings.quest_special_mission_initial);    
    this.setFullMessageText(this.getMessageWithNarrations(s));

    if (this.isAlreadyRead(s) && 
        this.getProgressState(s) == Quest.ProgressState.INPROGRESS) {
      return this.initiateQuestBackground(s);
    }

    return s;
  }
  
  initiateQuestBackground(state) { 
    let quest = {...state.quests['SpecialMissionQuest']};
    const rnd = new RNG();
    const gp_player = PlayerEgo.getGeoPoint(state.playerEgo);
    
    this.golemZone = new TriggerZone(golemZoneID, 
        new GeoPoint(
          gp_player.getLatitudeE6(), 
          gp_player.getLongitudeE6() ), 
        70, R.strings.target);
    this.golemZone.moveObject(150.0 + rnd.nextInt(100), rnd.nextInt(360));
    
    if (!quest.completedSteps || !quest.completedSteps.length) {
      Game.getGame().worldContainer.addQuestObject(this.golemZone, true);
      this.golemZone.display(true);
    } else if (quest.completedSteps.length == 1) {
      this.setupGolem(state);
    }

    return state;
  }
  
  handleEvent(state, objID, eventType, extraParams) {
    state = super.handleEvent(state, objID, eventType, extraParams);

    let quest = {...state.quests['SpecialMissionQuest']};
    quest.completedSteps = quest.completedSteps || [ ];

    if (this.getAcceptanceState(state) != Quest.AcceptanceState.ACCEPTED || 
        this.getProgressState(state) != Quest.ProgressState.INPROGRESS) { 
      return state; 
    }
    
    if (eventType == EventTypes.VISITED &&
        golemZoneID == objID) { 
      // Player reached triggered zone. Add golem
      Game.getGame().worldContainer.removeQuestObject(this.golemZone);
      this.golemZone.display(false);
      
      state = this.pushStep(state, {
        id: 1 ,
        time: (new Date().getTime()),
        narrationTextName: null,
      });
      state = this.applyToQuest(state, q => ({...q, golemLocation: (this.golemZone.getGeoPoint()),}));

      this.setupGolem(state);
      state = this.showNarrationMessage(state, QR.strings.quest_special_mission_narration_in_first_zone);
    } else if (      
        eventType == EventTypes.VISITED &&
        goalZoneID == objID && 
        quest.completedSteps.length == 1 &&
          (this.golem.getHealth(state) <= 0 ||
          PlayerEgo.isUnderEffectOfSpell(state.playerEgo, "potion_invisibility"))) {
      state = this.pushStep(state, {
        id: 2,
        time: (new Date().getTime()),
        narrationTextName: null,
      });
      state = this.showNarrationMessage(state, QR.strings.quest_special_mission_narration_final);
      
      Game.getGame().worldContainer.removeQuestObject(this.golem);
      Game.getGame().worldContainer.removeQuestObject(this.goalZone);
      this.golem = null;
      this.goalZone = null;

      //for(PhysicalObject item : Game.getGame().worldContainer.otherObjects)
      //  item.display(false);
      //Game.getGame().worldContainer.otherObjects.clear();
      state = this.setCompleted(state);
      GameSaver.saveGame(state);
    }
    return state;
  }

  setupGolem(state) {
    const quest = {...state.quests['SpecialMissionQuest']};
    const gp = new GeoPoint(quest.golemLocation.lat, quest.golemLocation.lng);
    this.golem = new Golem();
    this.golem.setLocation(gp);
    this.golem.display(true);
    Game.getGame().worldContainer.addQuestObject(this.golem, true);
     
    this.goalZone = new TriggerZone(goalZoneID, 
        gp, 30, R.strings.target);
      
    Game.getGame().worldContainer.addQuestObject(this.goalZone, true);
    this.goalZone.display(true);
  }

  onMessageOpen(state) {
    state = super.onMessageOpen(state);
    /* TODO migrate 
    if(progressState_ == ProgressState.INPROGRESS && 
        completedSteps_.size() == 0) {
      $log.d(TAG, "Message opened sending broadcast");
      Game.turnOnOffNotificationForButton(R.id.btnMap, true);
    }
    */
    return state;
  }
}
