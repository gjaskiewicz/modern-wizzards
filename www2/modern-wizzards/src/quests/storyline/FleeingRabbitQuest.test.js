import FleeingRabbitQuest from './FleeingRabbitQuest.js';
import Quest from '../Quest.js';
import MainLoop from '../../gamelogic/MainLoop.js';
import Game from '../../Game.js';

import TestData from '../../test/TestData.js';
import FakeMapController from '../../test/FakeMapController.js';

describe('FleeingRabbitQuest', () => {
  it('should be possible to complete', () => {
    const mapController = new FakeMapController();
    mapController.init();
    const mainLoop = new MainLoop();
    mainLoop.loopIteration = -1;
    let s = {
      playerEgo: {
        ...TestData.getFullEgo(),
      },
      gameState: Game.State.STATE_WALKING,
      quests: {
        FleeingRabbitQuest: {
          ...Quest.makeDefault(),
        },
      },
      achivments: { 
        granted: [ ],
      },
      worldContainer: {
        gatheredPlants: [ ],
        creaturesDefeated: [ ],
      },
    };

    const q = new FleeingRabbitQuest();
    s = q.initiateQuest(s);
    s = q.onMessageOpen(s);
    expect(!!q.fleeingRabbitCloseZone).toBe(true);
    expect(!!q.fleeingRabbit).toBe(false);

    const gp = q.fleeingRabbitCloseZone.getGeoPoint();

    s = {
      ...s,
      playerEgo: {
        ...s.playerEgo,
        lat: gp.lat,
        lng: gp.lng,
      }
    };

    s = mainLoop.iterateGameState(s);
    expect(!!q.fleeingRabbit).toBe(true);

    // TODO: test fight
  });
});
