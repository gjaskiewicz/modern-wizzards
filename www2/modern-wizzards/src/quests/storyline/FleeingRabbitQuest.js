import Quest from "../Quest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Game from "../../Game.js";
import RNG from "../../utils/RNG.js";
import Bunny from "../../gamelogic/creatures/Bunny.js";
import GeoPoint from "../../gis/GeoPoint.js";
import PlayerEgo from "../../PlayerEgo.js";
import Inventory from "../../gamelogic/Inventory.js";
import $log from "../../utils/log.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";
import TriggerZone from "../../gamelogic/world/TriggerZone.js";

const TAG = "FleeingRabbitQuest";
const fleeingRabbitCloseZoneID = "FleeingRabbitCloseZone";

export default class FleeingRabbitQuest extends Quest {
  
  constructor() {
    super();
    this.title_ = QR.strings.fleeing_rabbit_quest_title;
    this.from_ = QR.strings.fleeing_rabbit_quest_from;
    this.fromimg_ = QR.strings.quest_profile_william;

    this.fleeingRabbitCloseZone = null;
    this.fleeingRabbit = null;

    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
    EventSystem.INSTANCE.registerListener(EventTypes.KILLED, this);
  }

  initiateQuest(s) {
    s = this.setAccepted(s);
    return s;
  }

  restoreQuest(s) {
    this.setBasicMessage(QR.strings.fleeing_rabbit_quest_content);
    this.setFullMessageText(this.getMessageWithNarrations(s));

    if (this.getProgressState(s) != Quest.ProgressState.INPROGRESS) {
      return s;
    } else {
      if (this.isAlreadyRead(s)) {
        return this.initiateQuestBackground(s);
      }
      return s;
    }

    return s;
  }
  
  initiateQuestBackground(state) { 
    const rnd = new RNG();
    const gp_player = PlayerEgo.getGeoPoint(state.playerEgo);
    
    this.fleeingRabbitCloseZone = new TriggerZone(fleeingRabbitCloseZoneID, 
        new GeoPoint(
          gp_player.getLatitudeE6(), gp_player.getLongitudeE6()), 
        90,
        R.strings.target);
    this.fleeingRabbitCloseZone.moveObject(150.0 + rnd.nextInt(100), rnd.nextInt(360));
    
    Game.getGame().worldContainer.addQuestObject(this.fleeingRabbitCloseZone, false);
    this.fleeingRabbitCloseZone.display(true);

    return state;
  }
  
  handleEvent(state, objID, eventType, extraParams) {
    state = super.handleEvent(state, objID, eventType, extraParams);

    const rnd = new RNG();
    const quest = {...state.quests['FleeingRabbitQuest']};
    
    if (this.getAcceptanceState(state) != Quest.AcceptanceState.ACCEPTED || 
        this.getProgressState(state) != Quest.ProgressState.INPROGRESS) {
      return state;
    }
    
    if (eventType == EventTypes.VISITED &&
        fleeingRabbitCloseZoneID == objID) { 
      //Player reached triggered zone. Add rabbit exact location
      
      Game.getGame().worldContainer.removeQuestObject(this.fleeingRabbitCloseZone);
      this.fleeingRabbitCloseZone.display(false);
      
      this.fleeingRabbit = new Bunny();
      this.fleeingRabbit.attackWithoutWarning = true;
      this.fleeingRabbit.setLocation(this.fleeingRabbitCloseZone.getGeoPoint());
      this.fleeingRabbit.moveObject(rnd.nextInt(70), rnd.nextInt(360));
      this.fleeingRabbit.display(true);
      Game.getGame().worldContainer.addQuestObject(this.fleeingRabbit, false);
      
      state = this.showNarrationMessage(state, QR.strings.fleeing_rabbit_narration_close_to_rabbit);
      
    } else if(eventType == EventTypes.KILLED &&
              this.fleeingRabbit &&
              this.fleeingRabbit.nameID == objID) { //Player killed rabbit
      
      state = this.showNarrationMessage(state, QR.strings.fleeing_rabbit_narration_rabbit_killed);
      
      //TODO: poni�sze trzeba pokaza showNewInQuestMessage jak si� narracje przeczyta.
      state = this.pushStep(state, {
        id: 1,
        time: (new Date().getTime()),
        narrationTextName: "fleeing_rabbit_quest_end"
      });
      this.setFullMessageText(this.getMessageWithNarrations(state));
      // TODO migrate  Game.turnOnOffNotificationForButton(R.id.btnQuests, true);
      
      //Game.getGame().playerEgo.hasCreaturesOnMap = true;
      //for(PhysicalObject item : Game.getGame().worldContainer.otherObjects)
      //  item.display(false);
      //TODO: potrzebne?
       //Game.getGame().worldContainer.otherObjects.clear(); //Hope to have only plants here during this quest
      //Game.getGame().worldContainer.clearGeneratedSlabs();
      
      
      //w��czenie generacji kr�lik�w i kot�w
      state = {
        ...state,
        playerEgo: {
          ...state.playerEgo,
          knowledge: {
            ...state.playerEgo.knowledge,
            knownCreatures: {
              'creature_bunny': true,
              'creature_black_cat': true,
              'creature_strumpf': true,
            },
          }
        }
      };
      
      state = this.setCompleted(state);
      state = {
        ...state,
        playerEgo: (PlayerEgo.addExperience(state.playerEgo, 300)),
      };
      Game.saveGame(state);
    }

    return state;
  }
  
  onMessageOpen(state) {
    state = super.onMessageOpen(state);
    /* TODO migrate
    if(progressState_ == ProgressState.INPROGRESS && 
        completedSteps_.size() == 0) {
      $log.d(TAG, "Message opened sending broadcast");
      Game.turnOnOffNotificationForButton(R.id.btnMap, true);
    }
    */
    return state;
  }
}
