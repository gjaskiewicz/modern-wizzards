import Quest from "../Quest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Game from "../../Game.js";
import PlayerEgo from "../../PlayerEgo.js";
import GeoPoint from "../../gis/GeoPoint.js";
import $log from "../../utils/log.js";
import RNG from "../../utils/RNG.js";
import MWToast from "../../ui/MWToast.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";
import TriggerZone from "../../gamelogic/world/TriggerZone.js";

const TAG = "GoToPointQuest";

export default class GoToPointQuest extends Quest {
  
  constructor() {
    super();
    this.title_ = QR.strings.gotopoint_quest_title;
    this.from_ = QR.strings.gotopoint_quest_from;
    this.fromimg_ = QR.strings.quest_profile_william;
    this.triggerZones = [];

    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
    EventSystem.INSTANCE.registerListener(EventTypes.BACK_IN_BODY, this);
  }

  initiateQuest(s) {
    s = this.setAccepted(s);
    return s;
  }

  restoreQuest(s) {
    this.setBasicMessage(QR.strings.gotopoint_quest_content);
    this.setFullMessageText(this.getMessageWithNarrations(s));
    return s;
  }
  
  initiateQuestBackground(state) {
    const rnd = new RNG();
    const startingBearing = rnd.nextInt(90);
    const gp_player = PlayerEgo.getGeoPoint(state.playerEgo);
    
    this.triggerZones = [];
    for(let i = 0; i < 4; i++) 
    {
      const tz = new TriggerZone("GoToPointQuestZone", 
          new GeoPoint(
              gp_player.getLatitudeE6(), 
              gp_player.getLongitudeE6()), 
          40, 
          R.strings.target);
      tz.moveObject(120.0, 90 * i + startingBearing);
      tz.display(true);
      Game.getGame().worldContainer.addQuestObject(tz, true);
      this.triggerZones.push(tz);
    }

    return state;
  }
  
  handleEvent(state, objID, eventType, extraParams) {
    state = super.handleEvent(state, objID, eventType, extraParams);

    if (this.getAcceptanceState(state) != Quest.AcceptanceState.ACCEPTED ||
      this.getProgressState(state) != Quest.ProgressState.INPROGRESS) {
        return state;
    }

    const quest = {...state.quests['GoToPointQuest']};
    quest.completedSteps = quest.completedSteps || [ ];

    if (objID == "GoToPointQuestZone" &&
        eventType == EventTypes.VISITED && 
        quest.completedSteps.length == 0 &&
        !PlayerEgo.isUnderEffectOfSpell(state.playerEgo, "spell_oob")) {
      //Entered first area with normal walking
      state = this.pushStep(state, {
        id: 1,
        time: (new Date().getTime()),
        narrationTextName: "gotopoint_quest_narration_1"
      });
      
      for(let tz of this.triggerZones) {
        Game.getGame().worldContainer.removeQuestObject(tz);
        tz.display(false);
      }
      this.triggerZones = [ ];

      state = this.showNewInQuestMessage(state, QR.strings.gotopoint_quest_narration_1);
      this.setFullMessageText(this.getMessageWithNarrations(state));
      
      //Preparing next part of quest. Using oob.
      const tz = new TriggerZone("GoToPointWithOOBQuestZone", 
          PlayerEgo.getGeoPoint(state.playerEgo), 
          40, 
          R.strings.target);
      tz.moveObject(200.0, 180);
      tz.display(true);
      
      Game.getGame().worldContainer.addQuestObject(tz, true);
      state = {
        ...state,
        playerEgo: PlayerEgo.addKnownGlobalSpell(state.playerEgo, "spell_oob"),
      };
      this.triggerZones.push(tz);
    
      return state;
    } else if (objID == "GoToPointWithOOBQuestZone" &&
        eventType == EventTypes.VISITED && 
        quest.completedSteps.length == 1 &&
        PlayerEgo.isUnderEffectOfSpell(state.playerEgo, "spell_oob")) {

      state = this.pushStep(state, {
        id: 2,
        time: (new Date().getTime()),
        narrationTextName: "gotopoint_quest_narration_2"
      });
      
      state = {
        ...state,
        playerEgo: PlayerEgo.endOutOfBody(state.playerEgo, false),
      };
      
      for(let tz of this.triggerZones) {
        Game.getGame().worldContainer.removeQuestObject(tz);
        tz.display(false);
      }
      this.triggerZones = [ ];
      
      state = this.showNewInQuestMessage(state, QR.strings.gotopoint_quest_narration_2, null, null);
      this.setFullMessageText(this.getMessageWithNarrations(state));
      
      /*
      HashSet<PostProcessingRequestType> result = new HashSet<PostProcessingRequestType>();
      result.add(PostProcessingRequestType.REFRESHMAP);
      return result;
      */
    } else if(objID == "GoToPointWithOOBQuestZone" &&
        eventType == EventTypes.VISITED && 
        quest.completedSteps.length == 1 &&
        !PlayerEgo.isUnderEffectOfSpell(state.playerEgo, "spell_oob")) {
      if (!quest.oob_hint_shown) {
        MWToast.toast(QR.strings.gotopoint_quest_have_to_use_oob);
        state = this.applyToQuest(state, q => (
          {...q, oob_hint_shown: true}));
      }
    } else if (objID == "spell_oob" &&
        eventType == EventTypes.BACK_IN_BODY && 
        quest.completedSteps.length == 2) {
      state = this.setCompleted(state);
      state = {
        ...state,
        playerEgo: (PlayerEgo.addExperience(state.playerEgo, 200)),
      };
      Game.saveGame(state);
    }
    
    return state;
  }
  
  onMessageOpen(state) {
    state = super.onMessageOpen(state);
    /* TODO: migrate
    if(progressState_ == ProgressState.INPROGRESS && 
        completedSteps_.size() < 1) { //Player started to play and didn't visited any triggered area.
      Game.turnOnOffNotificationForButton(R.id.btnMap, true);
    }
    */
    return state;
  }

}
