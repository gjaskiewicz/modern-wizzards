import Quest from "../Quest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import RNG from "../../utils/RNG.js";
import MoveLocation from "../../utils/MoveLocation.js";
import CollectableItem from "../../gamelogic/world/CollectableItem.js";
import CollectPlant from "../../gamelogic/inventory/CollectPlant.js";
import SphericalZone from "../../gis/SphericalZone.js";
import TriggerZone from "../../gamelogic/world/TriggerZone.js";
import PlayerEgo from "../../PlayerEgo.js";
import Game from "../../Game.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";
import Inventory from "../../gamelogic/Inventory.js";
import GameContent from "../../gamelogic/GameContent.js";
import $log from "../../utils/log.js";

const COLLECT_PLANT_QUEST_ZONE = "CollectPlantQuestZone";
const TAG = "CollectPlantsQuest";
const targetZoneID = "CollectPlantsQuestTargetZone";

const items_types_to_collect =  [
  "item_daisyflower", "item_4clover", "item_swallowweed", "item_garlic"
];

export default class CollectPlantsQuest extends Quest {
  
  constructor() {
    super();
    this.title_ = QR.strings.collectplants_quest_title;
    this.from_ = QR.strings.collectplants_quest_from;
    this.fromimg_ = QR.strings.quest_profile_william;

    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
    EventSystem.INSTANCE.registerListener(EventTypes.COLLECTED, this);
  }

  initiateQuest(s) {
    s = this.setAccepted(s);
    return s;
  }
  
  initiateQuestBackground(state) {
    const rnd = new RNG();
    
    this.plantsZones = { }; // new HashMap<String, List<CollectableItem>>();
    this.triggeredZonesAbovePlants = [ ]; // new LinkedList<TriggerZone>();
  
    const gp_player = PlayerEgo.getGeoPoint(state.playerEgo);
    let angleD = 0;
    state = this.applyToQuest(state, q => ({
      ...q,
      collectedPlants: (q.collectedPlants || { }),
      completedSteps: (q.completedSteps || [ ]),
    }));
    
    const quest = state.quests['CollectPlantsQuest'];
    for (let plantType of items_types_to_collect) {
      if (quest.collectedPlants[plantType]) {
        // Skip already collected items. Meaning during restore.
        continue;
      }
      
      const tmpPlantsColl = [ ]; // new LinkedList<CollectableItem>();
      angleD++;
      const gp = MoveLocation.moveGeoPoint(gp_player, 
        100.0 + rnd.nextInt(100), 
        90 * angleD + rnd.nextInt(90));
      const plant = new CollectableItem(
          new SphericalZone(gp, 30 + (rnd.nextInt(15))),
          new CollectPlant(plantType));
      
      Game.getGame().worldContainer.addQuestObject(plant, true);
      plant.display(false);
      if(!quest.firstNarrationShown) {
        const tz = new TriggerZone(
          COLLECT_PLANT_QUEST_ZONE, gp, 60, R.strings.target);
        tz.display(true);
        
        this.triggeredZonesAbovePlants.push(tz);
        Game.getGame().worldContainer.addQuestObject(tz, true);
      } else {
        plant.display(true);
      }
      
      tmpPlantsColl.push(plant);
      this.plantsZones[plantType] = tmpPlantsColl;
    }

    return state;
    //this.applyToQuest(state, () => quest);
  }
  
  restoreQuest(state) {
    const quest = {...state.quests['CollectPlantsQuest']};
    this.setBasicMessage(QR.strings.collectplants_quest_content);
    this.setFullMessageText(this.getMessageWithNarrations(state));

    if (this.getProgressState(state) != Quest.ProgressState.INPROGRESS) {
      return state;
    }
    if (!quest.completedSteps || !quest.completedSteps.length) {
      if (this.isAlreadyRead(state)) {
        return this.initiateQuestBackground(state);
      }
      return state;
    }

    if(quest.completedSteps.length < items_types_to_collect.length) {
      state = this.initiateQuestBackground(state);
      if(quest.firstNarrationShown) {
        this.makePlantsVisible();
      }
      return state;
    } else if (quest.completedSteps.length == items_types_to_collect.length) { 
      // All plants collected setup destination point
      state = this.initiateQuestBackground(state);
      this.setupDestinationPoint(state);
      return state;
    }
    return state;
  }

  handleEvent(state, objID, eventType, extraParams) {
    state = super.handleEvent(state, objID, eventType, extraParams);

    // const quest = {...state.quests['CollectPlantsQuest']};
    const quest = () => state.quests['CollectPlantsQuest'];

    if (this.getAcceptanceState(state) != Quest.AcceptanceState.ACCEPTED || 
        this.getProgressState(state) != Quest.ProgressState.INPROGRESS) {
      return {...state};
    }
    
    if (objID == COLLECT_PLANT_QUEST_ZONE && 
        eventType == EventTypes.VISITED && 
        !quest().firstNarrationShown) {
      state = this.applyToQuest(state, q => (
        {...q, firstNarrationShown: true}));
      
      this.clearTriggeredZonesAbovePlants();
      this.makePlantsVisible();

      state = this.showNarrationMessage(state, QR.strings.collectplants_narration_close_to_plant);
      
      Game.saveGame(state);
    } else if (eventType == EventTypes.COLLECTED && 
               quest().completedSteps.length < items_types_to_collect.length) {
      $log.d(TAG, "Plant collected " + objID);
      let plantID = objID;
      state = this.applyToQuest(state, q => ({
        ...q,
        collectedPlants: {
          ...q.collectedPlants,
          [plantID]: 1,
        },
      }));
        
      delete this.plantsZones[objID];
      const plantsZonesSize = Object.keys(this.plantsZones).length;
      
      const step = {
        id: (items_types_to_collect.length - plantsZonesSize),
        time: (new Date().getTime()),
        narrationTextName: null,
      };
      
      if(quest().completedSteps.length == items_types_to_collect.length / 2 - 1) { 
        // Player collected half of requested plants
        step.narrationTextName == "collectplants_narration_use_telekinesis";
        state = this.showNarrationMessage(state, QR.strings.collectplants_narration_use_telekinesis);
        this.setFullMessageText(this.getMessageWithNarrations(state));
        state = {
          ...state,
          playerEgo: PlayerEgo.addKnownGlobalSpell(state.playerEgo, "spell_telekinesis"),
        };
        // TODO migrate   Game.turnOnOffNotificationForButton(R.id.btnSpells, true);
      }
      state = this.pushStep(state, step);
      
      if(quest().completedSteps.length == items_types_to_collect.length) { 
        // Last plant collected
        this.clearTriggeredZonesAbovePlants();

        //dodajemy wiadomosc gdzie ma sie udac
        state = this.pushStep(state, {
          id: items_types_to_collect.length,
          time: (new Date().getTime()),
          narrationTextName: "collectplants_narration_all_collected",
        });
        state = this.showNewInQuestMessage(state, QR.strings.collectplants_narration_all_collected);
        this.setupDestinationPoint(state);
      }
      
      Game.saveGame(state);   
    } else if(eventType == EventTypes.VISITED &&
          //completedSteps_.size() == items_types_to_collect.length+1 &&
          (state.playerEgo.inventory.filter(i => items_types_to_collect.includes(i.name)).length ==
          items_types_to_collect.length) &&
          targetZoneID == objID) {
      state = this.pushStep(state, {
        id: items_types_to_collect.length + 1,
        time: (new Date().getTime()),
        narrationTextName: "collectplants_narration_end",
      });
      
      //TODO: zast�pczo dodane wszystkie ro�liny.
      let knownPlants = { };
      for (let ci of GameContent.getInstance().getAllPlantsTypes())  {
        knownPlants[ci.getNameID()] = true;
      }
      state = {
        ...state,
        playerEgo: {
          ...state.playerEgo,
          knowledge: {
            ...state.playerEgo.knowledge,
            knownPlants
          }
        }
      };
      
      state = this.removeQuestPlants(state);
      this.targetZone.display(false);
      Game.getGame().worldContainer.removeQuestObject(this.targetZone);
      
      state = this.showNewInQuestMessage(state, QR.strings.collectplants_narration_end);
      this.setFullMessageText(this.getMessageWithNarrations(state));
      
      state = this.setCompleted(state); // FIXME: quest is overwritten
      state = {
        ...state,
        playerEgo: (PlayerEgo.addExperience(state.playerEgo, 300)),
      };
      Game.saveGame(state);
    }

    return state;
  }

  removeQuestPlants(state) {
    let inv = state.playerEgo.inventory;
    for (let plantName of items_types_to_collect) {
      if (Inventory.get().hasItem(inv, plantName)) {
        inv = Inventory.get().removePlant(inv, plantName);
      } else {
        $log.e(TAG, "Can't find plant [" + plantName + "] in the player's inventory");
      } 
    }
    return {
      ...state, 
      playerEgo: {
        ...state.playerEgo,
        inventory: inv,
      }
    };
  }

  setupDestinationPoint(state) {  
    const rnd = new RNG();
    const gp_player = PlayerEgo.getGeoPoint(state.playerEgo);
    this.targetZone = new TriggerZone(targetZoneID, gp_player, 
        40,
        R.strings.target);
    this.targetZone.moveObject(120.0 + rnd.nextInt(80), rnd.nextInt(360));
    this.targetZone.display(true);
    Game.getGame().worldContainer.addQuestObject(this.targetZone, true);
  }

  clearTriggeredZonesAbovePlants() {
    for (let tz of this.triggeredZonesAbovePlants) {
      Game.getGame().worldContainer.removeQuestObject(tz);
      tz.display(false);
    }
    this.triggeredZonesAbovePlants = [ ];
  }

  makePlantsVisible() {
    for (let pl of Object.values(this.plantsZones)) {
      for (let plant of pl) {
        plant.display(true);
      }
    }
  }
  
  onMessageOpen(state) {
    state = super.onMessageOpen(state);
    /* TODO migrate
    if(progressState_ == ProgressState.INPROGRESS && 
        completedSteps_.size() < items_types_to_collect.length/2) {
      Game.turnOnOffNotificationForButton(R.id.btnMap, true);
    }
    */
    return state;
  }
}
