import Quest from "../Quest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Game from "../../Game.js";
import PlayerEgo from "../../PlayerEgo.js";
import Inventory from "../../gamelogic/Inventory.js";
import $log from "../../utils/log.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";

const TAG = "InitialQuest";

export default class InitialQuest extends Quest {

  constructor() {
    super();
    this.title_ = QR.strings.quests_initial_message_title;
    this.from_ = QR.strings.quests_initial_message_from;
    this.fromimg_ = QR.strings.quest_profile_bf;

    EventSystem.INSTANCE.registerListener(EventTypes.USED, this);
  }

  initiateQuest(s) {
    s = this.setAccepted(s);
    s = this.addInitialPotion(s);
    return s;
  }

  restoreQuest(s) {
    this.setBasicMessage(QR.strings.quests_initial_message_content);
    this.setFullMessageText(this.getMessageWithNarrations(s));
    return s;
  }

  addInitialPotion(s) {
    if (!Inventory.get().hasItem(s.playerEgo.inventory, "potion_initial")) {
      return {
        ...s,
        playerEgo: {
          ...s.playerEgo,
          inventory: [...s.playerEgo.inventory, {name: "potion_initial"}]
        }
      }
    } else {
      return s;
    }
  }

  initiateQuestBackground(s) {
    return s;
  }
  
  onMessageOpen(s) {
    s = super.onMessageOpen(s);
    return s;
    /* TODO: migrate 
    if(progressState_ == ProgressState.INPROGRESS) {
      Game.turnOnOffNotificationForButton(R.id.btnInventory, true);
    }
    */
  }

  handleEvent(state, objID, eventType, extraParams) {
    state = super.handleEvent(state, objID, eventType, extraParams);
    $log.d(TAG,"Handling event");
    if (this.getAcceptanceState(state) == Quest.AcceptanceState.ACCEPTED && 
        this.getProgressState(state) == Quest.ProgressState.INPROGRESS && 
        objID == "potion_initial" &&
        eventType == EventTypes.USED) {
      state = this.showNarrationMessage(state, QR.strings.narration_initial_potion_effect);
      //this.setFullMessageText(this.getMessageWithNarrations(state));
      state = {
        ...state,
        playerEgo: (PlayerEgo.addExperience(state.playerEgo, 100)),
      };
      state = this.setCompleted(state);
      Game.saveGame(state);
    }
    return state;
  }
};
