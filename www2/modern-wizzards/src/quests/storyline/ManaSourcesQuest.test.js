import ManaSourcesQuest from './ManaSourcesQuest.js';
import Quest from '../Quest.js';
import Inventory from "../../gamelogic/Inventory.js";
import SpellFactory from '../../gamelogic/SpellFactory.js';
import Player from '../../gamelogic/creatures/Player.js'
import MainLoop from '../../gamelogic/MainLoop.js';
import Game from '../../Game.js';
import GameSaver from '../../utils/GameSaver.js';
import GameState from '../../GameState.js';

import TestData from '../../test/TestData.js';
import FakeMapController from '../../test/FakeMapController.js';

describe('ManaSourcesQuest', () => {
  GameSaver.disable();
  window.effects = {addEffect: () => null};

  it('should be possible to complete', () => {
    const mapController = new FakeMapController();
    mapController.init();
    const mainLoop = new MainLoop();
    mainLoop.loopIteration = -1;
    let s = {
      playerEgo: {
        ...TestData.getFullEgo(),
        mana: 0,
      },
      gameState: GameState.STATE_WALKING,
      quests: {
        ManaSourcesQuest: {
          ...Quest.makeDefault(),
        },
      },
      achivments: { 
        granted: [ ],
      },
      worldContainer: {
        gatheredPlants: [ ],
        creaturesDefeated: [ ],
      },
    };

    const q = new ManaSourcesQuest();
    s = q.initiateQuest(s);
    s = q.onMessageOpen(s);
    expect(q.manaZones.length).toBeGreaterThan(0);
    const gp = q.manaZones[0].getGeoPoint();

    s = {
      ...s,
      playerEgo: {
        ...s.playerEgo,
        lat: gp.lat,
        lng: gp.lng,
      }
    };

    s = mainLoop.iterateGameState(s);
    expect(s.quests.ManaSourcesQuest.completedSteps.length).toBe(1);
    expect(s.quests.ManaSourcesQuest.completedSteps[0].id).toBe(1);
    expect(s.quests.ManaSourcesQuest.progressState).toBe(Quest.ProgressState.INPROGRESS);
    expect(s.playerEgo.knowledge.generateManaZones).toBe(false);

    for (let i = 0; i < 10000; i++) {
      s = mainLoop.iterateGameState(s);
    }

    expect(s.quests.ManaSourcesQuest.progressState).toBe(Quest.ProgressState.COMPLITED);
    expect(s.playerEgo.knowledge.generateManaZones).toBe(true);
  });
});
