import Quest from "../Quest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Game from "../../Game.js";
import RNG from "../../utils/RNG.js";
import Bat from "../../gamelogic/creatures/Bat.js";
import GeoPoint from "../../gis/GeoPoint.js";
import PlayerEgo from "../../PlayerEgo.js";
import Inventory from "../../gamelogic/Inventory.js";
import $log from "../../utils/log.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";
import TriggerZone from "../../gamelogic/world/TriggerZone.js";

const TAG = "TeletransportQuest";
const pickUpZoneID = "PickUpZone";
const returnZoneID = "ReturnZone";
const amountOfBats = 8;

export default class TeletransportQuest extends Quest {
  
  constructor() {
    super();
    this.title_ = QR.strings.quest_teletransport_title;
    this.from_ = QR.strings.quest_teletransport_from;
    this.fromimg_ = QR.strings.quest_profile_william;
    
    this.pickUpZone = null;
    this.returnZone = null;
    this.bats = [ ];

    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
  }

  initiateQuest(s) {
    s = this.setAccepted(s);
    return s;
  }
  
  initiateQuestBackground(state) {
    const rnd = new RNG();
    const gp_player = PlayerEgo.getGeoPoint(state.playerEgo);
    
    // Delete potions in case of restoring
    state = this.cleanQuestRemains(state);
    
    this.pickUpZone = new TriggerZone(pickUpZoneID, 
        new GeoPoint(
          gp_player.getLatitudeE6(), 
          gp_player.getLongitudeE6()), 
        30, R.strings.target);
    this.pickUpZone.moveObject(200.0 + rnd.nextInt(100), rnd.nextInt(360));
    
    Game.getGame().worldContainer.addQuestObject(this.pickUpZone, true);
    this.pickUpZone.display(true);

    return state;
  }
  
  restoreQuest(state) {
    this.setBasicMessage(QR.strings.quest_teletransport_initial);
    this.setFullMessageText(this.getMessageWithNarrations(state));
    let quest = {...state.quests['TeletransportQuest']};

    if (!quest.completedSteps) {
      if (this.isAlreadyRead(state)) {
        return this.initiateQuestBackground(state);
      }
      return state;
    }

    switch(quest.completedSteps.length) {
      case 0:
        // TODO: obsłuzone na górze?
        return this.initiateQuestBackground(state);
      case 1:
        return this.handlePackageReceived(state);
      default:
        return state;
    }
  }
  
  cleanQuestRemains(state) {
    return this.deleteQuestPotions(state);
  }
  
  handleEvent(state, objID, eventType, extraParams) {
    state = super.handleEvent(state, objID, eventType, extraParams);
    
    if (this.getAcceptanceState(state) != Quest.AcceptanceState.ACCEPTED || 
        this.getProgressState(state) != Quest.ProgressState.INPROGRESS) {
      return state;
    }
    
    if (eventType == EventTypes.VISITED && pickUpZoneID == objID) { 
      Game.getGame().worldContainer.removeQuestObject(this.pickUpZone);
      this.pickUpZone.display(false);
      this.pickUpZone = null;
      
      state = this.pushStep(state, {
        id: 1,
        time: (new Date().getTime()),
        narrationTextName: "quest_teletransport_narration_package_received",
      });
      state = this.showNewInQuestMessage(state, QR.strings.quest_teletransport_narration_package_received);
      this.setFullMessageText(this.getMessageWithNarrations(state));
      
      state = this.handlePackageReceived(state);
      
      Game.saveGame(state);
      return state;
    } else if (eventType == EventTypes.VISITED && objID == returnZoneID) { 
      // Player killed bat and is in return zone
      // time
      state = this.pushStep(state, {
        id: 2,
        time: (new Date().getTime()),
        narrationTextName: "quest_teletransport_narration_end",
      });
      state = this.showNewInQuestMessage(state, QR.strings.quest_teletransport_narration_end);
      this.setFullMessageText(this.getMessageWithNarrations(state));
      
      for (let bat of this.bats) {
        Game.getGame().worldContainer.removeQuestObject(bat);
      }
      Game.getGame().worldContainer.removeQuestObject(this.returnZone);
       
      state = this.deleteQuestPotions(state);
      state = {
        ...state,
        playerEgo: {
          ...state.playerEgo,
          knowledge: {
            ...state.playerEgo.knowledge,
            knownCreatures: {
              ...state.playerEgo.knowledge.knownCreatures,
              'creature_bat': true,
            },
          }
        }
      };
      state = {
        ...state,
        playerEgo: (PlayerEgo.addExperience(state.playerEgo, 700)),
      };

      state = this.setCompleted(state);
      Game.saveGame(state);
    }

    return state;
  }
  
  onMessageOpen(state) {
    state = super.onMessageOpen(state);
    /* TODO migrate
    if(progressState_ == ProgressState.INPROGRESS && 
        completedSteps_.size() == 0) {
      $log.d(TAG, "Message opened sending broadcast");
      Game.turnOnOffNotificationForButton(R.id.btnMap, true);
    }
    */
    return state;
  }
  

  
  handlePackageReceived(state) {
    state = this.deleteQuestPotions(state); //Erase old potions in case of restoring quest
    state = this.createQuestPotions(state);
    
    const rnd = new RNG();
    const gp_player = PlayerEgo.getGeoPoint(state.playerEgo);
    
    this.returnZone = new TriggerZone(returnZoneID, 
        new GeoPoint(
          gp_player.getLatitudeE6(), 
          gp_player.getLongitudeE6()), 
        30,
        R.strings.target);
    const distanceToReturnZone = 300 + rnd.nextInt(100);
    const bearingToReturnZone = rnd.nextInt(360);
    this.returnZone.moveObject(distanceToReturnZone, bearingToReturnZone);
      
    //middle bat
    const middlebat = new Bat();
    middlebat.setLocation(new GeoPoint( 
      gp_player.getLatitudeE6(),
      gp_player.getLongitudeE6())
    );
    middlebat.moveObject(distanceToReturnZone/2, bearingToReturnZone);
    middlebat.display(true);
    Game.getGame().worldContainer.addQuestObject(middlebat, false);
    this.bats.push(middlebat);
    
    for(let i = 1; i < amountOfBats; ++i) {
      const bat = new Bat();
      //bat.attackWithoutWarning = true;
      bat.setLocation(new GeoPoint( 
          middlebat.getGeoPoint().getLatitudeE6(),
          middlebat.getGeoPoint().getLongitudeE6()) );
      const distanceToNewBat = distanceToReturnZone * (0.8+rnd.nextDouble()/5) /2 ;
      const bearingToNewBat = (i * (360/(amountOfBats-1))) + rnd.nextInt(20);
      //$log.d(TAG, "New bat distance: " + distanceToNewBat + " bearing: " + bearingToNewBat);
      bat.moveObject(distanceToNewBat, bearingToNewBat);
      bat.display(true);
      //if(i==0)
      //  bat.dontAttackUntil = MWTime.s() + 10;
      Game.getGame().worldContainer.addQuestObject(bat, false);
      this.bats.push(bat);
    }

    //Return zone is added at the end to be checked after bats in collection of otherObjects
    Game.getGame().worldContainer.addQuestObject(this.returnZone, true);
    this.returnZone.display(true);

    return state;
  }
  
  deleteQuestPotions(state) {
    const spellTokensToDelete = [ ]; // new LinkedList<SpellToken>();
    const inventory = state.playerEgo.inventory.filter(item => {
      return !item.blocked || item.name != 'potion_unknown'
    });
    return {
      ...state,
      playerEgo: {
        ...state.playerEgo,
        inventory
      }
    };
  }

  createQuestPotions(state) {
    for(let i = 0; i < 1; i++) {
      state = {
        ...state,
        playerEgo: {
          ...state.playerEgo,
          inventory: [...state.playerEgo.inventory, {
            name: 'potion_unknown',
            blocked: true,
          }],
        }
      };
    }
    return state;
  }
}
