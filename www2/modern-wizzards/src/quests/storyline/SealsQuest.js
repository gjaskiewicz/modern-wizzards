import Quest from "../Quest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Game from "../../Game.js";
import GameContent from "../../gamelogic/GameContent.js";
import RNG from "../../utils/RNG.js";
import Gnome from "../../gamelogic/creatures/Gnome.js";
import GeoPoint from "../../gis/GeoPoint.js";
import CollectableItem from "../../gamelogic/world/CollectableItem.js";
import CollectPlant from "../../gamelogic/inventory/CollectPlant.js";
import PlayerEgo from "../../PlayerEgo.js";
import Inventory from "../../gamelogic/Inventory.js";
import $log from "../../utils/log.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";
import TriggerZone from "../../gamelogic/world/TriggerZone.js";
import Collections from "../../utils/Collections.js";
import React from 'react';

const TAG = "SealsQuest";
const puzzlingGnomeZoneID = "puzzlingGnomeZone";

export default class SealsQuest extends Quest {
  
  constructor() {
    super();
    this.title_ = QR.strings.quest_seals_title;
    this.from_ = QR.strings.quest_seals_from;
    this.fromimg_ = QR.strings.quest_profile_anette;

    this.puzzlingGnomeZone = null;
    this.puzzlingGnome = null;

    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
    EventSystem.INSTANCE.registerListener(EventTypes.MESSAGE, this);
    EventSystem.INSTANCE.registerListener(EventTypes.KILLED, this);
  }

  initiateQuest(s) {
    s = this.setAccepted(s);
    return s;
  }

  restoreQuest(s) {
    this.setBasicMessage(QR.strings.quest_seals_initial);
    this.setFullMessageText(this.getMessageWithNarrations(s));
    return s;
  }
  
  initiateQuestBackground(state) {
    const rnd = new RNG();
    const gp_player = PlayerEgo.getGeoPoint(state.playerEgo);
    
    this.puzzlingGnomeZone = new TriggerZone(puzzlingGnomeZoneID, 
        new GeoPoint(
          gp_player.getLatitudeE6(), 
          gp_player.getLongitudeE6() ), 
        50, R.strings.target);
    this.puzzlingGnomeZone.moveObject(150.0 + rnd.nextInt(100), rnd.nextInt(360));
    
    Game.getGame().worldContainer.addQuestObject(this.puzzlingGnomeZone, true);
    this.puzzlingGnomeZone.display(true);
    this.puzzlingGnome = new Gnome();

    return state;
  }
  
  handleEvent(state, objID, eventType, extraParams) {
    state = super.handleEvent(state, objID, eventType, extraParams);

    let quest = {...state.quests['SealsQuest']};
    quest.completedSteps = quest.completedSteps || [ ];

    if (this.getAcceptanceState(state) != Quest.AcceptanceState.ACCEPTED || 
        this.getProgressState(state) != Quest.ProgressState.INPROGRESS) {
      return state;    
    };
    
    if (eventType == EventTypes.VISITED && puzzlingGnomeZoneID == objID) { 
      //Player reached triggered zone.
      Game.getGame().worldContainer.removeQuestObject(this.puzzlingGnomeZone);
      this.puzzlingGnomeZone.display(false);
      
      state = this.pushStep(state, {
        id: 1,
        time: (new Date().getTime()),
        narrationTextName: null,
      });
      
      state = this.showNarrationMessage(state, QR.strings.quest_seals_narration_close_gnome, false);
    } else if(eventType == EventTypes.MESSAGE && extraParams['message']) {
      
      if (extraParams['message'] == "fight" && quest.completedSteps.length == 1) {
        state = this.gnomeAttacks(state);
      } else if (extraParams['message'] == "talk" && quest.completedSteps.length == 1) {
        const updateMessage = this.getQuestion(
          QR.strings.quest_seals_gnome_before_first_question , 1);
        
        state = this.showNarrationMessage(state, updateMessage, false);
        state = this.pushStep(state, {
          id: 2,
          time: (new Date().getTime()),
          narrationTextName: null,
        });
    
      } else if (quest.completedSteps.length == 2) { 
        //Answer to first question
        const answer = extraParams["message"];
        if (answer == "good_answer_1") { 
          const updateMessage = this.getQuestion(
            QR.strings.quest_seals_gnome_first_question_correct, 2);
          state = this.showNarrationMessage(state, updateMessage, false);
          state = this.pushStep(state, {
            id: 3,
            time: (new Date().getTime()),
            narrationTextName: null,
          });
        } else { 
          //Wrong answer
          state = this.gnomeAttacks(state);
        }
      } else if (quest.completedSteps.length == 3) { 
        //Answer to second question
        const answer = extraParams["message"];
        if(answer == "good_answer_2") { 
          const updateMessage = this.getQuestion(
            QR.strings.quest_seals_gnome_second_question_correct, 3);
          state = this.showNarrationMessage(state, updateMessage, false);
          state = this.pushStep(state, {
            id: 3,
            time: (new Date().getTime()),
            narrationTextName: null,
          });
        } else { 
          // Wrong answer
          state = this.gnomeAttacks(state);
        }
      } else if (quest.completedSteps.length == 4) { 
        // Answer to third question
        const answer = extraParams["message"];
        if(answer == "good_answer_3") { 
          let updateMessage = QR.strings.quest_seals_gnome_all_questions_correct;
          state = this.showNarrationMessage(state, updateMessage, true);
          state = this.finishQuest(state);
        } else { 
          // Wrong answer
          state = this.gnomeAttacks(state);
        }
      }
    } else if(eventType == EventTypes.KILLED && 
              this.puzzlingGnome && 
              this.puzzlingGnome.nameID == objID) { 
      // Player killed gnome
      state = this.finishQuest(state);
    }
    return state;
  }
  
  onMessageOpen(state) {
    state = super.onMessageOpen(state);
    /* TODO migrate
    if(progressState_ == ProgressState.INPROGRESS && 
        completedSteps_.length == 0) {
      $log.d(TAG, "Message opened sending broadcast");
      Game.turnOnOffNotificationForButton(R.id.btnMap, true);
    }
    */
    return state;
  }
  
  gnomeAttacks(state) {
    this.puzzlingGnome.attackWithoutWarning = true;
    this.puzzlingGnome.containsItem = "item_keypart";
    this.puzzlingGnome.setLocation(this.puzzlingGnomeZone.getGeoPoint());
    //puzzlingGnome.moveObject(rnd.nextInt(100), rnd.nextInt(360));
    this.puzzlingGnome.display(true);
    Game.getGame().worldContainer.addQuestObject(this.puzzlingGnome, true);

    return state;
  }
  
  finishQuest(state) {
    let quest = {...state.quests['SealsQuest']};

    if (this.getProgressState(state) == Quest.ProgressState.COMPLITED) {
      return state;
    }
    
    state = new CollectableItem(
      null,
      new CollectPlant("item_keypart")).interact(state);
    
    //for(PhysicalObject item : Game.getGame().worldContainer.otherObjects)
    //  item.display(false);
    //TODO: to jest kiepski pomys�, mo�esz miec obiekty z quest�w pobocznych tutaj.
    //Game.getGame().worldContainer.otherObjects.clear(); //Hope to have only plants here during this quest
    
    state = this.pushStep(state, {
      id: quest.completedSteps.length + 1,
      time: (new Date().getTime()),
      narrationTextName: null
    });
    
    state = {
      ...state,
      playerEgo: {
        ...state.playerEgo,
        knowledge: {
          ...state.playerEgo.knowledge,
          knownCreatures: {
            ...state.playerEgo.knowledge.knownCreatures,
            'creature_gnome': true
          },
        }
      }
    };
    
    state = {
      ...state,
      playerEgo: (PlayerEgo.addExperience(state.playerEgo, 700)),
    };
    state = this.setCompleted(state);
    Game.saveGame(state);

    return state;
  }
  
  getQuestion(preamble, id) {
    let resurcesQuestionID = 0,
        resurcesQuestionAnswersID = 0;
    switch (id) {
    case 1:
      resurcesQuestionID = QR.strings.quest_seals_gnome_question_1;
      resurcesQuestionAnswersID = QR.strings.quest_seals_gnome_question_1_answers;
      break;
    case 2:
      resurcesQuestionID = QR.strings.quest_seals_gnome_question_2;
      resurcesQuestionAnswersID = QR.strings.quest_seals_gnome_question_2_answers;
      break;
    case 3:
      resurcesQuestionID = QR.strings.quest_seals_gnome_question_3;
      resurcesQuestionAnswersID = QR.strings.quest_seals_gnome_question_3_answers;
      break;
    default:
      return null;
    }
    
    //Get answers and shuffle them
    const style = {
      marginTop: '15px',
    }
    const answersArray = Collections.shuffle(resurcesQuestionAnswersID)
        .map(i => (<li style={style}>{i}</li>));
    
    return (
    <div>
      {preamble}
      <br />
      {resurcesQuestionID}
      <ul>
        {answersArray}
      </ul>
    </div>);
    
  }
}
