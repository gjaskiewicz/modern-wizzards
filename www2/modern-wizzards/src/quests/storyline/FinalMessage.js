import Quest from "../Quest.js";
import QR from "../../QR.js";
import Game from "../../Game.js";

const TAG = "FinalMessage";

export default class FinalMessage extends Quest {
  
  constructor() {
    super();  
    this.title_ = QR.strings.quest_special_mission_security_alert_title;
    this.from_ = QR.strings.quest_special_mission_security_alert_from;
    this.fromimg_ = QR.strings.quest_profile_anthony;
  }

  initiateQuest(s) {
    s = this.setAccepted(s);
    return s;
  }

  restoreQuest(s) {
    this.setBasicMessage(QR.strings.quest_special_mission_security_alert);
    this.setFullMessageText(this.getMessageWithNarrations(s));
    return s;
  }
  
  initiateQuestBackground(state) {
    state = this.setCompleted(state);
    Game.saveGame(state);
    return state;
  }
  
  onMessageOpen(state) {
    state = Game.getGame().getAchivContainer().getAchivGameFinished().achive(state);
    state = this.initiateQuestBackground(state);
    return state;
  }
}
