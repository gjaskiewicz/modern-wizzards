import Quest from "../Quest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Game from "../../Game.js";
import PlayerEgo from "../../PlayerEgo.js";
import GeoPoint from "../../gis/GeoPoint.js";
import Inventory from "../../gamelogic/Inventory.js";
import RNG from "../../utils/RNG.js";
import $log from "../../utils/log.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";
import ManaZone from "../../gamelogic/world/ManaZone.js";

const TAG = "AlchemyQuest";

export default class AlchemyQuest extends Quest {
  
  constructor() {
    super();
    this.title_ = QR.strings.quest_alchemy_initial_title;
    this.from_ = QR.strings.quest_alchemy_initial_from;
    this.fromimg_ = QR.strings.quest_profile_anette;

    EventSystem.INSTANCE.registerListener(EventTypes.CREATED_BY_ALCHEMY, this);
  }

  initiateQuest(s) {
    s = this.setAccepted(s);
    return s;
  }

  restoreQuest(s) {
    this.setBasicMessage(QR.strings.quest_alchemy_initial);
    this.setFullMessageText(this.getMessageWithNarrations(s));
    return s;
  }
  
  initiateQuestBackground(state) { 
    return {
      ...state,
      playerEgo: {
        ...state.playerEgo,
        knowledge: {
          ...state.playerEgo.knowledge,
          knownPotions: {'potion_health': true},
          isAlchemyActive: true,
        }
      }
    };
  }
  
  handleEvent(state, objID, eventType, extraParams) {
    state = super.handleEvent(state, objID, eventType, extraParams);
    const {usedPlants} = extraParams || { };

    if (this.getAcceptanceState(state) == Quest.AcceptanceState.ACCEPTED && 
        this.getProgressState(state) == Quest.ProgressState.INPROGRESS && 
        eventType == EventTypes.CREATED_BY_ALCHEMY &&
        objID == "potion_health") { 
      state = this.pushStep(state, {
        id: 1,
        time: (new Date().getTime()),
        narrationTextName: null,
      });
      
      const usedPlantsString = (usedPlants || [ ]).join(" ");
      $log.d(TAG, `Receved event CREATED_BY_ALCHEMY (${objID}). From plants: "${usedPlantsString}"`);
      
      state = {
        ...state,
        playerEgo: (PlayerEgo.addExperience(state.playerEgo, 600)),
      };
      
      //TODO: Te powinny byc dodane po questach pobocznych.
      state = {
        ...state,
        playerEgo: {
          ...state.playerEgo,
          knowledge: {
            ...state.playerEgo.knowledge,
            knownPotions: {
              'potion_health': true,
              'potion_luckjelly': true, 
              'potion_stimulation': true, 
              'potion_perception': true
            },
            isAlchemyActive: true,
          }
        }
      };
      
      state = this.setCompleted(state);
      Game.saveGame(state);
    }
    return state;
  }
  
  onMessageOpen(state) {
    state = super.onMessageOpen(state);
    /* TODO migrate
    if(progressState_ == ProgressState.INPROGRESS && 
        completedSteps_.size() == 0) {
      $log.d(TAG, "Message opened sending broadcast");
      Game.turnOnOffNotificationForButton(R.id.btnInventory, true);
    }
    */
    return state;
  }
}
