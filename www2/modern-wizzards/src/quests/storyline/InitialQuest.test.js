import InitialQuest from './InitialQuest.js';
import Quest from '../Quest.js';
import Inventory from "../../gamelogic/Inventory.js";
import TestData from '../../test/TestData.js';
import SpellFactory from '../../gamelogic/SpellFactory.js';
import Player from '../../gamelogic/creatures/Player.js'
import GameSaver from '../../utils/GameSaver.js';

describe('InitialQuest', () => {
  GameSaver.disable();

  it('should be possible to complete', () => {
    window.effects = {
      addEffect: () => { },
    };
    let s = {
      playerEgo: TestData.getFullEgo(),
      quests: {
        InitialQuest: {
          ...Quest.makeDefault(),
        },
      },
    };

    expect(Inventory.get().hasItem(s.playerEgo.inventory, 'potion_initial')).toBe(false);
    const q = new InitialQuest();
    s = q.initiateQuest(s);
    s = q.onMessageOpen(s);

    expect(Inventory.get().hasItem(s.playerEgo.inventory, 'potion_initial')).toBe(true);

    const spell = SpellFactory.getSpellByID('potion_initial', Player.INSTANCE);
    const cast = spell.castThis(s);
    s = cast.state;
    
    expect(s.quests.InitialQuest.progressState).toBe(Quest.ProgressState.COMPLITED);
  });

});
