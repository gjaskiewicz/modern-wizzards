import Quest from "../Quest.js";
import {GenericQuest, Step} from "../GenericQuest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Game from "../../Game.js";
import GameSaver from "../../utils/GameSaver.js";
import GameContent from "../../gamelogic/GameContent.js";
import RNG from "../../utils/RNG.js";
import Golem from "../../gamelogic/creatures/Golem.js";
import GeoPoint from "../../gis/GeoPoint.js";
import PlayerEgo from "../../PlayerEgo.js";
import TriggerZone from "../../gamelogic/world/TriggerZone.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";

const TAG = "SpecialMissionQuest";
const golemZoneID = "golemZone"; 
const goalZoneID = "goalZone";
const rnd = new RNG();

const getZone = (s) => {
  const gp_player = PlayerEgo.getGeoPoint(s.playerEgo);
  const golemZone = new TriggerZone(golemZoneID, 
      new GeoPoint(
        gp_player.getLatitudeE6(), 
        gp_player.getLongitudeE6() ), 
      70, R.strings.target);
  golemZone.moveObject(150.0 + rnd.nextInt(100), rnd.nextInt(360));
  return golemZone;
}

const setupGolem = (zone, s) => {
  zone = zone || getZone(s);
  const gp = zone.getGeoPoint();
  const golem = new Golem();
  golem.setLocation(gp);
  golem.display(true);
  Game.getGame().worldContainer.addQuestObject(golem, true);
   
  const goalZone = new TriggerZone(goalZoneID, 
      gp, 30, R.strings.target);
    
  Game.getGame().worldContainer.addQuestObject(goalZone, true);
  goalZone.display(true);
  return {golem, goalZone};
}

class GoToDoor extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
  }
  firstShow(s) { return s; }
  prepare(s) { 
    const gp_player = PlayerEgo.getGeoPoint(s.playerEgo);
    this.quest.golemZone = getZone(s);
    Game.getGame().worldContainer.addQuestObject(this.quest.golemZone, true);
    this.quest.golemZone.display(true);
    return s;
  }
  cleanup(s) { 
    Game.getGame().worldContainer.removeQuestObject(this.quest.golemZone);
    this.quest.golemZone.display(false);
    return s; 
  }
  handleEvent(state, objID, eventType, extraParams) { 
    if (eventType == EventTypes.VISITED && golemZoneID == objID) { 
      state = this.quest.pushStep(state, {
        id: 1 ,
        time: (new Date().getTime()),
        narrationTextName: null,
      });
    }
    return state; 
  }
}

class GetArtifact extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
  }
  firstShow(s) { 
    s = this.quest.showNarrationMessage(s, QR.strings.quest_special_mission_narration_in_first_zone);
    return s; 
  }
  prepare(s) { 
    let {goalZone, golem} = setupGolem(this.quest.golemZone, s);
    this.goalZone = goalZone;
    this.golem = golem;
    return s; 
  }
  cleanup(s) { 
    Game.getGame().worldContainer.removeQuestObject(this.golem);
    Game.getGame().worldContainer.removeQuestObject(this.goalZone);
    return s; 
  }
  handleEvent(state, objID, eventType, extraParams) { 
    if (eventType == EventTypes.VISITED &&
        goalZoneID == objID && 
        (this.golem.getHealth(state) <= 0 ||
         PlayerEgo.isUnderEffectOfSpell(state.playerEgo, "potion_invisibility"))) {
      state = this.quest.pushStep(state, {
        id: 2,
        time: (new Date().getTime()),
        narrationTextName: null,
      });
      state = this.quest.showNarrationMessage(state, QR.strings.quest_special_mission_narration_final);
    }
    return state; 
  }
}

export default class SpecialMissionQuest extends GenericQuest {
  constructor() {
    super();
    this.title_ = QR.strings.quest_special_mission_title;
    this.from_ = QR.strings.quest_special_mission_from;
    this.fromimg_ = QR.strings.quest_profile_anette;
    this.content_ = QR.strings.quest_special_mission_initial;

    this.golemZone = null;
    this.golem = null;
    this.goalZone = null;

    this.steps = [
      new GoToDoor(this),
      new GetArtifact(this),
      null
    ];
    
    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
  }

  initiateQuest(s) {
    s = super.initiateQuest(s);
    s = this.setAccepted(s);
    return s;
  }

  currentStep(s) {
    const quest = s.quests.SpecialMissionQuest || { };
    const completedSteps = quest.completedSteps || [ ];
    const maxId = Math.max(0, Math.max(...completedSteps.map(step => step.id || 0)));
    return this.steps[maxId];
  }
}