import Quest from "../Quest.js";
import {GenericQuest, Step} from "../GenericQuest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Game from "../../Game.js";
import GameContent from "../../gamelogic/GameContent.js";
import RNG from "../../utils/RNG.js";
import Gnome from "../../gamelogic/creatures/Gnome.js";
import GeoPoint from "../../gis/GeoPoint.js";
import CollectableItem from "../../gamelogic/world/CollectableItem.js";
import CollectPlant from "../../gamelogic/inventory/CollectPlant.js";
import PlayerEgo from "../../PlayerEgo.js";
import Inventory from "../../gamelogic/Inventory.js";
import $log from "../../utils/log.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";
import TriggerZone from "../../gamelogic/world/TriggerZone.js";
import Collections from "../../utils/Collections.js";
import React from 'react';

const TAG = "SealsQuest";
const puzzlingGnomeZoneID = "puzzlingGnomeZone";
const rnd = new RNG();

class MeetGnome extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
    this.puzzlingGnomeZone = null;
  }
  prepare(s) { 
    const gp_player = PlayerEgo.getGeoPoint(s.playerEgo);
    
    this.puzzlingGnomeZone = new TriggerZone(puzzlingGnomeZoneID, 
        new GeoPoint(
          gp_player.getLatitudeE6(), 
          gp_player.getLongitudeE6() ), 
        50, R.strings.target);
    this.puzzlingGnomeZone.moveObject(
      150.0 + rnd.nextInt(100), rnd.nextInt(360));
    
    Game.getGame().worldContainer.addQuestObject(
      this.puzzlingGnomeZone, true);
    this.puzzlingGnomeZone.display(true);
    return s;
  }
  cleanup(s) { 
    Game.getGame().worldContainer.removeQuestObject(this.puzzlingGnomeZone);
    this.puzzlingGnomeZone.display(false);      
    return s; 
  }
  handleEvent(state, objID, eventType, extraParams) { 
    if (eventType == EventTypes.VISITED && puzzlingGnomeZoneID == objID) { 
      // Player reached triggered zone.
      this.quest.stepNo++;
    }
    return state; 
  }
}

class TalkOrFight extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
  }
  firstShow(s) { 
    s = this.quest.showNarrationMessage(s, QR.strings.quest_seals_narration_close_gnome, false);
    return s; 
  }
  handleEvent(state, objID, eventType, extraParams) { 
    if(eventType == EventTypes.MESSAGE && extraParams['message']) {
      if (extraParams['message'] == "fight") {
        state = this.quest.gnomeAttacks(state);
      } else if (extraParams['message'] == "talk") {
        this.quest.stepNo++;
      }
    }
    return state; 
  }
}

class GnomeQuestion1 extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
  }
  firstShow(s) { 
    const updateMessage = getQuestion(
      QR.strings.quest_seals_gnome_before_first_question , 1);
    s = this.quest.showNarrationMessage(s, updateMessage, false);
    return s; 
  }
  prepare(s) { return s; }
  cleanup(s) { return s; }
  handleEvent(state, objID, eventType, extraParams) { 
    if(eventType == EventTypes.MESSAGE && extraParams['message']) {
      const answer = extraParams["message"];
      if (answer == "good_answer_1") { 
        this.quest.stepNo++;
      } else {
        state = this.quest.gnomeAttacks(state);
      }
    }
    return state; 
  }
}

class GnomeQuestion2 extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
  }
  firstShow(s) { 
    const updateMessage = getQuestion(
      QR.strings.quest_seals_gnome_first_question_correct, 2);
    s = this.quest.showNarrationMessage(s, updateMessage, false);
    return s; 
  }
  handleEvent(state, objID, eventType, extraParams) { 
    if (eventType == EventTypes.MESSAGE && extraParams['message']) {
      const answer = extraParams["message"];
      if (answer == "good_answer_2") { 
        this.quest.stepNo++;
      } else {
        state = this.quest.gnomeAttacks(state);
      }
    }
    return state; 
  }
}

class GnomeQuestion3 extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
  }
  firstShow(s) { 
    const updateMessage = getQuestion(
      QR.strings.quest_seals_gnome_second_question_correct, 3);
    s = this.quest.showNarrationMessage(s, updateMessage, false);
    return s; 
  }
  handleEvent(state, objID, eventType, extraParams) { 
    if (eventType == EventTypes.MESSAGE && extraParams['message']) {
      const answer = extraParams["message"];
      if (answer == "good_answer_3") { 
        this.quest.stepNo++;
      } else {
        state = this.quest.gnomeAttacks(state);
      }
    }
    return state; 
  }
}

class DefeatGnome extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
  }
  firstShow(s) { 
    let updateMessage = QR.strings.quest_seals_gnome_all_questions_correct;
    s = this.quest.showNarrationMessage(s, updateMessage, true);
    return s; 
  }
  prepare(s) { 
    s = new CollectableItem(
      null,
      new CollectPlant("item_keypart")).interact(s);
    this.quest.stepNo++;
    return s; 
  }
  cleanup(s) { return s; }
  handleEvent(state, objID, eventType, extraParams) { 
    return state; 
  }
}

class GetExp extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
  }
  prepare(s) { 
    s = {
      ...s,
      playerEgo: {
        ...s.playerEgo,
        knowledge: {
          ...s.playerEgo.knowledge,
          knownCreatures: {
            ...s.playerEgo.knowledge.knownCreatures,
            'creature_gnome': true
          },
        }
      }
    };
    s = {
      ...s,
      playerEgo: (PlayerEgo.addExperience(s.playerEgo, 700)),
    };
    s = window.gameController.showScreenState(s, "map")
    this.quest.stepNo++;
    return s; 
  }
}

export default class SealsQuest extends GenericQuest {

  constructor() {
    super();
    this.title_ = QR.strings.quest_seals_title;
    this.from_ = QR.strings.quest_seals_from;
    this.fromimg_ = QR.strings.quest_profile_anette;
    this.content_ = QR.strings.quest_seals_initial;

    this.steps = [
      new MeetGnome(this),
      new TalkOrFight(this),
      new GnomeQuestion1(this),
      new GnomeQuestion2(this),
      new GnomeQuestion3(this),
      new DefeatGnome(this),
      new GetExp(this),
      null
    ];
    this.stepNo = 0;

    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
    EventSystem.INSTANCE.registerListener(EventTypes.MESSAGE, this);
    EventSystem.INSTANCE.registerListener(EventTypes.KILLED, this);
  }

  initiateQuest(s) {
    this.stepNo = 0;
    s = super.initiateQuest(s);
    s = this.setAccepted(s);
    return s;
  }

  currentStep(s) {
    return this.steps[this.stepNo];
  }

  handleEvent(state, objID, eventType, extraParams) { 
    if (eventType == EventTypes.KILLED && 
        objID == "creature_gnome") { 
      // Player killed gnome
      this.stepNo = this.steps.length - 2;
    }
    state = super.handleEvent(state, objID, eventType, extraParams);
    return state; 
  }

  gnomeAttacks(state) {
    const gp_player = PlayerEgo.getGeoPoint(state.playerEgo);
    const puzzlingGnome = new Gnome();
    puzzlingGnome.attackWithoutWarning = true;
    puzzlingGnome.containsItem = "item_keypart";
    puzzlingGnome.setLocation(gp_player);
    puzzlingGnome.display(true);
    Game.getGame().worldContainer.addQuestObject(puzzlingGnome, true);
    return state;
  }
}

const getQuestion = (preamble, id) => {
  let resurcesQuestionID = 0,
      resurcesQuestionAnswersID = 0;
  switch (id) {
  case 1:
    resurcesQuestionID = QR.strings.quest_seals_gnome_question_1;
    resurcesQuestionAnswersID = QR.strings.quest_seals_gnome_question_1_answers;
    break;
  case 2:
    resurcesQuestionID = QR.strings.quest_seals_gnome_question_2;
    resurcesQuestionAnswersID = QR.strings.quest_seals_gnome_question_2_answers;
    break;
  case 3:
    resurcesQuestionID = QR.strings.quest_seals_gnome_question_3;
    resurcesQuestionAnswersID = QR.strings.quest_seals_gnome_question_3_answers;
    break;
  default:
    return null;
  }
  
  //Get answers and shuffle them
  const style = {
    marginTop: '15px',
  }
  const answersArray = Collections.shuffle(resurcesQuestionAnswersID)
      .map(i => (<li style={style}>{i}</li>));
  
  return (
  <div>
    {preamble}
    <br />
    {resurcesQuestionID}
    <ul>
      {answersArray}
    </ul>
  </div>);
}