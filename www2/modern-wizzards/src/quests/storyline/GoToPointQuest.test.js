import GoToPointQuest from './GoToPointQuest.js';
import Quest from '../Quest.js';
import Inventory from "../../gamelogic/Inventory.js";
import SpellFactory from '../../gamelogic/SpellFactory.js';
import Player from '../../gamelogic/creatures/Player.js'
import MainLoop from '../../gamelogic/MainLoop.js';
import Game from '../../Game.js';

import TestData from '../../test/TestData.js';
import FakeMapController from '../../test/FakeMapController.js';

describe('GoToPointQuest', () => {
  it('should be possible to complete', () => {
    const mapController = new FakeMapController();
    mapController.init();
    const mainLoop = new MainLoop();
    mainLoop.loopIteration = -1;
    let s = {
      playerEgo: TestData.getFullEgo(),
      gameState: Game.State.STATE_WALKING,
      quests: {
        GoToPointQuest: {
          acceptanceState: Quest.AcceptanceState.ACCEPTED, // FIXME
        },
      },
      worldContainer: {
        gatheredPlants: [ ],
        creaturesDefeated: [ ],
      },
    };

    const q = new GoToPointQuest();
    s = q.initiateQuest(s);
    s = q.onMessageOpen(s);

    expect(q.triggerZones.length).toBeGreaterThan(0);
    const gp = q.triggerZones[0].getGeoPoint();

    s = {
      ...s,
      playerEgo: {
        ...s.playerEgo,
        lat: gp.lat,
        lng: gp.lng,
      }
    };

    s = mainLoop.iterateGameState(s);
    expect(s.quests.GoToPointQuest.completedSteps.length).toBe(1);
    expect(s.quests.GoToPointQuest.completedSteps[0].id).toBe(1);

    // TODO: rest of the steps
  });

});
