import Quest from "../Quest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Game from "../../Game.js";
import PlayerEgo from "../../PlayerEgo.js";
import GeoPoint from "../../gis/GeoPoint.js";
import $log from "../../utils/log.js";
import RNG from "../../utils/RNG.js";
import MWToast from "../../ui/MWToast.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";
import TriggerZone from "../../gamelogic/world/TriggerZone.js";
import {GenericQuest, Step} from "../GenericQuest.js";

const rnd = new RNG();

class WalkToPointStep extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
    this.triggerZones = [ ];
  }
  prepare(state) { 
    const startingBearing = rnd.nextInt(90);
    const gp_player = PlayerEgo.getGeoPoint(state.playerEgo);
    
    this.triggerZones = [];
    for(let i = 0; i < 4; i++) 
    {
      const tz = new TriggerZone("GoToPointQuestZone", 
          new GeoPoint(
              gp_player.getLatitudeE6(), 
              gp_player.getLongitudeE6()), 
          40, 
          R.strings.target);
      tz.moveObject(120.0, 90 * i + startingBearing);
      tz.display(true);
      Game.getGame().worldContainer.addQuestObject(tz, true);
      this.triggerZones.push(tz);
    }
    return state;
  }
  cleanup(s) { 
    for(let tz of this.triggerZones) {
      Game.getGame().worldContainer.removeQuestObject(tz);
      tz.display(false);
    }
    this.triggerZones = [ ];
    return s;
  }
  handleEvent(state, objID, eventType, extraParams) { 
    if (objID == "GoToPointQuestZone" &&
        eventType == EventTypes.VISITED && 
        !PlayerEgo.isUnderEffectOfSpell(state.playerEgo, "spell_oob")) {
      // Entered first area with normal walking
      state = this.quest.pushStep(state, {
        id: 1,
        time: (new Date().getTime()),
        narrationTextName: "gotopoint_quest_narration_1"
      });
    }
    return state; 
  }
};

class OobToPointStep extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
    this.triggerZones = [ ];
    this.oob_hint_shown = false;
  }
  firstShow(state) {
    state = this.quest.showNewInQuestMessage(state, QR.strings.gotopoint_quest_narration_1);
    return state;
  }
  prepare(state) {
    //Preparing next part of quest. Using oob.
    const tz = new TriggerZone("GoToPointWithOOBQuestZone", 
        PlayerEgo.getGeoPoint(state.playerEgo), 
        40, 
        R.strings.target);
    tz.moveObject(200.0, 180);
    tz.display(true);
      
    Game.getGame().worldContainer.addQuestObject(tz, true);
    state = {
      ...state,
      playerEgo: PlayerEgo.addKnownGlobalSpell(state.playerEgo, "spell_oob"),
    };
    this.triggerZones.push(tz);
    return state;
  }
  cleanup(s) {
    for(let tz of this.triggerZones) {
      Game.getGame().worldContainer.removeQuestObject(tz);
      tz.display(false);
    }
    this.triggerZones = [ ];
    return s;
  }
  handleEvent(state, objID, eventType, extraParams) { 
    const quest = {...state.quests['GoToPointQuest']};
    if (objID == "GoToPointWithOOBQuestZone" &&
        eventType == EventTypes.VISITED && 
        PlayerEgo.isUnderEffectOfSpell(state.playerEgo, "spell_oob")) {
      state = this.quest.pushStep(state, {
        id: 2,
        time: (new Date().getTime()),
        narrationTextName: "gotopoint_quest_narration_2"
      });
      state = {
        ...state,
        playerEgo: PlayerEgo.endOutOfBody(state.playerEgo, false),
      };
    } else if(objID == "GoToPointWithOOBQuestZone" &&
        eventType == EventTypes.VISITED && 
        !PlayerEgo.isUnderEffectOfSpell(state.playerEgo, "spell_oob")) {
      if (!this.oob_hint_shown) {
        MWToast.toast(QR.strings.gotopoint_quest_have_to_use_oob);
        this.oob_hint_shown = true;
      }
    }

    return state; 
  } 
}

class OobWaitStep extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
  }
  firstShow(state) {
    state = this.quest.showNewInQuestMessage(state, QR.strings.gotopoint_quest_narration_2, null, null);
    return state;
  }
  handleEvent(state, objID, eventType, extraParams) { 
    if (objID == "spell_oob" &&
        eventType == EventTypes.BACK_IN_BODY) {
      state = this.quest.setCompleted(state);
      state = {
        ...state,
        playerEgo: (PlayerEgo.addExperience(state.playerEgo, 200)),
      };
    }
    return state;
  }
}

export default class GoToPointQuest extends GenericQuest {
  constructor() {
    super();
    this.title_ = QR.strings.gotopoint_quest_title;
    this.from_ = QR.strings.gotopoint_quest_from;
    this.fromimg_ = QR.strings.quest_profile_william;
    this.content_ = QR.strings.gotopoint_quest_content;
    this.triggerZones = [];
    this.steps = [
      new WalkToPointStep(this),
      new OobToPointStep(this),
      new OobWaitStep(this),
    ];

    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
    EventSystem.INSTANCE.registerListener(EventTypes.BACK_IN_BODY, this);
  }

  initiateQuest(s) {
    s = super.initiateQuest(s);
    s = this.setAccepted(s);
    return s;
  }

  currentStep(s) {
    const quest = s.quests.GoToPointQuest || { };
    const completedSteps = quest.completedSteps || [ ];
    const maxId = Math.max(0, Math.max(...completedSteps.map(step => step.id || 0)));
    return this.steps[maxId];
  }
}
