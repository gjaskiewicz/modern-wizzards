import Quest from "../Quest.js";
import QR from "../../QR.js";
import Game from "../../Game.js";

const TAG = "ThanksMessage";

export default class ThanksMessage extends Quest {

  constructor() {
    super();
    this.title_ = QR.strings.thanks_message_title;
    this.from_ = QR.strings.thanks_message_from;
    this.fromimg_ = QR.strings.quest_profile_bf;
  }

  initiateQuest(s) {
    s = this.setAccepted(s);
    return s;
  }

  restoreQuest(s) {
    this.setBasicMessage(QR.strings.thanks_message_content);
    this.setFullMessageText(this.getMessageWithNarrations(s));
    return s;
  }
  
  initiateQuestBackground(state) {
    state = this.setCompleted(state);
    Game.saveGame(state);
    return state;
  }
  
  onMessageOpen(state) {
    return this.initiateQuestBackground(state);
  }
}
