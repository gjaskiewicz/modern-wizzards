import Quest from "../Quest.js";
import {GenericQuest, Step} from "../GenericQuest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Game from "../../Game.js";
import PlayerEgo from "../../PlayerEgo.js";
import GeoPoint from "../../gis/GeoPoint.js";
import Inventory from "../../gamelogic/Inventory.js";
import RNG from "../../utils/RNG.js";
import $log from "../../utils/log.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";
import ManaZone from "../../gamelogic/world/ManaZone.js";

const TAG = "ManaSourcesQuest";
const rnd = new RNG();

class WalkToManaZone extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
  }
  firstShow(s) { return s; }
  prepare(s) { 
    const startingBearing = rnd.nextInt(90);
    const gp_player = PlayerEgo.getGeoPoint(s.playerEgo);
    this.quest.manaZones = [ ];
    
    for(let i = 0; i < 3; ++i) {
      const mz = new ManaZone(new GeoPoint(
        gp_player.getLatitudeE6(), 
        gp_player.getLongitudeE6()), 
        40
      );
      mz.moveObject(150.0, 120 * i + startingBearing);
      mz.display(true);
      Game.getGame().worldContainer.addQuestObject(mz, false);
      this.quest.manaZones.push(mz);
    }
    return s; 
  }
  cleanup(s) { return s; }
  handleEvent(state, objID, eventType, extraParams) { 
    if (ManaZone.manaZoneID == objID &&
        eventType == EventTypes.VISITED) {
      this.quest.stepNo++;
    }
    return state; 
  }
}

class WaitForMana extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
  }
  firstShow(s) { return s; }
  prepare(s) {
    if (PlayerEgo.isManaFull(s.playerEgo)) {
      this.quest.stepNo++;
    }
    return s;
  }
  cleanup(s) { return s; }
  handleEvent(s, objID, eventType, extraParams) { 
    if (objID == "player" && eventType == EventTypes.MANA_FULL) {
      this.quest.stepNo++;
    }
    return s; 
  }
}

class FullMana extends Step {
  constructor(quest) {
    super();
    this.quest = quest;
  }
  firstShow(s) { 
    s = this.quest.showNewInQuestMessage(s, QR.strings.quest_mana_sources_narration_mana_full);  
    return s; 
  }
  prepare(s) { 
    this.quest.stepNo++;

    s = {
      ...s,
      playerEgo: {
        ...s.playerEgo,
        knowledge: {
          ...s.playerEgo.knowledge,
          generateManaZones: true,
        }
      }
    };
    s = {
      ...s,
      playerEgo: (PlayerEgo.addExperience(s.playerEgo, 400)),
    };
    s = this.quest.pushStep(s, {
      id: 1,
      time: (new Date().getTime()),
      narrationTextName: "quest_mana_sources_narration_mana_full",
    });

    return s; 
  }
  cleanup(s) {
    for (let mz of this.quest.manaZones) {
      Game.getGame().worldContainer.removeQuestObject(mz);
      mz.display(false);
    }
    return s; 
  }
  handleEvent(state, objID, eventType, extraParams) { return state; }
}

export default class ManaSourcesQuest extends GenericQuest {
  constructor() {
    super();
    this.title_ = QR.strings.quest_mana_sources_title;
    this.from_ = QR.strings.quest_mana_sources_from;
    this.fromimg_ = QR.strings.quest_profile_anette;
    this.content_ = QR.strings.quest_mana_sources_initial;
    this.steps = [
      new WalkToManaZone(this),
      new WaitForMana(this),
      new FullMana(this),
      null,
    ];
    this.stepNo = 0;

    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
    EventSystem.INSTANCE.registerListener(EventTypes.MANA_FULL, this);
  }

  initiateQuest(s) {
    this.stepNo = 0;
    this.triggerZones = [];
    this.manaZones = [ ];
    s = super.initiateQuest(s);
    s = this.setAccepted(s);
    return s;
  }

  currentStep(s) {
    return this.steps[this.stepNo];
  }
}