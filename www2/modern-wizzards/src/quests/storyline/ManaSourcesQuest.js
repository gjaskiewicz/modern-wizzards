import Quest from "../Quest.js";
import R from "../../R.js";
import QR from "../../QR.js";
import Game from "../../Game.js";
import PlayerEgo from "../../PlayerEgo.js";
import GeoPoint from "../../gis/GeoPoint.js";
import Inventory from "../../gamelogic/Inventory.js";
import RNG from "../../utils/RNG.js";
import $log from "../../utils/log.js";
import EventTypes from "../../gamelogic/events/EventTypes.js";
import EventSystem from "../../gamelogic/events/EventSystem.js";
import ManaZone from "../../gamelogic/world/ManaZone.js";

const TAG = "ManaSourcesQuest";

export default class ManaSourcesQuest extends Quest {
  
  constructor() {
    super();
    this.title_ = QR.strings.quest_mana_sources_title;
    this.from_ = QR.strings.quest_mana_sources_from;
    this.fromimg_ = QR.strings.quest_profile_anette;
    this.manaZones = [ ];
    
    EventSystem.INSTANCE.registerListener(EventTypes.VISITED, this);
    EventSystem.INSTANCE.registerListener(EventTypes.MANA_FULL, this);
  }

  initiateQuest(s) {
    s = this.setAccepted(s);
    return s;
  }

  restoreQuest(s) {
    this.setBasicMessage(QR.strings.quest_mana_sources_initial);
    this.setFullMessageText(this.getMessageWithNarrations(s));
    return s;
  }
  
  initiateQuestBackground(state) {
    const rnd = new RNG();
    const startingBearing = rnd.nextInt(90);
    
    const gp_player = PlayerEgo.getGeoPoint(state.playerEgo);
    
    this.manaZones = [ ];
    
    for(let i = 0; i < 3; ++i) {
      const mz = new ManaZone(new GeoPoint(
        gp_player.getLatitudeE6(), 
        gp_player.getLongitudeE6()), 
        40
      );
      mz.moveObject(150.0, 120 * i + startingBearing);
      mz.display(true);
      Game.getGame().worldContainer.addQuestObject(mz, false);
      this.manaZones.push(mz);
    }

    return state;
  }
  
  handleEvent(state, objID, eventType, extraParams) {
    state = super.handleEvent(state, objID, eventType, extraParams);

    let quest = {...state.quests['ManaSourcesQuest']};
    quest.completedSteps = quest.completedSteps || [ ];
    
    if (this.getAcceptanceState(state) != Quest.AcceptanceState.ACCEPTED || 
        this.getProgressState(state) != Quest.ProgressState.INPROGRESS) {
      return state;
    }
    
    if (ManaZone.manaZoneID == objID &&
        eventType == EventTypes.VISITED && 
        !quest.completedSteps.length) {
      
      state = this.pushStep(state, {
        id: 1,
        time: (new Date().getTime()),
        narrationTextName: null
      });
      
      if(PlayerEgo.isManaFull(state.playerEgo)) {
        state = this.finishQuest(state);

        state = this.showNewInQuestMessage(state, QR.strings.quest_mana_sources_narration_mana_full);
        this.setFullMessageText(this.getMessageWithNarrations(state));
        state = this.setCompleted(state);
        state = {
          ...state,
          playerEgo: (PlayerEgo.addExperience(state.playerEgo, 400)),
        };
      }
    } else if(objID == "player" &&
          eventType == EventTypes.MANA_FULL && 
          quest.completedSteps.length == 1) {
      state = this.finishQuest(state);

      state = this.showNewInQuestMessage(state, QR.strings.quest_mana_sources_narration_mana_full);  
      this.setFullMessageText(this.getMessageWithNarrations(state));
      state = {
        ...state,
        playerEgo: (PlayerEgo.addExperience(state.playerEgo, 400)),
      };
    }

    return state;
  }
  
  onMessageOpen(state) {
    state = super.onMessageOpen(state);
    /* TODO migrate 
    if(progressState_ == ProgressState.INPROGRESS && 
        completedSteps_.size() < 1) { //Player started to play and didn't visited any triggered area.
      Game.turnOnOffNotificationForButton(R.id.btnMap, true);
    }
    */
    return state;
  }

  tryToRestoreQuest(state) {
    if (this.getProgressState(state) != Quest.ProgressState.INPROGRESS) {
      return null;
    }
    return this.initiateQuestBackground(state);
  }
  
  finishQuest(state) {
    if (this.getProgressState(state) == Quest.ProgressState.COMPLITED) {
      return state;
    }
    
    for (let mz of this.manaZones) {
      Game.getGame().worldContainer.removeQuestObject(mz);
      mz.display(false);
    }
    
    this.manaZones = [ ];
    state = this.setCompleted(state);
    state = {
      ...state,
      playerEgo: {
        ...state.playerEgo,
        knowledge: {
          ...state.playerEgo.knowledge,
          generateManaZones: true,
        }
      }
    };

    state = this.pushStep(state, {
      id: 2,
      time: (new Date().getTime()),
      narrationTextName: "quest_mana_sources_narration_mana_full",
    });
    
    Game.saveGame(state);
    return state;
  }
}
