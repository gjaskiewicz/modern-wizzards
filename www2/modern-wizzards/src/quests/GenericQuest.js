import Quest from "./Quest.js";
import GameSaver from "../utils/GameSaver.js";

export class Step {
  firstShow(s) { return s; }
  prepare(s) { return s; }
  cleanup(s) { return s; }
  handleEvent(state, objID, eventType, extraParams) { return state; }
}

export class GenericQuest extends Quest {
  constructor() {
    super();
    this.activeStep = null;
  }

  restoreQuest(state) {
    this.setBasicMessage(this.content_);
    this.setFullMessageText(this.getMessageWithNarrations(state));
    return this.initiateQuestBackground(state);
  }

  initiateQuestBackground(state) {
    if (!this.isAlreadyRead(state) ||
        this.getProgressState(state) != Quest.ProgressState.INPROGRESS ||
        this.getAcceptanceState(state) != Quest.AcceptanceState.ACCEPTED) {
      return state;
    }
    this.activeStep = this.currentStep(state);
    if (this.activeStep) {
      state = this.activeStep.prepare(state);
    }
    return state;
  }

  currentStep(s) { return null; }

  handleEvent(state, objID, eventType, extraParams) {
    if (this.getAcceptanceState(state) != Quest.AcceptanceState.ACCEPTED || 
        this.getProgressState(state) != Quest.ProgressState.INPROGRESS) {
      return state;
    }
    if (!this.activeStep) {
      return state;
    }
    state = this.activeStep.handleEvent(state, objID, eventType, extraParams);
    let newStep = this.currentStep(state);
    while (this.activeStep !== newStep) {
      state = this.activeStep.cleanup(state);
      this.activeStep = newStep;
      if (this.activeStep) {
        state = this.activeStep.firstShow(state);
        state = this.activeStep.prepare(state);
      } else {
        state = this.setCompleted(state);
        GameSaver.saveGame(state);
      }
      this.setFullMessageText(this.getMessageWithNarrations(state));
      newStep = this.currentStep(state);
    }
    return state;
  }
}

