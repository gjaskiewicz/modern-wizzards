import Game from "../Game.js";
import EventSystem from "../gamelogic/events/EventSystem.js";
import EventTypes from "../gamelogic/events/EventTypes.js";
import AllQuests from "./AllQuests.js";
import Quest from "./Quest.js";
import $log from '../utils/log.js';

const TAG = "QuestsEngine";

export default class QuestsEngine {

  static INSTANCE = new QuestsEngine();
  
  constructor() {
    this.nextQuestsCollection = {};
    this.extraQuestsCollection = {};
    this.timeToNextQuest = 20000; // [ms]
    this.DEV_MODE_NEXT_QUEST_IN = 4000; //In ms
    this.quests_ = [ ];
    this.reinitializedQuests = false;
    
    const next = (i1, i2) => {
      const questName = i1.getClassName();
      this.nextQuestsCollection[questName] = i2;
    };

    const extra = (i1, quests) => {
      const questName = i1.getClassName();
      this.extraQuestsCollection[questName] = quests;
    };

    next(AllQuests.InitialQuest, AllQuests.GoToPointQuest);
    next(AllQuests.GoToPointQuest, AllQuests.CollectPlantsQuest);
    next(AllQuests.CollectPlantsQuest, AllQuests.ManaSourcesQuest);
    next(AllQuests.ManaSourcesQuest, AllQuests.FleeingRabbitQuest);
    next(AllQuests.FleeingRabbitQuest, AllQuests.AlchemyQuest);
    next(AllQuests.AlchemyQuest, AllQuests.TeletransportQuest);
    next(AllQuests.TeletransportQuest, AllQuests.SealsQuest);
    next(AllQuests.SealsQuest, AllQuests.SpecialMissionQuest);
    next(AllQuests.SpecialMissionQuest, AllQuests.FinalMessage);
    next(AllQuests.FinalMessage, AllQuests.ThanksMessage);
    next(AllQuests.ThanksMessage, null);

    extra(AllQuests.TeletransportQuest, 
      [AllQuests.HauntedHouseQuest, AllQuests.LittleRedRidingHoodQuest]);
    
    EventSystem.INSTANCE.registerListener(EventTypes.QUEST_FINISHED, this);
    EventSystem.INSTANCE.registerListener(EventTypes.TICK, this);
    EventSystem.INSTANCE.registerListener(EventTypes.GAME_STARTED, this);
  }

  static getInstance() {
    return QuestsEngine.INSTANCE;
  }

  getCountOfUnopenedQuestMessages() {
    let counter = 0;
    for(let quest of this.quests_) {
      if(!quest.isAlreadyRead()) counter++;
    }
    return counter;
  }
  
  getQuestsColl() {
    return this.quests_;
  }
  
  addQuest(quest) {
    this.quests_ = [quest, ...this.quests_];
    // TODO: migrate Game.questsCollectionChanged();
  }
  
  getTimeToNextQuest(q, state) {
    if (state.playerEgo.isDev) {
      return this.DEV_MODE_NEXT_QUEST_IN;
    } else {
      return q.timeToNextQuest || this.timeToNextQuest;
    }
  }

  initQuest(state, quest) {
    if (!quest) {
      return state;
    }
    let nextID = quest.getClassName();
    return {
      ...state,
      quests: {
        ...state.quests,
        [nextID]: {
          ...Quest.makeDefault(),
          progressState: Quest.ProgressState.WAITING,
          waitUntil: (new Date().getTime() + this.getTimeToNextQuest(quest, state)),
        },
      }
    };
  }

  handleEvent(state, objID, eventType, extraParams) {
    if (eventType == EventTypes.TICK) {
      if (!this.reinitializedQuests) {
        return state;
      }
      return this.unlockWaitingQuests(state);
    } else if (eventType == EventTypes.QUEST_FINISHED) {
      let next = this.nextQuestsCollection[objID];
      let extra = this.extraQuestsCollection[objID];

      state = this.initQuest(state, next);
      for (let q of (extra || [])) {
        state = this.initQuest(state, q);
      }
      return state;
    } else if (eventType == EventTypes.GAME_STARTED) {
      this.reinitializedQuests = true;
      return this.reinitalizeQuests(state);
    } else {
      return state;
    }
  }

  reinitalizeQuests(state) {
    const quests = {...state.quests};
    for (let questName of Object.keys(quests)) {
      const quest = AllQuests[questName];
      // if (quest.getProgressState(state) == Quest.ProgressState.INPROGRESS) {
        const restoredState = quest.restoreQuest(state);
        if (restoredState === null) {
          throw `restore ${questName} quest failed`;
        } else {
          state = restoredState;
        }
      // }
    }
    if (!Object.keys(quests || { }).length) {
      return {
        ...state,
        quests: {
          InitialQuest: {
            ...Quest.makeDefault(),
            acceptanceState: Quest.AcceptanceState.NONE,
            progressState: Quest.ProgressState.WAITING,
            waitUntil: 0,
          },
        }
      }
    }
    return state;
  }

  unlockWaitingQuests(state) {
    let toNotify = [ ];
    const quests = {...state.quests};
    for (let questName of Object.keys(quests)) {
      const quest = state.quests[questName];
      const waitTo = quest.waitUntil || 0;
      const currentTime = new Date().getTime();
      if (quest.progressState == Quest.ProgressState.WAITING && waitTo <= currentTime) {
        quest.progressState = Quest.ProgressState.INPROGRESS;
        if (!AllQuests[questName]) {
          $log.e(TAG, `Quest '${questName}' not found`);
        } else {
          toNotify.push(AllQuests[questName]);
          state = AllQuests[questName].initiateQuest(state);
          state = AllQuests[questName].restoreQuest(state);
        }
      }
    }
    if (toNotify.length) {
      state = Game.getGame().showNewMessageAlert(state, toNotify[0]);
    }
    return state;
  }
}
