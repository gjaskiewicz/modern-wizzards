import Prolog from './prolog.js';
import SPELLBOOK from '../spellbook.js';

describe('Prolog', () => {

  it('should run on simple theory', () => {
    const prolog = new Prolog(`
      test(X) :- isA(X).
      isA(a).
      isA(aa).
    `);
    const res = prolog.runQuery('test(X)');
    expect(res.length).toBe(2);
    expect(res[0].length).toBe(1);
    expect(res[1].length).toBe(1);
    expect(res[0][0]['X']).toBe('a');
    expect(res[1][0]['X']).toBe('aa');
  });

  it('should run on lists theory', () => {
    const prolog = new Prolog(`
      test([X, Y, Z], 42) :- isA(X), isA(Y), isA(Z).
      isA(a).
    `);
    const res = prolog.runQuery('test([a, a, a], X)');
    expect(res.length).toBe(1);
    expect(res[0].length).toBe(1);
    expect(res[0][0]['X']).toBe('42');
  });

  describe('on spellbook', () => {
    let prolog;
    beforeEach(() => {
      prolog = new Prolog(SPELLBOOK);
    });

    it('should list spells', () => {
      const res = prolog.runQuery('spell(X)');
      expect(res.length).toBe(7);
      expect(res[0][0]['X']).toBe('potion_health');
      expect(res[1][0]['X']).toBe('potion_mana');
      expect(res[2][0]['X']).toBe('potion_stimulation');
      expect(res[3][0]['X']).toBe('potion_invisibility');
      expect(res[4][0]['X']).toBe('potion_antidote');
      expect(res[5][0]['X']).toBe('potion_perception');
      expect(res[6][0]['X']).toBe('potion_luckjelly');
    });

    it('should handle ingredients', () => {
      const res = prolog.runQuery('make([item_garlic, item_swallowweed, item_fernflower], X)');
      expect(res.length).toBe(1);
      expect(res[0].length).toBe(1);
      expect(res[0][0]['X']).toBe('potion_antidote');
    });

    it('should suggest ingredients', () => {
      const res = prolog.runQuery('make([A, item_garlic, item_fernflower], X)');
      const suggestions = res.map(suggest => suggest[1]['X']);
      expect(suggestions.includes('potion_health')).toBe(true);
      expect(suggestions.includes('potion_antidote')).toBe(true);
    });
  })
});

