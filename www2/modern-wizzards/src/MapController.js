import Color from "./Color.js";

export default class MapController {

  static inst = null;
  
  constructor()  {
    // TODO: cleanup
    // no-op
  }

  addClickHandler(handler) {
    return window.simpleMap.addClickHandler(handler);
  }
  
  displayObj(po)  {
    // TODO: cleanup
    // no-op
  }

  update(po) {
    // TODO: cleanup
    // no-op
  }
  
  dontDisplayObj(po) {
    // TODO: cleanup
    // no-op
  }

  showPlayerSight(state) {
    // TODO: cleanup
    // no-op
  }

  updateMap(state) {
    // TODO: cleanup
    // no-op
  }

  resize() {
    // TODO: cleanup
    // no-op
  }
  
  static getInst()  {
    if(MapController.inst == null) {
      MapController.init();
      // throw "MapController was not initialized";
    }
    return MapController.inst;
  }

  static init(/* ignore params */) {
    MapController.inst = new MapController();
  }
}
