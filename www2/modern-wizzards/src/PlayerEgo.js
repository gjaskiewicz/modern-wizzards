import $log from "./utils/log.js";
import MWToast from "./ui/MWToast.js";
import R from "./R.js";
import GeoPoint from "./gis/GeoPoint.js";
import EventTypes from "./gamelogic/events/EventTypes.js";
import EventSystem from "./gamelogic/events/EventSystem.js";
import GameState from "./GameState.js";

const TAG = "PlayerEgo";

export default class PlayerEgo {

  static addHealthPoints(playerEgo, hp, force=false) {
    $log.d("PlayerEgo", `Adding ${hp} HP`);
    var newHp = playerEgo.health;
    if (force) {
      newHp += hp;
    } else {
      newHp = Math.min(
        Math.max(playerEgo.maxHealth, newHp),
        newHp + hp);
    }
    newHp = Math.max(0, newHp);
    return {...playerEgo, health: newHp};
  }

  static addManaPoints(state, mana, force=false) {
    var newMana = state.playerEgo.mana;
    if (force) {
      newMana += mana;
    } else if(newMana >= state.playerEgo.maxMana && mana >= 0) {
      return { ...state };
    } else {
      newMana = Math.min(
        Math.max(state.playerEgo.maxMana, newMana),
        newMana + mana);
      if (newMana == state.playerEgo.maxMana) {
        const nameID = "player"; // TODO: extract
        state = EventSystem.INSTANCE.notifyListenersState(state, nameID, EventTypes.MANA_FULL, null);
        MWToast.toast(R.strings.player_mana_full);
      }
    }
    newMana = Math.max(0, newMana);
    return {
      ...state,
      playerEgo: {
        ...state.playerEgo,
        mana: newMana
      }
    };
  }

  static isManaFull(playerEgo) {
    return playerEgo.mana == playerEgo.maxMana;
  }

  static getGeoPoint(playerEgo) {
    return new GeoPoint(playerEgo.lat, playerEgo.lng);
  }

  static getPlayerLocation(playerEgo) {
    if(PlayerEgo.isUnderEffectOfSpell(playerEgo, "spell_oob")) {
      return new GeoPoint(playerEgo.oob_lat, playerEgo.oob_lng);
    }
    return new GeoPoint(playerEgo.lat, playerEgo.lng);
  }

  /** dodaje exp i troszczy sie o levelowanie
   * @param howMuch ile expa
   */
  static addExperience(playerEgo, howMuch) {
    if(this.getExpForLevel(playerEgo.expLevel + 1) <= playerEgo.experience + howMuch) {
      playerEgo = PlayerEgo.levelUp(playerEgo);
    }
    return {...playerEgo, experience: (playerEgo.experience + howMuch)};
  }

  static levelUp(playerEgo) {
    const expLevel = playerEgo.expLevel + 1;
    MWToast.toast(R.strings.player_levelup); //, Toast.LENGTH_SHORT);
    // większe limity mana i hp
    let factor = playerEgo.mana / playerEgo.maxMana;
    const maxMana = playerEgo.maxMana + 10;
    const mana = factor * maxMana;
    factor = playerEgo.health / playerEgo.maxHealth;
    const maxHealth = playerEgo.maxHealth + 10;
    const health = factor * maxHealth;
    // większe pole widzenia
    const sightRange = 300 + expLevel * 30;
    // szybciej regenetuje 
    const manaRegenFactor = playerEgo.manaRegenFactor + 0.5;
    //odblokowac kolejne czary...
    if(expLevel == 2) {
      playerEgo = PlayerEgo.addKnownFightSpell(playerEgo, "spell_fireball");
    } if(expLevel == 3)  {
      playerEgo = PlayerEgo.addKnownFightSpell(playerEgo, "spell_astralshield");
    } if(expLevel == 4)  {
      playerEgo = PlayerEgo.addKnownFightSpell(playerEgo, "spell_electrokinesis");
    }

    return {...playerEgo,
      expLevel,
      mana, maxMana,
      health, maxHealth,
      sightRange,
      manaRegenFactor
    };
  }

 /**
  * Dodaje nowy czar do listy umianych. Zapewnia nieduplikowanie.
  * @param spellID id czaru. 
  */
  static addKnownFightSpell(playerEgo, spellID) {
    if (playerEgo.knownFightSpells[spellID]) {
      return { ...playerEgo };
    }
    return {
      ...playerEgo,
      knownFightSpells: {
        ...playerEgo.knownFightSpells, 
        [spellID]: true 
      }
    };
  }

  static addKnownGlobalSpell(playerEgo, spellID) {
    if (playerEgo.knownGlobalSpells[spellID]) {
      return playerEgo;
    }
    return {
      ...playerEgo,
      knownGlobalSpells: {
        ...playerEgo.knownGlobalSpells,
        [spellID]: true,
      }
    };
  }

  static getExpForLevel(level) {
    let sum = 0;
    for(let i = 1; i <= level; i++) {
      sum += i;
    }
    return sum * 1000;
  }

  static removeFinishedSpells(playerEgo) {
    const underEffectOfSpell = 
        playerEgo.underEffectOfSpell.filter(spell => spell.turnDuration > 0);
    return {
      ...playerEgo,
      underEffectOfSpell
    }
  }

  static isUnderEffectOfSpell(playerEgo, spell) {
    return playerEgo.underEffectOfSpell.some(s => s.nameID == spell);
  }

  static endOutOfBody(playerEgo, immediate) {
    for (let spell of playerEgo.underEffectOfSpell) {
      if(spell.nameID == "spell_oob") {
        spell.forceReturnToBody(immediate); //setting return to body
      }
    }
    return playerEgo;
  }

  static getSightRange(playerEgo) {
    if (PlayerEgo.isUnderEffectOfSpell(playerEgo, "potion_perception")) { 
      return 550; 
    }
    return playerEgo.sightRange;
  }
  
  /**
   * Wskrzesza gracza. 
   */
  static resurect(state) {
    MWToast.toast(R.strings.player_resurected);
    return {
      ...state,
      gameState: GameState.STATE_WALKING,
      playerEgo: {
        ...state.playerEgo,
        health: 10,
        mana: 10,
      }
    };
  }
}
