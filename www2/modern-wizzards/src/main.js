import Game from './Game.js';
import Quest from './quests/Quest.js';

export default class GameController {

  constructor() {
    this.dispatch = () => { console.log("Dispatch not configured!"); }
  }

  defaultPlayerEgo() {
    return {
      setLocation: false,
      lat: 0,
      lng: 0,
      health: 100,
      mana: 100,
      maxHealth: 100,
      maxMana: 100,
      inventory: [ ],
      sightRange: 300,
      underEffectOfSpell: [ ],
      /** ilosc pkt doswiadczenia */
      experience: 0,
      /** obecny level */
      expLevel: 1,
      /** factor do regeneracji mana */
      manaRegenFactor: 2,
      /** czy juz zginal kiedys */
      killedOnce: false,
      /** czy ukonczyl pierwsza walke */
      firstFightDone: false,

      knownFightSpells: {
        "spell_physical": true,
      },
      knownGlobalSpells: { },
      knowledge: { // a.k.a GameContent
        isAlchemyActive: false,
        knownCreatures: { },
        knownPlants: { },
        knownPotions: { },
        generateManaZones: false,
      }
    };
  }

  defaultState() {
    return {
      loginData: {
        isLogged: false,
        photoURL: null,
        displayName: null,
      },
      gameState: Game.State.STATE_INIT,
      playerEgo: this.defaultPlayerEgo(),
      achivments: { 
        granted: [ ],
        daysInARow: 0,
        lastDay: 0,
      },
      ui: {
        menuOpen: false,
        activeScreen: 'login',
        previousScreens: [ ],
        activeOverlay: null,
        inventoryListFilter: undefined,
        spellListFilter: undefined,
        inventoryDetailsItem: undefined,
        genericOverlayParams: { },
        alertItem: undefined,
        fight: undefined,
        fightCreature: undefined,
      },
      /** oznacza czy gra juz kiedys byla uruchomiona? */
      isInitialized: false,
      /** czy lokalizacja została ustalona od ostatniego uruchomienia. Cholernie ważne przy generacji */
      locationIsNotSet: true,
      saveData: {
        lastGameSaveMs: null,
      },
      options: {
        GENERATE_TEST_DATA: false,
        USE_GPS: true,              // jesli false, uzyta bedzie domyslna lokalizacja (budojo)
        PLAY_SOUNDS: true,
        USE_VIBRATIONS: true,
        LOCK_VIEW_TO_LOCATION: true,
        SAVE_AND_LOAD: true,
        DEVELOPMENT_MODE: false,    // Faster loading new quests
        LOAD_SAVE_FROM_FILE: false, // Reset with every start of application even when file doesn't exists
        MAP_VISUALIZATION: 0,
        LANGUAGE_ID: 100,
        // used
        AUTOSAVE: 60,
        LOCK_ORIENTATION: true, 
        DISABLE_PRECISION_LOCATION: false,
        TERMS_OF_USE_ACCEPTED: false,
      },
      quests: {
/*
        GoToPointQuest: {
          ...Quest.makeDefault(),
          acceptanceState: Quest.AcceptanceState.ACCEPTED,
          progressState: Quest.ProgressState.INPROGRESS,
        },
        CollectPlantsQuest: {
          ...Quest.makeDefault(),
          acceptanceState: Quest.AcceptanceState.ACCEPTED,
          progressState: Quest.ProgressState.INPROGRESS,
        },
        ManaSourcesQuest: {
          ...Quest.makeDefault(),
          acceptanceState: Quest.AcceptanceState.ACCEPTED,
          progressState: Quest.ProgressState.INPROGRESS,
        },
        FleeingRabbitQuest: {
          ...Quest.makeDefault(),
          acceptanceState: Quest.AcceptanceState.ACCEPTED,
          progressState: Quest.ProgressState.INPROGRESS,
        },
        AlchemyQuest: {
          ...Quest.makeDefault(),
          acceptanceState: Quest.AcceptanceState.ACCEPTED,
          progressState: Quest.ProgressState.INPROGRESS,
        },
        TeletransportQuest: {
          ...Quest.makeDefault(),
          acceptanceState: Quest.AcceptanceState.ACCEPTED,
          progressState: Quest.ProgressState.INPROGRESS,
        },
        SealsQuest: {
          ...Quest.makeDefault(),
          acceptanceState: Quest.AcceptanceState.ACCEPTED,
          progressState: Quest.ProgressState.INPROGRESS,
        },
        SpecialMissionQuest: {
          ...Quest.makeDefault(),
          acceptanceState: Quest.AcceptanceState.ACCEPTED,
          progressState: Quest.ProgressState.INPROGRESS,
        },
        FinalMessage: {
          ...Quest.makeDefault(),
          acceptanceState: Quest.AcceptanceState.ACCEPTED,
          progressState: Quest.ProgressState.INPROGRESS,
        },
        ThanksMessage: {
          ...Quest.makeDefault(),
          acceptanceState: Quest.AcceptanceState.ACCEPTED,
          progressState: Quest.ProgressState.INPROGRESS,
        },
        HauntedHouseQuest: {
          ...Quest.makeDefault(),
        },
        LittleRedRidingHoodQuest: {
          ...Quest.makeDefault(),
          progressState: Quest.ProgressState.WAITING,
        },
*/
      },
      worldContainer: {
        saveDate: '1970-01-01',
        gatheredPlants: [ ],
        creaturesDefeated: [ ],
      },
      debug: {
        locationUpdates: 0,
      }
    }
  }

  merge(oldState, newState) {
    const res = {...oldState, ...newState };

    const mergeSubprop = function(prop) {
      if (newState[prop]) {
        res[prop] = {...oldState[prop], ...newState[prop]}
      }
    };

    mergeSubprop("playerEgo");
    res.playerEgo.knowledge = {
        ...oldState.playerEgo.knowledge, 
        ...newState.playerEgo.knowledge
    };

    mergeSubprop("loginData");
    mergeSubprop("saveData");
    mergeSubprop("options");
    mergeSubprop("worldContainer");
    mergeSubprop("achivments");
    mergeSubprop("ui");

    return res;
  }

  toggleMenu(isOpen) {
    this.dispatch(s => ({ ...s, ui: {...s.ui, menuOpen: isOpen} }));
  }

  signIn(user) {
    this.dispatch(s => {
      s = this.showScreenState(s, 'map');
      return {
        ...s, 
        loginData: {
          isLogged: true,
          photoURL: user.photoURL,
          displayName: user.displayName,
        }
      };
    });
  }

  applyToState(stateTransform, callback=undefined) {
    let newState = null;
    this.dispatch(
      s => {
        newState = this.merge(s, stateTransform(s)); 
        return newState;
      }, 
      () => callback && callback(newState));
  }

  applyToEgo(egoTransform) {
    this.dispatch(s => ({ ...s, playerEgo: egoTransform(s.playerEgo)}));
  }

  showScreenState(s, screen) {
    return {
      ...s,
      ui: {
        ...s.ui,
        activeScreen: screen,
        previousScreens: [ ],
      }
    };
  }

  showNextScreenState(s, screen, extraArgs={ }) {
    return {
      ...s,
      ui: {
        ...s.ui,
        previousScreens: [ ...s.ui.previousScreens, s.ui.activeScreen ],
        activeScreen: screen,
        ...extraArgs,
      },
    };
  }

  showBackScreenState(s) {
    if (s.ui.previousScreens.length == 0) {
      return this.showScreenState(s, 'map');
    } else {
      const previousScreens = [ ...s.ui.previousScreens ];
      const activeScreen = previousScreens.pop();
      return {
        ...s,
        ui: {
          ...s.ui,
          previousScreens,
          activeScreen,
        },
      };
    }
  }

  showScreen(screen) {
    this.dispatch(s => this.showScreenState(s, screen));
  }

  showOverlayState(s, overlay) {
    return { ...s, ui: {...s.ui, activeOverlay: overlay} };
  }

  showOverlay(overlay) {
    this.dispatch(s => this.showOverlayState(s, overlay));
  }
}

