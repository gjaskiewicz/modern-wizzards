import MapController from "../MapController.js";
import MWToast from "../ui/MWToast.js";

export default class CodesController {
  
  static parseCode(state, code) {
    if(code == "#move") {
      const newState = {...state, options: {...state.options, USE_GPS: false} };
      // Game.mainActivity.stopGPS();
      MWToast.toast("SELECT MOVE POSITION");

      let remove = MapController.getInst().addClickHandler(latLng => {
        window.gameController.applyToState(s => {
          return {
            ...s,
            playerEgo: {
              ...s.playerEgo,
              lat: latLng.lat(),
              lng: latLng.lng(),
            }
          }
        });
        remove();
      });

      // Options.saveOptions(Game.mainActivity);
      return state;
    }
    if(code == "#nogps") {
      const newState = {...state, options: {...state.options, USE_GPS: false} };
      // Game.mainActivity.stopGPS();
      MWToast.toast("GPS: " + newState.options.USE_GPS);
      // Options.saveOptions(Game.mainActivity);
      return newState;
    }
    else if(code == "#usegps") {
      const newState = {...state, options: {...state.options, USE_GPS: true} };
      MWToast.toast("GPS: " + newState.options.USE_GPS);
      // Options.saveOptions(Game.mainActivity);
    }
    else if(code == "#resurect") {
/*
      Player.getInst().resurect();
      Player.getInst().addManaPoints(1000, false);
      Player.getInst().addHealthPoints(1000, false);
*/
    }
    else if(code == "#giveall") {
      const playerEgo = state.playerEgo;
/* TODO: migrate
      playerEgo.inv.addPlant(new CollectPlant("item_daisyflower"));
      playerEgo.inv.addPlant(new CollectPlant("item_daisyflower"));
      playerEgo.inv.addPlant(new CollectPlant("item_fernflower"));
      playerEgo.inv.addPlant(new CollectPlant("item_fernflower"));
      playerEgo.inv.addPlant(new CollectPlant("item_4clover"));
      playerEgo.inv.addPlant(new CollectPlant("item_4clover"));
      playerEgo.inv.addPlant(new CollectPlant("item_rozkovnik"));
      playerEgo.inv.addPlant(new CollectPlant("item_rozkovnik"));
      playerEgo.inv.addPlant(new CollectPlant("item_garlic"));
      playerEgo.inv.addPlant(new CollectPlant("item_garlic"));
      playerEgo.inv.addPlant(new CollectPlant("item_swallowweed"));
      playerEgo.inv.addPlant(new CollectPlant("item_swallowweed"));
      playerEgo.inv.addPlant(new CollectPlant("item_cornerstone"));
      playerEgo.inv.addPlant(new CollectPlant("item_smurfblood"));
      playerEgo.inv.addPlant(new CollectPlant("item_cateye"));
      playerEgo.inv.addPlant(new CollectPlant("item_bunnyleg"));
      playerEgo.inv.addPlant(new CollectPlant("item_ectoplasma"));
      playerEgo.inv.addPlant(new CollectPlant("item_mandragora"));
      playerEgo.inv.addPlant(new CollectPlant("item_szczeciogon"));
      
      playerEgo.inv.addMixture(new SpellToken(new HealingPotion()));
      playerEgo.inv.addMixture(new SpellToken(new ManaPotion()));
      
      GameContent.getInstance().setAllAllowed();
*/
      MWToast.toast("All given!");
    }
    else  {
      MWToast.toast("WRONG CODE");
    }
    return state;
  }
}
