import GeoPoint from './GeoPoint.js';
import TestData from "../test/TestData.js";

describe('Geo Point', () => {
  it('should construct from microdegrees', () => {
    const warsaw1 = new GeoPoint(...TestData.cities.warsaw);
    const warsaw2 = new GeoPoint(
      TestData.cities.warsaw[0] * 1E6,
      TestData.cities.warsaw[1] * 1E6,
    );
    expect(warsaw1.getLatitude()).toEqual(warsaw2.getLatitude());
    expect(warsaw1.getLongitude()).toBe(warsaw2.getLongitude());
  });

  it('should get microdegrees', () => {
    const warsawGp = new GeoPoint(...TestData.cities.warsaw);
    expect(warsawGp.getLatitudeE6()).toBe(52233333);
    expect(warsawGp.getLongitudeE6()).toBe(21016667);
  });
});
