import Units from './Units.js';
import GeoPoint from './GeoPoint.js';
import TestData from "../test/TestData.js";

describe('GIS Units', () => {
  const warsaw = new GeoPoint(...TestData.cities.warsaw);
  const radom = new GeoPoint(...TestData.cities.radom);
  const auckland = new GeoPoint(...TestData.cities.auckland);
  const sanFransisco = new GeoPoint(...TestData.cities.sanFransisco);
  const tokyo = new GeoPoint(...TestData.cities.tokyo);
  const all = [warsaw, radom, auckland, sanFransisco, tokyo];

  describe('optimized distance', () => {
    it('should be reverisble', () => {
      for (let city1 of all) {
        for (let city2 of all) {
          var d12 = Units.optimizedDistance(city1, city2);
          var d21 = Units.optimizedDistance(city2, city1);
          expect(d12 - d21).toBe(0);
        }
      }
    });

    it('should be approx good', () => {
      var d = Units.optimizedDistance(warsaw, radom);
      expect(d).toBe(92233); // ~100 km
    });
  });

  describe('exact distance', () => {
    it('should be reverisble', () => {
      for (let city1 of all) {
        for (let city2 of all) {
          var d12 = Units.distance(city1.getLatitude(), city1.getLongitude(),
                                   city2.getLatitude(), city2.getLongitude());
          var d21 = Units.distance(city2.getLatitude(), city2.getLongitude(),
                                   city1.getLatitude(), city1.getLongitude());
          expect(d12 - d21).toBe(0);
        }
      }
    });

    it('should be approx good', () => {
      var d = Units.distance(warsaw.getLatitude(), warsaw.getLongitude(),
                             radom.getLatitude(), radom.getLongitude());
      expect(d).toBe(93234.23002056502); // ~100 km
    });
  });
});
