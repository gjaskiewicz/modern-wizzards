// android.location.Location

export default class Location {
  constructor(name, lat=undefined, lng=undefined) {
    this.nameID = name;
    this.lat = lat;
    this.lng = lng;
  }

  setLatitude(lat) {
    this.lat = lat;
  }

  setLongitude(lng) {
    this.lng = lng;
  }

  getLatitude() {
    return this.lat;
  }

  getLongitude() {
    return this.lng;
  }
}
