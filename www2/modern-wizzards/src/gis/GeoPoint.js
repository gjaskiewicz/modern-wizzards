export default class GeoPoint {
  constructor(lat, lng) {
    if (Math.abs(lat) > 360) {
      // bo w Javie jest typ int/float
      lat = lat / 1E6;
    }
    if (Math.abs(lng) > 360) {
      lng = lng / 1E6;
    }
    this.lat = lat;
    this.lng = lng;
  } 

  getLatitude() {
    return this.lat;
  }

  getLongitude() {
    return this.lng;
  }

  getLatitudeE6() {
    return this.lat * 1E6;
  }

  getLongitudeE6() {
    return this.lng * 1E6;
  }
}
