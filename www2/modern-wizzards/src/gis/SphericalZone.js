import Units from "./Units.js";
import MoveLocation from "../utils/MoveLocation.js";

export default class SphericalZone {
  
  constructor(/* GeoPoint */ centerLoc, rad) {
    this.center = centerLoc;
    // promien w metrach
    this.radius = Math.floor(rad);
    
  }

  intersects(/* GeoPoint */ loc) {
    if(this.center == null) { 
      return false; 
    }
    return Units.optimizedDistanceCheck(loc, this.center, this.radius);
  }

  getCenter() {
    return this.center;
  }

  moveCenter(distance, bearing) {
      this.center = MoveLocation.moveGeoPoint(this.center, distance, bearing);
  }

  setCenter(/* GeoPoint */ newCenter)  {
    this.center = newCenter;
  }
}
