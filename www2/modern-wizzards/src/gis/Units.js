import MWMath from '../utils/MWMath.js';
import GeoPoint from './GeoPoint.js';
import $log from '../utils/log.js';

export default class Units {

  // ile metrow przypada na mikrostopien LAT (20 000 000m obwodu ziemi / 180 000 000 microstopni)
  static MICROLATITUDE_TO_METER = 0.11;

  // do przeliczenia
  static MICROLONGITUDE_TO_METER = 0;
	
  /**
   * @return ile metrow przypada na mikrostopien
   */
  static latitudeToMeters() {
    return Units.MICROLATITUDE_TO_METER;
  }
	
  /**
   * ile metrow przypada na mikrostopien
   * @param latitude dla jakiej jest obliczane
   * @return ile metrow przypada na mikrostopien
   */
  static longitudeToMeters(latitude) {
    if (Units.MICROLONGITUDE_TO_METER == 0) {
      // przelicz
      Units.MICROLONGITUDE_TO_METER = Math.cos(MWMath.toRadians(latitude/1E6))*40 / 360 ;
      $log.i("PRZELICZONE MICROLONGITUDE_TO_METER = " + Units.MICROLONGITUDE_TO_METER);
    }
    return Units.MICROLONGITUDE_TO_METER;
  }
	
  /**
   * zwraca true jesli dystans jest mniejszy niz podany, w metrach
   * @param loc1 pkt1
   * @param loc2 pkt1
   * @param meterDistance sprawdzany dystans
   * @return true jesli dystans jest mniejszy
   */
  static optimizedDistanceCheck(/* GeoPoint */ loc1, /* GeoPoint */ loc2, meterDistance) {
    if(Math.abs(loc1.getLatitudeE6() - loc2.getLatitudeE6()) * Units.latitudeToMeters() > meterDistance ) {
      return false;
    } else if(Math.abs(loc1.getLongitudeE6() - loc2.getLongitudeE6()) * 
        Units.longitudeToMeters(loc1.getLatitudeE6()) > meterDistance) {
      return false;
    } else if(Units.optimizedDistance(loc1,loc2) > meterDistance) {
      return false;
    }
    return true;
  }
	
  /**
   * zwraca przyblizona odleglosc pomiedzy punktami, w metrach
   * @param loc1
   * @param loc2
   * @return
   */
  static optimizedDistance(/* GeoPoint */ loc1,/* GeoPoint */ loc2) {
    const latDiff = loc1.getLatitudeE6() - loc2.getLatitudeE6();
    const lngDiff = loc1.getLongitudeE6() - loc2.getLongitudeE6();
    return Math.floor(Math.sqrt(
      Math.pow(latDiff * Units.latitudeToMeters(), 2) +
      Math.pow(lngDiff * Units.longitudeToMeters(loc2.getLatitudeE6()), 2)
    ));
  }
	
  static toMicroDeg(degs) {
    return Math.floor(degs * 1E6);
  }
	
  // XXX: to nieprawda - poprawic
  static metersToDegLat(m) {
    return m / 171196.672;
  }

  static metersToDegLng(m) {
    return m / 111196.672;
  }
	
  static degLatToMeters(relLat) {
    return relLat * 111196.672;
  }

  static degLngToMeters(lat, relLng) {
    return relLng * 111196.672 * (1/ Math.cos(MWMath.toRadians(lat)));
  }
	
  /**
   * zwraca dokładny dystans pomiędzy punktami z uwzgl. krzywizny ziemi
   * @deprecated
   */
  static distance(lat1, lng1, lat2, lng2) {
    let R = 6371; // Radius of the earth in km
    const dLat = MWMath.toRadians(lat2 - lat1);
    const dLon = MWMath.toRadians(lng2 - lng1); 
    const a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(MWMath.toRadians(lat1)) * Math.cos(MWMath.toRadians(lat2)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2); 
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    const d = R * c * 1000; // Distance in meters
    return d;
  }
	
  static /* GeoPoint */ coordsToLocation(lat, lng) {
    //const lngDta = Math.floor(lng * 1E6);
    //const latDta = Math.floor(lat * 1E6);
    return new GeoPoint(lat, lng);
  }
}
