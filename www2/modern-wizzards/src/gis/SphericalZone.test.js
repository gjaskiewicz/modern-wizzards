import SphericalZone from './SphericalZone.js';
import GeoPoint from './GeoPoint.js';
import TestData from "../test/TestData.js";

describe('Geo Point', () => {
  it('should intersect', () => {
    const warsawZone = new SphericalZone(
      new GeoPoint(...TestData.cities.warsaw),
      100000 /* 100km */);
    const radomGp = new GeoPoint(...TestData.cities.radom);
    expect(warsawZone.intersects(radomGp)).toBe(true);
  });

  it('should move center', () => {
    const warsawZone = new SphericalZone(
      new GeoPoint(...TestData.cities.warsaw),
      20000 /* 20km */);
    const radomGp = new GeoPoint(...TestData.cities.radom);

    expect(warsawZone.intersects(radomGp)).toBe(false);
    warsawZone.moveCenter(100000, 180);
    expect(warsawZone.intersects(radomGp)).toBe(true);
  });

  it('should set center', () => {
    const someZone = new SphericalZone(
      new GeoPoint(...TestData.cities.auckland),
      200000 /* 200km */);
    const radomGp = new GeoPoint(...TestData.cities.radom);

    expect(someZone.intersects(radomGp)).toBe(false);
    someZone.setCenter(new GeoPoint(...TestData.cities.warsaw));
    expect(someZone.intersects(radomGp)).toBe(true);
  });
});
