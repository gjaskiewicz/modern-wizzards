import GameController from "./main.js";
import PlayerEgo from "./PlayerEgo.js";

describe('PlayerEgo', () => {
  let gc = new GameController();

  it('should add health', () => {
    let ego = gc.defaultPlayerEgo();
    expect(ego.health).toBe(100);

    ego = PlayerEgo.addHealthPoints(ego, 10);
    expect(ego.health).toBe(100);

    ego = PlayerEgo.addHealthPoints(ego, -10);
    expect(ego.health).toBe(90);

    ego = PlayerEgo.addHealthPoints(ego, 20, true);
    expect(ego.health).toBe(110);
  });

  it('should add mana', () => {
    let s = {
      playerEgo: gc.defaultPlayerEgo()
    };
    expect(s.playerEgo.mana).toBe(100);

    s = PlayerEgo.addManaPoints(s, 10);
    expect(s.playerEgo.mana).toBe(100);

    s = PlayerEgo.addManaPoints(s, -10);
    expect(s.playerEgo.mana).toBe(90);

    s = PlayerEgo.addManaPoints(s, 20, true);
    expect(s.playerEgo.mana).toBe(110);
  });

  it('should add exp', () => {
    let ego = gc.defaultPlayerEgo();
    expect(ego.experience).toBe(0);

    for (let i = 1; i < 10; i++) {
      ego = PlayerEgo.addExperience(ego, 1);
      expect(ego.experience).toBe(i);
    }
  });

  it('should level up', () => {
    let ego = gc.defaultPlayerEgo();
    expect(ego.expLevel).toBe(1);
    let lastEgo = ego;
    for (let i = 2; i < 10; i++) {
      const neededExp = PlayerEgo.getExpForLevel(i);
      lastEgo = ego;
      ego = PlayerEgo.addExperience(ego, neededExp - ego.experience);
      expect(ego.expLevel).toBe(i);
      expect(ego.health).not.toBeLessThan(lastEgo.health);
      expect(ego.maxHealth).not.toBeLessThan(lastEgo.maxHealth);
      expect(ego.mana).not.toBeLessThan(lastEgo.mana);
      expect(ego.maxMana).not.toBeLessThan(lastEgo.maxMana);
      expect(ego.sightRange).not.toBeLessThan(lastEgo.sightRange);
      expect(Object.keys(ego.knownFightSpells).length).not.toBeLessThan(
        Object.keys(lastEgo.knownFightSpells).length);
      expect(ego.expLevel).not.toBeLessThan(lastEgo.expLevel);
    }
  });
});
