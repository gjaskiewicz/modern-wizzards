import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import ToplevelErrorCatcher from './ToplevelErrorCatcher.js';
import registerServiceWorker from './registerServiceWorker';

const app = (
    <ToplevelErrorCatcher>
        <App />
    </ToplevelErrorCatcher>
);
ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
