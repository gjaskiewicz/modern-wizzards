import React from 'react';

import res_ilustration_6 from "./assets/quest/ilustration_6.png";
// generated with: find assets | grep "\." | awk '{ print "import res_"$1" from \"./"$1"\";" }' | sed 's/assets\/[a-z]*\///' | sed 's/\.[a-z][a-z][a-z]//'
import res_achiv_mixtures from "./assets/achivments/achiv_mixtures.png";
import res_achiv_juggle from "./assets/achivments/achiv_juggle.png";
import res_achiv_gamefinished from "./assets/achivments/achiv_gamefinished.png";
import res_achiv_distvariety from "./assets/achivments/achiv_distvariety.png";
import res_achiv_medit from "./assets/achivments/achiv_medit.png";
import res_achiv_disttraveled from "./assets/achivments/achiv_disttraveled.png";
import res_achiv_hunter from "./assets/achivments/achiv_hunter.png";
import res_achiv_plants from "./assets/achivments/achiv_plants.png";
import res_achiv_days from "./assets/achivments/achiv_days.png";
import res_achiv_unknown from "./assets/achivments/achiv_unknown.png";
import res_potion_mana_adv from "./assets/potions/potion_mana_adv.png";
import res_potion_antidote from "./assets/potions/potion_antidote.png";
import res_potion_invisibility from "./assets/potions/potion_invisibility.png";
import res_potion_unknown from "./assets/potions/potion_unknown.png";
import res_potion_stimulation from "./assets/potions/potion_stimulation.png";
import res_potion_luckjelly from "./assets/potions/potion_luckjelly.png";
import res_potion_mana from "./assets/potions/potion_mana.png";
import res_potion_health_adv from "./assets/potions/potion_health_adv.png";
import res_potion_initial from "./assets/potions/potion_initial.jpg";
import res_potion_perception from "./assets/potions/potion_perception.png";
import res_potion_health from "./assets/potions/potion_health.png";
import res_mw_map_lock_off from "./assets/ui/mw_map_lock_off.png";
import res_mw_highlight from "./assets/ui/mw_highlight.png";
import res_mw_map_centrize from "./assets/ui/mw_map_centrize.png";
import res_mw_map_lock_on from "./assets/ui/mw_map_lock_on.png";
import res_mw_logoicon from "./assets/ui/mw_logoicon.png";
import res_mw_fight_bar_exp from "./assets/ui/mw_fight_bar_exp.jpg";
import res_mw_fight_creature_hit from "./assets/ui/mw_fight_creature_hit.png";
import res_mw_fight_bar_bcg from "./assets/ui/mw_fight_bar_bcg.png";
import res_mw_fight_behaviour_bcg2 from "./assets/ui/mw_fight_behaviour_bcg2.png";
import res_mw_fight_behaviour_target from "./assets/ui/mw_fight_behaviour_target.png";
import res_mw_mainmenu_close from "./assets/ui/mw_mainmenu_close.png";
import res_mw_mainmenu_options from "./assets/ui/mw_mainmenu_options.png";
import res_mw_logo from "./assets/ui/mw_logo.png";
import res_mw_icon from "./assets/ui/mw_icon.png";
import res_mw_background from "./assets/ui/mw_background.jpg";
import res_mw_fight_bar_hp from "./assets/ui/mw_fight_bar_hp.png";
import res_mw_fight_bar_mana from "./assets/ui/mw_fight_bar_mana.png";
import res_mw_closed_mail from "./assets/ui/mw_closed_mail.png";
import res_mw_open_mail from "./assets/ui/mw_open_mail.png";
import res_creature_gnome from "./assets/creatures/creature_gnome.jpg";
import res_creature_warewolf from "./assets/creatures/creature_warewolf.jpg";
import res_creature_bunny from "./assets/creatures/creature_bunny.jpg";
import res_creature_bat from "./assets/creatures/creature_bat.jpg";
import res_creature_black_cat from "./assets/creatures/creature_black_cat.jpg";
import res_creature_strumpf from "./assets/creatures/creature_strumpf.jpg";
import res_creature_wraith from "./assets/creatures/creature_wraith.jpg";
import res_creature_golem from "./assets/creatures/creature_golem.jpg";
import res_item_cornerstone from "./assets/items/item_cornerstone.jpg";
import res_item_daisyflower from "./assets/items/item_daisyflower.jpg";
import res_item_cateye from "./assets/items/item_cateye.jpg";
import res_item_rozkovnik from "./assets/items/item_rozkovnik.jpg";
import res_item_swallowweed from "./assets/items/item_swallowweed.jpg";
import res_item_szczeciogon from "./assets/items/item_szczeciogon.jpg";
import res_item_keypart from "./assets/items/item_keypart.jpg";
import res_item_smurfblood from "./assets/items/item_smurfblood.jpg";
import res_item_4clover from "./assets/items/item_4clover.jpg";
import res_item_emptyglass from "./assets/items/item_emptyglass.png";
import res_item_ectoplasma from "./assets/items/item_ectoplasma.jpg";
import res_item_bunnyleg from "./assets/items/item_bunnyleg.jpg";
import res_item_garlic from "./assets/items/item_garlic.jpg";
import res_item_mandragora from "./assets/items/item_mandragora.jpg";
import res_item_fernflower from "./assets/items/item_fernflower.png";
import res_spell_oob from "./assets/spells/spell_oob.png";
import res_spell_astralshield from "./assets/spells/spell_astralshield.png";
import res_spell_fireball from "./assets/spells/spell_fireball.png";
import res_spell_electrokinesis from "./assets/spells/spell_electrokinesis.png";
import res_spell_telekinesis from "./assets/spells/spell_telekinesis.png";
import res_spell_physical from "./assets/spells/spell_physical.png";

export default class R {
  static drawables = {
    ilustration_6: res_ilustration_6,
    // generated with: find assets | grep "\." | awk '{ print "    "$1": res_"$1"," }' | sed 's/assets\/[a-z]*\///g' | sed 's/\.[a-z][a-z][a-z]//g'
    achiv_mixtures: res_achiv_mixtures,
    achiv_juggle: res_achiv_juggle,
    achiv_gamefinished: res_achiv_gamefinished,
    achiv_distvariety: res_achiv_distvariety,
    achiv_medit: res_achiv_medit,
    achiv_disttraveled: res_achiv_disttraveled,
    achiv_hunter: res_achiv_hunter,
    achiv_plants: res_achiv_plants,
    achiv_days: res_achiv_days,
    achiv_unknown: res_achiv_unknown,
    potion_mana_adv: res_potion_mana_adv,
    potion_antidote: res_potion_antidote,
    potion_invisibility: res_potion_invisibility,
    potion_unknown: res_potion_unknown,
    potion_stimulation: res_potion_stimulation,
    potion_luckjelly: res_potion_luckjelly,
    potion_mana: res_potion_mana,
    potion_health_adv: res_potion_health_adv,
    potion_initial: res_potion_initial,
    potion_perception: res_potion_perception,
    potion_health: res_potion_health,
    mw_map_lock_off: res_mw_map_lock_off,
    mw_highlight: res_mw_highlight,
    mw_map_centrize: res_mw_map_centrize,
    mw_map_lock_on: res_mw_map_lock_on,
    mw_logoicon: res_mw_logoicon,
    mw_fight_bar_exp: res_mw_fight_bar_exp,
    mw_fight_creature_hit: res_mw_fight_creature_hit,
    mw_fight_bar_bcg: res_mw_fight_bar_bcg,
    mw_fight_behaviour_bcg2: res_mw_fight_behaviour_bcg2,
    mw_fight_behaviour_target: res_mw_fight_behaviour_target,
    mw_mainmenu_close: res_mw_mainmenu_close,
    mw_mainmenu_options: res_mw_mainmenu_options,
    mw_logo: res_mw_logo,
    mw_icon: res_mw_icon,
    mw_background: res_mw_background,
    mw_fight_bar_hp: res_mw_fight_bar_hp,
    mw_fight_bar_mana: res_mw_fight_bar_mana,
    mw_closed_mail: res_mw_closed_mail,
    mw_open_mail: res_mw_open_mail,
    creature_gnome: res_creature_gnome,
    creature_warewolf: res_creature_warewolf,
    creature_bunny: res_creature_bunny,
    creature_bat: res_creature_bat,
    creature_black_cat: res_creature_black_cat,
    creature_strumpf: res_creature_strumpf,
    creature_wraith: res_creature_wraith,
    creature_golem: res_creature_golem,
    item_cornerstone: res_item_cornerstone,
    item_daisyflower: res_item_daisyflower,
    item_cateye: res_item_cateye,
    item_rozkovnik: res_item_rozkovnik,
    item_swallowweed: res_item_swallowweed,
    item_szczeciogon: res_item_szczeciogon,
    item_keypart: res_item_keypart,
    item_smurfblood: res_item_smurfblood,
    item_4clover: res_item_4clover,
    item_emptyglass: res_item_emptyglass,
    item_ectoplasma: res_item_ectoplasma,
    item_bunnyleg: res_item_bunnyleg,
    item_garlic: res_item_garlic,
    item_mandragora: res_item_mandragora,
    item_fernflower: res_item_fernflower,
    spell_oob: res_spell_oob,
    spell_astralshield: res_spell_astralshield,
    spell_fireball: res_spell_fireball,
    spell_electrokinesis: res_spell_electrokinesis,
    spell_telekinesis: res_spell_telekinesis,
    spell_physical: res_spell_physical,
  }

  static strings = {
    hello: "Hello World, czarodzieju!",
    app_name: "Modern Wizards",
    undef: "!undefined!",
    game_global_ok: "OK",
    game_global_yes: "Tak",
    game_global_no: "Nie",
    game_reallyquit: "Zakończyć grę? Postęp ostatniego zadania zostanie utracony!",
    game_gps_waiting_for: "Trwa określanie lokalizacji... Upewnij się że jesteś na otwartej przestrzeni!", 
    game_gps_not_enabled: "GPS WYŁĄCZONY! Włącz go i zrestartuj grę!", 
    game_gps_loc_set: "LOKALIZACJA WYZNACZONA, GRA SIĘ ROZPOCZYNA!",
    game_gps_loc_wait: "POCZEKAJ NA WYZNACZENIE LOKALIZACJI!",
    game_gps_loc_uncalculated: "LOKALIZACJA JEST WYZNACZANA",
    game_gps_loc_calculated: "LOKALIZACJA WYZNACZONA",    
    game_map_notargets: "Nie ma aktywnych celów",
    help_uri: "http://www.modernwizards.pl/help",
    intro_youtube_id: "DlOuPGrSmic",

    spells_repeled: "ZAKLĘCIE ODBITE!",
    spells_miss: "ZAKLĘCIE NIE TRAFIA!",
    spells_nomana: "NIE MASZ WYSTRACZAJĄCO DUŻO MANA!",
    spells_critical: "TRAFIENIE KRYTYCZNE!",
    
    disclaimer_title: "OSTRZEŻENIE",
    disclaimer_text: (
      <div>
        Modern Wizards jest grą geolokalizacyjną. Wszystkie obiekty oraz miejsca docelowe są wyznaczane w sposób pseudolosowy,
        co oznacza ze te mogą się pojawić na miejscach niedostępnych, niebezpiecznych, o zakazie wstępu lub na terenie prywatnym.
        <br/>
        <br/>
        Pamiętaj, że gra nie zmusza gracza do fizycznego dostania się do wskazanego miejsca – zamiast tego możesz użyć specjalnych funkcji w grze.
        <br/>
        <br/>
        Twórcy gry nie ponoszą odpowiedzialności za szkody powstałe przy rozgrywce.
        <br/>
        <br/>
        Aby rozpocząć grę przeczytaj i zaakceptuj <a taget="_blank" href="http://www.modernwizards.pl/regulamin\">pełen Regulamin</a>
      </div>
    ),   
    disclaimer_accept: "Akceptuję",       
    disclaimer_reject: "Odrzucam",    

    player_resurected: "Twoje nadprzyrodzone moce powróciły!",
    player_mana_charging: "Pobieranie mana ze źródła...",
    player_mana_full: "Osiągnąłeś maksymalny poziom mana",    
    player_levelup: "Awansowałeś na następny poziom!",    
    
    spell_telekinesis_object_too_far: "Jesteś za daleko!",      
        
    creature_bunny: "Królik",
    creature_bunny_desc: "To niegdyś urocze i miłe zwierzątko przeszło metamorfozę w żądną krwi bestię. Potężne kły i pazury, w połączeniu z niezwykłą szybkością, " + 
      "uczyniły z niego niebezpiecznego drapieżnika. Strach pomyśleć, jakiego rodzaju eksperymenty spowodowały przemianę.",
    creature_black_cat: "Czarny kot",
    creature_black_cat_desc: "Czarne koty od setek lat kojarzone są z nieszczęściami, które spotykają przechodniów. I rzeczywiście, zwierzęta te posiadają zdolność " + 
      "żywienia się pozytywną energią swoich ofiar, tzw. \"karmą\". Poza tym koty dysponują wyjątkowo ostrymi pazurami oraz szybkością i zwinnością niespotykanymi u " + 
      "innych gatunków.",
    creature_bat: "Wampierz",
    creature_bat_desc: "Małe latające straszydło, które żywi się energią magiczną innych stworzeń. Jego największym przysmakiem są bezbronne strumpfy, " + 
      "ale wygłodniały nietoperz nie waha się atakować również większych ofiar. Wyjątkowo wątły fizycznie, względnie powolny i łatwy do pokonania.",
    creature_gnome: "Gnom",
    creature_gnome_desc: "Podłe usposobienie oraz zamiłowanie do zagadek to główne cechy tej karłowatej rasy. W podaniach i bajdach znaleźć można wiele" + 
      "wzmianek o aktach nieprawości i bezwstydu dokonywanych przez skrzaty – sikanie do piwa czy podkradanie papieru toaletowego to ponoć najlżejsze z ich przewinień. " +
      "W walce skrzaty posługują się bronią drzewcową oraz prostymi, acz groźnymi zdolnościami pirotechnicznymi. Poruszają się raczej powoli.",
    creature_warewolf: "Wilkołak",
    creature_warewolf_desc: "Wilkołak jest przemienionym pod wpływem sił nadprzyrodzonych człowiekiem. Lykantropia (wilkołactwo) nadaje mu wyjątkową siłę i zwinność, lecz także powoduje deformacje ciała i nadmierne owłosienie. Osobnik jest zazwyczaj okresowo pozbawiany wolnej woli na rzecz zwierzęcych instynktów. Spotykając Wilkołaka należy pamiętać, że lykantropia jest zakaźliwa przez ślinę osobnika.\n\n Leczenie lykanotropii to proces żmudny i bardzo pracochłonny, który poziomem skomplikowania ustępuje jedynie pokrewnym mu egzorcyzmom. Podejmujący się kuracji znachor powinien nie tylko posiadać rozległą wiedzę z zakresu alchemii, zielarstwa i magii, lecz również cechować się olbrzymią siłą woli i odwagą.",
    creature_wraith: "Upiór",
    creature_wraith_desc: "Niematerialna istota uwięziona między światem żywych a światem umarłych. Sprzeczny  naturze stan nieżycia rodzi w niej gniew oraz nienawiść, które obraca przeciwko ludziom, nawiedzając ich i strasząc. Z czasem jednak zwykłe nękanie przestaje zjawie wystarczać i staje się ona naprawdę groźna.",    
    creature_strumpf: "Chochlik",
    creature_strumpf_desc: "O tych bojaźliwych, niebieskich chochlikach wiadomo bardzo niewiele. Według legend strumpfy żyją w ukrytych wioskach, gdzie czynią plugawą magię i warzą trujące napary. Wielu magów poluje na te stwory, by wykradać im strumpfastyczną esencję będącą cennym składnikiem różnorakich mikstur. Małe i szybkie strumpfy potrafią ukryć się niemal wszędzie. Posiadaja też ogromne szczęście, które może obrócić się przeciwko agresorowi.",   
    creature_golem: "Golem",
    creature_golem_desc: "Alchemicy przez całe stulecia zgłębiali tajemnice energii napędzającej odwieczną spiralę życia i śmierci. Jednym z kamieni milowych tych badań było rozpalenie boskiej iskry w nieożywionej materii. Tak powstał golem – pozbawiony wolnej woli sługa i strażnik.",  
    
    item_daisyflower: "Chamomile",
    item_daisyflower_desc: "Chamomile, czyli stokrotki, a także rumianek znane są z właściwości określania uczuć osób drugich. Mogą więc wzmocnić percepcję przyszłych wydarzeń.",
    item_fernflower: "Kwiat paproci",
    item_fernflower_desc: "Raz do roku kwiat paproci promieniuje tak silną aurą, że może go dostrzec nawet niewprawne oko. Znalezienie kwiatu poza tym okresem wymaga natomiast wielkiej magicznej intuicji. \n Znany jest ogromny wpływ kwiatu na zdrowie, powiadają zaś, że potrafi nawet przywróci do życia zmarłych.",
    item_4clover: "Koniczyna",
    item_4clover_desc: "Czwarty listek koniczyny świadczy o jej magicznych właściwościach. Koniczyna w szczególności przynosi szczęście.",
    item_rozkovnik: "Roskownik",
    item_rozkovnik_desc: "Według tradycji roskownik ma magiczną moc zdolną odblokować lub odkryć coś, co jest zablokowane lub zamknięte.",
    item_garlic: "Allium",
    item_garlic_desc: "Allium, popularnie zwany czosnkiem wampirzym. Wychodował go ponoć sam Nosferatu, by śmiać się z naiwnych głupców, którzy staraliby się odpędzać wampiry za jego pomocą. Zastosowanie znalazły jednak nasze babcie, odkrywając jego właściwości lecznicze.",
    item_swallowweed: "Jaskółcze ziele",
    item_swallowweed_desc: "Jaskółcze ziele, które po zerwaniu wydziela z łodygi żółty sok, spełnia niezwykłe role w leczeniu i magii ludowej. Każdy, kto nosi tę roślinę przy sobie, zyskuje zdolność łagodzenia sporów. ",
    item_cornerstone: "Kamień węgielny",
    item_cornerstone_desc: "Kamienie węgielne są używane od stuleci ze względu na swoją specjalną magiczną właściwość - sprawiają, że obiekty, w których skład wchodzą stają się trwalsze. ",
    item_szczeciogon: "Szczeciogon",
    item_szczeciogon_desc: "Chaiturus (łac.) lub tradycyjnie Szczeciogon. Roślina lecznicza, której kłos przypomina lwią kitę. Jako składnik wywaru, ma silne działanie przeciwtoksyczne. ",    
    item_mandragora: "Mandragora",
    item_mandragora_desc: "Znana roślina magiczna, której korzeń często przybiera kształt ludzkiej sylwetki. Mandragorę podaje się przede wszystkim kobietom w ciąży, gdyż jakoby zapewnia prawidłowy wzrost potomka. ",    
    item_cateye: "Kocimiętka",
    item_cateye_desc: "Kocimiętka posiada niezwykłą moc nęcenia i osłabiania woli. Działa wyjątkowo narkotycznie na koty, prawdopodobnie powodując u nich przewidzenia. Po destylacji może mieć wpływ także na inne stworzenia. ",    
    item_ectoplasma: "Ektoplazma",
    item_ectoplasma_desc: "Przeźroczysty śluz pozostawiany przez duchy i zjawy w miejscach wzmożonej aktywności paranormalnej. ",    
    item_smurfblood: "Energosencja",
    item_smurfblood_desc: "Filigranowy paciorek o nieokreślonym kształcie emanujący ze swojego wnętrza niebieskim światłem. ",    
    item_bunnyleg: "Królicza łapka",
    item_bunnyleg_desc: "Królicza łapka noszona jest zwykle w formie amuletu na szyi, zaś gdy ją ususzyć może być użyta jako składnik mikstur. ",    
    
    item_keypart: "Klucz runniczny",
    item_keypart_desc: "Runniczny klucz otrzymany od gnoma.\nPodobno otwiera zamkniętą bramę.",

    potion_create: "Uwarzyć ten eliksir?",
    
    potion_health: "Eliksir leczący",
    potion_health_desc: "Ziołowy napar, który przynosi natychmiastową ulgę zranionym przez siły nadprzyrodzone",
    potion_health_recept: "Zmieszaj i zagotuj dowolne rośliny o właściwościach leczniczych, to jest: Rumianek, Allium lub Kwiat paproci",
    potion_mana: "Eliksir mana",
    potion_mana_desc: "Wysokoenergetyczny napój, który szybko przywraca mana.",
    potion_mana_recept: "Zmieszaj sproszkowaną energosencję, koniczynę i jaskółcze ziele. Wstrząśnij, ale nie mieszaj.",
    potion_antidote: "Antidotum",
    potion_antidote_desc: "Leczy zakażenie wywołane przez nadprzyrodzone istotny, zwłaszcza wilkołaki i wampierze. ",
    potion_antidote_recept: "Zmieszaj świeże Allium, z kwiatem paproci i jaskółczym zielem",    
    potion_initial: "Specyfik",
    potion_initial_desc: "Dziwny, zielony i ponury płyn w szkle laboratoryjnym. W nozdrza bucha silny, ziołowy aromat, ale nie wydaje się niebezpieczny. ",
    potion_unknown: "Nieznana mikstura",
    potion_unknown_desc: "Nieznana mikstura",
    potion_invisibility: "Eliksir niewidzialności",
    potion_invisibility_desc: "Wypicie go sprawi, że twoje ciało zacznie emitować aurę, która uczni ciebie i twoje ubrania niewidzialnymi dla wszystkich istot. Oprócz braku widoczności, maskuje również twój zapach. Użyj go do przedarcia się niebezpieczne obszary, nie niepokojony przez ataki .",
    potion_invisibility_recept: "Zagotuj kwiat paproci z roskownikiem przez dwie minuty, dopóki ciecz nie stanie się przejrzysta. Gdy wystygnie, dodaj ektoplazmę.",    
    potion_perception: "Stymulant percepcji",
    potion_perception_desc: "Wyostrza twoje zmysły ułatwiając dostrzeżenie istot, nawet tych ukrytych, zarówno z mniejszej jak i z większej odległości. ",    
    potion_perception_recept: "Zmieszaj fragmenty roskownika, jaskółczego ziela i kocimięty w wodzie czerpanej w pobliżu źródła mana.",     
    potion_stimulation: "Stymulant siły",
    potion_stimulation_desc: "Napełnia cię niezwykłą siłą umożliwiając wykonywanie potężnych ataków i przyjmowanie mocniejszych ciosów. ",        
    potion_stimulation_recept: "Uważnie dodaj fragmenty szczeciogonu do dekoktu z mandragory. Kiedy kolor zmieni się na zielony, dodaj kamień węgielny, który powinien się rozpuścić. Gdy tak się stanie, wywar jest gotowy.",     
    potion_luckjelly: "Uszczęśliwiacz",
    potion_luckjelly_desc: "Połączone właściwości poszczególnych składników eliksiru wpływają na \"karmę\" pijącego. Przyczyny tego zjawiska pozostają nieznane.",        
    potion_luckjelly_recept: "By stworzyć ten eliksir potrzebujesz wysuszonej łapki nadprzyrodzonej istoty i cztery listki tej samej koniczyny. Dodaj je do dekoktu z rumianku i poczekaj aż stężeje.",        
    
    achiv_hunter: "Łowca",
    achiv_hunter_desc: "Przyznawane po pokananiu każdego gatunku stworów",
    achiv_plants: "Zbieracz",
    achiv_plants_desc: "Przyznawane po zebraniu każdego typu roślin i przedmiotów",
    achiv_gamefinished: "Oddany",
    achiv_gamefinished_desc: "Przyznawane po ukończeniu głównego wątku fabularnego",
    achiv_disttraveled: "Wędrowiec",
    achiv_disttraveled_desc: "Przyznawane po przebyciu 20 kilometrów",
    achiv_distvariety: "Podróżnik",
    achiv_distvariety_desc: "Przyznawane po pojawieniu się w miejscu odległym o 500 kilometrów",
    achiv_mixtures: "Alchemik",
    achiv_mixtures_desc: "Przyznawane po stworzeniu wszystkich typów mikstur",
    achiv_days: "Rzetelny",
    achiv_days_desc: "Przyznawane za uruchomienie gry 7 dni pod rząd",
    achiv_medit: "Mistrz Zen",
    achiv_medit_desc: "Przyznawane za wytrzymanie 15 minut w Źródle Mana",
    achiv_juggle: "Żongler",
    achiv_juggle_desc: "Przyznawane za ściągnięcie naraz 3 przedmiotów telekinezą",
    achivment_unlocked: "Odznaka odblokowana!",
    
    spell_oob: "Bilokacja",
    spell_oob_desc: "Pozwala na przemieszczanie się po ezoterycznej rzeczywistości",
    spell_telekinesis: "Telekineza",
    spell_telekinesis_desc: "Pozwala przyciągnąć porządany obiekt z odległości.",
    spell_telekinesis_pulling: "Przyciąganie obiektu...",
    spell_telekinesis_using: "Używanie telekinezy - kliknij na obiekt",
    spell_physical: "Uderzenie mocy",
    spell_physical_desc: "Walnij przeciwnika!",
    spell_physical_bite: "Ugryzienie",
    spell_physical_scratch: "Szarpanie",
    spell_physical_spear: "Dźgnięcie włócznią",
    spell_fireball: "Pirokineza",
    spell_fireball_desc: "Poślij płonącą kulę czystej energii magicznej przeciwnikowi prosto w twarz!",
    spell_battlespeed: "Przyspieszenie",
    spell_battlespeed_desc: "nevermind, creatures",
    spell_infection: "Zakażenie",
    spell_infection_desc: "nevermind, creatures",
    spell_astralshield: "Lygokineza",
    spell_astralshield_desc: "Przez kilka sekund ochrania cię przed większością ataków.",
    spell_electrokinesis: "Elektrokineza",
    spell_electrokinesis_desc: "Małe wyładowanie elektryczności przecina obszar powodując obrażenia.",
    spell_badluck: "Pech",
    spell_badluck_desc: "nevermind, creatures",
    
    // String for single item in messages list view
    back: "Powrót",
    use: "Użyj",
    alchemy: "Alchemia",
    accept: "Akceptuj",
    reject: "Odrzuć",
    later: "Może później",
    from: "Od: ",
    target: "Cel",
    usageBlocked: "Użycie zablokowane",

    // Strings for quests
    completed: "Ukończony",
    inprogress: "W trakcie",
    failed: "Niepowodzenie",
    menu_settings: "Ustawienia",
    text_view_id: "ID:",
    text_view_count: ". Count: ",
    yes_button: "Tak",
    no_button: "Nie",
    creature_cast_fc: "Stwór używa ",
    health_fc: "Zdrowie: ",
    mana_fc: "Mana: ",
    escape_fc: "Uciekać?",
    escape_text_fc: "Jesteś pewien, że chcesz uciec z walki?",
    run_button: "Uciekaj!",
    stay_button: "Zostań",
    accept_quest_button: "Przyjmij zadanie.",
    refuse_quest_button: "Odrzuć zadanie",
    no_spell_sc: "Brak zaklęć w tej kategorii",
    cast_spell_sc: "Rzucić to zaklęcie?",
    unknown: "Nieznane",
    cast_sc: "Rzuć",
    cant_cast_sc: "Nie można rzucić tego zaklęcia",
    player: "Gracz",
    click_where_wish_go: "Kliknij na miejsce docelowe.",
    no_mana: "Brak mana!",
    no_mana_text: "Brakuje ci mana! Przywróć mana w źródle!",
    my_curr_location: "Moje aktualne położenie: ",
    no_last_location: "Ostatnie położenie nieznane",
    oob_out_of_mana: "Wyczerpano mana!",
    oob_finished: "Powróciłeś do swojego ciała",
    alchemy_not_available: "Alchemia nie jest jeszcze dostępna.",
    resuraction: "Wskrzeszenie",
    first_player_death: (
      <div>
        <img src={res_ilustration_6} width="100%"  />
        Ostatni cios zwala cię z nóg i niemal tracisz przytomność. Wraz z siłami i wolą walki opuszczają cię wszystkie nabyte zdolności.
        Świat na powrót wygląda zwyczajnie, jak sprzed zażycia mikstury od BF. Jedyne, co teraz odczuwasz, to paląca potrzeba powrotu do Źródła Mana.
      </div>
    ),
   	
    want_to_take: "Czy chcesz to zebrać?",
    plant: "Roślina",
    mana_zone: "Źródło mana",
    trigger: "Trigger",
    
    
    // Screen Specyfic strings
    opt_title: "Opcje",
    opt_use_gps: "Użyj GPS",
    opt_use_mock: "Użyj mock location!",
    opt_language: "Język:",
    opt_codes: "Kod:",
    opt_gener_obj_box: "Odblokuj wszystko",
    opt_use_vibrations: "Wibracje",
    opt_use_sounds: "Dźwięki",
    opt_savenload: "Save And Load",
    opt_devmode: "Tryb deweloperski",
    opt_deletesaves: "Usuń zapisane stany",
    opt_loadsave: "Wczytaj stan gry z pliku",
    opt_save: "Zapisz",
    opt_save_but: "Zapisz",
    
    mainmenu_map: "Mapa",
    mainmenu_quests: "Zadania",
    mainmenu_spells: "Zaklęcia",
    mainmenu_inventory: "Ekwipunek",
    mainmenu_stat: "Statystyki",
    mainmenu_quit: "Wyjście",
    mainmenu_intro: "Zobacz intro",

    scr_bestiary_title: "Bestiariusz", 
    scr_alch_title: "Alchemia",
    scr_alch_ing: "Składniki:",
    scr_alch_mixpot: "Kociołek:",
    scr_alch_results: "Efekt końcowy:",
    scr_alch_clear: "Opróźnij",
    scr_fight_run: "Uciekaj",
    scr_fight_potions: "Eliksiry",
    scr_stats_title: "Statystyki",
    scr_stats_achivements: "Osiągnięcia",
    scr_stats_bestiary: "Bestiariusz",
    scr_stats_collectibles: "Przedmioty",
    scr_stats_recipes: "Receptury",
    scr_stats_spells_header: "Jesteś pod wpływem:\n",
    scr_stats_spells_none: "(brak)\n",
    scr_stats_resurect: "Utraciłeś wszystkie swoje moce!\n Udaj się do najbliższego źródła mana żeby je przywrócić ",
    scr_quests_title: "Zadania",
    scr_collectibles_title: "Przedmioty",
    scr_map_title: "Widok świata",
    scr_map_filters: "Filtry",
    scr_map_menu: "Menu",
    scr_map_terrain: "Teren",	
    scr_map_controls: "Sterowanie",
    scr_inv_title: "Ekwipunek",
    scr_inv_btn_items: "Przedmioty",
    scr_inv_btn_potions: "Eliksiry",		  
    scr_inv_btn_alchemy: "Alchemia",	
    scr_spells_title: "Zaklęcia",
    scr_spells_global: "Globalne",    		
    scr_spells_battle: "Bitewne", 
    scr_spells_empty_list: "Brak zaklęć w tej kategorii",
    
    alert_message_title: "Otrzymałeś nową wiadomość",		  
    alert_message_show: "Pokaż",	 
    alert_message_later: "Później",	 
    alert_item_title: "Znalazłeś:",		  
    alert_item_question: "Co robisz?",	 
    alert_item_take: "Zbierz",	 
    alert_item_leave: "Zostaw",	 
    alert_creature_title: "Zauważyłeś:",		  
    alert_creature_question: "Co robisz?",	 
    alert_creature_fight: "Walcz",	 
    alert_creature_run: "Uciekaj!",	 
    
    fight_: "Wyjdź",
    fight_lostalert_title: "Zginąłeś!",
    fight_lostalert_desc: "Twoje astralne zdrowie wyczerpało się! Udaj się do najbliższego źródła mana żeby je odzyskać!",
    fight_lostalert_ok: "Zrobię to.",
    fight_winalert_title: "Zwyciężyłeś!",
    fight_winalert_desc: "Wygrałeś walkę!",
    fight_winalert_ok: "Świetnie!",
    fight_instructions: "Jesteś w walce. Kliknij symbol akcji kiedy wskaźnik stwora jest na środku!", 

    list_title_receptures: "Receptury",
   
    move_the_camera: "Move the camera", 
  
    mapviewtype_feelings: "Widok odczuć",
    mapviewtype_satelite: "Widok realny",
    mapviewtype_normal: "Widok symboliczny",
    mapviewtype_terrain: "Widok terenu",
    mapviewtype_none: "Tylko obiekty",

    msg_saved: "Gra zapisana",
    loading: "Wczytywanie",
  }
}
