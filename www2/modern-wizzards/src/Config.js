const STAGING = {
  apiKey: "AIzaSyCS-JrZ57QgXwBh1rw_0mkOaqP4ujqmsGY",
  authDomain: "modern-wizzards-web-dev.firebaseapp.com",
  databaseURL: "https://modern-wizzards-web-dev.firebaseio.com",
  projectId: "modern-wizzards-web-dev",
  storageBucket: "modern-wizzards-web-dev.appspot.com",
  messagingSenderId: "207677770229"
};

const PROD = {
  apiKey: "AIzaSyB6MP3K0tHxXJ0g5-kwaKsQEX0Qem5cEfs",
  authDomain: "modern-wizzards-web.firebaseapp.com",
  databaseURL: "https://modern-wizzards-web.firebaseio.com",
  projectId: "modern-wizzards-web",
  storageBucket: "modern-wizzards-web.appspot.com",
  messagingSenderId: "425871201569"
};

export default {STAGING, PROD};
