
const componentToHex = function(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

const rgbToHex = function(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

export default class Color {
  static TRANSPARENT = 0x00ffffff;
  static SIGHT_COLOR = 0x400000ff;

  static toHexRGB(c) {
    return '#' + componentToHex(0x1000000 + (c % 0x1000000)).substring(1);
  }

  static opacity(c) {
    return ~~(c/0xffffff)/255;
  }

  static toRGBA(c) {
    const a = Color.opacity(c);
    const r = Math.floor(c % 0x1000000 / 0x10000);
    const g = Math.floor(c % 0x10000 / 0x100);
    const b = Math.floor(c % 0x100 / 0x1);
    return `rgba(${r}, ${g}, ${b}, ${a})`;
  }
}
