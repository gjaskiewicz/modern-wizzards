import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './MWToast.css';

class MWToastMessage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() { }

  render() {
    return (
      <div className="toastMessage toastMessageInfo fadeInOut">{this.props.text}</div>
    )
  }
}

class MWToastExtendedMessage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() { }

  render() {
    const img = this.props.img;
    var fadeClass = this.props.fading ? "fadeOut" : "";
    var classNames = `toastMessage ${fadeClass}`;
    const imageEl = img ? (<img className="mwtoastImg" src={img} />) : (<span />);
    const toastClass = this.props.type == 'ERROR' ? "toastMessageError" : "toastMessageInfo";
    return (
      <div className={`toastMessage fadeInOut ${toastClass}`}>
        {imageEl}
        {this.props.text}
      </div>
    )
  }
}

var nextToastId = 0;

export default class MWToast extends Component {
  constructor(props) {
    super(props);
    this.state = {messages: []};
  }

  static toast(text, spec=undefined) {    
    let currentToastId = nextToastId;
    nextToastId++;
    if (window.mwtoast) {
      window.mwtoast.setState(s => ({
        ...s, 
        messages: [
          ...s.messages, 
          {id: currentToastId, fading: true, text, spec}
        ] 
      }));
      setTimeout(() => MWToast.untoast(currentToastId), 5000);
    } else {
      console.log(`Dropped toast: ${text}`);
    }
  }

  static untoast(id) {
    window.mwtoast.setState(s => ({...s, messages: s.messages.filter(msg => msg.id != id) }));
  }

  static init(elementId) {
    var container = <MWToast />
    ReactDOM.render(
      container, 
      document.getElementById(elementId)
    );
  }

  componentDidMount() {
    if (window.mwtoast) {
      console.log("Error - mwtoast already mounted");
    }
    window.mwtoast = this;
  }

  render() {
    var messagesSnippets = this.state.messages.map((msg) => {
      if (msg.spec) {
        return (
        <MWToastExtendedMessage 
            key={msg.id} 
            text={msg.text} 
            fading={msg.fading}
            img={msg.spec.img}
            type={msg.spec.type}
         />);
      } else {
        return (<MWToastMessage key={msg.id} text={msg.text} fading={msg.fading} />);
      }
    });
    return (
      <div>
        {messagesSnippets}
      </div>
    );
  }
}
