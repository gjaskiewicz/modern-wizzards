import React from 'react';
import CommonBaseScreen from "./CommonBaseScreen.js";
import Button from "./Button.js";
import QuestsEngine from "../quests/QuestsEngine.js";
import AllQuests from "../quests/AllQuests.js";
import Quest from "../quests/Quest.js";
import R from "../R.js";
import './QuestsMessageListScreen.css';

const TAG = "QuestsMessageListActivity";

export default class QuestsMessageListScreen extends CommonBaseScreen {
  constructor(props) {
    super(props);
  }

  back() {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, activeScreen: 'map' } }
    ));
  }

  showQuestMessage(quest) {
    window.gameController.applyToState(s => {
      s = quest.openMessage(s);
      return window.gameController.showNextScreenState(s, 'questMessage', { questMessage:quest });
    });  
  }

  renderTitle() {
    return (<span>{R.strings.scr_quests_title}</span>);
  }

  renderContent() {
    const quests = this.props.state.quests || { };
    let questNames = Object.keys(quests);
    questNames = questNames.filter(q => {
      const questInst = AllQuests[q];
      const progress = questInst.getProgressState(this.props.state);
      return progress != Quest.ProgressState.NONE &&
          progress != Quest.ProgressState.WAITING;
    });
    questNames.sort((q1, q2) => 
      (quests[q2].waitUntil - quests[q1].waitUntil));
    const list = questNames.map(q => {
      const questInst = AllQuests[q];
      let mailImg = R.drawables.mw_closed_mail;
      if (questInst.isAlreadyRead(this.props.state)) {
        mailImg = R.drawables.mw_open_mail;
      }
      return (
        <li className="highlightable"
            key={q}
            onClick={() => this.showQuestMessage(questInst)}
        >
          <img src={mailImg} />
          <div className="text">
            {questInst.getTitle()}
            <br />
            {R.strings.from} {questInst.getFrom()}
          </div>
        </li>
      );
    });
    return (
    <div>
      <ul className="questListContainer">
        {list}
      </ul>
    </div>);
  }

  renderFooter() {
    return (
      <div>
        <Button onClick={() => this.back()}>{R.strings.back}</Button>
        &nbsp;
        <div className="mwbuttonPlaceholder" />
      </div>
    );
  }
}

/*

public class QuestsMessageListActivity extends MWBaseActivity {
  private static final String TAG = "QuestsMessageListActivity";
  private View mainView;
  private Context ctx = this;
  private ListView questsView;
  private ArrayList<Quest> quests = new ArrayList<Quest>();
  
  private LayoutInflater inflater;
  
  private BroadcastReceiver eventsReceiver; //Will receive notification updates of menu buttons
  private IntentFilter eventFilter;
  
    @Override
    public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       
         requestWindowFeature(Window.FEATURE_NO_TITLE);
         getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                 WindowManager.LayoutParams.FLAG_FULLSCREEN);
         
         inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         //mainView = inflater.inflate(, null, false);
         
         setContentView(R.layout.quests);
         
         questsView = (ListView)findViewById(R.id.quests_message_list_view);
         
         copyQuestsFromOrginalCollection();
         
         registerEventsReceiver();
         
         questsView.setAdapter(new ArrayAdapter<Quest>(this, R.layout.quests_message_item, quests ) {
          
          @Override
          public View getView(int position, View convertView, ViewGroup parent) {
            
            Quest q = (Quest)getItem(position);
            
            View row;
             
            if (null == convertView) {
              row = inflater.inflate(R.layout.quests_message_item, null);
            } else {
              row = convertView;
            }
            
            TextView messageTextView = (TextView) row.findViewById(R.id.message_title);
            ImageView messagePhoto =  (ImageView) row.findViewById(R.id.message_photo);
            
            TextAppearanceSpan  spanTitle = new TextAppearanceSpan(ctx, R.style.questMessageTitle);
            TextAppearanceSpan  spanExtraInfo = new TextAppearanceSpan(ctx, R.style.questMessageExtraInfo);
            SpannableStringBuilder builder = new SpannableStringBuilder();
            String fullMessage = q.getTitle()+"\n"+getString(R.string.from)+": " + q.getFrom();
            builder.append(fullMessage);
            builder.setSpan(spanTitle,0,fullMessage.indexOf("\n"), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(spanExtraInfo,fullMessage.indexOf("\n"), fullMessage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            messageTextView.setText(builder);
            if(q.isAlreadyRead()) {
              //messagePhoto.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.open_mail,null));
              messagePhoto.setImageResource(R.drawable.open_mail);
            }

            
            return row;
          }
        });
    
         //questsView.setAdapter(mAdapter); 
         //questsView.setBackgroundResource(R.drawable.mw_quests_messages_bg);
         questsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
          long arg3) {
        final Quest quest = QuestsEngine.getInstance().getQuestByID(arg2);
        
        displayMessageThread(QuestsMessageListActivity.this, quest);
            
        if(!quest.isAlreadyRead()) {
          //ponizsze przeniesione do MessageView
          quest.openMessage();
          ((ArrayAdapter<Quest>)questsView.getAdapter()).notifyDataSetChanged();
        }
        
      }

         });          
    }

  private void registerEventsReceiver() {
    eventFilter = new IntentFilter(Game.Actions.QUESTS_COLLECTION_CHANGED);
         eventsReceiver = new BroadcastReceiver() {
       @Override
       public void onReceive(Context context, Intent intent) {
         if(intent.getAction().equals(Game.Actions.QUESTS_COLLECTION_CHANGED)) {
           handleContentChange();
         }
       }
         };
         registerReceiver(eventsReceiver, eventFilter);
  }
    
    @Override
    public void onDestroy() {
      unregisterReceiver(eventsReceiver);
      $log.flush();
        super.onDestroy();
    }
    
  public static void displayMessageThread(Activity ctx, Quest quest) {
    if(quest == null) {
      $log.e(TAG, "displayMessageThread quest is null");
      return;
    }
    Intent i = new Intent(ctx, MessageViewActivity.class);
    i.putExtra("title", quest.getTitle());
    i.putExtra("message", quest.getFullMessageText());
    i.putExtra("from", quest.getFrom());
    i.putExtra("fromimg", quest.getFromImg());
    i.putExtra(MessageViewActivity.MESSAGE_CANCLOSE, true);
    i.putExtra("readOnly", quest.getAcceptanceState() == AcceptanceState.ACCEPTED ||
          quest.getAcceptanceState() == AcceptanceState.REJECTED ||
          quest.getProgressState() == ProgressState.COMPLITED || 
          quest.getProgressState() == ProgressState.INPROGRESS );
    i.putExtra("questClassName", quest.getClass().getName());
    ctx.startActivity(i);
  }
  
  public void handleContentChange() {
    
    questsView.setVisibility(View.GONE);
    copyQuestsFromOrginalCollection();
    ((ArrayAdapter<Quest>)questsView.getAdapter()).notifyDataSetChanged();
    questsView.setVisibility(View.VISIBLE);
    $log.d(TAG, "handleContentChange quests.size() = " + quests.size() );
  }

  private void copyQuestsFromOrginalCollection() {
    quests.clear();
    quests.addAll(QuestsEngine.getInstance().getQuestsColl());
  }
    
  @Override
  public void onResume() {
    Game.turnOnOffNotificationForButton(R.id.btnQuests, false);
    super.onResume();
  }

}
*/
