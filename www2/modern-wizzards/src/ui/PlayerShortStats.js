import React, { Component } from 'react';
import HorizontalBar from './HorizontalBar.js';
import './ShortStats.css';

export default class PlayerShortStats extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const floatLeft = {
      float: 'left',
      marginRight: '10px'
    }
    let avatar = <span />;
    if (this.props.isExpanded) {
      avatar = (<img id="user" style={floatLeft} className="creatureImage" src={this.props.loginData.photoURL} />);
    }
    const playerEgo = this.props.playerEgo;
    const healthRatio = playerEgo.health / playerEgo.maxHealth;
    const manaRatio = playerEgo.mana / playerEgo.maxMana;
    return (
      <div className="userContainer">
        <div style={floatLeft}>
          <div className="playerStatsName">
            {this.props.loginData.displayName}
          </div>
          <div className="playerStatsLabel">HP</div>
          <div className="playerStatsBar">
            <HorizontalBar className='healthBar' fillRatio={healthRatio} height={8} />
          </div>
          <div className="playerStatsLabel">Mana</div>
          <div className="playerStatsBar">
            <HorizontalBar className='manaBar' fillRatio={manaRatio} height={8} />
          </div>
        </div>
        {avatar}
      </div>
    )
  }
}
