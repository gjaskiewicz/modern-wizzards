import InventoryScreen from './InventoryScreen.js';
import TestData from '../test/TestData.js';
import GameController from '../main.js';
import Game from '../Game.js';

import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';

describe('InventoryScreen', () => {
  let state;
  let inv;

  const getElement = () => {
    return ReactTestUtils.renderIntoDocument(
      <InventoryScreen 
          inventory={state.playerEgo.inventory}
          gameState={state.gameState}
          filterBy={state.ui.inventoryListFilter}
      />);
  };

  beforeEach(() => {
    window.gameController = new GameController();
    window.gameController.dispatch = (func) => {state = func(state)};
    state = {
      gameState: Game.State.STATE_WALKING,
      playerEgo: TestData.getFullEgo(),
      ui: {
        inventoryListFilter: undefined,
      }
    };
    inv = getElement();
  });

  it('should show plants', () => {
    const buttons = ReactTestUtils.scryRenderedDOMComponentsWithClass(inv, 'mwbutton');
    const filterButton = buttons.filter(b => b.textContent == 'ITEMS')[0];
    ReactTestUtils.Simulate.click(filterButton);
    expect(state.ui.inventoryListFilter).toBe('items');

    const items = ReactTestUtils.scryRenderedDOMComponentsWithClass(inv, 'inventoryItem');
    expect(items.length).toBe(2);
    expect(items[0].textContent).toBe('2 Chamomile');
    expect(items[1].textContent).toBe('1 Fern flower');
  });

  it('should show potions', () => {
    const buttons = ReactTestUtils.scryRenderedDOMComponentsWithClass(inv, 'mwbutton');
    const filterButton = buttons.filter(b => b.textContent == 'POTIONS')[0];
    ReactTestUtils.Simulate.click(filterButton);
    expect(state.ui.inventoryListFilter).toBe('potions');

    inv = getElement();
    const items = ReactTestUtils.scryRenderedDOMComponentsWithClass(inv, 'inventoryItem');
    expect(items.length).toBe(2);
    expect(items[0].textContent).toBe("Healing potion");
    expect(items[1].textContent).toBe("Mana potion");
  });

  it('should show object details', () => {
    let buttons = ReactTestUtils.scryRenderedDOMComponentsWithClass(inv, 'mwbutton');
    const filterButton = buttons.filter(b => b.textContent == 'ITEMS')[0];
    ReactTestUtils.Simulate.click(filterButton);
    expect(state.ui.inventoryListFilter).toBe('items');

    buttons = ReactTestUtils.scryRenderedDOMComponentsWithClass(inv, 'inventoryImage');
    expect(buttons.length).toBe(2);
    ReactTestUtils.Simulate.click(buttons[0]);
    expect(state.ui.activeScreen).toBe('inventory_details');
    expect(!!state.ui.inventoryDetailsItem).toBe(true);
    expect(state.ui.inventoryDetailsItem.id).toBe('item_daisyflower');
  });
});
