import React, { Component } from 'react';
import './CommonBaseScreen.css';

const devText = (t) => (window.isDev ? t : <span />);

export default class CommonBaseScreen extends Component {
  constructor(props) {
    super(props);
  }

  renderTitle() {
    return devText(<span>!!Set title!!</span>);
  }

  renderHeader() {
    return devText(<span>!!Set header!!</span>);
  }

  renderContent() {
    return devText(<span>!!Set content!!</span>);
  }

  renderFooter() {
    return devText(<span>!!Set footer!!</span>);
  }

  resetScroll() {
    if (this.screenContentContainer) {
      this.screenContentContainer.scrollTop = 0;
    }
  }

  render() {
    return (
      <div className="screenFlexContainer">
        <div className="screenTitleContainer">
          {this.renderTitle()}
        </div>
        <div className="screenHeaderContainer">
          {this.renderHeader()}
        </div>
        <div className="screenContentContainer"
             ref={(el) => { this.screenContentContainer = el; }}>
          {this.renderContent()}
        </div>
        <div className='screenFooterContainer'>
          {this.renderFooter()}
        </div>
      </div>
    );
  }
}
