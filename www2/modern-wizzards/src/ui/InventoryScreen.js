import React, { Component } from 'react';
import R from "../R.js";
import Game from "../Game.js";
import Button from "./Button.js";
import CommonBaseScreen from './CommonBaseScreen.js';
import ObjectInfoFactory from '../gamelogic/metadata/ObjectInfoFactory.js';
import './InventoryScreen.css';

const FilterMode = {
  items: 'item_',
  potions: 'potion_'
};

class EmptyInventory extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <p>Your inventory is empty</p>
    )
  }
}

class FilledInventory extends Component {
  constructor(props) {
    super(props);
  }

  detailsView(item) {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, activeScreen: 'inventory_details', inventoryDetailsItem: item} }
    ));
  }

  render() {
    var count = this.props.inventory.length;
    var itemInfos = this.props.inventory.map(item => ({
      info: ObjectInfoFactory.get().getInfoStandarized(item.name),
      count: item.count,
      blocked: item.blocked, // TODO: użyć tego
    }));
    var items = itemInfos.map((item, i) => {
      var counter = undefined;
      if (item.count) {
        counter = (<span>{item.count}</span>);
      }
      const info = item.info;
      return (
        <li className="inventoryItem highlightable" 
            key={i} 
            onClick={() => this.detailsView(info)}>
          <img className="inventoryImage" src={info.getImageID()}/>
          <div className="inventoryCounter">
            {counter}
          </div>
          <div className="inventoryText">
            {info.getName()}
          </div>
        </li>
      );
    });

    return (
      <div>
        <p>Your inventory has {count} items</p>
        <ul>
          {items}
        </ul>
      </div>
    )
  }
}

class IdleInventoryScreen extends CommonBaseScreen {
  constructor(props) {
    super(props);
  }

  back() {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, activeScreen: 'map' } }
    ));
  }

  goToAlchemy() {
    window.gameController.applyToState(s => (
      window.gameController.showNextScreenState(s, 'alchemy')
    ));
  }

  toggleFilterMode(mode) {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, inventoryListFilter: mode} }
    ));
  }

  renderTitle() {
    return (R.strings.scr_inv_title);
  }

  renderHeader() {
    return (
      <div className="headerButtonContainer">
        <Button onClick={() => this.toggleFilterMode('items')}>{R.strings.scr_inv_btn_items}</Button>
        &nbsp;
        <Button onClick={() => this.toggleFilterMode('potions')}>{R.strings.scr_inv_btn_potions}</Button>
      </div>
    );
  }

  renderContent() {
    var content;
    if (this.props.inventory.length <= 0) {
      return (<EmptyInventory />)
    } else {
      const filterBy = FilterMode[this.props.filterBy] || FilterMode.items;
      const sublist = this.props.inventory.filter(item => item.name.startsWith(filterBy));
      return (<FilledInventory inventory={sublist} />)
    }
  }

  renderFooter() {
    let alchemyButton = <div className="mwbuttonPlaceholder" />
    if (this.props.isAlchemyActive) {
      alchemyButton = (<Button onClick={() => this.goToAlchemy()}>{R.strings.scr_inv_btn_alchemy}</Button>)
    }
    return (
      <div className="headerButtonContainer">
        <Button onClick={() => this.back()}>{R.strings.back}</Button>
        &nbsp;
        {alchemyButton}
      </div>
    );
  }
}

class FightInventoryScreen extends CommonBaseScreen {
  constructor(props) {
    super(props);
  }

  back() {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, activeScreen: 'fight' } }
    ));
  }

  renderTitle() {
    return (R.strings.scr_inv_title);
  }

  renderHeader() {
    return (
      <div />
    );
  }

  renderContent() {
    var content;
    if (this.props.inventory.length <= 0) {
      return (<EmptyInventory />)
    } else {
      const sublist = this.props.inventory.filter(item => item.name.startsWith(FilterMode.potions));
      return (<FilledInventory inventory={sublist} />)
    }
  }

  renderFooter() {
    return (
      <div className='footerMenu'>
        <div class="mwbutton" href="#" onClick={() => this.back()}>{R.strings.back}</div>
      </div>
    );
  }
}

export default class InventoryScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.gameState == Game.State.STATE_FIGHTING) {
      return (<FightInventoryScreen inventory={this.props.inventory} />)
    } else {
      return (<IdleInventoryScreen 
          inventory={this.props.inventory}
          filterBy={this.props.filterBy} 
          isAlchemyActive={this.props.isAlchemyActive} />)
    }
  }
}
