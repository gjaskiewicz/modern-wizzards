import React, { Component } from 'react';
import CookieBanner, { cookie } from 'react-cookie-banner';
import './CookieButton.css';
import R from '../R.js';

const COOKIE_NAME = 'mw-accept-cookies';

export default class CookieButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: !(cookie(COOKIE_NAME) || false),
    }
  }

  onAccept() {
    this.setState(() => ({
      active: false,
    }));
    cookie(COOKIE_NAME, true);
  }

  componentDidMount() {
    let active = !(cookie(COOKIE_NAME) || false);
    this.setState(() => ({active}));
  }

  render() {
    if (!this.state.active) {
      return (<div style={{display: 'none'}}/>);
    }
    return (
    <div className="cookieContainer">
      <div className="cookieText">
        <div>
          {R.strings.cookie_text}
        </div>
      </div>
      <div className="cookieButton">
        <div>
          <div onClick={() => this.onAccept()}>
            {R.strings.cookie_accept}
          </div>
        </div>
      </div>
    </div>)
  }
}