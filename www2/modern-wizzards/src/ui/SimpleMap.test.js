import SimpleMap from './SimpleMap.js';
import TestData from '../test/TestData.js';

describe('Simple map', () => {

  it('should project Warsaw', () => {
    const tileCords = SimpleMap.getTileCoord({
      lat: () => TestData.cities.warsaw[0], 
      lng: () => TestData.cities.warsaw[1],
    });
    expect(tileCords.x).toBe(36593);
    expect(tileCords.y).toBe(21578);
  });

  it('should project Radom', () => {
    const tileCords = SimpleMap.getTileCoord({
      lat: () => TestData.cities.radom[0], 
      lng: () => TestData.cities.radom[1],
    });
    expect(tileCords.x).toBe(36621);
    expect(tileCords.y).toBe(21823);
  });

  it('should calculate ground resolution', () => {
    expect(SimpleMap.groundResolution(TestData.cities.warsaw[1])).toBe(2.2297544409166865);
    expect(SimpleMap.groundResolution(TestData.cities.radom[1])).toBe(2.2275040537506787);
    expect(SimpleMap.groundResolution(TestData.cities.auckland[1])).toBe(2.37859836104487);
  });

  it('should unproject #1', () => {
    expect(SimpleMap.unprojectY(0)).toBe(85.05112877980659);
    expect(SimpleMap.unprojectY(256)).toBe(85.05065487982755);
    expect(SimpleMap.unprojectY(256 * 65536 / 2)).toBe(0);
    expect(SimpleMap.unprojectY(256 * 65536)).toBe(-85.05112877980659);

    expect(SimpleMap.unprojectX(0)).toBe(-180);
    expect(SimpleMap.unprojectX(256)).toBe(-179.9945068359375);
    expect(SimpleMap.unprojectX(256 * 65536 / 2)).toBe(0);
    expect(SimpleMap.unprojectX(256 * 65536)).toBe(180);
  });

  it('should unproject #2', () => {
    const tileCords = SimpleMap.project({
      lat: () => TestData.cities.warsaw[0], 
      lng: () => TestData.cities.warsaw[1],
    });
    expect(SimpleMap.unprojectY(tileCords.lat) - TestData.cities.warsaw[0]).not.toBeGreaterThan(0.001);
    expect(SimpleMap.unprojectX(tileCords.lng) - TestData.cities.warsaw[1]).not.toBeGreaterThan(0.001);
  });
});
