import React, { Component } from 'react';
import './HorizontalBar.css';

export default class HorizontalBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const fillRatio = this.props.fillRatio || 0;
    const totalWidth = this.props.totalWidth || 100;
    const height = this.props.height || 32;
    const leftPart = Math.floor(totalWidth * Math.min(fillRatio, 1));
    const rightPart = totalWidth - leftPart;
    const overflow = Math.max(fillRatio - 1, 0);

    const containerStyle = {
      width: `${totalWidth}px`,
      height: `${height}px`,
    };
    const leftStyle = {
      width: `${leftPart}px`,
      height: `${height}px`,
      backgroundSize: `${totalWidth}px ${height}px`,
    };
    const rightStyle = {
      width: `${rightPart}px`,
      height: `${height}px`,
    };
    let overflowStyle = {
      display: 'none',
    };
    if (overflow) {
      overflowStyle = {
        opacity: Math.min(0.9, 0.5 + overflow),
      }
    }
    const clazz = `horizontalBarContainer ${this.props.className}`;
    return (
      <div className={clazz} style={containerStyle}>
        <div className='overflowPart' style={overflowStyle} />
        <div className='leftBarPart' style={leftStyle} />
        <div className='rightBarPart' style={rightStyle} />
      </div>
    )
  }
}
