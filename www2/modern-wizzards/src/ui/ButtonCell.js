import React, { Component } from 'react';
import './Button.css';
import './ButtonCell.css';

export class ButtonCell extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="mwbuttonCellContainer">
        <div className="mwbutton" onClick={this.props.onClick}>{this.props.children}</div>
      </div>
    )
  }
}

export class ButtonCellHolder extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="mwbuttonCellHolder">
        {this.props.children}
      </div>
    )
  }
}

//export default {ButtonCell, ButtonCellHolder}
