import React, { Component } from 'react';
import R from '../R.js';
import ObjectInfoFactory from '../gamelogic/metadata/ObjectInfoFactory.js';
import SpellFactory from '../gamelogic/SpellFactory.js';
import $log from '../utils/log.js';
import './FightScreen.css';
import PlayerEgo from '../PlayerEgo.js';
import Player from '../gamelogic/creatures/Player.js';
import Button from './Button.js';
import Color from '../Color.js';

const TAG = "FIGHT";

class FightSpell extends Component {

  static nextMarker = 1;

  constructor(props) {
    super(props);
  }

  castSpell() {
    const spellID = this.props.spellID;
    const fight = this.props.fight;
    $log.i(TAG, "Fighting: casting seleted: " + spellID);
    let spell = SpellFactory.getSpellByID(spellID, Player.INSTANCE);

    window.gameController.applyToState(s => {
      s = fight.playerCastsSpell(s, spell);
      return s;
    });
    this.props.onCast(spell);
  }

  render() {
    const spellInfo = ObjectInfoFactory.get().getInfoStandarized(this.props.spellID);
    return (
      <li className="fightSpellItem highlightable"
          onClick={() => this.castSpell()}>
        <img className="fightSpellItemImage"
            src={spellInfo.getImageID()} />
        <div className="fightSpellItemText">
          {spellInfo.getName()}
        </div>
      </li>
    )
  }
}

export default class FightScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      markers: { }
    };
  }

  behaviorToStyle(pos, opacity) {
    //const px = Math.floor((pos + 100) / 200 * 400);
    const p = Math.floor((pos + 100) / 2);
    return {
      left: `${p}%`, //`${px}px`,
      opacity: (opacity / 255),
    }
  }

  gotoPotions() {
    window.gameController.showScreen('inventory');
  }

  runAway() {
    window.gameController.applyToState(s => {
      const fight = this.props.fight;
      s = fight.playerRunsFromTheFight(s);
      return window.gameController.showOverlayState(s, null);
    });
  }
  
  showRunFromFightDialog() {
    let genericOverlayParams = {
      yesHandler: {
        title: R.strings.run_button,
        invoke: () => this.runAway()
      },
      noHandler: {
        title: R.strings.stay_button,
        invoke: () => window.gameController.showOverlay(null),
      },
      renderContent: () => (<span>{R.strings.escape_text_fc}</span>),
    };
    window.gameController.applyToState(s => {
      const state = window.gameController.showOverlayState(s, "alert_generic");
      return {
        ...state,
        ui: {
          ...state.ui,
          genericOverlayParams
        }
      };
    });
  }

  castedSpell(spell) {
    let next = FightSpell.nextMarker++;
    let range = spell.behaviourRange;
    this.setState(s => ({
      ...s,
      markers: { 
        ...s.markers, 
        [next]: {
          color: spell.color || 0xffff0000,
          range
        },
      }
    }));
    setTimeout(() => {
      this.setState(s => {
        let markers = {...s.markers};
        delete markers[next];
        return {...s, markers};
      })
    }, 500)
  }

  render() {
    const creature = this.props.creature;
    const playerEgo = this.props.playerEgo;
    const fight = this.props.fight;
    if (!creature) {
      return <div />;
    }
    let minOpacity = 0;
    if (PlayerEgo.isUnderEffectOfSpell(playerEgo, "potion_perception")) {
      minOpacity = 50;
    }
    const position = creature.getBehaviourPosition();
    const targetStyle = this.behaviorToStyle(
      position,
      Math.max(creature.getBehaviourOpacity(), minOpacity),
    );
    const spellButtons = Object.keys(playerEgo.knownFightSpells).map(
      (spellID, i) => (<FightSpell spellID={spellID} fight={fight} key={i} onCast={(s) => this.castedSpell(s)}/>)
    );
    const markers = Object.keys(this.state.markers).map(k => {
      let markerDef = this.state.markers[k];
      let styleDef = {
        left: `${(100 - markerDef.range) / 2}%`,
        width: `${markerDef.range}%`,
      };
      let innerStyleDef = {
        backgroundColor: Color.toRGBA(markerDef.color),
      };
      return (
        <div key={k} className="hit-wave" style={styleDef}>
          <div className="hit-wave-inner" style={innerStyleDef} />
        </div>
      )
    });
    return (
      <div>
        <div className="fightContainer">
          <div className="fightBackground">
            {markers}
            <img className="fightTarget" 
                src={R.drawables.mw_fight_behaviour_target}
                style={targetStyle}
            />
          </div>
        </div>
        <div className="fightSpellContainer">
          <ul>
            {spellButtons}
          </ul>
        </div>
        <div className="headerButtonContainer">
          <Button onClick={() => this.gotoPotions()}>{R.strings.scr_fight_potions}</Button>
          &nbsp;
          <Button onClick={() => this.showRunFromFightDialog()}>{R.strings.scr_fight_run}</Button>
        </div>
      </div>
    )
  }
}
