var GoogleMapsLoader = require('google-maps');
GoogleMapsLoader.KEY = 'AIzaSyDsS9QskFWRuSdrXoTPQl2iPoKbVSLWrPw';

default export class GoogleMap extends Component {

  constructor(props, google)  {
    super(props);
    // FIXME
    this.objDisplayed = { }; // new HashSet<PhysicalObject>();
    this.google = google;
    this.playerMarker = null;
    this.playerSight = null;
  }

  static init(google, map) {
    MapController.inst = new MapController(google, map);
  }

  activateStamenLayer() {
    const layerID = 'stamen_layer';
    // Create a new ImageMapType layer.
    const layer = new this.google.maps.ImageMapType({
      name: layerID,
      getTileUrl: (coord, zoom) => {
        console.log(coord); 
        return `http://c.tile.stamen.com/watercolor/${zoom}/${coord.x}/${coord.y}.jpg`;
      },
      tileSize: new this.google.maps.Size(256, 256),
      minZoom: 1,
      maxZoom: 20
    });
    this.map.mapTypes.set(layerID, layer);
    this.map.setMapTypeId(layerID);
  }

  update(po) {
    const loc = po.getLocation();
    const latlng = new this.google.maps.LatLng(loc.getLatitude(), loc.getLongitude());
    po.dispCircle.setCenter(latlng);
    if (po.zone && po.zone.radius !== undefined) {
      po.dispCircle.setRadius(po.zone.radius);
    }
    // po.dispCircle.setOptions
  }

  dontDisplayObj(po) {
    this.objDisplayed[po] = undefined;
    po.dispCircle.setMap(null);
  }

  initializeMap(elementId) {
    this.map = new this.google.maps.Map(document.getElementById(elementId), {
      center: {lat: -34.397, lng: 150.644},
      zoom: 16
    });
    this.map.setOptions({
      draggable: false,
      zoomControl: false,
      scrollwheel: false,
      disableDoubleClickZoom: true
    });

    this.playerMarker = new this.google.maps.Marker({
      position: {lat: -34.397, lng: 150.644},
      map: this.map
    });
  }

  diplayObj(obj) {
    this.objDisplayed[po] = true;
    if(this.map != null)  {
      const loc = po.getLocation();
      const latlng = new this.google.maps.LatLng(loc.getLatitude(), loc.getLongitude());
      const color = po.getColor();
      po.dispCircle = new this.google.maps.Circle({
        strokeColor: Color.toHexRGB(Color.TRANSPARENT),
        strokeOpacity: Color.opacity(Color.TRANSPARENT),
        strokeWeight: 2,
        fillColor: Color.toHexRGB(color),
        fillOpacity: Color.opacity(color),
        map: this.map,
        center: latlng,
        radius: po.zone.radius,
        clickable: false,
      });
      return po.dispCircle;
    } else { 
      return null; 
    }
  }

  showPlayerSight(state) {
    const latlng = new this.google.maps.LatLng(
      state.playerEgo.lat, state.playerEgo.lng);
    this.playerSight = new this.google.maps.Circle({
      strokeColor: Color.toHexRGB(Color.SIGHT_COLOR),
      strokeOpacity: Color.opacity(Color.SIGHT_COLOR),
      strokeWeight: 5,
      fillColor: Color.toHexRGB(Color.TRANSPARENT),
      fillOpacity: Color.opacity(Color.TRANSPARENT),
      map: this.map,
      center: latlng,
      radius: PlayerEgo.getSightRange(state.playerEgo),
      clickable: false,
    });
  }

  updateMap(state) {
    const latlng = new this.google.maps.LatLng(
      state.playerEgo.lat, state.playerEgo.lng);
    if (this.playerSight) {
      this.playerSight.setCenter(latlng);
    }
    if (this.playerMarker) {
      this.map.setCenter(latlng);
      this.playerMarker.setPosition(latlng);
    }
  }

  resize() {
    this.google.maps.event.trigger(this.map, 'resize');
  }

  componentDidMount() {
    GoogleMapsLoader.load(function(google) {
      GoogleMap.init(google);
      GoogleMap.getInst().initializeMap('map');
      GoogleMap.getInst().activateStamenLayer();
      GoogleMap.getInst().showPlayerSight(gameController.defaultState());
    });
  }
    
  render() {
    return (<div id="map"></div>);
  }
}
