import React, { Component } from 'react';

const mydump = function(arr, level, path={}) {
  var dumped_text = "";
  if(!level) level = 0;

  var level_padding = "";
  for(var j=0;j<level+1;j++) level_padding += "    ";

  if(typeof(arr) == 'object') {  
      for(var item in arr) {
          if (item === undefined) {
            continue;
          }
          if (item == 'dispCircle') {
            dumped_text += level_padding + "'" + item + "' => >>> map circle <<<\n";
            continue;
          }
          var value = arr[item];

          if(typeof(value) == 'object') { 
            if (path[item]) {
              return `${level_padding} >>> ref (${typeof(item)}) <<<\n`;
            }
            var lt = value && value.length !== undefined ? ` ${value.length}` : '';
            dumped_text += level_padding + `'${item}' ...${lt}\n`;
            var newPath = {...path};
            newPath[item] = true;
            dumped_text += mydump(value, level+1, newPath);
          } else {
            dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
          }
      }
  } else { 
      dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
  }
  return dumped_text;
}

export default class DebugState extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const data = this.props.state;
    const printableData = {...data};
    printableData.ui = ">>> redacted <<<\n";
    // printableData.playerEgo.underEffectOfSpell = [ ]; //data.playerEgo.underEffectOfSpell.length;
    const tx = mydump(printableData);
       //JSON.stringify(printableData, null, 2);
    return (
      <div><pre>{tx}</pre></div>
    )
  }
}
