import React, { Component } from 'react';
import * as firebase from 'firebase';
import R from "../R.js";
import QR from "../QR.js";
import CommonBaseScreen from './CommonBaseScreen.js';
import $log from '../utils/log.js';
import DebugState from './DebugState.js';
import Button from './Button.js';
import MWToast from './MWToast.js';
import VoyHr from './VoyHr.js';
import './LoginScreen.css';

import goog_sign_in from "../assets/ui/goog_sign_in.png";

const TAG = "LoginScreen";

export default class LoginScreen extends CommonBaseScreen {
  constructor(props) {
    super(props);
  }

  signInByGoogle() {
    var provider = new firebase.auth.GoogleAuthProvider();
    // provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
    firebase.auth().signInWithRedirect(provider)
      .catch((error) => {
        MWToast.toast(error.message || 'Authentication error', {type: "ERROR"});
      });
  }

  signInByFacebook() {
    var provider = new firebase.auth.FacebookAuthProvider();
    provider.addScope('public_profile');
    // provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
    firebase.auth().signInWithRedirect(provider)
      .catch((error) => {
        MWToast.toast(error.message || 'Authentication error', {type: "ERROR"});
      });
  }

  renderTitle() {
    return (<span>Sign in</span>);
  }

  renderHeader() {
    return (<span></span>);
  }

  renderContent() {
    return (
      <div>
        <div className="loginBtnContainer">
          <button className="loginBtn loginBtn--facebook"
                  onClick={() => this.signInByFacebook()}>
            Login with Facebook
          </button>
        </div>
        <div className="loginBtnContainer">
          <button className="loginBtn loginBtn--google"
                  onClick={() => this.signInByGoogle()}>
            Login with Google
          </button>
        </div>
        <div className="limitWidth">
          <VoyHr />
          <div className="loginWelcomeAbout">
            {QR.strings.welcome_message}
          </div>
        </div>
      </div>
    );
  }

  renderFooter() {
    return (
      <div className="headerButtonContainer">
      </div>
    );
  }
}
