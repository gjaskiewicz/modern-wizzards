import React, { Component } from 'react';
import HorizontalBar from './HorizontalBar.js';
import './ShortStats.css';

export default class CreatureShortStats extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const floatLeft = {
      float: 'left',
      marginLeft: '10px'
    }
    const creature = this.props.creature;
    if (!creature) {
      return (<div />);
    }
    const healthRatio = creature.health / creature.maxHealth;
    const manaRatio = creature.mana / creature.maxMana;
    const info = creature.getInfo();
    let avatar = <span />;
    if (this.props.isExpanded) {
      avatar = (<img style={floatLeft} className="creatureImage" src={info.getImageID()} />);
    }
    return (
      <div className="creatureContainer">
        {avatar}
        <div style={floatLeft}>
          <div className="playerStatsName">
            {info.getName()}
          </div>
          <div className="playerStatsLabel">HP</div>
          <div className="playerStatsBar">
            <HorizontalBar className='healthBar' fillRatio={healthRatio} height={8} />
          </div>
          <div className="playerStatsLabel">Mana</div>
          <div className="playerStatsBar">
            <HorizontalBar className='manaBar' fillRatio={manaRatio} height={8} />
          </div>
        </div>
      </div>
    )
  }
}
