import AlchemyScreen from './AlchemyScreen.js';
import TestData from '../test/TestData.js';
import GameController from '../main.js';
import Game from '../Game.js';
import GameSaver from '../utils/GameSaver.js';

import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-dom/test-utils';

describe('Alchemy', () => {
  let state;
  let alch;
  GameSaver.disable();

  let getElement = () => {
    return ReactTestUtils.renderIntoDocument(
      <AlchemyScreen 
          playerEgo={state.playerEgo}
          gameState={state.gameState}
      />);
  };

  beforeEach(() => {
    window.gameController = new GameController();
    window.gameController.dispatch = (func) => {state = func(state)};
    state = {
      gameState: Game.State.STATE_WALKING,
      playerEgo: TestData.getFullEgo(),
      ui: { },
      achivments: { },
    };
    state.playerEgo.inventory = 
      [{name: 'item_daisyflower', count: 3}];
    alch = getElement();
  });

  it('should be able to make potions', () => { 
    const items = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyItem img");
    expect(items.length).toBe(1);

    // Add 1st item
    ReactTestUtils.Simulate.click(items[0]);
    const items_1 = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyMixList .alchemyItem");
    const result_1 = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyResultList .alchemyItem");
    expect(items_1.length).toBe(1);
    expect(result_1.length).toBe(0);

    // Add 2nd item
    ReactTestUtils.Simulate.click(items[0]);
    const items_2 = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyMixList .alchemyItem");
    const result_2 = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyResultList .alchemyItem");
    expect(items_2.length).toBe(2);
    expect(result_2.length).toBe(0);

    // Add 3nd item
    ReactTestUtils.Simulate.click(items[0]);
    const items_3 = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyMixList .alchemyItem");
    const result_3 = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyResultList .alchemyItem");
    expect(items_3.length).toBe(3);
    expect(result_3.length).toBe(1);

    const items2 = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyItem img");
    expect(items2.length).toBe(5);
    ReactTestUtils.Simulate.click(items2[4]); // FIXME
    expect(state.playerEgo.inventory).toEqual([{"name": "potion_health"}]);
  });

  it('should be able to suggest items', () => { 
    const items = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyItem img");
    expect(items.length).toBe(1);

    // Add 1st item
    ReactTestUtils.Simulate.click(items[0]);
    const items_1 = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyMixList .alchemyItem");
    const result_1 = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyResultList .alchemyItem");
    expect(items_1.length).toBe(1);
    expect(result_1.length).toBe(0);

    // Add 2nd item
    ReactTestUtils.Simulate.click(items[0]);
    const items_2 = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyMixList .alchemyItem");
    const result_2 = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyResultList .alchemyItem");
    expect(items_2.length).toBe(2);
    expect(result_2.length).toBe(0);
    const items2 = ReactDOM.findDOMNode(alch).querySelectorAll(".alchemyItem img");
    expect(items2.length).toBe(5);
  });
});
