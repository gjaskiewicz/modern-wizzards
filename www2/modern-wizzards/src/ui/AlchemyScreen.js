import React, { Component } from 'react';
import R from "../R.js";
import CommonBaseScreen from './CommonBaseScreen.js';
import ObjectInfoFactory from '../gamelogic/metadata/ObjectInfoFactory.js';
import AllSpells from '../AllSpells.js';
import Inventory from '../gamelogic/Inventory.js';
import Game from '../Game.js';
import GameSaver from '../utils/GameSaver.js';
import PlayerEgo from '../PlayerEgo.js';
import $log from '../utils/log.js';
import Button from './Button.js';
import EventSystem from '../gamelogic/events/EventSystem.js';
import EventTypes from '../gamelogic/events/EventTypes.js';
import VoyHr from './VoyHr.js';
import './AlchemyScreen.css';
import './Animations.css';

const TAG = 'ALCHEMY';

class InventoryElement extends Component {
  constructor(props) {
    super(props);
  }

  applyItem() {
    if (this.props.onApply) {
      this.props.onApply(this.props.item || this.props.itemName);
    }
  }

  render() {
    const itemInfo = ObjectInfoFactory.get().getInfoStandarized(
      (this.props.item || {}).name || this.props.itemName);

    let nameEl = <span />;
    if (this.props.showName) {
      nameEl = <span className="alchemyName">{itemInfo.getName()}</span>;
    }
    let classes = ["alchemyItem"];
    if (this.props.blocked) {
      classes.push("alchemyItemBlocked");
    }
    return (
      <div className={classes.join(" ")}>
        <img className="fade-in" 
            src={itemInfo.getImageID()} 
            onClick={() => this.applyItem()}
        />
        {nameEl}
      </div>
    );
  }
}

export default class AlchemyScreen extends CommonBaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      mixpotItems: [ ],
      possiblePotions: [ ],
      suggestion: { },
    }
  }

  back() {
    window.gameController.applyToState(s => (window.gameController.showBackScreenState(s)));
  }

  renderTitle() {
    return (R.strings.scr_alch_title);
  }

  renderHeader() {
    return (<span />);
  }

  clear() {
    this.setState(s => ({ 
      ...s, 
      mixpotItems: [], 
      possiblePotions: [], 
      suggestion: { } 
    }));
  }

  getPossiblePotions(mixpotItems) {
    const knownPotions = this.props.playerEgo.knowledge.knownPotions;
    if (mixpotItems.length == 3) {
      return AllSpells.get()
        .possiblePotions(mixpotItems.map(i => i.name))
        .filter(potion => knownPotions[potion]);
    }
    return [ ];
  }

  getSuggestion(mixpotItems) {
    const knownPotions = this.props.playerEgo.knowledge.knownPotions;
    if (mixpotItems.length == 2) {
      let suggestions = AllSpells.get().suggestAll(mixpotItems.map(i => i.name)) || [ ];
      suggestions = suggestions.filter(s => knownPotions[s.result]) || [ ];
      return suggestions[0] || { };
    }
    return { };
  }

  applyItem(item) {
    this.setState(s => {
      let invCount = 0;
      this.props.playerEgo.inventory.forEach((i) => {
        i.name == item.name ? invCount += i.count : undefined;
      });
      const mixCount =
          s.mixpotItems.filter(i => i.name == item.name).length;
      if (invCount <= mixCount) {
        $log.i(TAG, `insufficient ${item.name}`);
        return s;
      }
      if (s.mixpotItems.length < 3) {
        const mixpotItems = [...s.mixpotItems, item];
        const possiblePotions = this.getPossiblePotions(mixpotItems);
        const suggestion = this.getSuggestion(mixpotItems);
        return { ...s, mixpotItems, possiblePotions, suggestion };
      } else {
        return s;
      }
    });
  }

  removeItem(idx) {
    this.setState(s => {
      const mixpotItems = [...s.mixpotItems];
      mixpotItems.splice(idx, 1);
      const possiblePotions = this.getPossiblePotions(mixpotItems);
      const suggestion = this.getSuggestion(mixpotItems);
      return { ...s, mixpotItems, possiblePotions, suggestion };
    });
  }

  make(itemName) {
    const mixpot = this.state.mixpotItems;
    this.clear();
    window.gameController.applyToState(s => {
      let inv = s.playerEgo.inventory;
      for (let item of mixpot) {
        inv = Inventory.get().removePlant(inv, item.name);
      }
      inv = [...inv, {name: itemName}];

      s = Game.getGame().getAchivContainer().getAchivMixtures().addMixtureMade(s, itemName);
      s = EventSystem.INSTANCE.notifyListenersState(
        s, itemName, EventTypes.CREATED_BY_ALCHEMY, 
        {usedPlants: (mixpot.map(i => i.name))});
      s = {
        ...s,
        playerEgo: {
          ...PlayerEgo.addExperience(s.playerEgo, 200),
          inventory: inv,
        },
        ui: {
          activeScreen: 'inventory',
        }
      };
      GameSaver.saveGame(s);
      return s;
    });
  }

  renderContent() {
    const items = this.props.playerEgo.inventory.filter(i => i.name.startsWith('item_'));
    const canApply = (item) => 
      this.state.mixpotItems.filter(i => i.name == item.name).length < item.count;

    const itemsList = items.map((item, i) => (
      <InventoryElement 
          item={item} 
          key={i} 
          onApply={(item) => this.applyItem(item)}
          blocked={!canApply(item)}
          showName={true}/>
    ));
    const mixpotList = this.state.mixpotItems.map((item, i) => (
      <InventoryElement 
          item={item} 
          key={i} 
          onApply={(item) => this.removeItem(i)}
          showName={true}/>
    ));
    const potionsList = this.state.possiblePotions.map((itemName, i) => (
      <InventoryElement 
          itemName={itemName} 
          key={i} 
          onApply={(itemName) => this.make(itemName)}
          showName={true}/>
    ));
    let hint = <span />;
    if (this.state.suggestion.item) {
      hint = (
        <div>
          <div className="alchemySectionText">HINT</div>
          <div className="hintContent">
            <div className="hintText">Use</div>
            <InventoryElement itemName={this.state.suggestion.item} showName={true} />
            <div className="hintText">to get</div>
            <InventoryElement itemName={this.state.suggestion.result} showName={true} />
          </div>
        </div>
      );
    }

    return (
      <div>
        <div className="alchemySectionText">ITEMS</div>
        <div className="alchemyItemsList">{itemsList}</div>
        <VoyHr />
        <div className="alchemySectionText">MIXING POT</div>
        <div className="alchemyMixList">{mixpotList}</div>
        <VoyHr />
        <div className="alchemySectionText">RESULT</div>
        <div className="alchemyResultList">{potionsList}</div>
        <VoyHr />
        {hint}
      </div>
    );
  }

  renderFooter() {
    return (
      <div className="headerButtonContainer">
        <Button onClick={() => this.back()}>{R.strings.back}</Button>
        &nbsp;
        <Button onClick={() => this.clear()}>{R.strings.scr_alch_clear}</Button>
      </div>
    );
  }
}
