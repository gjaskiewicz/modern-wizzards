import React, { Component } from 'react';
import R from "../R.js";
import CommonBaseScreen from './CommonBaseScreen.js';
import $log from '../utils/log.js';
import Button from './Button.js';
import VoyHr from './VoyHr.js';
import Quest from '../quests/Quest.js';
import './QuestMessageScreen.css';

const TAG = "QuestMessageScreen";

export default class QuestMessageScreen extends CommonBaseScreen {
  constructor(props) {
    super(props);
    this.lastQuest = null;
  }

  back() {
    window.gameController.applyToState(s => (window.gameController.showBackScreenState(s)));
  }

  accept() {
    window.gameController.applyToState(s => {
      const quest = this.props.quest;
      return quest.setAccepted(s);
    });
  }

  reject() {
    window.gameController.applyToState(s => {
      const quest = this.props.quest;
      return quest.setRejected(s);
    });
  }

  renderTitle() {
    const item = this.props.item;
    return (<span>{item && item.getName()}</span>);
  }

  renderHeader() {
    const quest = this.props.quest;
    const img = quest && quest.getFromImg();
    if (img) {
      return (<div>
        {img}
      </div>);
    }
    return (<span></span>);
  }

  renderContent() {
    const quest = this.props.quest;
    if (!quest) return <span />;
    if (this.lastQuest != quest) {
      this.resetScroll();
    }
    this.lastQuest = quest;

    return (
      <div className="messageMain">
        {quest.getFullMessageText()}
      </div>
    );
  }

  renderFooter() {
    const quest = this.props.quest;
    const state = this.props.state;
    let onUseBtn;
    if (this.props.onUse) {
      onUseBtn = (<Button onClick={() => this.useItem()}>{R.strings.use}</Button>);
    } else {
      onUseBtn = (<div className="mwbuttonPlaceholder" />);
    }
    let acceptUi = <div />;
    if (quest && !quest.isNarration) {
      if (quest.getAcceptanceState(state) == Quest.AcceptanceState.NONE) {
        acceptUi = (
          <div className="headerButtonContainer">
            <Button onClick={() => this.accept()}>{R.strings.accept}</Button>
            &nbsp;
            <Button onClick={() => this.reject()}>{R.strings.reject}</Button>
          </div>);
      }
    }
    let closeBtn;
    if (!quest || quest.canClose === undefined || !!quest.canClose) {
      closeBtn = <Button onClick={() => this.back()}>{R.strings.back}</Button>;
    } else {
      closeBtn = (<div className="mwbuttonPlaceholder" />);
    }
    return (
      <div>
        {acceptUi}
        <VoyHr />
        <div className="headerButtonContainer">
          {closeBtn}
          &nbsp;
          {onUseBtn}
        </div>
      </div>
    );
  }
}
