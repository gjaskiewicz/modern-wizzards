import React, { Component } from 'react';
import R from '../R.js';
import Game from '../Game.js';
import {ButtonCell, ButtonCellHolder} from './ButtonCell.js';
import './AlertScreen.css';

export default class AlertScreen extends Component {
  constructor(props) {
    super(props);
    this.yesHandler = null;
    this.noHandler = null;
  }

  buttonForHandler(handler) {
    // FIXME: first fight message fix
    if (!handler || !handler.title) {
      return (<span />);
    } else {
      return (<ButtonCell onClick={() => handler.invoke()}>{handler.title}</ButtonCell>);
    }
  }

  renderContent() {
    if (window.isDev) {
      return (<span>!!Content!!</span>);
    } else {
      return <span />;
    }
  }

  getTitleText() {
    if (window.isDev) {
      return "!!Title!!";
    } else {
      return "";
    }
  }

  getHeadingText() {
    if (window.isDev) {
      return "!!Heading!!"
    } else {
      return "";
    }
  }

  render() {
    const yesButton = this.buttonForHandler(this.yesHandler);
    const noButton = this.buttonForHandler(this.noHandler);
    return (
      <div className="alertMessageContainer">
        <h1>{this.getTitleText()}</h1>
        <h2>{this.getHeadingText()}</h2>
        <div className="alertMessageContent">
          {this.renderContent()}
        </div>
        <div className="alertActionContainer">
          <ButtonCellHolder>
            {yesButton}
            &nbsp;
            {noButton}
          </ButtonCellHolder>
        </div>
      </div>
    );
  }
}
