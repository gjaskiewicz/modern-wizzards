import React, { Component } from 'react';
import R from '../R.js';
import Game from '../Game.js';
import AlertScreen from "./AlertScreen.js";

export default class AlertItemActivity extends AlertScreen {
  constructor(props) {
    super(props);
    this.yesHandler = {
      title: R.strings.alert_creature_fight,
      invoke: () => this.onFight(),
    }
    this.noHandler = {
      title: R.strings.alert_creature_run,
      invoke: () => this.onRun(),
    }
  }

  onRun() {
    const creature = this.props.creature;
    window.gameController.applyToState(s => {
      s = window.gameController.showOverlayState(s, null);
      s = Game.getGame().playerRunsFromCreature(s, creature);
      return s;
    });
  }

  onFight() {
    const creature = this.props.creature;
    window.gameController.applyToState(s => {
      s = window.gameController.showOverlayState(s, null);
      s = Game.getGame().playerFightsCreature(s, creature);
      return s;
    });
  }

  renderContent() {
    const creature = this.props.creature;
    const info = creature && creature.getInfo();
    if (!info) {
      return (<span />);
    }
    return (
      <div>
        <img src={info.getImageID()} />
        <br />
        <span>{R.strings.alert_creature_question}</span>
      </div>);
  }

  getHeadingText() {
    const creature = this.props.creature;
    const info = creature && creature.getInfo();
    return info && info.getName();
  }

  getTitleText() {
    return R.strings.alert_creature_title;
  }
}
