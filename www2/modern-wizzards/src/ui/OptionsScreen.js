import CommonBaseScreen from "./CommonBaseScreen.js";
import R from "../R.js";

export default class OptionsScreen extends CommonBaseScreen {
  constructor(props) {
    super(props);
  }

  back() {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, activeScreen: 'map' } }
    ));
  }

  renderTitle() {
    return (R.strings.opt_title);
  }

  renderContent() {
    return (<span>!!Set content!!</span>);
  }

  renderFooter() {
    return (
      <div>
        <a href="#" onClick={() => this.back()}>{R.strings.back}</a>
      </div>
    );
  }
}
