import Game from '../Game.js';
import Color from '../Color.js';
import PlayerEgo from '../PlayerEgo.js';
import GameState from '../GameState.js';
import ManaZone from '../gamelogic/world/ManaZone.js';

import React, { Component } from 'react';
import './SimpleMap.css';

const TILE_SIZE = 256;
const WORLD_SIZE = TILE_SIZE * 65536;

const getTileAddr = function(x, y) {
  return `http://c.tile.stamen.com/watercolor/16/${x}/${y}.jpg`;
}

const getBackupTileAddr = function(x, y) {
  return `http://mt1.google.com/vt/lyrs=m&x=${x}&y=${y}&z=16`;
}

const googLatLng = (lat, lng) => ({
  lat: () => lat,
  lng: () => lng
});

class MapObject extends Component {
  constructor(props) {
    super(props);
  }

  lat() {
    return this.props.pos.lat;
  }

  lng() {
    return this.props.pos.lng;
  }

  className() {
    return "";
  }

  render() {
    const {wp, mpp, offset} = this.props;
    const rad = Math.floor(this.props.rad / mpp);
    const {lat, lng} = SimpleMap.project(this);
    const ofsX = Math.floor(-offset.lng + lng - rad + wp.width/2);
    const ofsY = Math.floor(-offset.lat + lat - rad + wp.height/2);
    const color = this.props.color;
    const borderColor = this.props.borderColor;

    let style = {
      left: `${ofsX}px`,
      top: `${ofsY}px`,
      width: `${rad * 2}px`,
      height: `${rad * 2}px`,
      backgroundColor: Color.toRGBA(color),
    };
    if (borderColor) {
      style = {...style,
        borderColor: Color.toRGBA(borderColor),
        borderWidth: '2px',
        borderStyle: 'solid',
      };
    }
    return (<div className={this.className()} style={style}/>);
  }
}

class CircleOutlineMarker extends MapObject {
  className() {
    return "mapOutlineCircleMarker";
  }
}

class CircleMarker extends MapObject {
  className() {
    return "mapCircleMarker";
  }
}

export default class SimpleMap extends Component {
  // The mapping between latitude, longitude and pixels is defined by the web
  // mercator projection.
  static project(latLng) {
    var siny = Math.sin(latLng.lat() * Math.PI / 180);
    // Truncating to 0.9999 effectively limits latitude to 89.189. This is
    // about a third of a tile past the edge of the world tile.
    siny = Math.min(Math.max(siny, -0.9999), 0.9999);
    return {
      lat: WORLD_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)),
      lng: WORLD_SIZE * (0.5 + latLng.lng() / 360),
    };
  }

  static getTileCoord(latLng) {
    const pix = SimpleMap.project(latLng);
    const x = Math.floor(pix.lng / 256);
    const y = Math.floor(pix.lat / 256);
    return {x, y};
  }

  static unprojectY(y) {
    y = -(y / WORLD_SIZE - 0.5) * (4 * Math.PI)
    const gd = Math.atan(Math.sinh(y / 2)); // gudermannian function
    return gd * 180 / Math.PI;
  }

  static unprojectX(x) {
    return (x / WORLD_SIZE - 0.5) * 360;
  }

  static groundResolution(lat) { // (meters / pixel)
    return Math.abs(
      Math.cos(lat * Math.PI / 180) * 2 * Math.PI * 6378137) / WORLD_SIZE;
  }

  constructor(props) {
    super(props);
    this.clickHandlers = [ ];
    this.backup = { };
    if (window.simpleMap) {
      throw "Simple map already used";
    }
    window.simpleMap = this;
  }

  lat() {
    return this.props.state.playerEgo.lat;
  }

  lng() {
    return this.props.state.playerEgo.lng;
  }

  static getViewportSize() {
    const width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    const height = window.innerHeight
      || document.documentElement.clientHeight
      || document.body.clientHeight;

    return {width, height};
  }

  addClickHandler(handler) {
    this.clickHandlers = [...this.clickHandlers, handler];
    return () => {
      this.clickHandlers = this.clickHandlers.filter(h => h !== handler);
    };
  }
  
  handleClick(e, tileX, tileY) {
    const rect = e.target.getBoundingClientRect();
    let px = e.clientX - rect.x;
    let py = e.clientY - rect.y;
    const lng = SimpleMap.unprojectX(tileX * TILE_SIZE + px);
    const lat = SimpleMap.unprojectY(tileY * TILE_SIZE + py);
    for (let handler of this.clickHandlers) {
      handler(googLatLng(lat, lng));
    }
  }

  render() {
    let playerEgo = this.props.state.playerEgo;
    if (!playerEgo) {
      return <div />;
    }
    const mpp = SimpleMap.groundResolution(this.lat());
    const wp = SimpleMap.getViewportSize();
    const covX = Math.floor(wp.width / 2 / TILE_SIZE) + 1;
    const covY = Math.floor(wp.height / 2 / TILE_SIZE) + 1;

    const {lat, lng} = SimpleMap.project(this);
    const {x, y} = SimpleMap.getTileCoord(this);
    const tiles = [ ];
    for (let i = -covX; i <= covX; i++) {
      for (let j = -covY; j <= covY; j++) {
        if (x + i < 0) continue;
        if (y + j < 0) continue;
        const ofsX = Math.floor((x + i) * TILE_SIZE - lng + wp.width / 2);
        const ofsY = Math.floor((y + j) * TILE_SIZE - lat + wp.height / 2);
        const style = {
          left: `${ofsX}px`,
          top: `${ofsY}px`,
        };
        const key = `${x+i}--${y+j}`;
        let addr = getTileAddr(x + i, y + j);
        if (this.backup[key]) {
          addr = getBackupTileAddr(x + i, y + j);
        }
        const cssClasses = ["mapTile"];
        if (playerEgo.isDev) {
          cssClasses.push("devTile");
        }

        tiles.push(<img src={addr} 
                        key={key}
                        onError={() => this.backup[key] = true}
                        className={cssClasses.join(" ")} 
                        style={style} 
                        onClick={(e) => this.handleClick(e, x + i, y + j)}
                    />);
      }
    }

    const pos = {
      lat: playerEgo.lat,
      lng: playerEgo.lng,
    };
    const offset = {lat, lng};
    let sightRange = <div />;
    const sightRangeDist = PlayerEgo.getSightRange(playerEgo);
    if (sightRangeDist) {
      sightRange = (<CircleOutlineMarker pos={pos} 
          offset={offset} 
          rad={sightRangeDist}
          wp={wp}
          mpp={mpp}
      />);
    }

    const objToMarker = (obj) => {
      const gp = obj.getGeoPoint();
      return (<CircleMarker
        pos={gp}
        offset={offset} 
        rad={obj.zone.radius}
        wp={wp}
        mpp={mpp}
        color={obj.getColor()}
      />)
    };

    const objects = [ ];
    for (let obj of Game.getGame().getAllObjects()) {
      if (!obj.displayed) {
        continue;
      }
      if (this.props.state.gameState == GameState.STATE_PLAYERDEAD &&
          !(obj instanceof ManaZone)) {
        continue;
      }
      objects.push(objToMarker(obj));
    }
    const hasInvisibilityEffect = 
        PlayerEgo.isUnderEffectOfSpell(playerEgo, "potion_invisibility");
    const player = (<CircleMarker
      pos={pos}
      offset={offset} 
      rad={20}
      wp={wp}
      mpp={mpp}
      color={!hasInvisibilityEffect ? 0xffee82ee : 0xaaee82ee}
      borderColor={!hasInvisibilityEffect ? 0x00000000 : 0xffee82ee}
    />);

    const oobPointers = [ ];
    for (let spell of (playerEgo.underEffectOfSpell || [ ])) {
      // znalezc lepszy sposob
      if (spell.oobPointer) {
        oobPointers.push(objToMarker(spell.oobPointer));
      }
    }

    return (<div className="mapContainer">
      {tiles}
      {sightRange}
      {objects}
      {player}
      {oobPointers}
    </div>);
  }
}
