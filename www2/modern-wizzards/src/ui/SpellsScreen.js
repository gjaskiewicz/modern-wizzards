import React, { Component } from 'react';
import R from "../R.js";
import SpellFactory from '../gamelogic/SpellFactory.js';
import CommonBaseScreen from './CommonBaseScreen.js';
import Player from '../gamelogic/creatures/Player.js';
import Button from './Button.js';

const FilterMode = {
  GLOBAL: 'global',
  BATTLE: 'battle'
}

export default class SpellsScreen extends CommonBaseScreen {
  constructor(props) {
    super(props);
  }

  back() {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, activeScreen: 'map' } }
    ));
  }

  renderTitle() {
    return (R.strings.scr_spells_title);
  }

  toggleFilterMode(mode) {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, spellListFilter: mode } }
    ));
  }

  detailsView(item) {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, activeScreen: 'spell_details', spellDetailsItem: item} }
    ));
  }

  renderHeader() {
    return (
      <div className="headerButtonContainer">
        <Button onClick={() => this.toggleFilterMode(FilterMode.GLOBAL)}>
          {R.strings.scr_spells_global}
        </Button>
        &nbsp;
        <Button onClick={() => this.toggleFilterMode(FilterMode.BATTLE)}>
          {R.strings.scr_spells_battle}
        </Button>
      </div>
    );
  }

  castSpell(spellID) {
    const spell = SpellFactory.getSpellByID(spellID, Player.INSTANCE);
    if (this.props.filterBy != FilterMode.BATTLE) {
      window.gameController.applyToState(s => {
        const {state} = spell.castThis(s);
        return state;
      });
    }
  }

  renderContent() {
    const playerEgo = this.props.playerEgo;
    let knownSpells = [];
    if (this.props.filterBy == FilterMode.GLOBAL) {
      knownSpells = Object.keys(playerEgo.knownGlobalSpells);
    } else if (this.props.filterBy == FilterMode.BATTLE) {
      knownSpells = Object.keys(playerEgo.knownFightSpells);
    } else {
      // default
      knownSpells = Object.keys(playerEgo.knownGlobalSpells);
    }

    const spells = knownSpells.map(
      spellID => {
        const spell = SpellFactory.getSpellByID(spellID, null);
        return spell && spell.getInfo();
      }
    );
    const renderSpells = spells.filter(spell => !!spell);
    if (!renderSpells.length) {
      return (<div>{R.strings.scr_spells_empty_list}</div>);
    }

    const list = renderSpells
      .map((info, i) => (
      <li className="inventoryItem highlightable" 
          key={i}
          onClick={() => this.castSpell(info.id)}>
        <img className="inventoryImage" src={info.getImageID()} />
        <div className="inventoryText">
          {info.getName()}
        </div>
        <br />
      </li>
    ));
    return (<ul>{list}</ul>);
  }

  renderFooter() {
    return (
      <div className="headerButtonContainer">
        <Button onClick={() => this.back()}>{R.strings.back}</Button>
        &nbsp;
        <div className="mwbuttonPlaceholder" />
      </div>
    );
  }
}
