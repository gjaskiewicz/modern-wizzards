import React, { Component } from 'react';
import R from '../R.js';
import Game from '../Game.js';
import AlertScreen from "./AlertScreen.js";

export default class AlertItemActivity extends AlertScreen {
  constructor(props) {
    super(props);
    this.yesHandler = {
      title: R.strings.alert_item_take,
      invoke: () => this.onYes(),
    }
    this.noHandler = {
      title: R.strings.alert_item_leave,
      invoke: () => this.onNo(),
    }
  }

  onYes() {
    const collectItem = this.props.collectItem;
    window.gameController.applyToState(s => {
      s = Game.getGame().collectItem(s, collectItem);
      s = window.gameController.showOverlayState(s);
      return s;
    });
  }

  onNo() {
    const collectItem = this.props.collectItem;
    window.gameController.applyToState(s => {
      collectItem.playerWasUninterested = true; // side-effect
      s = window.gameController.showOverlayState(s);
      return s;
    });
  }

  renderContent() {
    const collectItem = this.props.collectItem || { };
    const item = collectItem.item;
    const info = item && item.getInfo();
    if (!info) {
      return (<span />);
    }
    return (<img src={info.getImageID()} />);
  }

  getHeadingText() {
    const collectItem = this.props.collectItem || { };
    const item = collectItem.item;
    const info = item && item.getInfo();
    return info && info.getName();
  }

  getTitleText() {
    return R.strings.alert_item_title;
  }
}
