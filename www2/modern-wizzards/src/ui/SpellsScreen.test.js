import SpellsScreen from './SpellsScreen.js';
import TestData from '../test/TestData.js';
import GameController from '../main.js';
import Game from '../Game.js';

import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';

describe('InventoryScreen', () => {
  let state;
  let inv;

  beforeEach(() => {
    window.gameController = new GameController();
    window.gameController.dispatch = (func) => {state = func(state)};
    state = {
      gameState: Game.State.STATE_WALKING,
      playerEgo: TestData.getFullEgo(),
      ui: { }
    };
    inv = ReactTestUtils.renderIntoDocument(
      <SpellsScreen 
          playerEgo={state.playerEgo}
      />);
  });

  it('should show kombat spells', () => {
    const buttons = ReactTestUtils.scryRenderedDOMComponentsWithClass(inv, 'mwbutton');
    const mapSpellsButton = buttons.filter(b => b.textContent == 'BATTLE')[0];
    ReactTestUtils.Simulate.click(mapSpellsButton);
  });

  it('should show map spells', () => {
    const buttons = ReactTestUtils.scryRenderedDOMComponentsWithClass(inv, 'mwbutton');
    const mapSpellsButton = buttons.filter(b => b.textContent == 'GLOBAL')[0];
    ReactTestUtils.Simulate.click(mapSpellsButton);

    const items = ReactTestUtils.scryRenderedDOMComponentsWithClass(inv, 'inventoryItem');
    expect(items.length).toBe(1);
    expect(items[0].textContent).toBe("Bilocation");
/*
    const items = ReactTestUtils.scryRenderedDOMComponentsWithClass(inv, 'inventoryItem');
    expect(items.length).toBe(1);
    expect(items[0].textContent).toBe(' Uderzenie mocy');
*/
  });

  it('should show spell details', () => {
    const buttons = ReactTestUtils.scryRenderedDOMComponentsWithClass(inv, 'mwbutton');
    const mapSpellsButton = buttons.filter(b => b.textContent == 'GLOBAL')[0];
    ReactTestUtils.Simulate.click(mapSpellsButton);

    /*
    const buttons = ReactTestUtils.scryRenderedDOMComponentsWithClass(inv, 'inventoryImage');
    expect(buttons.length).toBe(1);
    ReactTestUtils.Simulate.click(buttons[0]);

    expect(state.ui.activeScreen).toBe('spell_details');
    expect(!!state.ui.spellDetailsItem).toBe(true);
    expect(state.ui.spellDetailsItem.id).toBe('spell_physical');
    */
  });
});

