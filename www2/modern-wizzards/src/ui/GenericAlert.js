import React, { Component } from 'react';
import R from '../R.js';
import AlertScreen from "./AlertScreen.js";

export default class GenericAlert extends AlertScreen {
  constructor(props) {
    super(props);
    this.yesHandler = {
      invoke: () => this.props.params.yesHandler.invoke(),
    };
    Object.defineProperty(this.yesHandler, 'title', {
      get: () => this.props.params.yesHandler && this.props.params.yesHandler.title
    });
    this.noHandler = {
      title: R.strings.alert_creature_run,
      invoke: () => this.props.params.noHandler.invoke(),
    };
    Object.defineProperty(this.noHandler, 'title', {
      get: () => this.props.params.noHandler && this.props.params.noHandler.title
    });
  }

  getTitleText() {
    if (this.props.params.getTitleText) {
      return this.props.params.getTitleText();
    } else {
      return <span />
    }
  }

  getHeadingText() {
    if (this.props.params.getHeadingText) {
      return this.props.params.getHeadingText();
    } else {
      return <span />
    }
  }

  renderContent() {
    if (this.props.params.renderContent) {
      return this.props.params.renderContent();
    } else {
      return <span />
    }
  }
}
