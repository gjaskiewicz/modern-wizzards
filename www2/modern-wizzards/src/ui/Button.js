import React, { Component } from 'react';
import './Button.css';

export default class Button extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="mwbuttonContainer">
        <div className="mwbutton" onClick={this.props.onClick}>{this.props.children}</div>
      </div>
    )
  }
}