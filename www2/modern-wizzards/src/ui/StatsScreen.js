import React, { Component } from 'react';
import CommonBaseScreen from "./CommonBaseScreen.js";
import HorizontalBar from "./HorizontalBar.js";
import R from "../R.js";
import Game from '../Game.js';
import PlayerEgo from '../PlayerEgo.js';
import GameContent from '../gamelogic/GameContent.js';
import ObjectInfoFactory from '../gamelogic/metadata/ObjectInfoFactory.js';
import Button from './Button.js';
import './StatsScreen.css';

export default class OptionsScreen extends CommonBaseScreen {
  constructor(props) {
    super(props);
  }

  back() {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, activeScreen: 'map' } }
    ));
  }

  showInfo(item) {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, activeScreen: 'stats_details', statsDetailsItem: item } }
    ));
  }

  renderTitle() {
    return (R.strings.scr_stats_title);
  }

  renderContent() {
    const playerEgo = this.props.state.playerEgo;
    const healthRatio = playerEgo.health / playerEgo.maxHealth;
    const manaRatio = playerEgo.mana / playerEgo.maxMana;

    const xp = playerEgo.experience;
    const nextXp = PlayerEgo.getExpForLevel(playerEgo.expLevel + 1);
    const expRange = nextXp - PlayerEgo.getExpForLevel(playerEgo.expLevel);
    const currentProgres = xp - PlayerEgo.getExpForLevel(playerEgo.expLevel);
    const expRatio = Math.max(0, currentProgres / expRange);

    let spellsView;
    if (!playerEgo.underEffectOfSpell.length) {
      spellsView = (<span>{R.strings.scr_stats_spells_none}</span>);
    } else if (this.props.state.gameState == Game.State.STATE_PLAYERDEAD) { 
      spellsView = (<span>{R.strings.scr_stats_resurect}</span>);
    } else {
      const spellsList = playerEgo.underEffectOfSpell.map(spl => (<li>{spl.getInfo().getName()}</li>));
      spellsView = (<ul>{spellsList}</ul>);
    }

    const allCreatures = GameContent.getInstance().getAllCreaturesTypes();
    const creaturesView = allCreatures.map(c => {
      const info = c.getInfo();
      const image = info.getImageID();
      return (<img src={image} className="statsItem" onClick={() => this.showInfo(info)} />);
    });

    const allAchiv = Game.getGame().getAchivContainer().getAllAchivs();
    const achivView = allAchiv.map(a => {
      let image;
      const info = a.getInfo();
      if(!a.isAchived(this.props.state)) {
        image = R.drawables.achiv_unknown;
      } else {
        image = info.getImageID();
      }
      return (<img src={image} className="statsItem" onClick={() => this.showInfo(info)} />);
    });

    const allPlants = GameContent.getInstance().getAllPlantsTypes();
    const plantsView = allPlants.map(c => {
      const info = c.getInfo();
      const image = info.getImageID();
      return (<img src={image} className="statsItem" onClick={() => this.showInfo(info)} />);
    });

    const allKnownPotions = Object.keys(this.props.state.playerEgo.knowledge.knownPotions);
    const potionsViews = allKnownPotions.map(p => {
      const info = ObjectInfoFactory.get().getInfoStandarized(p);
      const image = info.getImageID();
      return (<img src={image} className="statsItem" onClick={() => this.showInfo(info)} />);
    });
    
    return (
      <div className="statsContentContainer">
        Health: 
        <span>{Math.floor(playerEgo.health)} / {playerEgo.maxHealth}</span>
        <HorizontalBar className='healthBar' fillRatio={healthRatio} height={12}/>
        <br />
        Mana:
        <span>{Math.floor(playerEgo.mana)} / {playerEgo.maxMana}</span>
        <HorizontalBar className='manaBar' fillRatio={manaRatio} height={12} />
        <br />
        <span>Level {playerEgo.expLevel}</span>
        <br />
        <span>Experience: {Math.floor(xp)} / {nextXp} XP</span>
        <HorizontalBar className='expBar' fillRatio={expRatio} height={12} />
        <br />
        {R.strings.scr_stats_spells_header}
        <br />
        {spellsView}
        <br />
        {R.strings.scr_stats_achivements}
        <br />
        <div className="statsSlider">
          {achivView}
        </div>
        <br />
        {R.strings.scr_stats_bestiary}
        <br />
        <div className="statsSlider">
          {creaturesView}
        </div>
        <br />
        {R.strings.scr_stats_collectibles}
        <br />
        <div className="statsSlider">
          {plantsView}
        </div>
        <br />
        {R.strings.scr_stats_recipes}
        <div className="statsSlider">
          {potionsViews}
        </div>
      </div>
    );
  }

  renderFooter() {
    return (
      <div>
        <Button onClick={() => this.back()}>{R.strings.back}</Button>
      </div>
    );
  }
}

