import React, { Component } from 'react';
import R from '../R.js';
import './LoadingScreen.css'

export default class LoadingScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="loading-screen">
        <img className="loading-logo" 
            src={R.drawables.mw_logoicon} 
        />
        <div className="loading-text">
          <span>{R.strings.loading} &#8230;</span>
        </div>
      </div>
    );
  }
}
