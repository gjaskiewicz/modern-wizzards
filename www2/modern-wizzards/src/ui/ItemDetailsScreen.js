import React, { Component } from 'react';
import R from "../R.js";
import CommonBaseScreen from './CommonBaseScreen.js';
import $log from '../utils/log.js';
import Button from './Button.js';
import VoyHr from './VoyHr.js';
import './ItemDetailsScreen.css';

const TAG = "ItemDetailsScreen";

export default class ItemDetailsScreen extends CommonBaseScreen {
  constructor(props) {
    super(props);
  }

  back() {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, activeScreen: (this.props.backTo || 'inventory') } }
    ));
  }

  useItem() {
    if (!this.props.onUse) {
      $log.e(TAG, "[onUse] not configured");
    } else {
      this.props.onUse();
    }
  }

  renderTitle() {
    const item = this.props.item;
    return (<span>{item && item.getName()}</span>);
  }

  renderHeader() {
    return (<span></span>);
  }

  getRecipe(item) {
    if (!item || this.props.onUse) {
      return <div />;
    }
    const aux = R.strings[item.getID() + '_recept'] || null;
    if (!aux) {
      return <div />
    }
    return (<div className="itemDetailsText">
        <VoyHr />
        {aux}
      </div>
    );
  }

  renderContent() {
    const item = this.props.item;
    const auxUi = this.getRecipe(item);
    return (
      <div className="itemDetailsContent">
        <img src={item && item.getImageID()} />
        <VoyHr />
        <div className="itemDetailsText">
          {item && item.getDescription()}
        </div>
        {auxUi}
      </div>
    );
  }

  renderFooter() {
    let onUseBtn;
    const item = this.props.item;
    if (this.props.onUse && item && item.getID() != 'potion_unknown') {
      onUseBtn = (<Button onClick={() => this.useItem()}>{R.strings.use}</Button>);
    } else {
      onUseBtn = (<div className="mwbuttonPlaceholder" />);
    }
    return (
      <div>
        <VoyHr />
        <div className="headerButtonContainer">
          <Button onClick={() => this.back()}>{R.strings.back}</Button>
          &nbsp;
          {onUseBtn}
        </div>
      </div>
    );
  }
}
