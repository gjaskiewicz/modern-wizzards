import React, { Component } from 'react';
import R from "../R.js";
import CommonBaseScreen from './CommonBaseScreen.js';
import $log from '../utils/log.js';
import DebugState from './DebugState.js';
import Button from './Button.js';
import AllQuests from '../quests/AllQuests.js';
import Quest from '../quests/Quest.js';
import QuestsEngine from '../quests/QuestsEngine.js';
import GameContent from '../gamelogic/GameContent.js';
import MWToast from './MWToast.js';
import MapController from '../MapController.js';
import GeoPoint from '../gis/GeoPoint.js';
import Game from '../Game.js';
import CollectableItem from '../gamelogic/world/CollectableItem.js';
import SphericalZone from '../gis/SphericalZone.js';
import ManaZone from '../gamelogic/world/ManaZone.js';
import CollectPlant from '../gamelogic/inventory/CollectPlant.js';
import VERSION from '../version.js';
import './DebugScreen.css';
import GameSaver from '../utils/GameSaver';

const TAG = "DebugScreen";

export default class DebugScreen extends CommonBaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      mode: 'state',
    };
  }

  back() {
    window.gameController.applyToState(s => (
      {...s, ui: {...s.ui, activeScreen: 'map' } }
    ));
  }

  renderTitle() {
    return (<span>&#x1f41e; &#x1f41e; &#x1f41e; Debug &#x1f41e; &#x1f41e; &#x1f41e;</span>);
  }

  renderHeader() {
    return (
      <div>
        <a href='#' onClick={() => this.toggleMode('state')}>State</a>
        &nbsp; | &nbsp;
        <a href='#' onClick={() => this.toggleMode('quests')}>Quests</a>
        &nbsp; | &nbsp;
        <a href='#' onClick={() => this.toggleMode('spawn')}>Spawn</a>
        &nbsp; | &nbsp;
        <a href='#' onClick={() => this.toggleMode('misc')}>Misc</a>
      </div>
    );
  }

  toggleMode(mode) {
    this.setState(s => ({...s, mode}));
  }

  startQuest(quest) {
    window.gameController.applyToState(s => ({
      ...s,
      quests: {
        ...s.quests,
        [quest]: {
          ...Quest.makeDefault(),
          acceptanceState: Quest.AcceptanceState.NONE,
          progressState: Quest.ProgressState.WAITING,
          waitUntil: 0,
        }
      },
    }));
  }

  clearQuests() {
    window.gameController.applyToState(s =>
      QuestsEngine.INSTANCE.reinitalizeQuests({...s, quests:{}})
    );
  }

  disableDevmode() {
    window.isDev = false;
    window.gameController.applyToState(s => ({
      ...s,
      playerEgo: {
        ...s.playerEgo,
        isDev: false,
      }
    }));
  }

  saveGame() {
    window.gameController.applyToState(s => {
      GameSaver.saveGame(s);
      return s;
    });
    this.back();
  }

  spawnCreature(cID) {
    MWToast.toast("SELECT CREATURE SPAWN POSITION");
    let remove = MapController.getInst().addClickHandler(latLng => {
      let creatures = GameContent.getInstance().getAllCreaturesTypes();
      creatures = creatures.filter(c => c.getInfo().getID() == cID);
      window.gameController.applyToState(s => {
        const creature = creatures[0].newInstance();
        creature.setLocation(new GeoPoint(latLng.lat(), latLng.lng()));
        creature.display(true);
        Game.getGame().worldContainer.addQuestObject(creature, false);
        MWToast.toast(`Spawned ${creature.getInfo().getName()}`);
        return s;
      });
      remove();
    });
    this.back();
  }

  spawnPlant(pID) {
    MWToast.toast("SELECT PLANT SPAWN POSITION");
    let remove = MapController.getInst().addClickHandler(latLng => {
      const gp = new GeoPoint(latLng.lat(), latLng.lng());
      window.gameController.applyToState(s => {
        const cIt = new CollectableItem(
          new SphericalZone(gp, 40),
          new CollectPlant(pID)
        );
        cIt.display(true);
        Game.getGame().worldContainer.addQuestObject(cIt, true);
        MWToast.toast(`Spawned ${pID}`);
        return s;
      });
      remove();
    });
    this.back();
  }

  spawnManaZone() {
    MWToast.toast("SELECT Mana Zone SPAWN POSITION");
    let remove = MapController.getInst().addClickHandler(latLng => {
      const gp = new GeoPoint(latLng.lat(), latLng.lng());
      window.gameController.applyToState(s => {
        const mz = new ManaZone(gp, 40);
        mz.display(true);
        Game.getGame().worldContainer.addQuestObject(mz, false);
        MWToast.toast(`Spawned Mana Zone`);
        return s;
      });
      remove();
    });
    this.back();
  }

  spawnPotion(pID) {
    MWToast.toast(`SPAWNING POTION ${pID}`);
    window.gameController.applyToState(s => {
      let inv = s.playerEgo.inventory;
      inv = [...inv, {name: pID}];
      return {
        ...s,
        playerEgo: {
          ...s.playerEgo,
          inventory: inv,
        },
        ui: {
          activeScreen: 'inventory',
        }
      };
    });
  }

  renderContent() {
    if (this.state.mode === 'state') {
      return (
        <DebugState state={this.props.state} />
      );
    } else if (this.state.mode === 'spawn') {
      const cMenu = GameContent.getInstance().getAllCreaturesTypes()
        .map(c => c.getInfo())
        .map(creature => {
          return (<li key={creature.getID()} onClick={() => this.spawnCreature(creature.getID())}>
            <img src={creature.getImageID()} />
            <span>{creature.getName()}</span>
          </li>);
        }
      );
      const pMenu = GameContent.getInstance().getAllPotionsTypes()
        .map(c => c.getInfo())
        .map(potion => {
          return (<li key={potion.getID()} onClick={() => this.spawnPotion(potion.getID())}>
            <img src={potion.getImageID()} />
            <span>{potion.getName()}</span>
          </li>);
        }
      );
      const plMenu = GameContent.getInstance().getAllPlantsTypes()
      .map(c => c.getInfo())
      .map(plant => {
        return (<li key={plant.getID()} onClick={() => this.spawnPlant(plant.getID())}>
          <img src={plant.getImageID()} />
          <span>{plant.getName()}</span>
        </li>);
      }
    );
      return (<div>
        <h2>Creatures</h2>
        <ul className="debugSpawnCreatures">
          {cMenu}
        </ul>
        <h2>Potions</h2>
        <ul className="debugSpawnCreatures">
          {pMenu}
        </ul>
        <h2>Plants</h2>
        <ul className="debugSpawnCreatures">
          {plMenu}
        </ul>
        <h2>Other</h2>
        <ul className="debugSpawnCreatures">
          <li onClick={() => this.spawnManaZone()}>Mana Zone</li>
        </ul>
        </div>)
    } else if (this.state.mode === 'misc') {
      return (<div>
        <p>VERSION {VERSION}</p>
        <p>Location changes: {this.props.state.debug.locationUpdates}</p>
        <p>Dev mode: {this.props.state.playerEgo.isDev ? 'ENABLED' : 'DISABLED'}</p>
        <hr />
        <a href='#' onClick={() => this.disableDevmode()}>Disable dev mode</a>
        <br /><br />
        <a href='#' onClick={() => this.saveGame()}>Force save game</a>
        <br />
      </div>);
    } else if (this.state.mode === 'quests') {
      const questK = Object.keys(AllQuests).map(q => (
        <li className="debugListItem" key={q} onClick={() => this.startQuest(q)}>{q}</li>
      ));
      questK.push(
        <li className="debugListItem" key='bomb' onClick={() => this.clearQuests()}>&#x1f4a3; RESET &#x1f4a3;</li>
      );
      return (
        <ul>{questK}</ul>
      );
    } else {
      return (<span>Unknown mode {this.state.mode}</span>);
    }
  }

  renderFooter() {
    return (
      <div className="headerButtonContainer">
        <Button onClick={() => this.back()}>{R.strings.back}</Button>
        &nbsp;
      </div>
    );
  }
}
