import Prolog from './thirdparty/prolog.js';
import SPELLBOOK from './spellbook.js';
import $log from './utils/log.js';

export default class AllSpells {
  static _inst = new AllSpells();
  static get() { 
    return AllSpells._inst;
  }

  constructor() { }
  
  getEngine() {
    if(!this.engine) {
      this.engine = new Prolog(SPELLBOOK);
    }
    return this.engine;
  }
   
  possiblePotions(/* List<String> */ str) {
    const prologList = `[${(str || []).join(',')}]`;
    const pQuery = `make(${prologList}, X).`;
    $log.d("PROLOG", "query: " + pQuery);

    const queryResult = this.getEngine().runQuery(pQuery);
    const results = queryResult.map(x => x[0]['X']);
    return results.sort().filter((value, index, array) => {
      return (index === 0) || (value !== array[index - 1]);
    });
  }

  suggestAll(str) {
    const prologList = `[${(str || []).join(',')}, A]`;
    const pQuery = `make(${prologList}, X).`;
    $log.d("PROLOG", "query: " + pQuery);  

    const queryResult = this.getEngine().runQuery(pQuery);
    const results = queryResult.map(x => ({item: x[0]['A'], result: x[1]['X']}));
    return results;
  }

  suggestOne(str) {
    const results = this.suggestAll(str);
    return results[0] || null;
  }
}
