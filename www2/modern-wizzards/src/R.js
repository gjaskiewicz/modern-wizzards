import React from 'react';
import './R.css';

import res_ilustration_6 from "./assets/quest/ilustration_6.png";
// generated with: find assets | grep "\." | awk '{ print "import res_"$1" from \"./"$1"\";" }' | sed 's/assets\/[a-z]*\///' | sed 's/\.[a-z][a-z][a-z]//'
import res_achiv_mixtures from "./assets/achivments/achiv_mixtures.png";
import res_achiv_juggle from "./assets/achivments/achiv_juggle.png";
import res_achiv_gamefinished from "./assets/achivments/achiv_gamefinished.png";
import res_achiv_distvariety from "./assets/achivments/achiv_distvariety.png";
import res_achiv_medit from "./assets/achivments/achiv_medit.png";
import res_achiv_disttraveled from "./assets/achivments/achiv_disttraveled.png";
import res_achiv_hunter from "./assets/achivments/achiv_hunter.png";
import res_achiv_plants from "./assets/achivments/achiv_plants.png";
import res_achiv_days from "./assets/achivments/achiv_days.png";
import res_achiv_unknown from "./assets/achivments/achiv_unknown.png";
import res_potion_mana_adv from "./assets/potions/potion_mana_adv.png";
import res_potion_antidote from "./assets/potions/potion_antidote.png";
import res_potion_invisibility from "./assets/potions/potion_invisibility.png";
import res_potion_unknown from "./assets/potions/potion_unknown.png";
import res_potion_stimulation from "./assets/potions/potion_stimulation.png";
import res_potion_luckjelly from "./assets/potions/potion_luckjelly.png";
import res_potion_mana from "./assets/potions/potion_mana.png";
import res_potion_health_adv from "./assets/potions/potion_health_adv.png";
import res_potion_initial from "./assets/potions/potion_initial.jpg";
import res_potion_perception from "./assets/potions/potion_perception.png";
import res_potion_health from "./assets/potions/potion_health.png";
import res_mw_map_lock_off from "./assets/ui/mw_map_lock_off.png";
import res_mw_highlight from "./assets/ui/mw_highlight.png";
import res_mw_map_centrize from "./assets/ui/mw_map_centrize.png";
import res_mw_map_lock_on from "./assets/ui/mw_map_lock_on.png";
import res_mw_logoicon from "./assets/ui/mw_logoicon.png";
import res_mw_fight_bar_exp from "./assets/ui/mw_fight_bar_exp.jpg";
import res_mw_fight_creature_hit from "./assets/ui/mw_fight_creature_hit.png";
import res_mw_fight_bar_bcg from "./assets/ui/mw_fight_bar_bcg.png";
import res_mw_fight_behaviour_bcg2 from "./assets/ui/mw_fight_behaviour_bcg2.png";
import res_mw_fight_behaviour_target from "./assets/ui/mw_fight_behaviour_target.png";
import res_mw_mainmenu_close from "./assets/ui/mw_mainmenu_close.png";
import res_mw_mainmenu_options from "./assets/ui/mw_mainmenu_options.png";
import res_mw_message_alert from "./assets/ui/mw_message_alert.png";
import res_mw_logo from "./assets/ui/mw_logo.png";
import res_mw_icon from "./assets/ui/mw_icon.png";
import res_mw_background from "./assets/ui/mw_background.jpg";
import res_mw_fight_bar_hp from "./assets/ui/mw_fight_bar_hp.png";
import res_mw_fight_bar_mana from "./assets/ui/mw_fight_bar_mana.png";
import res_mw_closed_mail from "./assets/ui/mw_closed_mail.png";
import res_mw_open_mail from "./assets/ui/mw_open_mail.png";
import res_creature_gnome from "./assets/creatures/creature_gnome.jpg";
import res_creature_warewolf from "./assets/creatures/creature_warewolf.jpg";
import res_creature_bunny from "./assets/creatures/creature_bunny.jpg";
import res_creature_bat from "./assets/creatures/creature_bat.jpg";
import res_creature_black_cat from "./assets/creatures/creature_black_cat.jpg";
import res_creature_strumpf from "./assets/creatures/creature_strumpf.jpg";
import res_creature_wraith from "./assets/creatures/creature_wraith.jpg";
import res_creature_golem from "./assets/creatures/creature_golem.jpg";
import res_item_cornerstone from "./assets/items/item_cornerstone.jpg";
import res_item_daisyflower from "./assets/items/item_daisyflower.jpg";
import res_item_cateye from "./assets/items/item_cateye.jpg";
import res_item_rozkovnik from "./assets/items/item_rozkovnik.jpg";
import res_item_swallowweed from "./assets/items/item_swallowweed.jpg";
import res_item_szczeciogon from "./assets/items/item_szczeciogon.jpg";
import res_item_keypart from "./assets/items/item_keypart.jpg";
import res_item_smurfblood from "./assets/items/item_smurfblood.jpg";
import res_item_4clover from "./assets/items/item_4clover.jpg";
import res_item_emptyglass from "./assets/items/item_emptyglass.png";
import res_item_ectoplasma from "./assets/items/item_ectoplasma.jpg";
import res_item_bunnyleg from "./assets/items/item_bunnyleg.jpg";
import res_item_garlic from "./assets/items/item_garlic.jpg";
import res_item_mandragora from "./assets/items/item_mandragora.jpg";
import res_item_fernflower from "./assets/items/item_fernflower.png";
import res_spell_oob from "./assets/spells/spell_oob.png";
import res_spell_astralshield from "./assets/spells/spell_astralshield.png";
import res_spell_fireball from "./assets/spells/spell_fireball.png";
import res_spell_electrokinesis from "./assets/spells/spell_electrokinesis.png";
import res_spell_telekinesis from "./assets/spells/spell_telekinesis.png";
import res_spell_physical from "./assets/spells/spell_physical.png";

export default class R {
  static drawables = {
    ilustration_6: res_ilustration_6,
    // generated with: find assets | grep "\." | awk '{ print "    "$1": res_"$1"," }' | sed 's/assets\/[a-z]*\///g' | sed 's/\.[a-z][a-z][a-z]//g'
    achiv_mixtures: res_achiv_mixtures,
    achiv_juggle: res_achiv_juggle,
    achiv_gamefinished: res_achiv_gamefinished,
    achiv_distvariety: res_achiv_distvariety,
    achiv_medit: res_achiv_medit,
    achiv_disttraveled: res_achiv_disttraveled,
    achiv_hunter: res_achiv_hunter,
    achiv_plants: res_achiv_plants,
    achiv_days: res_achiv_days,
    achiv_unknown: res_achiv_unknown,
    potion_mana_adv: res_potion_mana_adv,
    potion_antidote: res_potion_antidote,
    potion_invisibility: res_potion_invisibility,
    potion_unknown: res_potion_unknown,
    potion_stimulation: res_potion_stimulation,
    potion_luckjelly: res_potion_luckjelly,
    potion_mana: res_potion_mana,
    potion_health_adv: res_potion_health_adv,
    potion_initial: res_potion_initial,
    potion_perception: res_potion_perception,
    potion_health: res_potion_health,
    mw_map_lock_off: res_mw_map_lock_off,
    mw_highlight: res_mw_highlight,
    mw_map_centrize: res_mw_map_centrize,
    mw_map_lock_on: res_mw_map_lock_on,
    mw_logoicon: res_mw_logoicon,
    mw_fight_bar_exp: res_mw_fight_bar_exp,
    mw_fight_creature_hit: res_mw_fight_creature_hit,
    mw_fight_bar_bcg: res_mw_fight_bar_bcg,
    mw_fight_behaviour_bcg2: res_mw_fight_behaviour_bcg2,
    mw_fight_behaviour_target: res_mw_fight_behaviour_target,
    mw_mainmenu_close: res_mw_mainmenu_close,
    mw_mainmenu_options: res_mw_mainmenu_options,
    mw_message_alert: res_mw_message_alert,
    mw_logo: res_mw_logo,
    mw_icon: res_mw_icon,
    mw_background: res_mw_background,
    mw_fight_bar_hp: res_mw_fight_bar_hp,
    mw_fight_bar_mana: res_mw_fight_bar_mana,
    mw_closed_mail: res_mw_closed_mail,
    mw_open_mail: res_mw_open_mail,
    creature_gnome: res_creature_gnome,
    creature_warewolf: res_creature_warewolf,
    creature_bunny: res_creature_bunny,
    creature_bat: res_creature_bat,
    creature_black_cat: res_creature_black_cat,
    creature_strumpf: res_creature_strumpf,
    creature_wraith: res_creature_wraith,
    creature_golem: res_creature_golem,
    item_cornerstone: res_item_cornerstone,
    item_daisyflower: res_item_daisyflower,
    item_cateye: res_item_cateye,
    item_rozkovnik: res_item_rozkovnik,
    item_swallowweed: res_item_swallowweed,
    item_szczeciogon: res_item_szczeciogon,
    item_keypart: res_item_keypart,
    item_smurfblood: res_item_smurfblood,
    item_4clover: res_item_4clover,
    item_emptyglass: res_item_emptyglass,
    item_ectoplasma: res_item_ectoplasma,
    item_bunnyleg: res_item_bunnyleg,
    item_garlic: res_item_garlic,
    item_mandragora: res_item_mandragora,
    item_fernflower: res_item_fernflower,
    spell_oob: res_spell_oob,
    spell_astralshield: res_spell_astralshield,
    spell_fireball: res_spell_fireball,
    spell_electrokinesis: res_spell_electrokinesis,
    spell_telekinesis: res_spell_telekinesis,
    spell_physical: res_spell_physical,
  }

  static strings = {
    hello: 'Hello World, ModernWizzardsActivity!',
    app_name: 'Modern Wizards',
    undef: '!undefined!',
    
    game_global_ok: 'OK',
    game_global_yes: 'Yes',
    game_global_no: 'No',
    game_reallyquit: 'Quit the game? The last quest progress will be lost!',
    game_gps_waiting_for: 'Locating... make sure you are in open space! ', 
    game_gps_not_enabled: 'GPS IS NOT ENABLED! Turn it on and restart the game!', 
    game_gps_loc_set: 'LOCATION SET, GAME STARTS!',
    game_gps_loc_wait: 'WAIT FOR LOCATION TO BE SET!',
    game_gps_loc_uncalculated: 'Location is beeing calculated',
    game_gps_loc_calculated: 'Location is set',
        
    game_map_notargets: 'There are no targets',
    
    help_uri: 'http://www.modernwizards.net/help',
    feedback_uri: 'https://docs.google.com/forms/d/1RUchX95MZ_fU_jeE0F1fpZin8yRVOZjD8LQ3PUYbbyA',
    intro_youtube_id: 'PoblHYFUkjQ',

    spells_repeled: 'SPELL DEFLECTED!',
    spells_miss: 'SPELL MISSES!',
    spells_nomana: 'YOU DON\'T HAVE ENOUGH MANA!',
    spells_critical: 'CRITICAL HIT!',
    
    disclaimer_title: 'WARNING',
    disclaimer_text: (
      <div>
        Modern Wizards is a location-based game. All objects and destinations are determined in a pseudo-random way, which means that they can appear in inaccessible, dangerous, banned or private places.
        <br />
        <br />
        Please remember that the game does never force the player to physically get to the designated location - instead, you can use special features in the game, like spells.
        <br/>
        <br/>The game creators do not take responsibility for player actions.
        <br/>
        <br/>
        To start the game, please read and accept <a target="_blank" href="/tos.html">Terms Of Service</a>
      </div>),   
    disclaimer_accept: 'I accept',       
    disclaimer_reject: 'I decline',    
        
    player_resurected: 'Your supernatural powers are back!',
    player_mana_charging: 'Pulling mana from source...',
    player_mana_full: 'Your mana level is  full',    
    player_levelup: 'Your have reached the next level!',    
    
    spell_telekinesis_object_too_far: 'This is too far!',      
        
    creature_bunny: 'Test Bunny',
    creature_bunny_desc: 'This once charming and cute animal has gone through a metamorphosis and has changed into a bloodlust beast. Mighty fangs and claws combined with extraordinary speed make him a dangerous predator. It is fearsome to think of the experiments that made this mutation.',
    creature_black_cat: 'Black Cat',
    creature_black_cat_desc: 'For hundreds of years black cats were associated with misfortune that happened to passers-by. And that’s in fact true. These creatures have the power to feed on their victim’s positive energy, so called ‘karma’. Besides, cats own exceptionally sharp claws and their speed and agility is unmatched by other species.',
    creature_bat: 'Vampire Bat',
    creature_bat_desc: 'A small flying bogie, that feeds on other creatures’ astral energy. Their  biggest tidbit are vulnerable strumpfs, but a ravenous energetic bat doesn’t hesitate to attack bigger victims. They are exceptionally weak physically, relatively slow and easy to beat.',
    creature_gnome: 'Gnome',
    creature_gnome_desc: 'Mean nature and passion for riddles are the main features of this dwarfish race. In tales and fables One may find many mentions of deeds of iniquity and shamelessness committed by gnomes: peeing into beer or stealing toilet paper is said to be one of their lightest offenses. In combat the use haft weapons and simple, but nasty, pyrotechnic abilities. They are rather slow.',
    creature_warewolf: 'Werewolf',
    creature_warewolf_desc: 'A werewolf is a human being changed by supernatural powers. Lycanthropy gives him extraordinary speed and agility, but also deforms the body and makes him unduly hairy. The person is normally deprived of free will periodically and follows his animal instincts. Coming across a werewolf one must have in mind that lycanthropy is contagious by its saliva. \n\n Treating lycanthropy is a laborious and burdensome process, giving way only to exorcisms in terms of level of difficulty. A medic that undertakes a treatment must not only have a broad knowledge in the area of alchemy, herbalism and magic, but also huge willpower and bravery.',
    creature_wraith: 'Wraith',
    creature_wraith_desc: 'A immaterial creature trapped between the world of the living and the dead. A contradictory to nature state of not living breeds anger and hatred that is directed against human beings by haunting them. With time, however, simple hounding becomes insufficient for the phantom and it becomes really dangerous. ',    
    creature_strumpf: 'Strumpf',
    creature_strumpf_desc: 'There is very little known about this timorous, little blue pixies. According to legends, strumpfs live in hidden villages, where they perform filthy magic and brew mephitic infusions. Many mages hunt these creatures to obtain the strumpfatic essence, an important ingredient of various potions. Small and nimble, strumpfs can hide almost everywhere. Moreover, they are extremely lucky, which may rebound upon their aggressor.',   
    creature_golem: 'Golem',
    creature_golem_desc: 'Alchemists studied for centuries the mysteries of the energy propelling the eternal spiral of life and death. One of the milestones in these studies was to ignite the divine spark in inanimate material. That’s how a golem was created: a servant and guardian deprived of will. Extremely tough guardian',  
    
    item_daisyflower: 'Chamomile',
    item_daisyflower_desc: 'Chamomiles, and also daisies, are known for their properties of defining the feelings of the second party. Thus, they can increase the perception of future events.',
    item_fernflower: 'Fern flower',
    item_fernflower_desc: 'Once a year the fern flower radiates with such powerful aura, that even an untrained eye can see it. Finding the flower outside this period requires, on the other hand, strong astral perception. \n It is known for its enormous influence on health, and it is eve said that it can even bring the dead back to life.',
    item_4clover: 'Four-leaf clover',
    item_4clover_desc: 'The fourth leaf of a clover tells of her magic properties. A clover especially brings luck.',
    item_rozkovnik: 'Rozkovnik',
    item_rozkovnik_desc: 'According to tradition, rozkovnik has the magical power to unlock or discover something locked or hidden.',
    item_garlic: 'Allium',
    item_garlic_desc: 'Allium, commonly known as garlic, was seemingly grown by Nosferatu itself to laugh at naive fools that tried to drive away vampires with it. Our grannies found an application for it though, discovering its healing and spicy properties.',
    item_swallowweed: 'Swallow Weed',
    item_swallowweed_desc: 'Swallow weed, which exudes yellow juice from the stem after picking, plays an important role in healing and common magic. Everyone having this plant by his side gains the power to ease conflicts.',
    item_cornerstone: 'Corner Stone',
    item_cornerstone_desc: 'Corner stones are used for centuries for their special power: they make objects that they consist of more durable.',
    item_szczeciogon: 'Chaiturus',
    item_szczeciogon_desc: 'Chaiturus (Latin), or traditionally Bristletail. A curative plant, whose ear of grain resembles a lion tail. As an brew ingredient, it has a strong antitoxic effect.',    
    item_mandragora: 'Mandragora',
    item_mandragora_desc: 'A known magic plant, whose root often takes a human silhouette form. Mandrakes are given to pregnant women, as though it ensures the proper growth of the infant.',    
    item_cateye: 'Cat Mint',
    item_cateye_desc: 'Cat mint has the unusual ability to allure and weaken the will. It is extremely narcotic for cats, who probably experience hallucinations. After distillation it may have effect on other creatures too.',    
    item_ectoplasma: 'Ectoplasma',
    item_ectoplasma_desc: 'A transparent mucus left by ghosts and phantoms in places of increased paranormal activity.',    
    item_smurfblood: 'Stresence',
    item_smurfblood_desc: 'A filigree bead with undefined form that emanates with blue light from the inside.',    
    item_bunnyleg: 'Bunny leg',
    item_bunnyleg_desc: 'The bunny leg is often carried as a necklace amulet, but if dried, it may serve as a potion ingredient.',    
    
    item_keypart: 'Runic key',
    item_keypart_desc: 'Runic key received from gnome.\nIt supposed to be used to open sealed gate.',

    potion_create: 'Create this potion?',
    
    potion_health: 'Healing potion',
    potion_health_desc: 'Herby potion that brings instant effect to your body, healing the wound made by supernatural forces',
    potion_health_recept: 'Mix and boil any paranormal plants with healing attributes, that is: Chamomile, Allium or Fern flowers',
    potion_mana: 'Mana potion',
    potion_mana_desc: 'Highly energetizing drink that quickly restores Your mana level.',
    potion_mana_recept: 'Mix powdered Stresence, Clover and Swallowweed. Shake it, do not twist.',
    potion_antidote: 'Antidote',
    potion_antidote_desc: 'Heals you from infection caused by supernatural creatures, especially warewolfes and vampires. ',
    potion_antidote_recept: 'First, take one part of fresh Allium, and mix it with Fern Flower and Swallowweed',    
    potion_initial: 'Specific',
    potion_initial_desc: 'Strange, green and gloomy liquid in laboratory glass. Herbal smell kicks the nose, but it does not feel dangerous. ',
    potion_unknown: 'Unknown mixture',
    potion_unknown_desc: 'Unknown mixture', 
    potion_invisibility: 'Invisualiser',
    potion_invisibility_desc: 'Drinking this will make your body emit an aura, rendering you and your clothes invisible for all the creatures, normal and paranormal. Actually it does not only hide your visuality, but also all the smells. Use it to pass dangerous regions and to avoid attacks.',
    potion_invisibility_recept: 'Boil a Fern Flower with Roskovnik roots for two minutes, until the liquid gets transparent. Add the ectoplasm when it cools down.',    
    potion_perception: 'Perception stimulant',
    potion_perception_desc: 'Sharpens your sences, making it easier to spot things and creatures, also the hidden ones, from a short or long distance. ',    
    potion_perception_recept: 'Mix Rozkovnik, Swallowweed and Catmint parts in cold water taken near mana source.',     
    potion_stimulation: 'Strength stimulant',
    potion_stimulation_desc: 'Makes you abnormal powerful and able to both perform stronger attacks, and withstand hits. ',        
    potion_stimulation_recept: 'Carefully add parts of Chaiturus to Mandragora decoction. When color changes to green, add a Cornerstone, which should dissolve. When it does, the potion is ready.',     
    potion_luckjelly: 'Luckylizator',
    potion_luckjelly_desc: 'The combined attributes of the potion\'s ingredients brings particular "karma" effects on the drinker. The nature of this phenomenon is unknown.',        
    potion_luckjelly_recept: 'To create this potion you need one dried leg of creature with paranormal activities and four leafs of the same clover. Add those to esence of Chamomile decoction, and wait until it soidifies.',        
    
    achiv_hunter: 'Hunter',
    achiv_hunter_desc: 'Awarded when representatives of every species were killed',
    achiv_plants: 'Collector',
    achiv_plants_desc: 'Awarded when every collectible types were found',
    achiv_gamefinished: 'Devoted',
    achiv_gamefinished_desc: 'Awarded when all quests are passed',
    achiv_disttraveled: 'Walker',
    achiv_disttraveled_desc: 'Awarded after traveling 20km as Modern Wizard',
    achiv_distvariety: 'Voyager',
    achiv_distvariety_desc: 'Awarded after showing up in a place 500km away',
    achiv_mixtures: 'Alchemic',
    achiv_mixtures_desc: 'Awarded after successful creation of every mixture.',
    achiv_days: 'Honest',
    achiv_days_desc: 'Awarded for starting the game for 7 days in a row',
    achiv_medit: 'Zen Master',
    achiv_medit_desc: 'Awarded for withstanding 15 minutes in Mana Zone',
    achiv_juggle: 'Juggler',
    achiv_juggle_desc: 'Awarded for performing 3 telekinetics at once',
    achivment_unlocked: 'Achievement unlocked!',
    
    spell_oob: 'Bilocation',
    spell_oob_desc: 'Allows you to move in transcendent reality',
    spell_telekinesis: 'Telekinesis',
    spell_telekinesis_desc: 'Allows you to bring desired object from a distance.',
    spell_telekinesis_pulling: 'Pulling object...',
    spell_telekinesis_using: 'Using Telekinesis - click an item',
    spell_physical: 'Power punch',
    spell_physical_desc: 'Punch the opponent!',
    spell_physical_bite: 'bite',
    spell_physical_scratch: 'scratch',
    spell_physical_spear: 'spear thrust',
    spell_fireball: 'Pyrokinesis',
    spell_fireball_desc: 'Send a firing ball of pure magic straight into opponent\'s face',
    spell_battlespeed: 'Super Speed',
    spell_battlespeed_desc: 'nevermind, creatures',
    spell_infection: 'Infection',
    spell_infection_desc: 'nevermind, creatures',
    spell_astralshield: 'Lygokinesis',
    spell_astralshield_desc: 'Protects you from most of the attacks for few seconds.',
    spell_electrokinesis: 'Electrokinesis',
    spell_electrokinesis_desc: 'Small thunderbolt electrifies the area, causing damage.',
    spell_badluck: 'bad luck',
    spell_badluck_desc: 'nevermind, creatures',       

    // String for single item in messages list view
    back: 'Back',
    use: 'Use',
    alchemy: 'Alchemy',
    accept: 'Accept',
    reject: 'Reject',
    later: 'Maybe later',
    from: 'FROM',
    target: 'Cel',
    usageBlocked: 'Użycie zablokowane',

    // Strings for quests
    completed: 'completed',
    inprogress: 'in progress',
    failed: 'failed',
    menu_settings: 'Settings',
    text_view_id: 'ID:',
    text_view_count: '. Count: ',
    yes_button: 'Yes',
    no_button: 'No',
    creature_cast_fc: 'Creature uses a ',
    health_fc: 'Health: ',
    mana_fc: 'Mana: ',
    escape_fc: 'Escape?',
    escape_text_fc: 'Are you sure you want to run away from the fight?',
    run_button: 'RUN!',
    stay_button: 'Stay',
    accept_quest_button: 'Accept quest.',
    refuse_quest_button: 'Refuse quest',
    no_spell_sc: 'No spells in this category',
    cast_spell_sc: 'Cast this spell?',
    unknown: 'Unknown',
    cast_sc: 'Cast',
    cant_cast_sc: 'Cannot cast this spell',
    player: 'Player',
    click_where_wish_go: 'Click where you wish to go.',
    no_mana: 'No mana!',
    no_mana_text: 'You dont have enough mana! Restore it in Mana Source!',
    my_curr_location: 'My current location is: ',
    no_last_location: 'Last location uavailable.',
    oob_out_of_mana: 'Out of Mana!',
    oob_finished: 'You have returned to Your body',
    alchemy_not_available: 'Alchemy not yet available.',
    resuraction: 'Resurrection',
    
    first_player_death: (
      <div>
        <img src={res_ilustration_6} width="100%"  />
        The last strike makes you fall to your knees and you almost become unconscious. Together with the strength, you also lose all the skills you had developed. The world seems ordinary again, just like it did before you drunk the firts mixture from Bayery. The only thing you feel now is the acute need to go back to the Mana Source.
      </div>
    ),
     
    want_to_take: 'Do you want to take this?',
    plant: 'Plant',
    mana_zone: 'Mana Source',
    trigger: 'Trigger',
    
    // Screen Specyfic strings
    opt_title: 'Options',
    opt_use_gps: 'Use GPS',
    opt_use_mock: 'Use mock location!',
    opt_language: 'Language',
    opt_codes: 'Code:',
    opt_gener_obj_box: 'Unlock all',
    opt_use_vibrations: 'vibrations',
    opt_use_sounds: 'sounds',
    opt_savenload: 'Save And Load',
    opt_devmode: 'Development mode',
    opt_deletesaves: 'Delete saves',
    opt_loadsave: 'Load save from file',
    opt_save: 'Save',
    opt_save_but: 'Save',
    
    mainmenu_map: 'MAP',
    mainmenu_quests: 'QUESTS',
    mainmenu_spells: 'SPELLS',
    mainmenu_inventory: 'INVENTORY',
    mainmenu_stat: 'STATISTICS',
    mainmenu_quit: 'QUIT',
    mainmenu_intro: 'WATCH INTRO',

    scr_bestiary_title: 'BESTIARY', 
    scr_alch_title: 'ALCHEMY',
    scr_alch_ing: 'INGREDIENTS:',
    scr_alch_mixpot: 'MIXING POT:',
    scr_alch_results: 'RESULTS:',
    scr_alch_clear: 'CLEAR',
    scr_fight_run: 'RUN',
    scr_fight_potions: 'POTIONS',
    scr_stats_title: 'STATISTICS',
    scr_stats_achivements: 'ACHIEVEMENTS',
    scr_stats_bestiary: 'BESTIARY',
    scr_stats_collectibles: 'COLLECTIBLES',
    scr_stats_recipes: 'RECIPES',
    scr_stats_spells_header: 'You are under effect of:\n',
    scr_stats_spells_none: '(none)\n',
    scr_stats_resurect: 'You have lost all your powers!\n Go back to the nearest Mana Zone to regenerate',
    scr_quests_title: 'QUESTS',
    scr_collectibles_title: 'Collectibles',
    scr_map_title: 'WORLD VIEW',
    scr_map_filters: 'FILTERS',
    scr_map_menu: 'MENU',
    scr_map_terrain: 'TERRAIN',  
    scr_map_controls: 'CONTROLS',
    scr_inv_title: 'INVENTORY',
    scr_inv_btn_items: 'ITEMS',
    scr_inv_btn_potions: 'POTIONS',      
    scr_inv_btn_alchemy: 'ALCHEMY',    
    scr_spells_title: 'SPELLS',
    scr_spells_global: 'GLOBAL',        
    scr_spells_battle: 'BATTLE', 
    scr_spells_empty_list: "No spells in this category",
    
    alert_message_title: "INCOMING MESSAGE",
    alert_message_content: (
      <div>
        <img src={R.drawables.mw_message_alert} className="messageAlertImg" />
        <span>
          You have received a new message
        </span>
      </div>
    ),      
    alert_message_show: 'SHOW',   
    alert_message_later: 'LATER',   
    alert_item_title: 'YOU HAVE FOUND',      
    alert_item_question: 'What do you do?',   
    alert_item_take: 'TAKE',   
    alert_item_leave: 'LEAVE',   
    alert_creature_title: 'YOU HAVE SPOTTED',      
    alert_creature_question: 'What do you do?',   
    alert_creature_fight: 'FIGHT',   
    alert_creature_run: 'RUN!',   
    
    fight_: 'QUIT',
    fight_lostalert_title: 'You have lost!',
    fight_lostalert_desc: 'Your astral health is gone. Go to nearest Mana Zone to recover!',
    fight_lostalert_ok: 'I will.',
    fight_winalert_title: 'You have won!',
    fight_winalert_desc: 'You have won the fight!',
    fight_winalert_ok: 'Great!',
    fight_instructions: 'You are in a fight. Tap an action button when the indicatior is in the middle!', 

    list_title_receptures: 'Receptures',
     
    move_the_camera: 'Move the camera', 

    mapviewtype_feelings: 'Feelings view',
    mapviewtype_satelite: 'Real view',
    mapviewtype_normal: 'Symbolic view',
    mapviewtype_terrain: 'Terrain view',
    mapviewtype_none: 'Objects only',

    msg_saved: "Game saved",
    loading: "Loading",

    cookie_accept: 'Got it',
    cookie_text: 'Modern wizzards webpage is using cookies to do its magic',
  }
}
