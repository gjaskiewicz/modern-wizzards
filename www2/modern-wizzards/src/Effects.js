import React, { Component } from 'react';
import './Effects.css';

let ctr = 0;

export default class EffectsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {effects: []};

    if (window.effects) {
      console.error("Effects already initiated!");
    } else {
      window.effects = {
        addEffect: (clazz, time) => {
          let id = ctr++;
          this.setState(s => ({
            ...s,
            effects: [...s.effects, {id, clazz}]
          }));
          setTimeout(() => this.setState(s => ({ 
            ...s,
            effects: (s.effects.filter(c => c.id != id))
          })), time);
        }
      }
    }
  }

  render() {
    const containers = this.state.effects.map(e => {
      const clases = `overlay ${e.clazz}`;
      return <div className={clases} />
    });
    return (<div>{containers}</div>);
  }
}