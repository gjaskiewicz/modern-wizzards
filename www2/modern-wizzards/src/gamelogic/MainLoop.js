import $log from '../utils/log.js';
import MWToast from '../ui/MWToast.js';
import R from './../R.js';
import Game from '../Game.js';
import GameState from '../GameState.js';
import PlayerEgo from './../PlayerEgo.js';
import Creature from './Creature.js';
import MWTime from '../utils/MWTime.js';
import {ITERATIONS_PER_SECOND} from '../Constants.js';
import EventSystem from './events/EventSystem.js';
import EventTypes from './events/EventTypes.js';
import {LogManaAdder} from './regen/ManaAdder.js';
import {LinearHpAdder} from './regen/HealthAdder.js';
import GameSaver from '../utils/GameSaver.js';

const TAG = "MainLoop";

export default class MainLoop {

  constructor(gc) {
    this.loopIteration = 0;
    this.gameController = gc;
    this.lastUpdateTime = MWTime.s();
    this.manaAdder = new LogManaAdder();
    this.hpAdder = new LinearHpAdder();
  }

  startGameLoop() {
    $log.d(TAG, "Starting game loop"); 
    if (this.tracker) {
      $log.e(TAG, "Tracker already set");
      return;
    }
    this.tracker = 
      setInterval(() => this.onTick(), 1000 / ITERATIONS_PER_SECOND);
  }

  stopGameLoop() {
    if (!this.tracker) {
      $log.e(TAG, "No timer tracker");
      return;
    }
    clearInterval(this.tracker);
  }

  onTick() {
    this.gameController.applyToState(s => this.iterateGameState(s));
  }

  /**
   * Iteruje stan gry. Wywoływane ITERATIONS_PER_SECONDx na sekunde.
   */
  iterateGameState(state) {
    this.loopIteration++;
    // $log.d(TAG, `Iterating game loop at iteration ${this.loopIteration}`); 
    
    if(!state.playerEgo.setLocation && this.loopIteration % (ITERATIONS_PER_SECOND * 10) == 0) 	{
      MWToast.toast(R.strings.game_gps_loc_wait);
    }
	
    if (state.gameState == GameState.STATE_WALKING
        || state.gameState == GameState.STATE_PLAYERDEAD) {
			
      //animacje
/*
      for(PhysicalObject po : animations) {
        po.display(false);
        po.display(true);
      }

      if(animations.lenght) {
        MWMap2Activity.refreshMapView();
      }
*/
      //standardowe akcje, interakcje itp
      if (this.loopIteration % ITERATIONS_PER_SECOND == 0) {
        state = Game.getGame().checkInteractions(state);
        Game.getGame().refreshMap(state);
/*
        //Game.getGame().playerEgo.repaint();
        MWMap2Activity.refreshMapView();
        if (Game.getGlobalTopmostActivity() instanceof StatsActivity) {
          ((StatsActivity) Game.getGlobalTopmostActivity()).refresh();
        }
*/
      }
    }
    if (state.gameState == GameState.STATE_WALKING
        || state.gameState == GameState.STATE_FIGHTING) {

      for (let spl of state.playerEgo.underEffectOfSpell) {
        state = spl.takeEffect(state);
      }

      state = this.addHpAndMana(state);
      state = {
        ...state,
        playerEgo: PlayerEgo.removeFinishedSpells(state.playerEgo)
      };
    }

    if (state.gameState == GameState.STATE_FIGHTING) {
      state = state.ui.fight.iterateFight(state);
    }

    if (state.gameState == GameState.STATE_PLAYERDEAD) {
      // tylko checkInteraction w poszukiwaniu Mana Zone.
    }
    //$log.d(TAG, "Iterating game loop END"); 

    state = EventSystem.INSTANCE.notifyListenersState(
      state, null, EventTypes.TICK);

    if (state.options.AUTOSAVE) {
      if ((GameSaver.lastSaveTimeSec + state.options.AUTOSAVE) < MWTime.s()) {
        GameSaver.saveGame(state);
      }
    }
    return state;
  }

  /**
   * dodaje graczowi mana w logarytmiczny sposob, co iteracje. 
   * 
   * Czym wiecej mana, tym wolniej. 
   */
  addHpAndMana(state) {
    const t = MWTime.s();
    const dt = Math.max(t - this.lastUpdateTime, 0);
    this.lastUpdateTime = t;
    // TODO: test
    if (PlayerEgo.isUnderEffectOfSpell(state.playerEgo, "spell_oob")) {
      return state;
    }

    const manaToAdd = this.manaAdder.manaToAdd(
      state.playerEgo.mana, 
      dt, 
      state.playerEgo.manaRegenFactor);

    const hpToAdd = this.hpAdder.hpToAdd(state.playerEgo.health, dt);

    if (state.gameState == GameState.STATE_WALKING) {
      state = PlayerEgo.addManaPoints(state, manaToAdd, false);
      state.playerEgo = PlayerEgo.addHealthPoints(state.playerEgo, hpToAdd, false);
    }
    state.debug['mana_ingameGain'] = (state.debug['mana_ingameGain'] || 0) + manaToAdd;
    return state;
  }
}
