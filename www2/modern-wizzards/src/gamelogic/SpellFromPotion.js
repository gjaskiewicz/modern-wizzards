import Castable from './Castable.js';
import Spell from './Spell.js';

export default class SpellFromPotion extends Spell {
  constructor(owner) {
    super(owner);
    this.usageType = Castable.UsageType.ANYTIME;
    this.targetType = Castable.EffectTargetType.SELF;
    this.requiredMana = 0;
    this.ingredients = [ ];
    this.mixtureColor = 0x0000FF;
  }
/* TODO: migrate 
  public SpellFromPotion()  {
    this(Player.getInst());
  }
*/
  
  getColor() {
    return this.mixtureColor;
  }
  
  useThis()  { throw "Abstract magic method - not implemented"; }

  canMakeFromIng(ing1, ing2, ing3) {
    return (this.ingredients.includes(ing1) && 
        this.ingredients.includes(ing2) &&
        this.ingredients.includes(ing3));
  }
}
