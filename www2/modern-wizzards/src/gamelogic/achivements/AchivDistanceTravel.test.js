import AchivDistanceTravel from './AchivDistanceTravel.js';
import TestData from '../../test/TestData.js';
import Location from '../../gis/Location.js';

describe('AchivDistanceTravel', () => {

  it('should be attainable', () => {
    let state = {
      achivments: { 
        granted: [ ],
      }
    };
    const achiv = new AchivDistanceTravel();
    let loc = new Location(...TestData.cities.radom);
    const step = (TestData.cities.radom[0] - TestData.cities.warsaw[0]) / 10000;
    for (let i = 0; i < 2225; i++) {
      loc = new Location("test", TestData.cities.radom[0] + i * step, TestData.cities.radom[1]);
      state = achiv.addMetersTraveled(state, loc);
      if (i < 1500) {
        expect(achiv.isAchived(state)).toBe(false);
      }
    }
    expect(achiv.isAchived(state)).toBe(true);
  });

  it('should be attainable on reset', () => {
    let state = {
      achivments: { 
        granted: [ ],
      }
    };
    let achiv = new AchivDistanceTravel();
    let loc = new Location(...TestData.cities.radom);
    const step = (TestData.cities.radom[0] - TestData.cities.warsaw[0]) / 10000;
    for (let i = 0; i < 2250; i++) {
      loc = new Location("test", TestData.cities.radom[0] + i * step, TestData.cities.radom[1]);
      state = achiv.addMetersTraveled(state, loc);
      if (i < 1500) {
        expect(achiv.isAchived(state)).toBe(false);
      }
      if (i % 200 == 0) {
        achiv = new AchivDistanceTravel();
      }
    }
    expect(achiv.isAchived(state)).toBe(true);
  });
});