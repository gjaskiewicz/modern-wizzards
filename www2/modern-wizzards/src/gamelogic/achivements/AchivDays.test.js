import AchivDays from './AchivDays.js';

describe('AchivDays', () => {

  it('should be attainable on full day', () => {
    let state = {
      achivments: { 
        granted: [ ],
      }
    };
    let time = 1504288454000;
    const achiv = new AchivDays();
    achiv.getTime = () => {
      time += 24 * 60 * 60 * 1000;
      return time;
    };
    for (let i = 0; i < 7; i++) {
      expect(achiv.isAchived(state)).toBe(false);
      state = achiv.nextRun(state);
    }
    expect(achiv.isAchived(state)).toBe(true);
  });

  it('should be attainable on 1/2 day', () => {
    let state = {
      achivments: { 
        granted: [ ],
      }
    };
    let time = 1504288454000;
    const achiv = new AchivDays();
    achiv.getTime = () => {
      time += 12 * 60 * 60 * 1000;
      return time;
    };
    for (let i = 0; i < 13; i++) {
      expect(achiv.isAchived(state)).toBe(false);
      state = achiv.nextRun(state);
    }
    expect(achiv.isAchived(state)).toBe(true);
  });

});