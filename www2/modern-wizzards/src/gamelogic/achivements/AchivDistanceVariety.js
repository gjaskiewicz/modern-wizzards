import Achivement from "./Achivement.js";
import GeoPoint from "../../gis/GeoPoint.js";
import Units from "../../gis/Units.js";

export default class AchivDistanceVariety extends Achivement {
	
	constructor() {
		super("achiv_distvariety");
	}
	
	addMetersTraveled(state, /* Location */ l) {
		const locationGP = new GeoPoint(
				Math.floor(l.getLatitude() * 1E6), Math.floor(l.getLongitude() * 1E6));
		let variety = {...state.achivments.variety};
		let origVariety = {...state.achivments.variety};

		if(!variety.minLat) variety.minLat = locationGP.getLatitudeE6();
		if(!variety.maxLat) variety.maxLat = locationGP.getLatitudeE6();
		if(!variety.minLng) variety.minLng = locationGP.getLongitudeE6();
		if(!variety.maxLng) variety.maxLng = locationGP.getLongitudeE6();
		
		const minLat = Math.min(variety.minLat, variety.maxLat, locationGP.getLatitudeE6());
		const maxLat = Math.max(variety.minLat, variety.maxLat, locationGP.getLatitudeE6());
		const minLng = Math.min(variety.minLng, variety.maxLng, locationGP.getLongitudeE6());
		const maxLng = Math.max(variety.minLng, variety.maxLng, locationGP.getLongitudeE6());

		const changed = (
			origVariety.minLat != minLat || 
			origVariety.maxLat != maxLat || 
			origVariety.minLng != minLng || 
			origVariety.maxLng != maxLng
		);

		if(changed)	{
			const newState = {
				...state,
				achivments: {
					...state.achivments,
					variety: { minLat, minLng, maxLat, maxLng },
				}
			}

			const meterDelta = Units.optimizedDistance(
				new GeoPoint(minLat, minLng), new GeoPoint(maxLat, maxLng));
							
			if(meterDelta > 500000) {
				return this.achive(newState);
			} else {
				return newState;
			}
		}
		return state;
	}
}