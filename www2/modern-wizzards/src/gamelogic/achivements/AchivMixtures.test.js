import AchivMixtures from './AchivMixtures.js';
import TestData from '../../test/TestData.js';
import Location from '../../gis/Location.js';

describe('AchivMixtures', () => {

  it('should be attainable', () => {
    let state = {
      achivments: { 
        granted: [ ],
      }
    };
    let achiv = new AchivMixtures();
    for (let c of ['A', 'B', 'C', 'D', 'E']) {
      state = achiv.addMixtureMade(state, `potion_${c}`);
      state = achiv.addMixtureMade(state, `potion_${c}`);
      achiv = new AchivMixtures();
      state = achiv.addMixtureMade(state, `potion_${c}`);
      expect(achiv.isAchived(state)).toBe(false);
    }
    state = achiv.addMixtureMade(state, `potion_F`);
    expect(achiv.isAchived(state)).toBe(true);
  });
});