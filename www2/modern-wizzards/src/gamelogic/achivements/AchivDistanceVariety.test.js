import AchivDistanceVariety from './AchivDistanceVariety.js';
import TestData from '../../test/TestData.js';
import Location from '../../gis/Location.js';

describe('AchivDistanceVariety', () => {

  it('should be attainable', () => {
    let state = {
      achivments: { 
        granted: [ ],
      }
    };
    let achiv = new AchivDistanceVariety();
    let loc = new Location(...TestData.cities.radom);
    const step = (TestData.cities.radom[0] - TestData.cities.warsaw[0]) / 100;
    for (let i = 0; i < 500; i++) {
      loc = new Location("test", 
          TestData.cities.radom[0] + i * step, 
          TestData.cities.radom[1] + i * step);
      state = achiv.addMetersTraveled(state, loc);
      if (i % 10 == 0) {
        achiv = new AchivDistanceVariety();
      }
    }
    expect(achiv.isAchived(state)).toBe(true);
  });
});