import Achivement from "./Achivement.js";
import GeoPoint from "../../gis/GeoPoint.js";
import Units from "../../gis/Units.js";

export default class AchivDistanceTravel extends Achivement {	
	constructor() {
		super("achiv_disttraveled");
		this.prevLoc = null;
	}
	
	addMetersTraveled(state, /* Location */ l)	{
		const locationGP = new GeoPoint(Math.floor(l.getLatitude() * 1E6), Math.floor(l.getLongitude() * 1E6));
		
		if(!this.prevLoc) {
			this.prevLoc = locationGP;
		}
		let meterDelta = Units.optimizedDistance(this.prevLoc, locationGP);

		if(meterDelta > 1000) {
			meterDelta = 0;
		}
		this.prevLoc = locationGP;
		const metersTraveled = (state.achivments.metersTraveled || 0) + meterDelta;
		const newState = { ...state, achivments: {...state.achivments, metersTraveled} };		
		if(metersTraveled > 20000)	{
			return this.achive(newState);
		} else {
			return newState;
		}
	}
}