import Achivement from "./Achivement.js";

export default class AchivMixtures extends Achivement {
  
  constructor() {
    super("achiv_mixtures");
  }
  
  addMixtureMade(state, what) {
    const mixturesMade = state.achivments.mixturesMade || { };
    mixturesMade[what] = (mixturesMade[what] || 0) + 1;
    const newState = {
      ...state,
      achivments: {
        ...state.achivments,
        mixturesMade
      }
    };

    if(Object.keys(mixturesMade).length >= 6 && !this.isAchived(newState))  {
      return this.achive(newState);
    } else {
      return newState;
    }
  }
}
