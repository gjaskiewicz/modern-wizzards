import AchivPlants from './AchivPlants.js';
import TestData from '../../test/TestData.js';
import Location from '../../gis/Location.js';

describe('AchivPlants', () => {

  it('should be attainable', () => {
    let state = {
      achivments: { 
        granted: [ ],
      }
    };
    let achiv = new AchivPlants();
    for (let c of ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']) {
      state = achiv.addCollectedPlant(state, `item_${c}`);
      state = achiv.addCollectedPlant(state, `item_${c}`);
      achiv = new AchivPlants();
      state = achiv.addCollectedPlant(state, `item_${c}`);
      expect(achiv.isAchived(state)).toBe(false);
    }
    state = achiv.addCollectedPlant(state, `item_I`);
    expect(achiv.isAchived(state)).toBe(true);
  });
});