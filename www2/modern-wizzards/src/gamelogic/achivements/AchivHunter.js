import Achivement from "./Achivement.js";

export default class AchivHunter extends Achivement {
	
	constructor() {
		super("achiv_hunter");
	}
	
	addCreatureDefeated(state, creature)	{
		const defetedCreatures = state.achivments.defetedCreatures || { };
		defetedCreatures[creature] = (defetedCreatures[creature] || 0) + 1;
		const newState = {
			...state,
			achivments: {
				...state.achivments,
				defetedCreatures
			}
		};
		if(Object.keys(defetedCreatures).length >= 8 && !this.isAchived(newState))	{
			return this.achive(newState);
		} else {
			return newState;
		}
	}
}