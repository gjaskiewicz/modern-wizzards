import Achivement from "./Achivement.js";

export default class AchivGameFinished extends Achivement {
  constructor() {
    super("achiv_gamefinished");
  }
}
