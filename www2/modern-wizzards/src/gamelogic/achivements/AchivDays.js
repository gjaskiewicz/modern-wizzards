import Achivement from "./Achivement.js";

export default class AchivDays extends Achivement {
	
  constructor() {
    super("achiv_days");
  }

  getTime() {
    return (new Date()).getTime();
  }
	
  nextRun(state) {
    const today = Math.floor(this.getTime() / (1000 * 60 * 60 * 24));
    if(state.achivments.lastDay == today) {
      return state;
    } else if(state.achivments.lastDay + 1 == today) {
      const daysInARow = (state.achivments.daysInARow || 0) + 1;
      const lastDay = today;
      const newState = { ...state, achivments: {...state.achivments, daysInARow, lastDay} };
      if(daysInARow >= 7) {
        return this.achive(newState);
      }
      return newState;
    } else {
      return { ...state, achivments: {...state.achivments, daysInARow:1, lastDay:today} };
    }
  }
}
