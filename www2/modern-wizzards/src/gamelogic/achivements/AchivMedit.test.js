import AchivMedit from './AchivMedit.js';
import TestData from '../../test/TestData.js';
import Location from '../../gis/Location.js';

describe('AchivMedit', () => {

  it('should be attainable', () => {
    let state = {
      achivments: { 
        granted: [ ],
      }
    };
    let achiv = new AchivMedit();
    for (let i = 0; i < 1000; i++) {
      state = achiv.checkMedit(state, 1);
    }
    expect(achiv.isAchived(state)).toBe(true);
  });
});