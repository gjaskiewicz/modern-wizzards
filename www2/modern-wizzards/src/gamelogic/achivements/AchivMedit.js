import Achivement from "./Achivement.js";

export default class AchivMedit extends Achivement {
  	
  constructor() {
    super("achiv_medit");
    this.totalSecInZone = 0; /* transient */
  }
	
  stop() {
    this.totalSecInZone = 0;
  }
	
  checkMedit(state, dt) {
    if(this.isAchived(state)) {
      return state;
    }
    this.totalSecInZone += Math.max(dt, 1);
    if(this.totalSecInZone > 60 * 15) {
      return this.achive(state);
    }
    return state;
  }
}
