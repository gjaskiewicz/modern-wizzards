import AchivHunter from './AchivHunter.js';
import AchivPlants from './AchivPlants.js';
import AchivMixtures from './AchivMixtures.js';
import AchivDistanceTravel from './AchivDistanceTravel.js';
import AchivDistanceVariety from './AchivDistanceVariety.js';
import AchivDays from './AchivDays.js';
import AchivGameFinished from './AchivGameFinished.js';
import AchivMedit from './AchivMedit.js';
import AchivJuggle from './AchivJuggle.js';

export default class AchivContainer {

  constructor() {
    this.achivHunter = new AchivHunter();
    this.achivPlants = new AchivPlants();
    this.achivMixtures = new AchivMixtures();
    this.achivDistanceTravel = new AchivDistanceTravel();
    this.achivDistanceVariety = new AchivDistanceVariety();
    this.achivDays = new AchivDays();
    this.achivGameFinished = new AchivGameFinished();
    this.achivMedit = new AchivMedit();
    this.achivJuggle = new AchivJuggle();
    this.allAchives = [
      this.achivHunter, this.achivPlants, this.achivMixtures,
      this.achivDistanceTravel, this.achivDistanceVariety, 
      this.achivDays, this.achivMedit, this.achivJuggle,
      this.achivGameFinished
    ];
  }
  
  getAllAchivs() {
    return this.allAchives;
  }
  
  getAchivHunter() {
    return this.achivHunter;
  }

  getAchivPlants() {
    return this.achivPlants;
  }
  
  getAchivMixtures() {
    return this.achivMixtures;
  }

  getAchivDistanceTravel() {
    return this.achivDistanceTravel;
  }
  
  getAchivDistanceVariety() {
    return this.achivDistanceVariety;
  }
  
  getAchivGameFinished() {
    return this.achivGameFinished;
  }
  
  getAchivDays() {
    return this.achivDays;
  }
  
  getAchivMedit() {
    return this.achivMedit;
  }
  
  getAchivJuggle() {
    return this.achivJuggle;
  }
}