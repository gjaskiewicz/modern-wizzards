import AchivHunter from './AchivHunter.js';
import TestData from '../../test/TestData.js';
import Location from '../../gis/Location.js';

describe('AchivHunter', () => {

  it('should be attainable', () => {
    let state = {
      achivments: { 
        granted: [ ],
      }
    };
    let achiv = new AchivHunter();
    for (let c of ['A', 'B', 'C', 'D', 'E', 'F', 'G']) {
      state = achiv.addCreatureDefeated(state, `creature_${c}`);
      state = achiv.addCreatureDefeated(state, `creature_${c}`);
      achiv = new AchivHunter();
      state = achiv.addCreatureDefeated(state, `creature_${c}`);
      expect(achiv.isAchived(state)).toBe(false);
    }
    state = achiv.addCreatureDefeated(state, `creature_H`);
    expect(achiv.isAchived(state)).toBe(true);
  });
});