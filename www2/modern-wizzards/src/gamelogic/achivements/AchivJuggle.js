import Achivement from "./Achivement.js";

export default class AchivJuggle extends Achivement {
	
	constructor() {
		super("achiv_juggle");
	}
	
	checkJuggling(state)	{
		if(this.isAchived(state)) {
			return state;
		}
		let i = 0;
		for(let sp of state.playerEgo.underEffectOfSpell)	{
			if(sp.nameID.equals("spell_telekinesis")) {
				i++;
			}
		}
		if(i >= 3)	{
			return this.achive(state);
		}
		return state;
	}
}