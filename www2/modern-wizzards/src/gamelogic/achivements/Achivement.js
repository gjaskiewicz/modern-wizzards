import MWToast from "../../ui/MWToast.js";
import R from "../../R.js";
import ObjectInfoFactory from "../metadata/ObjectInfoFactory.js";

export default class Achivement {
  constructor(nameid) {
    this.nameid = nameid;
  }
	
  isAchived(state) {
    return state.achivments.granted.some(a => a == this.nameid);
  }
	
  getInfo() {
    return ObjectInfoFactory.get().getInfoStandarized(this.nameid);
  }
	
  achive(state) {
    if(this.isAchived(state)) {
      return state;
    }

    MWToast.toast(R.strings.achivment_unlocked, {
      img: this.getInfo().getImageID(),
    });
    return {
      ...state, 
      achivments: {
        ...state.achivments,
        granted: [...state.achivments.granted, this.nameid]
      }
    };
  }
}
