import Achivement from "./Achivement.js";

export default class AchivPlants extends Achivement {
	
	constructor() {
		super("achiv_plants");
	}
	
	addCollectedPlant(state, plant)	{
		const collectedPlants = state.achivments.collectedPlants || { };
		collectedPlants[plant] = (collectedPlants[plant] || 0) + 1;
		const newState = {
			...state,
			achivments: {
				...state.achivments,
				collectedPlants
			}
		};
		if(Object.keys(collectedPlants).length >= 9 && !this.isAchived(newState)) {
			return this.achive(newState);
		} else {
			return newState;
		}
	}
}