export default class FightBehaviour  {
  
  constructor() {
    this.behaviourPosition = 0;
    this.behaviourOpacity = 255;
  }
  
  getBehaviourPosition() {
    return this.behaviourPosition;
  }

  setBehaviourPosition(behaviourPosition) {
    this.behaviourPosition = behaviourPosition;
  }
  
  updateBehaviourPosition(creature, iteration) {
    this.setBehaviourPosition(Math.sin(iteration / 18) * 100);
  }
  
  /**
   * Widocznosc stworka w widoku walki
   * @return wodocznosc od 0 do 255
   */
  getBehaviourOpacity() {
    return this.behaviourOpacity;
  }
}
