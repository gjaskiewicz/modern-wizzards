import Fight from './Fight.js';
import Bunny from './creatures/Bunny.js';
import Player from './creatures/Player.js';
import Fireball from './spells/Fireball.js';
import TestData from '../test/TestData.js';
import GameController from '../main.js';
import MainLoop from './MainLoop.js';
import Game from '../Game.js';
import GameSaver from '../utils/GameSaver.js';

describe('Fight', () => {
  beforeEach(() => {
    window.gameController = new GameController();
    Game.getGame();
    window.effects = {addEffect: () => undefined};
    GameSaver.disable();
  });

  it('should be able to win with bunny', () => {
    const fightCreature = new Bunny();
    const fight = new Fight(fightCreature);
    let s = {
      gameState: Game.State.STATE_FIGHTING,
      playerEgo: TestData.getFullEgo(),
      ui: {
        fight, fightCreature,
      },
      achivments: { },
      worldContainer: {
        gatheredPlants: [ ],
        creaturesDefeated: [ ],
      },
    };

    const loop = new MainLoop(window.gameController);
    for(let i = 0; i < 60; i++) {
      s = loop.iterateGameState(s);
      if (i % 10 == 9) {
        let spell = new Fireball(Player.INSTANCE);
        spell.behaviourRange = 100; // na 100% trafi
        s = fight.playerCastsSpell(s, spell);
      }
    }
    expect(s.ui.fightCreature.getHealth()).toBe(0);
    expect(s.ui.fight.finished).toBe(true);
    expect(s.ui.fight.getFightIteration()).toBeLessThan(59);
  });

  it('should be able to loose with bunny', () => {
    const fightCreature = new Bunny();
    const fight = new Fight(fightCreature);
    let s = {
      gameState: Game.State.STATE_FIGHTING,
      playerEgo: {
        ...TestData.getFullEgo(),
        health: 10,
      },
      ui: {
        fight, fightCreature,
      },
      worldContainer: {
        gatheredPlants: [ ],
        creaturesDefeated: [ ],
      },
    };

    const loop = new MainLoop(window.gameController);
    for(let i = 0; i < 1000; i++) {
      s = loop.iterateGameState(s);
    }
    expect(s.playerEgo.health).toBe(0);
    expect(s.gameState).toBe(Game.State.STATE_PLAYERDEAD);
    expect(s.ui.fight.finished).toBe(true);
    expect(s.ui.fight.getFightIteration()).toBeLessThan(999);
  });
});
