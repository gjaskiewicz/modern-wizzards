import Inventory from './Inventory.js';
import TestData from '../test/TestData.js';

describe('Inventory', () => {
  let inv;

  beforeEach(() => {
    inv = [...TestData.getFullEgo().inventory];
    inv = inv.filter(i => i.name.startsWith("item_"));
  });

  describe('should add plant', () => {

    it('on empty', () => {
      const newInv = Inventory.get().addPlant(inv, "item_rozkovnik");
      expect(newInv[2]).toEqual({
          name: "item_rozkovnik",
          count: 1,
      });
    });

    it('on existing', () => { 
      const newInv = Inventory.get().addPlant(inv, "item_fernflower");
      expect(newInv[1]).toEqual({
        name: "item_fernflower",
        count: 2,
      });
    });
  });

  describe('should remove plant', () => {
    it('on empty', () => {
      const newInv = Inventory.get().removePlant(inv, "item_rozkovnik");
      expect(newInv[0]).toEqual({
        name: "item_daisyflower",
        count: 2,
      });
      expect(newInv[1]).toEqual({
        name: "item_fernflower",
        count: 1,
      });
      expect(newInv.length).toBe(2);
    });

    it('on existing', () => { 
      const newInv = Inventory.get().removePlant(inv, "item_daisyflower");
      expect(newInv[0]).toEqual({
        name: "item_daisyflower",
        count: 1,
      });
      expect(newInv[1]).toEqual({
        name: "item_fernflower",
        count: 1,
      });
      expect(newInv.length).toBe(2);
    });

    it('for single item', () => {
      const newInv = Inventory.get().removePlant(inv, "item_fernflower");
      expect(newInv[0]).toEqual({
        name: "item_daisyflower",
        count: 2,
      });
      expect(newInv.length).toBe(1);
    })
  });
});
