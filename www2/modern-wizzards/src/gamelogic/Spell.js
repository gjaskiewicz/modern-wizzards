import ObjectInfoFactory from './metadata/ObjectInfoFactory.js';
import MWToast from '../ui/MWToast.js';
import Player from './creatures/Player.js';
import Castable from './Castable.js';
import $log from '../utils/log.js';
import R from '../R.js';

const TAG = "SPELL";

export default class Spell {

  constructor(owner) {
    this.owner = owner;
    this.behaviourRange = 100;
  }

  getInfo() {
    return ObjectInfoFactory.get().getInfoStandarized(this.nameID);
  }

  setTarget(creature) {
    this.target = creature;
    if(this.target == Player.INSTANCE) {
      this.playerIsTarget = true;
    } else {
      this.playerIsTarget = false;
    }
  }

  getRequiredMana() {
    return this.requiredMana;
  }

  getEffectTargetType() {
    return this.targetType;
  }

  checkIfSpellHits(state)  {
    if (Math.abs(this.target.getBehaviourPosition()) < this.behaviourRange)   {
      //MWToast.makeToast( Game.getGlobalTopmostActivity(), "Spell HITS!", Toast.LENGTH_SHORT).show();
      if (this.target.isUnderEffectOfSpell(state, "spell_astralshield"))  {
        MWToast.toast(R.strings.spells_repeled);
        //TODO: odbija w przeciwnika?
        return false;
      }
      return true;
    } else {
      MWToast.toast(R.strings.spells_miss);
      return false;
    }
  }

  castThis(state) {
    $log.d(TAG, "CASTING :" + this.nameID);

    //jesli nie ma wystarczajaco mana
    const neededMana = this.getRequiredMana();
    if(this.owner.getMana(state) < neededMana)  {
      $log.d("SPELL", "CASTING :" + this.nameID + " FAILED! NO MANA!");
      if(this.owner == Player.INSTANCE)  {
        MWToast.toast(R.strings.spells_nomana); //, Toast.LENGTH_SHORT);
      }
      return {success: false, state}; //TODO: sprawdzenie gdzie indziej?
    } else {
      if (neededMana > 0) {
        state = this.owner.addMana(state, -neededMana);
      }
    }
    
    //dodaje do listy
    if(this.getEffectTargetType() == Castable.EffectTargetType.SELF)  {
      $log.d(TAG, "Adding self to effect target. Name: " + this.owner.nameID);
      state = this.owner.addUnderEffectOfSpell(state, this);
    } else if(this.getEffectTargetType() == Castable.EffectTargetType.ANOTHER)  {
      if(this.target == state.ui.fightCreature) {
        // TODO: migrate   Game.getGame().fight.controller.addSpellToDisplay(this);
      }
      if(this.checkIfSpellHits(state)) {
        state = this.target.addUnderEffectOfSpell(state, this);
      }
    } else {
      //AMBIENT//nic do roboty, tylko "take effect" 
    }
    $log.d(TAG, "CASTING " + this.nameID + " SUCCESS");
    return {success:true, state};
  }

  takeEffect(state)  {
    this.turnDuration--;
    if(this.turnDuration <= 0)  {
      $log.d("SPELL", "this spell should be removed");
    }
    return state;
  }
}
