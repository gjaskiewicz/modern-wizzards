import Spell from '../Spell';
import Castable from '../Castable.js';

export default class Drain extends Spell {

  static MANA_TO_TAKE = 10;
  
  constructor(owner) {
    super(owner);
    this.nameID = "spell_drain";
    this.requiredMana = 0;
    this.turnDuration = 1;
    this.usageType = Castable.UsageType.ANYTIME;
    this.targetType = Castable.EffectTargetType.ANOTHER;
    this.behaviourRange = 100;
  }

  takeEffect(state) {
    state = super.takeEffect(state);
    state = this.target.addMana(state, -Drain.MANA_TO_TAKE);
    return this.owner.addMana(state, Drain.MANA_TO_TAKE / 2, false);
  }
}
