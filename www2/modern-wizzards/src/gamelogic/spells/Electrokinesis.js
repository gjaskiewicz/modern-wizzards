import Spell from '../Spell';
import Castable from '../Castable.js';
import $log from '../../utils/log.js';
import {ITERATIONS_PER_SECOND} from '../../Constants.js';

export default class Electrokinesis extends Spell {
 
  constructor(owner)  {
    super(owner);
    this.nameID = "spell_electrokinesis";
    this.requiredMana = 25;
    this.turnDuration = 1;
    this.hitpointsToTake = 15;
    this.usageType = Castable.UsageType.BATTLE;
    this.targetType = Castable.EffectTargetType.ANOTHER;
    this.behaviourRange = 80;
    this.infects = false;
    /**spell zawierajšcy się przy rzucaniu*/ 
    this.infection = null;
    this.color = 0x683a75c4;
  }

  castThis(s) {
    const {success, state} = super.castThis(s);
    if (success && this.infects && this.infection)  {
      this.infection.setTarget(this.target);
      return {
        success: true,
        state: this.infection.castThis(state).state,
      };
    }
    return {success, state};
  }

  takeEffect(state) {
    state = super.takeEffect(state);
    $log.d("SPELL", "Eletrokinesis takes hitpoints");
    return this.target.addHealth(state, -this.hitpointsToTake);
  }

}
