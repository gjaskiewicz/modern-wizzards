import OutOfBodyExperience from './OutOfBodyExperience.js';
import Player from '../creatures/Player.js';
import GameController from '../../main.js';
import Game from '../../Game.js';
import PlayerEgo from '../../PlayerEgo.js';
import Units from '../../gis/Units.js';
import MainLoop from '../../gamelogic/MainLoop.js';
import EventSystem from '../events/EventSystem.js';
import EventType from '../events/EventTypes.js';

import TestData from '../../test/TestData.js';
import FakeMapController from '../../test/FakeMapController.js';

describe('OOBE', () => {
  window.gameController = new GameController();
  const mapController = new FakeMapController();
  mapController.init();
  let s = { };
  let backInBody = -100;
  EventSystem.INSTANCE.registerListener(
    EventType.BACK_IN_BODY, 
    {handleEvent: (s) => { backInBody++; return s; }});

  beforeEach(() => {
    backInBody = 0;
  });

  let setup = (mana) => {
    s = {
      playerEgo: {
        ...TestData.getFullEgo(),
        sightRange: 500,
        mana: (mana || 10000),
        maxMana: (mana || 10000),
      },
      achivments: { },
      worldContainer: {
        saveDate: '1970-01-01',
        gatheredPlants: [ ],
        creaturesDefeated: [ ],
      },
    };
    window.gameController.dispatch = (s_) => { s = s_(s) };
    const oob = new OutOfBodyExperience(Player.INSTANCE);
    const loop = new MainLoop(window.gameController);
    Game.getGame().initiateGameState(s);
    const item = Game.getGame().worldContainer.getAllGeneratedObjs().filter(i => i.displayed)[0];

    // cast
    const res = oob.castThis(s);
    expect(res.success).toBe(true);
    s = res.state;
    return [loop, item, oob];
  };

  let expectLocationEqual = (loc1, loc2) => {
    expect(loc1.lat).toBe(loc2.lat);
    expect(loc1.lng).toBe(loc2.lng);
  };

  it('should go interact', () => {
    let [loop, item, oob] = setup();

    mapController.handler({
      lat: () => item.locationGP.lat,
      lng: () => item.locationGP.lng
    });

    expect(s.playerEgo.underEffectOfSpell.length).toBe(1);
    expect(s.playerEgo.underEffectOfSpell[0]).toBe(oob);    

    // fly
    const dist0 = Units.optimizedDistance(PlayerEgo.getPlayerLocation(s.playerEgo), item.getGeoPoint(s));
    for (let i = 0; i < 1000; i++) {
      s = loop.iterateGameState(s);
    }
    const dist1 = Units.optimizedDistance(PlayerEgo.getPlayerLocation(s.playerEgo), item.getGeoPoint(s));
    expect(dist0).toBeGreaterThan(dist1);
    expect(s.ui.activeOverlay).toBe('alert_item');
  });

  it('should return by click', () => {
    let [loop, item, oob] = setup();

    mapController.handler({
      lat: () => item.locationGP.lat,
      lng: () => item.locationGP.lng
    });

    expect(s.playerEgo.underEffectOfSpell.length).toBe(1);
    expect(s.playerEgo.underEffectOfSpell[0]).toBe(oob);    
    expect(oob.oobPointer.displayed).toBe(true);

    // fly
    const dist0 = Units.optimizedDistance(PlayerEgo.getPlayerLocation(s.playerEgo), item.getGeoPoint(s));
    for (let i = 0; i < 100; i++) {
      s = loop.iterateGameState(s);
    }
    const dist1 = Units.optimizedDistance(PlayerEgo.getPlayerLocation(s.playerEgo), item.getGeoPoint(s));
    expect(dist0).toBeGreaterThan(dist1);
    expect(oob.oobPointer.displayed).toBe(true);

    // fly back
    const origGP = PlayerEgo.getGeoPoint(s.playerEgo);
    mapController.handler({
      lat: () => origGP.lat,
      lng: () => origGP.lng
    });
    for (let i = 0; i < 100; i++) {
      s = loop.iterateGameState(s);
    }
    expect(s.playerEgo.underEffectOfSpell.length).toBe(0);
    expectLocationEqual(PlayerEgo.getGeoPoint(s.playerEgo), PlayerEgo.getPlayerLocation(s.playerEgo));
    expect(oob.oobPointer.displayed).toBe(false);
    expect(backInBody).toBe(1);
  });

  it('should return when no mana', () => {
    let [loop, item, oob] = setup(30);

    mapController.handler({
      lat: () => item.locationGP.lat,
      lng: () => item.locationGP.lng
    });

    expect(s.playerEgo.underEffectOfSpell.length).toBe(1);
    expect(s.playerEgo.underEffectOfSpell[0]).toBe(oob);    
    expect(oob.oobPointer.displayed).toBe(true);

    // fly
    for (let i = 0; i < 200; i++) {
      s = loop.iterateGameState(s);
    }
    expect(s.playerEgo.underEffectOfSpell.length).toBe(0);
    expectLocationEqual(PlayerEgo.getGeoPoint(s.playerEgo), PlayerEgo.getPlayerLocation(s.playerEgo));
    expect(oob.oobPointer.displayed).toBe(false);
    expect(backInBody).toBe(1);
  });

  it('should return when time passed', () => {
    let [loop, item, oob] = setup();

    mapController.handler({
      lat: () => item.locationGP.lat,
      lng: () => item.locationGP.lng
    });

    expect(s.playerEgo.underEffectOfSpell.length).toBe(1);
    expect(s.playerEgo.underEffectOfSpell[0]).toBe(oob);    
    expect(oob.oobPointer.displayed).toBe(true);

    // fly
    for (let i = 0; i < 8000; i++) {
      s = loop.iterateGameState(s);
    }
    expect(s.playerEgo.mana).toBeGreaterThan(0);
    expect(s.playerEgo.underEffectOfSpell.length).toBe(0);
    expectLocationEqual(PlayerEgo.getGeoPoint(s.playerEgo), PlayerEgo.getPlayerLocation(s.playerEgo));
    expect(oob.oobPointer.displayed).toBe(false);
    expect(backInBody).toBe(1);
  });
});
