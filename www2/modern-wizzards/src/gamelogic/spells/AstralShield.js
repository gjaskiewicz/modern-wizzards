import Spell from '../Spell';
import Castable from '../Castable.js';
import {ITERATIONS_PER_SECOND} from '../../Constants.js';

export default class AstralShield extends Spell  {
  constructor(owner)  {
    super(owner);
    this.nameID = "spell_astralshield";
    this.requiredMana = 25;
    this.turnDuration = ITERATIONS_PER_SECOND * 7;
    this.usageType = Castable.UsageType.BATTLE;
    this.targetType = Castable.EffectTargetType.SELF;
  }

  takeEffect(s) {
    let state = super.takeEffect(s);
    if (this.turnDuration <= 0) {
      return state;
    }
    //zdejmowanie zlych czarow 
    if (this.target == null) { 
      return state;
    }
    for (let spl of this.target.underEffectOfSpell)  {
      if (spl.nameID == "spell_physical" ||
          spl.nameID == "spell_drain" ||
          spl.nameID == "spell_fireball")
        spl.turnDuration = 0;
    }
    return state;
  }   
}
