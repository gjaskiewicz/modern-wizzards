import Spell from '../Spell';
import Castable from '../Castable.js';
import {ITERATIONS_PER_SECOND} from '../../Constants.js';

export default class Speed extends Spell  {
  constructor(owner)  {
    super(owner);
    this.nameID = "spell_battlespeed";
    this.requiredMana = 10;
    this.turnDuration = ITERATIONS_PER_SECOND * 2;
    this.usageType = Castable.UsageType.BATTLE;
    this.targetType = Castable.EffectTargetType.SELF;
  }   
}
