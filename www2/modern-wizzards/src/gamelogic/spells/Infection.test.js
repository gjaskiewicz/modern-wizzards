import Infection from './Infection.js';
import PhysicalAttack from './PhysicalAttack.js';
import Bunny from '../creatures/Bunny.js';
import Player from '../creatures/Player.js';
import MainLoop from '../MainLoop.js';
import GameState from '../../GameState.js';
import GameSaver from '../../utils/GameSaver.js';
import TestData from '../../test/TestData.js';

describe('Infection', () => {
  window.effects = {addEffect: () => undefined};
  GameSaver.disable();

  it('should infest with attack', () => {
    let s = {
      playerEgo: TestData.getFullEgo(),
      ui: { },
    };
    const bunny = new Bunny();
    const pa = new PhysicalAttack(bunny);
    pa.setTarget(Player.INSTANCE);
    pa.infection = new Infection(bunny);
    pa.infects = true;

    const {state, success} = pa.castThis(s);
    expect(success).toBe(true);
    expect(state.playerEgo.underEffectOfSpell.length).toBe(2);
    expect(state.playerEgo.underEffectOfSpell[0].nameID).toBe('spell_physical');
    expect(state.playerEgo.underEffectOfSpell[1].nameID).toBe('spell_infection');
  });

  it('should take effect', () => {
    const mainLoop = new MainLoop();
    let s = {
      playerEgo: {
        ...TestData.getFullEgo(),
        health: 10,
      },
      gameState: GameState.STATE_WALKING,
      ui: { },
      worldContainer: {
        gatheredPlants: [ ],
      }
    };
    const bunny = new Bunny();
    let infection = new Infection(bunny);
    infection.setTarget(Player.INSTANCE);

    const {state, success} = infection.castThis(s);
    s = state;
    expect(success).toBe(true);
    expect(state.playerEgo.underEffectOfSpell.length).toBe(1);

    for (let i = 0; i < 1000; i++) {
      s = mainLoop.iterateGameState(s);
    }
    
    expect(s.playerEgo.health).toBe(5);
  });

  it('should kill player', () => {
    const mainLoop = new MainLoop();
    let s = {
      playerEgo: {
        ...TestData.getFullEgo(),
        health: 2,
      },
      gameState: GameState.STATE_WALKING,
      ui: { },
      worldContainer: {
        gatheredPlants: [ ],
      }
    };
    const bunny = new Bunny();
    let infection = new Infection(bunny);
    infection.setTarget(Player.INSTANCE);
    infection.turnDuration = 1000;

    const {state, success} = infection.castThis(s);
    s = state;
    expect(success).toBe(true);
    expect(state.playerEgo.underEffectOfSpell.length).toBe(1);

    for (let i = 0; i < 1000; i++) {
      s = mainLoop.iterateGameState(s);
    }
    
    expect(s.playerEgo.health).toBe(0);
  });
});