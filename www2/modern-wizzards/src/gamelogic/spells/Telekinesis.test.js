import Telekinesis from './Telekinesis.js';
import Player from '../creatures/Player.js';
import GameController from '../../main.js';
import Game from '../../Game.js';
import Units from '../../gis/Units.js';
import MainLoop from '../../gamelogic/MainLoop.js';

import TestData from '../../test/TestData.js';
import FakeMapController from '../../test/FakeMapController.js';

describe('Telekinesis', () => {

  it('should move items', () => {
    let s = {
      playerEgo: TestData.getFullEgo(),
      achivments: { },
      worldContainer: {
        saveDate: '1970-01-01',
        gatheredPlants: [ ],
        creaturesDefeated: [ ],
      },
    };

    window.gameController = new GameController();
    window.gameController.dispatch = (s_) => { s = s_(s) };
    const loop = new MainLoop(window.gameController);
    const mapController = new FakeMapController();
    mapController.init();
    Game.getGame().initiateGameState(s);

    const tk = new Telekinesis(Player.INSTANCE);
    tk.range = 400;

    // cast
    const res = tk.castThis(s);
    expect(res.success).toBe(true);
    s = res.state;

    // select
    const item = Game.getGame().worldContainer.getAllGeneratedObjs().filter(i => i.displayed)[0];
    mapController.handler({
      lat: () => item.locationGP.lat,
      lng: () => item.locationGP.lng
    });

    expect(tk.pulledObject).toBe(item);
    expect(s.playerEgo.underEffectOfSpell.length).toBe(1);
    expect(s.playerEgo.underEffectOfSpell[0]).toBe(tk);    

    // pull
    const dist0 = Units.optimizedDistance(Player.INSTANCE.getGeoPoint(s), item.getGeoPoint(s));
    for(let i=0;i < 1000;i++) {
      s = loop.iterateGameState(s);
    }
    const dist1 = Units.optimizedDistance(Player.INSTANCE.getGeoPoint(s), item.getGeoPoint(s));
    expect(dist0).toBeGreaterThan(dist1);
  });
});
