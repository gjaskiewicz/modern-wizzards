import Spell from '../Spell';
import Castable from '../Castable.js';

export default class Fireball extends Spell {

  constructor(owner)  {
    super(owner);
    this.nameID = "spell_fireball";
    this.requiredMana = 30;
    this.turnDuration = 1;
    this.usageType = Castable.UsageType.BATTLE;
    this.targetType = Castable.EffectTargetType.ANOTHER;
    this.behaviourRange = 25;
  }

  castThis(state) {
    return super.castThis(state); //koniecznie!
  }

  takeEffect(state) {
    state = super.takeEffect(state);
    let hpToTake = 25;
    if (state.playerEgo.isDev) {
      hpToTake = 300;
    }
    return this.target.addHealth(state, -hpToTake);
  }
}
