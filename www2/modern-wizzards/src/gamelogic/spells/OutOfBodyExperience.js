import Spell from '../Spell.js';
import PhysicalObject from '../PhysicalObject.js';
import Player from '../creatures/Player.js';
import Castable from '../Castable.js';
import PlayerEgo from '../../PlayerEgo.js';
import GeoPoint from '../../gis/GeoPoint.js';
import Units from '../../gis/Units.js';
import SphericalZone from '../../gis/SphericalZone.js';
import MapController from '../../MapController.js';
import MWToast from '../../ui/MWToast.js';
import R from '../../R.js';
// import Game from '../../GameState.js'; // XXX
import $log from '../../utils/log.js';
import {ITERATIONS_PER_SECOND} from '../../Constants.js';
import EventSystem from "../events/EventSystem.js";
import EventTypes from "../events/EventTypes.js";

const TAG = "OutOfBodyExperience";

const OOBState = {
  STATE_FLYING: 1,
  STATE_RETURNING: 0,
};

export default class OutOfBodyExperience extends Spell {
  
  constructor(/* Creature */ owner) {
    super(owner);
    this.nameID = "spell_oob";
    this.requiredMana = 10;
    this.usageType = Castable.UsageType.MAP;
    this.targetType = Castable.EffectTargetType.SELF;
    this.turnDuration = ITERATIONS_PER_SECOND * 60 * 5; //trwa 30 sekund, potem wraca gracza
    this.destination = null;
    this.canStillWalk = true;
    this.hasDestinationChanged = false;
    this.fastReturn = false;
    this.oobPointer = null;
    this.state = OOBState.STATE_FLYING;
  }

  castThis(state) {
    if(this.owner.isUnderEffectOfSpell(state, this.nameID))  {
      return {
        success: false,
        state
      };
    }
    
    this.oobPointer = new OOBpointer(this.owner.getGeoPoint(state));
    const clickListener = newTapListener(this, state);
    this.removeHandler = MapController.getInst().addClickHandler(clickListener);

    MWToast.toast(R.strings.click_where_wish_go);
    return {
      success: true,
      state: window.gameController.showScreenState(state, "map")
    };
  }
  
  forceReturnToBody(/* boolean */ immediate) {
    this.turnDuration = 1;
    this.fastReturn = immediate;
  }

  onRestoreFromSave() {
    super.onRestoreFromSave();
    $log.e(TAG, "Restoring OOBE!!!" );
    this.turnDuration = 0;
  }
  
  takeEffect(state) {
    if(this.turnDuration > 1) {
      // If turnDuration==1 we set returning to body and only after full return we set turnDuration to 0
      this.turnDuration--;
    }

    if(this.turnDuration == 0) {
      return state;
    }

    //czy wracac do ciala?
    if (this.turnDuration >= 0 && 
        (this.turnDuration % ITERATIONS_PER_SECOND == 0 || this.turnDuration == 1) && 
        this.canStillWalk) {
      state = this.owner.addMana(state, -5);
      if (this.owner.getMana(state) <= 0 || this.turnDuration == 1)  {
        this.canStillWalk = false;
        this.latVectL *= 2;
        this.lngVectL *= 2;
        this.removeHandler();
        this.destination = PlayerEgo.getGeoPoint(state.playerEgo);
        if (this.fastReturn) {
          state = {
            ...state,
            playerEgo: {
              ...state.playerEgo,
              oob_lat: this.destination.getLatitudeE6(),
              oob_lng: this.destination.getLongitudeE6(),
            }
          };
        }
          
        if (this.turnDuration != 1) {
          MWToast.toast(R.strings.oob_out_of_mana);
        }
        //TODO: zamknac czar;
      }
    }
    
    //plynny przejazd do miejsca docelowego
    const currLoc = PlayerEgo.getPlayerLocation(state.playerEgo);  // getPlayerLocation
    
    //olane ograniczenie na dystans
    //if(Units.optimizedDistanceCheck(Game.getGame().playerEgo.getLocation(), currLoc, 500))  {
    
    if (!this.canStillWalk) {
      this.destination = PlayerEgo.getGeoPoint(state.playerEgo);
      //gwarantuje, ¿e powrót do cia³a siê uda nawet jak cia³o "ucieka" 
    }
    const latDist = this.destination.getLatitudeE6() - currLoc.getLatitudeE6();
    const lngDist = this.destination.getLongitudeE6() - currLoc.getLongitudeE6();
    const metDist = Math.max(Units.optimizedDistance(this.destination, currLoc), 1);
    const latVect = Math.floor(this.latVectL * (Math.floor(latDist * Units.latitudeToMeters()) / metDist));
    const lngVect = Math.floor(this.lngVectL * (Math.floor(lngDist * Units.longitudeToMeters(
      this.destination.getLatitudeE6())) / metDist)
    );
    //ruch o pol metra
    const oob_lat = currLoc.getLatitudeE6() + latVect;
    const oob_lng = currLoc.getLongitudeE6() + lngVect;
    //if(this.turnDuration % 3 == 0) {
    this.oobPointer.setLocation(new GeoPoint(oob_lat, oob_lng));
    this.oobPointer.updateDisp();
    //}
    
    state = this.checkIfCloseToBody(state);
    return {
      ...state,
      playerEgo: {
        ...state.playerEgo,
        oob_lat,
        oob_lng
      }
    };
  }

  checkIfCloseToBody(state) {
    //dusza gracza jest blisko jego cia³a :)
    const oobLocation = new GeoPoint(state.playerEgo.oob_lat, state.playerEgo.oob_lng);
    if (Units.optimizedDistanceCheck(PlayerEgo.getGeoPoint(state.playerEgo), oobLocation, 15)) {
      /**
       * graczowi skoñczy³a siê mana
       * lub stwierdzi³, ¿e mu siê znudzi³o latanie po mieœci w samej duszy i wróci³ do cia³a.
       */
      if(!this.canStillWalk || (this.canStillWalk && this.hasDestinationChanged && 
          Units.optimizedDistanceCheck(PlayerEgo.getGeoPoint(state.playerEgo), this.destination, 15))) 
      {
        this.turnDuration = 0;
        // TODO delete?   Game.getGame().playerEgo.oobLocation = null;
        this.oobPointer.display(false);
        if (!this.fastReturn) {
          MWToast.toast(R.strings.oob_finished);
        }
          
        state = EventSystem.INSTANCE.notifyListenersState(
          state, this.nameID, EventTypes.BACK_IN_BODY);
      }
    }
    return state;
  }

  supercast(s) {
    return super.castThis(s);
  }
}

class OOBpointer extends PhysicalObject  {
  constructor(pos)  {
    super();
    this.zone = new SphericalZone(pos, 15);
    this.setLocation(pos);
  }
  
  getColor() {
    return 0xBBFFA0FF;
  }
}

const newTapListener = (caller, state) => {
  let clicked = false;
  return (point) => {
    if(!clicked)  {
      clicked = true;
      window.gameController.applyToState((s) => {
        let newState = caller.supercast(s).state;
        // ???
        const callerPoint = caller.owner.getGeoPoint(newState);
        caller.destination = new GeoPoint(point.lat(), point.lng());
        newState = {
          ...newState,
          playerEgo: {
            ...newState.playerEgo,
            oob_lat: callerPoint.getLatitude() * 1E6,
            oob_lng: callerPoint.getLongitude() * 1E6,
          },
        };
        caller.latVectL = Math.floor(1.5 / Units.latitudeToMeters());
        caller.lngVectL = Math.floor(1.5 / Units.longitudeToMeters(
          caller.owner.getGeoPoint(newState).getLatitudeE6())
        );
        return newState;
      });
    }
    
    caller.destination = new GeoPoint(point.lat(), point.lng());
    caller.hasDestinationChanged = true;
    caller.oobPointer.display(true);
    //MWMap2Activity.convLatLngToGeopoint(point);//jesli nie za daleko:
    $log.d(TAG, "OOB DESTINATION CHANGED!");
  }
};
