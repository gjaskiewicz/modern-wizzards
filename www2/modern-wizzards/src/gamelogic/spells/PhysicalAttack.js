import Spell from '../Spell';
import Castable from '../Castable.js';
import MWToast from '../../ui/MWToast.js';
import Dice from '../../utils/Dice.js';
import $log from '../../utils/log.js';
import R from '../../R.js';
import ObjectInfoFactory from '../metadata/ObjectInfoFactory.js';

export default class PhysicalAttack extends Spell {
  
  constructor(owner)  {
    super(owner);
    this.nameID = "spell_physical";
    this.requiredMana = 7;
    this.turnDuration = 1;
    this.hitpointsToTake = 12;
    this.usageType = Castable.UsageType.BATTLE;
    this.targetType = Castable.EffectTargetType.ANOTHER;
    this.behaviourRange = 15;
    this.chanceForCritical = 0.8;
    this.criticalFactor = 2;
    this.infects = false;
    /** spell zawierajšcy się przy rzucaniu */ 
    this.infection = null;
    this.color = 0x88888888;
  }

  castThis(s) {
    const {success, state} = super.castThis(s);
    s = state;
    if (success && this.infects && this.infection)  {
      this.infection.setTarget(this.target);
      s = this.infection.castThis(s).state;
    }

    if (success && this.target.isPlayer()) {
      window.effects.addEffect('hit-bleed', 1500);
    }
    return {success, state: s};
  }

  takeEffect(state) {
    state = super.takeEffect(state);
    $log.d("SPELL", "Physical takes hitpoints");
    let factor = 1;

    if(Dice.getDiceRoll(state, this.owner) > this.chanceForCritical) {
      factor = this.criticalFactor;
      MWToast.toast(R.strings.spells_critical); //, Toast.LENGTH_SHORT);
    }
    return this.target.addHealth(state, -factor * this.hitpointsToTake);
  }

  getInfo() {
    if(this.displayNameID == null) {
      this.displayNameID = this.nameID;
    }
    return ObjectInfoFactory.get().getInfoStandarized(this.displayNameID);
  }
}
