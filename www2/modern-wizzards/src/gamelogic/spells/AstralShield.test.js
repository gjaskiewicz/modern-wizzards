import AstralShield from './AstralShield.js';
import Fireball from './Fireball.js';
import Player from '../creatures/Player.js';
import Bunny from '../creatures/Bunny.js';
import MainLoop from '../../gamelogic/MainLoop.js';
import GameController from '../../main.js';
import GameState from '../../GameState.js';

import TestData from '../../test/TestData.js';

describe('Astral Shield', () => {
  let s = {
    playerEgo: TestData.getFullEgo(),
    gameState: GameState.STATE_WALKING,
    ui: { },
    worldContainer: {
      saveDate: '1970-01-01',
      gatheredPlants: [ ],
      creaturesDefeated: [ ],
    },
  };
  window.gameController = new GameController();
  window.gameController.dispatch = (s_) => { s = s_(s) };

  it('should protect agains fireball after casted', () => {
    const loop = new MainLoop(window.gameController);
    loop.loopIteration = -1;

    const owner = new Bunny();
    const attackSpell = new Fireball(owner);
    attackSpell.target = Player.INSTANCE;
    let castResult = attackSpell.castThis(s);
    expect(castResult.success).toBe(true);
    s = castResult.state;

    expect(s.playerEgo.underEffectOfSpell.length).toBe(1);
    expect(s.playerEgo.underEffectOfSpell[0].nameID).toBe('spell_fireball');

    const defenceSpell = new AstralShield(Player.INSTANCE);
    castResult = defenceSpell.castThis(s);
    expect(castResult.success).toBe(true);
    s = castResult.state;
    s = loop.iterateGameState(s);

    expect(s.playerEgo.underEffectOfSpell.length).toBe(1);
    expect(s.playerEgo.underEffectOfSpell[0].nameID).toBe('spell_astralshield');
  });
/*
  it('should protect agains fireball before casted', () => {
    const loop = new MainLoop(window.gameController);
    loop.loopIteration = -1;

    const defenceSpell = new AstralShield(Player.INSTANCE);
    let castResult = defenceSpell.castThis(s);
    expect(castResult.success).toBe(true);
    s = castResult.state;
    s = loop.iterateGameState(s);

    expect(s.playerEgo.underEffectOfSpell.length).toBe(1);
    expect(s.playerEgo.underEffectOfSpell[0].nameID).toBe('spell_astralshield');

    const owner = new Bunny();
    const attackSpell = new Fireball(owner);
    attackSpell.target = Player.INSTANCE;
    castResult = attackSpell.castThis(s);
    expect(castResult.success).toBe(true);
    s = castResult.state;

    expect(s.playerEgo.underEffectOfSpell.length).toBe(1);
    expect(s.playerEgo.underEffectOfSpell[0].nameID).toBe('spell_fireball');
  });
*/
});
