import Spell from '../Spell';
import Castable from '../Castable.js';
import MWToast from '../../ui/MWToast.js';
import R from '../../R.js';
import MapController from '../../MapController.js';
// import Game from '../../Game.js';
import PlayerEgo from '../../PlayerEgo.js';
import GeoPoint from '../../gis/GeoPoint.js';
import Units from '../../gis/Units.js';
import $log from '../../utils/log.js';
import CollectableItem from '../world/CollectableItem.js';

const TAG = "Telekinesis";

export default class Telekinesis extends Spell {
  
  constructor(owner)  {
    super(owner);
    this.nameID = "spell_telekinesis";
    this.requiredMana = 20;
    this.usageType = Castable.UsageType.MAP;
    this.targetType = Castable.EffectTargetType.SELF;
    this.turnDuration = 1000;
    this.pulledObject = null; /* CollectableItem */
    this.removeHandler = null;
    this.range = 250;
  }

  /*
  onRestoreFromSave() {
    super.onRestoreFromSave();
    turnDuration=0;
    for( Spell spl : Player.getInst().underEffectOfSpell)  {
      if(spl.nameID==this.nameID)
        Player.getInst().underEffectOfSpell.remove(spl);
    }
  }
  */
  
  castThis(state) {
    MWToast.toast(R.strings.spell_telekinesis_using);
    const clickListener = newTapListener(this, state);
    this.removeHandler = MapController.getInst().addClickHandler(clickListener);
    return {
      success: true,
      state: window.gameController.showScreenState(state, "map")
    };
  }

  takeEffect(state) {
    this.turnDuration--;
    const playerPoint = PlayerEgo.getGeoPoint(state.playerEgo);
    this.pulledObject.moveTowards(playerPoint, 5);
    if(this.pulledObject.zone.radius > 15 && this.turnDuration % 5 == 0) {
      this.pulledObject.zone.radius--;
    } 
      
    if(this.pulledObject.shouldInteract(playerPoint) && this.turnDuration > 30) {
      //to zakoncz
      this.turnDuration = 30;
    }
    /* Po co ?
    if(this.turnDuration == 0)  {
      this.pulledObject.display(false);
    }
    */

    this.pulledObject.updateDisp();
    return state;
  }

  pullObject(/* CollectableItem */ obj) {
    this.pulledObject = obj;
    window.gameController.applyToState(state => {
      state = super.castThis(state).state;
      // FIXME   state = Game.getGame().getAchivContainer().getAchivJuggle().checkJuggling(state);
      this.removeHandler();
      return state;
    });
  }  
}

const newTapListener = (caller, state) => {
  return (/* LatLng */ llpoint) => {
    const point = new GeoPoint(llpoint.lat(), llpoint.lng());
    $log.i(TAG, "onTap from Telekinesis");

    const playerPoint = PlayerEgo.getGeoPoint(state.playerEgo);
    if(!Units.optimizedDistanceCheck(point, playerPoint, caller.range))  {
      MWToast.toast(R.strings.spell_telekinesis_object_too_far);
      return;
    }
      
    for(let obj of window.game.worldContainer.getAllObjects()) {
      if(obj instanceof CollectableItem ) {
        if (obj.shouldInteract(point)) {
          MWToast.toast(R.strings.spell_telekinesis_pulling);
          caller.pullObject(obj);
        }
      }
    }
    return;
  }
}
