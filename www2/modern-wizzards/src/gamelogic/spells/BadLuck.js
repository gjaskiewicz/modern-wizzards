import Spell from '../Spell';
import Castable from '../Castable.js';
import {ITERATIONS_PER_SECOND} from '../../Constants.js';

export default class BadLuck extends Spell {
  constructor(owner) {
    super(owner);
    this.nameID = "spell_badluck";
    this.requiredMana = 0;
    this.turnDuration = ITERATIONS_PER_SECOND * 60 * 15; // dziala 15 
                                                         // minut!
    this.usageType = Castable.UsageType.BATTLE;
    this.targetType = Castable.EffectTargetType.ANOTHER;
  }

  castThis(state) {
    // zapewnienie ze gracz bedzie tylko pod jednš infekcjš
    if (this.target.isUnderEffectOfSpell(state, this.nameID))
      return {success:false, state};
    return super.castThis(state);
  }
}
