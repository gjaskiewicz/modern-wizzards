import Spell from '../Spell';
import Castable from '../Castable.js';
import Player from "../creatures/Player.js";
import Game from "../../Game.js";
import {ITERATIONS_PER_SECOND} from "../../Constants.js";

const takeHealthEvery = ITERATIONS_PER_SECOND; // turns

export default class Infection extends Spell {

	constructor(owner) {
		super(owner);
		this.nameID = "spell_infection";
		this.requiredMana = 0;
		this.turnDuration = 2 * ITERATIONS_PER_SECOND * 60 * 60;
		this.usageType = Castable.UsageType.ANYTIME;
		this.targetType = Castable.EffectTargetType.ANOTHER;
		this.behaviourRange = 100;
	}

	castThis(s) {
		// zapewnienie ze gracz bedzie tylko pod jedn� infekcj�
		if (this.target.isUnderEffectOfSpell(s, this.nameID)) {
      return {success: false, state: s};
    }
		return super.castThis(s);
	}

	takeEffect(s) {
		s = super.takeEffect(s);
		// zdejmuje HP a� po 5 pkt, a po godzinie reszte.
		if (this.turnDuration > (3600 * ITERATIONS_PER_SECOND)) {
			if (this.turnDuration % takeHealthEvery == 0 && this.target.getHealth(s) > 5) {
				s = this.target.addHealth(s, -1);
				if (this.target.isPlayer()) {
					window.effects.addEffect('infest', 1000);
				}
			}
		} else {
			if (this.turnDuration % takeHealthEvery == 0 && this.target.getHealth(s) > 0) {
				s = this.target.addHealth(s, -1);
				if (this.target.isPlayer()) {
					window.effects.addEffect('infest-hard', 1000);
				}
			}
		}
		if(this.target == Player.INSTANCE && Player.INSTANCE.getHealth(s) <= 0)	{
			//jak umar� to zako�cz. 
			s = Game.getGame().playerDies(s);
			this.turnDuration = 0;
		}
		return s;
	}

}