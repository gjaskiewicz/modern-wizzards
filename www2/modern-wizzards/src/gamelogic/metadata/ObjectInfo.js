export default class ObjectInfo {	
  constructor(id, imageID, name, description, gameObjectClass) {
    this.id = id;
    this.imageID = imageID;
    this.objName = name;
    this.description = description;
    this.gameObjectClass = gameObjectClass;
  }

  getImageID() {
    return this.imageID;
  }

  getName() {
    return this.objName;
  }

  getDescription() {
    return this.description;
  }

  getID() {
    return this.id;
  }

  getObjectClass() {
    return this.gameObjectClass;
  }
	
  /**
   * używać z rozwagš, nie bedzie zawsze dzialalo. 
   * @return
   */
  newInstance() {
/* TODO: migrate
    try {
      Constructor ctor = gameObjectClass.getConstructor();
      return ctor.newInstance();
    } catch (SecurityException e) {
      e.printStackTrace();
    } catch (NoSuchMethodException e) {
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      e.printStackTrace();
    }
*/
    return null;
  }
}
