import ObjectInfoFactory from './ObjectInfoFactory.js';

describe('Object info factory', () => {
  it('should get standarized info', () => {
    const manaMeta = ObjectInfoFactory.get().getInfoStandarized("potion_mana");
    expect(manaMeta.getName()).toEqual("Mana potion");
    expect(manaMeta.getImageID()).toEqual("potion_mana.png");
    expect(manaMeta.getDescription()).toEqual("Highly energetizing drink that quickly restores Your mana level.");
    expect(manaMeta.getID()).toEqual("potion_mana");
  });

  it('should cache standarized info', () => {
    let manaMeta;
    for (let i = 0; i < 10; i++) {
      manaMeta = ObjectInfoFactory.get().getInfoStandarized("potion_mana");
      expect(manaMeta.getName()).toEqual("Mana potion");
      expect(manaMeta.getImageID()).toEqual("potion_mana.png");
      expect(manaMeta.getDescription()).toEqual("Highly energetizing drink that quickly restores Your mana level.");
      expect(manaMeta.getID()).toEqual("potion_mana");
    }
  });

  it('should get info for unknown', () => {
    const manaMeta = ObjectInfoFactory.get().getInfoStandarized("potion_ratamahata");
    expect(manaMeta.getName()).toEqual("!potion_ratamahata!");
    expect(manaMeta.getImageID()).toBeUndefined();
    expect(manaMeta.getDescription()).toEqual("!potion_ratamahata_desc!");
    expect(manaMeta.getID()).toEqual("potion_ratamahata");
  });
});
