import ObjectInfo from "./ObjectInfo.js";
import R from "../../R.js";
import $log from "../../utils/log.js";

export default class ObjectInfoFactory {
  static instance = new ObjectInfoFactory();

  constructor() {
    this.storedRes = { };
  }
	
  static get() {
    return ObjectInfoFactory.instance; 
  }

  createById(id, ids, cls) {
    return new ObjectInfo(
        id, ids[0], R.strings[ids[1]], R.strings[ids[2]], cls);
  }
	
 /**
  * Automatycznie zaczytuje resources dla podanej nazwy -> chodzi przede wszystkim o rosliny, 
  * stwory i mikstury/czary
  * TODO: odizolowac obrazki od nazwy i opisu
  * @param nameID
  * @return zmapowany ObjectInfo
  */
  getInfoStandarized(nameID) {
    //albo wyciagamy z zebranych info
    if(this.storedRes[nameID])	{
      //$log.d("OBJ INFO", "resource \"" + nameID + "\" exists!");
      return this.storedRes[nameID];
    }
    //albo generujemy i dodajemy
    $log.d("OBJ INFO", "generating info for : " + nameID );

    const description = 0;
    const picture = R.drawables[nameID] || R.drawables.unknown;

    //if(nameID.contains("potion")) {
    //  picture = ctx.getResources().getIdentifier("potion_uni", "drawable", ctx.getPackageName());
    //}

    const name = R.strings[nameID] || "!"+nameID+"!";
    const descr = R.strings[nameID+"_desc"] || "!"+nameID+"_desc!";
    const res = new ObjectInfo(nameID, picture, name, descr, null);
    this.storedRes[nameID] = res;
    return res;
  }	
}
