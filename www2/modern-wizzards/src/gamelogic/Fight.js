import React from 'react';
import Player from "./creatures/Player.js";
import Game from "../Game.js";
import PlayerEgo from "../PlayerEgo.js";
import R from "../R.js";
import $log from "../utils/log.js";
import MWToast from "../ui/MWToast.js";
import MWTime from '../utils/MWTime.js';

export default class Fight {
  // public FightActivity controller;
  
  constructor(fightingWith) {
    $log.d("FIGHT", "FIGHT STARTS");
    this.creature = fightingWith;
    //nr iteracji
    this.fightIteration = 1;
    /** oznacza czy walka jeszcze trwa czy juz sie skonczyla (ale stan gry jeszcze zostaje) */
    this.finished = false;
    this.pauseFight = false;

    // Activity topmost = Game.mainActivity;
    // topmost.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    // topmost.startActivity(new Intent(topmost, FightActivity.class));
    //controller = (FightActivity)Game.getGame().getGlobalTopmostActivity();
  }
  
  getFightIteration() {
    return this.fightIteration;
  }
  
  iterateFight(s)  {
    //$log.d("FIGHT", "fight iter!: "+ fightIteration);
    if(this.pauseFight) {
      return s;
    } else if(this.finished) {
      return s;
    } else {
      this.fightIteration++;
      this.creature.updateBehaviourPosition(this.fightIteration);
      s = this.takeEffects(s);
      s = this.creature.thinkFight(s, this)
      s = this.checkIfFightEnds(s);
      return s;
    }
  }
  
  playerCastsSpell(s, /* Spell */ spell)  {
    spell.setTarget(this.creature);
    const {state} = spell.castThis(s);
    return state;
  }
  
  creatureCastsSpell(s, /* Spell */ spell)  {
    const info = spell.getInfo();
    const text = R.strings.creature_cast_fc;
    MWToast.toast(text + " " + info.getName() + "!"); //, Toast.LENGTH_SHORT).show();
    
    spell.setTarget(Player.INSTANCE);
    console.log(spell.target);
    const {state} = spell.castThis(s);
    return state;
  }

  takeEffects(state) {
    for(let spell of this.creature.underEffectOfSpell) {
      state = spell.takeEffect(state);
    }
    state = this.creature.removeFinishedSpells(state);
    return state;
  }
  
  fightEnds(s) {
    $log.d("FIGHT", "FIGHT FINISH");
    if (s.ui.activeScreen === 'fight') {
      s = window.gameController.showBackScreenState(s);
    }
    return {
      ...s,
      ui: {
        ...s.ui,
        fight: undefined,
        fightCreature: undefined,
      }
    }
  }

  checkIfFightEnds(state) {
    if(Player.INSTANCE.getHealth(state) <= 0)  {
      return this.playerLost(state);
    }
    if(this.creature.getHealth(state) <= 0)  {
      return this.playerWon(state);
    }
    return state;
  }

  playerRunsFromTheFight(s)  {
    $log.d("FIGHT", "PLAYER RUNS FROM THE FIGHT!");
    let ego = s.playerEgo;
    ego = PlayerEgo.addHealthPoints(s.playerEgo, -20);
    s = PlayerEgo.addManaPoints({...s, playerEgo:ego}, -20);
    this.creature.dontAttackUntil = MWTime.s() + 30; // side-effect
    return this.fightEnds({
      ...s,
      gameState: Game.State.STATE_WALKING,
    });
  }

  showWinAlert(s) {
    let genericOverlayParams = {
      yesHandler: {
        title: R.strings.fight_winalert_ok,
        invoke: () => window.gameController.applyToState(
          s => {
            s = window.gameController.showOverlayState(s, null);
            s = this.fightEnds(s);
            s = this.winAlertConfirmed({...s, gameState: Game.State.STATE_WALKING});
            s = this.creature.die(s);
            return s;
          }
        ),
      },
      noHandler: { },
      infoOnly: true,
      renderContent: () => (<span>{R.strings.fight_winalert_desc}</span>),
    };
    const state = window.gameController.showOverlayState(s, "alert_generic");
    return {
      ...state,
      ui: {
        ...state.ui,
        genericOverlayParams
      }
    };
  }

  showLooseAlert(s) {
    let genericOverlayParams = {
      yesHandler: {
        title: R.strings.fight_lostalert_ok,
        invoke: () => window.gameController.applyToState(
          s => this.fightEnds(window.gameController.showOverlayState(s, null))
        ),
      },
      noHandler: { },
      infoOnly: true,
      renderContent: () => (<span>{R.strings.fight_lostalert_desc}</span>),
    };
    const state = window.gameController.showOverlayState(s, "alert_generic");
    return {
      ...state,
      ui: {
        ...state.ui,
        genericOverlayParams
      }
    };
  }
  
  playerWon(s) {
    $log.d("FIGHT", "PLAYER WINS");
    this.finished = true;
    return this.showWinAlert(s);
  }

  playerLost(s) {
    $log.d("FIGHT", "PLAYER HAS LOST AND DIED");
    this.finished = true;
    s = Game.getGame().playerDies(s);
    return this.showLooseAlert(s)
  }

  winAlertConfirmed(s) {
    let ego = {
      ...s.playerEgo,
      firstFightDone: true
    };
    s = {
      ...s,
      playerEgo: PlayerEgo.addExperience(ego, this.creature.getExperienceForDefeat())
    };
    return this.creature.giveContainingItem(s);
  }

  instructionConfirmed() {
    this.pauseFight = false;
  }
}
