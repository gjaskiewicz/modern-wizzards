import GeoPoint from "../gis/GeoPoint.js";
import Location from "../gis/Location.js";
import Units from "../gis/Units.js";
import MoveLocation from "../utils/MoveLocation.js";
import ObjectInfoFactory from "./metadata/ObjectInfoFactory.js";
import Color from "../Color.js";
import MapController from "../MapController.js";
import $log from "../utils/log.js";

const TAG = "PhysicalObject";

/**
 * Klasa bazowa dla wszystkiego co może znalezc sie na mapie. 
 * @author bartek
 */
export default class PhysicalObject { // extends EventEmiter {
  
  /** czy wygenerowany czy questowy*/
/*
  public boolean isGenerated;
  public LocationSlab homeLocation;
  public int generationHash;
  
  private transient Location location;
  private GeoPoint locationGP;

  //obszar wystepowania
  public transient SphericalZone zone;
  private transient boolean displayed;
  public transient Circle dispCircle ;
  protected transient OverlayItem vis;
  
  //TODO: usuń mnie, bylem potrzebny bo Questy dzialajš na innym wštku
  private transient PhysicalObject toDisplay;
  */

  constructor()  {
    this.nameID = null;
    this.displayed = false;
    this.locationGP =  null;
    this.dispCircle = null;
  }

  shouldInteract(/* GeoPoint */ gp) {
    return false;
  }


  interact() { }

  setLocation(gpOrloc) {
    if (gpOrloc instanceof GeoPoint) {
      const gp = gpOrloc;
      this.locationGP = gp;
      this.location = new Location("Object location");
      this.location.setLatitude(gp.getLatitudeE6() / 1E6);
      this.location.setLongitude(gp.getLongitudeE6() / 1E6);
      if (this.zone) { 
        this.zone.setCenter(this.locationGP); 
      }
      if(this.dispCircle) {
/* TODO: migrate
        dispCircle.setCenter(MWMap2Activity.convLocationToLatLng(getLocation()));
*/
      }
    } else { 
      const lc = gpOrloc;
      this.location = lc;
      this.locationGP = new GeoPoint(Math.floor(lc.getLatitude() * 1E6), Math.floor(lc.getLongitude() * 1E6));
      if (this.zone) {
        this.zone.setCenter(this.locationGP);
      }
    }
  }

  getGeoPoint() {
    return this.locationGP;
  }
  
  /**
   * Pokazuje lub ukrywa wizualizajce obiektu ma mapie googla
   * @param display czy ma byc widoczny czy nie.
   */
  display(display)  {
    if(display && !this.displayed)  {
      //createVis(MWMapActivity.getGameVisual());
      this.displayed = true;
      MapController.getInst().displayObj(this);
    }
    if(!display && this.displayed)  {
      //MainLoop.animations.remove(this);
      //deleteVis(MWMapActivity.getGameVisual());
      this.displayed = false;
      MapController.getInst().dontDisplayObj(this);
/* TODO: migrate
      if(dispCircle!=null)  {
        //$log.d(TAG, "Destroy circle...");
        Game.getGame().mainActivity.runOnUiThread(
            new Runnable() {
              @Override
              public void run() {
                dispCircle.remove();
              }
            }
        );
      }
*/
    }
  }

  updateDisp() {
    if (this.displayed) {
      MapController.getInst().update(this);
    }
  }

  getLocation()  {
    if(this.location==null)  {
      this.setLocation(this.locationGP);
    }
    return this.location;
  }
  
  getGeoPoint() {
    return this.locationGP;
  }
  
  moveObject(distance, bearing) {
    MoveLocation.moveLocation(this.location, distance, bearing);
    this.locationGP = MoveLocation.moveGeoPoint(this.locationGP, distance, bearing);
    this.zone.moveCenter(distance, bearing);
  }

  getInfo() {
    return ObjectInfoFactory.get().getInfoStandarized(this.nameID);
  }

  moveTowards(destination, metersPerIter)  {
    const currLoc = this.getGeoPoint();
    const latVectL = Math.floor(1.5 / Units.latitudeToMeters());
    const lngVectL = 
      Math.floor(1.5 / Units.longitudeToMeters(destination));
      //Math.floor(1.5 / Units.longitudeToMeters(Game.getGame().playerEgo.getGeoPoint().getLatitudeE6()));
    const latDist = destination.getLatitudeE6() - currLoc.getLatitudeE6();
    const lngDist = destination.getLongitudeE6() - currLoc.getLongitudeE6();
    let metDist = Units.optimizedDistance(destination, currLoc);
    if (metDist == 0) { 
      metDist = 1;
    }
    const latVect = Math.floor(latVectL * (Math.floor(latDist * Units.latitudeToMeters()) / metDist));
    const lngVect = Math.floor(lngVectL * (Math.floor(lngDist * Units.longitudeToMeters(destination.getLatitudeE6())) / metDist));
    this.setLocation(new GeoPoint(currLoc.getLatitudeE6() + latVect, currLoc.getLongitudeE6() + lngVect));
  }

  getColor()  {
    return Color.TRANSPARENT;
  }
/* TODO: migrate
  getLatLng()  {
    return MWMap2Activity.convLocationToLatLng(this.getLocation());
  }
  */
}
