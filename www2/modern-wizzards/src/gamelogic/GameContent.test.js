import GameContent from './GameContent.js';
import ObjectInfoFactory from './metadata/ObjectInfoFactory.js';
import R from '../R.js';

describe('GameContent', () => {
  it('all plants should have metadata', () => {
    for (let plant of GameContent.getInstance().getAllPlantsTypes()) {
      const plantMeta = ObjectInfoFactory.get().getInfoStandarized(plant.getNameID());
      expect(plantMeta.getName()[0]).not.toBe('!');
      expect(plantMeta.getImageID()).not.toBe(R.drawables.unknown);
      expect(plantMeta.getDescription()[0]).not.toBe('!');
      expect(plantMeta.getID()).toEqual(plant.getNameID());
    }
  });

  it('all creatures should have metadata', () => {
    for (let creature of GameContent.getInstance().getAllCreaturesTypes()) {
      const creatureMeta = ObjectInfoFactory.get().getInfoStandarized(creature.nameID);
      expect(creatureMeta.getName()[0]).not.toBe('!');
      expect(creatureMeta.getImageID()).not.toBe(R.drawables.unknown);
      expect(creatureMeta.getDescription()[0]).not.toBe('!');
      expect(creatureMeta.getID()).toEqual(creature.nameID);
    }
  });

  it('all potions should have metadata', () => {
    for (let potion of GameContent.getInstance().getAllPotionsTypes()) {
      const potionMeta = ObjectInfoFactory.get().getInfoStandarized(potion.nameID);
      expect(potionMeta.getName()[0]).not.toBe('!');
      expect(potionMeta.getImageID()).not.toBe(R.drawables.unknown);
      expect(potionMeta.getDescription()[0]).not.toBe('!');
      expect(potionMeta.getID()).toEqual(potion.nameID);
    }
  });
});
