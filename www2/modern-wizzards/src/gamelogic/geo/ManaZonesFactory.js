import GeoPoint from "../../gis/GeoPoint.js";
import RNG from "../../utils/RNG.js";
import ManaZone from "../world/ManaZone.js";
import MapObjectFactory from "./MapObjectFactory.js";

export default class ManaZonesFactory {

  static createZones(/* LocationSlab */ ls) {
    const gdh = MapObjectFactory.getGeoDateHash(ls.location) + 77;
    const r = new RNG(gdh);    
    const result = [];

    // ma by od 4 do 3 zonów i basta. 
    const zonesLimit = r.nextInt(1) + 3;
    for (var i = 0; i <= zonesLimit; i++) {
      //double rand = r.nextDouble();
      //if (rand < 0.1 ) {
      const gp = new GeoPoint(
        ls.location.getLatitudeE6() + Math.floor(r.nextDouble() * ls.latSpan),
        ls.location.getLongitudeE6() + Math.floor(r.nextDouble() * ls.lngSpan)
      );
      const mz = new ManaZone(gp, Math.floor(70 + (r.nextDouble()-0.5)*40));
      mz.isGenerated = true;
      mz.homeLocation = ls;
      result.push(mz);
    }    
    return result;  
  }
}

