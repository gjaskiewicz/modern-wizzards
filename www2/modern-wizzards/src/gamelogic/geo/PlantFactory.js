import GameContent from "../GameContent.js";
import GeoPoint from "../../gis/GeoPoint.js";
import RNG from "../../utils/RNG.js";
import MapObjectFactory from "./MapObjectFactory.js";
import StatsHelper from "../../utils/StatsHelper.js";
import CollectPlant from "../inventory/CollectPlant.js";
import CollectableItem from "../world/CollectableItem.js";
import SphericalZone from "../../gis/SphericalZone.js";

const PLANTS_SEED = 1;

export default class PlantFactory {
  static createObjects(state, /* LocationSlab */ ls) {
    const r = new RNG(MapObjectFactory.getGeoDateHash(ls.location) + PLANTS_SEED);
    const result = [];
    
    const plantsLimit = r.nextInt(6) + 17;
    for (var i = 0; i <= plantsLimit; i++)  {
      const plantSeed = r.nextInt(RNG.MAX_NUMBER); //Integer.MAX_VALUE);
      const plantRandSet = new RNG(plantSeed);
      const latitude = (ls.location.getLatitudeE6() + Math.floor((plantRandSet.nextDouble()) * ls.latSpan));
      const longitude = (ls.location.getLongitudeE6() + Math.floor((plantRandSet.nextDouble()) * ls.lngSpan));
      const gp = new GeoPoint(latitude, longitude);
      const cp = PlantFactory.getObject(state, plantRandSet);
      if(cp == null) {
        continue;
      }
      const cIt = new CollectableItem(
        new SphericalZone(gp, 30 + (plantRandSet.nextInt(15))),
        cp
      );
      cIt.isGenerated = true; 
      cIt.homeLocation = ls;
      cIt.generationHash = plantSeed;
      if(state.worldContainer.gatheredPlants.includes(plantSeed)) {
        // nie dodawaj - juz bylo
      } else {
        // dodaj
        result.push(cIt);
      }
    }    
    return result;
  }
  
  static getObject(state, /* RNG */ r)  {
    // tylko aby sie wygenerowały;
    const plants = GameContent.getInstance().getAllPlantsTypes();
    const cp = StatsHelper.randomFromDistr(
        plants,
        (p) => p.popularity,
        r.nextDouble());
    if(!state.playerEgo.knowledge.knownPlants[cp.getNameID()]) {
      // nieznana rozlinka
      return null;
    }
    return new CollectPlant(cp.getNameID());
  }  
}
