import $log from "../../utils/log.js";
import RNG from "../../utils/RNG.js";
import StatsHelper from "../../utils/StatsHelper.js";
import MapObjectFactory from "./MapObjectFactory.js";
import GameContent from "../GameContent.js";
import GeoPoint from "../../gis/GeoPoint.js";

const CREATURES_SEED = 7;

export default class CreaturesFactory extends MapObjectFactory {

  static createCreatures(state, /* LocationSlab */ ls) {
    const r = new RNG(MapObjectFactory.getGeoDateHash(ls.location) + CREATURES_SEED);  
    const result = [];
    const limit = 15;
    for (let i = 0; i <= limit; i++) {
      const creatureSeed = r.nextInt(RNG.MAX_NUMBER);
      const creatureRandSet = new RNG(creatureSeed);
      const creature = CreaturesFactory.getCreature(state, creatureRandSet);
      if(creature == null) {
        continue;
      }
      creature.isGenerated = true;
      creature.homeLocation = ls;
      creature.generationHash = creatureSeed;
      creature.zone.radius = (creatureRandSet.nextDouble() * 25 + 30);
      const gp = new GeoPoint(
          ls.location.getLatitudeE6() + Math.floor(creatureRandSet.nextDouble() * ls.latSpan),
          ls.location.getLongitudeE6() + Math.floor(creatureRandSet.nextDouble() * ls.lngSpan)
      );
      creature.setLocation(gp);
      if(state.worldContainer.creaturesDefeated.includes(creatureSeed)) {
        // nie dodawaj - juz bylo
      } else {
        // dodaj
        result.push(creature);
      }
    }
    return result;
  }

  static getCreature(state, /* RNG */ r) {
    //tylko aby sie wygenerowały;
    const creatures = GameContent.getInstance().getAllCreaturesTypes();
    const cp = StatsHelper.randomFromDistr(
        creatures,
        (c) => c.popularity,
        r.nextDouble());
    if(!state.playerEgo.knowledge.knownCreatures[cp.nameID]) {
      //stworek jeszcze nie znany, nie dodajemy. 
      return null;
    }
    return cp.newInstance();
  }  
}
