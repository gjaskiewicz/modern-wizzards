import LocationSlab from "./LocationSlab.js";
import MWTime from "../../utils/MWTime.js";

export default class MapObjectFactoryUtils {  
  static getGeoDateHash(/* GeoPoint */ point) {
    const geohash = LocationSlab.geoPointToSlabHash(point) * 10000;  
    const day = MWTime.day();
    //zapis seeda = xxxxyyyyddhh;
    const seed = geohash + day;
    return seed;
  }  
}
