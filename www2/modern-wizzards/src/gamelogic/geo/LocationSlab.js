import GeoPoint from "../../gis/GeoPoint.js";
import ManaZonesFactory from "./ManaZonesFactory.js";
import PlantFactory from "./PlantFactory.js";
import CreaturesFactory from "./CreaturesFactory.js";
import GameContent from "../GameContent.js";
import MainLoop from "../MainLoop.js";

/*
 * Reprezentuje wycinek świata
 */
export default class LocationSlab {

  //microstopnie geograficzne
  //10000 microstopni geog ~1,1 kilometra
  static SLAB_SIZE_LAT = 5000;
  //proporcja km / stopnie dlugosci geog sie zmienia z szerokoscia geograficzna 
  static SLAB_SIZE_LNG = 8000;

  /*
   * współrzedne w mikrostopniach
   */
  constructor(latID, lngID, hash) {
    this.slabHash = hash;
    // zakres dlugosci i szerokosci
    this.latSpan = LocationSlab.SLAB_SIZE_LAT;
    this.lngSpan = LocationSlab.SLAB_SIZE_LNG;
    // lewa dolna wspolrzedna wycinka w microstopniach
    this.location = new GeoPoint(latID, lngID); /* GeoPoint */
    this.generatedCreatures = false;
    this.generatedManazones = false;
    this.generatedPlants = false;
    /** wygenerowane stworki w tym segmencie*/
    /* transient */ this.creatures = [];
    /** wygenerowane itemy w tym segmencie*/
    /* transient */ this.items = [];
    /** wygenerowane manazony w tym segmencie*/
    /* transient */ this.manaZones = [];
  }	
	
  static newSlabForLocation(/* GeoPoint */ g) {
    const locLatID = LocationSlab.latitudeToHash(g.getLatitudeE6()) * LocationSlab.SLAB_SIZE_LAT;
    const locLngID = LocationSlab.longitudeToHash(g.getLongitudeE6()) * LocationSlab.SLAB_SIZE_LNG;
    const slabHash = LocationSlab.geoPointToSlabHash(g); 
    return new LocationSlab(locLatID, locLngID, slabHash);
  }
	
  static geoPointToSlabHash(/* GeoPoint */ g)	{
    const lat = Math.floor(LocationSlab.latitudeToHash(g.getLatitudeE6()) * 100000);
    const lng = Math.floor(LocationSlab.longitudeToHash(g.getLongitudeE6()));
    return lat + lng;
    //XXXXXYYYYY
  }
	
  static latitudeToHash(/* int */ lat) {
    return Math.floor(lat / LocationSlab.SLAB_SIZE_LAT);
  }
	
  static longitudeToHash(/* int */ lng) {
    return Math.floor(lng / LocationSlab.SLAB_SIZE_LNG);
  }
	
  getHash() {
    return this.slabHash;
  }

  regenerate(state) {
    //if(!Options.GENERATE_TEST_DATA) return;
    //System.out.println("GENERATING OBJECTS FOR NEW SLAB!");
    if(!this.generatedCreatures && Object.keys(state.playerEgo.knowledge.knownCreatures).length) {
      this.creatures = this.creatures.concat(CreaturesFactory.createCreatures(state, this));
      this.generatedCreatures = true;
    }
    if(!this.generatedPlants && Object.keys(state.playerEgo.knowledge.knownPlants).length) {
      this.items = this.items.concat(PlantFactory.createObjects(state, this));
      this.generatedPlants = true;
    }
    if(!this.generatedManazones && state.playerEgo.knowledge.generateManaZones) {
      this.manaZones = this.manaZones.concat(ManaZonesFactory.createZones(this));
      this.generatedManazones = true;
    }
  }

  getAllGeneratedObjects() {
    return [...this.creatures, ...this.items, ...this.manaZones];
  }
	
  /**
   * usuwa obiekcik z mapy, mniejsza o to co to jest.
   * @param po
   * @return
   */
  remove(/* PhysicalObject */ po) {
    po.display(false);
    // TODO: migrate (remove?) MainLoop.animations.remove(po);

    const oldCreatures = this.creatures;
    const oldManaZones = this.manaZones;
    const oldItems = this.items;

    this.creatures = this.creatures.filter(x => x != po);
    this.manaZones = this.manaZones.filter(x => x != po);
    this.items = this.items.filter(x => x != po);
    return (oldCreatures.length != this.creatures.length) ||
      (oldManaZones.length != this.manaZones.length) ||
      (oldCreatures.length != this.creatures.length);
  }
	
	/*public static List<LocationSlab> upperBoundSlabAreaCover(double latMin, double latMax, double lngMin, double lngMax) {
		List<LocationSlab> result = new LinkedList<LocationSlab>();
		
		long locLatIDMin = (long)Math.ceil(latMin / latSpan);
		long locLngIDMin = (long)Math.ceil(lngMin / lngSpan);
		
		long locLatIDMax = (long)Math.ceil(latMax / latSpan);
		long locLngIDMax = (long)Math.ceil(lngMax / lngSpan);
		
		for (long i=locLatIDMin;i<=locLatIDMax;i++)
			for (long j=locLngIDMin;j<=locLngIDMax;j++) {
				result.add(new LocationSlab(i, j));
			}
		
		return result;
	}
	
	public long getRandomSeed() {
		Date toDay = new Date();
		int year = toDay.getYear();
		int mnth = toDay.getMonth();
		int day = toDay.getDay();
		
		long sdDate = Long.parseLong(""+year+mnth+day);
		long sdLoc = 
				(2083 * latID) % 4638551 +
				(4001 * lngID) % 4638551;
		
		return (25183 * sdDate + 17021 * sdLoc) % 8769499;
	}
	
	public boolean hasPosition(double lat, double lng) {
		double myLat = getLat();
		double myLng = getLng();
		
		if (myLat <= lat && lat < myLat + latSpan) 
			if (myLng <= lng && lng < myLng + lngSpan) {
				return true;
			}
		
		return false;
	}*/
	
}
