import $log from "../../utils/log.js";
import Units from "../../gis/Units.js";
import Game from "../../Game.js";
import MainLoop from "../MainLoop.js";
import GeoPoint from "../../gis/GeoPoint.js";
import LocationSlab from "./LocationSlab.js";
import TriggerZone from "../world/TriggerZone.js";

const TAG = "WorldContainer";

//obiekt przechowuje i zarząca obiektami na mapie - jak trzeba, to je generuje itp. 
export default class WorldContainer {
  
  constructor()  {
    /** dzien ROKU, użyj Calendar.getInstance().getTime().getDate()*/
    this.saveDate = 1;
    this.locationSlabs = { }; // new HashMap<Long, LocationSlab>();
    this.gatheredPlants = [ ];
    this.creaturesDefeated = [ ];
    /** obiekty questowe, nie zwiazane ze Slabami! */
    this.otherObjects = [ ];
    this.allObjects = [ ];
  }
  
  /**
   * używac do dodawania obiektow questowych na mape.
   * @param obj
   * @param clearObjects czy czyscic mape z obiektów dookoła.
   */
  addQuestObject(/* PhysicalObject */ obj, clearObjects)  {
    if(clearObjects)  {
      for(let po of this.getAllGeneratedObjs())  {
        if(Units.optimizedDistanceCheck(po.getGeoPoint(), 
            obj.getGeoPoint(), po.zone.radius + obj.zone.radius))  {
          po.homeLocation.remove(po);
        }
      }
    }
    this.otherObjects.push(obj);
    obj.display(true);
  }
  
  /**
   * używac do uzuwania obiektow questowych z mapy.
   * @param obj
   */
  removeQuestObject(/* PhysicalObject */ obj)  {
    this.otherObjects = this.otherObjects.filter(o => o != obj);
    // TODO: migrate  MainLoop.animations.remove(obj);
    obj.display(false);
  }
  
  /**
   * Wszystkie generowane obiekty ORAZ questowe obiekty. Użyj do czarów, interakcji itp.
   * @return
   */
  getAllObjects()  {
    const allObj = this.getAllGeneratedObjs();
    //allObj.addAll(Game.getGame().worldContainer.otherObjects);
    return allObj.concat(Game.getGame().worldContainer.otherObjects);
  }
  
  /**
   * zwraca liste wszystkich generowanych obiektów na mapie
   * @return
   */
  getAllGeneratedObjs(){
    let allObjects = [];
    for(let lsKey of Object.keys(this.locationSlabs)) {
      const ls = this.locationSlabs[lsKey];
      allObjects = allObjects.concat([...ls.creatures, ...ls.items, ...ls.manaZones])
    }
    return allObjects;
  }
  
  /**
   * zapelnia mape w slabie danego punktu i przyleglych lokalizacjach.
   * @param g - dla jakiego punktu generować. 
   */
  generateObjects(state, /* GeoPoint */ g)  {
    //dla wszystkich sasiednich "kwadratów"
    var newSlabs = { }; // new HashMap<Long, LocationSlab>();

    const sizeLat = LocationSlab.SLAB_SIZE_LAT / 1E6;
    const sizeLng = LocationSlab.SLAB_SIZE_LNG / 1E6;
    for (let loc of [
      new GeoPoint(g.getLatitude()+sizeLat, g.getLongitude()+sizeLng),
      new GeoPoint(g.getLatitude()+sizeLat, g.getLongitude()),
      new GeoPoint(g.getLatitude()+sizeLat, g.getLongitude()-sizeLng),
      new GeoPoint(g.getLatitude(), g.getLongitude()+sizeLng),
      g,
      new GeoPoint(g.getLatitude(), g.getLongitude()-sizeLng),
      new GeoPoint(g.getLatitude()-sizeLat, g.getLongitude()+sizeLng),
      new GeoPoint(g.getLatitude()-sizeLat, g.getLongitude()),
      new GeoPoint(g.getLatitude()-sizeLat, g.getLongitude()-sizeLng)
    ]) {
      this.generateObjectsForSlab(state, newSlabs, loc);
    }

    //zapewnia ze dalsze obiekty napewno znikną z mapy. 
    for(let lsKey of Object.keys(this.locationSlabs)) {
      const ls = this.locationSlabs[lsKey];
      if(!newSlabs[ls.getHash()])  {
        //$log.d(TAG,"Chowam obiektYYYYY bez slabu");
        for(let /* PhysicalObject */ po of ls.getAllGeneratedObjects())  {
          po.display(false);
          //$log.d(TAG,"Chowam obiekt bez slabu");
        }
      }
    }
    this.locationSlabs = newSlabs;
  }
  
  /**
   * generuje mape dla konkretnego Slaba (segmentu mapy)
   */
  /* private */ generateObjectsForSlab(state, newSlabs, g)  {
    const slabHash = LocationSlab.geoPointToSlabHash(g);
    if(this.isMapGeneratedForLocation(g))  {
      //na wypadek "aktualizacji"
      const oldOne = this.locationSlabs[LocationSlab.geoPointToSlabHash(g)];
      oldOne.regenerate(state); 
      newSlabs[slabHash] = oldOne;
      return false; //nie ma potrzeby, juz wygenerowane
    } else {
      const ls = LocationSlab.newSlabForLocation(g);
      ls.regenerate(state);
      newSlabs[slabHash] = ls;
      //locationSlabs.put(slabHash, ls);
      return true;
    }
  }
  
  /**
   * sprawdza czy generowano juz mape dla tego segmentu
   */
  isMapGeneratedForLocation(/* GeoPoint */ g)  {
    const hash = LocationSlab.geoPointToSlabHash(g);
    return this.locationSlabs[hash];
  }
  
  getTriggerZones()  {
    return this.otherObjects.filter(
      o => (o instanceof TriggerZone)
    );
  }

  /** uwaga - KLON */
  getAllQuestObjets() {
    return [...this.otherObjects];
  }
}
