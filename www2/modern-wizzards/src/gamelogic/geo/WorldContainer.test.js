import GeoPoint from '../../gis/GeoPoint.js';
import Units from '../../gis/Units.js';
import WorldContainer from './WorldContainer.js';
import GameController from '../../main.js';

describe('WorldContainer', () => {
  const warsaw = new GeoPoint(52.233333, 21.016667);

  it('should not generate objects with zero knowledge', () => {
    const w = new WorldContainer();
    const state = new GameController().defaultState();
    w.generateObjects(state, warsaw);
    expect(w.getAllGeneratedObjs().length).toBe(0);
    expect(Object.keys(w.locationSlabs).length).toBe(9);
  });

  it('should generate objects with plant knowledge', () => {
    const w = new WorldContainer();
    const state = new GameController().defaultState();
    state.playerEgo.knowledge = { // a.k.a GameContent
  isAlchemyActive: true,
  knownCreatures: { },
  knownPlants: {
    "item_daisyflower": true,
    "item_fernflower": true},
  knownPotions: { },
  generateManaZones: false,
    };
    w.generateObjects(state, warsaw);
    expect(w.getAllGeneratedObjs().length).not.toBeLessThan(2);
    expect(Object.keys(w.locationSlabs).length).toBe(9);
  });

  it('should generate mana zones', () => {
    const w = new WorldContainer();
    const state = new GameController().defaultState();
    state.playerEgo.knowledge = { // a.k.a GameContent
  isAlchemyActive: true,
  knownCreatures: { },
  knownPlants: { },
  knownPotions: { },
  generateManaZones: true ,
    };
    w.generateObjects(state, warsaw);
    expect(w.getAllGeneratedObjs().length).not.toBeLessThan(2);
    expect(Object.keys(w.locationSlabs).length).toBe(9);
  });

  it('should generate cretures with creature knowledge', () => {
    const w = new WorldContainer();
    const state = new GameController().defaultState();
    state.playerEgo.knowledge = { // a.k.a GameContent
      isAlchemyActive: true,
      knownCreatures: { "creature_bunny": true },
      knownPlants: { },
      knownPotions: { },
      generateManaZones: false ,
    };
    w.generateObjects(state, warsaw);
    expect(w.getAllGeneratedObjs().length).not.toBeLessThan(2);
    expect(Object.keys(w.locationSlabs).length).toBe(9);
  });
});
