import PhysicalObject from "../PhysicalObject.js";
import $log from "../../utils/log.js";
import MWToast from "../../ui/MWToast.js";
import SphericalZone from "../../gis/SphericalZone.js";
import R from "../../R.js";
import MWMath from "../../utils/MWMath.js";
import MWTime from "../../utils/MWTime.js";
import Game from "../../Game.js";
import GameState from "../../GameState.js";
import Player from "../creatures/Player.js";
import EventSystem from "../events/EventSystem.js";
import EventTypes from "../events/EventTypes.js";
import PlayerEgo from "../../PlayerEgo.js";

const TAG = "ManaZone";  

export default class ManaZone extends PhysicalObject {
  
  static manaZoneID = "ManaZoneID";
  static MANAZONE_COLOR = 0x809999FF;
  static effectTriggerTime = 0;

  constructor(/* GeoPoint */ gp, radius) {
    super();
    this.setLocation(gp);
    this.zone = new SphericalZone(gp, radius);
    this.firstEntered = false;
    this.isPlayerInside = false;
    this.alpha = 100;
    this.lastUpdateTimeS = 0;
  }

  shouldInteract(/* GeoPoint */ gp) {
    const intersects = this.zone.intersects(gp);
    if(intersects && !this.isPlayerInside)  {
      //gracz wlazł
      this.isPlayerInside = true;
      this.lastUpdateTimeS = MWTime.s();
      //animuj!
      //dispCircle.setFillColor(getColor());
      
      MWToast.toast(R.strings.player_mana_charging);
    } else if (!intersects && this.isPlayerInside) {
      //gracz wylazł
      this.isPlayerInside = false;
      Game.getGame().getAchivContainer().getAchivMedit().stop();
      //nie animuj!
    }
    return intersects;
  }

  interact(state) {
    // const p = state.playerEgo;
    if(!this.firstEntered) {
      this.firstEntered = true;
      state = EventSystem.INSTANCE.notifyListenersState(
          state, ManaZone.manaZoneID, EventTypes.VISITED);
    }
    if(state.gameState == GameState.STATE_PLAYERDEAD)  {
      state = PlayerEgo.resurect(state);
    }
    const now = MWTime.s();
    const dt = Math.max(0, now - this.lastUpdateTimeS) || 0;
    this.lastUpdateTimeS = now;
    if(!Player.INSTANCE.isUnderEffectOfSpell(state, "spell_oob"))  {
      state = PlayerEgo.addManaPoints(state, state.playerEgo.manaRegenFactor);
      state = Game.getGame().getAchivContainer().getAchivMedit().checkMedit(state, dt);
      this.tryTriggerEffect();
      return state;
    }
    return state;
  }

  tryTriggerEffect() {
    const now = (new Date()).getTime();
    if (ManaZone.effectTriggerTime + 5500 < now) {
      ManaZone.effectTriggerTime = now;
      window.effects.addEffect('charge-mana', 5000);
    }
  }

  getColor() {
    if(!this.isPlayerInside) {
      return ManaZone.MANAZONE_COLOR;
    } else {
      this.alpha += 7;
      const color = Math.floor((MWMath.sin(this.alpha)+1)*68+88) * 0x01000000 + 0x009999FF;
      return color;
    }
  }
}
