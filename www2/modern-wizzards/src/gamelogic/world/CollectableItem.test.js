import CollectableItem from './CollectableItem.js';
import SphericalZone from '../../gis/SphericalZone.js';
import Game from '../../Game.js';
import GameState from '../../GameState.js';
import GeoPoint from '../../gis/GeoPoint.js';
import MainLoop from '../MainLoop.js';
import CollectPlant from '../inventory/CollectPlant.js';
import GameController from '../../main.js';
import GameSaver from '../../utils/GameSaver.js';

import TestData from '../../test/TestData.js';
import FakeMapController from '../../test/FakeMapController.js';

describe('CollectableItem', () => {
  const mapController = new FakeMapController();
  mapController.init();
  window.gameController = new GameController();
  window.effects = {addEffect: () => undefined};
  GameSaver.disable();

  it('should be collectable', () => {
    const gp = new GeoPoint(...TestData.cities.radom);
    const item = new CollectableItem(
      new SphericalZone(gp, 30),
      new CollectPlant("item_fernflower"));
    const mainLoop = new MainLoop();
    let s = {
      playerEgo: {
        ...TestData.getFullEgo(),
        mana: 0,
        inventory: [ ],
      },
      gameState: GameState.STATE_WALKING,
      achivments: { 
        granted: [ ],
      },
      worldContainer: {
        gatheredPlants: [ ],
        creaturesDefeated: [ ],
      },
      ui: { },
    };

    Game.getGame().worldContainer.addQuestObject(item, true);

    for(let i = 0; i < 25; i++) {
      s = mainLoop.iterateGameState(s);
    }
    expect(s.ui.activeOverlay).toBe('alert_item');
    expect(s.ui.alertItem).toBe(item);
    expect(Game.getGame().worldContainer.getAllQuestObjets().length).toBe(1);
    expect(s.playerEgo.experience).toBe(0);

    s = Game.getGame().collectItem(s, item);

    expect(s.playerEgo.mana).toBeGreaterThan(0);
    expect(s.playerEgo.inventory.length).toBe(1);
    expect(s.playerEgo.experience).toBeGreaterThan(0);
    expect(Game.getGame().worldContainer.getAllQuestObjets().length).toBe(0);
  });
});
