import PhysicalObject from "../PhysicalObject.js";
import $log from "../../utils/log.js";
import SphericalZone from "../../gis/SphericalZone.js";
import R from "../../R.js";
import EventSystem from "../events/EventSystem.js";
import EventTypes from "../events/EventTypes.js";

const TAG = "TriggerZone";


export default class TriggerZone extends PhysicalObject {
  
  static COLOR = 0xD0D0D0D0;
  
  constructor(/* String */ ID, gp, rad, label_) {
    super();
    this.zoneID = ID;
    this.label = label_ || R.strings.trigger
    this.setLocation(gp);
    this.zone = new SphericalZone(gp, rad);
  }
  
  shouldInteract(gp) {
    return this.zone.intersects(gp);
  }

  interact(state) {
    state = EventSystem.INSTANCE.notifyListenersState(state, this.zoneID, EventTypes.VISITED);
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      window.gameController.applyToState(s => 
        EventSystem.INSTANCE.notifyListenersState(s, this.zoneID, EventTypes.EXITED));
    }, 3000);
    return state;
  }

  getColor() {
    return TriggerZone.COLOR;
  }
}
