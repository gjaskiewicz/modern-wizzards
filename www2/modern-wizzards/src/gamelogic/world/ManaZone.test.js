import ManaZone from './ManaZone.js';
import Game from '../../Game.js';
import GameState from '../../GameState.js';
import GeoPoint from '../../gis/GeoPoint.js';
import MainLoop from '../MainLoop.js';

import TestData from '../../test/TestData.js';
import FakeMapController from '../../test/FakeMapController.js';

describe('ManaZone', () => {
  const mapController = new FakeMapController();
  mapController.init();
  window.effects = {addEffect: () => undefined};

  it('should charge mana', () => {
    const mz = new ManaZone(new GeoPoint(
      ...TestData.cities.radom), 
      40
    );
    const mainLoop = new MainLoop();
    let s = {
      playerEgo: {
        ...TestData.getFullEgo(),
        mana: 0,
      },
      gameState: GameState.STATE_WALKING,
      achivments: { 
        granted: [ ],
      },
      worldContainer: {
        gatheredPlants: [ ],
        creaturesDefeated: [ ],
      },
    };

    Game.getGame().worldContainer.addQuestObject(mz, true);

    for(let i = 0; i < 25000; i++) {
      s = mainLoop.iterateGameState(s);
    }
    expect(s.playerEgo.mana).toBeGreaterThan(0);
    expect(s.achivments.granted).toContain('achiv_medit');
  });

  it('should resurrect player', () => {
    const mz = new ManaZone(new GeoPoint(
      ...TestData.cities.radom), 
      40
    );
    const mainLoop = new MainLoop();
    let s = {
      playerEgo: {
        ...TestData.getFullEgo(),
        mana: 0,
        health: 0,
      },
      gameState: GameState.STATE_PLAYERDEAD,
      achivments: { 
        granted: [ ],
      },
      worldContainer: {
        gatheredPlants: [ ],
        creaturesDefeated: [ ],
      },
    };

    Game.getGame().worldContainer.addQuestObject(mz, true);

    for(let i = 0; i < 25; i++) {
      s = mainLoop.iterateGameState(s);
    }
    expect(s.playerEgo.mana).toBeGreaterThan(0);
    expect(s.playerEgo.health).toBeGreaterThan(0);
    expect(s.gameState).toBe(GameState.STATE_WALKING);
  });
});
