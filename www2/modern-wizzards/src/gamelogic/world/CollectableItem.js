import $log from "../../utils/log.js";
import R from "../../R.js";
import GameState from "../../GameState.js";
import VibratorUtil from "../../utils/VibratorUtil.js";
import PhysicalObject from "../PhysicalObject.js";
import Creature from "../Creature.js";
import EventSystem from "../events/EventSystem.js";
import EventTypes from "../events/EventTypes.js";

const TAG = "CollectableItem";

export default class CollectableItem extends PhysicalObject {
  
  constructor(zone, item) {
    super();
    this.zone = zone;
    this.item = item;
    this.lastCheckVisited = false;
    this.playerWasUninterested = false;
    this.runningDialog = false;
    if(zone != null) {
      this.setLocation(zone.getCenter());
    }
  }

  shouldInteract(/* GeoPoint */ gp) {
    if(this.zone.intersects(gp))  {
      //cišgle jest w zonie i nie chciał.
      if(this.lastCheckVisited == true && this.playerWasUninterested) {
        return false;
      } else {
        this.lastCheckVisited = true;
        return true;
      }
    } else {
      this.lastCheckVisited = false;
      return false;
    }

    return false;
  }

  interact(state) {
    if (state.ui.activeOverlay || state.gameState != GameState.STATE_WALKING) {
      return state;
    }

    $log.d("PLANT", "Plant Interacts: " + this.item.nameID);
    const vPatern = [0, 300];
    VibratorUtil.vibrate(vPatern, -1);
    
    state = window.gameController.showOverlayState(state, 'alert_item');
    state = {...state, ui: {...state.ui, alertItem: this } };
    return state;
  }
  
  getItem() {
    return this.item;
  }
  
  getInfo() {
    return this.item.getInfo();
  }  
  
  collected(state) {
    return EventSystem.INSTANCE.notifyListenersState(
        state, this.item.getNameID(), EventTypes.COLLECTED);
  }
  
  getColor() {
    return this.item.getColor();
  }

  getItem() {
    return this.item;
  }

}
