

export default class Player {

  constructor(gameController) {
    this.gameController = gameController;
  }

  getSightRange() {
    if(isUnderEffectOfSpell("potion_perception")) { return 550; }
    return sightRange;
  }	
	
  setSightRange(range) {
    this.gameController.applyToEgo(e => ({...e, sightRange: range}));
  }
      
  addKnownSpell(spellID) {
    this.gameController.applyToEgo(e => {
      if (e.knownGlobalSpells[spellID]) {
        return e;
      };
      return {
        ...e,
        knownGlobalSpells: {
          ...e.knownGlobalSpells,
          [spellID]: true,
        }
      };
    });
  }

  getKnownSpells(playerEgo) {
    return playerEgo.knownGlobalSpells;
  }

  /*
  die() {
		notifyListeners(nameID, EventTypes.KILLED, null);
		Game.getGame().setGameState(GameState.STATE_PLAYERDEAD);
		underEffectOfSpell = new LinkedList<Spell>();
		manaPoints=0;
		if(!killedOnce) {
			killedOnce = true;
			
			Intent i = new Intent(Game.mainActivity, MessageViewActivity.class);
			//i.putExtra("title", Game.mainActivity.getResources().getString(R.string.resuraction));
			i.putExtra("message", Game.mainActivity.getResources().getString(R.string.first_player_death));
			i.putExtra(MessageViewActivity.MESSAGE_TYPE, MessageViewActivity.MESSAGE_TYPE_NARRATION);
			i.putExtra(MessageViewActivity.MESSAGE_CANCLOSE, true);
			i.putExtra("readOnly",true);
			Game.mainActivity.startActivity(i);
		}
		
		Game.saveGame();
	}
  */

  endOutOfBody(boolean immediate) {
    for( Spell spell : underEffectOfSpell) {
      if("spell_oob".equals(spell.nameID))
        ((OutOfBodyExperience)spell).forceReturnToBody(immediate); //setting return to body
      }
    }
  }
}
