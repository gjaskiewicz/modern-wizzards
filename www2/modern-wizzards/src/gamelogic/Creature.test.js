import Bunny from './creatures/Bunny.js';
import Player from './creatures/Player.js';
import BadLuck from './spells/BadLuck.js';
import TestData from '../test/TestData.js';

describe('Creature', () => {

  it('should be under effect of spell', () => {
    let s = {
      playerEgo: TestData.getFullEgo(),
      ui: { },
    };
    const bunny = new Bunny();
    const spell = new BadLuck(Player.INSTANCE);
    spell.setTarget(bunny);
    const {state, success} = spell.castThis(s);
    expect(success).toBe(true);
    expect(bunny.isUnderEffectOfSpell(state, spell.nameID)).toBe(true);
  });

});