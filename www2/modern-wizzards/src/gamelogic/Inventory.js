import $log from '../utils/log.js';

const TAG = "Inventory";

export default class Inventory {

  static inst = new Inventory();

  static get() {
    return Inventory.inst;
  }

  addPlant(inventory, itemID) {
    if (inventory.some(i => i.name == itemID)) {
      return inventory.map(item => (
        item.name != itemID ? item : {...item, count: (item.count || 0) + 1}
      ));
    } else {
      return [...inventory, {name: itemID, count: 1}];
    }
  }

  removePlant(inventory, itemID) {
    if (inventory.some(i => i.name == itemID)) {
      if (inventory.some(i => i.name == itemID && i.count <= 1)) {
        return inventory.filter(item => item.name !== itemID);
      } else {
        return inventory.map(item => (
          item.name != itemID ? item : {...item, count: (item.count || 0) - 1}
        ));
      }
    } else {
      $log.e(TAG, `Removing plant ${itemID} from aggregation plants collection found problem with not existing aggregation object`);
      return [...inventory];
    }
  }

  hasItem(inventory, itemID) {
    return !!inventory.some(i => i.name == itemID);
  }

  removeItem(inventory, itemID) {
    let needFilter = true;
    return inventory.filter(i => !needFilter || (needFilter = (i.name != itemID)));
  }
}
