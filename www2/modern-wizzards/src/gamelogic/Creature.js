import PhysicalObject from "./PhysicalObject.js";
import SphericalZone from "../gis/SphericalZone.js";
import SpellFactory from "./SpellFactory.js";
import GameState from "../GameState.js";
import $log from "../utils/log.js";
import VibratorUtil from "../utils/VibratorUtil.js";
import MWTime from "../utils/MWTime.js";
import EventSystem from "./events/EventSystem.js";
import EventTypes from "./events/EventTypes.js";
import FightBehaviour from "./FightBehaviour.js";
import CollectableItem from "./world/CollectableItem.js";
import CollectPlant from "./inventory/CollectPlant.js";
import GameSaver from "../utils/GameSaver.js";
import PlayerEgo from "../PlayerEgo.js";

export default class Creature extends PhysicalObject {

  constructor() {
    super();
    this.zone = new SphericalZone(null, 50);
    this.knownFightSpells = [ ];
    this.aggresivness = 0.0;
    this.dontAttackUntil = 0;
    this.health = 0;
    this.maxHealth = 0;
    this.mana = 0;
    this.maxMana = 0;
    this.underEffectOfSpell = [ ];
    this.fightBehaviour = new FightBehaviour();
    this.containsItem = null;
  }

  getMana(state) {
    return this.mana
  }

  getHealth(state) {
    return this.health;
  }

  addMana(state, mana) {
    this.mana = Math.min(Math.max(0, this.mana + mana), this.maxMana);
    return state;
  }

  addHealth(state, hp) {
    this.health = Math.min(Math.max(0, this.health + hp), this.maxHealth);
    return state;
  }

  addUnderEffectOfSpell(state, spell) {
    this.underEffectOfSpell.push(spell);
    return state;
  }

  removeFinishedSpells(state) {
    this.underEffectOfSpell = 
        this.underEffectOfSpell.filter(spell => spell.turnDuration > 0);
    return state;
  }

  getColor()  {
    return 0xA8FF8800 - 0x00000100 * Math.floor(this.aggresivness * 136);
  }
  
  getExperienceForDefeat()  {
    let pop = this.popularity;
    return 1500 / Math.max(pop, 2);
  }

  getBehaviourPosition() {
    return this.fightBehaviour.getBehaviourPosition();
  }

  getBehaviourOpacity() {
    return this.fightBehaviour.getBehaviourOpacity();
  }
  
  shouldInteract(gp)  {
    return this.zone.intersects(gp); // || this.displayed;
  }
  
  interact(state) {      
    if (state.gameState != GameState.STATE_WALKING)  {
      $log.d("CREATURE", "Creature: no interaction, player is not 'walking'");
      return state;
    }
    if (this.dontAttackUntil > MWTime.s())  {
      return state;
    }
    /* TODO: test */ 
    if (PlayerEgo.isUnderEffectOfSpell(state.playerEgo, "potion_invisibility")) {
      return state; //a przynajmniej nie atakuj
    }
    if (state.ui.activeOverlay) {
      $log.d("CREATURE", "Creature: sorry, runningDialog");
      return state;
    }

    $log.d("CREATURE", "Creature Interacts: " + this.nameID);
    VibratorUtil.vibrate([0, 300, 100, 300], -1);

    state = {...state, ui: {...state.ui, fightCreature: this } };
    if (!this.attackWithoutWarning) {
      state = window.gameController.showOverlayState(state, 'alert_creature');
    } else {
      state = window.game.playerFightsCreature(state, this);
    }
    return state;
  }

  thinkFight(state, fight) {
    // creature thinks... może ucieka, moze wykorzystuje miksturę, moze sprawdza efekty, strategie?
    //rzuca 1 na 10 sekund jesli moze. 
    if(fight.getFightIteration() % (25 * 3) == 0)  {
      const whichSpell = Math.floor((Math.random() * this.knownFightSpells.length));
      state = fight.creatureCastsSpell(state,
          this.castSpell(this.knownFightSpells[whichSpell]));
    }
    return state;
  }

  castSpell(spellID) {
    return SpellFactory.getSpellByID(spellID, this);
  }

  die(state) {
    let worldContainer = {...state.worldContainer};
    if(this.isGenerated) {
      this.homeLocation.creatures = this.homeLocation.creatures.filter(i => i !== this);
      const day = MWTime.day();
      if(worldContainer.saveDate != day)  {
        worldContainer = {
          saveDate: day,
          gatheredPlants: [ ],
          creaturesDefeated: [ ],
        };
      }
      worldContainer = {
        ...worldContainer,
        creaturesDefeated: [...worldContainer.creaturesDefeated, this.generationHash],
      }
      state = {
        ...state,
        worldContainer
      };
      GameSaver.saveGame(state);
    } else {
      //TODO: tutaj tez usuwac? na wszelki wypadek
      window.game.worldContainer.removeQuestObject(this);
    }
    state = EventSystem.INSTANCE.notifyListenersState(state, this.nameID, EventTypes.KILLED);
    state = window.game.getAchivContainer().getAchivHunter().addCreatureDefeated(state, this.nameID);
    this.display(false);
    // TODO: migrate   Game.getGame().invalidateMapView();

    return state;
  }

  isUnderEffectOfSpell(state, spellId) {
    return this.underEffectOfSpell.some(s => s.nameID == spellId);
  }

  giveContainingItem(state) {
    if (this.containsItem) {
      return new CollectableItem(
        null,
        new CollectPlant(this.containsItem)).interact(state);
    } else {
      return state;
    }
  }

  isPlayer() {
    return false;
  }

  newInstance() {
    return new this.constructor;
  }
}
