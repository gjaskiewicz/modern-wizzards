import CollectPlant from "./inventory/CollectPlant.js";
import Bunny from "./creatures/Bunny.js";
import Bat from "./creatures/Bat.js";
import BlackCat from "./creatures/BlackCat.js";
import Strumpf from "./creatures/Strumpf.js";
import Wraith from "./creatures/Wraith.js";
import Gnome from "./creatures/Gnome.js";
import Golem from "./creatures/Golem.js";
import Warewolf from "./creatures/Warewolf.js";
import HealingPotion from "./mixtures/HealingPotion.js";
import AntidotePotion from './mixtures/AntidotePotion.js';
import InvisibilityPotion from './mixtures/InvisibilityPotion.js';
import PerceptionPotion from './mixtures/PerceptionPotion.js';
import LuckPotion from './mixtures/LuckPotion.js';
import ManaPotion from "./mixtures/ManaPotion.js";
import StimulationPotion from "./mixtures/StimulationPotion.js";

/**
 * Przechowuje info o tym co gracz wie, umie, jakie funkcje w grze ma dostępne.
 * Bardzo do serializacji. 
 * @author bartek
 */
export default class GameContent {

  constructor() {
    this.allCreatures = [];
    this.allPotions = [];
    this.allPlants = [];
  }
  
  getAllPlantsTypes() {
    if(this.allPlants.length) {
      return this.allPlants;
    }
    
    let tempPlant = new CollectPlant("item_daisyflower");
    tempPlant.popularity = 10;
    this.allPlants.push(tempPlant);
    
    tempPlant = new CollectPlant("item_fernflower");
    tempPlant.popularity = 3;
    this.allPlants.push(tempPlant);
    
    tempPlant = new CollectPlant("item_rozkovnik");
    tempPlant.popularity = 7;
    this.allPlants.push(tempPlant);
    
    tempPlant = new CollectPlant("item_szczeciogon");
    tempPlant.popularity = 5;
    this.allPlants.push(tempPlant);
    
    tempPlant = new CollectPlant("item_cornerstone");
    tempPlant.popularity = 5;
    this.allPlants.push(tempPlant);
    
    tempPlant = new CollectPlant("item_4clover");
    tempPlant.popularity = 8;
    this.allPlants.push(tempPlant);
    
    tempPlant = new CollectPlant("item_mandragora");
    tempPlant.popularity = 4;
    this.allPlants.push(tempPlant);
    
    tempPlant = new CollectPlant("item_garlic");
    tempPlant.popularity = 9;
    this.allPlants.push(tempPlant);
    
    tempPlant = new CollectPlant("item_swallowweed");
    tempPlant.popularity = 8;
    this.allPlants.push(tempPlant);

    return this.allPlants; 
  }
  
  /**
   * Odblokowuje wszystko
   */
  setAllAllowed()  {
    let knownCreatures = { };
    this.getAllCreaturesTypes().forEach(c => knownCreatures[c.nameID] = true);
    let knownPlants = { };
    this.getAllPlantsTypes().forEach(c => knownPlants[c.getNameID()] = true);
    this.gameController.applyToEgo(e => ({
      ...e,
      setSightRange: 500,
      knownGlobalSpells: {
        "spell_oob": true,
        "spell_telekinesis": true
      },
      knownFightSpells: {
        "spell_fireball": true,
        "spell_astralshield": true,
        "spell_electrokinesis": true,
      },
      knowledge: {
        ...e.knowledge,
        isAlchemyActive: true,
        generateManaZones: true,
        knownCreatures,
        knownPlants,
        knownPotions: {
          "potion_health": true, 
          "potion_mana": true, 
          "potion_luckjelly": true, 
          "potion_invisibility": true, 
          "potion_antidote": true, 
          "potion_stimulation": true, 
          "potion_perception": true
        },
      },
    }));
  }
  
  /**
   * Zwraca listę wszystkich TYPÓW stworków istniejacych w grze, jako referencję np do generacji mapy
   * oraz do Bestiariusza. NIE MODYFIKOWAĆ WYNIKÓW!
   * @return jak w opisie
   */
  getAllCreaturesTypes() {
    if(this.allCreatures.length) {
      return this.allCreatures;
    }
    
    this.allCreatures.push(new Bunny());
    this.allCreatures.push(new Bat());
    this.allCreatures.push(new BlackCat());
    this.allCreatures.push(new Gnome());
    this.allCreatures.push(new Strumpf());
    this.allCreatures.push(new Wraith());
    this.allCreatures.push(new Golem());
    this.allCreatures.push(new Warewolf());

    return this.allCreatures;
  }
  
  /**
   * Zwraca listę wszystkich TYPÓW MIKSTUR istniejacych w grze, jako referencję np do receptur
   * NIE MODYFIKOWAĆ WYNIKÓW!
   * @return jak w opisie
   */
  getAllPotionsTypes() {
    if(this.allPotions.length) {
      return this.allPotions;
    }

    this.allPotions.push(new HealingPotion());
    this.allPotions.push(new ManaPotion());
    this.allPotions.push(new AntidotePotion());
    this.allPotions.push(new InvisibilityPotion());
    this.allPotions.push(new PerceptionPotion());
    this.allPotions.push(new StimulationPotion());
    this.allPotions.push(new LuckPotion());

    return this.allPotions;
  }  

  static getInstance() {
    if(!window.gameContent)
      window.gameContent = new GameContent();
    return window.gameContent;
  }
  
  static reloadGameContent(gc) {
    window.gameContent = gc;
  }
  
  getPotionsForIngredients(ing1, ing2, ing3)  {
    const res = [];
    
    for(let sp of this.getAllPotionsTypes())  {
      if(sp.canMakeFromIng(ing1, ing2, ing3))
        res.push(sp);
    }
      
    return res;
  }
}
