import ObjectInfoFactory from "../metadata/ObjectInfoFactory.js";

export default class CollectPlant {

  constructor(nameID) {
    this.nameID = nameID;
    /** skala popularności w odniesieniu do innych skladnikow */
    this.popularity = 1;
    this.poisonousPower = 0;
    this.visibility = 0;
  }
  
  static fromPlant(cp) {
    const p = new CollectPlant(cp.nameID);
    p.popularity = cp.popularity;
    p.visibility = cp.visibility;
  }

  getColor() {
    return 0x8800FF00 - 0x1100 * this.popularity;
    //return 0x8800FF00;
  }
  
  getNameID() {
    return this.nameID;
  }

  getInfo() {
    return ObjectInfoFactory.get().getInfoStandarized(this.nameID);
  }

  use() { }
}
