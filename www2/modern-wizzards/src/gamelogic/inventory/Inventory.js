import $log from "../../utils/log.js";

/**
 * 
 * @author Jarek
 *
 * Class for handling all operations on creature's ( including player's) inventory.
 * Especially useful for ListView Adapters.
 */
export default class Inventory {
  private String TAG = "Inventory";
  
  //private List<InventoryItem> inventory = new ArrayList<InventoryItem>();
  private List<SpellToken> mixtures = new ArrayList<SpellToken>();
  //private List<CollectPlant> plants = new ArrayList<CollectPlant>();
  private List<ItemsAggregation> plantsAggregation = new ArrayList<ItemsAggregation>();
  
  //private List<InventoryItem> withAggregatedPlants = new ArrayList<InventoryItem>();
  
  private transient List<BaseAdapter>  adaptersToBeNotified = new ArrayList<BaseAdapter>();
  
  private ItemsAggregation findAggregationWithName(String name) {
    $log.d(TAG, "findAggregationWithName [" + name + "]");
    for(ItemsAggregation pa: plantsAggregation) {
      $log.d(TAG, "tested name: [" + pa.getNameID() + "]");
      if ( pa.getNameID().equals(name) ) {
        return (ItemsAggregation)pa;
      }
    }
    return null;
  }
  
  public void addMixture(SpellToken mixture) {
    //inventory.add(mixture);
    mixtures.add(mixture);
    //withAggregatedPlants.add(mixture);
    if(Looper.myLooper() == Looper.getMainLooper())
      notifyAdapters();
  }
  
  public SpellToken getMixtureByName(String mixtureName) {
    SpellToken result = null;
    
    for(SpellToken mixture : getMixtures())
      if(mixtureName.equals(mixture.getInfo().getID())) {
        result = mixture;
        break;
      }
    return result;
  }
  
  public void addPlant(CollectPlant plant) {
    //inventory.add(plant);
    //plants.add(plant);
    
    ItemsAggregation pa = findAggregationWithName(plant.getNameID());
    if(pa != null)
      pa.count++;
    else
      plantsAggregation.add(new ItemsAggregation(plant));
    notifyAdapters();
    
    Activity activity = Game.getGlobalTopmostActivity();
    if( activity == null) return;
    if( activity instanceof InventoryActivity) {
      $log.d(TAG, "InventoryActivity on top");
      ((InventoryActivity)activity).refreshListView();
    }
  }
  
  public void removeMixture(SpellToken mixture) {
    //inventory.remove(mixture);
    mixtures.remove(mixture);
    //withAggregatedPlants.remove(mixture);
    notifyAdapters();
  }
  
  public void removePlant(CollectPlant plant) {
    //inventory.remove(plant);
    //plants.remove(plant);
    
    ItemsAggregation pa = findAggregationWithName(plant.getNameID());
    if(pa != null)
      if(pa.count <= 1)
        plantsAggregation.remove(pa);
      else
        pa.count--;
    else
      $log.e(TAG, "Removing plant "+plant.getNameID()+" from aggregation plants collection found problem with not existing aggregation object");
    
    notifyAdapters();
  }
  
  public CollectPlant getPlantByName(String plantName) {
    CollectPlant result = null;
    
    for(CollectPlant plant : getPlants())
      if(plant.getNameID().equals(plantName)) {
        result = plant;
        break;
      }
    return result;
  }
  
  
  public List<CollectPlant> getPlants() {
    List<CollectPlant> plants = new ArrayList<CollectPlant>();
    for(ItemsAggregation ia : plantsAggregation)  {
      for(int i=0; i< ia.count; i++)  
        plants.add((CollectPlant)ia.item);
    }
    return plants;
  }
  
  public List<SpellToken> getMixtures() {
    return mixtures;
  }

  
  public List<InventoryItem> getDisplayList() {
    LinkedList<InventoryItem> displayList = new LinkedList<InventoryItem>();
    if(InventoryActivity.SHOW_MIXTURES) displayList.addAll(mixtures);
    if(InventoryActivity.SHOW_ITEMS) displayList.addAll(plantsAggregation);
    return displayList;
  }
  
  public void registerAdapter(BaseAdapter ba) {
    $log.d(TAG, "in registerAdapter");
    adaptersToBeNotified.add(ba);
  }
  
  public void unregisterAdapter(BaseAdapter ba) {
    adaptersToBeNotified.remove(ba);
  }
  
  public void notifyAdapters() {
    for(BaseAdapter ba : adaptersToBeNotified)
      ba.notifyDataSetChanged();
  }
  
  /**
   * Some useful getters 
   */
  
  public int getMixturesCountByName(String name) {
    int count = 0;
    for (SpellToken mixture : mixtures) {
      if (name.equals(mixture.getInfo().getID())) ++count;
    }
    return count;
  }
  
}
