

export default class EventSystem {

  static INSTANCE = new EventSystem();

  constructor() {
    this.listeners = { };
  }

  registerListener(eventType, listener) {
    if (!this.listeners[eventType]) {
      this.listeners[eventType] = [ ];
    }
    this.listeners[eventType].push(listener);

    return () => { 
      this.listeners[eventType] = this.listeners[eventType].filter(l => l != listener)
    };
  }

  notifyListeners(objID, eventType, extraParams = null) {
    if (this.listeners[eventType]) {
      window.gameController.applyToState(s => {
        return this.notifyListenersState(s, objID, eventType, extraParams);
      });
    }
  }

  notifyListenersState(s, objID, eventType, extraParams = null) {
    for (let listener of (this.listeners[eventType] || [])) {
      s = listener.handleEvent(s, objID, eventType, extraParams);
    }
    return s;
  }
}