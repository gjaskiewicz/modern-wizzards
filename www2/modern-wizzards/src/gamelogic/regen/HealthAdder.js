export class LinearHpAdder {
  // 1hp / min -> k = 0.01666666666
  hpToAdd(m, t, k) { 
    k = k || 1;
    return t * k * 0.01666666666;
  }
}