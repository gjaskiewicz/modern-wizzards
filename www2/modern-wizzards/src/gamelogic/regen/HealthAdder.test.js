import {LinearHpAdder} from './HealthAdder.js';

describe('HP adder', () => {
  it('should charge in 100 minutes in single step', () => {
    const adder = new LinearHpAdder();
    const m = adder.hpToAdd(0, 6000);
    expect(Math.abs(m - 100)).not.toBeGreaterThan(1);
    expect(m).not.toBeGreaterThan(100);
  });


  it('should charge in 100 minutes in two steps', () => {
    let m = 0;
    const adder = new LinearHpAdder();
    for (let i = 0; i < 2; i++) {
      m = m + adder.hpToAdd(m, 3000);
    }
    expect(Math.abs(m - 100)).not.toBeGreaterThan(1);
    expect(m).not.toBeGreaterThan(100);    
  });

  it('should charge in 100 minutes in 1000 steps', () => {
    let m = 0;
    const adder = new LinearHpAdder();
    for (let i = 0; i < 1000; i++) {
      m = m + adder.hpToAdd(m, 6.0);
    }
    console.log(m);
    expect(Math.abs(m - 100)).not.toBeGreaterThan(10);
    expect(m).not.toBeGreaterThan(100);    
  });

  it('big steps == small steps', () => {
    let m1 = 0, m2 = 0;
    const a1 = new LinearHpAdder();
    const a2 = new LinearHpAdder();
    for (let i = 0; i < 3600 * 25; i++) {
      m1 = m1 + a1.hpToAdd(m1, 1.0 / 25, 2);
    }
    m2 = a2.hpToAdd(m2, 3600, 2);
    expect(Math.abs(m1 - m2)).not.toBeGreaterThan(1);
  })
});