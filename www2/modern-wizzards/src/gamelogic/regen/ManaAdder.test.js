import {LogManaAdder, SqrtManaAdder, LinearManaAdder} from './ManaAdder.js';
describe('Mana adder should regen mana', () => {

  for (let cls of [LogManaAdder, SqrtManaAdder, LinearManaAdder]) {
    describe(`${cls.name}`, () => {

      it('should charge in 4 hours in single step', () => {
        const adder = new cls();
        const m = adder.manaToAdd(0, 14400, 2);
        expect(Math.abs(m - 100)).not.toBeGreaterThan(10);
        expect(m).not.toBeGreaterThan(100);
      });


      it('should charge in 4 hours in two steps', () => {
        let m = 0;
        const adder = new cls();
        for (let i = 0; i < 2; i++) {
          m = m + adder.manaToAdd(m, 7200, 2);
        }
        expect(Math.abs(m - 100)).not.toBeGreaterThan(10);
        expect(m).not.toBeGreaterThan(100);    
      });

      it('should charge in 4 hours in 144 steps', () => {
        let m = 0;
        const adder = new cls();
        for (let i = 0; i < 144; i++) {
          m = m + adder.manaToAdd(m, 100, 2);
        }
        expect(Math.abs(m - 100)).not.toBeGreaterThan(10);
        expect(m).not.toBeGreaterThan(100);    
      });

      it('big steps == small steps', () => {
        let m1 = 0, m2 = 0;
        const a1 = new cls();
        const a2 = new cls();
        for (let i = 0; i < 3600 * 25; i++) {
          m1 = m1 + a1.manaToAdd(m1, 1.0 / 25, 2);
        }
        m2 = a2.manaToAdd(m2, 3600, 2);
        expect(Math.abs(m1 - m2)).not.toBeGreaterThan(1);
      })
    });
  }
});
