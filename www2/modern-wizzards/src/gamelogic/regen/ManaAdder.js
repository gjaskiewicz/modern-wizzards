export class ManaAdder {
  manaToAdd(m,t,k) { return 0 }
}

const LG_K = 1.52;
const LG_M = 9.33;
export class LogManaAdder extends ManaAdder {
  // 4h - 2mrg -- k = 10

  manaToAdd(m, t, k) { 
    k = k || 1;
    const t0 = Math.exp(m / LG_M) / (k * LG_K);
    return LG_M * (Math.log((t0 + t) * k * LG_K) - Math.log(t0 * k * LG_K));
  }
}

const SQ_K = 0.347;
export class SqrtManaAdder extends ManaAdder{
  // 4h - 2mrg -- k0 = 0.173
  manaToAdd(m, t, k) { 
    k = k || 1;
    const t0 = m * m / (k * SQ_K);
    return Math.sqrt((t0 + t) * k * SQ_K) - Math.sqrt(t0 * k * SQ_K);
  }
}

export class LinearManaAdder extends ManaAdder {
  // 4h - 2mrg -- k0 = 0.00347222222
  manaToAdd(m, t, k) { 
    k = k || 1;
    return t * k * 0.00347222222;
  }
}
