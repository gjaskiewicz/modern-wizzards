import HealingPotion from './mixtures/HealingPotion.js';
import ManaPotion from './mixtures/ManaPotion.js';
import StimulationPotion from './mixtures/StimulationPotion.js';
import LuckPotion from './mixtures/LuckPotion.js';
import AntidotePotion from './mixtures/AntidotePotion.js';
import InvisibilityPotion from './mixtures/InvisibilityPotion.js';
import InitialPotion from './mixtures/InitialPotion.js';
import PerceptionPotion from './mixtures/PerceptionPotion.js';
import UnknownPotion from './mixtures/UnknownPotion.js';
import PhysicalAttack from './spells/PhysicalAttack.js';
import Fireball from './spells/Fireball.js';
import AstralShield from './spells/AstralShield.js';
import Speed from './spells/Speed.js';
import Electrokinesis from './spells/Electrokinesis.js';
import Telekinesis from './spells/Telekinesis.js';
import OutOfBodyExperience from './spells/OutOfBodyExperience.js';
import $log from "../utils/log.js";

const TAG = "SpellFactory";

const SPELL_MAP = {
/* TODO: migrate */
  // battle spells
  "spell_physical": (owner) => new PhysicalAttack(owner),
  "spell_battlespeed": (owner) => new Speed(owner),
  "spell_astralshield": (owner) => new AstralShield(owner),
  "spell_fireball": (owner) => new Fireball(owner),
  "spell_electrokinesis": (owner) => new Electrokinesis(owner),
  // env spells
  "spell_oob": (owner) => new OutOfBodyExperience(owner),
  "spell_telekinesis": (owner) => new Telekinesis(owner),
  // potions
  "potion_initial": (owner) => new InitialPotion(owner),
  "potion_health": (owner) => new HealingPotion(owner),
  "potion_mana": (owner) => new ManaPotion(owner),
  "potion_antidote": (owner) => new AntidotePotion(owner),
  "potion_stimulation": (owner) => new StimulationPotion(owner),
  "potion_invisibility": (owner) => new InvisibilityPotion(owner),
  "potion_luckjelly": (owner) => new LuckPotion(owner),
  "potion_perception": (owner) => new PerceptionPotion(owner),
  "potion_unknown": (owner) => new UnknownPotion(owner),
}

export default class SpellFactory {

  static getSpellByID(spellID, owner) {
    if (SPELL_MAP[spellID]) {
      return SPELL_MAP[spellID](owner);
    }
    $log.e("SPELL FACTORY", "UNKNOWN SPELL ID!!: " + spellID);
    return null;
  }
}
