export default class Castable {
  static UsageType = {
    MAP:     "MAP",
    BATTLE:  "BATTLE",
    ANYTIME: "ANYTIME",
  }
  
  static EffectTargetType = {
    SELF: "SELF",
    ANOTHER: "ANOTHER",
    AMBIENT: "AMBIENT",
  }
  
  //TODO: duration in turns;
  
  getRequiredMana() { throw "Abstract-notImplemented"; }

  /* zabiera mane na zaś, dodaje do listy czarów */
  castThis(state) { throw "Abstract-notImplemented"; }
  takeEffect() { throw "Abstract-notImplemented"; }
  // ObjectInfo getInfo();
  getUsageType() { throw "Abstract-notImplemented"; }
  getEffectTargetType() { throw "Abstract-notImplemented"; }
}
