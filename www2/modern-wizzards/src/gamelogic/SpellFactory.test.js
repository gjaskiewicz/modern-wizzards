import SpellFactory from './SpellFactory.js';
import Player from './creatures/Player.js';
import R from '../R.js';

describe('Object info factory', () => {
  it('should get player battle spell info', () => {
    for (let spellID of ['spell_physical', 'spell_fireball', 'spell_electrokinesis', 
                         'spell_astralshield']) {
      const spell = SpellFactory.getSpellByID(spellID, Player.INSTANCE);
      expect(!!spell).toBe(true);
      const spellInfo = spell.getInfo();
      expect(spellInfo.getName()[0]).not.toBe('!');
      expect(spellInfo.getImageID()).not.toBe(R.drawables.unknown);
      expect(spellInfo.getDescription()[0]).not.toBe('!');
      expect(spellInfo.getID()).toEqual(spell.nameID);
    }
  });

  it('should get creatures battle spell info', () => {
    for (let spellID of ['spell_battlespeed']) {
      const spell = SpellFactory.getSpellByID(spellID, Player.INSTANCE);
      expect(!!spell).toBe(true);
      const spellInfo = spell.getInfo();
      expect(spellInfo.getName()[0]).not.toBe('!');
      expect(spellInfo.getDescription()[0]).not.toBe('!');
      expect(spellInfo.getID()).toEqual(spell.nameID);
    }
  });

  it('should get world spell info', () => {
    for (let spellID of ['spell_oob', 'spell_telekinesis']) {
      const spell = SpellFactory.getSpellByID(spellID, Player.INSTANCE);
      expect(!!spell).toBe(true);
      const spellInfo = spell.getInfo();
      expect(spellInfo.getName()[0]).not.toBe('!');
      expect(spellInfo.getImageID()).not.toBe(R.drawables.unknown);
      expect(spellInfo.getDescription()[0]).not.toBe('!');
      expect(spellInfo.getID()).toEqual(spell.nameID);
    }
  });
});
