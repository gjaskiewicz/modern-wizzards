import Golem from './Golem.js';
import GenericCreatureTest from './GenericCreatureTest.js';

describe('golem', () => {
  GenericCreatureTest.testOpacity(new Golem());
  GenericCreatureTest.testKombat(new Golem());
});
