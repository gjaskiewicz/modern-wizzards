export default class GenericCreatureTest {
  static testOpacity(creature) {
    it('should have correct opacity in kombat', () => {
      expect(creature.getBehaviourOpacity()).toEqual(255);    
    });
  };

  static testKombat(creature) {
    it('should stay in bounds in kombat', () => {
      for (let i = 0; i < 100; i++) {
        creature.updateBehaviourPosition(i);
        const pos = creature.getBehaviourPosition()
        expect(pos).toBeLessThan(100.1);
        expect(pos).not.toBeLessThan(-100);
      }
    });
  };
}
