import Warewolf from './Warewolf.js';
import GenericCreatureTest from './GenericCreatureTest.js';

describe('warewolf', () => {
  GenericCreatureTest.testOpacity(new Warewolf());
  GenericCreatureTest.testKombat(new Warewolf());
});
