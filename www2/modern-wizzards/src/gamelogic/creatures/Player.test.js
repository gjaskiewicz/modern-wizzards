import Bunny from './Bunny.js';
import Player from './Player.js';
import BadLuck from '../spells/BadLuck.js';
import TestData from '../../test/TestData.js';

describe('Creature', () => {

  it('should be under effect of spell', () => {
    let s = {
      playerEgo: TestData.getFullEgo(),
      ui: { },
    };
    const bunny = new Bunny();
    const spell = new BadLuck(bunny);
    spell.setTarget(Player.INSTANCE);
    const {state, success} = spell.castThis(s);    
    expect(success).toBe(true);
    expect(Player.INSTANCE.isUnderEffectOfSpell(state, spell.nameID)).toBe(true);
  });
});