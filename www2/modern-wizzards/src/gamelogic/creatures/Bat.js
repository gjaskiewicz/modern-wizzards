import FightBehaviour from "../FightBehaviour.js";
import Creature from "../Creature.js";
import PhysicalAttack from "../spells/PhysicalAttack.js";
import Drain from "../spells/Drain.js";

export default class Bat extends Creature {
  
  constructor()  {
    super();
    this.nameID = "creature_bat";
    this.mana = 50;
    this.maxMana = 50;
    this.health = 30;
    this.maxHealth = 30;
    this.popularity = 7;
    this.aggresivness = 0.5;
    
    this.knownFightSpells.push("spell_physical");
  }

  castSpell(knownSpell)  {
    let toCast = null;
    if(knownSpell == "spell_physical")  {
      const pa = new PhysicalAttack(this);
      pa.displayNameID = "spell_physical_bite";        
      pa.hitpointsToTake = 7;
      pa.infects = true;
      pa.infection = new Drain(this);
      toCast = pa;
    }
    return toCast;
  }
  
  updateBehaviourPosition(iteration) {
    this.fightBehaviour.updateBehaviourPosition(this, iteration);
  }
}
