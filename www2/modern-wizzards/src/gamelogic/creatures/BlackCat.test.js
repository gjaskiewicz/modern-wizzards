import BlackCat from './BlackCat.js';
import GenericCreatureTest from './GenericCreatureTest.js';

describe('black cat', () => {
  GenericCreatureTest.testOpacity(new BlackCat());
  GenericCreatureTest.testKombat(new BlackCat());
});
