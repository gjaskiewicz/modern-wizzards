import FightBehaviour from "../FightBehaviour.js";
import Creature from "../Creature.js";
import PhysicalAttack from "../spells/PhysicalAttack.js";

class GolemBehaviour extends FightBehaviour {
  updateBehaviourPosition(obj, iteration) {
    this.behaviourPosition = Math.sin(iteration / 27) * 50;
  }
};

export default class Golem extends Creature {
  
  constructor()  {
    super();
    this.nameID = "creature_golem";
    
    this.health = 250;
    this.maxHealth = 250;
    this.mana = 300;
    this.maxMana = 300;
    this.popularity = 0;
    this.aggresivness = 1.0;
    
    this.containsItem = null;
    this.fightBehaviour = new GolemBehaviour();
    this.knownFightSpells.push("spell_physical");
  }
  
  castSpell(knownSpell)  {
    let toCast = null;
    if(knownSpell == "spell_physical")  {
      const pa = new PhysicalAttack(this);
      pa.hitpointsToTake = 25;
      toCast = pa;
    }
    return toCast;
  }

  updateBehaviourPosition(iteration) {
    this.fightBehaviour.updateBehaviourPosition(this, iteration);
  }
};
