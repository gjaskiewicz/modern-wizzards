import PlayerEgo from '../../PlayerEgo.js';

export default class Player {

  static INSTANCE = new Player();

  getMana(state) {
    return state.playerEgo.mana;
  }

  getHealth(state) {
    return state.playerEgo.health;
  }

  getGeoPoint(state) {
    return PlayerEgo.getGeoPoint(state.playerEgo);
  }

  addMana(state, mana) {
    return PlayerEgo.addManaPoints(state, mana);
  }

  addHealth(state, hp) {
    return {
      ...state,
      playerEgo: PlayerEgo.addHealthPoints(state.playerEgo, hp)
    }
  }

  isUnderEffectOfSpell(state, spell) {
    return PlayerEgo.isUnderEffectOfSpell(state.playerEgo, spell);
  }

  addUnderEffectOfSpell(state, spell) {
    return {
      ...state,
      playerEgo: {
        ...state.playerEgo,
        underEffectOfSpell: [...state.playerEgo.underEffectOfSpell, spell]
      }
    }
  }

  removeUnderEffectOfSpell(state, spellID) {
    const underEffectOfSpell = state.playerEgo.underEffectOfSpell.filter(
        spell => spell.nameID != spellID)
    return {
      ...state,
      playerEgo: {
        ...state.playerEgo,
        underEffectOfSpell
      }
    }
  }

  removeFinishedSpells(state) {
    return {
      ...state,
      playerEgo: PlayerEgo.removeFinishedSpells(state.playerEgo)
    }
  }

  getBehaviourPosition() {
    // TODO: czy tu był sinus?
    return 0;
  }

  isPlayer() {
    return true;
  }

  newInstance() {
    throw "Dont instantiate Player!";
  }
}
