import FightBehaviour from "../FightBehaviour.js";
import Creature from "../Creature.js";
import PhysicalAttack from "../spells/PhysicalAttack.js";
import BadLuck from "../spells/BadLuck.js";

export default class BlackCat extends Creature {
  
  constructor()  {
    super();
    this.nameID = "creature_black_cat";
    this.health = 60;
    this.maxHealth = 60;
    this.mana = 70;
    this.maxMana = 70;
    this.popularity = 6;
    this.aggresivness = 0.3;
    
    this.containsItem = "item_cateye";
    
    this.knownFightSpells.push("spell_physical");
    this.knownFightSpells.push("spell_physical2");
    this.knownFightSpells.push("spell_badluck");
    
    //this.knownSpells.push(new Thunderbolt());
    //this.knownSpells.push(new IcySpike());
  }

  castSpell(knownSpell) {
    let toCast = null;
    if(knownSpell == "spell_physical") {
      const pa = new PhysicalAttack(this);
      pa.hitpointsToTake = 10;
      pa.displayNameID = "spell_physical_bite";      
      toCast = pa;
    }
    if(knownSpell == "spell_physical2") {
      const pa = new PhysicalAttack(this);
      pa.hitpointsToTake = 8;
      pa.displayNameID = "spell_physical_scratch";      
      toCast = pa;
    }
    else if(knownSpell.equals("spell_badluck"))  {
      toCast = new BadLuck(this);
    }
    return toCast;
  }
  
  updateBehaviourPosition(iteration) {
    this.fightBehaviour.updateBehaviourPosition(this, iteration);
  }
}
