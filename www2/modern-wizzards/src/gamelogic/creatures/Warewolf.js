import FightBehaviour from "../FightBehaviour.js";
import Creature from "../Creature.js";
import PhysicalAttack from "../spells/PhysicalAttack.js";
import RNG from "../../utils/RNG.js";
import Infection from "../spells/Infection.js";

export default class Warewolf extends Creature {
  
  constructor() {
    super();
    this.nameID = "creature_warewolf";
    
    this.mana = 200;
    this.maxMana = 200;
    this.health = 180;
    this.maxHealth = 180;
    this.popularity = 3;
    this.aggresivness = 1.0;
    this.knownFightSpells.push("spell_physical");
    this.knownFightSpells.push("spell_physical2");

    this.movesNormalFor = 0;
    this.movesFastFor = 0;
    this.ownIter = 0;
    this.rng = new RNG();
  }

  castSpell(knownSpell)  {
    let toCast = null;
    if(knownSpell == "spell_physical") {
      const pa = new PhysicalAttack(this);
      pa.hitpointsToTake = 20;
      pa.displayNameID = "spell_physical_bite";          
      pa.infects = true;
      pa.infection = new Infection(this);
      toCast = pa;
    }
    if(knownSpell == "spell_physical2") {
      const pa = new PhysicalAttack(this);
      pa.hitpointsToTake = 15;
      pa.displayNameID = "spell_physical_scratch";      
      toCast = pa;
    }
    return toCast;
  }

  updateBehaviourPosition(iteration) {
    this.ownIter++;
    if(this.movesFastFor > 0) { 
      this.ownIter++;
      this.movesFastFor--;
    }
    else if(this.movesFastFor==0)  {
      this.movesFastFor--;
      this.movesNormalFor= Math.floor(this.rng.nextFloat() * 100);
    }
    else if(this.movesNormalFor>0)  { 
      this.movesNormalFor--;
    }
    else if(this.movesNormalFor==0)  {
      this.movesNormalFor--;
      this.movesFastFor=Math.floor(this.rng.nextFloat() * 100);
    }
    
    this.fightBehaviour.setBehaviourPosition(Math.sin((this.ownIter) / 18) * 100);
  }
}
