import Bat from './Bat.js';
import GenericCreatureTest from './GenericCreatureTest.js';

describe('bat', () => {
  GenericCreatureTest.testOpacity(new Bat());
  GenericCreatureTest.testKombat(new Bat());
});
