import Strumpf from './Strumpf.js';
import GenericCreatureTest from './GenericCreatureTest.js';

describe('strumpf', () => {
  GenericCreatureTest.testOpacity(new Strumpf());
  GenericCreatureTest.testKombat(new Strumpf());
});
