import Wraith from './Wraith.js';
import GenericCreatureTest from './GenericCreatureTest.js';

describe('wraith', () => {
  GenericCreatureTest.testOpacity(new Wraith());
  GenericCreatureTest.testKombat(new Wraith());
});
