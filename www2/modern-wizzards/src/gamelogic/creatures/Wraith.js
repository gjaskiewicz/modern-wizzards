import FightBehaviour from "../FightBehaviour.js";
import Creature from "../Creature.js";
import PhysicalAttack from "../spells/PhysicalAttack.js";

class WraithBehaviour extends FightBehaviour {
  constructor() {
    super();
    this.invisibleFor = 0;
    this.movesFor = 100;
  }
  
  updateBehaviourPosition(obj, iteration) {
    this.behaviourPosition = (Math.sin(iteration / 17)+Math.sin(iteration / 10) * 0.2) * 80;
    if(this.invisibleFor > 0) {
      this.invisibleFor--;
      this.behaviourOpacity = 255 - this.invisibleFor * 25;
      if (this.behaviourOpacity < 0) {
        this.ehaviourOpacity = 0;
      }
    } else if(this.invisibleFor == 0) {
      this.invisibleFor--;
      this.movesFor = Math.floor(Math.random() * 100 + 25);
    } else if(this.movesFor > 0) {
      this.behaviourOpacity = this.movesFor * 25;
      if (this.behaviourOpacity > 255) {
        this.behaviourOpacity = 255;
      }
      this.movesFor--;
    } else if(this.movesFor==0) {
      this.movesFor--;
      this.invisibleFor = Math.floor(Math.random()*100+25);
    }
  }
}

export default class Wraith extends Creature {
  
  constructor()  {
    super();
    this.nameID = "creature_wraith";
    
    this.health = 130;
    this.maxHealth = 130;
    this.mana = 200;
    this.maxMana = 200;
    this.popularity = 4;
    this.aggresivness = 0.9;
    
    this.containsItem = "item_ectoplasma";
    
    this.fightBehaviour = new WraithBehaviour();
    
    this.knownFightSpells.push("spell_physical");
  }
  
  castSpell(knownSpell)  {
    let toCast = null;
    if(knownSpell == "spell_physical")  {
      const pa = new PhysicalAttack(this);
      pa.hitpointsToTake = 10;
      //pa.infects = true;
      //pa.infection = new Infection(this);
      toCast = pa;
    }
    return toCast;
  }
  
  updateBehaviourPosition(iteration) {
    this.fightBehaviour.updateBehaviourPosition(this, iteration);
  }
}
