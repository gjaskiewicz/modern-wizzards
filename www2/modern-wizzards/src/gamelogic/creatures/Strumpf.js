import FightBehaviour from "../FightBehaviour.js";
import Creature from "../Creature.js";
import PhysicalAttack from "../spells/PhysicalAttack.js";
import RNG from "../../utils/RNG.js";

export default class Strumpf extends Creature {
  
  constructor() {
    super();
    this.nameID = "creature_strumpf";
    this.health = 30;
    this.maxHealth = 30;
    this.mana = 40;
    this.maxMana = 40;
    this.popularity = 5;
    this.aggresivness = 0.1;
    
    this.containsItem = "item_smurfblood";
    
    this.fightBehaviour = new StrumpfBehaviour();    
    this.knownFightSpells.push("spell_physical");
  }
  
  castSpell(knownSpell)  {
    let toCast = null;
    if(knownSpell == "spell_physical")  {
      const pa = new PhysicalAttack(this);
      pa.hitpointsToTake = 8;
      //pa.infects = true;
      //pa.infection = new Infection(this);
      toCast = pa;
    }
    return toCast;
  }

  updateBehaviourPosition(iteration) {
    this.fightBehaviour.updateBehaviourPosition(this, iteration);
  }
}

class StrumpfBehaviour extends FightBehaviour {
  constructor() {
    super();
    this.invisibleFor = 1;
    this.movesFor = 0;
    this.movesFrom = 0;
    this.movesTo = 0;
    this.moveDelta = 4;
    this.rng = new RNG();
  }
    
  updateBehaviourPosition(iteration) {
    if(this.invisibleFor > 0) {
      this.invisibleFor--;
      this.behaviourOpacity = 255 - this.invisibleFor * 77;
      if(this.behaviourOpacity < 0) this.behaviourOpacity = 0;
    }
    else if(this.invisibleFor <= 0)  {
      this.invisibleFor--;
    }
    
    if(this.movesFor > 0){
      let sign = -1;
      if(this.movesTo < this.movesFrom) sign = 1;
      this.behaviourPosition = this.movesTo + (this.movesFor * this.moveDelta * sign); 
      this.movesFor--;
      this.behaviourOpacity = this.movesFor * 77;
      if(this.behaviourOpacity > 255) this.behaviourOpacity = 255;
    }
    else if(this.movesFor <= 0) {
      this.movesFor--;
      this.invisibleFor = Math.floor((this.rng.nextFloat() * 50 + 25));
      do {
        this.movesFrom = Math.floor(this.rng.nextFloat() * 200 - 100);
        this.movesTo = Math.floor(this.rng.nextFloat() * 200 - 100);
        this.movesFor = Math.floor(Math.abs((this.movesFrom - this.movesTo) / this.moveDelta));
      }
      while(this.movesFor < 25);
      this.behaviourPosition = this.movesFrom;
    }
  }
}
