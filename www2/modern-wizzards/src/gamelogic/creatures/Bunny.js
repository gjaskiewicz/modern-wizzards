import FightBehaviour from "../FightBehaviour.js";
import Creature from "../Creature.js";
import PhysicalAttack from "../spells/PhysicalAttack.js";
import Speed from "../spells/Speed.js";

class BunnyBehaviour extends FightBehaviour {

  constructor(JUMP_ITER, JUMP_DIST, WAIT_ITER, dir, wait) {
    super();
    this.JUMP_ITER = JUMP_ITER;
    this.JUMP_DIST = JUMP_DIST;
    this.WAIT_ITER = WAIT_ITER;
    this.jumping = JUMP_ITER;
    this.dir = dir;
    this.wait = wait;
  }

  getJumpIter(bunny) {
    if(bunny.isUnderEffectOfSpell({ }, "spell_battlespeed"))  // FIXME
      return 30;
    else
      return this.JUMP_ITER;
  }

  getJumpDist(bunny) {
    if(bunny.isUnderEffectOfSpell({ }, "spell_battlespeed"))  // FIXME
      return 30;
    else
      return this.JUMP_DIST;
  }

  getWaitIter(bunny) {
    if(bunny.isUnderEffectOfSpell({ }, "spell_battlespeed"))  // FIXME
      return 0;
    else
      return this.WAIT_ITER;
  }

  updateBehaviourPosition(bunny, iteration) {
    if (this.jumping > 0) {
      this.jumping--;
      this.behaviourPosition += this.dir * this.getJumpDist(bunny);
      if (this.behaviourPosition > 100)  {
        this.dir = -1;
        this.behaviourPosition = 100;
      }
      if (this.behaviourPosition < -100)  {
        this.dir = 1;
        this.behaviourPosition = -100;
      }
    }
    else if (this.wait > 0)  {
      this.wait--;
    }
    else if (this.jumping == 0) {
      this.wait = this.getWaitIter(bunny);
      this.jumping = -1;
    }
    else if (this.wait == 0) {
      this.jumping = this.getJumpIter(bunny);
      this.wait = -1;
    }
      
    this.setBehaviourPosition(this.behaviourPosition);
  }
}

export default class Bunny extends Creature {

  constructor()  {
    super();
    this.nameID = "creature_bunny";
    this.health = 40;
    this.maxHealth = 40;
    this.mana = 40;
    this.maxMana = 40;
    this.popularity = 7;
    this.aggresivness = 0.2;
    
    this.fightBehaviour = new BunnyBehaviour(3, 11, 7, 1, 0);
    
    this.containsItem = "item_bunnyleg";
    
    this.knownFightSpells.push("spell_physical");
    this.knownFightSpells.push("spell_battlespeed");
  }
  
  castSpell(knownSpell)  {
    let toCast = null;
    if(knownSpell == "spell_physical" || true)  {    // FIXME
      const pa = new PhysicalAttack(this);
      pa.hitpointsToTake = 8;
      pa.displayNameID = "spell_physical_bite";
      /* TODO: migrate
      //pa.infects = true;
      //pa.infection = new Infection(this);
      */
      toCast = pa;
    } else if(knownSpell == "spell_battlespeed")  {
      toCast = new Speed(this);
    }
    return toCast;
  }
  
  updateBehaviourPosition(iteration) {
    this.fightBehaviour.updateBehaviourPosition(this, iteration);
  }
}
