import FightBehaviour from "../FightBehaviour.js";
import Creature from "../Creature.js";
import PhysicalAttack from "../spells/PhysicalAttack.js";
import Drain from "../spells/Drain.js";

export default class Gnome extends Creature {
  
  constructor()  {
    super();
    this.nameID = "creature_gnome";
    
    this.mana = 100;
    this.health = 100;
    this.maxMana = 100;
    this.maxHealth = 100;
    this.aggresivness = 0.6;
    
    this.popularity = 4;
    
    this.knownFightSpells.push("spell_physical");
  }
  
  castSpell(knownSpell)  {
    let toCast = null;
    if(knownSpell == "spell_physical")  {
      const pa = new PhysicalAttack(this);
      pa.hitpointsToTake = 15;
      pa.displayNameID = "spell_physical_spear";      
      toCast = pa;
    }
    return toCast;
  }
  
  updateBehaviourPosition(iteration) {
    this.fightBehaviour.updateBehaviourPosition(this, iteration);
  }
}
