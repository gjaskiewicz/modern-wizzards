import Bunny from './Bunny.js';
import Speed from '../spells/Speed.js';

describe('Bunny', () => {
  it('should move in kombat', () => {
    const bunny = new Bunny();
    bunny.updateBehaviourPosition(1);
    expect(bunny.getBehaviourPosition()).toEqual(11);

    bunny.updateBehaviourPosition(2);
    expect(bunny.getBehaviourPosition()).toEqual(22);

    bunny.updateBehaviourPosition(3);
    expect(bunny.getBehaviourPosition()).toEqual(33);

    bunny.updateBehaviourPosition(4);
    expect(bunny.getBehaviourPosition()).toEqual(33);

    bunny.updateBehaviourPosition(5);
    expect(bunny.getBehaviourPosition()).toEqual(33);
  });

  it('should move in kombat with battlespeed', () => {
    const bunny = new Bunny();
    const spell = new Speed(bunny);
    const {success, state} = spell.castThis({ });
    expect(success).toBe(true);

    bunny.updateBehaviourPosition(1);
    expect(bunny.getBehaviourPosition()).toEqual(30);

    bunny.updateBehaviourPosition(2);
    expect(bunny.getBehaviourPosition()).toEqual(60);

    bunny.updateBehaviourPosition(3);
    expect(bunny.getBehaviourPosition()).toEqual(90);

    bunny.updateBehaviourPosition(4);
    expect(bunny.getBehaviourPosition()).toEqual(90);

    bunny.updateBehaviourPosition(5);
    expect(bunny.getBehaviourPosition()).toEqual(90);
  });

  it('should have correct opacity in kombat', () => {
    const bunny = new Bunny();
    expect(bunny.getBehaviourOpacity()).toEqual(255);    
  });

  it('should stay in bounds in kombat', () => {
    const bunny = new Bunny();
    for (let i = 0; i < 100; i++) {
      bunny.updateBehaviourPosition(1);
      const pos = bunny.getBehaviourPosition()
      expect(pos).toBeLessThan(100.1);
      expect(pos).not.toBeLessThan(-100);
    }
  });
});
