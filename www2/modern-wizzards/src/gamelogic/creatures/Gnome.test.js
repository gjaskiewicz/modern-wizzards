import Gnome from './Gnome.js';
import GenericCreatureTest from './GenericCreatureTest.js';

describe('gnome', () => {
  GenericCreatureTest.testOpacity(new Gnome());
  GenericCreatureTest.testKombat(new Gnome());
});
