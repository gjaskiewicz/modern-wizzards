import SpellFromPotion from "../SpellFromPotion.js";
import $log from  "../../utils/log.js";
import {ITERATIONS_PER_SECOND} from '../../Constants.js';

export default class LuckPotion extends SpellFromPotion {

  constructor(owner) {
    super(owner);
    this.nameID = "potion_luckjelly";
    this.mixtureColor = 0x40FFFFFF;
    this.turnDuration = ITERATIONS_PER_SECOND * 60 * 15; // trwa 15 minut
  }
  
  castThis(state) {
    // zapewnienie ze gracz bedzie tylko pod jednš miksturš
    if (this.owner.isUnderEffectOfSpell(state, this.nameID))
      return {
        success: false,
        state
      };
    return super.castThis(state);
  }

  takeEffect(state) {
    return super.takeEffect(state);
  }
}
