import StimulationPotion from './StimulationPotion.js';
import Player from '../creatures/Player.js';
import TestData from '../../test/TestData.js';

describe('StimulationPotion', () => {
  it('should stimulate', () => {
    let s = {
      playerEgo: {
         ...TestData.getFullEgo(),
         health: 70,
         mana: 70,
      }
    };
    let potion = new StimulationPotion(Player.INSTANCE);
    const {success, state} = potion.castThis(s);
    expect(success).toBe(true);
    expect(state.playerEgo.health).toBe(120);
    expect(state.playerEgo.mana).toBe(120);
  });
});
