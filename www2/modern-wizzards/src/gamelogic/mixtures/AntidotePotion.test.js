import AntidotePotion from './AntidotePotion.js';
import Player from '../creatures/Player.js';
import TestData from '../../test/TestData.js';

describe('AntidotePotion', () => {
  it('should cast', () => {
    let s = {
      playerEgo: TestData.getFullEgo()
    };
    let potion = new AntidotePotion(Player.INSTANCE);
    const cast = potion.castThis(s);
    expect(cast.success).toBe(true);
  });
});
