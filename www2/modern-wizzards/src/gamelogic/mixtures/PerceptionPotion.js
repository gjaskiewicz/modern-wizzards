import SpellFromPotion from "../SpellFromPotion.js";

export default class PerceptionPotion extends SpellFromPotion {
  constructor(owner) {
    super(owner);
    this.nameID="potion_perception";
    this.mixtureColor = 0xFF00FF00;
    this.turnDuration = 30 * 60 * 5; // trwa 5 minut.
  }
}
