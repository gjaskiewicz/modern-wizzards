import InvisibilityPotion from './InvisibilityPotion.js';
import Player from '../creatures/Player.js';
import TestData from '../../test/TestData.js';

describe('InvisibilityPotion', () => {

  it('should cast', () => {
    let s = {
      playerEgo: TestData.getFullEgo(),
    };
    let potion = new InvisibilityPotion(Player.INSTANCE);
    const {success, state} = potion.castThis(s);
    expect(success).toBe(true);
    expect(state.playerEgo.underEffectOfSpell[0].nameID).toBe("potion_invisibility");
  });
});
