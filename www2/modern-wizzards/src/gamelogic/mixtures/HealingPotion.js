import SpellFromPotion from "../SpellFromPotion.js";
import PlayerEgo from "../../PlayerEgo.js";

export default class HealingPotion extends SpellFromPotion {

  constructor(owner) {
    super(owner);
    this.nameID = "potion_health";
    this.mixtureColor = 0xFF0000;
  }
	
  castThis(state) {
    return {
      success: true,
      state: {...state, playerEgo: PlayerEgo.addHealthPoints(state.playerEgo, 50) }
    };
  }
}

