import InitialPotion from './InitialPotion.js';
import Player from '../creatures/Player.js';
import TestData from '../../test/TestData.js';

describe('InitialPotion', () => {
  window.effects = {addEffect: () => null};

  it('should cast', () => {
    let s = {
      playerEgo: TestData.getFullEgo(),
    };
    let potion = new InitialPotion(Player.INSTANCE);
    const {success, state} = potion.castThis(s);
    expect(success).toBe(true);
  });
});
