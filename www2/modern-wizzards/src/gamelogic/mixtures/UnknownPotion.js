import SpellFromPotion from "../SpellFromPotion.js";
import $log from  "../../utils/log.js";
import EventSystem from "../events/EventSystem.js";
import EventTypes from "../events/EventTypes.js";

export default class UnknownPotion extends SpellFromPotion {
  
  constructor(owner)  {
    super(owner);
    this.nameID = "potion_unknown";
    this.inner = null;
  }
  
  getRequiredMana() {
    return 0;
  }

  castThis(state) {
    $log.d("Unknown wrapper", "using of potion");
    state = EventSystem.INSTANCE.notifyListenersState(
        state, this.getInfo().getID(), EventTypes.USED);
    if(this.inner != null) {
      let castResult = this.inner.castThis();
      state = castResult.state;
    }
    return {
      success: true,
      state
    };
  }
}
