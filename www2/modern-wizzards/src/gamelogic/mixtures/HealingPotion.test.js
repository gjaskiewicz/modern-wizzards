import HealingPotion from './HealingPotion.js';
import Player from '../creatures/Player.js';
import TestData from '../../test/TestData.js';

describe('HealingPotion', () => {
  it('should heal', () => {
    let s = {
      playerEgo: {
         ...TestData.getFullEgo(),
         health: 10,
      }
    };
    let potion = new HealingPotion(Player.INSTANCE);
    const {success, state} = potion.castThis(s);
    expect(success).toBe(true);
    expect(state.playerEgo.health).toBe(60);
  });
});
