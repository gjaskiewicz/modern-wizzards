import LuckPotion from './LuckPotion.js';
import Player from '../creatures/Player.js';
import TestData from '../../test/TestData.js';

describe('LuckPotion', () => {

  it('should cast', () => {
    let s = {
      playerEgo: TestData.getFullEgo(),
    };
    let potion = new LuckPotion(Player.INSTANCE);
    const {success, state} = potion.castThis(s);
    expect(success).toBe(true);
    expect(state.playerEgo.underEffectOfSpell[0].nameID).toBe("potion_luckjelly");
  });
});
