import ManaPotion from './ManaPotion.js';
import Player from '../creatures/Player.js';
import TestData from '../../test/TestData.js';

describe('ManaPotion', () => {
  it('should restore mana', () => {
    let s = {
      playerEgo: {
         ...TestData.getFullEgo(),
         mana: 10,
      }
    };
    let potion = new ManaPotion(Player.INSTANCE);
    const {success, state} = potion.castThis(s);
    expect(success).toBe(true);
    expect(state.playerEgo.mana).toBe(100);
  });
});
