import SpellFromPotion from "../SpellFromPotion.js";

export default class InvisibilityPotion extends SpellFromPotion {

  constructor(owner)  {
    super(owner);
    this.nameID = "potion_invisibility";
    this.mixtureColor = 0x40FFFFFF;
    this.turnDuration = 30 * 60 * 3; //trwa 3 minuty
  }
}
