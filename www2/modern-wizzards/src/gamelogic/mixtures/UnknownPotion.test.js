import UnknownPotion from './UnknownPotion.js';
import Player from '../creatures/Player.js';
import TestData from '../../test/TestData.js';

describe('UnknownPotion', () => {

  it('should cast without inner spell', () => {
    let s = {
      playerEgo: TestData.getFullEgo(),
    };
    let potion = new UnknownPotion(Player.INSTANCE);
    const {success, state} = potion.castThis(s);
    expect(success).toBe(true);
    // TODO: test listeners
  });
});
