import SpellFromPotion from "../SpellFromPotion.js";
import EventSystem from "../events/EventSystem.js";
import EventTypes from "../events/EventTypes.js";
import $log from  "../../utils/log.js";

export default class InitialPotion extends SpellFromPotion {

  constructor(owner)  {
    super(owner);
    this.nameID = "potion_initial";
  }
  
  getRequiredMana() {
    return 0;
  }

  castThis(state) {
    $log.d("InitialPotion", "using of potion");
    state = EventSystem.INSTANCE.notifyListenersState(state, this.getInfo().getID(), EventTypes.USED);

    window.effects.addEffect('blinker', 3000);
    return {
      success: true,
      state
    };
  }

  takeEffect() { }
}
