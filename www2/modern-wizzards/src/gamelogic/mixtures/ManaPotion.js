import SpellFromPotion from "../SpellFromPotion.js";
import PlayerEgo from "../../PlayerEgo.js";

export default class ManaPotion extends SpellFromPotion {

  constructor(owner)	{
    super(owner);
    this.nameID = "potion_mana";
    this.mixtureColor = 0x0000FF;
  }
	
  castThis(state) {
    return {
      success: true,
      state: PlayerEgo.addManaPoints(state, 100),
    };
  }
}

