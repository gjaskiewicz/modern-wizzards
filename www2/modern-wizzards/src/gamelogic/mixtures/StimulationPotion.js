import SpellFromPotion from "../SpellFromPotion.js";
import PlayerEgo from "../../PlayerEgo.js";
import {ITERATIONS_PER_SECOND} from "../../Constants.js";

export default class StimulationPotion extends SpellFromPotion {

  constructor(owner) {
    super(owner);
    this.nameID = "potion_stimulation";
    this.mixtureColor = 0xFF00FF;
    this.turnDuration = 30 * 60; // dziala minute
  }

  castThis(state) {
    state = super.castThis(state).state;
    state.playerEgo = PlayerEgo.addHealthPoints(state.playerEgo, 50, true);
    state = PlayerEgo.addManaPoints(state, 50, true);
    return {
      success: true,
      state
    };
  }

  takeEffect(state) {
    state = super.takeEffect(state);
    if(this.turnDuration % ITERATIONS_PER_SECOND == 0) {
      if(state.playerEgo.health > state.playerEgo.maxHealthPoints) {
        state.playerEgo = PlayerEgo.takeHealth(state.playerEgo, 1);
      }
      if(state.playerEgo.mana > state.playerEgo.maxManaPoints) {
        state.playerEgo = PlayerEgo.takeMana(state.playerEgo, 1);
      }
    }
    return state;
  }
}
