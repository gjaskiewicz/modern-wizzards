import SpellFromPotion from "../SpellFromPotion.js";

export default class AntidotePotion extends SpellFromPotion {

  constructor(owner)  {
    super(owner);
    this.nameID="potion_antidote";
    this.mixtureColor = 0xFF00FF00;
  }
  
  castThis(state) {
    state = this.owner.removeUnderEffectOfSpell(state, "spell_infection");
    return {success: true, state};
  }

  takeEffect() { }
}
