import React, { Component } from 'react';

export default class ToplevelErrorCatcher extends Component {
    constructor(props) {
      super(props);
    }
  
    componentDidCatch(error, info) {
      console.log(error);
    }
  
    render() {
      return (this.props.children);
    }
  }