export default { 
  STATE_ERROR:     -1,
  STATE_INIT:       0,  // przed startem gry / PRZED OKREŚLENIEM LOKALIZACJI! 
  STATE_INTRO:      1,  // poczatkowe akcje wprowadzajace /raczej zbedne   
  STATE_WALKING:    2,  // normalna gra
  STATE_FIGHTING:   3,  // stan walki 
  STATE_PLAYERDEAD: 4,  // gracz umarł - powrót do SafeZone.
}