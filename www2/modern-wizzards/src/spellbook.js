const SPELLBOOK = `
make(Ing, X) :- spell(X), has_ingredients(X, Ing).

eq(X, X).

mix([X, Y, Z], Xa, Ya, Za) :- perm(X,Y,Z, Xa, Ya, Za).
mix([X, Y], Xa, Ya, void) :- perm(X,Y, Xa, Ya).
mix([X], Xa, void, void) :- perm(X, Xa).
perm(X,Y,Z, X,Y,Z).
perm(X,Y,Z, X,Z,Y).
perm(X,Y,Z, Y,X,Z).
perm(X,Y,Z, Y,Z,X).
perm(X,Y,Z, Z,X,Y).
perm(X,Y,Z, Z,Y,X). 
perm(X,Y, X,Y).
perm(X,Y, Y,X).
perm(X, X).

# - zatwierdzone
has_ingredients(potion_antidote, Ing)  :- mix(Ing, X, Y, Z), eq(X, item_garlic), eq(Y, item_swallowweed), eq(Z, item_fernflower).
has_ingredients(potion_mana, Ing)  :- mix(Ing, X, Y, Z), eq(X, item_smurfblood), eq(Y, item_4clover), eq(Z, item_swallowweed).
has_ingredients(potion_health, Ing)  :- mix(Ing, X, Y, Z), attr_healing(X), attr_healing(Y), attr_healing(Z).
has_ingredients(potion_luckjelly, Ing)  :- mix(Ing, X, Y, Z), eq(X, item_4clover), eq(Y, item_bunnyleg), eq(Z, item_daisyflower).
has_ingredients(potion_stimulation, Ing)  :- mix(Ing, X, Y, Z), eq(X, item_cornerstone), eq(Y, item_szczeciogon), eq(Z, item_mandragora).
has_ingredients(potion_invisibility, Ing)  :- mix(Ing, X, Y, Z), eq(X, item_ectoplasma), eq(Y, item_rozkovnik), eq(Z, item_fernflower).
has_ingredients(potion_perception, Ing)  :- mix(Ing, X, Y, Z), eq(X, item_cateye), eq(Y, item_swallowweed), eq(Z, item_rozkovnik).

# - declare potions!
spell(potion_health).
spell(potion_mana).
spell(potion_stimulation).
spell(potion_invisibility).
spell(potion_antidote).
spell(potion_perception).
spell(potion_luckjelly).

# knowledge
attr_healing(item_daisyflower).
attr_healing(item_fernflower).
attr_healing(item_garlic).
attr_healing(item_mandragora).
attr_lucky(item_bunnyleg).
attr_lucky(item_4clover).
attr_lucky(item_daisyflower).
`;

export default SPELLBOOK;