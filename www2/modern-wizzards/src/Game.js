import React from 'react';
import $log from "./utils/log.js";
import R from "./R.js";
import MWToast from "./ui/MWToast.js";
import PlayerEgo from "./PlayerEgo.js";
import Units from "./gis/Units.js";
import WorldContainer from "./gamelogic/geo/WorldContainer.js";
import AchivContainer from "./gamelogic/achivements/AchivContainer.js";
import CollectPlant from "./gamelogic/inventory/CollectPlant.js";
import Inventory from "./gamelogic/Inventory.js";
import Fight from "./gamelogic/Fight.js";
import Spell from "./gamelogic/Spell.js";
import MWTime from "./utils/MWTime.js";
import ManaZone from "./gamelogic/world/ManaZone.js";
import GameState from "./GameState.js";
import GameSaver from "./utils/GameSaver.js";
import EventSystem from "./gamelogic/events/EventSystem.js";
import EventTypes from "./gamelogic/events/EventTypes.js";
import {LogManaAdder} from './gamelogic/regen/ManaAdder.js';
import {LinearHpAdder} from './gamelogic/regen/HealthAdder.js';

const TAG = "Game";

export default class Game { 
  objectsToRestore = [ ];

  static instance = null;

  // TODO: remove
  static State = GameState;
  
  static getGame() {
    if(!window.game) {
      window.game = new Game();
    }
    return window.game;
  }

  constructor() {
    this.worldContainer = new WorldContainer();
    this.achivContainer = new AchivContainer();
  }

  getAchivContainer() {
    return this.achivContainer;
  }
  
  /**
   * Gra wczytana z sejwa. DRegeneruje tez mana i zdrowko. 
   * @param game
   */
  static reloadGame(state, gameController) {
    $log.i(TAG, "GAME RELOADED");
    
    //Regenerate only when player was not dead
    if(state.gameState == Game.State.STATE_PLAYERDEAD) return state;
    
    let secondsInSleep = 0;
    if (state.saveData.lastGameSaveMs) {
      var d = new Date();
      secondsInSleep = (d.getTime() - state.saveData.lastGameSaveMs) / 1000;
      $log.i(TAG, "SECONDS SINCE SAVE: " + secondsInSleep);
    } else {
      secondsInSleep = 60 * 60 * 12; // 12 godzin
      $log.i(TAG, "UNKNOWN SECONDS SINCE SAVE! ");
    }
    
    // liniowo, 100 pkt w 3000 sek = prawie godzina
    if(state.gameState == Game.State.STATE_WALKING)  {
      const hpAdder = new LinearHpAdder();
      const hpToAdd = hpAdder.hpToAdd(state.playerEgo.health, secondsInSleep);
      state.playerEgo = PlayerEgo.addHealthPoints(state.playerEgo, hpToAdd, false);
    }
    // full regen w 4 godziny
    const baseMana = state.playerEgo.mana;
    const manaAdder = new LogManaAdder();
    const manaToAdd = manaAdder.manaToAdd(
      baseMana, 
      secondsInSleep, 
      state.playerEgo.manaRegenFactor);

    state = PlayerEgo.addManaPoints(state, manaToAdd, false);
    state = Game.getGame().getAchivContainer().getAchivDays().nextRun(state);
    state.debug['mana_initialGain'] = manaToAdd;
    state.debug['mana_base'] = baseMana;
    return state;
  }

  static saveGame(state) {
    GameSaver.saveGame(state);
  }

  checkInteractions(state) {
    if(state.playerEgo.locationIsNotSet) {
      return state;
    }
    // w walce nie sprawdzamy
    if(state.gameState == Game.State.STATE_FIGHTING) { 
      return state; 
    }

    const location = PlayerEgo.getPlayerLocation(state.playerEgo);
    for(let obj of this.worldContainer.getAllGeneratedObjs()) {
      //$log.d(TAG, "Checking " + worldContainer.creatures.size() + "creature objects");
      if (obj.shouldInteract(location)) {
        state = obj.interact(state);
      }
    }  

    const otherObjectsClone = this.worldContainer.getAllQuestObjets();
    for(let obj of otherObjectsClone) {
      if (obj.shouldInteract(location)) {
        state = obj.interact(state);
      }
    }

    return state;
  }

  locationChanged(state, l) {
    if (l == null) { 
      return state; 
    }
    let newState = {...state};
    newState.playerEgo.lat = l.getLatitude()
    newState.playerEgo.lng = l.getLongitude();
    newState.debug.locationUpdates = newState.debug.locationUpdates + 1;
  
    newState = this.getAchivContainer().getAchivDistanceTravel().addMetersTraveled(newState, l);
    newState = this.getAchivContainer().getAchivDistanceVariety().addMetersTraveled(newState, l);

    if (!state.playerEgo.setLocation) {
      newState.playerEgo.setLocation = true;
/*
      mainActivity.stopAnimationOnLogo();
      mainActivity.mainMenuView.invalidate();
*/
      MWToast.toast(R.strings.game_gps_loc_set);
      this.initiateGameState(state);

      newState = EventSystem.INSTANCE.notifyListenersState(
        newState, null, EventTypes.GAME_STARTED);
      /* // emit eventu dla QE 
      Intent i = new Intent("com.jutsugames.modernwizzards.LOCATION_SET");
      Game.mainActivity.sendBroadcast(i);
      */
    }
    return newState;
  }

  initiateGameState(state) {
    $log.d(TAG, "initiateGameState");
    const gp = PlayerEgo.getGeoPoint(state.playerEgo);
    this.worldContainer.generateObjects(state, gp);
    if(state.gameState != Game.State.STATE_PLAYERDEAD) { 
      state.gameState = Game.State.STATE_WALKING;

      //wyświetlenie poczatkowych obiektów na mapie
      $log.i(`generate ${this.worldContainer.getAllGeneratedObjs().length} objects on map`);
      for(let obj of this.worldContainer.getAllGeneratedObjs()) {
        if (Units.optimizedDistanceCheck(
              obj.getGeoPoint(), 
              gp, 
              PlayerEgo.getSightRange(state.playerEgo))) {
          obj.display(true);
        }
      }
    }
  }

  collectItem(state, /* CollectableItem */ collectableItem) {
    const invItem = collectableItem.getItem();
    if (invItem instanceof CollectPlant) {
      const popularity = Math.max(invItem.popularity, 20);
      const extraExp = Math.floor(200 / popularity);
      
      state = {...state,
        playerEgo: {
          ...state.playerEgo,
          inventory: Inventory.get().addPlant(state.playerEgo.inventory, invItem.getNameID())
        }
      };

      state = this.getAchivContainer().getAchivPlants().addCollectedPlant(state, invItem.getNameID());
      state = {
        ...state,
        playerEgo: PlayerEgo.addExperience(state.playerEgo, extraExp),
      };
//    } else if (invItem instanceof SpellToken) {
//      playerEgo.inv.addMixture((SpellToken)invItem);
    } else {
      $log.e(TAG,"unknown type of collectable item");
    }
    state = this.removeMapItem(state, collectableItem);
    state = collectableItem.collected(state);

    Game.saveGame(state); 
    return state;
  }

  useItem(item) {
    if (item instanceof Spell) {
    }
  }

  removeMapItem(state, collectableItem) {
    $log.d(TAG, "removeMapItem " + collectableItem.getItem().getNameID());
    collectableItem.display(false);
    let worldContainer = state.worldContainer;
    if(collectableItem.isGenerated) {
      const day = MWTime.day();
      if(worldContainer.saveDate != day)  {
        worldContainer = {
          creaturesDefeated: [ ],
          gatheredPlants: [ ],
          saveDate: day,
        };
      }
      worldContainer = {
        ...worldContainer,
        gatheredPlants: [...worldContainer.gatheredPlants, collectableItem.generationHash],
      }
      
      collectableItem.homeLocation.items = 
          collectableItem.homeLocation.items.filter(i => i !== collectableItem);
    } else {
      this.worldContainer.removeQuestObject(collectableItem);
    }

    return {
      ...state,
      worldContainer,
    }
  }

  showFightInstruction(state, creature) {
    let genericOverlayParams = {
      yesHandler: {
        title: R.strings.game_global_ok,
        invoke: () => window.gameController.applyToState(
          s => this.startFight(window.gameController.showOverlayState(s, null), creature)
        ),
      },
      noHandler: null,
      renderContent: () => (<span>{R.strings.fight_instructions}</span>),
    };
    state = window.gameController.showOverlayState(state, "alert_generic");
    return {
      ...state,
      ui: {
        ...state.ui,
        genericOverlayParams
      }
    };
  }

  startFight(state, creature) {
    state = {
      ...state,
      playerEgo: PlayerEgo.endOutOfBody(state.playerEgo, false),
    };
    state = window.gameController.showNextScreenState(state, 'fight', {fight: new Fight(creature)});
    return {
      ...state,
      gameState: Game.State.STATE_FIGHTING,
    };
  }

  playerFightsCreature(state, creature) {
    if(!state.playerEgo.firstFightDone) {
      return this.showFightInstruction(state, creature);
    } else {
      return this.startFight(state, creature);
    }
  }
  
  playerRunsFromCreature(state, creature) {
    //TODO: zabrac health, mana, honor, experience?
    // sprawia, że creature przez chwile nie atakuje znowu.
    creature.dontAttackUntil = MWTime.s() + 30; // side-effect
    return PlayerEgo.addManaPoints(state, -20);
  }

  /** 
   * Wywolywane w chwili smierci wszelakiej.
   */
  playerDies(state) {
    // notifyListeners(nameID, EventTypes.KILLED, null);
    if(!state.playerEgo.killedOnce) {      
      const q = {
        isNarration: true,
        getFromImg: () => <span />,
        getFullMessageText: () => R.strings.first_player_death,
      };
      state = {
        ...state,
        ui: {...state.ui, activeScreen: 'questMessage', questMessage:q, previousScreens: [ ]}
      }
    }
    state = {
      ...state,
      gameState: Game.State.STATE_PLAYERDEAD,
      playerEgo: {
        ...state.playerEgo,
        underEffectOfSpell: [ ],
        mana: 0,
        killedOnce: true,
      }
    };
    GameSaver.saveGame(state);
    return state;
  }

  refreshMap(state) {
    const sightRange = PlayerEgo.getSightRange(state.playerEgo);
    const playerLoc = PlayerEgo.getPlayerLocation(state.playerEgo);
    this.worldContainer.generateObjects(state, playerLoc);
    const objects = this.worldContainer.getAllGeneratedObjs();
    for (let obj of objects) {
      if (Units.optimizedDistanceCheck(obj.getGeoPoint(), playerLoc, sightRange)) {
        if (state.gameState != GameState.STATE_PLAYERDEAD || obj instanceof ManaZone) {
          obj.display(true);
        } else {
          obj.display(false);
        }
      } else {
        obj.display(false);
      }
    }
  }

  showNewMessageAlert(state, quest) {
    if (state.ui.activeOverlay) {
      MWToast.toast(R.strings.alert_message_title);
      return state;
    } else {
      let genericOverlayParams = {
        yesHandler: {
          title: R.strings.alert_message_show,
          invoke: () => window.gameController.applyToState(
            s => { 
              s = window.gameController.showOverlayState(s, null);
              s = quest.openMessage(s);
              return window.gameController.showNextScreenState(s, 'questMessage', { questMessage:quest });
            }
          ),
        },
        noHandler: {
          title: R.strings.alert_message_later,
          invoke: () => window.gameController.applyToState(
            s => window.gameController.showOverlayState(s, null)
          ),
        },
        infoOnly: true,
        getTitleText: () => (<span>{R.strings.alert_message_title}</span>),
        renderContent: () => (<span>{R.strings.alert_message_content}</span>),
      };
      state = window.gameController.showOverlayState(state, "alert_generic");
      return {
        ...state,
        ui: {
          ...state.ui,
          genericOverlayParams
        }
      };
    }
  }

  showDiscalimer(s) {
    let genericOverlayParams = {
      yesHandler: {
        title: R.strings.disclaimer_accept,
        invoke: () => window.gameController.applyToState(
          s => {
            s = {...s, 
              options: {
                ...s.options,
                TERMS_OF_USE_ACCEPTED: true,
              }
            }
            Game.saveGame(s);
            return window.gameController.showOverlayState(s, null)
          }
        ),
      },
      noHandler: { 
        title: R.strings.disclaimer_reject,
        invoke: () => {
          // TODO: close window
        }
      },
      getTitleText: () => (R.strings.disclaimer_title),
      renderContent: () => (<div>{R.strings.disclaimer_text}</div>),
    };
    const state = window.gameController.showOverlayState(s, "alert_generic");
    return {
      ...state,
      ui: {
        ...state.ui,
        genericOverlayParams
      }
    };
  }

  getAllObjects() {
    return [
      ...this.worldContainer.getAllGeneratedObjs(), 
      ...this.worldContainer.getAllQuestObjets(),
    ];
  }
}
