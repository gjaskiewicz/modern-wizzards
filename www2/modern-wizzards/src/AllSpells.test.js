import AllSpells from './AllSpells.js';

describe('AllSpells', () => {
  it('should determine potions #1', () => {
    const result = AllSpells.get().possiblePotions(['item_garlic', 'item_swallowweed', 'item_fernflower']);
    expect(result.length).toBe(1);
    expect(result[0]).toBe('potion_antidote');
  });

  it('should determine potions #2', () => {
    const result = AllSpells.get().possiblePotions(['item_daisyflower', 'item_fernflower', 'item_garlic']);
    expect(result.length).toBe(1);
    expect(result[0]).toBe('potion_health');
  });

  it('should suggest single potion', () => {
    const result = AllSpells.get().suggestOne(['item_daisyflower', 'item_fernflower']);
    expect(result).toEqual({item: 'item_daisyflower', result: 'potion_health'});
  });

  it('should suggest all potion', () => {
    const result = AllSpells.get().suggestAll(['item_daisyflower', 'item_fernflower']);
    expect(result.length).toBeGreaterThan(1);
  });
});

