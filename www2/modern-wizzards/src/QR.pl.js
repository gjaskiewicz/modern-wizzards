
import React, { Component } from 'react';
import res_profile_william from "./assets/quest/profile_william.jpg";
import res_logo_bf_s from "./assets/quest/logo_bf_s.png";
import res_ilustration_5 from "./assets/quest/ilustration_5.png";
import res_narration_initial_potion_effect from "./assets/quest/narration_initial_potion_effect.jpg";
import res_profile_anette from "./assets/quest/profile_anette.jpg";
import res_ilustration_12 from "./assets/quest/ilustration_12.png";
import res_collectplants_narration_use_telekinesis from "./assets/quest/collectplants_narration_use_telekinesis.jpg";
import res_profile_anthony from "./assets/quest/profile_anthony.jpg";
import res_ilustration_13_v2 from "./assets/quest/ilustration_13_v2.png";
import res_ilustration_7 from "./assets/quest/ilustration_7.png";
import res_collectplants_narration_close_to_plant from "./assets/quest/collectplants_narration_close_to_plant.jpg";
import res_fleeing_rabbit_narration_close_to_rabbit from "./assets/quest/fleeing_rabbit_narration_close_to_rabbit.jpg";

export default class QR {
  static drawables = {
    ilustration_5: res_ilustration_5,
    ilustration_7: res_ilustration_7,
    ilustration_12: res_ilustration_12,
    ilustration_13_v2: res_ilustration_13_v2,
    collectplants_narration_use_telekinesis: res_collectplants_narration_use_telekinesis,
    profile_william: res_profile_william,
    logo_bf_s: res_logo_bf_s,
    narration_initial_potion_effect: res_narration_initial_potion_effect,
    collectplants_narration_close_to_plant: res_collectplants_narration_close_to_plant,
    fleeing_rabbit_narration_close_to_rabbit: res_fleeing_rabbit_narration_close_to_rabbit,
    profile_anette: res_profile_anette,
    profile_anthony: res_profile_anthony,
  }

  static strings = {
    quest_profile_bf: (<img src={res_logo_bf_s} width="100" align="left" />),
    quest_profile_william: (<img src={res_profile_william} width="100" align="middle|left" />), 
    quest_profile_anette: (<img src={res_profile_anette} width="100" align="middle\|left" />), 
    quest_profile_anthony: (<img src={res_profile_anthony} width="100" align="middle\|left" />),         
    quest_profile_unknown: (<span />),         
    
    // Initial quest
    quests_initial_message_title: "Witaj w Modern Wizards",
    quests_initial_message_from: "Bayery Farmaceutics",
    quests_initial_message_content: (
    <div>
      Witaj!
      <br/>
      <br/>W imieniu firmy Bayery Farmaceutics pragniemy poinformować Cię, że Twoje zgłoszenie do projektu Modern Wizards zostało rozpatrzone pozytywnie. Podczas zbliżających się testów, Ty oraz pozostali ochotnicy, będziecie wypełniać proste zadania, znajdując się pod wpływem jednego z naszych nowych środków. Zadania te mają charakter terenowy i mogą być realizowane w czasie wolnym, nie zakłócając Twojego porządku dnia. Prosimy jednak o prowadzenie skrupulatnych notatek oraz częste raportowanie wszelkich spostrzeżeń.
      <br/>W otrzymanym od nas <a href="#screen=inventory/potions">wyposażeniu</a> znajdziesz teraz fiolkę z substancją, której działanie pragniemy przeanalizować. Wypij ją, gdy tylko będziesz gotów rozpocząć swój udział w badaniach.*
      <br/>
      <br/>Dziękujemy i powodzenia,
      <br/>Zespół Bayery Farmaceutics
      <br/>*Uwaga: Aby testowany środek zadział prawidłowo musisz znajdywać się na otwartej przestrzeni, pod gołym niebem.
    </div>),
  narration_initial_potion_effect: (
    <div>
      <img src={res_narration_initial_potion_effect} width="100%" />
      Gorzki smak specyfiku początkowo przyprawia cię o mdłości. Jednak, kiedy pociągasz ostatni łyk, nudności ustępują i przeradzają się w uczucie ciepła – jego pulsujące fale rozchodzą się po twoim ciele od splotu słonecznego aż po koniuszki palców i czubek głowy. Towarzyszy im przyjemne mrowienie, które z każdą kolejną sekundą zdaje się narastać.
      <br/>
      <br/>Nagle twój organizm przeszywa spazm bólu. Nogi uginają się pod tobą, reszta ciała zwija w kłębek, lecz ty sam dalej stoisz wyprostowany, patrząc na siebie z góry. Świat dookoła uległ fantastycznej przemianie – kolory nabrały intensywniejszych odcieni, budynki zmieniły się w strzeliste wieże, zaś ludzie stali się przejrzyści, jakby niematerialni.
      <br/>
      <br/>Po chwili zostajesz z powrotem wessany do swojego ciała. Wstajesz, powoli otwierasz oczy i stwierdzasz, że chyba wszystko wróciło do normy. Ale czy na pewno…?
    </div>),

  // Go to point quest
  gotopoint_quest_title: "Test Bilokacji",
  gotopoint_quest_from: "dr W. Alpine BF",
  gotopoint_quest_content: (
    <div>
      Witaj,
      <br/>
      <br/>nazywam się William Alpine i jestem członkiem Sekcji Badawczej Bayery Farmaceutics. Mam nadzorować Twój udział w testach, więc to właśnie ode mnie będziesz otrzymywać zadania oraz wytyczne.
      <br/>Przejdę od razu do rzeczy - wyznaczyłem w <a href="#screen=map">Twojej okolicy</a> cztery punkty. Udaj się do dowolnego z nich. Pomoże nam to dokonać ostatnich kalibracji komunikatora.
      <br/>
      <br/>Skontaktuję się z Tobą, kiedy będziesz na miejscu.
      <br/>
      <br/>Z poważaniem,
      <br/>dr W. Alpine
    </div>),
    gotopoint_quest_narration_1: (
      <div>
        Doskonale. Sygnał jest mocny, a komunikator działa bez zarzutu!
        <br/>
        <br/>Teraz zajmiemy się czymś znacznie bardziej interesującym niż spacerowanie po okolicy. Wyznaczyliśmy Ci kolejne miejsce do którego musisz dotrzeć. Jednak tym razem masz dostać się tam nie ciałem, lecz duchem.
        <br/>Dotychczasowe badania pokazują, że nasz specyfik uaktywnia pewną paranormalną zdolność, którą określamy mianem <a href="#screen=spells/global">Bilokacji</a>. Usiądź wygodnie, zamknij oczy, skup się, weź głęboki oddech i... pomyśl o tym że gdzieś idziesz.
        <br/>
        <br/>Powodzenia,
        <br/>dr W. Alpine
      </div>),
    gotopoint_quest_narration_2: (
      <div>
        Gratuluję, udało Ci się! Z czasem Bilokacja stanie się dla Ciebie równie naturalna, jak oddychanie. Bywa wyczerpująca, ale wykorzystuj ją zawsze kiedy będziesz mieć problem z dostaniem się do celu. 
        <br/>
        <br/>Teraz trochę odpocznij i nabierz energii. Niedługo otrzymasz wiadomość dotyczącą następnego zadania.
        <br/>
        <br/>Z poważaniem,
        <br/>dr W. Alpine
      </div>),
    gotopoint_quest_have_to_use_oob: "Użyj czaru \"Bilokacja\", aby się tu dostać.", 

    // Collect plant quest
    collectplants_quest_title: "Test Nadpercepcji",
    collectplants_quest_from: "dr W. Alpine, BF ", 
    collectplants_quest_content: (
    <div>
      Witaj,
      <br/>
      <br/>
      zgodnie z zapowiedzią mam dla Ciebie nowe wytyczne - chcemy zbadać granice nadpercepcji rozbudzonej w Tobie przez nasz środek. Rozejrzyj się w <a href="#screen=map">wyznaczonych miejscach</a> w poszukiwaniu roślin o wyjątkowych właściwościach. Przesyłam Ci też <a href="#screen=stats">pakiet danych</a>, które powinny ułatwić ich identyfikację.
      Pobierz próbki i czekaj na dalsze instrukcje
      <br/>
      <br/>
      Z poważaniem,
      <br/>
      dr W. Alpine
    </div>),
    collectplants_narration_close_to_plant: (
    <div>
      <img src={res_collectplants_narration_close_to_plant} width="100%" />
      W miarę jak zbliżasz się do wskazanego miejsca, odczuwasz coraz bardziej jej obecność. Zauważasz że czułeś ją już wcześniej, teraz jednak wiesz gdzie jest źródło tego uczucia. 
      Kiedy roślina jest już w zasięgu twojego wzroku, zauważasz, że świetlista aura i soczyste barwy magicznej flory w niezwykły sposób kontrastują z wyblakłym otoczeniem. Sama roślina wygląda jakby była rozmyta i przeźroczysta
    </div>),
    collectplants_narration_use_telekinesis: (
    <div>
      <img src={res_collectplants_narration_use_telekinesis} width="100%" />
      Tym razem, gdy sięgasz po roślinę, ta jakby samoistnie wskakuje w twoje ręce. Może udałoby Ci się to powtórzyć na większą odległość...? 
      <br/>
      <br/>
      <a href="#screen=spells/global">[nauczyłeś się Telekinezy]</a>
    </div>),
    collectplants_narration_all_collected: (
    <div>
      Wygląda na to, że zebrałeś wszystkie próbki. Dostarcz je do <a href="#screen=map">umówionego miejsca</a> i przekaż pracownikowi BF, którego tam spotkasz.
    </div>),
    collectplants_narration_end: (
    <div>
      Świetnie, właśnie otrzymałem informację, że składniki zostały dostarczone. Dziękuję w imieniu zespołu BF, i gratuluję ukończenia kolejnego etapu testów!
      <br/>
      <br/>Pozdrawiam,
      <br/>dr W. Alpine
    </div>),   

    // Mana source quest
    quest_mana_sources_title: "Test Energetyzacji",
    quest_mana_sources_from: "Annette Renault, BF",
    quest_mana_sources_initial: (
    <div>
      Witaj,
      <br/>
      <br/>tutaj Anne Renault z Działu ds. Medycyny Paranormalnej. Mój zwierzchnik poprosił mnie, żebym zleciła Ci małą, nieoficjalną misję wykraczającą nieco poza standardowy tok badań.
      <br/>Widzisz, każde użycie magicznej zdolności kosztuje Cię energię, którą możesz szybko zregenerować w tzw. Źródłach Mana występujących na przecięciach żył wodnych. Zaznaczyłam na Twojej mapie <a href="#screen=map">kilka takich miejsc</a>. Odwiedź teraz Źródło i zostań w nim do czasu, aż Twój organizm dostroi się do przepływów energii Mana i w pełni zregenerujesz energię.
      <br/>
      <br/>Pozdrawiam,
      <br/>Anne
    </div>),
    quest_mana_sources_narration_mana_full: (
    <div>
      Powinieneś być w stanie samodzielnie odnajdywać pobliskie Źródła Mana.
      <br/>Korzystaj z nich często, inaczej Twoja mana może regenerować się nawet kilka godzin.
      <br/>
      <br/>Pozdrawiam,
      <br/>Anne
    </div>),

    // Fleeing rabbit
    fleeing_rabbit_quest_title: "Sytuacja alarmowa",
    fleeing_rabbit_quest_from: "dr W. Alpine, BF ",
    fleeing_rabbit_quest_content: (
    <div>
      Witam,
      <br/>
      <br/>wystąpiły pewne nieoczekiwane komplikacje. Jeden z naszych obiektów doświadczalnych uciekł z laboratorium. Wiemy, że rozwinęły się w Tobie zdolności telekinetyczne, które z pewnością będą bardzo pomocne w tej sytuacji.
      <br/>Poza tym, jesteś najbliżej miejsca zdarzenia ze wszystkich agentów. Zaznaczam Ci na mapie <a href="#screen=map">miejsce</a>, gdzie ostatnio widziano zbiegłe zwierzę. Postaraj się je zlokalizować i unieszkodliwić.
      <br/>
      <br/>Liczymy na Ciebie,
      <br/>dr. W. Alpine
    </div>),
    fleeing_rabbit_narration_close_to_rabbit: (
    <div>
      <img src={res_fleeing_rabbit_narration_close_to_rabbit} width="100%" />
      W miarę jak zbliżasz się do wskazanego punktu zaczynasz odczuwać niepokój. Wzdłuż twojego kręgosłupa przebiega dreszcz, a przedramiona pokrywa gęsia skórka.
      <br/>Stwór musi być gdzieś bardzo, bardzo blisko...
    </div>),
    fleeing_rabbit_narration_rabbit_killed: (
    <div>
      <img src={res_ilustration_5} width="100%" /> 
      <br/>Kiedy zmutowany królik pada bez życia, spostrzegasz, że otacza go taka sama świetlista aura, jak ta wokół roślin, które wcześniej znalazłeś. Czyżby on sam też miał nadprzyrodzone właściwości...? 
      <br/>Chociaż okolica wydaje się być już bezpieczna, nie opuszcza cię dziwne uczucie, że w pobliżu nadal jest coś niezwykłego. Coś, czego obecności jeszcze nie odczuwałeś. 
      <br/>To stworzenie nie było chyba jedynym przedstawicielem swojego gatunku...
    </div>),
    fleeing_rabbit_quest_end: (
    <div>
      Świetna robota! Już się baliśmy, że bydle zwieje na dobre i będzie siać popłoch, jak jakaś chupacabra! Dostaniesz trochę wolnego od badań – przynajmniej tak możemy Ci się odwdzięczyć.
      <br/>
      <br/>Muszę przypomnieć Ci, że wszystkie wydarzenia związane z testami są objęte klauzulą poufności. Ten incydent bardzo zmartwił naszych przełożonych. 
      <br/>
      <br/>Pozdrawiam,
      <br/>dr. W. Alpine
    </div>),

    // Alchemy quest
    quest_alchemy_initial_title: "Test Alchemizacji",
    quest_alchemy_initial_from: "Annette Renault, BF",
    quest_alchemy_initial: (
    <div>
      Cześć,
      <br/>
      <br/>Trochę Ci się oberwało, co? Mam dla Ciebie w związku z tym pewną poradę:
      <br/>Kiedy kolejny raz zbierzesz Kwiat Paproci, Allium lub Chamomile, zetrzyj je na proch i zmieszaj ze sobą, a następnie zaparz jak herbatę. 
      <br/>Wywar powinien pomóc zagoić rany zadane przez to biedne zwierzę.
      <br/>
      <br/>Na zdrowie,
      <br/>Anne
    </div>),

    // Teletransportion quest
    quest_teletransport_title: "Teletransportacja",
    quest_teletransport_from: "dr W. Alpine, BF",
    quest_teletransport_initial: (
    <div>
      Witaj,
      <br/>
      <br/>mamy ostatnio kłopoty z transportem pewnego specyfiku między oddziałami firmy i chcielibyśmy, wykorzystać twoje zdolności w tej sprawie. Raporty kurierów są niejasne, wspominają o "latających straszydłach" oraz "wszechogarniającej bezsilności". Miej się na baczności.
      <br/>Jedna z przesyłek została dzisiaj porzucona – przesyłam Ci jej <a href="#screen=map">przybliżone współrzędne</a>. Znajdź ją i dostarcz do naszego laboratorium.
      <br/>
      <br/>Uważaj na siebie,
      <br/>dr W. Alpine
    </div>),
    quest_teletransport_narration_package_received: (
    <div>
        Dostałem sygnał że przejąłeś przesyłkę. Udaj się teraz do <a href="#screen=map">miejsca docelowego</a>, gdzie przekażesz przesyłkę przedstawicielowi Działu Chemicznego.
    </div>),    
    quest_teletransport_narration_end: (
    <div>
      Przekażę Twoje spostrzeżenia odpowiednim osobom. To już nie pierwszy taki przypadek, więc problem wydaje się nasilać. Uważaj na siebie. 
      <br/>
      <br/>Pozdrawiam,
      <br/>dr W. Alpine
    </div>),

    // Special mission - seals
    quest_seals_title: "Misja specjalna",
    quest_seals_from: "Anette Ranault, BF",
    
    quest_seals_initial: (
    <div>
      Witam ponownie,
      <br/>Tym razem mam dla Ciebie zadanie od samego pana Dee, więc słuchaj uważnie... 
      <br/>Niedaleko stąd, w wystającej z ziemi skale, znajdziesz zapieczętowane drzwi, niewidoczne dla zwykłego oka. Żeby je otworzyć potrzebujesz runy-klucza, która zaginęła w <a href="#screen=map">tej okolicy</a>
      <br/>Zdobądź ten artefakt i czekaj na dalsze wskazówki.
      <br/>
      <br/>Powodzenia,
      <br/>Anne
    </div>),
    
    quest_seals_narration_close_gnome: (
      // style="text-size: 17pt;"
    <div>
      Rozglądając się po okolicy nie widzisz nic nadzwyczajnego. Nagle dostrzegasz ruch przy ziemi. Spod włazu wychodzi malutki i bardzo brzydki człowieczek. Ma długą, spiczastą czapkę, wielkie stopy, a w równie wielkiej dłoni trzyma coś na kształt włóczni. Nie zwraca specjalnie na ciebie uwagi.
      <br/><img src={res_ilustration_7} width="100%" /> 
      <br/>Zauważasz, że na szyi ma iskrzący magicznym światłem poplątany, metalowy amulet. W tym momencie skrzat zwraca ku tobie swoją twarz i zdziwiony pyta skrzeczącym głosem:
      <br/>"Ty mnie widzisz?!"
      <br/>
      <span>
        <a href='#message/fight'>WALCZ</a>
        <br/>
        <br/>
        <a href='#message/talk'>ROZMAWIAJ</a>
      </span>
      <br/>
    </div>),
    
    quest_seals_gnome_before_first_question: (
    <div>
      <i>-Ciekawe, ciekawe. Duży jesteś, a mnie tylko małe dziatki widują. I to też rzadko.</i>
      <br />
      <br />Skrzat zauważa, że patrzysz na jego amulet na szyi. On zaś patrzy na to, co masz w ręku.
      <br />
      <br />
      -o, podoba Ci się moje świecuszko? Mnie się podoba to świecące ustrojostwo co masz w ręku. Możemy zagrać o nie w sza-ra-dy. Lubisz szarady? Lubisz? Jak zgadniesz, dostaniesz błysotkę, jak nie, to ja zabieram twoją. Khym-khym. No więc:
      <br />
    </div>),
    
    quest_seals_gnome_question_1: (
    <div>
      Zazwyczaj nocą chałturzy
      <br />Trupie śpiewając nuty
      <br />Choć od zwiastowania burzy
      <br />Słuch ma całkiem popsuty
      <br />
    </div>),

    quest_seals_gnome_question_1_answers: [
      (<a href='#message/good_answer_1'>CISZA</a>),
      (<a href='#message/wrong_answer_1'>WIATR</a>),
      (<a href='#message/wrong_answer_1'>ORGANISTA</a>),
    ],

    quest_seals_gnome_question_2: (
    <div>
      <br />Jednych boleśnie poucza
      <br />Innym daje zbawienie
      <br />Głód mu bez przerwy dokucza
      <br />Przenigdy zaś pragnienie
      <br />
    </div>),
    
    quest_seals_gnome_question_2_answers: [
      (<a href='#message/good_answer_2'>OGIEŃ</a>),
      (<a href='#message/wrong_answer_2'>ZBAWICIEL</a>),
      (<a href='#message/wrong_answer_2'>PRĄD</a>),
    ],
    
    quest_seals_gnome_question_3: (
    <div>
      <br />Szukaj, a znajdziesz odpowiedź
      <br />Enigmy rozwiązanie
      <br />Należycie wzrokiem omieć
      <br />Szarady mojej granie
      <br />
    </div>),
    
    quest_seals_gnome_question_3_answers: [
      (<a href='#message/good_answer_3'>SENS</a>),
      (<a href='#message/wrong_answer_3'>NUTA</a>),
      (<a href='#message/wrong_answer_3'>SZYFR</a>),
    ],

    quest_seals_gnome_first_question_correct: (
    <div>
      Kiedy poprawnie odpowiedziałeś na zagadkę, skrzat skrzywił się i podrapał po głowie.
      <br />
      <br />
      <i>-Ooo, dobryś ty. Z tą nie będziesz taki cfany:</i>
      <br />
    </div>),

    quest_seals_gnome_second_question_correct: (
    <div>
      <i>-Uuuooouuggghhh. Dobra, masz tu moją najlepszą. Od 100 lat nikt jej nie rozwiązał!:</i>
      <br />
    </div>),
    
    quest_seals_gnome_all_questions_correct: (
    <div>
      <br />
      <i>-AAARRGGHH! Łajza!</i>
      <br />
      <br />
      Skrzat wyraźnie zdenerwowany zaczyna podskakiwać. Zdejmuje z szyi amulet i rzuca ci go pod nogi.
      <br />
      <br />
      <i>-A wsadź se go! Znikaj stąd i nie wracaj, bo cię jeszcze dopadnę!</i>
      <br />
    </div>),

    // Special mission - main
    quest_special_mission_title: "Misja specjalna, cd.",
    quest_special_mission_from: "Anette Ranault, BF",
    quest_special_mission_initial: (
    <div>
      Odzyskałeś artefakt?
      <br />Świetnie! Teraz możesz ruszać do podziemi, o których Ci mówiłam. W środku znajdziesz obiekt, który musisz przechwycić. Będąc na dole zachowaj szczególną ostrożność – nie wiem co może Ciebie tam spotkać.
      <br />
      <br />Trzymam kciuki,
      <br />Anne
      <br />
    </div>),

    quest_special_mission_narration_in_first_zone: (
      <div>
        <br />Kiedy zbliżasz się do wskazanych przez Anne drzwi, kamień runiczny w twojej kieszeni zaczyna drżeć i jarzyć się niebieskawym światłem. Dotykasz powierzchni wrót, a te samoistnie ustępują Ci z drogi.
        <br /><img src={res_ilustration_12} width="100%"  /> 
        <br />Spiralnymi schodami schodzisz do zakurzonej piwniczki, rozglądasz się i, gdy Twój wzrok przywyka wreszcie do ciemności, coś zauważasz – na środku pomieszczenia stoi mała urna otoczona kręgiem przedziwnych symboli. Za nią natomiast dostrzegasz olbrzymi, niedbale wyrzeźbiony z gliny posąg człowieka.
      </div>
    ),

    quest_special_mission_narration_final: (
    <div>
      <br />Po powierzchni urny przeskakuje kilka magicznych iskier, przez co wypuszczasz naczynie z rąk i rozbija się ono z trzaskiem na tysiąc kawałków. Przez chwilę wydaje ci się, że było zupełnie puste, jednak szybko orientujesz się w jak wielkim byłeś błędzie.
      <br />Wokół fragmentów popielnicy gromadzi się świetlista energia. Mimo, że nie czujesz wiatru, zaczyna ona wirować w powietrzu. Popioły unoszą się do góry, wirując, w końcu zbijają się w środku i przybierają kształt ludzkiej postaci. Kiedy formuje się twarz mężczyzny, ta uśmiecha się do ciebie i przemawia silnym, choć ochrypłym głosem:
      <br />
      <img src={res_ilustration_13_v2} width="100%"  /> 
      <br />- Witaj, przyjacielu. Nazywam się John Dee i winien Ci jestem podziękowania za uwolnienie. Mam wobec Ciebie dług wdzięczności.
      <br />Zostałeś obdarzony wielką mocą. Uważaj na siebie, bo możesz nie zdawać sobie sprawy z jak potężnymi siłami masz do czynienia. Tymczasem żegnaj, mam wiele do zrobienia.
      <br />
      <br />Widmo w mniegniu oka rozpływa się i zmienia w wiatr, który obiega cię dookoła i równie szybko znika z zasięgu twojego wzroku.
    </div>),

    // Final message
    quest_special_mission_security_alert_from: "Anthony Coldstone, BF",
    quest_special_mission_security_alert_title: "Naruszenie protokołu bezpieczeństwa",
    quest_special_mission_security_alert: (<div>
      <br />Testerze,
      <br />    
      <br />wykryliśmy, że kontaktowała się z Tobą nasza była laborantka, dr Anette Renault. Prosimy o zaprzestanie wszelkiej korespondencji z tą osobą oraz złożenie raportu z dotychczas odbytych rozmów w trybie pilnym.
      <br />
      <br />Anthony Coldstone,
      <br />Ochrona BF
    </div>),

    // Thanks message
    thanks_message_from: "Bayery Development",
    thanks_message_title: "Post Scriptum",
    thanks_message_content: (<div>
      Dziękujemy za udział w testach!
      <br />
      <br />Jeśli napotkałeś na błąd w grze lub chcesz się podzielić z nami swoimi uwagami, skorzystaj z <a href="openweb/http://www.modernwizards.pl/bugs-report">tego formularza.</a>
      <br />
      <br />Zapraszamy także na <a href="http://www.modernwizards.pl">naszą stronę internetową</a> i do polubienia naszego <a href="http://www.facebook.com/ModernWizardsPL">profilu na Facebooku</a>
      <br />
      <br />Pozdrawiamy,
      <br /><a href="openweb/http://www.modernwizards.pl/credits">Bayery Development</a>
    </div>),

    // Sidequest: Haunted house
    quest_haunted_house_from: "Hrabia Wundermann",
    quest_haunted_house_title: "Pogromca duchów",
    quest_haunted_house_content: (<div>
      Dzień dobry,
      <br />
      <br />skądinąd wiem, iż dysponujesz wachlarzem niecodziennych zdolności, które mógłbym wykorzystać.
      <br />Otóż kupiłem niedawno zabytkową posiadłość i dzieją się w niej kuriozalne rzeczy – zatrzaskują się za mną drzwi, przedmioty same zmieniają położenie, a w pustych korytarzach słychać nieraz kroki. Jak nic duch, pomyślałem!
      <br />Byłbym wdzięczny, gdyby zająć się w najbliższym czasie moim problemem. Zapewniam również, że ten niewielki wysiłek bardzo Ci się opłaci.
      <br />
      <br />Z wyrazami szacunku,
      <br />der Wundermann
    </div>),

    quest_haunted_house_end: (<div>
      Zaprawdę jest z Ciebie nie lada fachowiec w dziedzinie zjawisk nadprzyrodzonych! Sam parałem się niegdyś pokrewnym zajęciem, ale tyle to już lat minęło...
      <br />Zgodnie z naszą umową mam coś dla Pana(i). Oto przepis na eliksir niewidzialności – wystarczy doprowadzić porcję duchowej ektoplazmy do wrzenia i dodać doń pyłki z kwiatu paproci oraz cztery listki z jednej i tej samej koniczyny. Et voila, że tak powiem!
      <br />
      <br />Z wyrazami szacunku,
      <br />der Wundermann
    </div>),

    // Sidequest: Little Red Riding Hood
    quest_lrrh_from: "Ms. Warwitch",
    quest_lrrh_title: "Potrzebne antidotum",
    quest_lrrh_grandMa_zone_name: "Ms. Warwitch",
    quest_lrrh_content: (<div>
      Witaj młodzieńcze,
      <br />
      <br />nie znamy się, ale polecił mi Cię mój dobry znajomy. Piszę, bo od kiedy udziabał mnie ten pieruński wilk, doskwiera mi potworna infekcja. I chociaż jestem, tak jak i Ty, adeptką sztuk tajemnych,  nie mam sił aby uwarzyć dla siebie lekarstwo.
      <br />Zbierz proszę składniki na Antidotum: Allium, Jaskółcze Ziele oraz Kwiat Paproci, a następnie przygotuj z nich miksturę i przynieś je do mojej chatki. Podaruję Ci w nagrodę przepis na inną, bardzo przydatną miksturę.
      <br />
      <br />Pozdrawiam ciepło,
      <br />Babcia Jagoda
    </div>),
    quest_lrrh_without_antidote: (<div>
      Witaj młodzieńcze,
      <br />miło, że mnie odwiedziłeś, ale wiesz, że bardzo potrzebuję tego antidotum. Przynieś je proszę.
    </div>),
    quest_lrrh_end: (<div>
      Ach, dziękuję Ci, dziecko!
      <br />Bez tego Antidotum czerwie i muchy ucztowałyby na moim truchle nie później jak za dwa księżyce. Co ja, biedna, bym bez ciebie zrobiła...?
      <br />W nagrodę za Twój trud zdradzę ci przepis na Eliksir Mana: pochwyć jednego strumpfa i zmieszaj jego esencję z sokiem jaskółczego ziela oraz czterolistną koniczyną. Gotuj mieszankę przez trzynaście minut, a potem odstaw ją na kilka chwil, by przestygła. I już, gotowe!
    </div>),
  }
}
