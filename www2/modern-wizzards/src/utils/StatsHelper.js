import $log from "./log.js";

export default class StatsHelper {
  static randomFromDistr(population, scoreFunc, v) {
    if (v < 0 || v > 1) {
      $log.e("StatsHelper", `value ${v} out of range`);
    }
    let totalSum = 0;
    population.forEach(k => { totalSum += scoreFunc(k); });
    let cumulative = 0;
    let lastMember = null;
    if (totalSum == 0) {
      return null;
    }

    for (let k of population) {
      cumulative += (scoreFunc(k) / totalSum);
      if (v <= cumulative) {
        return k;
      }
    }
    $log.e("StatsHelper", `could not draw from distr with ${v}`);
    return null;
  }
}
