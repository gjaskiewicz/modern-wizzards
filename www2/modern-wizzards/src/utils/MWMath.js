export default class MWMath {
  static sinValues = null; 

  static sin(deg) {
    deg = Math.round(deg) % 360;
    if(deg<0) deg+=360;
    if(MWMath.sinValues == null) {
      MWMath.sinValues = [];
      for(let i = 0 ; i < 360 ; i++) {
        MWMath.sinValues[i] = Math.sin(MWMath.toRadians(i));
      }
    }
    return MWMath.sinValues[deg];
  }

  static toRadians(deg) {
    return deg * Math.PI / 180;
  }

  static toDegrees(rad) {
    return rad / Math.PI * 180;
  }
}
