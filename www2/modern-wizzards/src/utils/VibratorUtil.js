// import Options from '../Options.js';

export default class VibratorUtil {

  static init() {
    navigator.vibrate = navigator.vibrate
      || navigator.webkitVibrate
      || navigator.mozVibrate
      || navigator.msVibrate;
  }
	
  static vibrate(pattern, options, repeat=1) {
    if(!options.USE_VIBRATIONS) {
      return;
    }
    // TODO: sprawidzic czy apka otwarta jest tabie
    if (navigator.vibrate) {
      navigator.vibrate(pattern);
    }
  }
}
