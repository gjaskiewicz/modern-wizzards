import GeoPoint from "../gis/GeoPoint.js";
import MWMath from "./MWMath.js";
import $log from "./log.js";

const TAG = "MoveGeoPoint";

export default class MoveLocation {
  /**
   * Method moves passed GeoPoint with certain distance with certain direction. 
   * @param gp 
   * @param distance
   * @param bearing
   */
  static moveLocation(/* Location */ lt, distance, bearing) {
    const dist = distance/6371000.0;
    const brng = MWMath.toRadians(bearing);
    const lat1 = MWMath.toRadians(lt.getLatitude());
    const lon1 = MWMath.toRadians(lt.getLongitude());

    const lat2 = Math.asin( Math.sin(lat1)*Math.cos(dist) + Math.cos(lat1)*Math.sin(dist)*Math.cos(brng) );
    const a = Math.atan2(Math.sin(brng)*Math.sin(dist)*Math.cos(lat1), Math.cos(dist)-Math.sin(lat1)*Math.sin(lat2));

    let lon2 = lon1 + a;

    lon2 = (lon2 + 3 * Math.PI) % (2 * Math.PI) - Math.PI;
    
    lt.setLatitude(MWMath.toDegrees(lat2));
    lt.setLongitude(MWMath.toDegrees(lon2));
  }
  
  static moveGeoPoint(/* GeoPoint */ gp, distance, bearing) {
    const dist = distance/6371000.0;
    const brng = MWMath.toRadians(bearing);
    const lat1 = MWMath.toRadians(gp.getLatitudeE6()/1E6);
    const lon1 = MWMath.toRadians(gp.getLongitudeE6()/1E6);

    const lat2 = Math.asin( Math.sin(lat1)*Math.cos(dist) + Math.cos(lat1)*Math.sin(dist)*Math.cos(brng) );
    const a = Math.atan2(Math.sin(brng)*Math.sin(dist)*Math.cos(lat1), Math.cos(dist)-Math.sin(lat1)*Math.sin(lat2));

    let lon2 = lon1 + a;

    lon2 = (lon2+ 3*Math.PI) % (2*Math.PI) - Math.PI;
    
    return new GeoPoint(MWMath.toDegrees(lat2), MWMath.toDegrees(lon2));
  }
}
