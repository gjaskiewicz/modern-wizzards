import RNG from './RNG.js';

const RAND = new RNG();

export default class Dice {

  /** zwraca "rzut ko�cia" w przedziale <0-1>, uwzgl�dniaj�c szcz�cie
	 * zawsze czym wi�cej tym bardziej "na korzysc!" rzucaj�cego!
	 */
  static getDiceRoll(state, creature)	{
		let roll = RAND.nextDouble();
		let positive = this.baseLuckFactor;
		let negative = 1;
		if(creature.isUnderEffectOfSpell(state, "spell_badluck"))	{
			negative += 1;
		}
		if(creature.isUnderEffectOfSpell(state, "potion_luckjelly"))	{
			positive += 1;
    }
    if (creature.isPlayer()) {
      for (let ii of state.playerEgo.inventory)	{
        if (ii.nameID === "item_4clover" || ii.nameID === "item_bunnyleg") {
          positive += 0.2;
          break;
        }
      }
    }
		Math.pow(roll, negative/positive);
		return roll;
	}
}