import MWTime from './MWTime.js';

describe('MWTime', () => {
  it('should be constant through the day', () => {
    expect(MWTime.dayOfTs(31536000000)).toBe(365);
    for (let i=0; i < (24 * 60); i++) {
      expect(MWTime.dayOfTs(31536000000 + (i * 1000))).toBe(365);
    }
  });
});
