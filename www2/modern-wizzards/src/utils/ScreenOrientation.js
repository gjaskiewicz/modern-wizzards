export default class ScreenOrientation {

  static lock(screen) {
    const lockOrientationUniversal = screen.lockOrientation 
      || screen.mozLockOrientation 
      || screen.msLockOrientation;
    if (!lockOrientationUniversal) {
      return false;
    }
    return !!lockOrientationUniversal("portrait-primary");
  }
}
