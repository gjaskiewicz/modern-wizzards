class Logger {

  static Level = {
    NONE: 4,
    ERROR: 3,
    WARNING: 2,
    INFO: 1,
    DEBUG: 0,
  }

  constructor(level) {
    this.level = level
  }
  

  i(tag, txt) {
    this.level <= Logger.Level.INFO && console.log(`INFO(${tag}): ${txt}`);
  }

  d(tag, txt) {
    this.level <= Logger.Level.DEBUG && console.log(`DEBG(${tag}): ${txt}`);
  }

  e(tag, txt) {
    this.level <= Logger.Level.ERROR && console.log(`%c ERR (${tag}): ${txt}`, 'color: #ff0000');
  }
}

var $log = new Logger(Logger.Level.INFO);
export default $log; 
