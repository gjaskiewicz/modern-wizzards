import Collections from './Collections.js';

describe('Collections', () => {
  it('should shuffle', () => {
    let a = Collections.shuffle([1,2,3]);
    expect(a.length).toBe(3);
  })
});