import StatsHelper from './StatsHelper';

describe('StatsHelper', () => {

  it('handles uniform distribution', () => {
    const spec = ["a", "b"];

    let vA = 0;
    let vB = 0;
    for (let i=0;i<1.0;i+=0.001) {
      let v = StatsHelper.randomFromDistr(spec, () => 1, i);
      if (v == "a") vA++;
      if (v == "b") vB++;
    }
    expect(vA + vB).toBe(1000);
    expect(vA).toBe(500);
    expect(vB).toBe(500);
  });

  it('handles non-uniform distribution', () => {
    const spec = {
      "10%": 1,
      "90%": 9,
    };

    let v1 = 0;
    let v9 = 0;
    for (let i=0;i<1.0;i+=0.001) {
      let v = StatsHelper.randomFromDistr(Object.keys(spec), s => spec[s], i);
      if (v == "10%") v1++;
      if (v == "90%") v9++;
    }
    expect(v1 + v9).toBe(1000);
    expect(v1).toBe(100);
    expect(v9).toBe(900);
  });
});
