import MWToast from '../ui/MWToast.js';
import R from '../R.js';
import MWTime from './MWTime.js';

export default class GameSaver {

  static isDisabled = false;
  static lastSaveTimeSec = 0;

  static disable() {
    GameSaver.isDisabled = true;
  }

  static saveGame(state) {
    GameSaver.lastSaveTimeSec = MWTime.s();
    if (GameSaver.isDisabled) {
      if (!state) {
        throw "Cannot save falsy state!";
      }
      return;
    }
    const update_state = {
      playerEgo: {
        ...state.playerEgo,
        underEffectOfSpell: null,
      },
      gameState: state.gameState,
      achivments: state.achivments,
      options: state.options,
      saveData: {
        lastGameSaveMs: (new Date().getTime()),
      },
      isInitialized: true,
      worldContainer: state.worldContainer,
      quests: state.quests,
    };
    window.gameController.ref.update(update_state).then(i => {
      MWToast.toast(R.strings.msg_saved);
    });
  }
};
