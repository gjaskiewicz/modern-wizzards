import ScreenOrientation from './ScreenOrientation.js';

describe('ScreenOrientation', () => {

  it('should not break', () => {
    expect(ScreenOrientation.lock({})).toBe(false);
  });

});
