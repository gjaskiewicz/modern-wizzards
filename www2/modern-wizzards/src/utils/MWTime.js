export default class MWTime {

  static day() {
    const data = new Date();
    return MWTime.dayOfTs(data.getTime());
  }

  static dayOfTs(ts) {
    return Math.floor(ts / (1000 * 60 * 60 * 24));
  }

  static s() {
    const data = new Date();
    return data.getTime() / 1000;
  }
}
