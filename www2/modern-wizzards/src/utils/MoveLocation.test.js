import GeoPoint from '../gis/GeoPoint.js';
import Units from '../gis/Units.js';
import MoveLocation from './MoveLocation.js';

describe('MoveLocation', () => {
  const warsaw = new GeoPoint(52.233333, 21.016667);
  const radom = new GeoPoint(51.4, 21.166667);

  it('move GeoPoint', () => {
    var loc = MoveLocation.moveGeoPoint(warsaw, 92000, 175);
    expect(Units.optimizedDistance(radom, loc)).toBeLessThan(3000); // ~3 km
  });
});
