import RNG from './RNG';

describe('RNG', () => {

  it('creates same random int numbers', () => {
    for (let i = 0; i < 10; i++) {
      const r = new RNG(0);
      expect(r.nextInt() % 100).toBe(45);
      expect(r.nextInt() % 100).toBe(6);
      expect(r.nextInt() % 100).toBe(8);
      expect(r.nextInt() % 100).toBe(36);
      expect(r.nextInt() % 100).toBe(36);
    }
  });

  it('creates same random float numbers', () => {
    for (let i = 0; i < 10; i++) {
      const r = new RNG(0);
      expect(r.nextFloat()).toBe(0.000005748588594490936);
      expect(r.nextFloat()).toBe(0.6551540487702722);
      expect(r.nextFloat()).toBe(0.30481433882602227);
      expect(r.nextFloat()).toBe(0.6324834826553629);
      expect(r.nextFloat()).toBe(0.9958810810911847);
    }
  });
});
