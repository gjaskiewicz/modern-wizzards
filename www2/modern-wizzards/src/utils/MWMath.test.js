import MWMath from './MWMath';

describe('MWMath', () => {

  it('calculates approx. sin values', () => {
    let integral = 0;
    for(let i = 0; i < 360; i++) {
      const v = MWMath.sin(i);
      integral += v*v * (Math.PI / 180);
    }
    expect(Math.abs(integral - Math.PI)).toBeLessThan(0.000001);
  });
});
