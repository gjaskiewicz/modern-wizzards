import Dice from './Dice.js';
import Bunny from '../gamelogic/creatures/Bunny.js';
import Player from '../gamelogic/creatures/Player.js';
import TestData from '../test/TestData.js';
import LuckPotion from '../gamelogic/mixtures/LuckPotion.js'
import BadLuck from '../gamelogic/spells/BadLuck.js';

describe('Dice', () => {
  it('should roll for Bunny', () => {
    let s = { };
    let creature = new Bunny();
    let res = 0;
    for (let i = 0; i < 100; i++) {
      res += Dice.getDiceRoll(s, creature);
    }
    expect(res).toBeGreaterThan(0.1);
  });

  it('should roll for Player', () => {
    let s = {
      playerEgo: TestData.getFullEgo()
    };
    let res = 0;
    for (let i = 0; i < 100; i++) {
      res += Dice.getDiceRoll(s, Player.INSTANCE);
    }
    expect(res).toBeGreaterThan(0.1);
  });

  it('should roll for Player with inventory modifiers', () => {
    let s = {
      playerEgo: {
        ...TestData.getFullEgo(),
        inventory: [
          {name: 'item_4clover', count: 1},
          {name: 'item_bunnyleg', count: 1},
        ]
      }
    };
    let res = 0;
    for (let i = 0; i < 100; i++) {
      res += Dice.getDiceRoll(s, Player.INSTANCE);
    }
    expect(res).toBeGreaterThan(0.1);
  });

  it('should roll for Player with potion modifiers', () => {
    let s = {
      playerEgo: TestData.getFullEgo(),
    };
    const potion = new LuckPotion(Player.INSTANCE);
    s = potion.castThis(s).state;
    let res = 0;
    for (let i = 0; i < 100; i++) {
      res += Dice.getDiceRoll(s, Player.INSTANCE);
    }
    expect(res).toBeGreaterThan(0.1);
  });

  it('should roll for Player with spell modifiers', () => {
    let s = {
      playerEgo: TestData.getFullEgo(),
      ui: { } // fightCreature?
    };
    const spell = new BadLuck(new Bunny());
    spell.setTarget(Player.INSTANCE);
    const {state, success} = spell.castThis(s);
    s = state;
    expect(success).toBe(true);
    let res = 0;
    for (let i = 0; i < 100; i++) {
      res += Dice.getDiceRoll(s, Player.INSTANCE);
    }
    expect(res).toBeGreaterThan(0.1);
  });
})